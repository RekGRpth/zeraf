.class public Lcom/mediatek/datatransfer/modules/SmsBackupComposer;
.super Lcom/mediatek/datatransfer/modules/Composer;
.source "SmsBackupComposer.java"


# static fields
.field private static final BEGIN_VBODY:Ljava/lang/String; = "BEGIN:VBODY"

.field private static final BEGIN_VCARD:Ljava/lang/String; = "BEGIN:VCARD"

.field private static final BEGIN_VMSG:Ljava/lang/String; = "BEGIN:VMSG"

.field private static final CHARSET:Ljava/lang/String; = "CHARSET="

.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/SmsBackupComposer"

.field private static final COLUMN_NAME_ADDRESS:Ljava/lang/String; = "address"

.field private static final COLUMN_NAME_BODY:Ljava/lang/String; = "body"

.field private static final COLUMN_NAME_DATE:Ljava/lang/String; = "date"

.field private static final COLUMN_NAME_LOCKED:Ljava/lang/String; = "locked"

.field private static final COLUMN_NAME_READ:Ljava/lang/String; = "read"

.field private static final COLUMN_NAME_SC:Ljava/lang/String; = "service_center"

.field private static final COLUMN_NAME_SEEN:Ljava/lang/String; = "seen"

.field private static final COLUMN_NAME_SIM_ID:Ljava/lang/String; = "sim_id"

.field private static final COLUMN_NAME_THREAD_ID:Ljava/lang/String; = "thread_id"

.field private static final COLUMN_NAME_TYPE:Ljava/lang/String; = "type"

.field private static final DATE:Ljava/lang/String; = "Date:"

.field private static final ENCODING:Ljava/lang/String; = "ENCODING="

.field private static final END_VBODY:Ljava/lang/String; = "END:VBODY"

.field private static final END_VCARD:Ljava/lang/String; = "END:VCARD"

.field private static final END_VMSG:Ljava/lang/String; = "END:VMSG"

.field private static final FROMTEL:Ljava/lang/String; = "FROMTEL:"

.field private static final QUOTED:Ljava/lang/String; = "QUOTED-PRINTABLE"

.field private static final SUBJECT:Ljava/lang/String; = "Subject;"

.field private static final TRICKY_TO_GET_DRAFT_SMS_ADDRESS:Ljava/lang/String; = "canonical_addresses.address from sms,threads,canonical_addresses where sms.thread_id=threads._id and threads.recipient_ids=canonical_addresses._id and sms.thread_id ="

.field private static final UTF:Ljava/lang/String; = "UTF-8"

.field private static final VERSION:Ljava/lang/String; = "VERSION:"

.field private static final VMESSAGE_END_OF_COLON:Ljava/lang/String; = ":"

.field private static final VMESSAGE_END_OF_LINE:Ljava/lang/String; = "\r\n"

.field private static final VMESSAGE_END_OF_SEMICOLON:Ljava/lang/String; = ";"

.field private static final XBOX:Ljava/lang/String; = "X-BOX:"

.field private static final XLOCKED:Ljava/lang/String; = "X-LOCKED:"

.field private static final XREAD:Ljava/lang/String; = "X-READ:"

.field private static final XSEEN:Ljava/lang/String; = "X-SEEN:"

.field private static final XSIMID:Ljava/lang/String; = "X-SIMID:"

.field private static final XTYPE:Ljava/lang/String; = "X-TYPE:"

.field private static final mSmsUriArray:[Landroid/net/Uri;

.field private static final mStorageName:Ljava/lang/String; = "sms.vmsg"

.field private static final mStoragePath:Ljava/lang/String; = "sms"


# instance fields
.field private mSmsCursorArray:[Landroid/database/Cursor;

.field mWriter:Ljava/io/Writer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Landroid/provider/Telephony$Sms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Landroid/provider/Telephony$Sms$Sent;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsUriArray:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/modules/Composer;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/database/Cursor;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    iput-object v2, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mWriter:Ljava/io/Writer;

    return-void
.end method

.method public static combineVmsg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BEGIN:VMSG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "VERSION:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "1.1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "BEGIN:VCARD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "FROMTEL:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "END:VCARD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "BEGIN:VBODY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "X-BOX:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "X-READ:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "X-SEEN:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "X-SIMID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "X-LOCKED:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "X-TYPE:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "SMS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Date:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Subject;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "ENCODING="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "QUOTED-PRINTABLE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "CHARSET="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "END:VBODY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "END:VMSG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private formatTimeStampString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # J

    const-string v1, "yyyy-MM-dd kk:mm:ss"

    invoke-static {v1, p2, p3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getCount()I
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    add-int/2addr v1, v5

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const-string v5, "DataTransfer/SmsBackupComposer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getCount():"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method public getModuleType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public implementComposeOneEntity()Z
    .locals 35

    const/16 v24, 0x0

    const/4 v15, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    array-length v3, v3

    if-ge v15, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    aget-object v3, v3, v15

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    aget-object v3, v3, v15

    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    aget-object v33, v3, v15

    const-string v3, "date"

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-wide/from16 v1, v19

    invoke-direct {v0, v3, v1, v2}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->formatTimeStampString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v32

    const-string v3, "read"

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    if-nez v22, :cond_6

    const-string v23, "UNREAD"

    :goto_1
    const-string v3, "seen"

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v3, "type"

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/4 v12, 0x0

    packed-switch v11, :pswitch_data_0

    const-string v12, "INBOX"

    :goto_2
    const-string v3, "sim_id"

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v27

    const-string v18, "0"

    const-wide/16 v3, 0x0

    cmp-long v3, v27, v3

    if-ltz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v27

    invoke-static {v3, v0, v1}, Lcom/mediatek/telephony/SimInfoManager;->getSlotById(Landroid/content/Context;J)I

    move-result v29

    add-int/lit8 v3, v29, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    :cond_0
    const-string v3, "locked"

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const/4 v3, 0x1

    move/from16 v0, v16

    if-ne v0, v3, :cond_7

    const-string v17, "LOCKED"

    :goto_3
    const/16 v30, 0x0

    const/4 v3, 0x3

    if-ne v15, v3, :cond_8

    const-string v3, "thread_id"

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "content://sms"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "canonical_addresses.address from sms,threads,canonical_addresses where sms.thread_id=threads._id and threads.recipient_ids=canonical_addresses._id and sms.thread_id ="

    move-object/from16 v0, v34

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v34, " --"

    move-object/from16 v0, v34

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    if-eqz v13, :cond_b

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "address"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    :goto_4
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :goto_5
    if-nez v8, :cond_1

    const-string v8, ""

    :cond_1
    const-string v3, "service_center"

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    const-string v3, "body"

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v25, Ljava/lang/StringBuffer;

    move-object/from16 v0, v25

    invoke-direct {v0, v9}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const/16 v21, 0x0

    const-string v3, "END:VBODY"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v21

    :cond_2
    if-ltz v21, :cond_3

    const-string v3, "/"

    move-object/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    const-string v3, "END:VBODY"

    add-int/lit8 v4, v21, 0x1

    const-string v5, "END:VBODY"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;I)I

    move-result v21

    if-gez v21, :cond_2

    :cond_3
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mWriter:Ljava/io/Writer;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mWriter:Ljava/io/Writer;

    move-object/from16 v34, v0

    move-object/from16 v3, v32

    move-object/from16 v4, v23

    move-object v5, v12

    move-object/from16 v6, v18

    move-object/from16 v7, v17

    invoke-static/range {v3 .. v10}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->combineVmsg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v24, 0x1

    :cond_4
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->moveToNext()Z

    :cond_5
    :goto_6
    return v24

    :cond_6
    const-string v23, "READ"

    goto/16 :goto_1

    :pswitch_0
    const-string v12, "INBOX"

    goto/16 :goto_2

    :pswitch_1
    const-string v12, "SENDBOX"

    goto/16 :goto_2

    :cond_7
    const-string v17, "UNLOCKED"

    goto/16 :goto_3

    :cond_8
    const-string v3, "address"

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v33

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_5

    :catch_0
    move-exception v14

    :try_start_1
    const-string v3, "DataTransfer/SmsBackupComposer"

    const-string v4, "mWriter.write() failed"

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_6

    :catchall_0
    move-exception v3

    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->moveToNext()Z

    throw v3

    :cond_9
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    :cond_a
    move-object/from16 v8, v30

    goto/16 :goto_4

    :cond_b
    move-object/from16 v8, v30

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public init()Z
    .locals 9

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    :goto_0
    sget-object v0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsUriArray:[Landroid/net/Uri;

    array-length v0, v0

    if-ge v6, v0, :cond_1

    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsUriArray:[Landroid/net/Uri;

    aget-object v1, v1, v6

    const-string v5, "date ASC"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v8, v6

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    aget-object v0, v0, v6

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    aget-object v0, v0, v6

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v7, 0x1

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "DataTransfer/SmsBackupComposer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v7
.end method

.method public isAfterLast()Z
    .locals 8

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v4, 0x0

    :cond_0
    const-string v5, "DataTransfer/SmsBackupComposer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isAfterLast():"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v4

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public final onEnd()V
    .locals 7

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    :try_start_0
    const-string v5, "DataTransfer/SmsBackupComposer"

    const-string v6, "SmsBackupComposer onEnd"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mWriter:Ljava/io/Writer;

    if-eqz v5, :cond_0

    const-string v5, "DataTransfer/SmsBackupComposer"

    const-string v6, "mWriter.close()"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mWriter:Ljava/io/Writer;

    invoke-virtual {v5}, Ljava/io/Writer;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mSmsCursorArray:[Landroid/database/Cursor;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v5, "DataTransfer/SmsBackupComposer"

    const-string v6, "mWriter.close() failed"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final onStart()V
    .locals 6

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onStart()V

    const-string v3, "DataTransfer/SmsBackupComposer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onStart():mParentFolderPath:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "sms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "sms.vmsg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    :try_start_1
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v3, p0, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->mWriter:Ljava/io/Writer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v3, "DataTransfer/SmsBackupComposer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onStart():file:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DataTransfer/SmsBackupComposer"

    const-string v4, "onStart():create file failed"

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v3, "DataTransfer/SmsBackupComposer"

    const-string v4, "new BufferedWriter failed"

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
