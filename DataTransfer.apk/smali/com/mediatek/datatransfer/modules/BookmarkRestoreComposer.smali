.class public Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;
.super Lcom/mediatek/datatransfer/modules/Composer;
.source "BookmarkRestoreComposer.java"


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/BookmarkRestoreComposer"

.field private static final FILE_EXT:Ljava/lang/String; = ".url"

.field private static final TAG_BASEURL:Ljava/lang/String; = "BASEURL="

.field private static final TAG_DEFAULT:Ljava/lang/String; = "[DEFAULT]"

.field private static final TAG_SHORTCUT:Ljava/lang/String; = "[InternetShortcut]"

.field private static final TAG_URL:Ljava/lang/String; = "URL="


# instance fields
.field final ANYWHERE:Ljava/lang/String;

.field final EMBEDDED:Ljava/lang/String;

.field final HOSTNAME:Ljava/lang/String;

.field final NOT_END:Ljava/lang/String;

.field final NOT_IN:Ljava/lang/String;

.field final PORT:Ljava/lang/String;

.field final SUB_DOMAIN:Ljava/lang/String;

.field final TOP_DOMAINS:Ljava/lang/String;

.field final URL:Ljava/lang/String;

.field final URL_PATH:Ljava/lang/String;

.field private mFileList:[Ljava/io/File;

.field private mIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/modules/Composer;-><init>(Landroid/content/Context;)V

    const-string v0, "(?i:[a-z0-9]|[a-z0-9][-a-z0-9]*[a-z0-9])"

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->SUB_DOMAIN:Ljava/lang/String;

    const-string v0, "(?x-i:com\\b|edu\\b|biz\\b|in(?:t|fo)\\b|mil\\b|net\\b|org\\b|[a-z][a-z]\\b)"

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->TOP_DOMAINS:Ljava/lang/String;

    const-string v0, "(?:(?i:[a-z0-9]|[a-z0-9][-a-z0-9]*[a-z0-9])\\.)+(?x-i:com\\b|edu\\b|biz\\b|in(?:t|fo)\\b|mil\\b|net\\b|org\\b|[a-z][a-z]\\b)"

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->HOSTNAME:Ljava/lang/String;

    const-string v0, ";:\"\'<>()\\[\\]{}\\s\\x7F-\\xFF"

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->NOT_IN:Ljava/lang/String;

    const-string v0, "!.,?"

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->NOT_END:Ljava/lang/String;

    const-string v0, "[^;:\"\'<>()\\[\\]{}\\s\\x7F-\\xFF!.,?]"

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->ANYWHERE:Ljava/lang/String;

    const-string v0, "[!.,?]"

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->EMBEDDED:Ljava/lang/String;

    const-string v0, "/[^;:\"\'<>()\\[\\]{}\\s\\x7F-\\xFF!.,?]*([!.,?]+[^;:\"\'<>()\\[\\]{}\\s\\x7F-\\xFF!.,?]+)*"

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->URL_PATH:Ljava/lang/String;

    const-string v0, "(?:[0-5]?[0-9]{1,4}|6(?:[0-4][0-9]{3}|5(?:[0-4][0-9]{2}|5(?:[0-2][0-9]|3[0-5]))))"

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->PORT:Ljava/lang/String;

    const-string v0, "(?x:\n\\b\n(?:\n(?:ftp|https?)://[-\\w]+(\\.\\w[-\\w]*)+\n|\n(?:(?i:[a-z0-9]|[a-z0-9][-a-z0-9]*[a-z0-9])\\.)+(?x-i:com\\b|edu\\b|biz\\b|in(?:t|fo)\\b|mil\\b|net\\b|org\\b|[a-z][a-z]\\b)\n)\n(?::(?:[0-5]?[0-9]{1,4}|6(?:[0-4][0-9]{3}|5(?:[0-4][0-9]{2}|5(?:[0-2][0-9]|3[0-5])))))?\n(?:/[^;:\"\'<>()\\[\\]{}\\s\\x7F-\\xFF!.,?]*([!.,?]+[^;:\"\'<>()\\[\\]{}\\s\\x7F-\\xFF!.,?]+)*)?\n)"

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->URL:Ljava/lang/String;

    return-void
.end method

.method private deleteAllBookmarks()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private parseBookmarkFileUrl(Ljava/io/File;)Ljava/lang/String;
    .locals 11
    .param p1    # Ljava/io/File;

    const/4 v7, 0x0

    const/4 v1, 0x0

    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v3}, Ljava/io/FileInputStream;->available()I

    move-result v5

    new-array v0, v5, [B

    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    const-string v8, "UTF-8"

    invoke-static {v0, v8}, Lorg/apache/http/util/EncodingUtils;->getString([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v8, "DataTransfer/BookmarkRestoreComposer"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "content = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_0
    move-object v6, v7

    :cond_1
    :goto_0
    return-object v6

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    move-object v6, v7

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    move-object v6, v7

    goto :goto_0

    :cond_2
    const-string v8, "URL="

    invoke-virtual {v1, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    const-string v9, "URL="

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int v4, v8, v9

    const-string v8, "DataTransfer/BookmarkRestoreComposer"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "content = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "  index = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    const-string v8, "DataTransfer/BookmarkRestoreComposer"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "url = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v6, :cond_1

    move-object v6, v7

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    array-length v0, v0

    goto :goto_0
.end method

.method public getModuleType()I
    .locals 1

    const/16 v0, 0x200

    return v0
.end method

.method protected implementComposeOneEntity()Z
    .locals 9

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->isAfterLast()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    iget v7, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mIndex:I

    aget-object v0, v6, v7

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    const-string v7, ".url"

    invoke-virtual {v1, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0}, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->parseBookmarkFileUrl(Ljava/io/File;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "DataTransfer/BookmarkRestoreComposer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "implementComposeOneEntity():url  = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  and file is = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "bookmark"

    iget v7, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mIndex:I

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "title"

    invoke-virtual {v2, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "url"

    invoke-virtual {v2, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    const/4 v3, 0x1

    iget v6, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mIndex:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mIndex:I

    :cond_0
    const-string v6, "DataTransfer/BookmarkRestoreComposer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "implementComposeOneEntity():"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method

.method public init()Z
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "bookmark"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const-string v2, "DataTransfer/BookmarkRestoreComposer"

    const-string v3, "Bookmark backup file is not correct"

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    array-length v2, v2

    if-nez v2, :cond_3

    :cond_2
    const-string v2, "DataTransfer/BookmarkRestoreComposer"

    const-string v3, "init(): Bookmark backup folder is empty"

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v1, "DataTransfer/BookmarkRestoreComposer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Init(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isAfterLast()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mIndex:I

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mFileList:[Ljava/io/File;

    array-length v2, v2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEnd()V
    .locals 2

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    const-string v0, "DataTransfer/BookmarkRestoreComposer"

    const-string v1, "onEnd()"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onStart()V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->deleteAllBookmarks()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;->mIndex:I

    const-string v0, "DataTransfer/BookmarkRestoreComposer"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
