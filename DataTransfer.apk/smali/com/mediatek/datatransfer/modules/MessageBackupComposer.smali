.class public Lcom/mediatek/datatransfer/modules/MessageBackupComposer;
.super Lcom/mediatek/datatransfer/modules/Composer;
.source "MessageBackupComposer.java"


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/MessageBackupComposer"


# instance fields
.field private mMessageComposers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/modules/Composer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/modules/Composer;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getComposed(I)I
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getComposed()I

    move-result v1

    :cond_1
    return v1
.end method

.method public getCount()I
    .locals 6

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0

    :cond_1
    const-string v3, "DataTransfer/MessageBackupComposer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCount():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method

.method public getModuleType()I
    .locals 1

    const/16 v0, 0x40

    return v0
.end method

.method public implementComposeOneEntity()Z
    .locals 4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->composeOneEntity()Z

    move-result v2

    :cond_1
    return v2
.end method

.method public init()Z
    .locals 6

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->init()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const-string v3, "DataTransfer/MessageBackupComposer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "init():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v2
.end method

.method public isAfterLast()Z
    .locals 6

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x0

    :cond_1
    const-string v3, "DataTransfer/MessageBackupComposer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isAfterLast():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v2
.end method

.method public onEnd()V
    .locals 3

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onStart()V

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->onStart()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final setParentFolderPath(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParams:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParams:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParams:Ljava/util/List;

    const-string v3, "sms"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    new-instance v3, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParams:Ljava/util/List;

    const-string v3, "mms"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    new-instance v3, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;->mMessageComposers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    invoke-virtual {v0, p1}, Lcom/mediatek/datatransfer/modules/Composer;->setParentFolderPath(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    return-void
.end method
