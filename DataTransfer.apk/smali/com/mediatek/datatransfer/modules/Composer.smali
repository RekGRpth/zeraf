.class public abstract Lcom/mediatek/datatransfer/modules/Composer;
.super Ljava/lang/Object;
.source "Composer.java"


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/Composer"


# instance fields
.field private mComposeredCount:I

.field protected mContext:Landroid/content/Context;

.field protected mIsCancel:Z

.field protected mParams:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mParentFolderPath:Ljava/lang/String;

.field protected mReporter:Lcom/mediatek/datatransfer/ProgressReporter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mIsCancel:Z

    iput v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mComposeredCount:I

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public composeOneEntity()Z
    .locals 2

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/Composer;->implementComposeOneEntity()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mComposeredCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mComposeredCount:I

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    invoke-interface {v1, p0, v0}, Lcom/mediatek/datatransfer/ProgressReporter;->onOneFinished(Lcom/mediatek/datatransfer/modules/Composer;Z)V

    :cond_1
    return v0
.end method

.method public getComposed()I
    .locals 1

    iget v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mComposeredCount:I

    return v0
.end method

.method public abstract getCount()I
.end method

.method public abstract getModuleType()I
.end method

.method protected abstract implementComposeOneEntity()Z
.end method

.method public increaseComposed(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mComposeredCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mComposeredCount:I

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    invoke-interface {v0, p0, p1}, Lcom/mediatek/datatransfer/ProgressReporter;->onOneFinished(Lcom/mediatek/datatransfer/modules/Composer;Z)V

    :cond_1
    return-void
.end method

.method public abstract init()Z
.end method

.method public abstract isAfterLast()Z
.end method

.method public declared-synchronized isCancel()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mIsCancel:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onEnd()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v1

    iget v2, p0, Lcom/mediatek/datatransfer/modules/Composer;->mComposeredCount:I

    if-ne v1, v2, :cond_1

    iget v1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mComposeredCount:I

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    invoke-interface {v1, p0, v0}, Lcom/mediatek/datatransfer/ProgressReporter;->onEnd(Lcom/mediatek/datatransfer/modules/Composer;Z)V

    const-string v1, "DataTransfer/Composer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onEnd: result is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "DataTransfer/Composer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onEnd: getCount is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", and composed count is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/datatransfer/modules/Composer;->mComposeredCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    invoke-interface {v0, p0}, Lcom/mediatek/datatransfer/ProgressReporter;->onStart(Lcom/mediatek/datatransfer/modules/Composer;)V

    :cond_0
    return-void
.end method

.method public declared-synchronized setCancel(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mIsCancel:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setParams(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParams:Ljava/util/List;

    return-void
.end method

.method public setParentFolderPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    return-void
.end method

.method public setReporter(Lcom/mediatek/datatransfer/ProgressReporter;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/ProgressReporter;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    return-void
.end method
