.class Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;
.super Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;
.source "AppRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/AppRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppRestoreStatusListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/AppRestoreActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/AppRestoreActivity;Lcom/mediatek/datatransfer/AppRestoreActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/AppRestoreActivity;
    .param p2    # Lcom/mediatek/datatransfer/AppRestoreActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;-><init>(Lcom/mediatek/datatransfer/AppRestoreActivity;)V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Lcom/mediatek/datatransfer/modules/Composer;I)V
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$800(Lcom/mediatek/datatransfer/AppRestoreActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProgressChange, p = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;-><init>(Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;ILcom/mediatek/datatransfer/modules/Composer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onRestoreEnd(ZLjava/util/ArrayList;)V
    .locals 2
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$800(Lcom/mediatek/datatransfer/AppRestoreActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onRestoreEnd"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$1;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$1;-><init>(Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
