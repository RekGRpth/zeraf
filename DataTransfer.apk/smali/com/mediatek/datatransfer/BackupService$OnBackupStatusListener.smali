.class public interface abstract Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;
.super Ljava/lang/Object;
.source "BackupService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/BackupService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnBackupStatusListener"
.end annotation


# virtual methods
.method public abstract onBackupEnd(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onBackupErr(Ljava/io/IOException;)V
.end method

.method public abstract onComposerChanged(Lcom/mediatek/datatransfer/modules/Composer;)V
.end method

.method public abstract onProgressChanged(Lcom/mediatek/datatransfer/modules/Composer;I)V
.end method
