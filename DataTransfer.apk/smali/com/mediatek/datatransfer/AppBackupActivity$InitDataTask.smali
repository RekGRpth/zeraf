.class Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;
.super Landroid/os/AsyncTask;
.source "AppBackupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/AppBackupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field appDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/AppSnippet;",
            ">;"
        }
    .end annotation
.end field

.field mAppInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mediatek/datatransfer/AppBackupActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/AppBackupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/AppBackupActivity;Lcom/mediatek/datatransfer/AppBackupActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/AppBackupActivity;
    .param p2    # Lcom/mediatek/datatransfer/AppBackupActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Long;
    .locals 8
    .param p1    # [Ljava/lang/Void;

    iget-object v6, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-static {v6}, Lcom/mediatek/datatransfer/modules/AppBackupComposer;->getUserAppInfoList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->mAppInfoList:Ljava/util/List;

    iget-object v6, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-virtual {v6}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->appDatas:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->mAppInfoList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageItemInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageItemInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v5, Lcom/mediatek/datatransfer/AppSnippet;

    iget-object v6, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-direct {v5, v1, v3, v6}, Lcom/mediatek/datatransfer/AppSnippet;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->appDatas:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v6, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->appDatas:Ljava/util/ArrayList;

    new-instance v7, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask$1;

    invoke-direct {v7, p0}, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask$1;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;)V

    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v6, 0x0

    return-object v6
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .locals 3
    .param p1    # Ljava/lang/Long;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v2, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->appDatas:Ljava/util/ArrayList;

    invoke-static {v0, v2}, Lcom/mediatek/datatransfer/AppBackupActivity;->access$200(Lcom/mediatek/datatransfer/AppBackupActivity;Ljava/util/ArrayList;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->appDatas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->setButtonsEnable(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->updateButtonState()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v2, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-static {v2}, Lcom/mediatek/datatransfer/AppBackupActivity;->access$300(Lcom/mediatek/datatransfer/AppBackupActivity;)Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->setOnBackupStatusListener(Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->showLoadingContent(Z)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->showLoadingContent(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    const v1, 0x7f060029

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->setButtonsEnable(Z)V

    return-void
.end method
