.class public Lcom/mediatek/datatransfer/BackupService$BackupBinder;
.super Landroid/os/Binder;
.source "BackupService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/BackupService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BackupBinder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/BackupService;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/BackupService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelBackup()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/BackupService;->access$002(Lcom/mediatek/datatransfer/BackupService;I)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupEngine;->cancel()V

    :cond_0
    const-string v0, "DataTransfer/BackupService"

    const-string v1, "cancelBackup"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public continueBackup()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/BackupService;->access$002(Lcom/mediatek/datatransfer/BackupService;I)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupEngine;->continueBackup()V

    :cond_0
    const-string v0, "DataTransfer/BackupService"

    const-string v1, "continueBackup"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getAppBackupResult()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$300(Lcom/mediatek/datatransfer/BackupService;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getBackupItemParam(I)Ljava/util/ArrayList;
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    iget-object v0, v0, Lcom/mediatek/datatransfer/BackupService;->mParasMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getBackupResult()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$200(Lcom/mediatek/datatransfer/BackupService;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getBackupResultType()Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$600(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    move-result-object v0

    return-object v0
.end method

.method public getCurBackupProgress()Lcom/mediatek/datatransfer/BackupService$BackupProgress;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$400(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    move-result-object v0

    return-object v0
.end method

.method public getState()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$000(Lcom/mediatek/datatransfer/BackupService;)I

    move-result v0

    return v0
.end method

.method public pauseBackup()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/BackupService;->access$002(Lcom/mediatek/datatransfer/BackupService;I)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupEngine;->pause()V

    :cond_0
    const-string v0, "DataTransfer/BackupService"

    const-string v1, "pauseBackup"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/BackupService;->access$002(Lcom/mediatek/datatransfer/BackupService;I)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$200(Lcom/mediatek/datatransfer/BackupService;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$200(Lcom/mediatek/datatransfer/BackupService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$300(Lcom/mediatek/datatransfer/BackupService;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$300(Lcom/mediatek/datatransfer/BackupService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    iget-object v0, v0, Lcom/mediatek/datatransfer/BackupService;->mParasMap:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    iget-object v0, v0, Lcom/mediatek/datatransfer/BackupService;->mParasMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_2
    return-void
.end method

.method public setBackupItemParam(ILjava/util/ArrayList;)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    iget-object v0, v0, Lcom/mediatek/datatransfer/BackupService;->mParasMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/datatransfer/BackupEngine;->setBackupItemParam(ILjava/util/ArrayList;)V

    return-void
.end method

.method public setBackupModelList(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->reset()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    new-instance v1, Lcom/mediatek/datatransfer/BackupEngine;

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    iget-object v3, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-direct {v1, v2, v3}, Lcom/mediatek/datatransfer/BackupEngine;-><init>(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)V

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/BackupService;->access$102(Lcom/mediatek/datatransfer/BackupService;Lcom/mediatek/datatransfer/BackupEngine;)Lcom/mediatek/datatransfer/BackupEngine;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/datatransfer/BackupEngine;->setBackupModelList(Ljava/util/ArrayList;)V

    return-void
.end method

.method public setOnBackupChangedListner(Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)V
    .locals 1
    .param p1    # Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v0, p1}, Lcom/mediatek/datatransfer/BackupService;->access$502(Lcom/mediatek/datatransfer/BackupService;Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    return-void
.end method

.method public startBackup(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    new-instance v2, Landroid/app/Notification;

    invoke-direct {v2}, Landroid/app/Notification;-><init>()V

    invoke-virtual {v1, v3, v2}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v1}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-virtual {v1, v2}, Lcom/mediatek/datatransfer/BackupEngine;->setOnBackupDoneListner(Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v1}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mediatek/datatransfer/BackupEngine;->startBackup(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v1, v3}, Lcom/mediatek/datatransfer/BackupService;->access$002(Lcom/mediatek/datatransfer/BackupService;I)I

    :goto_0
    const-string v1, "DataTransfer/BackupService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startBackup: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v1}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/mediatek/datatransfer/BackupEngine;->setOnBackupDoneListner(Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;)V

    goto :goto_0
.end method

.method public stopForeground()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->this$0:Lcom/mediatek/datatransfer/BackupService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Service;->stopForeground(Z)V

    const-string v0, "DataTransfer/BackupService"

    const-string v1, "stopFreground"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
