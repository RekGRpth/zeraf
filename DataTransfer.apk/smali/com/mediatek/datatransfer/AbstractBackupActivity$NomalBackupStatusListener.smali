.class public Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;
.super Ljava/lang/Object;
.source "AbstractBackupActivity.java"

# interfaces
.implements Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/AbstractBackupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NomalBackupStatusListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackupEnd(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onBackupErr(Ljava/io/IOException;)V
    .locals 2
    .param p1    # Ljava/io/IOException;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->errChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->pauseBackup()V

    :cond_0
    return-void
.end method

.method public onComposerChanged(Lcom/mediatek/datatransfer/modules/Composer;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;

    return-void
.end method

.method public onProgressChanged(Lcom/mediatek/datatransfer/modules/Composer;I)V
    .locals 2
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener$1;

    invoke-direct {v1, p0, p2}, Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener$1;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
