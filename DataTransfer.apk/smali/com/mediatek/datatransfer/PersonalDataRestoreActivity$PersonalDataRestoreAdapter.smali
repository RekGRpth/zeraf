.class Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;
.super Landroid/widget/BaseAdapter;
.source "PersonalDataRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PersonalDataRestoreAdapter"
.end annotation


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayoutId:I

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/PersonalItemData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/PersonalItemData;",
            ">;I)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p3, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mList:Ljava/util/ArrayList;

    iput p4, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mLayoutId:I

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public changeData(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/PersonalItemData;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mList:Ljava/util/ArrayList;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/PersonalItemData;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/PersonalItemData;->getType()I

    move-result v1

    int-to-long v1, v1

    return-wide v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v7, 0x0

    move-object v5, p2

    if-nez v5, :cond_0

    iget-object v6, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v8, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mLayoutId:I

    invoke-virtual {v6, v8, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    :cond_0
    iget-object v6, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/datatransfer/PersonalItemData;

    const/high16 v6, 0x7f080000

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v6, 0x7f080001

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v6, 0x7f080012

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/PersonalItemData;->getIconId()I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/PersonalItemData;->getTextId()I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/PersonalItemData;->isEnable()Z

    move-result v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    if-nez v1, :cond_1

    const/4 v6, 0x1

    :goto_0
    invoke-virtual {v5, v6}, Landroid/view/View;->setClickable(Z)V

    if-eqz v1, :cond_2

    iget-object v6, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-virtual {v6, p1}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedByPosition(I)Z

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_1
    return-object v5

    :cond_1
    move v6, v7

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v7}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-virtual {v5, v7}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1
.end method
