.class Lcom/mediatek/datatransfer/PersonalDataBackupActivity$5;
.super Ljava/lang/Object;
.source "PersonalDataBackupActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->showSIMLockedDialog(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

.field final synthetic val$simInfo:Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$5;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iput-object p2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$5;->val$simInfo:Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$5;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$5;->val$simInfo:Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$100(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-result-object v0

    invoke-static {}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$200()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "simInfo = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$5;->val$simInfo:Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-wide v3, v3, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimInfoId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$5;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v1, v0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$300(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$5;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/16 v2, 0x7da

    invoke-virtual {v1, v2}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method
