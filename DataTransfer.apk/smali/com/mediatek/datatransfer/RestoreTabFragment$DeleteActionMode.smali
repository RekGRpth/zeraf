.class Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;
.super Ljava/lang/Object;
.source "RestoreTabFragment.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/RestoreTabFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeleteActionMode"
.end annotation


# instance fields
.field private mCheckedCount:I

.field private mCheckedItemIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMode:Landroid/view/ActionMode;

.field final synthetic this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/RestoreTabFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setAllItemChecked(Z)V
    .locals 7
    .param p1    # Z

    iget-object v5, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-virtual {v5}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    const/4 v5, 0x0

    iput v5, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    iget-object v5, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->clear()V

    invoke-virtual {v4}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    invoke-virtual {v4, v2}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    instance-of v5, v3, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    if-eqz v5, :cond_0

    move-object v1, v3

    check-cast v1, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    invoke-virtual {v1, p1}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->setChecked(Z)V

    if-eqz p1, :cond_0

    iget-object v5, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->getAccociateFile()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget v5, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->updateTitle()V

    return-void
.end method

.method private updateTitle()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    const v2, 0x7f06002f

    invoke-virtual {v1, v2}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public confirmSyncCheckedPositons()V
    .locals 8

    const/4 v7, 0x0

    iput v7, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-virtual {v7}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {v5}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    invoke-virtual {v5, v3}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v4

    instance-of v7, v4, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    if-eqz v7, :cond_0

    move-object v2, v4

    check-cast v2, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    invoke-virtual {v2}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->getAccociateFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iget-object v7, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-virtual {v7, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->setChecked(Z)V

    iget v7, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/HashSet;->clear()V

    iput-object v6, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->updateTitle()V

    return-void
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v2

    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->setAllItemChecked(Z)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v2}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->setAllItemChecked(Z)V

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060034

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/RestoreTabFragment;->access$800(Lcom/mediatek/datatransfer/RestoreTabFragment;Ljava/util/HashSet;)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f080015
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    const/4 v3, 0x1

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mMode:Landroid/view/ActionMode;

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-static {v1}, Lcom/mediatek/datatransfer/RestoreTabFragment;->access$900(Lcom/mediatek/datatransfer/RestoreTabFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-direct {p0, v2}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->setAllItemChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-static {v1, v3}, Lcom/mediatek/datatransfer/RestoreTabFragment;->access$500(Lcom/mediatek/datatransfer/RestoreTabFragment;Z)V

    return v3
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 3
    .param p1    # Landroid/view/ActionMode;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iput-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    iput v2, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/RestoreTabFragment;->access$002(Lcom/mediatek/datatransfer/RestoreTabFragment;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreTabFragment;->access$900(Lcom/mediatek/datatransfer/RestoreTabFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-static {v0, v2}, Lcom/mediatek/datatransfer/RestoreTabFragment;->access$500(Lcom/mediatek/datatransfer/RestoreTabFragment;Z)V

    return-void
.end method

.method public onPreferenceItemClick(Landroid/preference/PreferenceScreen;I)V
    .locals 5
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    instance-of v4, v2, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    if-eqz v4, :cond_0

    move-object v1, v2

    check-cast v1, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->isChecked()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v1, v3}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->setChecked(Z)V

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->getAccociateFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    if-eqz v3, :cond_2

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget v4, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    :goto_1
    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->updateTitle()V

    :cond_0
    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget v4, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    goto :goto_1
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    const/4 v0, 0x0

    return v0
.end method

.method public restoreState(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const-string v4, "checkedItems"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-virtual {v4}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v4

    if-lez v4, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->confirmSyncCheckedPositons()V

    :cond_1
    return-void
.end method

.method public saveState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v3, "checkedItems"

    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public setItemChecked(Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;Z)V
    .locals 2
    .param p1    # Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->isChecked()Z

    move-result v1

    if-eq v1, p2, :cond_0

    invoke-virtual {p1, p2}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->setChecked(Z)V

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->updateTitle()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedItemIds:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteActionMode;->mCheckedCount:I

    goto :goto_0
.end method
