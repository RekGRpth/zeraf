.class Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BackupService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/BackupService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NewDataNotifyReceiver"
.end annotation


# static fields
.field public static final CLASS_TAG:Ljava/lang/String; = "NotificationReceiver"


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/BackupService;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/BackupService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v5, 0x0

    const-string v2, "com.mediatek.datatransfer.newdata"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "NotificationReceiver"

    const-string v3, "BackupService ------>ACTION_NEW_DATA_DETECTED received"

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "notify_type"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "filename"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v2}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-static {v2}, Lcom/mediatek/datatransfer/BackupService;->access$100(Lcom/mediatek/datatransfer/BackupService;)Lcom/mediatek/datatransfer/BackupEngine;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/datatransfer/BackupEngine;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/mediatek/datatransfer/utils/NotifyManager;->showNewDetectionNotification(ILjava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;->this$0:Lcom/mediatek/datatransfer/BackupService;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/datatransfer/BackupService$NewDataNotifyReceiver;->this$0:Lcom/mediatek/datatransfer/BackupService;

    const v4, 0x7f060040

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->abortBroadcast()V

    :cond_0
    return-void
.end method
