.class public Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;
.super Ljava/lang/Object;
.source "AbstractRestoreActivity.java"

# interfaces
.implements Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/AbstractRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NormalRestoreStatusListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComposerChanged(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    const-string v1, "onComposerChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onProgressChanged(Lcom/mediatek/datatransfer/modules/Composer;I)V
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;
    .param p2    # I

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProgressChange, p = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener$1;

    invoke-direct {v1, p0, p2}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener$1;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onRestoreEnd(ZLjava/util/ArrayList;)V
    .locals 2
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    const-string v1, "onRestoreEnd"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onRestoreErr(Ljava/io/IOException;)V
    .locals 2
    .param p1    # Ljava/io/IOException;

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    const-string v1, "onRestoreErr"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->errChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getState()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->pauseRestore()V

    :cond_0
    return-void
.end method
