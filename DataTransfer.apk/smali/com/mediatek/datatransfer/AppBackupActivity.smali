.class public Lcom/mediatek/datatransfer/AppBackupActivity;
.super Lcom/mediatek/datatransfer/AbstractBackupActivity;
.source "AppBackupActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;,
        Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;,
        Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;
    }
.end annotation


# instance fields
.field private CLASS_TAG:Ljava/lang/String;

.field private mAdapter:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;

.field private mBackupStatusListener:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

.field private mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/AppSnippet;",
            ">;"
        }
    .end annotation
.end field

.field private mInitDataTask:Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;

.field private mIsCheckedBackupStatus:Z

.field private mIsDataInitialed:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;-><init>()V

    const-string v0, "DataTransfer/AppBackupActivity"

    iput-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mData:Ljava/util/List;

    iput-boolean v2, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mIsDataInitialed:Z

    new-instance v0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity;Lcom/mediatek/datatransfer/AppBackupActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mBackupStatusListener:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    iput-boolean v2, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mIsCheckedBackupStatus:Z

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/AppBackupActivity;Ljava/util/ArrayList;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/AppBackupActivity;
    .param p1    # Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AppBackupActivity;->updateData(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/datatransfer/AppBackupActivity;)Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mBackupStatusListener:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/datatransfer/AppBackupActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/datatransfer/AppBackupActivity;Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AppBackupActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AppBackupActivity;->getAppSnippetByPackageName(Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/datatransfer/AppBackupActivity;Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AppBackupActivity;
    .param p1    # Lcom/mediatek/datatransfer/AppSnippet;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AppBackupActivity;->formatProgressDialogMsg(Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private formatProgressDialogMsg(Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/AppSnippet;

    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f06000e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/AppSnippet;->getName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getAppSnippetByPackageName(Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/AppSnippet;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/AppSnippet;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v2, v1

    :cond_1
    return-object v2
.end method

.method private getSelectedPackageNameList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mAdapter:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;

    invoke-virtual {v4}, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;->getCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/CheckedListActivity;->getItemByPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/AppSnippet;

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedByPosition(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/AppSnippet;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private updateData(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/AppSnippet;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "updateData, list is null"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mData:Ljava/util/List;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mAdapter:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;

    invoke-virtual {v0, p1}, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;->changeData(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->syncUnCheckedItems()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mAdapter:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AppBackupActivity;->updateTitle()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->updateButtonState()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mIsDataInitialed:Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "data is initialed, to checkBackupState"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AppBackupActivity;->checkBackupState()V

    goto :goto_0
.end method


# virtual methods
.method protected afterServiceConnected()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "afterServiceConnected, to checkBackupState"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AppBackupActivity;->checkBackupState()V

    return-void
.end method

.method protected checkBackupState()V
    .locals 9

    const/4 v8, 0x1

    iget-boolean v5, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mIsCheckedBackupStatus:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v6, "can not checkBackupState, as it has been checked"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v5, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mIsDataInitialed:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v6, "can not checkBackupState, wait data to initialed"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v6, "to checkBackupState"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v8, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mIsCheckedBackupStatus:Z

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v4

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "checkBackupState: state = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->checkBackupState()V

    goto :goto_0

    :pswitch_1
    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    const/16 v6, 0x10

    invoke-virtual {v5, v6}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getBackupItemParam(I)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getCurBackupProgress()Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    move-result-object v1

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "checkBackupState: Max = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mMax:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " curprogress = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v4, v8, :cond_3

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    :cond_3
    iget v5, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    iget v6, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mMax:I

    if-ge v5, v6, :cond_4

    iget v5, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/mediatek/datatransfer/AppBackupActivity;->getAppSnippetByPackageName(Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/datatransfer/AppBackupActivity;->formatProgressDialogMsg(Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget v6, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mMax:I

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setMax(I)V

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget v6, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getBackupResultType()Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v6}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getAppBackupResult()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/mediatek/datatransfer/AppBackupActivity;->showBackupResult(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public initBackupAdapter()Landroid/widget/BaseAdapter;
    .locals 3

    new-instance v0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;

    iget-object v1, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mData:Ljava/util/List;

    const/high16 v2, 0x7f030000

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity;Landroid/content/Context;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mAdapter:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mAdapter:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;

    return-object v0
.end method

.method public onCheckedCountChanged()V
    .locals 0

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->onCheckedCountChanged()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AppBackupActivity;->updateTitle()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-super {p0, p1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v1, Lcom/mediatek/datatransfer/AppBackupActivity$1;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/AppBackupActivity$1;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity;)V

    const v3, 0x7f060020

    invoke-static {p0, v3, p2, v1}, Lcom/mediatek/datatransfer/ResultDialog;->createResultDlg(Landroid/content/Context;ILandroid/os/Bundle;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    const v3, 0x7f060031

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    move-object v0, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 5
    .param p1    # I
    .param p2    # Landroid/app/Dialog;
    .param p3    # Landroid/os/Bundle;

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object v1, p2

    check-cast v1, Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mData:Ljava/util/List;

    const/4 v4, 0x1

    invoke-static {v3, p0, p3, v4}, Lcom/mediatek/datatransfer/ResultDialog;->createAppResultAdapter(Ljava/util/List;Landroid/content/Context;Landroid/os/Bundle;I)Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d4
        :pswitch_0
    .end packed-switch
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    new-instance v0, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity;Lcom/mediatek/datatransfer/AppBackupActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mInitDataTask:Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mInitDataTask:Lcom/mediatek/datatransfer/AppBackupActivity$InitDataTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected showBackupResult(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;)V
    .locals 6
    .param p1    # Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    sget-object v3, Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;->Cancel:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    if-eq p1, v3, :cond_2

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "result"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mData:Ljava/util/List;

    const/4 v4, 0x1

    invoke-static {v3, p0, v1, v4}, Lcom/mediatek/datatransfer/ResultDialog;->createAppResultAdapter(Ljava/util/List;Landroid/content/Context;Landroid/os/Bundle;I)Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;

    move-result-object v0

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f060020

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x104000a

    new-instance v5, Lcom/mediatek/datatransfer/AppBackupActivity$2;

    invoke-direct {v5, p0}, Lcom/mediatek/datatransfer/AppBackupActivity$2;-><init>(Lcom/mediatek/datatransfer/AppBackupActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->stopService()V

    goto :goto_0
.end method

.method public startBackup()V
    .locals 11

    const/16 v9, 0x10

    const/4 v10, 0x0

    iget-object v7, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v8, "startBackup"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->startService()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v7, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v7, v2}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->setBackupModelList(Ljava/util/ArrayList;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AppBackupActivity;->getSelectedPackageNameList()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v7, v9, v3}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->setBackupItemParam(ILjava/util/ArrayList;)V

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getAppsBackupPath()Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "backup path is: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v7, v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->startBackup(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->showProgress()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v7, v10}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/app/ProgressDialog;->setMax(I)V

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/mediatek/datatransfer/AppBackupActivity;->getAppSnippetByPackageName(Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/datatransfer/AppBackupActivity;->formatProgressDialogMsg(Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;

    move-result-object v4

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v7, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v7, 0x7d2

    invoke-virtual {p0, v7}, Landroid/app/Activity;->showDialog(I)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->stopService()V

    goto :goto_0
.end method

.method public updateTitle()V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f060029

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity;->mAdapter:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupAdapter;->getCount()I

    move-result v2

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AppBackupActivity;->getSelectedPackageNameList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
