.class public Lcom/mediatek/datatransfer/AppRestoreActivity;
.super Lcom/mediatek/datatransfer/AbstractRestoreActivity;
.source "AppRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;,
        Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;,
        Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;
    }
.end annotation


# instance fields
.field private CLASS_TAG:Ljava/lang/String;

.field private mAdapter:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;

.field private mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/AppSnippet;",
            ">;"
        }
    .end annotation
.end field

.field private mFile:Ljava/io/File;

.field private mInitDataTask:Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;

.field private mIsCheckedRestoreStatus:Z

.field private mIsDataInitialed:Z

.field private mRestoreStoreStatusListener:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;-><init>()V

    const-string v0, "DataTransfer/AppRestoreActivity"

    iput-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mIsDataInitialed:Z

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mIsCheckedRestoreStatus:Z

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/datatransfer/AppRestoreActivity;Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AppRestoreActivity;->getAppSnippetByApkName(Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/datatransfer/AppRestoreActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mData:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/mediatek/datatransfer/AppRestoreActivity;Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;
    .param p1    # Lcom/mediatek/datatransfer/AppSnippet;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AppRestoreActivity;->formatProgressDialogMsg(Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/AppRestoreActivity;)Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mAdapter:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/datatransfer/AppRestoreActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mIsDataInitialed:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/datatransfer/AppRestoreActivity;)Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mRestoreStoreStatusListener:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/datatransfer/AppRestoreActivity;Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;)Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;
    .param p1    # Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mRestoreStoreStatusListener:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/datatransfer/AppRestoreActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->checkRestoreState()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/datatransfer/AppRestoreActivity;)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/datatransfer/AppRestoreActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/datatransfer/AppRestoreActivity;Ljava/util/ArrayList;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/AppRestoreActivity;
    .param p1    # Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AppRestoreActivity;->showRestoreResult(Ljava/util/ArrayList;)V

    return-void
.end method

.method private checkRestoreState()V
    .locals 9

    const/4 v8, 0x1

    iget-boolean v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mIsCheckedRestoreStatus:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v6, "can not checkRestoreState, as it has been checked"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mIsDataInitialed:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v6, "can not checkRestoreState, wait data to initialed"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v6, "all ready. to checkRestoreState"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v8, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mIsCheckedRestoreStatus:Z

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getState()I

    move-result v4

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "checkRestoreState: state = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    const/16 v6, 0x10

    invoke-virtual {v5, v6}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getRestoreItemParam(I)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getCurRestoreProgress()Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    move-result-object v2

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "checkRestoreState: Max = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mMax:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " curprogress = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v4, v8, :cond_3

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->showProgressDialog()V

    :cond_3
    iget v5, v2, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    iget v6, v2, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mMax:I

    if-ge v5, v6, :cond_4

    iget v5, v2, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->getAppSnippetByApkName(Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/datatransfer/AppRestoreActivity;->formatProgressDialogMsg(Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogMessage(Ljava/lang/CharSequence;)V

    :cond_4
    iget v5, v2, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mMax:I

    invoke-virtual {p0, v5}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogMax(I)V

    iget v5, v2, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    invoke-virtual {p0, v5}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogProgress(I)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getAppRestoreResult()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/datatransfer/AppRestoreActivity;->showRestoreResult(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->errChecked()Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private formatProgressDialogMsg(Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/AppSnippet;

    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f06000f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/AppSnippet;->getName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getAppSnippetByApkName(Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/AppSnippet;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/AppSnippet;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v2, v1

    :cond_1
    return-object v2
.end method

.method private getSelectedApkNameList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/CheckedListActivity;->getItemByPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/AppSnippet;

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedByPosition(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/AppSnippet;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private showRestoreResult(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->dismissProgressDialog()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "result"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mData:Ljava/util/List;

    const/4 v4, 0x2

    invoke-static {v3, p0, v1, v4}, Lcom/mediatek/datatransfer/ResultDialog;->createAppResultAdapter(Ljava/util/List;Landroid/content/Context;Landroid/os/Bundle;I)Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;

    move-result-object v0

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f060021

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x104000a

    new-instance v5, Lcom/mediatek/datatransfer/AppRestoreActivity$1;

    invoke-direct {v5, p0}, Lcom/mediatek/datatransfer/AppRestoreActivity$1;-><init>(Lcom/mediatek/datatransfer/AppRestoreActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    return-void
.end method


# virtual methods
.method protected afterServiceConnected()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "afterServiceConnected, to checkRestorestate"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->checkRestoreState()V

    return-void
.end method

.method public initAdapter()Landroid/widget/BaseAdapter;
    .locals 3

    new-instance v0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;

    const/4 v1, 0x0

    const/high16 v2, 0x7f030000

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;-><init>(Lcom/mediatek/datatransfer/AppRestoreActivity;Landroid/content/Context;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mAdapter:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mAdapter:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;

    return-object v0
.end method

.method protected notifyListItemCheckedChanged()V
    .locals 0

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->notifyListItemCheckedChanged()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->updateTitle()V

    return-void
.end method

.method public onCheckedCountChanged()V
    .locals 0

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onCheckedCountChanged()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->updateTitle()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onCreate(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "filename"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    new-instance v2, Ljava/io/File;

    const-string v3, "filename"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mFile:Ljava/io/File;

    :goto_0
    iget-object v2, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate: file is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mFile:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->updateTitle()V

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onStart()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;-><init>(Lcom/mediatek/datatransfer/AppRestoreActivity;Lcom/mediatek/datatransfer/AppRestoreActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mInitDataTask:Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mInitDataTask:Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f060033

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method protected startRestore()V
    .locals 9

    const/16 v7, 0x10

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v6, "startRestore"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->startService()V

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v5, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v5, v1}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->setRestoreModelList(Ljava/util/ArrayList;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->getSelectedApkNameList()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v5, v7, v3}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->setRestoreItemParam(ILjava/util/ArrayList;)V

    iget-object v5, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    iget-object v6, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->mFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->startRestore(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "first restore app name: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->getAppSnippetByApkName(Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/datatransfer/AppRestoreActivity;->formatProgressDialogMsg(Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->showProgressDialog()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogMax(I)V

    invoke-virtual {p0, v8}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogProgress(I)V

    invoke-virtual {p0, v2}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogMessage(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v5, 0x7d2

    invoke-virtual {p0, v5}, Landroid/app/Activity;->showDialog(I)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->stopService()V

    goto :goto_0
.end method

.method protected updateTitle()V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f060029

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCheckedCount()I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
