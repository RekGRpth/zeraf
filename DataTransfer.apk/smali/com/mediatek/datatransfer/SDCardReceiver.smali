.class public Lcom/mediatek/datatransfer/SDCardReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SDCardReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;
    }
.end annotation


# static fields
.field private static final AT_MOUNT_ACTION:Ljava/lang/String; = "com.mediatek.autotest.mount"

.field private static final AT_UNMOUNT_ACTION:Ljava/lang/String; = "com.mediatek.autotest.unmount"

.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/SDCardReceiver"

.field private static mListenerList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sInstance:Lcom/mediatek/datatransfer/SDCardReceiver;


# instance fields
.field private mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/mediatek/datatransfer/SDCardReceiver;
    .locals 1

    sget-object v0, Lcom/mediatek/datatransfer/SDCardReceiver;->sInstance:Lcom/mediatek/datatransfer/SDCardReceiver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/datatransfer/SDCardReceiver;

    invoke-direct {v0}, Lcom/mediatek/datatransfer/SDCardReceiver;-><init>()V

    sput-object v0, Lcom/mediatek/datatransfer/SDCardReceiver;->sInstance:Lcom/mediatek/datatransfer/SDCardReceiver;

    :cond_0
    sget-object v0, Lcom/mediatek/datatransfer/SDCardReceiver;->sInstance:Lcom/mediatek/datatransfer/SDCardReceiver;

    return-object v0
.end method

.method private startScanSDCard(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/datatransfer/SDCardReceiver;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/SDCardReceiver;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    new-instance v1, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;

    invoke-direct {v1}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;-><init>()V

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->addScanHandler(Lcom/mediatek/datatransfer/utils/BackupsHandler;)Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/SDCardReceiver;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    new-instance v1, Lcom/mediatek/datatransfer/utils/PlutoBackupHandler;

    invoke-direct {v1}, Lcom/mediatek/datatransfer/utils/PlutoBackupHandler;-><init>()V

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->addScanHandler(Lcom/mediatek/datatransfer/utils/BackupsHandler;)Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/SDCardReceiver;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    new-instance v1, Lcom/mediatek/datatransfer/utils/SmartPhoneBackupHandler;

    invoke-direct {v1}, Lcom/mediatek/datatransfer/utils/SmartPhoneBackupHandler;-><init>()V

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->addScanHandler(Lcom/mediatek/datatransfer/utils/BackupsHandler;)Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/SDCardReceiver;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    new-instance v1, Lcom/mediatek/datatransfer/utils/DataTransferBackupHandler;

    invoke-direct {v1}, Lcom/mediatek/datatransfer/utils/DataTransferBackupHandler;-><init>()V

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->addScanHandler(Lcom/mediatek/datatransfer/utils/BackupsHandler;)Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/SDCardReceiver;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->startScan()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v8, p0, Lcom/mediatek/datatransfer/SDCardReceiver;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    if-nez v8, :cond_0

    new-instance v8, Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    const/4 v9, 0x0

    invoke-direct {v8, p1, v9}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v8, p0, Lcom/mediatek/datatransfer/SDCardReceiver;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v8, "DataTransfer/SDCardReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  SDCardReceiver -> onReceive: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "com.mediatek.autotest.unmount"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    :cond_1
    sget-object v8, Lcom/mediatek/datatransfer/SDCardReceiver;->mListenerList:Ljava/util/HashSet;

    if-eqz v8, :cond_2

    sget-object v8, Lcom/mediatek/datatransfer/SDCardReceiver;->mListenerList:Ljava/util/HashSet;

    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    const-string v8, "DataTransfer/SDCardReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  listener : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v8, 0x0

    invoke-interface {v3, v8}, Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;->onSDCardStatusChanged(Z)V

    goto :goto_0

    :cond_2
    iget-object v8, p0, Lcom/mediatek/datatransfer/SDCardReceiver;->mFileScanner:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-virtual {v8}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->quitScan()V

    invoke-static {p1}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/datatransfer/utils/NotifyManager;->clearNotification()V

    const-string v8, "notification"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    const/4 v8, 0x1

    invoke-virtual {v4, v8}, Landroid/app/NotificationManager;->cancel(I)V

    const/4 v8, 0x2

    invoke-virtual {v4, v8}, Landroid/app/NotificationManager;->cancel(I)V

    const/4 v8, 0x3

    invoke-virtual {v4, v8}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    const-string v8, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    const-string v8, "com.mediatek.autotest.mount"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    :cond_5
    sget-object v8, Lcom/mediatek/datatransfer/SDCardReceiver;->mListenerList:Ljava/util/HashSet;

    if-eqz v8, :cond_6

    sget-object v8, Lcom/mediatek/datatransfer/SDCardReceiver;->mListenerList:Ljava/util/HashSet;

    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    const-string v8, "DataTransfer/SDCardReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  listener : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v8, 0x1

    invoke-interface {v3, v8}, Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;->onSDCardStatusChanged(Z)V

    goto :goto_2

    :cond_6
    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/SDCardReceiver;->startScanSDCard(Landroid/content/Context;)V

    goto :goto_1

    :cond_7
    const-string v8, "com.mediatek.SD_SWAP"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    const-string v8, "SD_EXIST"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    sget-object v8, Lcom/mediatek/datatransfer/SDCardReceiver;->mListenerList:Ljava/util/HashSet;

    if-eqz v8, :cond_8

    sget-object v8, Lcom/mediatek/datatransfer/SDCardReceiver;->mListenerList:Ljava/util/HashSet;

    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    const-string v8, "DataTransfer/SDCardReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  listener : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v6}, Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;->onSDCardStatusChanged(Z)V

    goto :goto_3

    :cond_8
    if-eqz v6, :cond_3

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/SDCardReceiver;->startScanSDCard(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_9
    const-string v8, "com.mediatek.datatransfer.newdata_transfer"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "DataTransfer/SDCardReceiver"

    const-string v9, "NotificationReceiver ----->ACTION_NEW_DATA_DETECTED_TRANSFER"

    invoke-static {v8, v9}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.mediatek.datatransfer.newdata"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v8, "notify_type"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v8, "filename"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v8, "filename"

    invoke-virtual {v5, v8, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "notify_type"

    invoke-virtual {v5, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v8, 0x0

    invoke-virtual {p1, v5, v8}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public registerOnSDCardChangedListener(Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;)V
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    sget-object v0, Lcom/mediatek/datatransfer/SDCardReceiver;->mListenerList:Ljava/util/HashSet;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/mediatek/datatransfer/SDCardReceiver;->mListenerList:Ljava/util/HashSet;

    :cond_0
    const-string v0, "DataTransfer/SDCardReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerOnSDCardChangedListener:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/mediatek/datatransfer/SDCardReceiver;->mListenerList:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public unRegisterOnSDCardChangedListener(Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;)V
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    const-string v0, "DataTransfer/SDCardReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unRegisterOnSDCardChangedListener:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/mediatek/datatransfer/SDCardReceiver;->mListenerList:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method
