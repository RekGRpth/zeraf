.class Lcom/mediatek/datatransfer/AbstractBackupActivity$5;
.super Landroid/os/Handler;
.source "AbstractBackupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/AbstractBackupActivity;->initHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$5;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$5;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$5;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$5;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$5;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->pauseBackup()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$5;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    const/16 v1, 0x7d8

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x501
        :pswitch_0
    .end packed-switch
.end method
