.class public Lcom/mediatek/datatransfer/AppSnippet;
.super Ljava/lang/Object;
.source "AppSnippet.java"


# instance fields
.field public mFileName:Ljava/lang/String;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mName:Ljava/lang/CharSequence;

.field private mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppSnippet;->mIcon:Landroid/graphics/drawable/Drawable;

    iput-object p2, p0, Lcom/mediatek/datatransfer/AppSnippet;->mName:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/mediatek/datatransfer/AppSnippet;->mPackageName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public SetFileName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppSnippet;->mFileName:Ljava/lang/String;

    return-void
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppSnippet;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppSnippet;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getName()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppSnippet;->mName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppSnippet;->mPackageName:Ljava/lang/String;

    return-object v0
.end method
