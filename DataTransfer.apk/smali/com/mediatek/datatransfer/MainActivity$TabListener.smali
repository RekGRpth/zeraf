.class Lcom/mediatek/datatransfer/MainActivity$TabListener;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TabListener"
.end annotation


# instance fields
.field private mFragment:Landroid/app/Fragment;

.field private mTag:Ljava/lang/String;

.field final synthetic this$0:Lcom/mediatek/datatransfer/MainActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/MainActivity;Landroid/app/Fragment;Ljava/lang/String;)V
    .locals 0
    .param p2    # Landroid/app/Fragment;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/MainActivity$TabListener;->this$0:Lcom/mediatek/datatransfer/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/mediatek/datatransfer/MainActivity$TabListener;->mFragment:Landroid/app/Fragment;

    iput-object p3, p0, Lcom/mediatek/datatransfer/MainActivity$TabListener;->mTag:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 5
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    iget-object v2, p0, Lcom/mediatek/datatransfer/MainActivity$TabListener;->this$0:Lcom/mediatek/datatransfer/MainActivity;

    invoke-static {v2}, Lcom/mediatek/datatransfer/MainActivity;->access$000(Lcom/mediatek/datatransfer/MainActivity;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/MainActivity$TabListener;->this$0:Lcom/mediatek/datatransfer/MainActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/datatransfer/MainActivity$TabListener;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    const v2, 0x1020002

    iget-object v3, p0, Lcom/mediatek/datatransfer/MainActivity$TabListener;->mFragment:Landroid/app/Fragment;

    iget-object v4, p0, Lcom/mediatek/datatransfer/MainActivity$TabListener;->mTag:Ljava/lang/String;

    invoke-virtual {p2, v2, v3, v4}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    :cond_0
    return-void
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 1
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    iget-object v0, p0, Lcom/mediatek/datatransfer/MainActivity$TabListener;->mFragment:Landroid/app/Fragment;

    invoke-virtual {p2, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    return-void
.end method
