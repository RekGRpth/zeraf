.class public Lcom/mediatek/datatransfer/CheckedListActivity;
.super Landroid/app/ListActivity;
.source "CheckedListActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;
    }
.end annotation


# instance fields
.field private final SAVE_STATE_UNCHECKED_IDS:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field protected mDisabledIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field protected mUnCheckedIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    const-string v0, "CheckListActivity/"

    iput-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->TAG:Ljava/lang/String;

    const-string v0, "CheckedListActivity/unchecked_ids"

    iput-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->SAVE_STATE_UNCHECKED_IDS:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mDisabledIds:Ljava/util/ArrayList;

    return-void
.end method

.method private notifyItemCheckChanged()V
    .locals 3

    iget-object v2, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mListeners:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;

    invoke-interface {v1}, Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;->onCheckedCountChanged()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private restoreInstanceState(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const-string v6, "CheckedListActivity/unchecked_ids"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, v1

    array-length v5, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-wide v3, v0, v2

    iget-object v6, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getCheckedCount()I
    .locals 2

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getUnCheckedCount()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getCount()I
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    :cond_0
    return v1
.end method

.method public getDisabledCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mDisabledIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getEnabledCount()I
    .locals 2

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getDisabledCount()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getItemByPosition(I)Ljava/lang/Object;
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "B&R"

    const-string v2, "CheckListActivity/getItemByPosition: adapter is null, please check"

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public getUnCheckedCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public isAllChecked(Z)Z
    .locals 3
    .param p1    # Z

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getUnCheckedCount()I

    move-result v1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getDisabledCount()I

    move-result v2

    sub-int/2addr v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCheckedCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isItemCheckedById(J)Z
    .locals 3
    .param p1    # J

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public isItemCheckedByPosition(I)Z
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedById(J)Z

    move-result v3

    :cond_0
    return v3
.end method

.method public isItemDisabledById(J)Z
    .locals 3
    .param p1    # J

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mDisabledIds:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mDisabledIds:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public isItemDisabledByPosition(I)Z
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemDisabledById(J)Z

    move-result v3

    :cond_0
    return v3
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const-string v0, "B&R"

    const-string v1, "CheckListActivity/onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/CheckedListActivity;->restoreInstanceState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    const-string v0, "B&R"

    const-string v1, "CheckListActivity/onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    invoke-super/range {p0 .. p5}, Landroid/app/ListActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    invoke-virtual {p0, p3}, Lcom/mediatek/datatransfer/CheckedListActivity;->revertItemCheckedByPosition(I)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "B&R"

    const-string v1, "CheckListActivity/onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v0, v2, [J

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    aput-wide v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v3, "CheckedListActivity/unchecked_ids"

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    return-void
.end method

.method protected registerOnCheckedCountChangedListener(Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;)V
    .locals 1
    .param p1    # Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;

    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mListeners:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public revertItemCheckedByPosition(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedByPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0, p1, v1}, Lcom/mediatek/datatransfer/CheckedListActivity;->setItemCheckedByPosition(IZ)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAllChecked(Z)V
    .locals 8
    .param p1    # Z

    iget-object v6, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v1, :cond_1

    invoke-interface {v0, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v3

    iget-object v6, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_1
    iget-object v6, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mDisabledIds:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_1

    iget-object v6, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mDisabledIds:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->notifyItemCheckChanged()V

    return-void
.end method

.method public setItemCheckedById(JZ)V
    .locals 3
    .param p1    # J
    .param p3    # Z

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_2

    iget-object v1, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->notifyItemCheckChanged()V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setItemCheckedByPosition(IZ)V
    .locals 5
    .param p1    # I
    .param p2    # Z

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v1

    if-eqz p2, :cond_2

    iget-object v3, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->notifyItemCheckChanged()V

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setItemDisabledById(JZ)V
    .locals 2
    .param p1    # J
    .param p3    # Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mDisabledIds:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p3, :cond_2

    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mDisabledIds:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mDisabledIds:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mDisabledIds:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setItemDisabledByPosition(IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v1

    invoke-virtual {p0, v1, v2, p2}, Lcom/mediatek/datatransfer/CheckedListActivity;->setItemDisabledById(JZ)V

    :cond_0
    return-void
.end method

.method protected syncUnCheckedItems()V
    .locals 8

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v6, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    :goto_0
    return-void

    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v1, :cond_2

    invoke-interface {v0, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    iget-object v6, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    iget-object v6, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    iput-object v4, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mUnCheckedIds:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected unRegisterOnCheckedCountChangedListener(Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;)V
    .locals 1
    .param p1    # Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;

    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/CheckedListActivity;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
