.class public Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;
.super Ljava/lang/Object;
.source "ActionBatch.java"

# interfaces
.implements Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ForgetAction"
.end annotation


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field final mHasNewerVersion:Z

.field final mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DictionaryProvider:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;Z)V
    .locals 3
    .param p1    # Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "New TryRemove action : "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iput-boolean p2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mHasNewerVersion:Z

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v6, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    iget-object v3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->TAG:Ljava/lang/String;

    const-string v4, "TryRemoveAction with a null word list!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-array v3, v7, [Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Trying to remove word list : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v3}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v3, v3, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v4, v4, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v0, v3, v4}, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/MetadataDbHelper;->getContentValuesByWordListId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v3, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->TAG:Ljava/lang/String;

    const-string v4, "Trying to update the metadata of a non-existing wordlist. Cancelling."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v3, "status"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-boolean v3, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mHasNewerVersion:Z

    if-eqz v3, :cond_2

    if-eq v7, v1, :cond_2

    sget-object v3, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected status for forgetting a word list info : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", removing URL to prevent re-download"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v3, 0x3

    if-eq v3, v1, :cond_3

    const/4 v3, 0x4

    if-eq v3, v1, :cond_3

    const/4 v3, 0x5

    if-ne v3, v1, :cond_4

    :cond_3
    const-string v3, "url"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "pendingUpdates"

    const-string v4, "id = ? AND version = ?"

    new-array v5, v6, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v6, v6, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v6, v6, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    const-string v3, "pendingUpdates"

    const-string v4, "id = ? AND version = ?"

    new-array v5, v6, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget-object v6, v6, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mId:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;->mWordList:Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;

    iget v6, v6, Lcom/google/android/inputmethod/latin/dictionarypack/metadata/WordListMetadata;->mVersion:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v0, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0
.end method
