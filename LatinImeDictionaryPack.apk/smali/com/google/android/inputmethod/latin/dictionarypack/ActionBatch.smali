.class public Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;
.super Ljava/lang/Object;
.source "ActionBatch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$FinishDeleteAction;,
        Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDeleteAction;,
        Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$ForgetAction;,
        Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$UpdateDataAction;,
        Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$MakeAvailableAction;,
        Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$DisableAction;,
        Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$EnableAction;,
        Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$InstallAfterDownloadAction;,
        Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$StartDownloadAction;,
        Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;
    }
.end annotation


# instance fields
.field private final mActions:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->mActions:Ljava/util/Queue;

    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;)V
    .locals 1
    .param p1    # Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;

    iget-object v0, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->mActions:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public execute(Landroid/content/Context;Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "Executing a batch of actions"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/google/android/inputmethod/latin/dictionarypack/Utils;->l([Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch;->mActions:Ljava/util/Queue;

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;

    :try_start_0
    invoke-interface {v0, p1}, Lcom/google/android/inputmethod/latin/dictionarypack/ActionBatch$Action;->execute(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    if-eqz p2, :cond_0

    invoke-interface {p2, v1}, Lcom/google/android/inputmethod/latin/dictionarypack/ProblemReporter;->report(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_1
    return-void
.end method
