.class public Lcom/mediatek/camera/ext/FeatureExtension;
.super Ljava/lang/Object;
.source "FeatureExtension.java"

# interfaces
.implements Lcom/mediatek/camera/ext/IFeatureExtension;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkMMSVideoCodec(ILandroid/media/CamcorderProfile;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/media/CamcorderProfile;

    return-void
.end method

.method public isDelayRestartPreview()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isPrioritizePreviewSize()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public updateSceneStrings(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "normal"

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    :cond_1
    return-void
.end method

.method public updateWBStrings([Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # [Ljava/lang/CharSequence;

    return-void
.end method
