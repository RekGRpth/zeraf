.class public Lcom/mediatek/camera/AndroidCamera;
.super Ljava/lang/Object;
.source "AndroidCamera.java"

# interfaces
.implements Lcom/mediatek/camera/ICamera;


# static fields
.field private static final TAG:Ljava/lang/String; = "AndroidCamera"


# instance fields
.field protected mCamera:Landroid/hardware/Camera;


# direct methods
.method public constructor <init>(Landroid/hardware/Camera;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/android/camera/Util;->assertError(Z)V

    iput-object p1, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addCallbackBuffer([B)V
    .locals 1
    .param p1    # [B

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    return-void
.end method

.method public addRawImageCallbackBuffer([B)V
    .locals 1
    .param p1    # [B

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->addRawImageCallbackBuffer([B)V

    return-void
.end method

.method public autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$AutoFocusCallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    return-void
.end method

.method public cancelAutoFocus()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    return-void
.end method

.method public cancelContinuousShot()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelContinuousShot()V

    return-void
.end method

.method public cancelSDPreview()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelSDPreview()V

    return-void
.end method

.method public getInstance()Landroid/hardware/Camera;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    return-object v0
.end method

.method public getParameters()Landroid/hardware/Camera$Parameters;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    return-object v0
.end method

.method public lock()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->lock()V

    return-void
.end method

.method public reconnect()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->reconnect()V

    return-void
.end method

.method public release()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    return-void
.end method

.method public setASDCallback(Landroid/hardware/Camera$ASDCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$ASDCallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setASDCallback(Landroid/hardware/Camera$ASDCallback;)V

    return-void
.end method

.method public setAUTORAMACallback(Landroid/hardware/Camera$AUTORAMACallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$AUTORAMACallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setAUTORAMACallback(Landroid/hardware/Camera$AUTORAMACallback;)V

    return-void
.end method

.method public setAUTORAMAMVCallback(Landroid/hardware/Camera$AUTORAMAMVCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$AUTORAMAMVCallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setAUTORAMAMVCallback(Landroid/hardware/Camera$AUTORAMAMVCallback;)V

    return-void
.end method

.method public setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$AutoFocusMoveCallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V

    return-void
.end method

.method public setCSDoneCallback(Landroid/hardware/Camera$ContinuousShotDone;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$ContinuousShotDone;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setCSDoneCallback(Landroid/hardware/Camera$ContinuousShotDone;)V

    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    return-void
.end method

.method public setContinuousShotSpeed(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setContinuousShotSpeed(I)V

    return-void
.end method

.method public setDisplayOrientation(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    return-void
.end method

.method public setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$ErrorCallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    return-void
.end method

.method public setFaceDetectionListener(Landroid/hardware/Camera$FaceDetectionListener;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$FaceDetectionListener;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setFaceDetectionListener(Landroid/hardware/Camera$FaceDetectionListener;)V

    return-void
.end method

.method public setMAVCallback(Landroid/hardware/Camera$MAVCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$MAVCallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setMAVCallback(Landroid/hardware/Camera$MAVCallback;)V

    return-void
.end method

.method public setParameters(Landroid/hardware/Camera$Parameters;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$Parameters;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    return-void
.end method

.method public setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$PreviewCallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    return-void
.end method

.method public setPreviewDoneCallback(Landroid/hardware/Camera$ZSDPreviewDone;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$ZSDPreviewDone;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewDoneCallback(Landroid/hardware/Camera$ZSDPreviewDone;)V

    return-void
.end method

.method public setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    .locals 1
    .param p1    # Landroid/graphics/SurfaceTexture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    return-void
.end method

.method public setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$SmileCallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V

    return-void
.end method

.method public setZoomChangeListener(Landroid/hardware/Camera$OnZoomChangeListener;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$OnZoomChangeListener;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setZoomChangeListener(Landroid/hardware/Camera$OnZoomChangeListener;)V

    return-void
.end method

.method public startAUTORAMA(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->startAUTORAMA(I)V

    return-void
.end method

.method public startFaceDetection()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startFaceDetection()V

    return-void
.end method

.method public startMAV(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->startMAV(I)V

    return-void
.end method

.method public startPreview()V
    .locals 2

    const-string v0, "AndroidCamera"

    const-string v1, "startPreview()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    return-void
.end method

.method public startSDPreview()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startSDPreview()V

    return-void
.end method

.method public startSmoothZoom(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->startSmoothZoom(I)V

    return-void
.end method

.method public stopAUTORAMA(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->stopAUTORAMA(I)V

    return-void
.end method

.method public stopFaceDetection()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopFaceDetection()V

    return-void
.end method

.method public stopMAV(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->stopMAV(I)V

    return-void
.end method

.method public stopPreview()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    return-void
.end method

.method public takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$ShutterCallback;
    .param p2    # Landroid/hardware/Camera$PictureCallback;
    .param p3    # Landroid/hardware/Camera$PictureCallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1, p2, p3}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    return-void
.end method

.method public takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$ShutterCallback;
    .param p2    # Landroid/hardware/Camera$PictureCallback;
    .param p3    # Landroid/hardware/Camera$PictureCallback;
    .param p4    # Landroid/hardware/Camera$PictureCallback;

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    return-void
.end method

.method public unlock()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/camera/AndroidCamera;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->unlock()V

    return-void
.end method
