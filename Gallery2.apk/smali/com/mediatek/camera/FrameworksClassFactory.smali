.class public Lcom/mediatek/camera/FrameworksClassFactory;
.super Ljava/lang/Object;
.source "FrameworksClassFactory.java"


# static fields
.field private static final LOG:Z = true

.field private static final MOCK_CAMERA:Z

.field private static final TAG:Ljava/lang/String; = "FrameworksClassFactory"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/camera/FrameworksClassFactory;->MOCK_CAMERA:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V
    .locals 1
    .param p0    # I
    .param p1    # Landroid/hardware/Camera$CameraInfo;

    sget-boolean v0, Lcom/mediatek/camera/FrameworksClassFactory;->MOCK_CAMERA:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/mediatek/mock/hardware/MockCamera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    goto :goto_0
.end method

.method public static getMediaRecorder()Landroid/media/MediaRecorder;
    .locals 1

    sget-boolean v0, Lcom/mediatek/camera/FrameworksClassFactory;->MOCK_CAMERA:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mediatek/mock/media/MockMediaRecorder;

    invoke-direct {v0}, Lcom/mediatek/mock/media/MockMediaRecorder;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/media/MediaRecorder;

    invoke-direct {v0}, Landroid/media/MediaRecorder;-><init>()V

    goto :goto_0
.end method

.method public static getMtkCamcorderProfile(II)Landroid/media/CamcorderProfile;
    .locals 1
    .param p0    # I
    .param p1    # I

    sget-boolean v0, Lcom/mediatek/camera/FrameworksClassFactory;->MOCK_CAMERA:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/mediatek/mock/media/MockCamcorderProfileHelper;->getMtkCamcorderProfile(II)Landroid/media/CamcorderProfile;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/mediatek/camcorder/CamcorderProfileEx;->getProfile(II)Landroid/media/CamcorderProfile;

    move-result-object v0

    goto :goto_0
.end method

.method public static getNumberOfCameras()I
    .locals 1

    sget-boolean v0, Lcom/mediatek/camera/FrameworksClassFactory;->MOCK_CAMERA:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/mock/hardware/MockCamera;->getNumberOfCameras()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    goto :goto_0
.end method

.method public static isMockCamera()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/camera/FrameworksClassFactory;->MOCK_CAMERA:Z

    return v0
.end method

.method public static openCamera(I)Lcom/mediatek/camera/ICamera;
    .locals 3
    .param p0    # I

    sget-boolean v1, Lcom/mediatek/camera/FrameworksClassFactory;->MOCK_CAMERA:Z

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/mediatek/mock/hardware/MockCamera;->open(I)Lcom/mediatek/mock/hardware/MockCamera;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "FrameworksClassFactory"

    const-string v2, "openCamera:got null hardware camera!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/mediatek/camera/AndroidCamera;

    invoke-direct {v1, v0}, Lcom/mediatek/camera/AndroidCamera;-><init>(Landroid/hardware/Camera;)V

    goto :goto_0
.end method
