.class Lcom/mediatek/mock/hardware/MockCamera$EventHandler;
.super Landroid/os/Handler;
.source "MockCamera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mock/hardware/MockCamera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mCamera:Lcom/mediatek/mock/hardware/MockCamera;

.field final synthetic this$0:Lcom/mediatek/mock/hardware/MockCamera;


# direct methods
.method public constructor <init>(Lcom/mediatek/mock/hardware/MockCamera;Lcom/mediatek/mock/hardware/MockCamera;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Lcom/mediatek/mock/hardware/MockCamera;
    .param p3    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->mCamera:Lcom/mediatek/mock/hardware/MockCamera;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 24
    .param p1    # Landroid/os/Message;

    const-string v20, "MockCamera"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "handleMessage: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v20, v0

    sparse-switch v20, :sswitch_data_0

    const-string v20, "MockCamera"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Unknown message type "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$000(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$ShutterCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$000(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$ShutterCallback;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/hardware/Camera$ShutterCallback;->onShutter()V

    goto :goto_0

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$100(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$100(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, [B

    check-cast v20, [B

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Landroid/hardware/Camera$PictureCallback;->onPictureTaken([BLandroid/hardware/Camera;)V

    goto :goto_0

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$200(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$200(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, [B

    check-cast v20, [B

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Landroid/hardware/Camera$PictureCallback;->onPictureTaken([BLandroid/hardware/Camera;)V

    const-string v20, "evbracketshot"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/mediatek/mock/hardware/MockCamera;->access$300(Lcom/mediatek/mock/hardware/MockCamera;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$200(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, [B

    check-cast v20, [B

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Landroid/hardware/Camera$PictureCallback;->onPictureTaken([BLandroid/hardware/Camera;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$200(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, [B

    check-cast v20, [B

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Landroid/hardware/Camera$PictureCallback;->onPictureTaken([BLandroid/hardware/Camera;)V

    goto/16 :goto_0

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$400(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$PreviewCallback;

    move-result-object v14

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$500(Lcom/mediatek/mock/hardware/MockCamera;)Z

    move-result v20

    if-eqz v20, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mock/hardware/MockCamera;->access$402(Lcom/mediatek/mock/hardware/MockCamera;Landroid/hardware/Camera$PreviewCallback;)Landroid/hardware/Camera$PreviewCallback;

    :cond_1
    :goto_1
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, [B

    check-cast v20, [B

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v14, v0, v1}, Landroid/hardware/Camera$PreviewCallback;->onPreviewFrame([BLandroid/hardware/Camera;)V

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$600(Lcom/mediatek/mock/hardware/MockCamera;)Z

    move-result v20

    if-nez v20, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    const/16 v22, 0x0

    invoke-static/range {v20 .. v22}, Lcom/mediatek/mock/hardware/MockCamera;->access$700(Lcom/mediatek/mock/hardware/MockCamera;ZZ)V

    goto :goto_1

    :sswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$800(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$800(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, [B

    check-cast v20, [B

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Landroid/hardware/Camera$PictureCallback;->onPictureTaken([BLandroid/hardware/Camera;)V

    goto/16 :goto_0

    :sswitch_5
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$900(Lcom/mediatek/mock/hardware/MockCamera;)Ljava/lang/Object;

    move-result-object v21

    monitor-enter v21

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1000(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AutoFocusCallback;

    move-result-object v6

    monitor-exit v21
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v20, v0

    if-nez v20, :cond_3

    const/4 v15, 0x0

    :goto_2
    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-interface {v6, v15, v0}, Landroid/hardware/Camera$AutoFocusCallback;->onAutoFocus(ZLandroid/hardware/Camera;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v20

    :try_start_1
    monitor-exit v21
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v20

    :cond_3
    const/4 v15, 0x1

    goto :goto_2

    :sswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1100(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$OnZoomChangeListener;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1100(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$OnZoomChangeListener;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg2:I

    move/from16 v20, v0

    if-eqz v20, :cond_4

    const/16 v20, 0x1

    :goto_3
    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v20

    move-object/from16 v3, v23

    invoke-interface {v0, v1, v2, v3}, Landroid/hardware/Camera$OnZoomChangeListener;->onZoomChange(IZLandroid/hardware/Camera;)V

    goto/16 :goto_0

    :cond_4
    const/16 v20, 0x0

    goto :goto_3

    :sswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1200(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$FaceDetectionListener;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1200(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$FaceDetectionListener;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, [Landroid/hardware/Camera$Face;

    check-cast v20, [Landroid/hardware/Camera$Face;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Landroid/hardware/Camera$FaceDetectionListener;->onFaceDetection([Landroid/hardware/Camera$Face;Landroid/hardware/Camera;)V

    goto/16 :goto_0

    :sswitch_8
    const-string v20, "MockCamera"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Error "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1300(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$ErrorCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1300(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$ErrorCallback;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    const/16 v22, 0x0

    invoke-interface/range {v20 .. v22}, Landroid/hardware/Camera$ErrorCallback;->onError(ILandroid/hardware/Camera;)V

    goto/16 :goto_0

    :sswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1400(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AutoFocusMoveCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1400(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AutoFocusMoveCallback;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v20, v0

    if-nez v20, :cond_5

    const/16 v20, 0x0

    :goto_4
    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v20

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Landroid/hardware/Camera$AutoFocusMoveCallback;->onAutoFocusMoving(ZLandroid/hardware/Camera;)V

    goto/16 :goto_0

    :cond_5
    const/16 v20, 0x1

    goto :goto_4

    :sswitch_a
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v20, v0

    packed-switch v20, :pswitch_data_0

    :pswitch_0
    const-string v20, "MockCamera"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Unknown MTK-extended notify message type "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1500(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$SmileCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1500(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$SmileCallback;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/hardware/Camera$SmileCallback;->onSmile()V

    goto/16 :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1600(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$ASDCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1600(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$ASDCallback;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg2:I

    move/from16 v21, v0

    invoke-interface/range {v20 .. v21}, Landroid/hardware/Camera$ASDCallback;->onDetecte(I)V

    goto/16 :goto_0

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1700(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$MAVCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1700(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$MAVCallback;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/hardware/Camera$MAVCallback;->onFrame()V

    goto/16 :goto_0

    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1800(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$ContinuousShotDone;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1800(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$ContinuousShotDone;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg2:I

    move/from16 v21, v0

    invoke-interface/range {v20 .. v21}, Landroid/hardware/Camera$ContinuousShotDone;->onConinuousShotDone(I)V

    goto/16 :goto_0

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1900(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$ZSDPreviewDone;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$1900(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$ZSDPreviewDone;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/hardware/Camera$ZSDPreviewDone;->onPreviewDone()V

    goto/16 :goto_0

    :pswitch_6
    const-string v20, "MockCamera"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "preview frame arrived, sFrameReporter = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/mediatek/mock/hardware/MockCamera;->access$2000()Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$2100(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/graphics/SurfaceTexture;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v20, "mEventHandler"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v11

    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$2100(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/graphics/SurfaceTexture;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/os/Handler;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    const/16 v20, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V
    :try_end_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v10

    invoke-virtual {v10}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0

    :sswitch_b
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v20, v0

    packed-switch v20, :pswitch_data_1

    const-string v20, "MockCamera"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Unknown MTK-extended data message type "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_7
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, [B

    move-object/from16 v5, v20

    check-cast v5, [B

    const-string v20, "MockCamera"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "MTK_CAMERA_MSG_EXT_DATA_AUTORAMA: byteArray.length = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    array-length v0, v5

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v13

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/nio/IntBuffer;->get(I)I

    move-result v20

    if-nez v20, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$2200(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AUTORAMAMVCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/nio/IntBuffer;->get(I)I

    move-result v17

    const/16 v20, 0x2

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/nio/IntBuffer;->get(I)I

    move-result v19

    const/16 v20, 0x3

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/nio/IntBuffer;->get(I)I

    move-result v8

    const v20, 0xffff

    and-int v20, v20, v17

    shl-int/lit8 v20, v20, 0x10

    const v21, 0xffff

    and-int v21, v21, v19

    add-int v18, v20, v21

    const-string v20, "MockCamera"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "call mAUTORAMAMVCallback: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/mediatek/mock/hardware/MockCamera;->access$2300(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AUTORAMACallback;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " dir:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " x:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " y:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " xy:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$2200(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AUTORAMAMVCallback;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-interface {v0, v1, v8}, Landroid/hardware/Camera$AUTORAMAMVCallback;->onFrame(II)V

    goto/16 :goto_0

    :cond_6
    const-string v20, "MockCamera"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "call mAUTORAMACallback: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/mediatek/mock/hardware/MockCamera;->access$2300(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AUTORAMACallback;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$2300(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AUTORAMACallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$2300(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AUTORAMACallback;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/hardware/Camera$AUTORAMACallback;->onCapture()V

    goto/16 :goto_0

    :pswitch_8
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, [B

    move-object/from16 v5, v20

    check-cast v5, [B

    const-string v20, "MockCamera"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "MTK_CAMERA_MSG_EXT_DATA_AF: byteArray.length = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    array-length v0, v5

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$2400(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AFDataCallback;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mock/hardware/MockCamera$EventHandler;->this$0:Lcom/mediatek/mock/hardware/MockCamera;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/mock/hardware/MockCamera;->access$2400(Lcom/mediatek/mock/hardware/MockCamera;)Landroid/hardware/Camera$AFDataCallback;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, [B

    check-cast v20, [B

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v4, v0, v1}, Landroid/hardware/Camera$AFDataCallback;->onAFData([BLandroid/hardware/Camera;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_b
        0x1 -> :sswitch_8
        0x2 -> :sswitch_0
        0x4 -> :sswitch_5
        0x8 -> :sswitch_6
        0x10 -> :sswitch_3
        0x40 -> :sswitch_4
        0x80 -> :sswitch_1
        0x100 -> :sswitch_2
        0x400 -> :sswitch_7
        0x800 -> :sswitch_9
        0x40000000 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
