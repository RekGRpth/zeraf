.class public Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;
.super Ljava/lang/Object;
.source "PictureQualityTool.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/pq/PictureQualityTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Apply"
.end annotation


# instance fields
.field private mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

.field final synthetic this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;


# direct methods
.method public constructor <init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    invoke-virtual {v2}, Lcom/mediatek/gallery3d/pq/ImageDecoder;->apply()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    invoke-static {v2, v0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->access$202(Lcom/mediatek/gallery3d/pq/PictureQualityTool;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    :goto_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;->this$0:Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v2, v2, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void

    :cond_1
    const-string v2, "Gallery2/PictureQualityTool"

    const-string v3, "bitmap == null##########################"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDecoder(Lcom/mediatek/gallery3d/pq/ImageDecoder;)V
    .locals 0
    .param p1    # Lcom/mediatek/gallery3d/pq/ImageDecoder;

    iput-object p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    return-void
.end method
