.class public Lcom/mediatek/gallery3d/pq/PictureQualityTool;
.super Landroid/app/Activity;
.source "PictureQualityTool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarChangeLisenter;,
        Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarTouchVisibleLisenter;,
        Lcom/mediatek/gallery3d/pq/PictureQualityTool$VisibleLisenter;,
        Lcom/mediatek/gallery3d/pq/PictureQualityTool$SettingXYAxisLisenter;,
        Lcom/mediatek/gallery3d/pq/PictureQualityTool$MyGestureListener;,
        Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;,
        Lcom/mediatek/gallery3d/pq/PictureQualityTool$DecodeImage;
    }
.end annotation


# static fields
.field public static final ACTION_PQ:Ljava/lang/String; = "com.android.camera.action.PQ"

.field public static final BACKUP_PIXEL_COUNT:I = 0x75300

.field public static final SUCCESS_LOAD_BITMAP:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Gallery2/PictureQualityTool"

.field private static isEnterADVmode:Z


# instance fields
.field public PQADVMode:Landroid/view/MenuItem;

.field private PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

.field public PQSwitchmemu:Landroid/view/MenuItem;

.field public WindowsHeight:I

.field public WindowsWidth:I

.field public hueSeekBarTouchBase:Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;

.field lastX:I

.field lastY:I

.field public mActionBar:Landroid/app/ActionBar;

.field private mApply:Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mDecodeImage:Lcom/mediatek/gallery3d/pq/PictureQualityTool$DecodeImage;

.field public mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mGlobalSatOption:I

.field private mGlobleSatRange:I

.field private mGrassSatOption:I

.field private mGrassSatRange:I

.field private mGrassToneOption:I

.field private mGrassToneRange:I

.field public mHandler:Landroid/os/Handler;

.field private mHudOptionADV:I

.field private mHudRangeADV:I

.field private mImageView:Landroid/widget/ImageView;

.field private mImageViewADV:Landroid/widget/ImageView;

.field private mImageViewTouchBase:Lcom/mediatek/gallery3d/pq/ImageViewTouchBase;

.field mMetric:Landroid/graphics/Matrix;

.field public mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

.field public mPQMineType:Ljava/lang/String;

.field public mPqUri:Ljava/lang/String;

.field private mSatOptionADV:I

.field private mSatRangeADV:I

.field public mSeekBarChangeLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarChangeLisenter;

.field private mSeekBarGlobal:Landroid/widget/SeekBar;

.field private mSeekBarGrassSat:Landroid/widget/SeekBar;

.field private mSeekBarGrassTone:Landroid/widget/SeekBar;

.field private mSeekBarSharpness:Landroid/widget/SeekBar;

.field private mSeekBarSkinSat:Landroid/widget/SeekBar;

.field private mSeekBarSkinTone:Landroid/widget/SeekBar;

.field private mSeekBarSkySat:Landroid/widget/SeekBar;

.field private mSeekBarSkyTone:Landroid/widget/SeekBar;

.field public mSettingXYAxisLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$SettingXYAxisLisenter;

.field private mSharpnessOption:I

.field private mSharpnessRange:I

.field private mSkinSatOption:I

.field private mSkinSatRange:I

.field private mSkinToneOption:I

.field private mSkinToneRange:I

.field private mSkySatOption:I

.field private mSkySatRange:I

.field private mSkyToneOption:I

.field private mSkyToneRange:I

.field private mTextViewGlobal:Landroid/widget/TextView;

.field private mTextViewGlobalSatRange:Landroid/widget/TextView;

.field private mTextViewGrassSat:Landroid/widget/TextView;

.field private mTextViewGrassSatRange:Landroid/widget/TextView;

.field private mTextViewGrassTone:Landroid/widget/TextView;

.field private mTextViewGrassToneMin:Landroid/widget/TextView;

.field private mTextViewGrassToneRange:Landroid/widget/TextView;

.field private mTextViewSharpness:Landroid/widget/TextView;

.field private mTextViewSharpnessRange:Landroid/widget/TextView;

.field private mTextViewSkinSat:Landroid/widget/TextView;

.field private mTextViewSkinSatRange:Landroid/widget/TextView;

.field private mTextViewSkinTone:Landroid/widget/TextView;

.field private mTextViewSkinToneMin:Landroid/widget/TextView;

.field private mTextViewSkinToneRange:Landroid/widget/TextView;

.field private mTextViewSkySat:Landroid/widget/TextView;

.field private mTextViewSkySatRange:Landroid/widget/TextView;

.field private mTextViewSkyTone:Landroid/widget/TextView;

.field private mTextViewSkyToneMin:Landroid/widget/TextView;

.field private mTextViewSkyToneRange:Landroid/widget/TextView;

.field mTileImageDecoder:Lcom/mediatek/gallery3d/pq/TileImageDecoder;

.field public mView:Landroid/view/View;

.field public mVisibleLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$VisibleLisenter;

.field public mbitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

.field public options:Landroid/graphics/BitmapFactory$Options;

.field private origionGrassToneIndex:I

.field private origionGrassToneSIndex:I

.field private origionSatAdjIndex:I

.field private origionSharpnessIndex:I

.field private origionSkinToneIndex:I

.field private origionSkinToneSIndex:I

.field private origionSkyToneIndex:I

.field private origionSkyToneSIndex:I

.field public saturationSeekBarTouchBase:Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;

.field private seekBar_hue:Landroid/widget/SeekBar;

.field public seekBar_saturation:Landroid/widget/SeekBar;

.field public sign:Z

.field private textView_hue:Landroid/widget/TextView;

.field private textView_hue_left:Landroid/widget/TextView;

.field private textView_hue_left_temple:Landroid/widget/TextView;

.field private textView_hue_progress:Landroid/widget/TextView;

.field private textView_hue_progress_temple:Landroid/widget/TextView;

.field private textView_hue_temple:Landroid/widget/TextView;

.field private textView_saturation:Landroid/widget/TextView;

.field private textView_saturation_left:Landroid/widget/TextView;

.field private textView_saturation_left_temple:Landroid/widget/TextView;

.field private textView_saturation_progress:Landroid/widget/TextView;

.field private textView_saturation_progress_temple:Landroid/widget/TextView;

.field private textView_saturation_temple:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mMetric:Landroid/graphics/Matrix;

    iput-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mbitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    iput-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageViewTouchBase:Lcom/mediatek/gallery3d/pq/ImageViewTouchBase;

    iput-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarChangeLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarChangeLisenter;

    iput-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    iput-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->sign:Z

    iput-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTileImageDecoder:Lcom/mediatek/gallery3d/pq/TileImageDecoder;

    new-instance v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$1;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->isEnterADVmode:Z

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_left_temple:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_temple:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_progress_temple:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSharpness:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessOption:I

    return v0
.end method

.method static synthetic access$1402(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessOption:I

    return p1
.end method

.method static synthetic access$1500(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessRange:I

    return v0
.end method

.method static synthetic access$1600(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSharpness:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGlobal:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGlobalSatOption:I

    return v0
.end method

.method static synthetic access$1802(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGlobalSatOption:I

    return p1
.end method

.method static synthetic access$1900(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGlobleSatRange:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGlobal:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/gallery3d/pq/PictureQualityTool;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinTone:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneOption:I

    return v0
.end method

.method static synthetic access$2202(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneOption:I

    return p1
.end method

.method static synthetic access$2300(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    return v0
.end method

.method static synthetic access$2400(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinTone:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassTone:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneOption:I

    return v0
.end method

.method static synthetic access$2602(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneOption:I

    return p1
.end method

.method static synthetic access$2700(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    return v0
.end method

.method static synthetic access$2800(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassTone:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkyTone:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageViewADV:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneOption:I

    return v0
.end method

.method static synthetic access$3002(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneOption:I

    return p1
.end method

.method static synthetic access$3100(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    return v0
.end method

.method static synthetic access$3200(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyTone:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinSat:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinSatOption:I

    return v0
.end method

.method static synthetic access$3402(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinSatOption:I

    return p1
.end method

.method static synthetic access$3500(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinSatRange:I

    return v0
.end method

.method static synthetic access$3600(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinSat:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassSat:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassSatOption:I

    return v0
.end method

.method static synthetic access$3802(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassSatOption:I

    return p1
.end method

.method static synthetic access$3900(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassSatRange:I

    return v0
.end method

.method static synthetic access$4000(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassSat:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkySat:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkySatOption:I

    return v0
.end method

.method static synthetic access$4202(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkySatOption:I

    return p1
.end method

.method static synthetic access$4300(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkySatRange:I

    return v0
.end method

.method static synthetic access$4400(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkySat:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHudOptionADV:I

    return v0
.end method

.method static synthetic access$4502(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHudOptionADV:I

    return p1
.end method

.method static synthetic access$4600(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHudRangeADV:I

    return v0
.end method

.method static synthetic access$4700(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_progress:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSatOptionADV:I

    return v0
.end method

.method static synthetic access$4802(Lcom/mediatek/gallery3d/pq/PictureQualityTool;I)I
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSatOptionADV:I

    return p1
.end method

.method static synthetic access$4900(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSatRangeADV:I

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Lcom/mediatek/gallery3d/pq/PictureQualityJni;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_progress:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_hue:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_left_temple:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_temple:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/pq/PictureQualityTool;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_progress_temple:Landroid/widget/TextView;

    return-object v0
.end method

.method private addSeekBarListener()V
    .locals 2

    new-instance v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarChangeLisenter;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarChangeLisenter;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarChangeLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarChangeLisenter;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarChangeLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarChangeLisenter;

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;->setLisenter(Lcom/mediatek/gallery3d/pq/SeekBarChangeInterface;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSharpness:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGlobal:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinTone:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassTone:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkyTone:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinSat:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassSat:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkySat:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method private addSeekBarListenerADVMode()V
    .locals 7

    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/mediatek/gallery3d/pq/PictureQualityTool$MyGestureListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$MyGestureListener;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;Lcom/mediatek/gallery3d/pq/PictureQualityTool$1;)V

    invoke-direct {v0, p0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v0, Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsWidth:I

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsHeight:I

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_left:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_progress:Landroid/widget/TextView;

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;-><init>(IILandroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->hueSeekBarTouchBase:Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_hue:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->hueSeekBarTouchBase:Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v0, Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsWidth:I

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsHeight:I

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_left:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_progress:Landroid/widget/TextView;

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;-><init>(IILandroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->saturationSeekBarTouchBase:Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_saturation:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->saturationSeekBarTouchBase:Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v6, Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarTouchVisibleLisenter;

    invoke-direct {v6, p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarTouchVisibleLisenter;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->hueSeekBarTouchBase:Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_hue:Landroid/widget/SeekBar;

    invoke-virtual {v0, v6, v1}, Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;->setLisenter(Lcom/mediatek/gallery3d/pq/SetViewVisible;Landroid/widget/SeekBar;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->saturationSeekBarTouchBase:Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_saturation:Landroid/widget/SeekBar;

    invoke-virtual {v0, v6, v1}, Lcom/mediatek/gallery3d/pq/SeekBarTouchBase;->setLisenter(Lcom/mediatek/gallery3d/pq/SetViewVisible;Landroid/widget/SeekBar;)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->addSeekBarListenerADVmode()V

    return-void
.end method

.method private addSeekBarListenerADVmode()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_hue:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_saturation:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method private closeStream(Ljava/io/Closeable;)V
    .locals 1
    .param p1    # Ljava/io/Closeable;

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private enterADVmode()V
    .locals 1

    const v0, 0x7f040044

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->initPQToolViewADVMode()V

    return-void
.end method

.method private enterBasemode()V
    .locals 1

    const v0, 0x7f040043

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->initPQToolView()V

    return-void
.end method

.method private getOriginIndex()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSharpAdjIndex()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSharpnessIndex:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneHIndex()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkyToneIndex:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneHIndex()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionGrassToneIndex:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneHIndex()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkinToneIndex:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSatAdjIndex()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSatAdjIndex:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneSIndex()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkinToneSIndex:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneSIndex()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionGrassToneSIndex:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneSIndex()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkyToneSIndex:I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "origionSharpnessIndex=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSharpnessIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "origionSkyToneIndex=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkyToneIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "origionGrassToneIndex=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionGrassToneIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "origionSkinToneIndex=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkinToneIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "origionSatAdjIndex=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSatAdjIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "origionSkinToneSIndex=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkinToneSIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "origionGrassToneSIndex=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionGrassToneSIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "origionSkyToneSIndex=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkyToneSIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private getViewById()V
    .locals 1

    const v0, 0x7f0b0070

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyToneMin:Landroid/widget/TextView;

    const v0, 0x7f0b00de

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinToneMin:Landroid/widget/TextView;

    const v0, 0x7f0b00e5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassToneMin:Landroid/widget/TextView;

    const v0, 0x7f0b00d4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageView:Landroid/widget/ImageView;

    const v0, 0x7f0b00d8

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSharpnessRange:Landroid/widget/TextView;

    const v0, 0x7f0b00dc

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGlobalSatRange:Landroid/widget/TextView;

    const v0, 0x7f0b00ee

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyToneRange:Landroid/widget/TextView;

    const v0, 0x7f0b00e0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinToneRange:Landroid/widget/TextView;

    const v0, 0x7f0b00e7

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassToneRange:Landroid/widget/TextView;

    const v0, 0x7f0b00e4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinSatRange:Landroid/widget/TextView;

    const v0, 0x7f0b00eb

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassSatRange:Landroid/widget/TextView;

    const v0, 0x7f0b00f2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkySatRange:Landroid/widget/TextView;

    const v0, 0x7f0b00d7

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSharpness:Landroid/widget/TextView;

    const v0, 0x7f0b00d6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSharpness:Landroid/widget/SeekBar;

    const v0, 0x7f0b00ef

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyTone:Landroid/widget/TextView;

    const v0, 0x7f0b00ed

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkyTone:Landroid/widget/SeekBar;

    const v0, 0x7f0b00e8

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassTone:Landroid/widget/TextView;

    const v0, 0x7f0b00e6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassTone:Landroid/widget/SeekBar;

    const v0, 0x7f0b00df

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinTone:Landroid/widget/TextView;

    const v0, 0x7f0b00dd

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinTone:Landroid/widget/SeekBar;

    const v0, 0x7f0b00db

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGlobal:Landroid/widget/TextView;

    const v0, 0x7f0b00d9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGlobal:Landroid/widget/SeekBar;

    const v0, 0x7f0b00e3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinSat:Landroid/widget/TextView;

    const v0, 0x7f0b00e1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinSat:Landroid/widget/SeekBar;

    const v0, 0x7f0b00ec

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassSat:Landroid/widget/TextView;

    const v0, 0x7f0b00ea

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassSat:Landroid/widget/SeekBar;

    const v0, 0x7f0b00f3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkySat:Landroid/widget/TextView;

    const v0, 0x7f0b00f1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkySat:Landroid/widget/SeekBar;

    return-void
.end method

.method private getViewByIdADVMode()V
    .locals 11

    const v2, 0x7f04004a

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/16 v10, 0x8

    const v0, 0x7f0b00f4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageViewADV:Landroid/widget/ImageView;

    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    invoke-virtual {v7, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04004b

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04004c

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Lcom/mediatek/gallery3d/pq/ImageViewTouchBase;

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mMetric:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageViewADV:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mbitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    iget v6, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/gallery3d/pq/ImageViewTouchBase;-><init>(Landroid/content/Context;Landroid/graphics/Matrix;Landroid/widget/ImageView;Landroid/graphics/Bitmap;Landroid/graphics/BitmapRegionDecoder;I)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageViewTouchBase:Lcom/mediatek/gallery3d/pq/ImageViewTouchBase;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageViewADV:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageViewTouchBase:Lcom/mediatek/gallery3d/pq/ImageViewTouchBase;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0b0102

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_hue:Landroid/widget/SeekBar;

    const v0, 0x7f0b010a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_left:Landroid/widget/TextView;

    const v0, 0x7f0b010b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue:Landroid/widget/TextView;

    const v0, 0x7f0b010c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_progress:Landroid/widget/TextView;

    const v0, 0x7f0b0103

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_left_temple:Landroid/widget/TextView;

    const v0, 0x7f0b0104

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_temple:Landroid/widget/TextView;

    const v0, 0x7f0b0105

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_progress_temple:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_hue:Landroid/widget/SeekBar;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_left:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_progress:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_left_temple:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_temple:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_progress_temple:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    const v0, 0x7f0b0106

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_saturation:Landroid/widget/SeekBar;

    const v0, 0x7f0b010d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_left:Landroid/widget/TextView;

    const v0, 0x7f0b010e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation:Landroid/widget/TextView;

    const v0, 0x7f0b010f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_progress:Landroid/widget/TextView;

    const v0, 0x7f0b0107

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_left_temple:Landroid/widget/TextView;

    const v0, 0x7f0b0108

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_temple:Landroid/widget/TextView;

    const v0, 0x7f0b0109

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_progress_temple:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_saturation:Landroid/widget/SeekBar;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_left:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_progress:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_left_temple:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_temple:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_progress_temple:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v10}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setVisibilityADM(Landroid/view/View;I)V

    new-instance v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$VisibleLisenter;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$VisibleLisenter;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mVisibleLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$VisibleLisenter;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageViewTouchBase:Lcom/mediatek/gallery3d/pq/ImageViewTouchBase;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mVisibleLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$VisibleLisenter;

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/pq/ImageViewTouchBase;->setVisibleLisenter(Lcom/mediatek/gallery3d/pq/SetViewVisible;)V

    new-instance v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool$SettingXYAxisLisenter;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$SettingXYAxisLisenter;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSettingXYAxisLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$SettingXYAxisLisenter;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mImageViewTouchBase:Lcom/mediatek/gallery3d/pq/ImageViewTouchBase;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSettingXYAxisLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$SettingXYAxisLisenter;

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/pq/ImageViewTouchBase;->setXYAxisLisenter(Lcom/mediatek/gallery3d/pq/SetXYAxisIndex;)V

    return-void
.end method

.method private initPQToolView()V
    .locals 2

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->getViewById()V

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecodeImage:Lcom/mediatek/gallery3d/pq/PictureQualityTool$DecodeImage;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setRangeAndIndex()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->addSeekBarListener()V

    return-void
.end method

.method private initPQToolViewADVMode()V
    .locals 2

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->getViewByIdADVMode()V

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecodeImage:Lcom/mediatek/gallery3d/pq/PictureQualityTool$DecodeImage;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->setRangeAndIndexADVMode()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->addSeekBarListenerADVMode()V

    return-void
.end method

.method private onSaveClicked()V
    .locals 4

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "global"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSatAdjIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "sharpness"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSharpAdjIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "skyTone"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneHIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "skinTone"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneHRange()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "grassTone"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneHRange()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "skinSat"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneSIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "grassSat"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneSIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "skySat"

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneSIndex()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method

.method private recoverIndex()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSharpnessIndex:I

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeSetSharpAdjIndex(I)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSatAdjIndex:I

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeSetSatAdjIndex(I)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkinToneIndex:I

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeSetSkinToneHIndex(I)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionGrassToneIndex:I

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeSetGrassToneHIndex(I)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkyToneIndex:I

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeSetSkyToneHIndex(I)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkinToneSIndex:I

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeSetSkinToneSIndex(I)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionGrassToneSIndex:I

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeSetGrassToneSIndex(I)Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    iget v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->origionSkyToneSIndex:I

    invoke-static {v0}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeSetSkyToneSIndex(I)Z

    return-void
.end method

.method private setRangeAndIndex()V
    .locals 4

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->getOriginIndex()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSharpAdjRange()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessRange:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSatAdjRange()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGlobleSatRange:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneHRange()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneHRange()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneHRange()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSkinToneRange=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mGrassToneRange=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSkyToneRange=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneSRange()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinSatRange:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneSRange()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassSatRange:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneSRange()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkySatRange:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyToneMin:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinToneMin:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassToneMin:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSharpnessRange:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessRange:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGlobalSatRange:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGlobleSatRange:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyToneRange:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinToneRange:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassToneRange:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinSatRange:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinSatRange:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassSatRange:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassSatRange:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkySatRange:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkySatRange:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSharpness:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sharpness:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSharpAdjIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSharpness:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSharpAdjIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessRange:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PQJni.nativeGetSkyToneHIndex()=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneHIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PQJni.nativeGetSkyToneHIndex()=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneHIndex()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneHIndex()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkyTone:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sky tone(Hue):  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneHIndex()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkyTone:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneHIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassTone:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Grass tone(Hue):  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneHIndex()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassTone:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneHIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinTone:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Skin tone(Hue):  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneHIndex()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinTone:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneHIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinToneRange:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGlobal:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Global Sat.:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSatAdjIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGlobal:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSatAdjIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGlobleSatRange:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SkyToneRange "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SkinToneRange "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkyToneRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " GrassToneRange "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassToneRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mColorRange "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGlobleSatRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSharpnessRange "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSharpnessRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkinSat:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Skin tone(Sat):  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneSIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkinSat:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkinToneSIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkinSatRange:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewGrassSat:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Grass tone(Sat):  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneSIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarGrassSat:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetGrassToneSIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mGrassSatRange:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mTextViewSkySat:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sky tone(Sat):  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneSIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarSkySat:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSkyToneSIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSkySatRange:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method

.method private setRangeAndIndexADVMode()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetHueAdjRange()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHudRangeADV:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSatAdjRange()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSatRangeADV:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHudRangeADV:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_progress:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Hue:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetHueAdjIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_hue:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetHueAdjIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHudRangeADV:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSatRangeADV:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_progress:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sat:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSatAdjIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->seekBar_saturation:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSatAdjIndex()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHudRangeADV:I

    add-int/lit8 v2, v2, -0x1

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_temple:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHudRangeADV:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_hue_progress_temple:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Hue:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetHueAdjIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_temple:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSatRangeADV:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->textView_saturation_progress_temple:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sat:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQJni:Lcom/mediatek/gallery3d/pq/PictureQualityJni;

    invoke-static {}, Lcom/mediatek/gallery3d/pq/PictureQualityJni;->nativeGetSatAdjIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public getMineType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p1    # Landroid/net/Uri;

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Path==="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    const/4 v8, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "mime_type"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mMineType===== "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v6, :cond_1

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v8

    :catch_0
    move-exception v7

    :try_start_1
    const-string v0, "Gallery2/PictureQualityTool"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ ]:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_1

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public onBackPressed()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->recoverIndex()V

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 22
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/16 v1, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const/16 v1, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    new-instance v21, Landroid/util/DisplayMetrics;

    invoke-direct/range {v21 .. v21}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    move-object/from16 v0, v21

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    move-object/from16 v0, v21

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsWidth:I

    move-object/from16 v0, v21

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsHeight:I

    const-string v1, "Gallery2/PictureQualityTool"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WindowsWidth=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " WindowsHeight=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7f040043

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mView:Landroid/view/View;

    const v1, 0x7f040043

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v20

    const-string v1, "PQUri"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mPqUri:Ljava/lang/String;

    const-string v1, "PQMineType"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mPQMineType:Ljava/lang/String;

    const-string v1, "PQViewWidth"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string v1, "PQViewHeight"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v1, "PQLevelCount"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v2, p0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mPQMineType:Ljava/lang/String;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mPQMineType:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/gallery3d/common/BitmapUtils;->isSupportedByRegionDecoder(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/16 v6, 0x3c0

    new-instance v1, Lcom/mediatek/gallery3d/pq/ImageDecoder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mPqUri:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsHeight:I

    invoke-direct/range {v1 .. v9}, Lcom/mediatek/gallery3d/pq/ImageDecoder;-><init>(Landroid/content/Context;Ljava/lang/String;IIIIII)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    :cond_0
    :goto_0
    new-instance v1, Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mSeekBarChangeLisenter:Lcom/mediatek/gallery3d/pq/PictureQualityTool$SeekBarChangeLisenter;

    invoke-direct {v1, v3}, Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;-><init>(Lcom/mediatek/gallery3d/pq/SeekBarChangeInterface;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    new-instance v1, Lcom/mediatek/gallery3d/pq/PictureQualityTool$DecodeImage;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$DecodeImage;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecodeImage:Lcom/mediatek/gallery3d/pq/PictureQualityTool$DecodeImage;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecodeImage:Lcom/mediatek/gallery3d/pq/PictureQualityTool$DecodeImage;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    invoke-virtual {v1, v3}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$DecodeImage;->setDecoder(Lcom/mediatek/gallery3d/pq/ImageDecoder;)V

    new-instance v1, Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;-><init>(Lcom/mediatek/gallery3d/pq/PictureQualityTool;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mApply:Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mApply:Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    invoke-virtual {v1, v3}, Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;->setDecoder(Lcom/mediatek/gallery3d/pq/ImageDecoder;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mPQMineType:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/gallery3d/common/BitmapUtils;->isSupportedByRegionDecoder(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    instance-of v1, v1, Lcom/mediatek/gallery3d/pq/TileImageDecoder;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mApply:Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;

    invoke-virtual {v1, v3}, Lcom/mediatek/gallery3d/pq/ImageDecoder;->setApply(Ljava/lang/Runnable;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mOnSeekBarChangelisenter:Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mApply:Lcom/mediatek/gallery3d/pq/PictureQualityTool$Apply;

    invoke-virtual {v1, v3}, Lcom/mediatek/gallery3d/pq/OnSeekBarChangelisenter;->setDecodeImage(Ljava/lang/Runnable;)V

    const/4 v1, 0x0

    sput-boolean v1, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->isEnterADVmode:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isPictureQualityEnhanceSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inPostProc:Z

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v3, 0x1

    iput v3, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->initPQToolView()V

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->addSeekBarListener()V

    return-void

    :cond_3
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v6

    new-instance v10, Lcom/mediatek/gallery3d/pq/TileImageDecoder;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mPqUri:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsWidth:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsHeight:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    move-object v11, v2

    move v15, v6

    move/from16 v17, v7

    move/from16 v18, v8

    move/from16 v19, v9

    invoke-direct/range {v10 .. v19}, Lcom/mediatek/gallery3d/pq/TileImageDecoder;-><init>(Landroid/content/Context;Ljava/lang/String;IIILandroid/os/Handler;III)V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11000c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f0b017d

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQSwitchmemu:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQSwitchmemu:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget-boolean v0, Lcom/mediatek/gallery3d/util/MtkLog;->SUPPORT_PQ_ADV:Z

    if-nez v0, :cond_0

    const v0, 0x7f0b017e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQADVMode:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQADVMode:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->isEnterADVmode:Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1    # Landroid/view/MenuItem;

    const/high16 v8, 0x40000000

    const/4 v4, 0x0

    const/4 v7, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :goto_0
    return v7

    :sswitch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->recoverIndex()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->onSaveClicked()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->onSaveClicked()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :sswitch_3
    sget-boolean v3, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->isEnterADVmode:Z

    if-eqz v3, :cond_0

    sput-boolean v4, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->isEnterADVmode:Z

    const-string v3, "Base"

    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->enterBasemode()V

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQSwitchmemu:Landroid/view/MenuItem;

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->PQSwitchmemu:Landroid/view/MenuItem;

    invoke-interface {v3, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mMetric:Landroid/graphics/Matrix;

    const/16 v4, 0x9

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->setValues([F)V

    iget v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsWidth:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v2, v3, v4

    iget v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsHeight:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v1, v3, v4

    sub-float v3, v2, v1

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mMetric:Landroid/graphics/Matrix;

    invoke-virtual {v3, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mMetric:Landroid/graphics/Matrix;

    iget v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsWidth:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    sub-float/2addr v4, v5

    div-float/2addr v4, v8

    iget v5, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->WindowsHeight:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v0

    sub-float/2addr v5, v6

    div-float/2addr v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    sput-boolean v7, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->isEnterADVmode:Z

    const-string v3, "ADV"

    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->enterADVmode()V

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :sswitch_4
    iget-boolean v3, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->sign:Z

    if-nez v3, :cond_2

    const-string v3, "PQ off"

    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    iput-boolean v7, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->sign:Z

    goto/16 :goto_0

    :cond_2
    const-string v3, "PQ on"

    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    iput-boolean v4, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->sign:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0b001c -> :sswitch_1
        0x7f0b014d -> :sswitch_2
        0x7f0b017d -> :sswitch_4
        0x7f0b017e -> :sswitch_3
    .end sparse-switch

    :array_0
    .array-data 4
        0x3f800000
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x3f800000
    .end array-data
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->isEnterADVmode:Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->mDecoder:Lcom/mediatek/gallery3d/pq/ImageDecoder;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/pq/ImageDecoder;->recycle()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/gallery3d/pq/PictureQualityTool;->isEnterADVmode:Z

    return-void
.end method

.method public setTextViewPositionAsSeekBar(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/View;
    .param p4    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v7

    const-string v8, "Gallery2/PictureQualityTool"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "left=="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " top=="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " right=="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " bottom=="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " width="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Landroid/widget/AbsoluteLayout$LayoutParams;

    const/4 v8, -0x2

    const/4 v9, -0x2

    invoke-direct {v3, v8, v9, v1, v6}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v4, Landroid/widget/AbsoluteLayout$LayoutParams;

    const/4 v8, -0x2

    const/4 v9, -0x2

    invoke-direct {v4, v8, v9, v5, v6}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    invoke-virtual {p3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/AbsoluteLayout$LayoutParams;

    const/4 v8, -0x2

    const/4 v9, -0x2

    div-int/lit8 v10, v7, 0x2

    add-int/2addr v10, v1

    invoke-direct {v2, v8, v9, v10, v0}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    invoke-virtual {p4, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setVisibilityADM(Landroid/view/View;I)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method
