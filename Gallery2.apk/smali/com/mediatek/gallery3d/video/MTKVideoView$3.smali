.class Lcom/mediatek/gallery3d/video/MTKVideoView$3;
.super Ljava/lang/Object;
.source "MTKVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/gallery3d/video/MTKVideoView;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/video/MTKVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 11
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v10, 0x1

    const/4 v9, -0x1

    const-string v6, "Gallery2/VideoPlayer/MTKVideoView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/gallery3d/ui/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1300(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v6

    if-ne v6, v9, :cond_1

    const-string v6, "Gallery2/VideoPlayer/MTKVideoView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Duplicate error message. error message has been sent! error=("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/gallery3d/ui/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v10

    :cond_1
    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget-object v7, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v7}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getCurrentPosition()I

    move-result v7

    invoke-static {v6, v7}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1402(Lcom/mediatek/gallery3d/video/MTKVideoView;I)I

    const-string v6, "Gallery2/VideoPlayer/MTKVideoView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onError() mSeekWhenPrepared="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v8}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1500(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mDuration="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v8}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1600(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/gallery3d/ui/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget-object v7, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v7}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1600(Lcom/mediatek/gallery3d/video/MTKVideoView;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    invoke-static {v6, v7}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1602(Lcom/mediatek/gallery3d/video/MTKVideoView;I)I

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6, v9}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1702(Lcom/mediatek/gallery3d/video/MTKVideoView;I)I

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6, v9}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1802(Lcom/mediatek/gallery3d/video/MTKVideoView;I)I

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$1900(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/widget/MediaController;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2000(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/widget/MediaController;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/MediaController;->hide()V

    :cond_2
    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2100(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2300(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v7}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2200(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/media/MediaPlayer;

    move-result-object v7

    invoke-interface {v6, v7, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v6

    if-nez v6, :cond_0

    :cond_3
    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v6}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2400(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/16 v6, 0x104

    if-ne p2, v6, :cond_5

    const v2, 0x2050057

    :goto_1
    const-string v0, "ERROR_DIALOG_TAG"

    iget-object v6, p0, Lcom/mediatek/gallery3d/video/MTKVideoView$3;->this$0:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-static {v6}, Lcom/mediatek/gallery3d/video/MTKVideoView;->access$2500(Lcom/mediatek/gallery3d/video/MTKVideoView;)Landroid/content/Context;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v6, "ERROR_DIALOG_TAG"

    invoke-virtual {v1, v6}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    check-cast v4, Landroid/app/DialogFragment;

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_4
    invoke-static {v2}, Lcom/mediatek/gallery3d/video/ErrorDialogFragment;->newInstance(I)Lcom/mediatek/gallery3d/video/ErrorDialogFragment;

    move-result-object v3

    const-string v6, "ERROR_DIALOG_TAG"

    invoke-virtual {v3, v1, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    goto/16 :goto_0

    :cond_5
    const/16 v6, 0x105

    if-ne p2, v6, :cond_6

    const v2, 0x2050058

    goto :goto_1

    :cond_6
    const/16 v6, 0x106

    if-ne p2, v6, :cond_7

    const v2, 0x2050059

    goto :goto_1

    :cond_7
    const/16 v6, 0x107

    if-ne p2, v6, :cond_8

    const v2, 0x205005a

    goto :goto_1

    :cond_8
    const/16 v6, 0x108

    if-ne p2, v6, :cond_9

    const v2, 0x2050081

    goto :goto_1

    :cond_9
    const/16 v6, 0xc8

    if-ne p2, v6, :cond_a

    const v2, 0x1040015

    goto :goto_1

    :cond_a
    const v2, 0x1040011

    goto :goto_1
.end method
