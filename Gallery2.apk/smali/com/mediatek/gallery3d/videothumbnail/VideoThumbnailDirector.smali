.class public Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;
.super Ljava/lang/Object;
.source "VideoThumbnailDirector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;,
        Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String; = "Gallery2/VideoThumbnailDirector"


# instance fields
.field private final HANDLER_CONCURRENCY:I

.field private volatile mActiveEnd:I

.field private volatile mActiveStart:I

.field private mCurrentStarterIndex:I

.field private mCurrentStoperIndex:I

.field private mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

.field private mGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

.field private volatile mIsStageUpdated:Z

.field private mLockSecretaryBeauty:Ljava/lang/Object;

.field private mLockStarterIndex:Ljava/lang/Object;

.field private mLockStoperIndex:Ljava/lang/Object;

.field private mSecretary:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;

.field private mSlotRenderer:Lcom/android/gallery3d/ui/AlbumSlotRenderer;

.field private mThumbnailStarters:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

.field private mThumbnailStopers:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

.field private mVideoThumbnailPlayer:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/android/gallery3d/app/AbstractGalleryActivity;Lcom/android/gallery3d/ui/AlbumSlotRenderer;)V
    .locals 3
    .param p1    # Lcom/android/gallery3d/app/AbstractGalleryActivity;
    .param p2    # Lcom/android/gallery3d/ui/AlbumSlotRenderer;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->HANDLER_CONCURRENCY:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mLockSecretaryBeauty:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mLockStarterIndex:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mLockStoperIndex:Ljava/lang/Object;

    iput v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mActiveStart:I

    iput v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mActiveEnd:I

    new-array v0, v1, [Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStarters:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    new-array v0, v1, [Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStopers:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    sget-boolean v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    iput-object p2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mSlotRenderer:Lcom/android/gallery3d/ui/AlbumSlotRenderer;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->setGalleryActivity(Lcom/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-static {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->setDirector(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Lcom/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mVideoThumbnailPlayer:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->isThumbnailInStage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Lcom/android/gallery3d/ui/AlbumSlotRenderer;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mSlotRenderer:Lcom/android/gallery3d/ui/AlbumSlotRenderer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mLockSecretaryBeauty:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Z
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    iget-boolean v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mIsStageUpdated:Z

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->respondToStageUpdate()V

    return-void
.end method

.method private collectAllThumbnailsOnStage(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/gallery3d/data/LocalVideo;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mActiveStart:I

    :goto_0
    iget v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mActiveEnd:I

    if-ge v2, v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v4, v2}, Lcom/android/gallery3d/ui/AlbumSlidingWindow;->isActiveSlot(I)Z

    move-result v4

    if-eqz v4, :cond_0

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v4, v2}, Lcom/android/gallery3d/ui/AlbumSlidingWindow;->get(I)Lcom/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    iget v4, v1, Lcom/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->mediaType:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    iget-object v4, v1, Lcom/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/android/gallery3d/data/MediaItem;

    check-cast v4, Lcom/android/gallery3d/data/LocalVideo;

    move-object v3, v4

    check-cast v3, Lcom/android/gallery3d/data/LocalVideo;

    if-eqz v3, :cond_2

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method private isThumbnailInStage(Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    iget v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mActiveStart:I

    :goto_0
    iget v5, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mActiveEnd:I

    if-ge v2, v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    if-nez v5, :cond_1

    :cond_0
    :goto_1
    return v3

    :cond_1
    iget-object v5, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v5, v2}, Lcom/android/gallery3d/ui/AlbumSlidingWindow;->isActiveSlot(I)Z

    move-result v5

    if-eqz v5, :cond_0

    :try_start_0
    iget-object v5, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v5, v2}, Lcom/android/gallery3d/ui/AlbumSlidingWindow;->get(I)Lcom/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    iget v5, v1, Lcom/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->mediaType:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_2

    iget-object v5, v1, Lcom/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/android/gallery3d/data/MediaItem;

    check-cast v5, Lcom/android/gallery3d/data/LocalVideo;

    move-object v4, v5

    check-cast v4, Lcom/android/gallery3d/data/LocalVideo;

    if-eqz v4, :cond_0

    iget-object v5, v4, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private renderThumbnailByPath(Ljava/lang/String;Lcom/android/gallery3d/ui/GLCanvas;II)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mVideoThumbnailPlayer:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->renderThumbnail(Ljava/lang/String;Lcom/android/gallery3d/ui/GLCanvas;II)Z

    move-result v0

    return v0
.end method

.method private requestThumbnails(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/gallery3d/data/LocalVideo;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->cancelPendingTranscode()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/data/LocalVideo;

    iget v2, v1, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I

    if-nez v2, :cond_0

    invoke-static {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->requestThumbnail(Lcom/android/gallery3d/data/LocalVideo;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private respondToStageUpdate()V
    .locals 3

    iget-boolean v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mIsStageUpdated:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/AlbumSlidingWindow;->isAllActiveSlotsStaticThumbnailReady()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mVideoThumbnailPlayer:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-virtual {v2, v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->getPlayingThumbnails(Ljava/util/List;)V

    invoke-direct {p0, v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->scrollThumbnailsOutOfScene(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->collectAllThumbnailsOnStage(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->shuffleThumbnails()V

    invoke-direct {p0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->requestThumbnails(Ljava/util/List;)V

    invoke-direct {p0, v1, v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->selectPreparedThumbnails(Ljava/util/List;Ljava/util/List;)V

    invoke-direct {p0, v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->scrollThumbnailsIntoScene(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mIsStageUpdated:Z

    goto :goto_0
.end method

.method private scrollThumbnailsIntoScene(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStarters:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStarters:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;->cancelPendingRunnables()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mSlotRenderer:Lcom/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/AlbumSlotRenderer;->isStageChanging()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_2
    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->startThumbnails(Ljava/util/List;)Z

    move-result v1

    goto :goto_1
.end method

.method private scrollThumbnailsOutOfScene(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x1

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStopers:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStopers:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;->cancelPendingRunnables()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mSlotRenderer:Lcom/android/gallery3d/ui/AlbumSlotRenderer;

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/AlbumSlotRenderer;->isStageChanging()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    return v1

    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->stopThumbnails(Ljava/util/List;Z)Z

    move-result v1

    goto :goto_1
.end method

.method private selectPreparedThumbnails(Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/gallery3d/data/LocalVideo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->clear()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/data/LocalVideo;

    iget v2, v1, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v2, v1, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private shuffleThumbnails()V
    .locals 0

    return-void
.end method

.method private startThumbnails(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mCurrentStarterIndex:I

    rem-int/lit8 v0, v0, 0x1

    iget v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mCurrentStarterIndex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mCurrentStarterIndex:I

    iget v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mCurrentStarterIndex:I

    rem-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mCurrentStarterIndex:I

    iget-object v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStarters:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v3, v4, v0

    if-eqz v3, :cond_0

    new-instance v4, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;

    invoke-direct {v4, p0, v2, v3}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;Ljava/lang/String;Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;)V

    invoke-virtual {v3, v4}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;->submit(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    return v4
.end method

.method private stopThumbnails(Ljava/util/List;Z)Z
    .locals 5
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mCurrentStoperIndex:I

    rem-int/lit8 v0, v0, 0x1

    iget v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mCurrentStoperIndex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mCurrentStoperIndex:I

    iget v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mCurrentStoperIndex:I

    rem-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mCurrentStoperIndex:I

    iget-object v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStopers:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v3, v4, v0

    if-eqz v3, :cond_0

    new-instance v4, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$4;

    invoke-direct {v4, p0, p2, v2, v3}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$4;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;ZLjava/lang/String;Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;)V

    invoke-virtual {v3, v4}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;->submit(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    return v4
.end method


# virtual methods
.method public pause()V
    .locals 2

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mSecretary:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mSecretary:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;

    new-instance v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$2;

    invoke-direct {v1, p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$2;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    invoke-static {}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->pause()V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStarters:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStarters:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStopers:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStopers:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public pumpLiveThumbnails()V
    .locals 2

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mLockSecretaryBeauty:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mIsStageUpdated:Z

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mLockSecretaryBeauty:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public renderThumbnail(Lcom/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;Lcom/android/gallery3d/ui/GLCanvas;II)Z
    .locals 4
    .param p1    # Lcom/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    .param p2    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p3    # I
    .param p4    # I

    const/4 v1, 0x0

    iget v2, p1, Lcom/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->mediaType:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p1, Lcom/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/android/gallery3d/data/MediaItem;

    check-cast v0, Lcom/android/gallery3d/data/LocalVideo;

    if-eqz v0, :cond_0

    iget v2, v0, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v1, v0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    invoke-direct {p0, v1, p2, p3, p4}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->renderThumbnailByPath(Ljava/lang/String;Lcom/android/gallery3d/ui/GLCanvas;II)Z

    move-result v1

    goto :goto_0
.end method

.method public resume(Lcom/android/gallery3d/ui/AlbumSlidingWindow;)V
    .locals 3
    .param p1    # Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->pumpLiveThumbnails()V

    iput-object p1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-static {}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->start()V

    new-instance v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$1;

    invoke-direct {v1, p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$1;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mGalleryActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->create(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer$OnFrameAvailableListener;Lcom/android/gallery3d/app/AbstractGalleryActivity;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mVideoThumbnailPlayer:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mVideoThumbnailPlayer:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->resume()V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStarters:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    new-instance v2, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;-><init>()V

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStopers:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    new-instance v2, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;-><init>()V

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStarters:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mThumbnailStopers:[Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$1;)V

    iput-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mSecretary:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mSecretary:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public updateStage()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/AlbumSlidingWindow;->getActiveStart()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mActiveStart:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mDataWindow:Lcom/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/AlbumSlidingWindow;->getActiveEnd()I

    move-result v0

    iput v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->mActiveEnd:I

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->pumpLiveThumbnails()V

    return-void
.end method
