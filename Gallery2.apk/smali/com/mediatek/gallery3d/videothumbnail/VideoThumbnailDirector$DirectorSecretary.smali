.class Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;
.super Ljava/lang/Thread;
.source "VideoThumbnailDirector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DirectorSecretary"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;


# direct methods
.method private constructor <init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;
    .param p2    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$1;

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const-string v1, "pretty secretary"

    invoke-virtual {p0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-static {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->access$500(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :goto_1
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-static {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->access$600(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    :try_start_1
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-static {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->access$500(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    monitor-exit v2

    :cond_1
    :goto_2
    return-void

    :cond_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-static {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->access$400(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Lcom/android/gallery3d/ui/AlbumSlotRenderer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/AlbumSlotRenderer;->isStageFixed()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-static {v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->access$700(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)V

    const-wide/16 v1, 0x50

    :try_start_3
    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$DirectorSecretary;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :catchall_0
    move-exception v1

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1
.end method
