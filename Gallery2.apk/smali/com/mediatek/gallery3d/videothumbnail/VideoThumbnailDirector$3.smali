.class Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;
.super Ljava/lang/Object;
.source "VideoThumbnailDirector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->startThumbnails(Ljava/util/List;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

.field final synthetic val$path:Ljava/lang/String;

.field final synthetic val$playerStarter:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;


# direct methods
.method constructor <init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;Ljava/lang/String;Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    iput-object p2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->val$path:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->val$playerStarter:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-static {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->access$200(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->val$path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->isThumbnailPlaying(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->val$path:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->access$300(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-static {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->access$400(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Lcom/android/gallery3d/ui/AlbumSlotRenderer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/AlbumSlotRenderer;->isStageChanging()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->this$0:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;

    invoke-static {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;->access$200(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector;)Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->val$path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailPlayer;->openThumbnail(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$3;->val$playerStarter:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;

    invoke-virtual {v0, p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailDirector$PlayerHandler;->submit(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method
