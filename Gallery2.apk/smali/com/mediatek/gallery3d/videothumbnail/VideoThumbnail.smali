.class public abstract Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;
.super Ljava/lang/Object;
.source "VideoThumbnail.java"

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
.implements Lcom/android/gallery3d/ui/GLRoot$OnGLIdleListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Gallery2/VideoThumbnail"

.field static final TEXTURE_HEIGHT:I = 0x80

.field static final TEXTURE_WIDTH:I = 0x80


# instance fields
.field protected isReadyForRender:Z

.field public volatile isWorking:Z

.field private mDestRect:Landroid/graphics/RectF;

.field protected mHasNewFrame:Z

.field private mHasTexture:Z

.field private mHeight:I

.field private mSrcRect:Landroid/graphics/RectF;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mTransformFinal:[F

.field private mTransformForCropingCenter:[F

.field private mTransformFromSurfaceTexture:[F

.field private mVideoFrameTexture:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;

.field private mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/16 v0, 0x80

    const/16 v2, 0x10

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mWidth:I

    iput v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHeight:I

    new-array v0, v2, [F

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformFromSurfaceTexture:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformForCropingCenter:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformFinal:[F

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHasTexture:Z

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHasNewFrame:Z

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->isReadyForRender:Z

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->isWorking:Z

    return-void
.end method

.method private drawByCropingCenter(Lcom/android/gallery3d/ui/GLCanvas;II)V
    .locals 14
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHasTexture:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->isWorking:Z

    if-nez v1, :cond_1

    :cond_0
    monitor-exit p0

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformFromSurfaceTexture:[F

    invoke-virtual {v1, v2}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    const/4 v1, 0x2

    invoke-interface {p1, v1}, Lcom/android/gallery3d/ui/GLCanvas;->save(I)V

    div-int/lit8 v8, p2, 0x2

    div-int/lit8 v9, p3, 0x2

    int-to-float v1, v8

    int-to-float v2, v9

    invoke-interface {p1, v1, v2}, Lcom/android/gallery3d/ui/GLCanvas;->translate(FF)V

    const/high16 v1, 0x3f800000

    const/high16 v2, -0x40800000

    const/high16 v3, 0x3f800000

    invoke-interface {p1, v1, v2, v3}, Lcom/android/gallery3d/ui/GLCanvas;->scale(FFF)V

    neg-int v1, v8

    int-to-float v1, v1

    neg-int v2, v9

    int-to-float v2, v2

    invoke-interface {p1, v1, v2}, Lcom/android/gallery3d/ui/GLCanvas;->translate(FF)V

    new-instance v13, Landroid/graphics/RectF;

    const/4 v1, 0x0

    const/4 v2, 0x0

    move/from16 v0, p2

    int-to-float v3, v0

    move/from16 v0, p3

    int-to-float v4, v0

    invoke-direct {v13, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mWidth:I

    iget v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHeight:I

    if-le v1, v2, :cond_2

    iget v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mWidth:I

    iget v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHeight:I

    sub-int/2addr v1, v2

    mul-int/lit16 v1, v1, 0x80

    int-to-float v1, v1

    iget v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mWidth:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    div-float v11, v1, v2

    const/high16 v1, 0x43000000

    sub-float v10, v1, v11

    new-instance v12, Landroid/graphics/RectF;

    const/4 v1, 0x0

    const/high16 v2, 0x43000000

    invoke-direct {v12, v11, v1, v10, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    :goto_1
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mVideoFrameTexture:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;

    invoke-static {v12, v13, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->genCononTexCoords(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;)V

    invoke-direct {p0, v12}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->genExtTexMatForSubTile(Landroid/graphics/RectF;)V

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformFinal:[F

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformFromSurfaceTexture:[F

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformForCropingCenter:[F

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mVideoFrameTexture:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;

    iget-object v3, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformFinal:[F

    iget v1, v13, Landroid/graphics/RectF;->left:F

    float-to-int v4, v1

    iget v1, v13, Landroid/graphics/RectF;->top:F

    float-to-int v5, v1

    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v6, v1

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-int v7, v1

    move-object v1, p1

    invoke-interface/range {v1 .. v7}, Lcom/android/gallery3d/ui/GLCanvas;->drawTexture(Lcom/android/gallery3d/ui/BasicTexture;[FIIII)V

    invoke-interface {p1}, Lcom/android/gallery3d/ui/GLCanvas;->restore()V

    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_2
    :try_start_1
    iget v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHeight:I

    iget v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mWidth:I

    sub-int/2addr v1, v2

    mul-int/lit16 v1, v1, 0x80

    int-to-float v1, v1

    iget v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHeight:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    div-float v11, v1, v2

    const/high16 v1, 0x43000000

    sub-float v10, v1, v11

    new-instance v12, Landroid/graphics/RectF;

    const/4 v1, 0x0

    const/high16 v2, 0x43000000

    invoke-direct {v12, v1, v11, v2, v10}, Landroid/graphics/RectF;-><init>(FFFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private static genCononTexCoords(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;)V
    .locals 9
    .param p0    # Landroid/graphics/RectF;
    .param p1    # Landroid/graphics/RectF;
    .param p2    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;

    invoke-virtual {p2}, Lcom/android/gallery3d/ui/ExtTexture;->getWidth()I

    move-result v3

    invoke-virtual {p2}, Lcom/android/gallery3d/ui/ExtTexture;->getHeight()I

    move-result v0

    invoke-virtual {p2}, Lcom/android/gallery3d/ui/ExtTexture;->getTextureWidth()I

    move-result v2

    invoke-virtual {p2}, Lcom/android/gallery3d/ui/ExtTexture;->getTextureHeight()I

    move-result v1

    iget v6, p0, Landroid/graphics/RectF;->left:F

    int-to-float v7, v2

    div-float/2addr v6, v7

    iput v6, p0, Landroid/graphics/RectF;->left:F

    iget v6, p0, Landroid/graphics/RectF;->right:F

    int-to-float v7, v2

    div-float/2addr v6, v7

    iput v6, p0, Landroid/graphics/RectF;->right:F

    iget v6, p0, Landroid/graphics/RectF;->top:F

    int-to-float v7, v1

    div-float/2addr v6, v7

    iput v6, p0, Landroid/graphics/RectF;->top:F

    iget v6, p0, Landroid/graphics/RectF;->bottom:F

    int-to-float v7, v1

    div-float/2addr v6, v7

    iput v6, p0, Landroid/graphics/RectF;->bottom:F

    int-to-float v6, v3

    int-to-float v7, v2

    div-float v4, v6, v7

    iget v6, p0, Landroid/graphics/RectF;->right:F

    cmpl-float v6, v6, v4

    if-lez v6, :cond_0

    iget v6, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v7

    iget v8, p0, Landroid/graphics/RectF;->left:F

    sub-float v8, v4, v8

    mul-float/2addr v7, v8

    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v8

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, p1, Landroid/graphics/RectF;->right:F

    iput v4, p0, Landroid/graphics/RectF;->right:F

    :cond_0
    int-to-float v6, v0

    int-to-float v7, v1

    div-float v5, v6, v7

    iget v6, p0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v6, v6, v5

    if-lez v6, :cond_1

    iget v6, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v7

    iget v8, p0, Landroid/graphics/RectF;->top:F

    sub-float v8, v5, v8

    mul-float/2addr v7, v8

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v8

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, p1, Landroid/graphics/RectF;->bottom:F

    iput v5, p0, Landroid/graphics/RectF;->bottom:F

    :cond_1
    return-void
.end method

.method private genExtTexMatForSubTile(Landroid/graphics/RectF;)V
    .locals 5
    .param p1    # Landroid/graphics/RectF;

    const/high16 v4, 0x3f800000

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformForCropingCenter:[F

    const/4 v1, 0x0

    iget v2, p1, Landroid/graphics/RectF;->right:F

    iget v3, p1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformForCropingCenter:[F

    const/4 v1, 0x5

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    iget v3, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformForCropingCenter:[F

    const/16 v1, 0xa

    aput v4, v0, v1

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformForCropingCenter:[F

    const/16 v1, 0xc

    iget v2, p1, Landroid/graphics/RectF;->left:F

    aput v2, v0, v1

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformForCropingCenter:[F

    const/16 v1, 0xd

    iget v2, p1, Landroid/graphics/RectF;->top:F

    aput v2, v0, v1

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mTransformForCropingCenter:[F

    const/16 v1, 0xf

    aput v4, v0, v1

    return-void
.end method

.method private static releaseSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    .locals 1
    .param p0    # Landroid/graphics/SurfaceTexture;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    sget-boolean v0, Lcom/android/gallery3d/common/ApiHelper;->HAS_RELEASE_SURFACE_TEXTURE:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/SurfaceTexture;->release()V

    :cond_0
    return-void
.end method

.method private static setDefaultBufferSize(Landroid/graphics/SurfaceTexture;II)V
    .locals 1
    .param p0    # Landroid/graphics/SurfaceTexture;
    .param p1    # I
    .param p2    # I

    sget-boolean v0, Lcom/android/gallery3d/common/ApiHelper;->HAS_SET_DEFALT_BUFFER_SIZE:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    :cond_0
    return-void
.end method


# virtual methods
.method public acquireSurfaceTexture()V
    .locals 3

    const/16 v2, 0x80

    new-instance v0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;

    const v1, 0x8d65

    invoke-direct {v0, p0, v1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;-><init>(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;I)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mVideoFrameTexture:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mVideoFrameTexture:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;

    invoke-virtual {v0, v2, v2}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;->setSize(II)V

    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mVideoFrameTexture:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/ExtTexture;->getId()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-static {v0, v2, v2}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->setDefaultBufferSize(Landroid/graphics/SurfaceTexture;II)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHasTexture:Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public draw(Lcom/android/gallery3d/ui/GLCanvas;II)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->drawByCropingCenter(Lcom/android/gallery3d/ui/GLCanvas;II)V

    return-void
.end method

.method public getSurfaceTexture()Landroid/graphics/SurfaceTexture;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method public abstract onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
.end method

.method public onGLIdle(Lcom/android/gallery3d/ui/GLCanvas;Z)Z
    .locals 4
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;
    .param p2    # Z

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->isWorking:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHasTexture:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHasNewFrame:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->updateTexImage()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHasNewFrame:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->isReadyForRender:Z

    :cond_1
    monitor-exit p0

    :goto_0
    return v3

    :catch_0
    move-exception v0

    const-string v1, "Gallery2/VideoThumbnail"

    const-string v2, "notify author that mSurfaceTexture in thumbnail released when updating tex img"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    iput p2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mWidth:I

    iput p3, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHeight:I

    return-void
.end method

.method public releaseSurfaceTexture()V
    .locals 2

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHasTexture:Z

    if-nez v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mHasTexture:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mVideoFrameTexture:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/ExtTexture;->recycle()V

    iput-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mVideoFrameTexture:Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail$VideoFrameTexture;

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-static {v0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->releaseSurfaceTexture(Landroid/graphics/SurfaceTexture;)V

    iput-object v1, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnail;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
