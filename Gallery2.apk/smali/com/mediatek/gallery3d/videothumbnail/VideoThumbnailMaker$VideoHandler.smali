.class Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;
.super Ljava/lang/Thread;
.source "VideoThumbnailMaker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VideoHandler"
.end annotation


# instance fields
.field private mCurrentVideo:Lcom/android/gallery3d/data/LocalVideo;

.field private final mVideoQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/android/gallery3d/data/LocalVideo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DynamicThumbnailRequestHandler-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->mVideoQueue:Ljava/util/concurrent/BlockingQueue;

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;Lcom/android/gallery3d/data/LocalVideo;)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;
    .param p1    # Lcom/android/gallery3d/data/LocalVideo;

    invoke-direct {p0, p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->submit(Lcom/android/gallery3d/data/LocalVideo;)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->cancelAllTranscode()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;)V
    .locals 0
    .param p0    # Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    invoke-direct {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->cancelPendingTranscode()V

    return-void
.end method

.method private cancelAllTranscode()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->cancelPendingTranscode()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->cancelCurrentTranscode()V

    invoke-static {}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->access$300()Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    return-void
.end method

.method private cancelCurrentTranscode()V
    .locals 6

    invoke-static {}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->access$200()Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->access$200()Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/mediatek/transcode/VideoTranscode;->cancel(J)V

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->mCurrentVideo:Lcom/android/gallery3d/data/LocalVideo;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    if-eqz v2, :cond_1

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v0, Lcom/android/gallery3d/data/LocalVideo;->dynamicThumbnailPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".tmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_1
    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private cancelPendingTranscode()V
    .locals 3

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->mVideoQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/data/LocalVideo;

    const/4 v2, 0x0

    iput v2, v1, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->mVideoQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->clear()V

    return-void
.end method

.method private cancelPendingTranscode(Lcom/android/gallery3d/data/LocalVideo;)Z
    .locals 1
    .param p1    # Lcom/android/gallery3d/data/LocalVideo;

    const/4 v0, 0x0

    iput v0, p1, Lcom/android/gallery3d/data/LocalVideo;->thumbNailState:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->mVideoQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private submit(Lcom/android/gallery3d/data/LocalVideo;)V
    .locals 3
    .param p1    # Lcom/android/gallery3d/data/LocalVideo;

    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->access$100(Lcom/android/gallery3d/data/LocalVideo;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Gallery2/VideoThumbnailMaker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "submit transcoding request for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->mVideoQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Gallery2/VideoThumbnailMaker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should be started before submitting tasks."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 5

    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->mVideoQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/data/LocalVideo;

    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iput-object v0, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->mCurrentVideo:Lcom/android/gallery3d/data/LocalVideo;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v2, "Gallery2/VideoThumbnailMaker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handle transcoding request for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->mCurrentVideo:Lcom/android/gallery3d/data/LocalVideo;

    iget-object v4, v4, Lcom/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker$VideoHandler;->mCurrentVideo:Lcom/android/gallery3d/data/LocalVideo;

    invoke-static {v2}, Lcom/mediatek/gallery3d/videothumbnail/VideoThumbnailMaker;->access$000(Lcom/android/gallery3d/data/LocalVideo;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Gallery2/VideoThumbnailMaker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Terminating "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    return-void

    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
.end method
