.class public Lcom/mediatek/gallery3d/ui/MavSeekBar;
.super Landroid/widget/SeekBar;
.source "MavSeekBar.java"


# static fields
.field private static final MSG_UPDATE_THUMB_APHPA:I = 0x0

.field public static final STATE_LOADING:I = 0x0

.field public static final STATE_SLIDING:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Gallery2/MavSeekBar"


# instance fields
.field private alpha:I

.field private mHander:Landroid/os/Handler;

.field private mProgressDrawableLoading:Landroid/graphics/drawable/Drawable;

.field private mProgressDrawableSliding:Landroid/graphics/drawable/Drawable;

.field private mState:I

.field private mThumb:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->alpha:I

    new-instance v0, Lcom/mediatek/gallery3d/ui/MavSeekBar$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar$1;-><init>(Lcom/mediatek/gallery3d/ui/MavSeekBar;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mHander:Landroid/os/Handler;

    const-string v0, "Gallery2/MavSeekBar"

    const-string v1, "constructor #1 called"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->initializeDrawable()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->alpha:I

    new-instance v0, Lcom/mediatek/gallery3d/ui/MavSeekBar$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar$1;-><init>(Lcom/mediatek/gallery3d/ui/MavSeekBar;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mHander:Landroid/os/Handler;

    const-string v0, "Gallery2/MavSeekBar"

    const-string v1, "constructor #2 called"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->initializeDrawable()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->alpha:I

    new-instance v0, Lcom/mediatek/gallery3d/ui/MavSeekBar$1;

    invoke-direct {v0, p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar$1;-><init>(Lcom/mediatek/gallery3d/ui/MavSeekBar;)V

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mHander:Landroid/os/Handler;

    const-string v0, "Gallery2/MavSeekBar"

    const-string v1, "constructor #3 called"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->initializeDrawable()V

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/gallery3d/ui/MavSeekBar;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/MavSeekBar;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/gallery3d/ui/MavSeekBar;)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/MavSeekBar;

    iget v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->alpha:I

    return v0
.end method

.method static synthetic access$112(Lcom/mediatek/gallery3d/ui/MavSeekBar;I)I
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/MavSeekBar;
    .param p1    # I

    iget v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->alpha:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->alpha:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/gallery3d/ui/MavSeekBar;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/gallery3d/ui/MavSeekBar;

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mHander:Landroid/os/Handler;

    return-object v0
.end method

.method private init()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->setState(I)V

    return-void
.end method

.method private initializeDrawable()V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020136

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020137

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mProgressDrawableLoading:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mProgressDrawableSliding:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private setState(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    const/4 v2, 0x0

    iput p1, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mState:I

    iget v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mState:I

    if-nez v0, :cond_1

    const-string v0, "Gallery2/MavSeekBar"

    const-string v1, "set MavSeekBar state as STATE_LOADING"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mHander:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Landroid/widget/AbsSeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v2}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mProgressDrawableLoading:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v2}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mState:I

    if-ne v0, v3, :cond_0

    const-string v0, "Gallery2/MavSeekBar"

    const-string v1, "set MavSeekBar state as STATE_SLIDING"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getMax()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->setProgress(I)V

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->showThumb()V

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mProgressDrawableSliding:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public getState()I
    .locals 1

    iget v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mState:I

    return v0
.end method

.method public restore()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->init()V

    return-void
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mHander:Landroid/os/Handler;

    return-void
.end method

.method public declared-synchronized setProgress(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mState:I

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getMax()I

    move-result v0

    if-lt p1, v0, :cond_1

    const-string v0, "TAG"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enter sliding mode, state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", max: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getMax()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", progress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->setState(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mState:I

    if-ne v0, v1, :cond_0

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->init()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/gallery3d/ui/MavSeekBar;->restore()V

    :cond_0
    return-void
.end method

.method public showThumb()V
    .locals 2

    const/4 v1, 0x0

    iput v1, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->alpha:I

    iget-object v0, p0, Lcom/mediatek/gallery3d/ui/MavSeekBar;->mHander:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public syncProgressByGyroSensor(I)V
    .locals 0
    .param p1    # I

    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method
