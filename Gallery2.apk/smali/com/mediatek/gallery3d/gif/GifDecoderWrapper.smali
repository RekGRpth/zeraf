.class public Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;
.super Ljava/lang/Object;
.source "GifDecoderWrapper.java"


# static fields
.field public static final INVALID_VALUE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Gallery2/GifDecoderWrapper"


# instance fields
.field private mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;


# direct methods
.method private constructor <init>(Lcom/mediatek/common/gifdecoder/IGifDecoder;)V
    .locals 0
    .param p1    # Lcom/mediatek/common/gifdecoder/IGifDecoder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    return-void
.end method

.method public static createGifDecoderWrapper(Ljava/io/FileDescriptor;)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;
    .locals 2
    .param p0    # Ljava/io/FileDescriptor;

    const/4 v1, 0x0

    invoke-static {v1, p0}, Lcom/mediatek/gallery3d/gif/GifHelper;->createGifDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Ljava/io/FileDescriptor;)Lcom/mediatek/gifdecoder/GifDecoder;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-direct {v1, v0}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;-><init>(Lcom/mediatek/common/gifdecoder/IGifDecoder;)V

    goto :goto_0
.end method

.method public static createGifDecoderWrapper(Ljava/io/InputStream;)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;
    .locals 2
    .param p0    # Ljava/io/InputStream;

    const/4 v1, 0x0

    invoke-static {v1, p0}, Lcom/mediatek/gallery3d/gif/GifHelper;->createGifDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Ljava/io/InputStream;)Lcom/mediatek/gifdecoder/GifDecoder;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-direct {v1, v0}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;-><init>(Lcom/mediatek/common/gifdecoder/IGifDecoder;)V

    goto :goto_0
.end method

.method public static createGifDecoderWrapper(Ljava/lang/String;)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1, p0}, Lcom/mediatek/gallery3d/gif/GifHelper;->createGifDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;)Lcom/mediatek/gifdecoder/GifDecoder;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-direct {v1, v0}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;-><init>(Lcom/mediatek/common/gifdecoder/IGifDecoder;)V

    goto :goto_0
.end method

.method public static createGifDecoderWrapper([BII)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;
    .locals 2
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    invoke-static {v1, p0, p1, p2}, Lcom/mediatek/gallery3d/gif/GifHelper;->createGifDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;[BII)Lcom/mediatek/gifdecoder/GifDecoder;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-direct {v1, v0}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;-><init>(Lcom/mediatek/common/gifdecoder/IGifDecoder;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 0

    return-void
.end method

.method public getFrameBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    invoke-interface {v0, p1}, Lcom/mediatek/common/gifdecoder/IGifDecoder;->getFrameBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getFrameDuration(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    invoke-interface {v0, p1}, Lcom/mediatek/common/gifdecoder/IGifDecoder;->getFrameDuration(I)I

    move-result v0

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    invoke-interface {v0}, Lcom/mediatek/common/gifdecoder/IGifDecoder;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getTotalDuration()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    invoke-interface {v0}, Lcom/mediatek/common/gifdecoder/IGifDecoder;->getTotalDuration()I

    move-result v0

    goto :goto_0
.end method

.method public getTotalFrameCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    invoke-interface {v0}, Lcom/mediatek/common/gifdecoder/IGifDecoder;->getTotalFrameCount()I

    move-result v0

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->mGifDecoder:Lcom/mediatek/common/gifdecoder/IGifDecoder;

    invoke-interface {v0}, Lcom/mediatek/common/gifdecoder/IGifDecoder;->getWidth()I

    move-result v0

    goto :goto_0
.end method
