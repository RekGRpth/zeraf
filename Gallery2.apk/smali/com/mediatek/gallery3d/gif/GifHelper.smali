.class public Lcom/mediatek/gallery3d/gif/GifHelper;
.super Ljava/lang/Object;
.source "GifHelper.java"


# static fields
.field public static final FILE_EXTENSION:Ljava/lang/String; = "gif"

.field public static final MIME_TYPE:Ljava/lang/String; = "image/gif"

.field private static final TAG:Ljava/lang/String; = "Gallery2/GifHelper"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createGifDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Ljava/io/FileDescriptor;)Lcom/mediatek/gifdecoder/GifDecoder;
    .locals 4
    .param p0    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p1    # Ljava/io/FileDescriptor;

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-static {v1}, Lcom/mediatek/gallery3d/gif/GifHelper;->createGifDecoderInner(Ljava/io/InputStream;)Lcom/mediatek/gifdecoder/GifDecoder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v2

    const-string v3, "Gallery2/GifHelper"

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createGifDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Ljava/io/InputStream;)Lcom/mediatek/gifdecoder/GifDecoder;
    .locals 2
    .param p0    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p1    # Ljava/io/InputStream;

    :try_start_0
    invoke-static {p1}, Lcom/mediatek/gallery3d/gif/GifHelper;->createGifDecoderInner(Ljava/io/InputStream;)Lcom/mediatek/gifdecoder/GifDecoder;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "Gallery2/GifHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static createGifDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;)Lcom/mediatek/gifdecoder/GifDecoder;
    .locals 4
    .param p0    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p1    # Ljava/lang/String;

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/mediatek/gallery3d/gif/GifHelper;->createGifDecoderInner(Ljava/io/InputStream;)Lcom/mediatek/gifdecoder/GifDecoder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v2

    const-string v3, "Gallery2/GifHelper"

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createGifDecoder(Lcom/android/gallery3d/util/ThreadPool$JobContext;[BII)Lcom/mediatek/gifdecoder/GifDecoder;
    .locals 3
    .param p0    # Lcom/android/gallery3d/util/ThreadPool$JobContext;
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    if-nez p1, :cond_1

    const-string v1, "Gallery2/GifHelper"

    const-string v2, "createGifDecoder:find null buffer!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/mediatek/gifdecoder/GifDecoder;

    invoke-direct {v0, p1, p2, p3}, Lcom/mediatek/gifdecoder/GifDecoder;-><init>([BII)V

    invoke-virtual {v0}, Lcom/mediatek/gifdecoder/GifDecoder;->getTotalFrameCount()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Gallery2/GifHelper"

    const-string v2, "createGifDecoder:got invalid GifDecoder"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static createGifDecoderInner(Ljava/io/InputStream;)Lcom/mediatek/gifdecoder/GifDecoder;
    .locals 3
    .param p0    # Ljava/io/InputStream;

    if-nez p0, :cond_1

    const-string v1, "Gallery2/GifHelper"

    const-string v2, "createGifDecoder:find null InputStream!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/mediatek/gifdecoder/GifDecoder;

    invoke-direct {v0, p0}, Lcom/mediatek/gifdecoder/GifDecoder;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/mediatek/gifdecoder/GifDecoder;->getTotalFrameCount()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Gallery2/GifHelper"

    const-string v2, "createGifDecoder:got invalid GifDecoder"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method
