.class public Lcom/android/gallery3d/util/RangeIntArray;
.super Ljava/lang/Object;
.source "RangeIntArray.java"


# instance fields
.field private mData:[I

.field private mOffset:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sub-int v0, p2, p1

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/gallery3d/util/RangeIntArray;->mData:[I

    iput p1, p0, Lcom/android/gallery3d/util/RangeIntArray;->mOffset:I

    return-void
.end method

.method public constructor <init>([III)V
    .locals 0
    .param p1    # [I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/gallery3d/util/RangeIntArray;->mData:[I

    iput p2, p0, Lcom/android/gallery3d/util/RangeIntArray;->mOffset:I

    return-void
.end method


# virtual methods
.method public get(I)I
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/util/RangeIntArray;->mData:[I

    iget v1, p0, Lcom/android/gallery3d/util/RangeIntArray;->mOffset:I

    sub-int v1, p1, v1

    aget v0, v0, v1

    return v0
.end method

.method public indexOf(I)I
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/gallery3d/util/RangeIntArray;->mData:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/gallery3d/util/RangeIntArray;->mData:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    iget v1, p0, Lcom/android/gallery3d/util/RangeIntArray;->mOffset:I

    add-int/2addr v1, v0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const v1, 0x7fffffff

    goto :goto_1
.end method

.method public put(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/gallery3d/util/RangeIntArray;->mData:[I

    iget v1, p0, Lcom/android/gallery3d/util/RangeIntArray;->mOffset:I

    sub-int v1, p1, v1

    aput p2, v0, v1

    return-void
.end method
