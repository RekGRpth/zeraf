.class public Lcom/android/gallery3d/filtershow/PanelController;
.super Ljava/lang/Object;
.source "PanelController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/filtershow/PanelController$ViewType;,
        Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;,
        Lcom/android/gallery3d/filtershow/PanelController$Panel;
    }
.end annotation


# static fields
.field private static final ANIM_DURATION:I = 0xc8

.field private static COMPONENT:I = 0x0

.field private static HORIZONTAL_MOVE:I = 0x0

.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/PanelController"

.field private static PANEL:I

.field private static VERTICAL_MOVE:I


# instance fields
.field private mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

.field private mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

.field private mCurrentPanel:Landroid/view/View;

.field private mDisableFilterButtons:Z

.field private final mImageViews:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

.field private final mPanels:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Lcom/android/gallery3d/filtershow/PanelController$Panel;",
            ">;"
        }
    .end annotation
.end field

.field private mRowPanel:Landroid/view/View;

.field private mSaveAndSetPreset:Z

.field private mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

.field private final mViews:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Lcom/android/gallery3d/filtershow/PanelController$ViewType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    sput v0, Lcom/android/gallery3d/filtershow/PanelController;->PANEL:I

    sput v1, Lcom/android/gallery3d/filtershow/PanelController;->COMPONENT:I

    sput v0, Lcom/android/gallery3d/filtershow/PanelController;->VERTICAL_MOVE:I

    sput v1, Lcom/android/gallery3d/filtershow/PanelController;->HORIZONTAL_MOVE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mDisableFilterButtons:Z

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mSaveAndSetPreset:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mPanels:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mViews:Ljava/util/HashMap;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mImageViews:Ljava/util/Vector;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentPanel:Landroid/view/View;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mRowPanel:Landroid/view/View;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    return-void
.end method

.method static synthetic access$000(Lcom/android/gallery3d/filtershow/PanelController;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/gallery3d/filtershow/PanelController;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mRowPanel:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100()I
    .locals 1

    sget v0, Lcom/android/gallery3d/filtershow/PanelController;->HORIZONTAL_MOVE:I

    return v0
.end method

.method static synthetic access$200()I
    .locals 1

    sget v0, Lcom/android/gallery3d/filtershow/PanelController;->VERTICAL_MOVE:I

    return v0
.end method

.method static synthetic access$400(Lcom/android/gallery3d/filtershow/PanelController;)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;
    .locals 1
    .param p0    # Lcom/android/gallery3d/filtershow/PanelController;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/gallery3d/filtershow/PanelController;)Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;
    .locals 1
    .param p0    # Lcom/android/gallery3d/filtershow/PanelController;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    return-object v0
.end method

.method private showCropPopupMenu(Lcom/android/gallery3d/filtershow/ui/FramedTextButton;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/filtershow/ui/FramedTextButton;

    new-instance v0, Landroid/widget/PopupMenu;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f110005

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    new-instance v1, Lcom/android/gallery3d/filtershow/PanelController$2;

    invoke-direct {v1, p0, p1}, Lcom/android/gallery3d/filtershow/PanelController$2;-><init>(Lcom/android/gallery3d/filtershow/PanelController;Lcom/android/gallery3d/filtershow/ui/FramedTextButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method

.method private showCurvesPopupMenu(Lcom/android/gallery3d/filtershow/ui/ImageCurves;Lcom/android/gallery3d/filtershow/ui/FramedTextButton;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/filtershow/ui/ImageCurves;
    .param p2    # Lcom/android/gallery3d/filtershow/ui/FramedTextButton;

    new-instance v0, Landroid/widget/PopupMenu;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f110006

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    new-instance v1, Lcom/android/gallery3d/filtershow/PanelController$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/gallery3d/filtershow/PanelController$1;-><init>(Lcom/android/gallery3d/filtershow/PanelController;Lcom/android/gallery3d/filtershow/ui/ImageCurves;Lcom/android/gallery3d/filtershow/ui/FramedTextButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method


# virtual methods
.method public addComponent(Landroid/view/View;Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/View;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mPanels:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/PanelController$Panel;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p2}, Lcom/android/gallery3d/filtershow/PanelController$Panel;->addView(Landroid/view/View;)V

    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mViews:Ljava/util/HashMap;

    new-instance v2, Lcom/android/gallery3d/filtershow/PanelController$ViewType;

    sget v3, Lcom/android/gallery3d/filtershow/PanelController;->COMPONENT:I

    invoke-direct {v2, p0, p2, v3}, Lcom/android/gallery3d/filtershow/PanelController$ViewType;-><init>(Lcom/android/gallery3d/filtershow/PanelController;Landroid/view/View;I)V

    invoke-virtual {v1, p2, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public addImageView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mImageViews:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object v0, p1

    check-cast v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0, p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setPanelController(Lcom/android/gallery3d/filtershow/PanelController;)V

    return-void
.end method

.method public addPanel(Landroid/view/View;Landroid/view/View;I)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/View;
    .param p3    # I

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mPanels:Ljava/util/HashMap;

    new-instance v1, Lcom/android/gallery3d/filtershow/PanelController$Panel;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/gallery3d/filtershow/PanelController$Panel;-><init>(Lcom/android/gallery3d/filtershow/PanelController;Landroid/view/View;Landroid/view/View;I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mViews:Ljava/util/HashMap;

    new-instance v1, Lcom/android/gallery3d/filtershow/PanelController$ViewType;

    sget v2, Lcom/android/gallery3d/filtershow/PanelController;->PANEL:I

    invoke-direct {v1, p0, p1, v2}, Lcom/android/gallery3d/filtershow/PanelController$ViewType;-><init>(Lcom/android/gallery3d/filtershow/PanelController;Landroid/view/View;I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addView(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mViews:Ljava/util/HashMap;

    new-instance v1, Lcom/android/gallery3d/filtershow/PanelController$ViewType;

    sget v2, Lcom/android/gallery3d/filtershow/PanelController;->COMPONENT:I

    invoke-direct {v1, p0, p1, v2}, Lcom/android/gallery3d/filtershow/PanelController$ViewType;-><init>(Lcom/android/gallery3d/filtershow/PanelController;Landroid/view/View;I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public ensureFilter(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/PanelController;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->getFilter(Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/PanelController;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;-><init>(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setHistoryName(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->getFilter(Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0195

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c018b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterTinyPlanet;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterTinyPlanet;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0196

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterVignette;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterVignette;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c018d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_4
    if-nez v1, :cond_5

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c018e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterContrast;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterContrast;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0190

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterSaturated;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterSaturated;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0191

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterBwFilter;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterBwFilter;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_7
    if-nez v1, :cond_8

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0193

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_8
    if-nez v1, :cond_9

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c018c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterExposure;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterExposure;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_9
    if-nez v1, :cond_a

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c018f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterVibrance;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterVibrance;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_a
    if-nez v1, :cond_b

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0194

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterShadows;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterShadows;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_b
    if-nez v1, :cond_c

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0197

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterRedEye;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterRedEye;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_c
    if-nez v1, :cond_d

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0192

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    new-instance v3, Lcom/android/gallery3d/filtershow/filters/ImageFilterWBalance;

    invoke-direct {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterWBalance;-><init>()V

    invoke-virtual {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v1

    :cond_d
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v3, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setCurrentFilter(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mSaveAndSetPreset:Z

    return-void
.end method

.method public getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    return-object v0
.end method

.method public haveSavePreset(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mSaveAndSetPreset:Z

    return-void
.end method

.method public onBackPressed()Z
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->selected()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getHistory()Lcom/android/gallery3d/filtershow/HistoryAdapter;

    move-result-object v0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    instance-of v2, v2, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    check-cast v2, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;

    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->showPreGeometry()V

    :cond_2
    iget-boolean v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mSaveAndSetPreset:Z

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->deleteLast()I

    move-result v1

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v2, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->onItemClick(I)V

    :cond_3
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentPanel:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/android/gallery3d/filtershow/PanelController;->showPanel(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->select()V

    iget-boolean v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mDisableFilterButtons:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->enableFilterButtons()V

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->resetHistory()V

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mDisableFilterButtons:Z

    :cond_4
    move v2, v3

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mViews:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/PanelController$ViewType;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/PanelController$ViewType;->type()I

    move-result v1

    sget v2, Lcom/android/gallery3d/filtershow/PanelController;->PANEL:I

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/PanelController;->showPanel(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/PanelController$ViewType;->type()I

    move-result v1

    sget v2, Lcom/android/gallery3d/filtershow/PanelController;->COMPONENT:I

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/PanelController;->showComponent(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onNewValue(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->onNewValue(I)V

    return-void
.end method

.method public resetParameters()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentPanel:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/PanelController;->showPanel(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->resetParameter()V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->select()V

    :cond_0
    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mDisableFilterButtons:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->enableFilterButtons()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mDisableFilterButtons:Z

    :cond_1
    return-void
.end method

.method public setActivity(Lcom/android/gallery3d/filtershow/FilterShowActivity;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/FilterShowActivity;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    return-void
.end method

.method public setCurrentPanel(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/PanelController;->showPanel(Landroid/view/View;)V

    return-void
.end method

.method public setImagePreset(Lcom/android/gallery3d/filtershow/filters/ImageFilter;Ljava/lang/String;)Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .locals 2
    .param p1    # Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/PanelController;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;-><init>(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->add(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V

    invoke-virtual {v0, p2}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setHistoryName(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setIsFx(Z)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v1, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    invoke-virtual {p1, p2}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->setName(Ljava/lang/String;)V

    return-object p1
.end method

.method public setMasterImage(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    return-void
.end method

.method public setRowPanel(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mRowPanel:Landroid/view/View;

    return-void
.end method

.method public setUtilityPanel(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/View;
    .param p4    # Landroid/view/View;
    .param p5    # Landroid/view/View;

    new-instance v0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;-><init>(Lcom/android/gallery3d/filtershow/PanelController;Landroid/content/Context;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    return-void
.end method

.method public showComponent(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    const v10, 0x7f0b002b

    const/4 v9, 0x0

    const v8, 0x7f0b0026

    const/4 v7, 0x1

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->selected()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mPanels:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentPanel:Landroid/view/View;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/filtershow/PanelController$Panel;

    const/4 v5, -0x1

    sget v6, Lcom/android/gallery3d/filtershow/PanelController;->VERTICAL_MOVE:I

    invoke-virtual {v2, v5, v6}, Lcom/android/gallery3d/filtershow/PanelController$Panel;->unselect(II)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->select()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f0b0035

    if-ne v5, v6, :cond_1

    invoke-virtual {p0, v10}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v3

    check-cast v3, Lcom/android/gallery3d/filtershow/ui/ImageCurves;

    check-cast p1, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;

    invoke-direct {p0, v3, p1}, Lcom/android/gallery3d/filtershow/PanelController;->showCurvesPopupMenu(Lcom/android/gallery3d/filtershow/ui/ImageCurves;Lcom/android/gallery3d/filtershow/ui/FramedTextButton;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f0b0034

    if-ne v5, v6, :cond_2

    check-cast p1, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/filtershow/PanelController;->showCropPopupMenu(Lcom/android/gallery3d/filtershow/ui/FramedTextButton;)V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->unselect()V

    :cond_3
    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->hideAspectButtons()V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->hideCurvesButtons()V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_4
    :goto_1
    :pswitch_0
    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->select()V

    goto :goto_0

    :pswitch_1
    const v5, 0x7f0b002e

    invoke-virtual {p0, v5}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c018b

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mDisableFilterButtons:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->disableFilterButtons()V

    iput-boolean v7, p0, Lcom/android/gallery3d/filtershow/PanelController;->mDisableFilterButtons:Z

    goto :goto_1

    :pswitch_2
    const v5, 0x7f0b0027

    invoke-virtual {p0, v5}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0198

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    check-cast v5, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->updateCurrentValue()V

    goto :goto_1

    :pswitch_3
    const v5, 0x7f0b0028

    invoke-virtual {p0, v5}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0199

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v9}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setShowParameter(Z)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    instance-of v5, v5, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    iget-boolean v5, v5, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->firstTimeCropDisplayed:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    check-cast v5, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->applyClear()V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    iput-boolean v9, v5, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->firstTimeCropDisplayed:Z

    :cond_5
    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->showAspectButtons()V

    goto/16 :goto_1

    :pswitch_4
    const v5, 0x7f0b0029

    invoke-virtual {p0, v5}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c019a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    check-cast v5, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->updateCurrentValue()V

    goto/16 :goto_1

    :pswitch_5
    const v5, 0x7f0b002a

    invoke-virtual {p0, v5}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c019b

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v9}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setShowParameter(Z)V

    goto/16 :goto_1

    :pswitch_6
    invoke-virtual {p0, v8}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0196

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_7
    invoke-virtual {p0, v10}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v3

    check-cast v3, Lcom/android/gallery3d/filtershow/ui/ImageCurves;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0195

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/gallery3d/filtershow/ui/ImageCurves;->setFilterName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v9}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setShowParameter(Z)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->showCurvesButtons()V

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_8
    const v5, 0x7f0b002d

    invoke-virtual {p0, v5}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c018d

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_9
    invoke-virtual {p0, v8}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c018e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_a
    invoke-virtual {p0, v8}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0190

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_b
    invoke-virtual {p0, v8}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0191

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_c
    invoke-virtual {p0, v8}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0192

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v9}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setShowParameter(Z)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_d
    invoke-virtual {p0, v8}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0193

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_e
    invoke-virtual {p0, v8}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c018c

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_f
    invoke-virtual {p0, v8}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c018f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_10
    invoke-virtual {p0, v8}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0194

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_11
    invoke-virtual {p0, v8}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v5

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0197

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setEffectName(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/PanelController;->ensureFilter(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_12
    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->showAspectButtons()V

    goto/16 :goto_1

    :pswitch_13
    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v5

    instance-of v5, v5, Lcom/android/gallery3d/filtershow/filters/ImageFilterTinyPlanet;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->saveImage()V

    goto/16 :goto_1

    :cond_6
    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    instance-of v5, v5, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    check-cast v5, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->saveAndSetPreset()V

    :cond_7
    iget-object v5, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentPanel:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/android/gallery3d/filtershow/PanelController;->showPanel(Landroid/view/View;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0034
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_c
        :pswitch_e
        :pswitch_6
        :pswitch_9
        :pswitch_10
        :pswitch_f
        :pswitch_8
        :pswitch_7
        :pswitch_d
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public showCurrentPanel()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->selected()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentPanel:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/PanelController;->showPanel(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->select()V

    goto :goto_0
.end method

.method public showDefaultImageView()V
    .locals 2

    const v0, 0x7f0b0026

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/PanelController;->showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mMasterImage:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setCurrentFilter(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V

    return-void
.end method

.method public showImageView(I)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController;->mImageViews:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, p1, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    move-object v1, v2

    check-cast v1, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public showPanel(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v7, 0x0

    const/4 v9, -0x1

    iput-boolean v7, p0, Lcom/android/gallery3d/filtershow/PanelController;->mSaveAndSetPreset:Z

    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/PanelController;->mPanels:Ljava/util/HashMap;

    iget-object v8, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentPanel:Landroid/view/View;

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/gallery3d/filtershow/PanelController$Panel;

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v7}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->selected()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v7}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->unselect()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v6, 0x1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentPanel:Landroid/view/View;

    if-ne v7, p1, :cond_0

    sget v7, Lcom/android/gallery3d/filtershow/PanelController;->VERTICAL_MOVE:I

    invoke-virtual {v3, v9, v7}, Lcom/android/gallery3d/filtershow/PanelController$Panel;->select(II)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/PanelController;->showDefaultImageView()V

    :cond_0
    iget-object v7, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentPanel:Landroid/view/View;

    if-ne v7, p1, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/android/gallery3d/filtershow/PanelController;->mPanels:Ljava/util/HashMap;

    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/gallery3d/filtershow/PanelController$Panel;

    if-nez v6, :cond_4

    const/4 v4, -0x1

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/PanelController$Panel;->getPosition()I

    move-result v4

    :cond_2
    sget v7, Lcom/android/gallery3d/filtershow/PanelController;->HORIZONTAL_MOVE:I

    invoke-virtual {v5, v4, v7}, Lcom/android/gallery3d/filtershow/PanelController$Panel;->select(II)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    if-eqz v3, :cond_3

    invoke-virtual {v5}, Lcom/android/gallery3d/filtershow/PanelController$Panel;->getPosition()I

    move-result v7

    sget v8, Lcom/android/gallery3d/filtershow/PanelController;->HORIZONTAL_MOVE:I

    invoke-virtual {v3, v7, v8}, Lcom/android/gallery3d/filtershow/PanelController$Panel;->unselect(II)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/PanelController;->showDefaultImageView()V

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/PanelController;->mCurrentPanel:Landroid/view/View;

    goto :goto_0

    :cond_4
    sget v7, Lcom/android/gallery3d/filtershow/PanelController;->VERTICAL_MOVE:I

    invoke-virtual {v5, v9, v7}, Lcom/android/gallery3d/filtershow/PanelController$Panel;->select(II)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_1
.end method

.method public showParameter(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController;->mUtilityPanel:Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setShowParameter(Z)V

    return-void
.end method
