.class public Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;
.super Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;
.source "ImageCrop.java"


# static fields
.field private static final BOTTOM_LEFT:I = 0x9

.field private static final BOTTOM_RIGHT:I = 0xc

.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/ImageCrop"

.field private static final LOGV:Z = false

.field private static final MIN_CROP_WIDTH_HEIGHT:F = 0.1f

.field private static final MOVE_BLOCK:I = 0x10

.field private static final MOVE_BOTTOM:I = 0x8

.field private static final MOVE_LEFT:I = 0x1

.field private static final MOVE_RIGHT:I = 0x4

.field private static final MOVE_TOP:I = 0x2

.field private static final TOP_LEFT:I = 0x3

.field private static final TOP_RIGHT:I = 0x6

.field private static final gPaint:Landroid/graphics/Paint;

.field private static mTouchTolerance:I


# instance fields
.field private final borderPaint:Landroid/graphics/Paint;

.field private final cropIndicator:Landroid/graphics/drawable/Drawable;

.field private final indicatorSize:I

.field private mAspect:Ljava/lang/String;

.field private mAspectHeight:F

.field private mAspectTextSize:I

.field private mAspectWidth:F

.field private final mBorderColor:I

.field private mFirstDraw:Z

.field private mFixAspectRatio:Z

.field private mLastRot:F

.field private movingEdges:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x2d

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mTouchTolerance:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/high16 v3, 0x3f800000

    const/16 v2, 0xff

    invoke-direct {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFirstDraw:Z

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectWidth:F

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectHeight:F

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mLastRot:F

    const/16 v1, 0x80

    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mBorderColor:I

    const-string v1, ""

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspect:Ljava/lang/String;

    const/16 v1, 0x18

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectTextSize:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropIndicator:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0a0080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->indicatorSize:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->borderPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->borderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->borderPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mBorderColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->borderPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/high16 v3, 0x3f800000

    const/16 v2, 0xff

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFirstDraw:Z

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectWidth:F

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectHeight:F

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mLastRot:F

    const/16 v1, 0x80

    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mBorderColor:I

    const-string v1, ""

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspect:Ljava/lang/String;

    const/16 v1, 0x18

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectTextSize:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropIndicator:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0a0080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->indicatorSize:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->borderPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->borderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->borderPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mBorderColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->borderPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method private bitCycleLeft(III)I
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v5, 0x1

    shl-int/2addr v5, p3

    add-int/lit8 v2, v5, -0x1

    and-int v3, p1, v2

    rem-int/2addr p2, p3

    sub-int v5, p3, p2

    shr-int v0, v3, v5

    shl-int v5, v3, p2

    and-int v1, v5, v2

    xor-int/lit8 v5, v2, -0x1

    and-int v4, p1, v5

    or-int/2addr v4, v1

    or-int/2addr v4, v0

    return v4
.end method

.method private cropSetup()V
    .locals 4

    iget-boolean v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getRotatedCropBounds()Landroid/graphics/RectF;

    move-result-object v0

    iget v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectWidth:F

    iget v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectHeight:F

    invoke-static {v0, v2, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->fixAspectRatio(Landroid/graphics/RectF;FF)V

    invoke-direct {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getUnrotatedCropBounds(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setCropBounds(Landroid/graphics/RectF;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalCropBounds()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setCropBounds(Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method private detectMovingEdges(FF)V
    .locals 10
    .param p1    # F
    .param p2    # F

    const/16 v9, 0x10

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropBoundsDisplayed()Landroid/graphics/RectF;

    move-result-object v1

    iput v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    iget v7, v1, Landroid/graphics/RectF;->left:F

    sub-float v7, p1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v7, v1, Landroid/graphics/RectF;->right:F

    sub-float v7, p1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sget v7, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mTouchTolerance:I

    int-to-float v7, v7

    cmpg-float v7, v2, v7

    if-gtz v7, :cond_4

    cmpg-float v7, v2, v3

    if-gez v7, :cond_4

    iget v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    or-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    :cond_0
    :goto_0
    iget v7, v1, Landroid/graphics/RectF;->top:F

    sub-float v7, p2, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v7, v1, Landroid/graphics/RectF;->bottom:F

    sub-float v7, p2, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v7, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mTouchTolerance:I

    int-to-float v7, v7

    cmpg-float v7, v4, v7

    if-gtz v7, :cond_5

    move v7, v5

    :goto_1
    cmpg-float v8, v4, v0

    if-gez v8, :cond_6

    :goto_2
    and-int/2addr v5, v7

    if-eqz v5, :cond_7

    iget v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    :cond_1
    :goto_3
    invoke-virtual {v1, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v5

    if-eqz v5, :cond_2

    iget v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    if-nez v5, :cond_2

    iput v9, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    :cond_2
    iget-boolean v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    if-eqz v5, :cond_3

    iget v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    if-eq v5, v9, :cond_3

    iget v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    invoke-direct {p0, v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->fixEdgeToCorner(I)I

    move-result v5

    iput v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    :cond_4
    sget v7, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mTouchTolerance:I

    int-to-float v7, v7

    cmpg-float v7, v3, v7

    if-gtz v7, :cond_0

    iget v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    or-int/lit8 v7, v7, 0x4

    iput v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    goto :goto_0

    :cond_5
    move v7, v6

    goto :goto_1

    :cond_6
    move v5, v6

    goto :goto_2

    :cond_7
    sget v5, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mTouchTolerance:I

    int-to-float v5, v5

    cmpg-float v5, v0, v5

    if-gtz v5, :cond_1

    iget v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    goto :goto_3
.end method

.method private drawIndicator(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/drawable/Drawable;
    .param p3    # F
    .param p4    # F

    float-to-int v2, p3

    iget v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->indicatorSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v0, v2, v3

    float-to-int v2, p4

    iget v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->indicatorSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v1, v2, v3

    iget v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->indicatorSize:I

    add-int/2addr v2, v0

    iget v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->indicatorSize:I

    add-int/2addr v3, v1

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private drawRuleOfThird(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    .locals 13
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/RectF;
    .param p3    # Landroid/graphics/Paint;

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/high16 v2, 0x40400000

    div-float v10, v0, v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v0

    const/high16 v2, 0x40400000

    div-float v11, v0, v2

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float v1, v0, v10

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float v12, v0, v11

    const/4 v8, 0x0

    :goto_0
    const/4 v0, 0x2

    if-ge v8, v0, :cond_0

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    move-object v0, p1

    move v3, v1

    move-object/from16 v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-float/2addr v1, v10

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    move v4, v12

    :goto_1
    const/4 v0, 0x2

    if-ge v9, v0, :cond_1

    iget v3, p2, Landroid/graphics/RectF;->left:F

    iget v5, p2, Landroid/graphics/RectF;->right:F

    move-object v2, p1

    move v6, v4

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-float/2addr v4, v11

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private fixEdgeToCorner(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    or-int/lit8 p1, p1, 0x2

    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    or-int/lit8 p1, p1, 0x1

    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    or-int/lit8 p1, p1, 0x8

    :cond_2
    const/16 v0, 0x8

    if-ne p1, v0, :cond_3

    or-int/lit8 p1, p1, 0x4

    :cond_3
    return p1
.end method

.method private fixedCornerResize(Landroid/graphics/RectF;IFF)Landroid/graphics/RectF;
    .locals 6
    .param p1    # Landroid/graphics/RectF;
    .param p2    # I
    .param p3    # F
    .param p4    # F

    const/4 v0, 0x0

    const/16 v1, 0xc

    if-ne p2, v1, :cond_1

    new-instance v0, Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    iget v3, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v4

    add-float/2addr v3, v4

    add-float/2addr v3, p3

    iget v4, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/16 v1, 0x9

    if-ne p2, v1, :cond_2

    new-instance v0, Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float/2addr v1, v2

    add-float/2addr v1, p3

    iget v2, p1, Landroid/graphics/RectF;->top:F

    iget v3, p1, Landroid/graphics/RectF;->right:F

    iget v4, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x3

    if-ne p2, v1, :cond_3

    new-instance v0, Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float/2addr v1, v2

    add-float/2addr v1, p3

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    add-float/2addr v2, p4

    iget v3, p1, Landroid/graphics/RectF;->right:F

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x6

    if-ne p2, v1, :cond_0

    new-instance v0, Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    add-float/2addr v2, p4

    iget v3, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v4

    add-float/2addr v3, v4

    add-float/2addr v3, p3

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0
.end method

.method private getRotatedCropBounds()Landroid/graphics/RectF;
    .locals 5

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalCropBounds()Landroid/graphics/RectF;

    move-result-object v0

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v3

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropRotationMatrix(FLandroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto :goto_0
.end method

.method private getRotatedStraightenBounds()Landroid/graphics/RectF;
    .locals 4

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalStraighten()F

    move-result v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getUntranslatedStraightenCropBounds(Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v2

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropRotationMatrix(FLandroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto :goto_0
.end method

.method private getScaledMinWidthHeight()F
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v0, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const v3, 0x3dcccccd

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0, v3, v4}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->computeScale(FF)F

    move-result v3

    div-float v1, v2, v3

    return v1
.end method

.method private getUnrotatedCropBounds(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 6
    .param p1    # Landroid/graphics/RectF;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v4

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropRotationMatrix(FLandroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v1

    if-nez v1, :cond_1

    move-object v0, v3

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v4

    if-nez v4, :cond_2

    move-object v0, v3

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    move-result v4

    if-nez v4, :cond_0

    move-object v0, v3

    goto :goto_0
.end method

.method private moveEdges(FF)V
    .locals 26
    .param p1    # F
    .param p2    # F

    invoke-direct/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getRotatedCropBounds()Landroid/graphics/RectF;

    move-result-object v7

    invoke-direct/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getScaledMinWidthHeight()F

    move-result v17

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->computeScale(FF)F

    move-result v19

    div-float v8, p1, v19

    div-float v9, p2, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    move/from16 v23, v0

    if-eqz v23, :cond_3

    const/16 v23, 0x10

    move/from16 v0, v20

    move/from16 v1, v23

    if-eq v0, v1, :cond_3

    const/16 v23, 0x1

    move/from16 v0, v20

    move/from16 v1, v23

    if-ne v0, v1, :cond_0

    or-int/lit8 v20, v20, 0x2

    :cond_0
    const/16 v23, 0x2

    move/from16 v0, v20

    move/from16 v1, v23

    if-ne v0, v1, :cond_1

    or-int/lit8 v20, v20, 0x1

    :cond_1
    const/16 v23, 0x4

    move/from16 v0, v20

    move/from16 v1, v23

    if-ne v0, v1, :cond_2

    or-int/lit8 v20, v20, 0x8

    :cond_2
    const/16 v23, 0x8

    move/from16 v0, v20

    move/from16 v1, v23

    if-ne v0, v1, :cond_3

    or-int/lit8 v20, v20, 0x4

    :cond_3
    const/16 v23, 0x10

    move/from16 v0, v20

    move/from16 v1, v23

    if-ne v0, v1, :cond_a

    invoke-direct/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getRotatedStraightenBounds()Landroid/graphics/RectF;

    move-result-object v22

    const/16 v23, 0x0

    cmpl-float v23, v8, v23

    if-lez v23, :cond_8

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v23, v0

    iget v0, v7, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    invoke-static {v0, v8}, Ljava/lang/Math;->min(FF)F

    move-result v8

    :goto_0
    const/16 v23, 0x0

    cmpl-float v23, v9, v23

    if-lez v23, :cond_9

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v23, v0

    iget v0, v7, Landroid/graphics/RectF;->bottom:F

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    invoke-static {v0, v9}, Ljava/lang/Math;->min(FF)F

    move-result v9

    :goto_1
    invoke-virtual {v7, v8, v9}, Landroid/graphics/RectF;->offset(FF)V

    :cond_4
    :goto_2
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    invoke-virtual/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v23

    invoke-virtual/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v24

    move-object/from16 v0, p0

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropRotationMatrix(FLandroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v15

    new-instance v16, Landroid/graphics/Matrix;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual/range {v15 .. v16}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v23

    if-nez v23, :cond_5

    :cond_5
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    move-result v23

    if-nez v23, :cond_6

    :cond_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setCropBounds(Landroid/graphics/RectF;)V

    :cond_7
    :goto_3
    return-void

    :cond_8
    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v23, v0

    iget v0, v7, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    invoke-static {v0, v8}, Ljava/lang/Math;->max(FF)F

    move-result v8

    goto :goto_0

    :cond_9
    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    iget v0, v7, Landroid/graphics/RectF;->top:F

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    invoke-static {v0, v9}, Ljava/lang/Math;->max(FF)F

    move-result v9

    goto :goto_1

    :cond_a
    const/4 v11, 0x0

    const/4 v12, 0x0

    and-int/lit8 v23, v20, 0x1

    if-eqz v23, :cond_b

    iget v0, v7, Landroid/graphics/RectF;->left:F

    move/from16 v23, v0

    add-float v23, v23, v8

    iget v0, v7, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    sub-float v24, v24, v17

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->min(FF)F

    move-result v23

    iget v0, v7, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    sub-float v11, v23, v24

    :cond_b
    and-int/lit8 v23, v20, 0x2

    if-eqz v23, :cond_c

    iget v0, v7, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    add-float v23, v23, v9

    iget v0, v7, Landroid/graphics/RectF;->bottom:F

    move/from16 v24, v0

    sub-float v24, v24, v17

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->min(FF)F

    move-result v23

    iget v0, v7, Landroid/graphics/RectF;->top:F

    move/from16 v24, v0

    sub-float v12, v23, v24

    :cond_c
    and-int/lit8 v23, v20, 0x4

    if-eqz v23, :cond_d

    iget v0, v7, Landroid/graphics/RectF;->right:F

    move/from16 v23, v0

    add-float v23, v23, v8

    iget v0, v7, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    add-float v24, v24, v17

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->max(FF)F

    move-result v23

    iget v0, v7, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    sub-float v11, v23, v24

    :cond_d
    and-int/lit8 v23, v20, 0x8

    if-eqz v23, :cond_e

    iget v0, v7, Landroid/graphics/RectF;->bottom:F

    move/from16 v23, v0

    add-float v23, v23, v9

    iget v0, v7, Landroid/graphics/RectF;->top:F

    move/from16 v24, v0

    add-float v24, v24, v17

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->max(FF)F

    move-result v23

    iget v0, v7, Landroid/graphics/RectF;->bottom:F

    move/from16 v24, v0

    sub-float v12, v23, v24

    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    move/from16 v23, v0

    if-eqz v23, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropBoundsDisplayed()Landroid/graphics/RectF;

    move-result-object v6

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v13, v0, [F

    const/16 v23, 0x0

    iget v0, v6, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    aput v24, v13, v23

    const/16 v23, 0x1

    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    move/from16 v24, v0

    aput v24, v13, v23

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v14, v0, [F

    const/16 v23, 0x0

    iget v0, v6, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    aput v24, v14, v23

    const/16 v23, 0x1

    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v24, v0

    aput v24, v14, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    move/from16 v23, v0

    const/16 v24, 0x3

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    move/from16 v23, v0

    const/16 v24, 0xc

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_10

    :cond_f
    const/16 v23, 0x1

    iget v0, v6, Landroid/graphics/RectF;->top:F

    move/from16 v24, v0

    aput v24, v13, v23

    const/16 v23, 0x1

    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    move/from16 v24, v0

    aput v24, v14, v23

    :cond_10
    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v4, v0, [F

    const/16 v23, 0x0

    const/16 v24, 0x0

    aget v24, v13, v24

    const/16 v25, 0x0

    aget v25, v14, v25

    sub-float v24, v24, v25

    aput v24, v4, v23

    const/16 v23, 0x1

    const/16 v24, 0x1

    aget v24, v13, v24

    const/16 v25, 0x1

    aget v25, v14, v25

    sub-float v24, v24, v25

    aput v24, v4, v23

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v10, v0, [F

    const/16 v23, 0x0

    aput v11, v10, v23

    const/16 v23, 0x1

    aput v12, v10, v23

    invoke-static {v4}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMath;->normalize([F)[F

    move-result-object v5

    invoke-static {v10, v5}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMath;->scalarProjection([F[F)F

    move-result v21

    const/16 v23, 0x0

    aget v23, v5, v23

    mul-float v11, v21, v23

    const/16 v23, 0x1

    aget v23, v5, v23

    mul-float v12, v21, v23

    mul-float v23, v11, v19

    mul-float v24, v12, v19

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v0, v6, v1, v2, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->fixedCornerResize(Landroid/graphics/RectF;IFF)Landroid/graphics/RectF;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropBoundDisplayMatrix()Landroid/graphics/Matrix;

    move-result-object v15

    new-instance v16, Landroid/graphics/Matrix;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual/range {v15 .. v16}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v23

    if-eqz v23, :cond_7

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    move-result v23

    if-eqz v23, :cond_7

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setCropBounds(Landroid/graphics/RectF;)V

    goto/16 :goto_3

    :cond_11
    and-int/lit8 v23, v20, 0x1

    if-eqz v23, :cond_12

    iget v0, v7, Landroid/graphics/RectF;->left:F

    move/from16 v23, v0

    add-float v23, v23, v11

    move/from16 v0, v23

    iput v0, v7, Landroid/graphics/RectF;->left:F

    :cond_12
    and-int/lit8 v23, v20, 0x2

    if-eqz v23, :cond_13

    iget v0, v7, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    add-float v23, v23, v12

    move/from16 v0, v23

    iput v0, v7, Landroid/graphics/RectF;->top:F

    :cond_13
    and-int/lit8 v23, v20, 0x4

    if-eqz v23, :cond_14

    iget v0, v7, Landroid/graphics/RectF;->right:F

    move/from16 v23, v0

    add-float v23, v23, v11

    move/from16 v0, v23

    iput v0, v7, Landroid/graphics/RectF;->right:F

    :cond_14
    and-int/lit8 v23, v20, 0x8

    if-eqz v23, :cond_4

    iget v0, v7, Landroid/graphics/RectF;->bottom:F

    move/from16 v23, v0

    add-float v23, v23, v12

    move/from16 v0, v23

    iput v0, v7, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_2
.end method

.method public static setTouchTolerance(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mTouchTolerance:I

    return-void
.end method

.method private swapAspect()V
    .locals 2

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectWidth:F

    iget v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectHeight:F

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectWidth:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectHeight:F

    return-void
.end method

.method private switchCropBounds(ILandroid/graphics/RectF;)Z
    .locals 13
    .param p1    # I
    .param p2    # Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropBoundsDisplayed()Landroid/graphics/RectF;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    and-int/lit8 v8, p1, 0x4

    if-eqz v8, :cond_5

    iget v8, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCurrentX:F

    iget v9, v0, Landroid/graphics/RectF;->right:F

    sub-float v1, v8, v9

    :cond_0
    :goto_0
    and-int/lit8 v8, p1, 0x8

    if-eqz v8, :cond_6

    iget v8, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCurrentY:F

    iget v9, v0, Landroid/graphics/RectF;->bottom:F

    sub-float v3, v8, v9

    :cond_1
    :goto_1
    const/4 v7, 0x0

    const/16 v8, 0xc

    if-ne p1, v8, :cond_7

    new-instance v7, Landroid/graphics/RectF;

    iget v8, v0, Landroid/graphics/RectF;->left:F

    iget v9, v0, Landroid/graphics/RectF;->top:F

    iget v10, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v11

    add-float/2addr v10, v11

    iget v11, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v12

    add-float/2addr v11, v12

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    :cond_2
    :goto_2
    and-int/lit8 v8, p1, 0x4

    if-eqz v8, :cond_a

    iget v8, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCurrentX:F

    iget v9, v7, Landroid/graphics/RectF;->right:F

    sub-float v2, v8, v9

    :cond_3
    :goto_3
    and-int/lit8 v8, p1, 0x8

    if-eqz v8, :cond_b

    iget v8, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCurrentY:F

    iget v9, v7, Landroid/graphics/RectF;->bottom:F

    sub-float v4, v8, v9

    :cond_4
    :goto_4
    mul-float v8, v1, v1

    mul-float v9, v3, v3

    add-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    mul-float v10, v2, v2

    mul-float v11, v4, v4

    add-float/2addr v10, v11

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    cmpl-double v8, v8, v10

    if-lez v8, :cond_e

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropBoundDisplayMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v8

    if-nez v8, :cond_c

    const/4 v8, 0x0

    :goto_5
    return v8

    :cond_5
    and-int/lit8 v8, p1, 0x1

    if-eqz v8, :cond_0

    iget v8, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCurrentX:F

    iget v9, v0, Landroid/graphics/RectF;->left:F

    sub-float v1, v8, v9

    goto :goto_0

    :cond_6
    and-int/lit8 v8, p1, 0x2

    if-eqz v8, :cond_1

    iget v8, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCurrentY:F

    iget v9, v0, Landroid/graphics/RectF;->top:F

    sub-float v3, v8, v9

    goto :goto_1

    :cond_7
    const/16 v8, 0x9

    if-ne p1, v8, :cond_8

    new-instance v7, Landroid/graphics/RectF;

    iget v8, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v9

    sub-float/2addr v8, v9

    iget v9, v0, Landroid/graphics/RectF;->top:F

    iget v10, v0, Landroid/graphics/RectF;->right:F

    iget v11, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v12

    add-float/2addr v11, v12

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_2

    :cond_8
    const/4 v8, 0x3

    if-ne p1, v8, :cond_9

    new-instance v7, Landroid/graphics/RectF;

    iget v8, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v9

    sub-float/2addr v8, v9

    iget v9, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v10

    sub-float/2addr v9, v10

    iget v10, v0, Landroid/graphics/RectF;->right:F

    iget v11, v0, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto/16 :goto_2

    :cond_9
    const/4 v8, 0x6

    if-ne p1, v8, :cond_2

    new-instance v7, Landroid/graphics/RectF;

    iget v8, v0, Landroid/graphics/RectF;->left:F

    iget v9, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v10

    sub-float/2addr v9, v10

    iget v10, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v11

    add-float/2addr v10, v11

    iget v11, v0, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto/16 :goto_2

    :cond_a
    and-int/lit8 v8, p1, 0x1

    if-eqz v8, :cond_3

    iget v8, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCurrentX:F

    iget v9, v7, Landroid/graphics/RectF;->left:F

    sub-float v2, v8, v9

    goto/16 :goto_3

    :cond_b
    and-int/lit8 v8, p1, 0x2

    if-eqz v8, :cond_4

    iget v8, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCurrentY:F

    iget v9, v7, Landroid/graphics/RectF;->top:F

    sub-float v4, v8, v9

    goto/16 :goto_4

    :cond_c
    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    move-result v8

    if-nez v8, :cond_d

    const/4 v8, 0x0

    goto/16 :goto_5

    :cond_d
    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->swapAspect()V

    invoke-virtual {p2, v7}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    const/4 v8, 0x1

    goto/16 :goto_5

    :cond_e
    const/4 v8, 0x0

    goto/16 :goto_5
.end method


# virtual methods
.method public apply(FF)V
    .locals 2
    .param p1    # F
    .param p2    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    iput p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectWidth:F

    iput p2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectHeight:F

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalStraighten()F

    move-result v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getUntranslatedStraightenCropBounds(Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setLocalCropBounds(Landroid/graphics/RectF;)V

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropSetup()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->saveAndSetPreset()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public applyClear()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalStraighten()F

    move-result v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getUntranslatedStraightenCropBounds(Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setLocalCropBounds(Landroid/graphics/RectF;)V

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropSetup()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->saveAndSetPreset()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public applyOriginal()V
    .locals 5

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v2

    div-float v4, v3, v2

    iput v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectWidth:F

    div-float v4, v0, v2

    iput v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectHeight:F

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalStraighten()F

    move-result v4

    invoke-static {v1, v4}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getUntranslatedStraightenCropBounds(Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setLocalCropBounds(Landroid/graphics/RectF;)V

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropSetup()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->saveAndSetPreset()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method protected decoder(IF)I
    .locals 3
    .param p1    # I
    .param p2    # F

    const/4 v2, 0x4

    invoke-virtual {p0, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->constrainedRotation(F)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return p1

    :sswitch_0
    const/4 v1, 0x3

    invoke-direct {p0, p1, v1, v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->bitCycleLeft(III)I

    move-result p1

    goto :goto_0

    :sswitch_1
    const/4 v1, 0x2

    invoke-direct {p0, p1, v1, v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->bitCycleLeft(III)I

    move-result p1

    goto :goto_0

    :sswitch_2
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1, v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->bitCycleLeft(III)I

    move-result p1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch
.end method

.method protected drawShape(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 27
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/Bitmap;

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setDither(Z)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    const/16 v4, 0xff

    const/16 v5, 0xff

    const/16 v8, 0xff

    const/16 v26, 0xff

    move/from16 v0, v26

    invoke-virtual {v3, v4, v5, v8, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFirstDraw:Z

    if-eqz v3, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropSetup()V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFirstDraw:Z

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v20

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->drawTransformed(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)Landroid/graphics/RectF;

    move-result-object v13

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mBorderColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40400000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->drawRuleOfThird(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    if-eqz v3, :cond_3

    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v24

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v17

    mul-float v3, v24, v24

    mul-float v4, v17, v17

    add-float/2addr v3, v4

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-float v0, v3

    move/from16 v16, v0

    const/high16 v14, 0x41a00000

    div-float v3, v16, v14

    float-to-int v0, v3

    move/from16 v19, v0

    const/4 v3, 0x2

    new-array v0, v3, [F

    move-object/from16 v22, v0

    const/4 v3, 0x0

    iget v4, v13, Landroid/graphics/RectF;->left:F

    aput v4, v22, v3

    const/4 v3, 0x1

    iget v4, v13, Landroid/graphics/RectF;->top:F

    aput v4, v22, v3

    const/4 v3, 0x0

    aget v3, v22, v3

    const/high16 v4, 0x40000000

    div-float v4, v24, v4

    add-float v10, v3, v4

    const/4 v3, 0x1

    aget v3, v22, v3

    const/high16 v4, 0x40000000

    div-float v4, v17, v4

    add-float/2addr v3, v4

    const/high16 v4, 0x40a00000

    add-float v11, v3, v4

    const/4 v3, 0x2

    new-array v9, v3, [F

    const/4 v3, 0x0

    iget v4, v13, Landroid/graphics/RectF;->right:F

    aput v4, v9, v3

    const/4 v3, 0x1

    iget v4, v13, Landroid/graphics/RectF;->bottom:F

    aput v4, v9, v3

    move-object/from16 v0, v22

    invoke-static {v0, v9}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMath;->getUnitVectorFromPoints([F[F)[F

    move-result-object v23

    move-object/from16 v12, v22

    const/16 v25, 0x0

    :goto_0
    move/from16 v0, v25

    move/from16 v1, v19

    if-ge v0, v1, :cond_2

    const/4 v3, 0x0

    aget v3, v12, v3

    const/4 v4, 0x0

    aget v4, v23, v4

    mul-float/2addr v4, v14

    add-float v6, v3, v4

    const/4 v3, 0x1

    aget v3, v12, v3

    const/4 v4, 0x1

    aget v4, v23, v4

    mul-float/2addr v4, v14

    add-float v7, v3, v4

    rem-int/lit8 v3, v25, 0x2

    if-nez v3, :cond_1

    div-int/lit8 v3, v19, 0x2

    sub-int v3, v25, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/4 v4, 0x2

    if-le v3, v4, :cond_1

    const/4 v3, 0x0

    aget v4, v12, v3

    const/4 v3, 0x1

    aget v5, v12, v3

    sget-object v8, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_1
    const/4 v3, 0x0

    aput v6, v12, v3

    const/4 v3, 0x1

    aput v7, v12, v3

    add-int/lit8 v25, v25, 0x1

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectTextSize:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspect:Ljava/lang/String;

    sget-object v4, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10, v11, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_3
    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mBorderColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40400000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v3, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->gPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->drawStraighten(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->decoder(IF)I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCenterX:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCenterY:F

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->unrotatedCropBounds()Landroid/graphics/RectF;

    move-result-object v21

    if-nez v15, :cond_c

    const/16 v18, 0x1

    :goto_1
    and-int/lit8 v3, v15, 0x2

    if-nez v3, :cond_4

    if-eqz v18, :cond_5

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    move-object/from16 v0, v21

    iget v5, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->drawIndicator(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V

    :cond_5
    and-int/lit8 v3, v15, 0x8

    if-nez v3, :cond_6

    if-eqz v18, :cond_7

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropIndicator:Landroid/graphics/drawable/Drawable;

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    move-object/from16 v0, v21

    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->drawIndicator(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V

    :cond_7
    and-int/lit8 v3, v15, 0x1

    if-nez v3, :cond_8

    if-eqz v18, :cond_9

    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropIndicator:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, v21

    iget v4, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->drawIndicator(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V

    :cond_9
    and-int/lit8 v3, v15, 0x4

    if-nez v3, :cond_a

    if-eqz v18, :cond_b

    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropIndicator:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, v21

    iget v4, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->drawIndicator(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V

    :cond_b
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_c
    const/16 v18, 0x0

    goto :goto_1
.end method

.method protected gainedVisibility()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v0

    iget v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mLastRot:F

    sub-float v1, v0, v1

    const/high16 v2, 0x42b40000

    div-float/2addr v1, v2

    float-to-int v1, v1

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->swapAspect()V

    :cond_0
    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropSetup()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFirstDraw:Z

    return-void
.end method

.method protected getCropBoundDisplayMatrix()Landroid/graphics/Matrix;
    .locals 4

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v2

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropRotationMatrix(FLandroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0, v2, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->computeScale(FF)F

    move-result v1

    iget v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mXOffset:F

    iget v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mYOffset:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCenterX:F

    iget v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCenterY:F

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    return-object v0
.end method

.method protected getCropBoundsDisplayed()Landroid/graphics/RectF;
    .locals 6

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalCropBounds()Landroid/graphics/RectF;

    move-result-object v0

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v4

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getCropRotationMatrix(FLandroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    :goto_0
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0, v4, v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->computeScale(FF)F

    move-result v3

    iget v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCenterX:F

    iget v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCenterY:F

    invoke-virtual {v2, v3, v3, v4, v5}, Landroid/graphics/Matrix;->setScale(FFFF)V

    iget v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mXOffset:F

    iget v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mYOffset:F

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    return-object v1

    :cond_0
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto :goto_0
.end method

.method protected getCropRotationMatrix(FLandroid/graphics/RectF;)Landroid/graphics/Matrix;
    .locals 3
    .param p1    # F
    .param p2    # Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalGeoFlipMatrix(FF)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {v0}, Landroid/graphics/Matrix;->rectStaysRect()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c0199

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public imageLoaded()V
    .locals 0

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->imageLoaded()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->syncLocalToMasterGeometry()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->applyClear()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method protected lostVisibility()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mLastRot:F

    return-void
.end method

.method public resetParameter()V
    .locals 0

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->resetParameter()V

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->cropSetup()V

    return-void
.end method

.method protected setActionDown(FF)V
    .locals 0
    .param p1    # F
    .param p2    # F

    invoke-super {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setActionDown(FF)V

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->detectMovingEdges(FF)V

    return-void
.end method

.method protected setActionMove(FF)V
    .locals 2
    .param p1    # F
    .param p2    # F

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCurrentX:F

    sub-float v0, p1, v0

    iget v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->mCurrentY:F

    sub-float v1, p2, v1

    invoke-direct {p0, v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->moveEdges(FF)V

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setActionMove(FF)V

    return-void
.end method

.method protected setActionUp()V
    .locals 1

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setActionUp()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->movingEdges:I

    return-void
.end method

.method public setAspectString(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspect:Ljava/lang/String;

    return-void
.end method

.method public setAspectTextSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectTextSize:I

    return-void
.end method

.method public setCropBounds(Landroid/graphics/RectF;)V
    .locals 14
    .param p1    # Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getScaledMinWidthHeight()F

    move-result v3

    iget v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectWidth:F

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mAspectHeight:F

    iget-boolean v10, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    if-eqz v10, :cond_0

    mul-float v10, v1, v0

    div-float/2addr v3, v10

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalRotation()F

    move-result v10

    const/high16 v11, 0x42b40000

    div-float/2addr v10, v11

    float-to-int v7, v10

    rem-int/lit8 v10, v7, 0x2

    if-eqz v10, :cond_0

    move v9, v1

    move v1, v0

    move v0, v9

    :cond_0
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v4

    iget-boolean v10, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    if-eqz v10, :cond_6

    mul-float v10, v3, v1

    cmpg-float v10, v5, v10

    if-ltz v10, :cond_1

    mul-float v10, v3, v0

    cmpg-float v10, v4, v10

    if-gez v10, :cond_2

    :cond_1
    mul-float v5, v3, v1

    mul-float v4, v3, v0

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v10

    cmpg-float v10, v10, v3

    if-gez v10, :cond_3

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v5

    :cond_3
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v10

    cmpg-float v10, v10, v3

    if-gez v10, :cond_4

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v4

    :cond_4
    iget v10, v2, Landroid/graphics/RectF;->left:F

    iget v11, v2, Landroid/graphics/RectF;->top:F

    iget v12, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v12, v5

    iget v13, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v13, v4

    invoke-virtual {v2, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalPhotoBounds()Landroid/graphics/RectF;

    move-result-object v10

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->getLocalStraighten()F

    move-result v11

    invoke-static {v10, v11}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->getUntranslatedStraightenCropBounds(Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    iget-boolean v10, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->mFixAspectRatio:Z

    if-eqz v10, :cond_5

    invoke-static {v2, v1, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->fixAspectRatio(Landroid/graphics/RectF;FF)V

    :cond_5
    invoke-virtual {p0, v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageGeometry;->setLocalCropBounds(Landroid/graphics/RectF;)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    :cond_6
    cmpg-float v10, v5, v3

    if-gez v10, :cond_7

    move v5, v3

    :cond_7
    cmpg-float v10, v4, v3

    if-gez v10, :cond_2

    move v4, v3

    goto :goto_0
.end method
