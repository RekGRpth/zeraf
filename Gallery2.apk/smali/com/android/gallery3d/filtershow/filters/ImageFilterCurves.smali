.class public Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;
.super Lcom/android/gallery3d/filtershow/filters/ImageFilter;
.source "ImageFilterCurves.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/ImageFilterCurves"


# instance fields
.field private final mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/gallery3d/filtershow/ui/Spline;

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    const-string v0, "Curves"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->reset()V

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 12
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/ui/Spline;->isOriginal()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x100

    new-array v4, v0, [I

    const/4 v0, 0x0

    invoke-virtual {p0, v4, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->populateArray([II)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->nativeApplyGradientFilter(Landroid/graphics/Bitmap;II[I[I[I)V

    :cond_0
    const/4 v9, 0x0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/ui/Spline;->isOriginal()Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x100

    new-array v9, v0, [I

    const/4 v0, 0x1

    invoke-virtual {p0, v9, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->populateArray([II)V

    :cond_1
    const/4 v10, 0x0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/ui/Spline;->isOriginal()Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x100

    new-array v10, v0, [I

    const/4 v0, 0x2

    invoke-virtual {p0, v10, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->populateArray([II)V

    :cond_2
    const/4 v11, 0x0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/ui/Spline;->isOriginal()Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x100

    new-array v11, v0, [I

    const/4 v0, 0x3

    invoke-virtual {p0, v11, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->populateArray([II)V

    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    move-object v5, p0

    move-object v6, p1

    invoke-virtual/range {v5 .. v11}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->nativeApplyGradientFilter(Landroid/graphics/Bitmap;II[I[I[I)V

    return-object p1
.end method

.method public clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x4

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    aget-object v2, v2, v1

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    aget-object v2, v2, v1

    invoke-virtual {v0, v2, v1}, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->setSpline(Lcom/android/gallery3d/filtershow/ui/Spline;I)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    return-object v0
.end method

.method public getSpline(I)Lcom/android/gallery3d/filtershow/ui/Spline;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public isNil()Z
    .locals 2

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/ui/Spline;->isOriginal()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public populateArray([II)V
    .locals 5
    .param p1    # [I
    .param p2    # I

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    aget-object v2, v3, p2

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/ui/Spline;->getAppliedCurve()[F

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    const/16 v3, 0x100

    if-ge v1, v3, :cond_0

    aget v3, v0, v1

    const/high16 v4, 0x437f0000

    mul-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, p1, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    new-instance v1, Lcom/android/gallery3d/filtershow/ui/Spline;

    invoke-direct {v1}, Lcom/android/gallery3d/filtershow/ui/Spline;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/android/gallery3d/filtershow/ui/Spline;->addPoint(FF)I

    invoke-virtual {v1, v3, v2}, Lcom/android/gallery3d/filtershow/ui/Spline;->addPoint(FF)I

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    new-instance v3, Lcom/android/gallery3d/filtershow/ui/Spline;

    invoke-direct {v3, v1}, Lcom/android/gallery3d/filtershow/ui/Spline;-><init>(Lcom/android/gallery3d/filtershow/ui/Spline;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public same(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)Z
    .locals 6
    .param p1    # Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->same(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;

    const/4 v1, 0x0

    :goto_1
    const/4 v4, 0x4

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    aget-object v4, v4, v1

    iget-object v5, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    aget-object v5, v5, v1

    if-ne v4, v5, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public setSpline(Lcom/android/gallery3d/filtershow/ui/Spline;I)V
    .locals 2
    .param p1    # Lcom/android/gallery3d/filtershow/ui/Spline;
    .param p2    # I

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterCurves;->mSplines:[Lcom/android/gallery3d/filtershow/ui/Spline;

    new-instance v1, Lcom/android/gallery3d/filtershow/ui/Spline;

    invoke-direct {v1, p1}, Lcom/android/gallery3d/filtershow/ui/Spline;-><init>(Lcom/android/gallery3d/filtershow/ui/Spline;)V

    aput-object v1, v0, p2

    return-void
.end method
