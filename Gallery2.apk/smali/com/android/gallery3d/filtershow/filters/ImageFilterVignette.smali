.class public Lcom/android/gallery3d/filtershow/filters/ImageFilterVignette;
.super Lcom/android/gallery3d/filtershow/filters/ImageFilter;
.source "ImageFilterVignette.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->setFilterType(B)V

    const-string v0, "Vignette"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 5
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v4, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mParameter:I

    int-to-float v1, v4

    const/high16 v4, 0x42c80000

    div-float v2, v1, v4

    invoke-virtual {p0, p1, v3, v0, v2}, Lcom/android/gallery3d/filtershow/filters/ImageFilterVignette;->nativeApplyFilter(Landroid/graphics/Bitmap;IIF)V

    return-object p1
.end method

.method protected native nativeApplyFilter(Landroid/graphics/Bitmap;IIF)V
.end method
