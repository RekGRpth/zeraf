.class public Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;
.super Lcom/android/gallery3d/filtershow/filters/ImageFilter;
.source "ImageFilterHue.java"


# instance fields
.field private cmatrix:Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;->cmatrix:Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    const-string v0, "Hue"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mName:Ljava/lang/String;

    new-instance v0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;->cmatrix:Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    const/16 v0, 0xb4

    iput v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mMaxParameter:I

    const/16 v0, -0xb4

    iput v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mMinParameter:I

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 5
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v4, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mParameter:I

    int-to-float v1, v4

    move v2, v1

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;->cmatrix:Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    invoke-virtual {v4}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->identity()V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;->cmatrix:Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    invoke-virtual {v4, v2}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->setHue(F)V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;->cmatrix:Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    invoke-virtual {v4}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->getMatrix()[F

    move-result-object v4

    invoke-virtual {p0, p1, v3, v0, v4}, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;->nativeApplyFilter(Landroid/graphics/Bitmap;II[F)V

    return-object p1
.end method

.method public clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;

    new-instance v1, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;->cmatrix:Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    invoke-direct {v1, v2}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;-><init>(Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;)V

    iput-object v1, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;->cmatrix:Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterHue;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    return-object v0
.end method

.method protected native nativeApplyFilter(Landroid/graphics/Bitmap;II[F)V
.end method
