.class public Lcom/android/gallery3d/filtershow/HistoryAdapter;
.super Landroid/widget/ArrayAdapter;
.source "HistoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/gallery3d/filtershow/presets/ImagePreset;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/HistoryAdapter"


# instance fields
.field private mBorders:Ljava/lang/String;

.field private mCrop:Ljava/lang/String;

.field private mCurrentPresetPosition:I

.field private mMirror:Ljava/lang/String;

.field private mRedoMenuItem:Landroid/view/MenuItem;

.field private mResetMenuItem:Landroid/view/MenuItem;

.field private mRotate:Ljava/lang/String;

.field private mStraighten:Ljava/lang/String;

.field private mUndoMenuItem:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mBorders:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCrop:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mRotate:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mStraighten:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mMirror:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mUndoMenuItem:Landroid/view/MenuItem;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mRedoMenuItem:Landroid/view/MenuItem;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mResetMenuItem:Landroid/view/MenuItem;

    move-object v0, p1

    check-cast v0, Lcom/android/gallery3d/filtershow/FilterShowActivity;

    const v1, 0x7f0c0171

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mBorders:Ljava/lang/String;

    const v1, 0x7f0c0199

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCrop:Ljava/lang/String;

    const v1, 0x7f0c019a

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mRotate:Ljava/lang/String;

    const v1, 0x7f0c0198

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mStraighten:Ljava/lang/String;

    const v1, 0x7f0c019b

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mMirror:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addHistoryItem(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->canAddHistoryItem(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->insert(Lcom/android/gallery3d/filtershow/presets/ImagePreset;I)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->updateMenuItems()V

    :cond_0
    return-void
.end method

.method public canAddHistoryItem(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)Z
    .locals 2
    .param p1    # Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->getLast()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->same(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->getLast()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->historyName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->historyName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public canRedo()Z
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public canReset()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    if-gt v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public canUndo()Z
    .locals 2

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public deleteLast()I
    .locals 2

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    :cond_1
    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    goto :goto_0
.end method

.method public getLast()Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .locals 2

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    invoke-virtual {p0, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v9, 0x7f0200ec

    const v8, 0x7f0200ed

    move-object v5, p2

    if-nez v5, :cond_0

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v6, 0x7f040019

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    if-eqz v1, :cond_2

    const v6, 0x7f0b0059

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->historyName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const v6, 0x7f0b0058

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iget v6, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    if-ne p1, v6, :cond_3

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    const v6, 0x7f0b005a

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne p1, v6, :cond_4

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    :goto_1
    return-object v5

    :cond_3
    const/4 v6, 0x4

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->historyName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mBorders:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    const v6, 0x7f0200ea

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_5
    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->historyName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mStraighten:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_6
    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->historyName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCrop:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_7
    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->historyName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mRotate:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_8
    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->historyName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mMirror:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_9
    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->isFx()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_a
    const v6, 0x7f0200eb

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public insert(Lcom/android/gallery3d/filtershow/presets/ImagePreset;I)V
    .locals 3
    .param p1    # Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .param p2    # I

    iget v2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    if-eqz v2, :cond_2

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    :goto_0
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->clear()V

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iput p2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->canAddHistoryItem(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_2
    return-void

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    iput p2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto :goto_2
.end method

.method public bridge synthetic insert(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    check-cast p1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p0, p1, p2}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->insert(Lcom/android/gallery3d/filtershow/presets/ImagePreset;I)V

    return-void
.end method

.method public redo()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->updateMenuItems()V

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    return v0
.end method

.method public reset()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->addHistoryItem(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->updateMenuItems()V

    goto :goto_0
.end method

.method public setCurrentPreset(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->updateMenuItems()V

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setMenuItems(Landroid/view/MenuItem;Landroid/view/MenuItem;Landroid/view/MenuItem;)V
    .locals 0
    .param p1    # Landroid/view/MenuItem;
    .param p2    # Landroid/view/MenuItem;
    .param p3    # Landroid/view/MenuItem;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mUndoMenuItem:Landroid/view/MenuItem;

    iput-object p2, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mRedoMenuItem:Landroid/view/MenuItem;

    iput-object p3, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mResetMenuItem:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->updateMenuItems()V

    return-void
.end method

.method public undo()I
    .locals 2

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->updateMenuItems()V

    iget v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mCurrentPresetPosition:I

    return v0
.end method

.method public updateMenuItems()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mUndoMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mUndoMenuItem:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->canUndo()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mRedoMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mRedoMenuItem:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->canRedo()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mResetMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/HistoryAdapter;->mResetMenuItem:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->canReset()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_2
    return-void
.end method
