.class public Lcom/android/gallery3d/filtershow/presets/ImagePresetOld;
.super Lcom/android/gallery3d/filtershow/presets/ImagePreset;
.source "ImagePresetOld.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;-><init>()V

    return-void
.end method


# virtual methods
.method public name()Ljava/lang/String;
    .locals 1

    const-string v0, "Old"

    return-object v0
.end method

.method public setup()V
    .locals 5

    new-instance v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterGradient;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterGradient;-><init>()V

    const/high16 v1, -0x1000000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/filtershow/filters/ImageFilterGradient;->addColor(IF)V

    const/16 v1, 0xff

    const/16 v2, 0xe4

    const/16 v3, 0xe7

    const/16 v4, 0xc1

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    const/high16 v2, 0x3f800000

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/filtershow/filters/ImageFilterGradient;->addColor(IF)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mFilters:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method
