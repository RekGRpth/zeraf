.class public Lcom/android/gallery3d/filtershow/ui/Spline;
.super Ljava/lang/Object;
.source "Spline.java"


# static fields
.field public static final BLUE:I = 0x3

.field public static final GREEN:I = 0x2

.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/Spline"

.field public static final RED:I = 0x1

.field public static final RGB:I

.field private static mCurveHandle:Landroid/graphics/drawable/Drawable;

.field private static mCurveHandleSize:I

.field private static mCurveWidth:I


# instance fields
.field private final gPaint:Landroid/graphics/Paint;

.field private mCurrentControlPoint:Lcom/android/gallery3d/filtershow/ui/ControlPoint;

.field private final mPoints:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/android/gallery3d/filtershow/ui/ControlPoint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurrentControlPoint:Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    return-void
.end method

.method public constructor <init>(Lcom/android/gallery3d/filtershow/ui/Spline;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/filtershow/ui/Spline;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurrentControlPoint:Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    iput-object v3, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p1, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget-object v3, p1, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    new-instance v1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    invoke-direct {v1, v2}, Lcom/android/gallery3d/filtershow/ui/ControlPoint;-><init>(Lcom/android/gallery3d/filtershow/ui/ControlPoint;)V

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget-object v3, p1, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurrentControlPoint:Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    if-ne v3, v2, :cond_0

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurrentControlPoint:Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-void
.end method

.method public static colorForCurve(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/high16 v0, -0x10000

    goto :goto_0

    :pswitch_1
    const v0, -0xff0100

    goto :goto_0

    :pswitch_2
    const v0, -0xffff01

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static curveHandleSize()I
    .locals 1

    sget v0, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandleSize:I

    return v0
.end method

.method private didMovePoint(Lcom/android/gallery3d/filtershow/ui/ControlPoint;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurrentControlPoint:Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    return-void
.end method

.method private drawGrid(Landroid/graphics/Canvas;FF)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # F
    .param p3    # F

    const/high16 v7, 0x40400000

    const/16 v6, 0xc8

    const/16 v4, 0x96

    const/16 v3, 0x64

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    const/16 v2, 0x80

    invoke-virtual {v0, v2, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/high16 v0, 0x41100000

    div-float v9, p3, v0

    const/high16 v0, 0x41100000

    div-float v10, p2, v0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v0, v2, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, p3

    move v3, p2

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    const/16 v2, 0x80

    invoke-virtual {v0, v2, v6, v6, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40800000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    div-float v9, p3, v7

    div-float v10, p2, v7

    const/4 v8, 0x1

    :goto_0
    const/4 v0, 0x3

    if-ge v8, v0, :cond_0

    int-to-float v0, v8

    mul-float v2, v0, v9

    int-to-float v0, v8

    mul-float v4, v0, v9

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    int-to-float v0, v8

    mul-float v3, v0, v10

    int-to-float v0, v8

    mul-float v5, v0, v10

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    move v6, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v3, v1

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v3, p2

    move v4, v1

    move v5, p2

    move v6, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v3, p2

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->gPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, p3

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawHandles(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/drawable/Drawable;
    .param p3    # F
    .param p4    # F

    float-to-int v2, p3

    sget v3, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandleSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v0, v2, v3

    float-to-int v2, p4

    sget v3, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandleSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v1, v2, v3

    sget v2, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandleSize:I

    add-int/2addr v2, v0

    sget v3, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandleSize:I

    add-int/2addr v3, v1

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public static setCurveHandle(Landroid/graphics/drawable/Drawable;I)V
    .locals 0
    .param p0    # Landroid/graphics/drawable/Drawable;
    .param p1    # I

    sput-object p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandle:Landroid/graphics/drawable/Drawable;

    sput p1, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandleSize:I

    return-void
.end method

.method public static setCurveWidth(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveWidth:I

    return-void
.end method


# virtual methods
.method public addPoint(FF)I
    .locals 1
    .param p1    # F
    .param p2    # F

    new-instance v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    invoke-direct {v0, p1, p2}, Lcom/android/gallery3d/filtershow/ui/ControlPoint;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/ui/Spline;->addPoint(Lcom/android/gallery3d/filtershow/ui/ControlPoint;)I

    move-result v0

    return v0
.end method

.method public addPoint(Lcom/android/gallery3d/filtershow/ui/ControlPoint;)I
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public copy()Lcom/android/gallery3d/filtershow/ui/Spline;
    .locals 4

    new-instance v2, Lcom/android/gallery3d/filtershow/ui/Spline;

    invoke-direct {v2}, Lcom/android/gallery3d/filtershow/ui/Spline;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->copy()Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/filtershow/ui/Spline;->addPoint(Lcom/android/gallery3d/filtershow/ui/ControlPoint;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public deletePoint(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;IIIZZ)V
    .locals 54
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z
    .param p6    # Z

    sget v4, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandleSize:I

    sub-int v4, p3, v4

    int-to-float v0, v4

    move/from16 v41, v0

    sget v4, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandleSize:I

    sub-int v4, p4, v4

    int-to-float v8, v4

    sget v4, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandleSize:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v0, v4

    move/from16 v26, v0

    sget v4, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandleSize:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v0, v4

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    new-array v0, v4, [Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    move-object/from16 v32, v0

    const/16 v28, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    move/from16 v0, v28

    if-ge v0, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    move/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    new-instance v4, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    move-object/from16 v0, v30

    iget v7, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    mul-float v7, v7, v41

    move-object/from16 v0, v30

    iget v10, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    mul-float/2addr v10, v8

    invoke-direct {v4, v7, v10}, Lcom/android/gallery3d/filtershow/ui/ControlPoint;-><init>(FF)V

    aput-object v4, v32, v28

    add-int/lit8 v28, v28, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/ui/Spline;->solveSystem([Lcom/android/gallery3d/filtershow/ui/ControlPoint;)[D

    move-result-object v25

    new-instance v31, Landroid/graphics/Path;

    invoke-direct/range {v31 .. v31}, Landroid/graphics/Path;-><init>()V

    const/4 v4, 0x0

    const/4 v7, 0x0

    aget-object v7, v32, v7

    iget v7, v7, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v7}, Landroid/graphics/Path;->moveTo(FF)V

    const/16 v28, 0x0

    :goto_1
    move-object/from16 v0, v32

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v28

    if-ge v0, v4, :cond_4

    aget-object v4, v32, v28

    iget v4, v4, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    float-to-double v0, v4

    move-wide/from16 v44, v0

    add-int/lit8 v4, v28, 0x1

    aget-object v4, v32, v4

    iget v4, v4, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    float-to-double v0, v4

    move-wide/from16 v46, v0

    aget-object v4, v32, v28

    iget v4, v4, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    float-to-double v0, v4

    move-wide/from16 v50, v0

    add-int/lit8 v4, v28, 0x1

    aget-object v4, v32, v4

    iget v4, v4, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    float-to-double v0, v4

    move-wide/from16 v52, v0

    move-wide/from16 v42, v44

    :goto_2
    cmpg-double v4, v42, v46

    if-gez v4, :cond_3

    sub-double v21, v46, v44

    mul-double v23, v21, v21

    sub-double v10, v42, v44

    div-double v18, v10, v21

    const-wide/high16 v10, 0x3ff0000000000000L

    sub-double v16, v10, v18

    mul-double v33, v16, v50

    mul-double v35, v18, v52

    mul-double v10, v16, v16

    mul-double v10, v10, v16

    sub-double v10, v10, v16

    aget-wide v12, v25, v28

    mul-double v37, v10, v12

    mul-double v10, v18, v18

    mul-double v10, v10, v18

    sub-double v10, v10, v18

    add-int/lit8 v4, v28, 0x1

    aget-wide v12, v25, v4

    mul-double v39, v10, v12

    add-double v10, v33, v35

    const-wide/high16 v12, 0x4018000000000000L

    div-double v12, v23, v12

    add-double v14, v37, v39

    mul-double/2addr v12, v14

    add-double v48, v10, v12

    float-to-double v10, v8

    cmpl-double v4, v48, v10

    if-lez v4, :cond_1

    float-to-double v0, v8

    move-wide/from16 v48, v0

    :cond_1
    const-wide/16 v10, 0x0

    cmpg-double v4, v48, v10

    if-gez v4, :cond_2

    const-wide/16 v48, 0x0

    :cond_2
    move-wide/from16 v0, v42

    double-to-float v4, v0

    move-wide/from16 v0, v48

    double-to-float v7, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    const-wide/high16 v10, 0x4034000000000000L

    add-double v42, v42, v10

    goto :goto_2

    :cond_3
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_1

    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v41

    invoke-direct {v0, v1, v2, v8}, Lcom/android/gallery3d/filtershow/ui/Spline;->drawGrid(Landroid/graphics/Canvas;FF)V

    move-object/from16 v0, v32

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v29, v32, v4

    move-object/from16 v0, v29

    iget v4, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move-object/from16 v0, v29

    iget v7, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v7}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, v29

    iget v4, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move-object/from16 v0, v31

    move/from16 v1, v41

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Path;->lineTo(FF)V

    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/4 v4, 0x1

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    const/4 v4, 0x1

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setDither(Z)V

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget v20, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveWidth:I

    if-eqz p5, :cond_5

    move/from16 v0, v20

    int-to-double v10, v0

    const-wide/high16 v12, 0x3ff8000000000000L

    mul-double/2addr v10, v12

    double-to-int v0, v10

    move/from16 v20, v0

    :cond_5
    add-int/lit8 v4, v20, 0x2

    int-to-float v4, v4

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/high16 v4, -0x1000000

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1, v9}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    if-eqz p6, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurrentControlPoint:Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurrentControlPoint:Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget v4, v4, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    mul-float v5, v4, v41

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurrentControlPoint:Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget v4, v4, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    mul-float v6, v4, v8

    const/high16 v4, 0x40400000

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/high16 v4, -0x1000000

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v4, p1

    move v7, v5

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const/4 v11, 0x0

    move-object/from16 v10, p1

    move v12, v6

    move v13, v5

    move v14, v6

    move-object v15, v9

    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const/high16 v4, 0x3f800000

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v4, p1

    move v7, v5

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const/4 v11, 0x0

    move-object/from16 v10, p1

    move v12, v6

    move v13, v5

    move v14, v6

    move-object v15, v9

    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_6
    move/from16 v0, v20

    int-to-float v4, v0

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1, v9}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    if-eqz p5, :cond_7

    const/16 v28, 0x0

    :goto_3
    move-object/from16 v0, v32

    array-length v4, v0

    move/from16 v0, v28

    if-ge v0, v4, :cond_7

    aget-object v4, v32, v28

    iget v0, v4, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v42, v0

    aget-object v4, v32, v28

    iget v0, v4, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v48, v0

    sget-object v4, Lcom/android/gallery3d/filtershow/ui/Spline;->mCurveHandle:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v42

    move/from16 v3, v48

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/android/gallery3d/filtershow/ui/Spline;->drawHandles(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FF)V

    add-int/lit8 v28, v28, 0x1

    goto :goto_3

    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public getAppliedCurve()[F
    .locals 47

    const/16 v41, 0x100

    move/from16 v0, v41

    new-array v7, v0, [F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Ljava/util/Vector;->size()I

    move-result v41

    move/from16 v0, v41

    new-array v0, v0, [Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    move-object/from16 v19, v0

    const/4 v14, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Ljava/util/Vector;->size()I

    move-result v41

    move/from16 v0, v41

    if-ge v14, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    invoke-virtual {v0, v14}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    new-instance v41, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v42, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v43, v0

    invoke-direct/range {v41 .. v43}, Lcom/android/gallery3d/filtershow/ui/ControlPoint;-><init>(FF)V

    aput-object v41, v19, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/ui/Spline;->solveSystem([Lcom/android/gallery3d/filtershow/ui/ControlPoint;)[D

    move-result-object v12

    const/16 v20, 0x0

    const/16 v13, 0x100

    const/16 v41, 0x0

    aget-object v41, v19, v41

    move-object/from16 v0, v41

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v41, v0

    const/16 v42, 0x0

    cmpl-float v41, v41, v42

    if-eqz v41, :cond_1

    const/16 v41, 0x0

    aget-object v41, v19, v41

    move-object/from16 v0, v41

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v41, v0

    const/high16 v42, 0x43800000

    mul-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v0, v0

    move/from16 v20, v0

    :cond_1
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v41, v0

    add-int/lit8 v41, v41, -0x1

    aget-object v41, v19, v41

    move-object/from16 v0, v41

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v41, v0

    const/high16 v42, 0x3f800000

    cmpl-float v41, v41, v42

    if-eqz v41, :cond_2

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v41, v0

    add-int/lit8 v41, v41, -0x1

    aget-object v41, v19, v41

    move-object/from16 v0, v41

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v41, v0

    const/high16 v42, 0x43800000

    mul-float v41, v41, v42

    move/from16 v0, v41

    float-to-int v13, v0

    :cond_2
    const/4 v14, 0x0

    :goto_1
    move/from16 v0, v20

    if-ge v14, v0, :cond_3

    const/high16 v41, 0x3f800000

    const/16 v42, 0x0

    aget-object v42, v19, v42

    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v42, v0

    sub-float v41, v41, v42

    aput v41, v7, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_3
    move v14, v13

    :goto_2
    const/16 v41, 0x100

    move/from16 v0, v41

    if-ge v14, v0, :cond_4

    const/high16 v41, 0x3f800000

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v42, v0

    add-int/lit8 v42, v42, -0x1

    aget-object v42, v19, v42

    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v42, v0

    sub-float v41, v41, v42

    aput v41, v7, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_4
    move/from16 v14, v20

    :goto_3
    if-ge v14, v13, :cond_a

    const/4 v6, 0x0

    const/16 v16, 0x0

    int-to-double v0, v14

    move-wide/from16 v41, v0

    const-wide/high16 v43, 0x4070000000000000L

    div-double v29, v41, v43

    const/16 v18, 0x0

    const/4 v15, 0x0

    :goto_4
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v41, v0

    add-int/lit8 v41, v41, -0x1

    move/from16 v0, v41

    if-ge v15, v0, :cond_6

    aget-object v41, v19, v15

    move-object/from16 v0, v41

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v41, v0

    cmpl-double v41, v29, v41

    if-ltz v41, :cond_5

    add-int/lit8 v41, v15, 0x1

    aget-object v41, v19, v41

    move-object/from16 v0, v41

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v41, v0

    cmpg-double v41, v29, v41

    if-gtz v41, :cond_5

    move/from16 v18, v15

    :cond_5
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    :cond_6
    aget-object v6, v19, v18

    add-int/lit8 v41, v18, 0x1

    aget-object v16, v19, v41

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v41, v0

    cmpg-double v41, v29, v41

    if-gtz v41, :cond_9

    iget v0, v6, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v31, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v33, v0

    iget v0, v6, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v37, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v41, v0

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v39, v0

    sub-double v8, v33, v31

    mul-double v10, v8, v8

    sub-double v41, v29, v31

    div-double v4, v41, v8

    const-wide/high16 v41, 0x3ff0000000000000L

    sub-double v2, v41, v4

    mul-double v21, v2, v37

    mul-double v23, v4, v39

    mul-double v41, v2, v2

    mul-double v41, v41, v2

    sub-double v41, v41, v2

    aget-wide v43, v12, v18

    mul-double v25, v41, v43

    mul-double v41, v4, v4

    mul-double v41, v41, v4

    sub-double v41, v41, v4

    add-int/lit8 v43, v18, 0x1

    aget-wide v43, v12, v43

    mul-double v27, v41, v43

    add-double v41, v21, v23

    const-wide/high16 v43, 0x4018000000000000L

    div-double v43, v10, v43

    add-double v45, v25, v27

    mul-double v43, v43, v45

    add-double v35, v41, v43

    const-wide/high16 v41, 0x3ff0000000000000L

    cmpl-double v41, v35, v41

    if-lez v41, :cond_7

    const-wide/high16 v35, 0x3ff0000000000000L

    :cond_7
    const-wide/16 v41, 0x0

    cmpg-double v41, v35, v41

    if-gez v41, :cond_8

    const-wide/16 v35, 0x0

    :cond_8
    const-wide/high16 v41, 0x3ff0000000000000L

    sub-double v41, v41, v35

    move-wide/from16 v0, v41

    double-to-float v0, v0

    move/from16 v41, v0

    aput v41, v7, v14

    :goto_5
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_3

    :cond_9
    const/high16 v41, 0x3f800000

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v42, v0

    sub-float v41, v41, v42

    aput v41, v7, v14

    goto :goto_5

    :cond_a
    return-object v7
.end method

.method public getNbPoints()I
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getPoint(I)Lcom/android/gallery3d/filtershow/ui/ControlPoint;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    return-object v0
.end method

.method public isOriginal()Z
    .locals 6

    const/high16 v5, 0x3f800000

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/ui/Spline;->getNbPoints()I

    move-result v0

    const/4 v3, 0x2

    if-le v0, v3, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    cmpl-float v0, v0, v5

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_4

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public isPointContained(FI)Z
    .locals 4
    .param p1    # F
    .param p2    # I

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget v3, v1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    cmpl-float v3, v3, p1

    if-lez v3, :cond_1

    :cond_0
    :goto_1
    return v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, p2, 0x1

    :goto_2
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget v3, v1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    cmpg-float v3, v3, p1

    if-ltz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public movePoint(IFF)V
    .locals 2
    .param p1    # I
    .param p2    # F
    .param p3    # F

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iput p2, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    iput p3, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    invoke-direct {p0, v0}, Lcom/android/gallery3d/filtershow/ui/Spline;->didMovePoint(Lcom/android/gallery3d/filtershow/ui/ControlPoint;)V

    goto :goto_0
.end method

.method public show()V
    .locals 5

    const-string v2, "Gallery2/Spline"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "show curve "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/ui/Spline;->mPoints:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    const-string v2, "Gallery2/Spline"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "point "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method solveSystem([Lcom/android/gallery3d/filtershow/ui/ControlPoint;)[D
    .locals 29
    .param p1    # [Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v19, v0

    const/16 v23, 0x3

    move/from16 v0, v19

    move/from16 v1, v23

    filled-new-array {v0, v1}, [I

    move-result-object v23

    sget-object v24, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, [[D

    move/from16 v0, v19

    new-array v0, v0, [D

    move-object/from16 v20, v0

    move/from16 v0, v19

    new-array v0, v0, [D

    move-object/from16 v21, v0

    const/16 v23, 0x0

    aget-object v23, v22, v23

    const/16 v24, 0x1

    const-wide/high16 v25, 0x3ff0000000000000L

    aput-wide v25, v23, v24

    add-int/lit8 v23, v19, -0x1

    aget-object v23, v22, v23

    const/16 v24, 0x1

    const-wide/high16 v25, 0x3ff0000000000000L

    aput-wide v25, v23, v24

    const-wide v4, 0x3fc5555555555555L

    const-wide v2, 0x3fd5555555555555L

    const/16 v16, 0x1

    :goto_0
    add-int/lit8 v23, v19, -0x1

    move/from16 v0, v16

    move/from16 v1, v23

    if-ge v0, v1, :cond_0

    aget-object v23, p1, v16

    move-object/from16 v0, v23

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v23, v0

    add-int/lit8 v24, v16, -0x1

    aget-object v24, p1, v24

    move-object/from16 v0, v24

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    float-to-double v10, v0

    add-int/lit8 v23, v16, 0x1

    aget-object v23, p1, v23

    move-object/from16 v0, v23

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v23, v0

    add-int/lit8 v24, v16, -0x1

    aget-object v24, p1, v24

    move-object/from16 v0, v24

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    float-to-double v14, v0

    add-int/lit8 v23, v16, 0x1

    aget-object v23, p1, v23

    move-object/from16 v0, v23

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v23, v0

    aget-object v24, p1, v16

    move-object/from16 v0, v24

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    float-to-double v6, v0

    add-int/lit8 v23, v16, 0x1

    aget-object v23, p1, v23

    move-object/from16 v0, v23

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v23, v0

    aget-object v24, p1, v16

    move-object/from16 v0, v24

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    float-to-double v8, v0

    aget-object v23, p1, v16

    move-object/from16 v0, v23

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v23, v0

    add-int/lit8 v24, v16, -0x1

    aget-object v24, p1, v24

    move-object/from16 v0, v24

    iget v0, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    float-to-double v12, v0

    aget-object v23, v22, v16

    const/16 v24, 0x0

    mul-double v25, v4, v10

    aput-wide v25, v23, v24

    aget-object v23, v22, v16

    const/16 v24, 0x1

    mul-double v25, v2, v14

    aput-wide v25, v23, v24

    aget-object v23, v22, v16

    const/16 v24, 0x2

    mul-double v25, v4, v6

    aput-wide v25, v23, v24

    div-double v23, v8, v6

    div-double v25, v12, v10

    sub-double v23, v23, v25

    aput-wide v23, v20, v16

    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    :cond_0
    const/16 v16, 0x1

    :goto_1
    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_1

    aget-object v23, v22, v16

    const/16 v24, 0x0

    aget-wide v23, v23, v24

    add-int/lit8 v25, v16, -0x1

    aget-object v25, v22, v25

    const/16 v26, 0x1

    aget-wide v25, v25, v26

    div-double v17, v23, v25

    aget-object v23, v22, v16

    const/16 v24, 0x1

    aget-object v25, v22, v16

    const/16 v26, 0x1

    aget-wide v25, v25, v26

    add-int/lit8 v27, v16, -0x1

    aget-object v27, v22, v27

    const/16 v28, 0x2

    aget-wide v27, v27, v28

    mul-double v27, v27, v17

    sub-double v25, v25, v27

    aput-wide v25, v23, v24

    aget-wide v23, v20, v16

    add-int/lit8 v25, v16, -0x1

    aget-wide v25, v20, v25

    mul-double v25, v25, v17

    sub-double v23, v23, v25

    aput-wide v23, v20, v16

    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v23, v19, -0x1

    add-int/lit8 v24, v19, -0x1

    aget-wide v24, v20, v24

    add-int/lit8 v26, v19, -0x1

    aget-object v26, v22, v26

    const/16 v27, 0x1

    aget-wide v26, v26, v27

    div-double v24, v24, v26

    aput-wide v24, v21, v23

    add-int/lit8 v16, v19, -0x2

    :goto_2
    if-ltz v16, :cond_2

    aget-wide v23, v20, v16

    aget-object v25, v22, v16

    const/16 v26, 0x2

    aget-wide v25, v25, v26

    add-int/lit8 v27, v16, 0x1

    aget-wide v27, v21, v27

    mul-double v25, v25, v27

    sub-double v23, v23, v25

    aget-object v25, v22, v16

    const/16 v26, 0x1

    aget-wide v25, v25, v26

    div-double v23, v23, v25

    aput-wide v23, v21, v16

    add-int/lit8 v16, v16, -0x1

    goto :goto_2

    :cond_2
    return-object v21
.end method
