.class public Lcom/android/gallery3d/filtershow/ui/FramedTextButton;
.super Landroid/widget/ImageButton;
.source "FramedTextButton.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/FramedTextButton"

.field private static gPaint:Landroid/graphics/Paint;

.field private static gPath:Landroid/graphics/Path;

.field private static mTextPadding:I

.field private static mTextSize:I

.field private static mTrianglePadding:I

.field private static mTriangleSize:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x18

    sput v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextSize:I

    const/16 v0, 0x14

    sput v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    sput-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPath:Landroid/graphics/Path;

    const/4 v0, 0x2

    sput v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTrianglePadding:I

    const/16 v0, 0x1e

    sput v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTriangleSize:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mText:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/android/gallery3d/R$styleable;->ImageButtonTitle:[I

    invoke-virtual {v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mText:Ljava/lang/String;

    return-void
.end method

.method public static setTextPadding(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    return-void
.end method

.method public static setTextSize(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextSize:I

    return-void
.end method

.method public static setTrianglePadding(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTrianglePadding:I

    return-void
.end method

.method public static setTriangleSize(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTriangleSize:I

    return-void
.end method


# virtual methods
.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v13, 0x40000000

    const/16 v12, 0xff

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    const/16 v1, 0x60

    invoke-virtual {v0, v1, v12, v12, v12}, Landroid/graphics/Paint;->setARGB(IIII)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v13}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v9

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v7

    sget v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    int-to-float v1, v0

    sget v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    int-to-float v2, v0

    sget v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    sub-int v0, v9, v0

    int-to-float v3, v0

    sget v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    sub-int v0, v7, v0

    int-to-float v4, v0

    sget-object v5, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPath:Landroid/graphics/Path;

    sget v1, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    sub-int v1, v9, v1

    sget v2, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTrianglePadding:I

    sub-int/2addr v1, v2

    sget v2, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTriangleSize:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    sget v2, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    sub-int v2, v7, v2

    sget v3, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTrianglePadding:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPath:Landroid/graphics/Path;

    sget v1, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    sub-int v1, v9, v1

    sget v2, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTrianglePadding:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    sget v2, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    sub-int v2, v7, v2

    sget v3, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTrianglePadding:I

    sub-int/2addr v2, v3

    sget v3, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTriangleSize:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPath:Landroid/graphics/Path;

    sget v1, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    sub-int v1, v9, v1

    sget v2, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTrianglePadding:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    sget v2, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextPadding:I

    sub-int v2, v7, v2

    sget v3, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTrianglePadding:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    const/16 v1, 0x80

    invoke-virtual {v0, v1, v12, v12, v12}, Landroid/graphics/Paint;->setARGB(IIII)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPath:Landroid/graphics/Path;

    sget-object v1, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mText:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v12, v12, v12, v12}, Landroid/graphics/Paint;->setARGB(IIII)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    sget v1, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    sget-object v0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mText:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    int-to-float v0, v9

    sub-float/2addr v0, v8

    div-float/2addr v0, v13

    float-to-int v10, v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v7

    div-int/lit8 v11, v0, 0x2

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mText:Ljava/lang/String;

    int-to-float v1, v10

    int-to-float v2, v11

    sget-object v3, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->gPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mText:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setTextFrom(I)V
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mContext:Landroid/content/Context;

    const v1, 0x7f0c019d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mContext:Landroid/content/Context;

    const v1, 0x7f0c019e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mContext:Landroid/content/Context;

    const v1, 0x7f0c019f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->mContext:Landroid/content/Context;

    const v1, 0x7f0c01a0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0162
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
