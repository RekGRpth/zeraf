.class Lcom/android/gallery3d/gadget/WidgetTypeChooser$1;
.super Ljava/lang/Object;
.source "WidgetTypeChooser.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/gadget/WidgetTypeChooser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/gallery3d/gadget/WidgetTypeChooser;


# direct methods
.method constructor <init>(Lcom/android/gallery3d/gadget/WidgetTypeChooser;)V
    .locals 0

    iput-object p1, p0, Lcom/android/gallery3d/gadget/WidgetTypeChooser$1;->this$0:Lcom/android/gallery3d/gadget/WidgetTypeChooser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3
    .param p1    # Landroid/widget/RadioGroup;
    .param p2    # I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "widget-type"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/gadget/WidgetTypeChooser$1;->this$0:Lcom/android/gallery3d/gadget/WidgetTypeChooser;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object v1, p0, Lcom/android/gallery3d/gadget/WidgetTypeChooser$1;->this$0:Lcom/android/gallery3d/gadget/WidgetTypeChooser;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    return-void
.end method
