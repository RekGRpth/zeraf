.class public Lcom/android/gallery3d/ui/UndoBarView;
.super Lcom/android/gallery3d/ui/GLView;
.source "UndoBarView.java"


# static fields
.field private static ANIM_TIME:J = 0x0L

.field private static final GRAY:I = -0x555556

.field private static final NO_ANIMATION:J = -0x1L

.field private static final TAG:Ljava/lang/String; = "Gallery2/UndoBarView"

.field private static final WHITE:I = -0x1


# instance fields
.field private mAlpha:F

.field private mAnimationStartTime:J

.field private final mBarHeight:I

.field private final mBarMargin:I

.field private final mClickRegion:I

.field private final mDeletedText:Lcom/android/gallery3d/ui/StringTexture;

.field private final mDeletedTextMargin:I

.field private mDownOnButton:Z

.field private mFromAlpha:F

.field private final mIconMargin:I

.field private final mIconSize:I

.field private mOnClickListener:Lcom/android/gallery3d/ui/GLView$OnClickListener;

.field private final mPanel:Lcom/android/gallery3d/ui/NinePatchTexture;

.field private final mSeparatorBottomMargin:I

.field private final mSeparatorRightMargin:I

.field private final mSeparatorTopMargin:I

.field private final mSeparatorWidth:I

.field private mToAlpha:F

.field private final mUndoIcon:Lcom/android/gallery3d/ui/ResourceTexture;

.field private final mUndoText:Lcom/android/gallery3d/ui/StringTexture;

.field private final mUndoTextMargin:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0xc8

    sput-wide v0, Lcom/android/gallery3d/ui/UndoBarView;->ANIM_TIME:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const/16 v3, 0xc

    const/16 v2, 0xa

    const/4 v5, 0x1

    const/16 v4, 0x10

    invoke-direct {p0}, Lcom/android/gallery3d/ui/GLView;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAnimationStartTime:J

    const/16 v0, 0x30

    invoke-static {v0}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarHeight:I

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarMargin:I

    invoke-static {v4}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mUndoTextMargin:I

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mIconMargin:I

    const/16 v0, 0x20

    invoke-static {v0}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mIconSize:I

    invoke-static {v3}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorRightMargin:I

    invoke-static {v2}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorTopMargin:I

    invoke-static {v2}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorBottomMargin:I

    invoke-static {v5}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorWidth:I

    invoke-static {v4}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mDeletedTextMargin:I

    new-instance v0, Lcom/android/gallery3d/ui/NinePatchTexture;

    const v1, 0x7f02013f

    invoke-direct {v0, p1, v1}, Lcom/android/gallery3d/ui/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mPanel:Lcom/android/gallery3d/ui/NinePatchTexture;

    const v0, 0x7f0c0259

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v1

    int-to-float v1, v1

    const v2, -0x555556

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, v5}, Lcom/android/gallery3d/ui/StringTexture;->newInstance(Ljava/lang/String;FIFZ)Lcom/android/gallery3d/ui/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mUndoText:Lcom/android/gallery3d/ui/StringTexture;

    const v0, 0x7f0c0258

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4}, Lcom/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/android/gallery3d/ui/StringTexture;->newInstance(Ljava/lang/String;FI)Lcom/android/gallery3d/ui/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mDeletedText:Lcom/android/gallery3d/ui/StringTexture;

    new-instance v0, Lcom/android/gallery3d/ui/ResourceTexture;

    const v1, 0x7f0200c8

    invoke-direct {v0, p1, v1}, Lcom/android/gallery3d/ui/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mUndoIcon:Lcom/android/gallery3d/ui/ResourceTexture;

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarMargin:I

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mUndoTextMargin:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mUndoText:Lcom/android/gallery3d/ui/StringTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/UploadedTexture;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mIconMargin:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mIconSize:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorRightMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mClickRegion:I

    return-void
.end method

.method private advanceAnimation()V
    .locals 8

    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    iget-wide v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAnimationStartTime:J

    cmp-long v1, v1, v6

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAnimationStartTime:J

    sub-long/2addr v1, v3

    long-to-float v1, v1

    sget-wide v2, Lcom/android/gallery3d/ui/UndoBarView;->ANIM_TIME:J

    long-to-float v2, v2

    div-float v0, v1, v2

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mFromAlpha:F

    iget v2, p0, Lcom/android/gallery3d/ui/UndoBarView;->mToAlpha:F

    iget v3, p0, Lcom/android/gallery3d/ui/UndoBarView;->mFromAlpha:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    :goto_1
    add-float/2addr v1, v0

    iput v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAlpha:F

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAlpha:F

    const/high16 v2, 0x3f800000

    invoke-static {v1, v5, v2}, Lcom/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    iput v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAlpha:F

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAlpha:F

    iget v2, p0, Lcom/android/gallery3d/ui/UndoBarView;->mToAlpha:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    iput-wide v6, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAnimationStartTime:J

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAlpha:F

    cmpl-float v1, v1, v5

    if-nez v1, :cond_1

    const/4 v1, 0x1

    invoke-super {p0, v1}, Lcom/android/gallery3d/ui/GLView;->setVisibility(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/gallery3d/ui/GLView;->invalidate()V

    goto :goto_0

    :cond_2
    neg-float v0, v0

    goto :goto_1
.end method

.method private static getTargetAlpha(I)F
    .locals 1
    .param p0    # I

    if-nez p0, :cond_0

    const/high16 v0, 0x3f800000

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private inUndoButton(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/GLView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/GLView;->getHeight()I

    move-result v0

    iget v4, p0, Lcom/android/gallery3d/ui/UndoBarView;->mClickRegion:I

    sub-int v4, v1, v4

    int-to-float v4, v4

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_0

    int-to-float v4, v1

    cmpg-float v4, v2, v4

    if-gez v4, :cond_0

    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_0

    int-to-float v4, v0

    cmpg-float v4, v3, v4

    if-gez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public animateVisibility(I)V
    .locals 5
    .param p1    # I

    const-wide/16 v3, -0x1

    invoke-static {p1}, Lcom/android/gallery3d/ui/UndoBarView;->getTargetAlpha(I)F

    move-result v0

    iget-wide v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAnimationStartTime:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAlpha:F

    cmpl-float v1, v1, v0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAnimationStartTime:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mToAlpha:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    :cond_2
    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAlpha:F

    iput v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mFromAlpha:F

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mToAlpha:F

    invoke-static {}, Lcom/android/gallery3d/ui/AnimationTime;->startTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAnimationStartTime:J

    const/4 v1, 0x0

    invoke-super {p0, v1}, Lcom/android/gallery3d/ui/GLView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/GLView;->invalidate()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/android/gallery3d/ui/GLView;->setMeasuredSize(II)V

    return-void
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/gallery3d/ui/UndoBarView;->inUndoButton(Landroid/view/MotionEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mDownOnButton:Z

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mDownOnButton:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mOnClickListener:Lcom/android/gallery3d/ui/GLView$OnClickListener;

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/gallery3d/ui/UndoBarView;->inUndoButton(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mOnClickListener:Lcom/android/gallery3d/ui/GLView$OnClickListener;

    invoke-interface {v0, p0}, Lcom/android/gallery3d/ui/GLView$OnClickListener;->onClick(Lcom/android/gallery3d/ui/GLView;)V

    :cond_1
    iput-boolean v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mDownOnButton:Z

    goto :goto_0

    :pswitch_3
    iput-boolean v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mDownOnButton:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected render(Lcom/android/gallery3d/ui/GLCanvas;)V
    .locals 12
    .param p1    # Lcom/android/gallery3d/ui/GLCanvas;

    invoke-super {p0, p1}, Lcom/android/gallery3d/ui/GLView;->render(Lcom/android/gallery3d/ui/GLCanvas;)V

    invoke-direct {p0}, Lcom/android/gallery3d/ui/UndoBarView;->advanceAnimation()V

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/android/gallery3d/ui/GLCanvas;->save(I)V

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAlpha:F

    invoke-interface {p1, v0}, Lcom/android/gallery3d/ui/GLCanvas;->multiplyAlpha(F)V

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/GLView;->getWidth()I

    move-result v11

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/GLView;->getHeight()I

    move-result v10

    iget-object v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mPanel:Lcom/android/gallery3d/ui/NinePatchTexture;

    iget v2, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarMargin:I

    const/4 v3, 0x0

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarMargin:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v4, v11, v1

    iget v5, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarHeight:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/gallery3d/ui/NinePatchTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarMargin:I

    sub-int v2, v11, v0

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mUndoTextMargin:I

    iget-object v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mUndoText:Lcom/android/gallery3d/ui/StringTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/UploadedTexture;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    sub-int/2addr v2, v0

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarHeight:I

    iget-object v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mUndoText:Lcom/android/gallery3d/ui/StringTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/UploadedTexture;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v3, v0, 0x2

    iget-object v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mUndoText:Lcom/android/gallery3d/ui/StringTexture;

    invoke-virtual {v0, p1, v2, v3}, Lcom/android/gallery3d/ui/BasicTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;II)V

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mIconMargin:I

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mIconSize:I

    add-int/2addr v0, v1

    sub-int/2addr v2, v0

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarHeight:I

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mIconSize:I

    sub-int/2addr v0, v1

    div-int/lit8 v3, v0, 0x2

    iget-object v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mUndoIcon:Lcom/android/gallery3d/ui/ResourceTexture;

    iget v4, p0, Lcom/android/gallery3d/ui/UndoBarView;->mIconSize:I

    iget v5, p0, Lcom/android/gallery3d/ui/UndoBarView;->mIconSize:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/gallery3d/ui/ResourceTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;IIII)V

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorRightMargin:I

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorWidth:I

    add-int/2addr v0, v1

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorTopMargin:I

    int-to-float v5, v2

    int-to-float v6, v3

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorWidth:I

    int-to-float v7, v0

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarHeight:I

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorTopMargin:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mSeparatorBottomMargin:I

    sub-int/2addr v0, v1

    int-to-float v8, v0

    const v9, -0x555556

    move-object v4, p1

    invoke-interface/range {v4 .. v9}, Lcom/android/gallery3d/ui/GLCanvas;->fillRect(FFFFI)V

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarMargin:I

    iget v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mDeletedTextMargin:I

    add-int v2, v0, v1

    iget v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mBarHeight:I

    iget-object v1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mDeletedText:Lcom/android/gallery3d/ui/StringTexture;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/UploadedTexture;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v3, v0, 0x2

    iget-object v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mDeletedText:Lcom/android/gallery3d/ui/StringTexture;

    invoke-virtual {v0, p1, v2, v3}, Lcom/android/gallery3d/ui/BasicTexture;->draw(Lcom/android/gallery3d/ui/GLCanvas;II)V

    invoke-interface {p1}, Lcom/android/gallery3d/ui/GLCanvas;->restore()V

    return-void
.end method

.method public setOnClickListener(Lcom/android/gallery3d/ui/GLView$OnClickListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/ui/GLView$OnClickListener;

    iput-object p1, p0, Lcom/android/gallery3d/ui/UndoBarView;->mOnClickListener:Lcom/android/gallery3d/ui/GLView$OnClickListener;

    return-void
.end method

.method public setVisibility(I)V
    .locals 2
    .param p1    # I

    invoke-static {p1}, Lcom/android/gallery3d/ui/UndoBarView;->getTargetAlpha(I)F

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAlpha:F

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/gallery3d/ui/UndoBarView;->mAnimationStartTime:J

    invoke-super {p0, p1}, Lcom/android/gallery3d/ui/GLView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/GLView;->invalidate()V

    return-void
.end method
