.class public Lcom/android/gallery3d/app/SlideshowPage$Slide;
.super Ljava/lang/Object;
.source "SlideshowPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/SlideshowPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Slide"
.end annotation


# instance fields
.field public bitmap:Landroid/graphics/Bitmap;

.field public index:I

.field public item:Lcom/android/gallery3d/data/MediaItem;

.field public subType:I


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/data/MediaItem;ILandroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/data/MediaItem;
    .param p2    # I
    .param p3    # Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/android/gallery3d/app/SlideshowPage$Slide;->bitmap:Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/android/gallery3d/app/SlideshowPage$Slide;->item:Lcom/android/gallery3d/data/MediaItem;

    iput p2, p0, Lcom/android/gallery3d/app/SlideshowPage$Slide;->index:I

    return-void
.end method
