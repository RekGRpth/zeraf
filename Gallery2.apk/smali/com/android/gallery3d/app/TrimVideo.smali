.class public Lcom/android/gallery3d/app/TrimVideo;
.super Landroid/app/Activity;
.source "TrimVideo.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Lcom/android/gallery3d/app/ControllerOverlay$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/app/TrimVideo$ContentResolverQueryCallback;
    }
.end annotation


# static fields
.field public static final KEY_TRIM_END:Ljava/lang/String; = "trim_end"

.field public static final KEY_TRIM_START:Ljava/lang/String; = "trim_start"

.field public static final KEY_VIDEO_POSITION:Ljava/lang/String; = "video_pos"

.field private static final TAG:Ljava/lang/String; = "Gallery2/TrimVideo"

.field private static final TIME_STAMP_NAME:Ljava/lang/String; = "\'TRIM\'_yyyyMMdd_HHmmss"

.field public static final TRIM_ACTION:Ljava/lang/String; = "com.android.camera.action.TRIM"

.field private static mDstFile:Ljava/io/File;

.field private static mHasPaused:Z

.field private static mIsSaving:Z

.field private static mPlayTrimVideo:Z

.field public static mProgress:Landroid/app/ProgressDialog;

.field private static saveFolderName:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mController:Lcom/android/gallery3d/app/TrimControllerOverlay;

.field private mDragging:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mProgressChecker:Ljava/lang/Runnable;

.field private mSaveDirectory:Ljava/io/File;

.field private mSaveFileName:Ljava/lang/String;

.field private mSaveVideoTextView:Landroid/widget/TextView;

.field private final mShowDialogRunnable:Ljava/lang/Runnable;

.field private final mShowToastRunnable:Ljava/lang/Runnable;

.field private mSrcFile:Ljava/io/File;

.field private mSrcVideoPath:Ljava/lang/String;

.field private final mStartVideoRunnable:Ljava/lang/Runnable;

.field private mTrimEndTime:I

.field private mTrimStartTime:I

.field private mUri:Landroid/net/Uri;

.field private mVideoPosition:I

.field private mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/gallery3d/app/TrimVideo;->mPlayTrimVideo:Z

    sput-boolean v0, Lcom/android/gallery3d/app/TrimVideo;->mIsSaving:Z

    sput-object v1, Lcom/android/gallery3d/app/TrimVideo;->mDstFile:Ljava/io/File;

    sput-object v1, Lcom/android/gallery3d/app/TrimVideo;->saveFolderName:Ljava/lang/String;

    sput-boolean v0, Lcom/android/gallery3d/app/TrimVideo;->mHasPaused:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mHandler:Landroid/os/Handler;

    iput v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    iput v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    iput v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    iput-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSrcVideoPath:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveFileName:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSrcFile:Ljava/io/File;

    iput-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveDirectory:Ljava/io/File;

    new-instance v0, Lcom/android/gallery3d/app/TrimVideo$1;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/app/TrimVideo$1;-><init>(Lcom/android/gallery3d/app/TrimVideo;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mShowDialogRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/gallery3d/app/TrimVideo$2;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/app/TrimVideo$2;-><init>(Lcom/android/gallery3d/app/TrimVideo;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mShowToastRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/gallery3d/app/TrimVideo$3;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/app/TrimVideo$3;-><init>(Lcom/android/gallery3d/app/TrimVideo;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mStartVideoRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/gallery3d/app/TrimVideo$5;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/app/TrimVideo$5;-><init>(Lcom/android/gallery3d/app/TrimVideo;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mProgressChecker:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/android/gallery3d/app/TrimVideo;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->showProgressDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/gallery3d/app/TrimVideo;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveVideoTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/gallery3d/app/TrimVideo;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/gallery3d/app/TrimVideo;)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mSrcFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/gallery3d/app/TrimVideo;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    iget v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    return v0
.end method

.method static synthetic access$1300(Lcom/android/gallery3d/app/TrimVideo;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    iget v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    return v0
.end method

.method static synthetic access$1400(Lcom/android/gallery3d/app/TrimVideo;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mShowToastRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/gallery3d/app/TrimVideo;Ljava/io/File;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;
    .param p1    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/TrimVideo;->insertContent(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/gallery3d/app/TrimVideo;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mStartVideoRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    sget-boolean v0, Lcom/android/gallery3d/app/TrimVideo;->mHasPaused:Z

    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/gallery3d/app/TrimVideo;->saveFolderName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/android/gallery3d/app/TrimVideo;->saveFolderName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400()Ljava/io/File;
    .locals 1

    sget-object v0, Lcom/android/gallery3d/app/TrimVideo;->mDstFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$402(Ljava/io/File;)Ljava/io/File;
    .locals 0
    .param p0    # Ljava/io/File;

    sput-object p0, Lcom/android/gallery3d/app/TrimVideo;->mDstFile:Ljava/io/File;

    return-object p0
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/gallery3d/app/TrimVideo;->mPlayTrimVideo:Z

    return p0
.end method

.method static synthetic access$602(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/gallery3d/app/TrimVideo;->mIsSaving:Z

    return p0
.end method

.method static synthetic access$700(Lcom/android/gallery3d/app/TrimVideo;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->trimVideo()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/gallery3d/app/TrimVideo;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->setProgress()I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/android/gallery3d/app/TrimVideo;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/TrimVideo;

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mProgressChecker:Ljava/lang/Runnable;

    return-object v0
.end method

.method private getSaveDirectory()Ljava/io/File;
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    new-array v0, v1, [Ljava/io/File;

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "_data"

    aput-object v2, v1, v3

    new-instance v2, Lcom/android/gallery3d/app/TrimVideo$6;

    invoke-direct {v2, p0, v0}, Lcom/android/gallery3d/app/TrimVideo$6;-><init>(Lcom/android/gallery3d/app/TrimVideo;[Ljava/io/File;)V

    invoke-direct {p0, v1, v2}, Lcom/android/gallery3d/app/TrimVideo;->querySource([Ljava/lang/String;Lcom/android/gallery3d/app/TrimVideo$ContentResolverQueryCallback;)V

    aget-object v1, v0, v3

    return-object v1
.end method

.method private insertContent(Ljava/io/File;)Landroid/net/Uri;
    .locals 9
    .param p1    # Ljava/io/File;

    const-string v6, "Gallery2/TrimVideo"

    const-string v7, "insertContent()"

    invoke-static {v6, v7}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    div-long v2, v0, v6

    new-instance v5, Landroid/content/ContentValues;

    const/16 v6, 0xc

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    const-string v6, "title"

    iget-object v7, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveFileName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "_display_name"

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "mime_type"

    const-string v7, "video/mp4"

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "datetaken"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "date_modified"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "date_added"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "_data"

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "_size"

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v6, 0x4

    new-array v4, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "datetaken"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const-string v7, "latitude"

    aput-object v7, v4, v6

    const/4 v6, 0x2

    const-string v7, "longitude"

    aput-object v7, v4, v6

    const/4 v6, 0x3

    const-string v7, "resolution"

    aput-object v7, v4, v6

    new-instance v6, Lcom/android/gallery3d/app/TrimVideo$8;

    invoke-direct {v6, p0, v5}, Lcom/android/gallery3d/app/TrimVideo$8;-><init>(Lcom/android/gallery3d/app/TrimVideo;Landroid/content/ContentValues;)V

    invoke-direct {p0, v4, v6}, Lcom/android/gallery3d/app/TrimVideo;->querySource([Ljava/lang/String;Lcom/android/gallery3d/app/TrimVideo$ContentResolverQueryCallback;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v6

    return-object v6
.end method

.method private pauseVideo()V
    .locals 2

    const-string v0, "Gallery2/TrimVideo"

    const-string v1, "pauseVideo()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mController:Lcom/android/gallery3d/app/TrimControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/CommonControllerOverlay;->showPaused()V

    return-void
.end method

.method private playVideo()V
    .locals 2

    const-string v0, "Gallery2/TrimVideo"

    const-string v1, "playVideo()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mController:Lcom/android/gallery3d/app/TrimControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/TrimControllerOverlay;->showPlaying()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->setProgress()I

    return-void
.end method

.method private querySource([Ljava/lang/String;Lcom/android/gallery3d/app/TrimVideo$ContentResolverQueryCallback;)V
    .locals 7
    .param p1    # [Ljava/lang/String;
    .param p2    # Lcom/android/gallery3d/app/TrimVideo$ContentResolverQueryCallback;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v6, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mUri:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Gallery2/TrimVideo"

    const-string v2, "querySource() (cursor != null) && cursor.moveToNext()"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p2, v6}, Lcom/android/gallery3d/app/TrimVideo$ContentResolverQueryCallback;->onCursorResult(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v6, :cond_1

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1

    :catch_0
    move-exception v1

    if-eqz v6, :cond_1

    goto :goto_0
.end method

.method private setProgress()I
    .locals 5

    const-string v1, "Gallery2/TrimVideo"

    const-string v2, "setProgress()"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getCurrentPosition()I

    move-result v1

    iput v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    iget v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    invoke-virtual {v1, v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    iput v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    :cond_0
    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    iget v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    if-lt v1, v2, :cond_2

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    if-lez v1, :cond_2

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    iget v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    if-le v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    invoke-virtual {v1, v2}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    iput v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    :cond_1
    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mController:Lcom/android/gallery3d/app/TrimControllerOverlay;

    invoke-virtual {v1}, Lcom/android/gallery3d/app/CommonControllerOverlay;->showEnded()V

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->pause()V

    :cond_2
    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getDuration()I

    move-result v0

    if-lez v0, :cond_3

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    if-nez v1, :cond_3

    iput v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    :cond_3
    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mController:Lcom/android/gallery3d/app/TrimControllerOverlay;

    iget v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    iget v3, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    iget v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/android/gallery3d/app/TrimControllerOverlay;->setTimes(IIII)V

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    return v1
.end method

.method private showProgressDialog()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/gallery3d/app/TrimVideo;->mProgress:Landroid/app/ProgressDialog;

    sget-object v0, Lcom/android/gallery3d/app/TrimVideo;->mProgress:Landroid/app/ProgressDialog;

    const v1, 0x7f0c02ca

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/android/gallery3d/app/TrimVideo;->mProgress:Landroid/app/ProgressDialog;

    const v1, 0x7f0c02cb

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/android/gallery3d/app/TrimVideo;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    sget-object v0, Lcom/android/gallery3d/app/TrimVideo;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    sget-object v0, Lcom/android/gallery3d/app/TrimVideo;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private trimVideo()V
    .locals 7

    const/16 v6, 0x64

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v1, "Gallery2/TrimVideo"

    const-string v2, "trimVideo() start"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    iget v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    sub-int v0, v1, v2

    const-string v1, "Gallery2/TrimVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delta is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-ge v0, v6, :cond_0

    const-string v1, "Gallery2/TrimVideo"

    const-string v2, "delta < 100"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c02cd

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveVideoTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveVideoTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    sput-boolean v5, Lcom/android/gallery3d/app/TrimVideo;->mIsSaving:Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getDuration()I

    move-result v1

    sub-int/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v6, :cond_1

    const-string v1, "Gallery2/TrimVideo"

    const-string v2, "it will be onBackPressed"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->onBackPressed()V

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveVideoTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveVideoTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    sput-boolean v5, Lcom/android/gallery3d/app/TrimVideo;->mIsSaving:Z

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->getSaveDirectory()Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveDirectory:Ljava/io/File;

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveDirectory:Ljava/io/File;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_2
    const-string v1, "Gallery2/TrimVideo"

    const-string v2, "(mSaveDirectory == null) || !mSaveDirectory.canWrite()"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    const-string v3, "download"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveDirectory:Ljava/io/File;

    const v1, 0x7f0c02c0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/gallery3d/app/TrimVideo;->saveFolderName:Ljava/lang/String;

    :goto_1
    const-string v1, "Gallery2/TrimVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveFolderName is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/gallery3d/app/TrimVideo;->saveFolderName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "\'TRIM\'_yyyyMMdd_HHmmss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/sql/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveFileName:Ljava/lang/String;

    const-string v1, "Gallery2/TrimVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSaveFileName is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveDirectory:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".mp4"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v1, Lcom/android/gallery3d/app/TrimVideo;->mDstFile:Ljava/io/File;

    sget-object v1, Lcom/android/gallery3d/app/TrimVideo;->mDstFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "Gallery2/TrimVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDstFile name is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/gallery3d/app/TrimVideo;->mDstFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mSrcVideoPath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSrcFile:Ljava/io/File;

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSrcFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "Gallery2/TrimVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSrcFile name is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/app/TrimVideo;->mSrcFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/android/gallery3d/app/TrimVideo$7;

    invoke-direct {v2, p0}, Lcom/android/gallery3d/app/TrimVideo$7;-><init>(Lcom/android/gallery3d/app/TrimVideo;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/gallery3d/app/TrimVideo;->saveFolderName:Ljava/lang/String;

    goto/16 :goto_1
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;

    const-string v0, "Gallery2/TrimVideo"

    const-string v1, "onCompletion()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mController:Lcom/android/gallery3d/app/TrimControllerOverlay;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/CommonControllerOverlay;->showEnded()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mContext:Landroid/content/Context;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v4, "Gallery2/TrimVideo"

    const-string v5, "onCreate()"

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const/16 v4, 0x9

    invoke-virtual {p0, v4}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    const v4, 0x7f04005c

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setCustomView(I)V

    const v4, 0x7f0b013c

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveVideoTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveVideoTextView:Landroid/widget/TextView;

    new-instance v5, Lcom/android/gallery3d/app/TrimVideo$4;

    invoke-direct {v5, p0}, Lcom/android/gallery3d/app/TrimVideo$4;-><init>(Lcom/android/gallery3d/app/TrimVideo;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mUri:Landroid/net/Uri;

    const-string v4, "media-item-path"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mSrcVideoPath:Ljava/lang/String;

    const v4, 0x7f04005d

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setContentView(I)V

    const v4, 0x7f0b013d

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0b0083

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/mediatek/gallery3d/video/MTKVideoView;

    iput-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    new-instance v4, Lcom/android/gallery3d/app/TrimControllerOverlay;

    iget-object v5, p0, Lcom/android/gallery3d/app/TrimVideo;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/android/gallery3d/app/TrimControllerOverlay;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mController:Lcom/android/gallery3d/app/TrimControllerOverlay;

    check-cast v3, Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mController:Lcom/android/gallery3d/app/TrimControllerOverlay;

    invoke-virtual {v4}, Lcom/android/gallery3d/app/CommonControllerOverlay;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mController:Lcom/android/gallery3d/app/TrimControllerOverlay;

    invoke-virtual {v4, p0}, Lcom/android/gallery3d/app/CommonControllerOverlay;->setListener(Lcom/android/gallery3d/app/ControllerOverlay$Listener;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mController:Lcom/android/gallery3d/app/TrimControllerOverlay;

    invoke-virtual {v4, v7}, Lcom/android/gallery3d/app/CommonControllerOverlay;->setCanReplay(Z)V

    iget-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v4, p0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v4, p0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget-object v5, p0, Lcom/android/gallery3d/app/TrimVideo;->mUri:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setVideoURI(Landroid/net/Uri;Ljava/util/Map;Z)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->playVideo()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "Gallery2/TrimVideo"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onHidden()V
    .locals 0

    return-void
.end method

.method public onIsRTSP()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 2

    const-string v0, "Gallery2/TrimVideo"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/gallery3d/app/TrimVideo;->mHasPaused:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->suspend()V

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPlayPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->pauseVideo()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->playVideo()V

    goto :goto_0
.end method

.method public onReplay()V
    .locals 2

    const-string v0, "Gallery2/TrimVideo"

    const-string v1, "onReplay()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->playVideo()V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "Gallery2/TrimVideo"

    const-string v1, "onRestoreInstanceState()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "trim_start"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    const-string v0, "trim_end"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    const-string v0, "video_pos"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    const-string v0, "Gallery2/TrimVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTrimStartTime is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mTrimEndTime is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mVideoPosition is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "Gallery2/TrimVideo"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mDragging:Z

    sget-boolean v0, Lcom/android/gallery3d/app/TrimVideo;->mHasPaused:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/video/MTKVideoView;->resume()V

    sput-boolean v2, Lcom/android/gallery3d/app/TrimVideo;->mHasPaused:Z

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mProgressChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    sget-boolean v0, Lcom/android/gallery3d/app/TrimVideo;->mIsSaving:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/gallery3d/app/TrimVideo;->mProgress:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->showProgressDialog()V

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveVideoTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mSaveVideoTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    sget-boolean v0, Lcom/android/gallery3d/app/TrimVideo;->mPlayTrimVideo:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mStartVideoRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "Gallery2/TrimVideo"

    const-string v1, "onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "trim_start"

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "trim_end"

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "video_pos"

    iget v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSeekEnd(III)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "Gallery2/TrimVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSeekEnd() seekto time is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", start is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", end is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mDragging is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mDragging:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mDragging:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0, p1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    iput p2, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimStartTime:I

    iput p3, p0, Lcom/android/gallery3d/app/TrimVideo;->mTrimEndTime:I

    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->setProgress()I

    return-void
.end method

.method public onSeekMove(I)V
    .locals 3
    .param p1    # I

    const-string v0, "Gallery2/TrimVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSeekMove() seekto time is ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mDragging is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mDragging:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mDragging:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mVideoView:Lcom/mediatek/gallery3d/video/MTKVideoView;

    invoke-virtual {v0, p1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    :cond_0
    return-void
.end method

.method public onSeekStart()V
    .locals 3

    const-string v0, "Gallery2/TrimVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSeekStart() mDragging is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/TrimVideo;->mDragging:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mDragging:Z

    invoke-direct {p0}, Lcom/android/gallery3d/app/TrimVideo;->pauseVideo()V

    return-void
.end method

.method public onShown()V
    .locals 0

    return-void
.end method

.method public onStop()V
    .locals 2

    const-string v0, "Gallery2/TrimVideo"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/android/gallery3d/app/TrimVideo;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/gallery3d/app/TrimVideo;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    sput-object v0, Lcom/android/gallery3d/app/TrimVideo;->mProgress:Landroid/app/ProgressDialog;

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public showDialogCommand()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mShowDialogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mShowDialogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public showToast()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mShowToastRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/TrimVideo;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/TrimVideo;->mShowToastRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
