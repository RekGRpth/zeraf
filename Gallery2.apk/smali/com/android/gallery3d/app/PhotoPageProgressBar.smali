.class public Lcom/android/gallery3d/app/PhotoPageProgressBar;
.super Ljava/lang/Object;
.source "PhotoPageProgressBar.java"


# instance fields
.field private mContainer:Landroid/view/ViewGroup;

.field private mProgress:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040042

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/gallery3d/app/PhotoPageProgressBar;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPageProgressBar;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPageProgressBar;->mContainer:Landroid/view/ViewGroup;

    const v2, 0x7f0b00d2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/app/PhotoPageProgressBar;->mProgress:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public hideProgress()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoPageProgressBar;->mContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setProgress(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPageProgressBar;->mContainer:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPageProgressBar;->mProgress:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPageProgressBar;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    mul-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x64

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoPageProgressBar;->mProgress:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
