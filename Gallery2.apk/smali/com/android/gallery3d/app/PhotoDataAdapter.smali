.class public Lcom/android/gallery3d/app/PhotoDataAdapter;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Lcom/android/gallery3d/app/PhotoPage$Model;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/app/PhotoDataAdapter$MavRenderThread;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$MavDecoderListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$GifDecoderListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailJob;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailJob;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$UpdateContent;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageJob;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;,
        Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;
    }
.end annotation


# static fields
.field private static final BIT_DRM_SCREEN_NAIL:I = 0x40000000

.field private static final BIT_FULL_IMAGE:I = 0x2

.field private static final BIT_GIF_ANIMATION:I = -0x80000000

.field private static final BIT_MAV_PLAYBACK:I = 0x10000000

.field private static final BIT_SCREEN_NAIL:I = 0x1

.field private static final BIT_SECOND_SCREEN_NAIL:I = 0x20000000

.field private static final DATA_CACHE_SIZE:I = 0x100

.field private static final IMAGE_CACHE_SIZE:I = 0x7

.field private static final MIN_LOAD_COUNT:I = 0x10

.field private static final MSG_LOAD_FINISH:I = 0x2

.field private static final MSG_LOAD_START:I = 0x1

.field private static final MSG_RUN_OBJECT:I = 0x3

.field private static final MSG_UPDATE_IMAGE_REQUESTS:I = 0x4

.field private static final SCREEN_NAIL_MAX:I = 0x3

.field private static final TAG:Ljava/lang/String; = "Gallery2/PhotoDataAdapter"

.field public static final TYPE_LOAD_FRAME:I = 0x1

.field public static final TYPE_LOAD_TOTAL_COUNT:I

.field private static final mIsDrmSupported:Z

.field private static final mIsGifAnimationSupported:Z

.field private static final mIsMavSupported:Z

.field private static final mIsStereoDisplaySupported:Z

.field private static sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;


# instance fields
.field private mActiveEnd:I

.field private mActiveStart:I

.field private final mActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

.field private mCameraIndex:I

.field private final mChanges:[J

.field private mConsumedItemPath:Lcom/android/gallery3d/data/Path;

.field private mContentEnd:I

.field private mContentStart:I

.field private mCurrentIndex:I

.field private mCurrentMpoIndex:I

.field private mCurrentScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private final mData:[Lcom/android/gallery3d/data/MediaItem;

.field private mDataListener:Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

.field private mFirstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private mFocusHintDirection:I

.field private mFocusHintPath:Lcom/android/gallery3d/data/Path;

.field private mImageCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mIsActive:Z

.field private mIsMavLoadingFinished:Z

.field private mIsMavStereoMode:Z

.field private mIsPanorama:Z

.field private mIsStaticCamera:Z

.field private mItemPath:Lcom/android/gallery3d/data/Path;

.field private final mMainHandler:Landroid/os/Handler;

.field private mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

.field private mNeedFullImage:Z

.field private mNextMpoIndex:I

.field private mOldCurrentScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private mOldFirstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private mOldSecondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private final mPaths:[Lcom/android/gallery3d/data/Path;

.field private final mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

.field private final mReDecodeToImproveImageQuality:Z

.field private mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

.field private mRenderLock:Ljava/lang/Object;

.field public mRenderRequested:Z

.field private mSecondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private mSize:I

.field private final mSource:Lcom/android/gallery3d/data/MediaSet;

.field private final mSourceListener:Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;

.field private mSourceVersion:J

.field private final mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

.field private final mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

.field private mTimeIntervalDRM:Z

.field private final mUploader:Lcom/android/gallery3d/ui/TiledTexture$Uploader;

.field private reloadCameraItem:Z


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isGifAnimationSupported()Z

    move-result v9

    sput-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isDrmSupported()Z

    move-result v9

    sput-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isStereoDisplaySupported()Z

    move-result v9

    sput-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isMAVSupported()Z

    move-result v9

    sput-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavSupported:Z

    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v9, :cond_0

    move v1, v7

    :goto_0
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    if-eqz v9, :cond_1

    move v0, v7

    :goto_1
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v9, :cond_2

    move v6, v7

    :goto_2
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v9, :cond_3

    move v5, v7

    :goto_3
    const/4 v3, 0x0

    add-int/lit8 v9, v1, 0x10

    add-int/lit8 v9, v9, -0x2

    add-int/2addr v9, v6

    add-int/2addr v9, v5

    new-array v9, v9, [Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    sput-object v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    sget-object v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    new-instance v10, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v10, v8, v7}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v10, v9, v3

    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v9, :cond_7

    sget-object v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v4, 0x1

    new-instance v10, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    const/high16 v11, 0x20000000

    invoke-direct {v10, v8, v11}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v10, v9, v4

    :goto_4
    const/4 v2, 0x1

    move v4, v3

    :goto_5
    const/4 v9, 0x7

    if-ge v2, v9, :cond_4

    sget-object v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v4, 0x1

    new-instance v10, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v10, v2, v7}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v10, v9, v4

    sget-object v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    new-instance v10, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    neg-int v11, v2

    invoke-direct {v10, v11, v7}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v10, v9, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_0
    move v1, v8

    goto :goto_0

    :cond_1
    move v0, v8

    goto :goto_1

    :cond_2
    move v6, v8

    goto :goto_2

    :cond_3
    move v5, v8

    goto :goto_3

    :cond_4
    sget-object v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v4, 0x1

    new-instance v9, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    const/4 v10, 0x2

    invoke-direct {v9, v8, v10}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v9, v7, v4

    sget-boolean v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v7, :cond_5

    sget-object v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    new-instance v9, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    const/high16 v10, -0x80000000

    invoke-direct {v9, v8, v10}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v9, v7, v3

    move v3, v4

    :cond_5
    sget-boolean v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v7, :cond_6

    sget-object v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    new-instance v9, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    const/high16 v10, 0x10000000

    invoke-direct {v9, v8, v10}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v9, v7, v3

    move v3, v4

    :cond_6
    return-void

    :cond_7
    move v3, v4

    goto :goto_4
.end method

.method public constructor <init>(Lcom/android/gallery3d/app/AbstractGalleryActivity;Lcom/android/gallery3d/ui/PhotoView;Lcom/android/gallery3d/data/MediaSet;Lcom/android/gallery3d/data/Path;IIZZ)V
    .locals 7
    .param p1    # Lcom/android/gallery3d/app/AbstractGalleryActivity;
    .param p2    # Lcom/android/gallery3d/ui/PhotoView;
    .param p3    # Lcom/android/gallery3d/data/MediaSet;
    .param p4    # Lcom/android/gallery3d/data/Path;
    .param p5    # I
    .param p6    # I
    .param p7    # Z
    .param p8    # Z

    const/4 v6, 0x0

    const/4 v3, 0x7

    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReDecodeToImproveImageQuality:Z

    new-instance v0, Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-direct {v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    const/16 v0, 0x100

    new-array v0, v0, [Lcom/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    iput v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    iput v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iput v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    iput v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    new-array v0, v3, [J

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    new-array v0, v3, [Lcom/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/android/gallery3d/data/Path;

    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J

    iput v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    iput v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintDirection:I

    iput-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/android/gallery3d/data/Path;

    new-instance v0, Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;

    invoke-direct {v0, p0, v6}, Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/app/PhotoDataAdapter$1;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceListener:Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;

    iput-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->reloadCameraItem:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    iput-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderRequested:Z

    iput-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavStereoMode:Z

    iput v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentMpoIndex:I

    iput v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mNextMpoIndex:I

    invoke-static {p3}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/android/gallery3d/data/MediaSet;

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/ui/PhotoView;

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-static {p4}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    iput p5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    iput p6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    iput-boolean p7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsPanorama:Z

    iput-boolean p8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStaticCamera:Z

    invoke-virtual {p1}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iput-boolean v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mNeedFullImage:Z

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->reloadCameraItem:Z

    const-string v0, "Gallery2/PhotoDataAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PhotoDataAdapter reloadCameraItem "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->reloadCameraItem:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    const-wide/16 v1, -0x1

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->fill([JJ)V

    new-instance v0, Lcom/android/gallery3d/ui/TiledTexture$Uploader;

    invoke-virtual {p1}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/gallery3d/ui/TiledTexture$Uploader;-><init>(Lcom/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mUploader:Lcom/android/gallery3d/ui/TiledTexture$Uploader;

    new-instance v0, Lcom/android/gallery3d/app/PhotoDataAdapter$1;

    invoke-virtual {p1}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$1;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    return v0
.end method

.method static synthetic access$1300(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    return v0
.end method

.method static synthetic access$1302(Lcom/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    return p1
.end method

.method static synthetic access$1400(Lcom/android/gallery3d/app/PhotoDataAdapter;)[Lcom/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    return v0
.end method

.method static synthetic access$1502(Lcom/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    return p1
.end method

.method static synthetic access$1600(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/data/Path;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;)Lcom/android/gallery3d/data/Path;
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/android/gallery3d/app/PhotoDataAdapter;)J
    .locals 2
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-wide v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J

    return-wide v0
.end method

.method static synthetic access$1802(Lcom/android/gallery3d/app/PhotoDataAdapter;J)J
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J

    return-wide p1
.end method

.method static synthetic access$1900(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    return v0
.end method

.method static synthetic access$1902(Lcom/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    return p1
.end method

.method static synthetic access$200(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    return-void
.end method

.method static synthetic access$2000(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    return v0
.end method

.method static synthetic access$2002(Lcom/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    return p1
.end method

.method static synthetic access$2100(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    return-void
.end method

.method static synthetic access$2200()Z
    .locals 1

    sget-boolean v0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->restoreDrmConsumeStatus()V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    return-void
.end method

.method static synthetic access$2500(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider()V

    return-void
.end method

.method static synthetic access$2600(Lcom/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    return-void
.end method

.method static synthetic access$2800(Lcom/android/gallery3d/app/PhotoDataAdapter;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Ljava/util/concurrent/Callable;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/gallery3d/app/PhotoDataAdapter;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->reloadCameraItem:Z

    return v0
.end method

.method static synthetic access$3002(Lcom/android/gallery3d/app/PhotoDataAdapter;Z)Z
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->reloadCameraItem:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/data/Path;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;)Lcom/android/gallery3d/data/Path;
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/android/gallery3d/data/Path;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintDirection:I

    return v0
.end method

.method static synthetic access$3300(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    return v0
.end method

.method static synthetic access$3400(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateOriginScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$3500(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateSecondScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$3600(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateGifDecoder(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$3800(Lcom/android/gallery3d/app/PhotoDataAdapter;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    return v0
.end method

.method static synthetic access$3900(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/MediaItem;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isTemporaryItem(Lcom/android/gallery3d/data/MediaItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4000(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/ui/PhotoView;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;Lcom/android/gallery3d/data/MediaItem;I)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;
    .param p3    # Lcom/android/gallery3d/data/MediaItem;
    .param p4    # I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateMavDecoder(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;Lcom/android/gallery3d/data/MediaItem;I)V

    return-void
.end method

.method static synthetic access$4200(Lcom/android/gallery3d/app/PhotoDataAdapter;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavStereoMode:Z

    return v0
.end method

.method static synthetic access$4202(Lcom/android/gallery3d/app/PhotoDataAdapter;Z)Z
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavStereoMode:Z

    return p1
.end method

.method static synthetic access$4300()Z
    .locals 1

    sget-boolean v0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    return v0
.end method

.method static synthetic access$4400(Lcom/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentMpoIndex:I

    return v0
.end method

.method static synthetic access$4500(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/ui/ScreenNail;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/ui/ScreenNail;

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    return-object p1
.end method

.method static synthetic access$4600(Lcom/android/gallery3d/app/PhotoDataAdapter;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/MediaItem;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->newPlaceholderScreenNail(Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/gallery3d/app/PhotoDataAdapter;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/gallery3d/app/PhotoDataAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/PhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateFullImage(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method private executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private fireDataChange()V
    .locals 15

    const v10, 0x7fffffff

    const/4 v14, 0x0

    const/4 v13, 0x7

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->startProfilePhotoPageFireDataChange()V

    const/4 v1, 0x0

    const/4 v3, -0x3

    :goto_0
    const/4 v9, 0x3

    if-gt v3, v9, :cond_1

    iget v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v9, v3

    invoke-direct {p0, v9}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getVersion(I)J

    move-result-wide v5

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    add-int/lit8 v11, v3, 0x3

    aget-wide v11, v9, v11

    cmp-long v9, v11, v5

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    add-int/lit8 v11, v3, 0x3

    aput-wide v5, v9, v11

    const/4 v1, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->stopProfilePhotoPageFireDataChange()V

    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x7

    new-array v2, v13, [I

    new-array v7, v13, [Lcom/android/gallery3d/data/Path;

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/android/gallery3d/data/Path;

    invoke-static {v9, v14, v7, v14, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v13, :cond_3

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/android/gallery3d/data/Path;

    iget v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v11, v3

    add-int/lit8 v11, v11, -0x3

    invoke-direct {p0, v11}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v11

    aput-object v11, v9, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v13, :cond_8

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/android/gallery3d/data/Path;

    aget-object v8, v9, v3

    if-nez v8, :cond_4

    aput v10, v2, v3

    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_5
    if-ge v4, v13, :cond_5

    aget-object v9, v7, v4

    if-ne v9, v8, :cond_6

    :cond_5
    if-ge v4, v13, :cond_7

    add-int/lit8 v9, v4, -0x3

    :goto_6
    aput v9, v2, v3

    goto :goto_4

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_7
    move v9, v10

    goto :goto_6

    :cond_8
    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    iget v10, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    neg-int v10, v10

    iget v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v11, v11, -0x1

    iget v12, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int/2addr v11, v12

    invoke-virtual {v9, v2, v10, v11}, Lcom/android/gallery3d/ui/PhotoView;->notifyDataChange([III)V

    invoke-virtual {p0, v14}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v9

    if-eqz v9, :cond_9

    const-string v9, "Gallery2/PhotoDataAdapter"

    const-string v10, "fireDataChange: enterCameraPreview"

    invoke-static {v9, v10}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v9}, Lcom/android/gallery3d/ui/PhotoView;->enterCameraPreview()V

    :cond_9
    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->stopProfilePhotoPageFireDataChange()V

    goto :goto_1
.end method

.method private getItem(I)Lcom/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    if-ltz p1, :cond_0

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-ge p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt p1, v0, :cond_2

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ge p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt p1, v0, :cond_3

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit16 v1, p1, 0x100

    aget-object v0, v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private getItemInternal(I)Lcom/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    if-ltz p1, :cond_0

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-lt p1, v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge p1, v1, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit16 v1, p1, 0x100

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method private getPath(I)Lcom/android/gallery3d/data/Path;
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItemInternal(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0
.end method

.method private getScreenNails([Landroid/graphics/Bitmap;)[Lcom/android/gallery3d/ui/ScreenNail;
    .locals 7
    .param p1    # [Landroid/graphics/Bitmap;

    if-nez p1, :cond_1

    const/4 v3, 0x0

    :cond_0
    return-object v3

    :cond_1
    array-length v1, p1

    new-array v3, v1, [Lcom/android/gallery3d/ui/ScreenNail;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v4, p1, v0

    if-nez v4, :cond_2

    const-string v4, "Gallery2/PhotoDataAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bmps["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] is null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v4

    aget-object v5, p1, v0

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getMtkScreenNail(Lcom/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v2

    if-nez v2, :cond_3

    new-instance v2, Lcom/android/gallery3d/ui/BitmapScreenNail;

    aget-object v4, p1, v0

    invoke-direct {v2, v4}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    :cond_3
    aput-object v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getTargetScreenNail(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 2
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v0, :cond_2

    const-string v0, "Gallery2/PhotoDataAdapter"

    const-string v1, "return current MpoFrame"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_0

    :cond_3
    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_0
.end method

.method private getVersion(I)J
    .locals 3
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItemInternal(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v1, -0x1

    :goto_0
    return-wide v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getDataVersion()J

    move-result-wide v1

    goto :goto_0
.end method

.method private isTemporaryItem(Lcom/android/gallery3d/data/MediaItem;)Z
    .locals 6
    .param p1    # Lcom/android/gallery3d/data/MediaItem;

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v2, p1, Lcom/android/gallery3d/data/LocalMediaItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v0}, Lcom/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v2

    sget v3, Lcom/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/LocalMediaItem;->getSize()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/LocalMediaItem;->getDateInMs()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private newPlaceholderScreenNail(Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 3
    .param p1    # Lcom/android/gallery3d/data/MediaItem;

    invoke-static {p1}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getMtkScreenNail(Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v0

    new-instance v1, Lcom/android/gallery3d/ui/BitmapScreenNail;

    invoke-direct {v1, v2, v0}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(II)V

    goto :goto_0
.end method

.method private requestRender()V
    .locals 5

    const-string v2, "Gallery2/PhotoDataAdapter"

    const-string v3, "requestRender"

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderRequested:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-string v2, "Gallery2/PhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "request render consumed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private resetDrmConsumeStatus()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mConsumedItemPath:Lcom/android/gallery3d/data/Path;

    return-void
.end method

.method private restoreDrmConsumeStatus()V
    .locals 4

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mConsumedItemPath:Lcom/android/gallery3d/data/Path;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTimeIntervalDRM:Z

    if-eqz v2, :cond_2

    const-string v2, "Gallery2/PhotoDataAdapter"

    const-string v3, "restoreDrmConsumeStatus:for time interval media..."

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    rem-int/lit16 v3, v3, 0x100

    aget-object v1, v2, v3

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->hasDrmRights()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "Gallery2/PhotoDataAdapter"

    const-string v3, "restoreDrmConsumeStatus:we have no rights "

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->resetDrmConsumeStatus()V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mConsumedItemPath:Lcom/android/gallery3d/data/Path;

    if-ne v2, v3, :cond_3

    iget-boolean v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->enterConsumeMode()V

    :cond_3
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->resetDrmConsumeStatus()V

    goto :goto_0
.end method

.method private saveDrmConsumeStatus()V
    .locals 4

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->resetDrmConsumeStatus()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    rem-int/lit16 v3, v3, 0x100

    aget-object v0, v2, v3

    if-nez v0, :cond_1

    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mConsumedItemPath:Lcom/android/gallery3d/data/Path;

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v2

    goto :goto_0
.end method

.method private startGifAnimation(Lcom/android/gallery3d/data/Path;)V
    .locals 7
    .param p1    # Lcom/android/gallery3d/data/Path;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getTotalFrameCount()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getWidth()I

    move-result v3

    if-lez v3, :cond_2

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getHeight()I

    move-result v3

    if-lez v3, :cond_2

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getWidth()I

    move-result v3

    int-to-long v3, v3

    iget-object v5, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v5}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getHeight()I

    move-result v5

    int-to-long v5, v5

    mul-long/2addr v3, v5

    const-wide/16 v5, 0x4

    mul-long/2addr v3, v5

    const-wide v5, 0x100000000L

    cmp-long v3, v3, v5

    if-ltz v3, :cond_3

    :cond_2
    const-string v3, "Gallery2/PhotoDataAdapter"

    const-string v4, "startGifAnimation:illegal gif frame dimension"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    if-ne p1, v3, :cond_0

    new-instance v2, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter$1;)V

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iput-object v3, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iput v0, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->animatedIndex:I

    iput-object v1, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->entry:Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    const/4 v5, 0x3

    new-instance v6, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;

    invoke-direct {v6, p0, p1, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;)V

    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private startMavPlayback(Lcom/android/gallery3d/data/Path;)V
    .locals 7
    .param p1    # Lcom/android/gallery3d/data/Path;

    const-string v4, "Gallery2/PhotoDataAdapter"

    const-string v5, "startMavPlayback"

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoTotalCount:I

    if-lez v3, :cond_3

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v4

    if-ne p1, v4, :cond_2

    div-int/lit8 v2, v3, 0x2

    const-string v4, "Gallery2/PhotoDataAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "the middle frame is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    add-int/lit8 v5, v3, -0x1

    invoke-interface {v4, v5, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;->setSeekBar(II)V

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;->setStatus(Z)V

    :cond_1
    new-instance v4, Lcom/android/gallery3d/app/PhotoDataAdapter$MavRenderThread;

    invoke-direct {v4, p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter$MavRenderThread;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_2
    const-string v4, "Gallery2/PhotoDataAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "incorrect path: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MtkLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v4, "Gallery2/PhotoDataAdapter"

    const-string v5, "mpoTotalCount <= 0"

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MtkLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startTaskIfNeeded(II)Lcom/android/gallery3d/util/Future;
    .locals 11
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/android/gallery3d/util/Future",
            "<*>;"
        }
    .end annotation

    iget v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt p1, v6, :cond_0

    iget v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt p1, v6, :cond_1

    :cond_0
    const/4 v6, 0x0

    :goto_0
    return-object v6

    :cond_1
    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_2

    const/4 v6, 0x0

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit16 v7, p1, 0x100

    aget-object v1, v6, v7

    if-eqz v1, :cond_3

    const/4 v6, 0x1

    :goto_1
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getDataVersion()J

    move-result-wide v4

    const/4 v6, 0x1

    if-ne p2, v6, :cond_4

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v6, :cond_4

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    cmp-long v6, v6, v4

    if-nez v6, :cond_4

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    goto :goto_0

    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    :cond_4
    const/4 v6, 0x2

    if-ne p2, v6, :cond_5

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v6, :cond_5

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    cmp-long v6, v6, v4

    if-nez v6, :cond_5

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    goto :goto_0

    :cond_5
    const/high16 v6, -0x80000000

    if-ne p2, v6, :cond_6

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v6, :cond_6

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isGifSupported()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedGif:J

    cmp-long v6, v6, v4

    if-nez v6, :cond_6

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    goto :goto_0

    :cond_6
    const/high16 v6, 0x20000000

    if-ne p2, v6, :cond_7

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v6, :cond_7

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedSecondScreenNail:J

    cmp-long v6, v6, v4

    if-nez v6, :cond_7

    sget-boolean v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v6, :cond_7

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    goto :goto_0

    :cond_7
    const/4 v6, 0x2

    if-ne p2, v6, :cond_8

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v6, :cond_8

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedOriginScreenNail:J

    cmp-long v6, v6, v4

    if-nez v6, :cond_8

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    goto :goto_0

    :cond_8
    const/high16 v6, 0x10000000

    if-ne p2, v6, :cond_9

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v6, :cond_9

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isMAVSupported()Z

    move-result v6

    if-eqz v6, :cond_9

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedMav:J

    cmp-long v6, v6, v4

    if-nez v6, :cond_9

    const-string v6, "Gallery2/PhotoDataAdapter"

    const-string v7, "startTaskIfNeed: return existed mpoDecoderTask"

    invoke-static {v6, v7}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_9
    const/4 v6, 0x1

    if-ne p2, v6, :cond_b

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    cmp-long v6, v6, v4

    if-eqz v6, :cond_b

    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v6

    if-eqz v6, :cond_a

    iget v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    if-ne p1, v6, :cond_a

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->getScreenNail()Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->triggerPhotoPageDecodeScreenNail()V

    iput-wide v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    iput-object v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_a
    iput-wide v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->triggerPhotoPageDecodeScreenNail()V

    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;

    invoke-direct {v7, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailListener;

    invoke-direct {v8, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$ScreenNailListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v6, v7, v8}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v6

    iput-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_b
    const/4 v6, 0x2

    if-ne p2, v6, :cond_c

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    cmp-long v6, v6, v4

    if-eqz v6, :cond_c

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v6

    and-int/lit8 v6, v6, 0x40

    if-eqz v6, :cond_c

    iput-wide v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageJob;

    invoke-direct {v7, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageJob;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageListener;

    invoke-direct {v8, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$FullImageListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v6, v7, v8}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v6

    iput-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_c
    const/4 v6, 0x2

    if-ne p2, v6, :cond_d

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedOriginScreenNail:J

    cmp-long v6, v6, v4

    if-eqz v6, :cond_d

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v6

    and-int/lit8 v6, v6, 0x40

    if-nez v6, :cond_d

    iput-wide v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedOriginScreenNail:J

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    const/4 v6, 0x1

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalFrame:Z

    const/16 v6, 0x3c0

    iput v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalTargetSize:I

    const/4 v6, 0x1

    invoke-static {v2, v6}, Lcom/mediatek/gallery3d/util/MediatekFeature;->enablePictureQualityEnhance(Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Z)V

    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailJob;

    invoke-direct {v7, v1, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailJob;-><init>(Lcom/android/gallery3d/data/MediaItem;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;)V

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailListener;

    invoke-direct {v8, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$OriginScreenNailListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v6, v7, v8}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v6

    iput-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_d
    sget-boolean v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v6, :cond_e

    const/high16 v6, -0x80000000

    if-ne p2, v6, :cond_e

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedGif:J

    cmp-long v6, v6, v4

    if-eqz v6, :cond_e

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v6

    and-int/lit16 v6, v6, 0x1000

    if-eqz v6, :cond_e

    iput-wide v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedGif:J

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    const/4 v6, 0x1

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inGifDecoder:Z

    const-string v6, "Gallery2/PhotoDataAdapter"

    const-string v7, "startTaskIfNeeded:start GifDecoder task"

    invoke-static {v6, v7}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    const/4 v7, 0x1

    invoke-virtual {v1, v7, v2}, Lcom/android/gallery3d/data/MediaItem;->requestImage(ILcom/mediatek/gallery3d/util/MediatekFeature$Params;)Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v7

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$GifDecoderListener;

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifDecoderListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;)V

    invoke-virtual {v6, v7, v8}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v6

    iput-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_e
    sget-boolean v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v6, :cond_f

    const/high16 v6, 0x20000000

    if-ne p2, v6, :cond_f

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedSecondScreenNail:J

    cmp-long v6, v6, v4

    if-eqz v6, :cond_f

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v6

    const/high16 v7, 0x10000

    and-int/2addr v6, v7

    if-eqz v6, :cond_f

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->getStereoLayout()I

    move-result v6

    iput v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoLayout:I

    iput-wide v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedSecondScreenNail:J

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    const/4 v6, 0x1

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFrame:Z

    const/4 v6, 0x1

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFrame:Z

    const/4 v6, 0x1

    invoke-static {v2, v6}, Lcom/mediatek/gallery3d/util/MediatekFeature;->enablePictureQualityEnhance(Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Z)V

    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailJob;

    invoke-direct {v7, p0, v1, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailJob;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;Lcom/mediatek/gallery3d/util/MediatekFeature$Params;)V

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailListener;

    invoke-direct {v8, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$SecondScreenNailListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v6, v7, v8}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v6

    iput-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_f
    sget-boolean v6, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v6, :cond_10

    const/high16 v6, 0x10000000

    if-ne p2, v6, :cond_10

    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v6}, Lcom/android/gallery3d/ui/PhotoView;->getFilmMode()Z

    move-result v6

    if-nez v6, :cond_10

    iget-wide v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedMav:J

    cmp-long v6, v6, v4

    if-eqz v6, :cond_10

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v6

    const/high16 v7, 0x2000000

    and-int/2addr v6, v7

    if-eqz v6, :cond_10

    const-string v6, "Gallery2/PhotoDataAdapter"

    const-string v7, "create mav decoder task"

    invoke-static {v6, v7}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-wide v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedMav:J

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    const/4 v6, 0x1

    iput-boolean v6, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inMpoTotalCount:Z

    const-string v6, "Gallery2/PhotoDataAdapter"

    const-string v7, "get mav total count"

    invoke-static {v6, v7}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavLoadingFinished:Z

    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    iget-boolean v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavLoadingFinished:Z

    invoke-virtual {v6, v7}, Lcom/android/gallery3d/ui/PhotoView;->setMavLoadingFinished(Z)V

    iget-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    const/4 v7, 0x1

    invoke-virtual {v1, v7, v2}, Lcom/android/gallery3d/data/MediaItem;->requestImage(ILcom/mediatek/gallery3d/util/MediatekFeature$Params;)Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v7

    new-instance v8, Lcom/android/gallery3d/app/PhotoDataAdapter$MavDecoderListener;

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {v8, p0, v9, v1, v10}, Lcom/android/gallery3d/app/PhotoDataAdapter$MavDecoderListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/data/MediaItem;I)V

    invoke-virtual {v6, v7, v8}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v6

    iput-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    iget-object v6, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    goto/16 :goto_0

    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method private updateCurrentIndex(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    const-string v2, "Gallery2/PhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateCurrentIndex: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    if-ne v2, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderRequested:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit16 v3, p1, 0x100

    aget-object v0, v2, v3

    if-nez v0, :cond_4

    const/4 v2, 0x0

    :goto_1
    iput-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    sget-boolean v2, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2, v5}, Lcom/android/gallery3d/ui/PhotoView;->setStereoMode(Z)V

    :cond_2
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateDrmScreenNail()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    invoke-interface {v2, p1, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;->onPhotoChanged(ILcom/android/gallery3d/data/Path;)V

    :cond_3
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    invoke-virtual {p0, v5}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/PhotoView;->enterCameraPreview()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/PhotoView;->setWantPictureCenterCallbacks(Z)V

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_4
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v2

    goto :goto_1

    :cond_5
    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v2, v1, v2

    invoke-virtual {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/android/gallery3d/ui/PhotoView;->leaveCameraPreview()V

    goto :goto_0
.end method

.method private updateDrmScreenNail()V
    .locals 9

    sget-boolean v5, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    if-nez v5, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    :goto_0
    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ge v1, v5, :cond_0

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItemInternal(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v3

    if-nez v3, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_2

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-static {v5}, Lcom/mediatek/gallery3d/util/MediatekFeature;->toMtkScreenNail(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    move-result-object v4

    if-eqz v4, :cond_2

    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v2, v1, v5

    const/4 v5, -0x3

    if-lt v2, v5, :cond_2

    const/4 v5, 0x3

    if-gt v2, v5, :cond_2

    if-eqz v2, :cond_4

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    :cond_4
    invoke-direct {p0, v4, v3, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateDrmScreenNail(Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;Lcom/android/gallery3d/data/MediaItem;Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    add-int/lit8 v6, v2, 0x3

    const-wide/16 v7, -0x1

    aput-wide v7, v5, v6

    goto :goto_1
.end method

.method private updateDrmScreenNail(Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;I)Z
    .locals 4
    .param p1    # Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-direct {p0, p2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateDrmScreenNail(Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;Lcom/android/gallery3d/data/MediaItem;Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Z

    move-result v3

    goto :goto_0
.end method

.method private updateDrmScreenNail(Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;Lcom/android/gallery3d/data/MediaItem;Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Z
    .locals 6
    .param p1    # Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;
    .param p2    # Lcom/android/gallery3d/data/MediaItem;
    .param p3    # Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    invoke-virtual {p2}, Lcom/android/gallery3d/data/MediaItem;->getSubType()I

    move-result v1

    const/16 v0, 0xc

    invoke-virtual {p1}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getSubType()I

    move-result v2

    iget-boolean v3, p3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-eqz v3, :cond_0

    and-int v3, v2, v0

    if-nez v3, :cond_1

    :cond_0
    iget-boolean v3, p3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-nez v3, :cond_2

    and-int v3, v2, v0

    if-nez v3, :cond_2

    :cond_1
    iget-boolean v3, p3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-eqz v3, :cond_4

    and-int/lit8 v2, v2, -0xd

    invoke-virtual {p1, v2}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->setSubType(I)V

    :goto_1
    move v5, v4

    :cond_2
    return v5

    :cond_3
    move v3, v5

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v1}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->setSubType(I)V

    goto :goto_1
.end method

.method private updateFullImage(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 9
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/BitmapRegionDecoder;",
            ">;)V"
        }
    .end annotation

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_0

    iget-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eq v4, p2, :cond_2

    :cond_0
    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/BitmapRegionDecoder;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p2, :cond_3

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/BitmapRegionDecoder;

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/android/gallery3d/data/DataManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/android/gallery3d/data/DataManager;->getMediaObject(Lcom/android/gallery3d/data/Path;)Lcom/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_3

    iget-boolean v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isOutOfLimitation(Ljava/lang/String;II)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "Gallery2/PhotoDataAdapter"

    const-string v5, "out of limitation: %s [mime type: %s, width: %d, height: %d]"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    const/4 v7, 0x2

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    iput-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/BitmapRegionDecoder;

    iput-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    iget-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v4}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v4

    if-ne p1, v4, :cond_4

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4, v7}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    :cond_4
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto :goto_0
.end method

.method private updateGifDecoder(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;)V"
        }
    .end annotation

    const-string v2, "Gallery2/PhotoDataAdapter"

    const-string v3, ">> updateGifDecoder"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eq v2, p2, :cond_1

    :cond_0
    const-string v2, "Gallery2/PhotoDataAdapter"

    const-string v3, "updateGifDecoder:invalid entry or future"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v2, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-static {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature;->toMtkScreenNail(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/ui/MtkBitmapScreenNail;->getSubType()I

    move-result v2

    invoke-static {v2}, Lcom/mediatek/gallery3d/drm/DrmHelper;->showDrmMicroThumb(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "Gallery2/PhotoDataAdapter"

    const-string v3, "updateGifDecoder:no animation for non-consume drm"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    const-string v2, "Gallery2/PhotoDataAdapter"

    const-string v3, "<< updateGifDecoder"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->startGifAnimation(Lcom/android/gallery3d/data/Path;)V

    goto :goto_1
.end method

.method private updateImageCache()V
    .locals 13

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->startProfilePhotoPageUpdateImageCache()V

    new-instance v8, Ljava/util/HashSet;

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    :goto_0
    iget v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ge v1, v9, :cond_14

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit16 v10, v1, 0x100

    aget-object v4, v9, v10

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v6

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v9, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-virtual {v8, v6}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    if-eqz v0, :cond_13

    iget v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v9, v1, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_3

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_2

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    :cond_2
    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    const-wide/16 v9, -0x1

    iput-wide v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    :cond_3
    iget-wide v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    invoke-virtual {v4}, Lcom/android/gallery3d/data/MediaObject;->getDataVersion()J

    move-result-wide v11

    cmp-long v9, v9, v11

    if-eqz v9, :cond_4

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    instance-of v9, v9, Lcom/android/gallery3d/ui/BitmapScreenNail;

    if-eqz v9, :cond_4

    iget-object v7, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    check-cast v7, Lcom/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {v4}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v9

    invoke-virtual {v4}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v10

    invoke-virtual {v7, v9, v10}, Lcom/android/gallery3d/ui/BitmapScreenNail;->updatePlaceholderSize(II)V

    :cond_4
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v9, :cond_6

    iget v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v9, v1, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    if-lez v9, :cond_6

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_5

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    :cond_5
    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    const-wide/16 v9, -0x1

    iput-wide v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedGif:J

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_6

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_6
    iget v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v9, v1, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    if-lez v9, :cond_9

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    :cond_7
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_8

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_8
    const-wide/16 v9, -0x1

    iput-wide v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedOriginScreenNail:J

    :cond_9
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v9, :cond_c

    iget v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v9, v1, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    if-lez v9, :cond_c

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_a

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    :cond_a
    const-wide/16 v9, -0x1

    iput-wide v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedSecondScreenNail:J

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_b

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_b
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_c

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_c
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v9, :cond_0

    iget v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v9, v1, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    if-lez v9, :cond_0

    const-string v9, "Gallery2/PhotoDataAdapter"

    const-string v10, "updateImageCache: release mav"

    invoke-static {v9, v10}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_d

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    :cond_d
    const/4 v9, 0x0

    iput v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoTotalCount:I

    const-wide/16 v9, -0x1

    iput-wide v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedMav:J

    const/4 v9, 0x1

    iput-boolean v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->isMpoFrameRecyled:Z

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_e

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_e
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v9, :cond_10

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_f

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_f
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_10

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_10
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_0

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    array-length v5, v9

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_12

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    aget-object v9, v9, v3

    if-eqz v9, :cond_11

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    aget-object v9, v9, v3

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    const/4 v10, 0x0

    aput-object v10, v9, v3

    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_12
    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    goto/16 :goto_1

    :cond_13
    new-instance v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    const/4 v9, 0x0

    invoke-direct {v0, v9}, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter$1;)V

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v9, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_14
    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_15
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/gallery3d/data/Path;

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v9, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_16

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_16
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_17

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_17
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_18

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_18
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v9, :cond_1a

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_19

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_19
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_1a

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_1a
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v9, :cond_21

    const-string v9, "Gallery2/PhotoDataAdapter"

    const-string v10, "updateImageCache: release mav"

    invoke-static {v9, v10}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_1b

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_1b
    const/4 v9, 0x0

    iput v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoTotalCount:I

    const/4 v9, 0x1

    iput-boolean v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->isMpoFrameRecyled:Z

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_1c

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_1c
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v9, :cond_1e

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_1d

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_1d
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_1e

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondMpoFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_1e
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_21

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    array-length v5, v9

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v5, :cond_20

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    aget-object v9, v9, v3

    if-eqz v9, :cond_1f

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    aget-object v9, v9, v3

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    const/4 v10, 0x0

    aput-object v10, v9, v3

    :cond_1f
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_20
    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    :cond_21
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_22

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_22

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_22
    sget-boolean v9, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v9, :cond_15

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v9, :cond_23

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v9}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_23
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_24

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_24
    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_15

    iget-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    const/4 v9, 0x0

    iput-object v9, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    goto/16 :goto_3

    :cond_25
    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->stopProfilePhotoPageUpdateImageCache()V

    return-void
.end method

.method private updateImageRequests()V
    .locals 13

    const-wide/16 v11, -0x1

    const/4 v10, 0x0

    iget-boolean v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit16 v9, v1, 0x100

    aget-object v5, v8, v9

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v8

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    if-ne v8, v9, :cond_0

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->startProfilePhotoPageUpdateImageRequest()V

    const/4 v7, 0x0

    const/4 v3, 0x0

    :goto_1
    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    array-length v8, v8

    if-ge v3, v8, :cond_4

    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    aget-object v8, v8, v3

    iget v6, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;->indexOffset:I

    sget-object v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->sImageFetchSeq:[Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    aget-object v8, v8, v3

    iget v0, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageFetch;->imageBit:I

    const/4 v8, 0x2

    if-ne v0, v8, :cond_3

    iget-boolean v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mNeedFullImage:Z

    if-nez v8, :cond_3

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    add-int v8, v1, v6

    invoke-direct {p0, v8, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->startTaskIfNeeded(II)Lcom/android/gallery3d/util/Future;

    move-result-object v7

    if-eqz v7, :cond_2

    :cond_4
    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_6

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_6

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    :cond_6
    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_7

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_7

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    :cond_7
    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v8, :cond_8

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_8

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_8

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedGif:J

    :cond_8
    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v8, :cond_9

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_9

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_9

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedSecondScreenNail:J

    :cond_9
    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_a

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_a

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedOriginScreenNail:J

    :cond_a
    sget-boolean v8, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v8, :cond_5

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v8, :cond_5

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_5

    iget-object v8, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v10, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    iput-wide v11, v2, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedMav:J

    goto :goto_2

    :cond_b
    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->stopProfilePhotoPageUpdateImageRequest()V

    goto/16 :goto_0
.end method

.method private updateMavDecoder(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;Lcom/android/gallery3d/data/MediaItem;I)V
    .locals 10
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p3    # Lcom/android/gallery3d/data/MediaItem;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;",
            "Lcom/android/gallery3d/data/MediaItem;",
            "I)V"
        }
    .end annotation

    const-string v5, "Gallery2/PhotoDataAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ">> updateMavDecoder, type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v3, :cond_0

    iget-object v5, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eq v5, p2, :cond_1

    :cond_0
    const-string v5, "Gallery2/PhotoDataAdapter"

    const-string v6, "updateMpoDecoder:invalid entry or future"

    invoke-static {v5, v6}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    if-nez p4, :cond_7

    const/4 v5, 0x0

    iput-object v5, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget v5, v5, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->mpoTotalCount:I

    iput v5, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoTotalCount:I

    const-string v5, "Gallery2/PhotoDataAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "the mav total count is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoTotalCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    iget v6, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoTotalCount:I

    add-int/lit8 v6, v6, -0x1

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;->setSeekBar(II)V

    :cond_2
    new-instance v4, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v4}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inMpoFrames:Z

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/mediatek/gallery3d/util/MediatekFeature;->availableMemoryForMavPlayback(Lcom/android/gallery3d/app/AbstractGalleryActivity;)J

    move-result-wide v0

    const-wide/32 v5, 0x6400000

    cmp-long v5, v0, v5

    if-lez v5, :cond_5

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v5

    iput v5, v4, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inTargetDisplayHeight:I

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v5

    iput v5, v4, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inTargetDisplayWidth:I

    :cond_3
    :goto_1
    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inPQEnhance:Z

    const-string v5, "Gallery2/PhotoDataAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "display width: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", height: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    invoke-virtual {p3, v5}, Lcom/android/gallery3d/data/MediaItem;->setMavListener(Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;)V

    const-string v5, "Gallery2/PhotoDataAdapter"

    const-string v6, "start load all mav frames"

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    const/4 v6, 0x1

    invoke-virtual {p3, v6, v4}, Lcom/android/gallery3d/data/MediaItem;->requestImage(ILcom/mediatek/gallery3d/util/MediatekFeature$Params;)Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v6

    new-instance v7, Lcom/android/gallery3d/app/PhotoDataAdapter$MavDecoderListener;

    invoke-virtual {p3}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct {v7, p0, v8, p3, v9}, Lcom/android/gallery3d/app/PhotoDataAdapter$MavDecoderListener;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/data/MediaItem;I)V

    invoke-virtual {v5, v6, v7}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v5

    iput-object v5, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    :cond_4
    :goto_2
    const-string v5, "Gallery2/PhotoDataAdapter"

    const-string v6, "<< updateMavDecoder"

    invoke-static {v5, v6}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const-wide/32 v5, 0x6400000

    cmp-long v5, v0, v5

    if-gtz v5, :cond_6

    const-wide/32 v5, 0x3200000

    cmp-long v5, v0, v5

    if-lez v5, :cond_6

    const-string v5, "Gallery2/PhotoDataAdapter"

    const-string v6, "no enough memory, degrade sample rate to 1/4"

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    iput v5, v4, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inTargetDisplayHeight:I

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    iput v5, v4, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inTargetDisplayWidth:I

    goto :goto_1

    :cond_6
    const-wide/32 v5, 0x3200000

    cmp-long v5, v0, v5

    if-gtz v5, :cond_3

    const-string v5, "Gallery2/PhotoDataAdapter"

    const-string v6, "no enough memory, degrade sample rate to 1/9"

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x3

    iput v5, v4, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inTargetDisplayHeight:I

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x3

    iput v5, v4, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inTargetDisplayWidth:I

    goto/16 :goto_1

    :cond_7
    const/4 v5, 0x1

    if-ne p4, v5, :cond_4

    const/4 v5, 0x0

    iput-object v5, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v5, v5, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->mpoFrames:[Landroid/graphics/Bitmap;

    invoke-direct {p0, v5}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getScreenNails([Landroid/graphics/Bitmap;)[Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v5

    iput-object v5, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    const-string v5, "Gallery2/PhotoDataAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mpo frame width: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", height: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-interface {v7}, Lcom/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    iput-boolean v5, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->isMpoFrameRecyled:Z

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavLoadingFinished:Z

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    iget-boolean v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavLoadingFinished:Z

    invoke-virtual {v5, v6}, Lcom/android/gallery3d/ui/PhotoView;->setMavLoadingFinished(Z)V

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->startMavPlayback(Lcom/android/gallery3d/data/Path;)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto/16 :goto_2
.end method

.method private updateOriginScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/android/gallery3d/ui/ScreenNail;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v2, p2, :cond_2

    :cond_0
    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v1, :cond_1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/gallery3d/ui/ScreenNail;

    iput-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->originScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    if-ne p1, v2, :cond_3

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    :cond_3
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto :goto_0
.end method

.method private updateScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 6
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/android/gallery3d/ui/ScreenNail;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->startProfilePhotoPageDecodeScreenNailListener()V

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v5, p2, :cond_2

    :cond_0
    if-eqz v4, :cond_1

    invoke-interface {v4}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v5, 0x0

    iput-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-nez v4, :cond_6

    const/4 v2, 0x1

    :goto_1
    iget-object v5, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    instance-of v5, v5, Lcom/android/gallery3d/ui/BitmapScreenNail;

    if-eqz v5, :cond_3

    iget-object v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    check-cast v3, Lcom/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {v3, v4}, Lcom/android/gallery3d/ui/BitmapScreenNail;->combine(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v4

    :cond_3
    if-nez v4, :cond_7

    iput-boolean v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    :goto_2
    const/4 v1, -0x3

    :goto_3
    const/4 v5, 0x3

    if-gt v1, v5, :cond_5

    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v5, v1

    invoke-direct {p0, v5}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v5

    if-ne p1, v5, :cond_8

    if-nez v1, :cond_4

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    :cond_4
    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v5, v1}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    :cond_5
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->stopProfilePhotoPageDecodeScreenNailListener()V

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    goto :goto_1

    :cond_7
    iput-boolean v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    iput-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_2

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private updateScreenNailSize(Lcom/android/gallery3d/ui/ScreenNail;Lcom/android/gallery3d/data/MediaItem;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/ui/ScreenNail;
    .param p2    # Lcom/android/gallery3d/data/MediaItem;

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getImageOptions()Lcom/mediatek/gallery3d/ext/IImageOptions;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IImageOptions;->shouldUseOriginalSize()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method

.method private updateScreenNailUploadQueue()V
    .locals 2

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mUploader:Lcom/android/gallery3d/ui/TiledTexture$Uploader;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/TiledTexture$Uploader;->clear()V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->uploadScreenNail(I)V

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->uploadScreenNail(I)V

    neg-int v1, v0

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->uploadScreenNail(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateSecondScreenNail(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/util/Future;)V
    .locals 7
    .param p1    # Lcom/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/data/Path;",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v1, :cond_0

    iget-object v4, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eq v4, p2, :cond_2

    :cond_0
    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->recycle()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    if-eqz v0, :cond_3

    iget-object v4, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_3

    iget-object v4, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFrame:Landroid/graphics/Bitmap;

    if-nez v4, :cond_4

    :goto_1
    iput-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    new-instance v3, Lcom/android/gallery3d/ui/BitmapScreenNail;

    iget-object v4, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    if-ne p1, v3, :cond_3

    const-string v3, "Gallery2/PhotoDataAdapter"

    const-string v4, "updateSecondScreenNail:update tileProvider"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v5}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    rem-int/lit16 v4, v4, 0x100

    aget-object v2, v3, v4

    invoke-virtual {v2}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v3

    const/high16 v4, 0x80000

    and-int/2addr v3, v4

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v6}, Lcom/android/gallery3d/ui/PhotoView;->allowStereoMode(Z)V

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v6}, Lcom/android/gallery3d/ui/PhotoView;->setStereoMode(Z)V

    :cond_3
    :goto_2
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto :goto_0

    :cond_4
    new-instance v3, Lcom/android/gallery3d/ui/BitmapScreenNail;

    iget-object v4, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFrame:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v5}, Lcom/android/gallery3d/ui/PhotoView;->allowStereoMode(Z)V

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v5}, Lcom/android/gallery3d/ui/PhotoView;->setStereoMode(Z)V

    goto :goto_2
.end method

.method private updateSlidingWindow()V
    .locals 6

    const/4 v5, 0x0

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->startProfilePhotoPageUpdateSlidingWindow()V

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/lit8 v3, v3, -0x3

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v4, v4, -0x7

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v5, v4}, Lcom/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v4, v2, 0x7

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-ne v3, v2, :cond_0

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ne v3, v0, :cond_0

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->stopProfilePhotoPageUpdateSlidingWindow()V

    :goto_0
    return-void

    :cond_0
    iput v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    iput v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/lit8 v3, v3, -0x80

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit16 v4, v4, -0x100

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v5, v4}, Lcom/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit16 v4, v2, 0x100

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-gt v3, v4, :cond_1

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt v3, v4, :cond_1

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    sub-int v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/16 v4, 0x10

    if-le v3, v4, :cond_5

    :cond_1
    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    :goto_1
    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge v1, v3, :cond_4

    if-lt v1, v2, :cond_2

    if-lt v1, v0, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit16 v4, v1, 0x100

    const/4 v5, 0x0

    aput-object v5, v3, v4

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iput v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    iput v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v3}, Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->notifyDirty()V

    :cond_5
    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->stopProfilePhotoPageUpdateSlidingWindow()V

    goto :goto_0
.end method

.method private updateTileProvider()V
    .locals 3

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->clear()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    goto :goto_0
.end method

.method private updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V
    .locals 7
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->startProfilePhotoPageUpdateTileProvider()V

    iget-object v2, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    iget-object v0, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v2, :cond_2

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProviderEx(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "Gallery2/PhotoDataAdapter"

    const-string v5, "updateTileProvider:we return!"

    invoke-static {v4, v5}, Lcom/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->stopProfilePhotoPageUpdateTileProvider()V

    :goto_0
    return-void

    :cond_0
    if-eqz v0, :cond_1

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v6

    invoke-virtual {v4, v2, v5, v6}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/android/gallery3d/ui/ScreenNail;II)V

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v4, v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setRegionDecoder(Landroid/graphics/BitmapRegionDecoder;)V

    :goto_1
    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekMMProfile;->stopProfilePhotoPageUpdateTileProvider()V

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Lcom/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v3

    invoke-interface {v2}, Lcom/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v1

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v4, v2, v3, v1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/android/gallery3d/ui/ScreenNail;II)V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v4}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->clear()V

    goto :goto_1
.end method

.method private updateTileProviderEx(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Z
    .locals 12
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    const/4 v8, 0x1

    const/4 v9, 0x0

    if-eqz p1, :cond_1

    iget-object v7, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_1

    move v7, v8

    :goto_0
    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    iget-object v7, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-static {v7}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getScreenNailSubType(Lcom/android/gallery3d/ui/ScreenNail;)I

    move-result v4

    invoke-static {v4}, Lcom/mediatek/gallery3d/util/MediatekFeature;->showDrmMicroThumb(I)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    :goto_1
    return v9

    :cond_1
    move v7, v9

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getTargetScreenNail(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v7, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-static {v5, v7}, Lcom/mediatek/gallery3d/util/MediatekFeature;->syncSubType(Lcom/android/gallery3d/ui/ScreenNail;Lcom/android/gallery3d/ui/ScreenNail;)Z

    invoke-static {v5}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getSizeForSubtype(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/android/gallery3d/ui/PhotoView$Size;

    move-result-object v3

    if-eqz v3, :cond_5

    iget v6, v3, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    iget v2, v3, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    :goto_2
    iget-object v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v7, v5, v6, v2}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/android/gallery3d/ui/ScreenNail;II)V

    sget-boolean v7, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    iget-object v10, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-virtual {v7, v8, v10}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setStereoScreenNail(ILcom/android/gallery3d/ui/ScreenNail;)V

    iget-object v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    const/4 v10, 0x2

    iget-object v11, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-virtual {v7, v10, v11}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setStereoScreenNail(ILcom/android/gallery3d/ui/ScreenNail;)V

    :cond_3
    iget-object v7, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v7, :cond_4

    iget v7, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoLayout:I

    iget-object v10, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v10}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v10

    invoke-static {v8, v7, v10}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->adjustDim(ZII)I

    move-result v1

    iget v7, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->stereoLayout:I

    iget-object v10, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v10}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v10

    invoke-static {v9, v7, v10}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->adjustDim(ZII)I

    move-result v0

    iget-object v7, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    iget-object v9, p1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v7, v9, v5, v1, v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setRegionDecoder(Landroid/graphics/BitmapRegionDecoder;Lcom/android/gallery3d/ui/ScreenNail;II)V

    :cond_4
    move v9, v8

    goto :goto_1

    :cond_5
    invoke-interface {v5}, Lcom/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v6

    invoke-interface {v5}, Lcom/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v2

    goto :goto_2
.end method

.method private uploadScreenNail(I)V
    .locals 7
    .param p1    # I

    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int v1, v5, p1

    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt v1, v5, :cond_0

    iget v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt v1, v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    instance-of v5, v3, Lcom/android/gallery3d/ui/TiledScreenNail;

    if-eqz v5, :cond_0

    check-cast v3, Lcom/android/gallery3d/ui/TiledScreenNail;

    invoke-virtual {v3}, Lcom/android/gallery3d/ui/TiledScreenNail;->getTexture()Lcom/android/gallery3d/ui/TiledTexture;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/android/gallery3d/ui/TiledTexture;->isReady()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mUploader:Lcom/android/gallery3d/ui/TiledTexture$Uploader;

    invoke-virtual {v5, v4}, Lcom/android/gallery3d/ui/TiledTexture$Uploader;->addTexture(Lcom/android/gallery3d/ui/TiledTexture;)V

    goto :goto_0
.end method


# virtual methods
.method public cancelCurrentMavDecodeTask()V
    .locals 3

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    sget-boolean v1, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v1}, Lcom/android/gallery3d/util/Future;->cancel()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoTotalCount:I

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedMav:J

    :cond_1
    return-void
.end method

.method public enterConsumeMode()V
    .locals 5

    const/4 v4, 0x1

    sget-boolean v2, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_1

    const-string v2, "Gallery2/PhotoDataAdapter"

    const-string v3, "enter is null"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-boolean v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    if-eqz v2, :cond_2

    const-string v2, "Gallery2/PhotoDataAdapter"

    const-string v3, "enterConsumeMode:we already in consumed mode!"

    invoke-static {v2, v3}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iput-boolean v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItemInternal(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->isTimeInterval()Z

    move-result v2

    if-eqz v2, :cond_3

    iput-boolean v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTimeIntervalDRM:Z

    :cond_3
    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateDrmScreenNail()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->startGifAnimation(Lcom/android/gallery3d/data/Path;)V

    goto :goto_0
.end method

.method public enteredConsumeMode()Z
    .locals 3

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-boolean v1, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->enteredConsumeMode:Z

    goto :goto_0
.end method

.method public getCurrentIndex()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    return v0
.end method

.method public getImageHeight()I
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getImageHeight()I

    move-result v0

    return v0
.end method

.method public getImageRotation(I)I
    .locals 2
    .param p1    # I

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v1, p1

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v1

    goto :goto_0
.end method

.method public getImageSize(ILcom/android/gallery3d/ui/PhotoView$Size;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/gallery3d/ui/PhotoView$Size;

    const/4 v2, 0x0

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v1, p1

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    iput v2, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    iput v2, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v1

    iput v1, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v1

    iput v1, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    goto :goto_0
.end method

.method public getImageWidth()I
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getImageWidth()I

    move-result v0

    return v0
.end method

.method public getLevelCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getLevelCount()I

    move-result v0

    return v0
.end method

.method public getLoadingState(I)I
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v3, p1

    invoke-direct {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-boolean v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    if-eqz v2, :cond_2

    const/4 v1, 0x2

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;
    .locals 3
    .param p1    # I

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int v0, v1, p1

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/android/gallery3d/data/MediaItem;

    rem-int/lit16 v2, v0, 0x100

    aget-object v1, v1, v2

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getScreenNail()Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method

.method public getScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 6
    .param p1    # I

    const/4 v3, 0x0

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int v1, v4, p1

    if-ltz v1, :cond_0

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-ge v1, v4, :cond_0

    iget-boolean v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt v1, v4, :cond_3

    iget v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ge v1, v4, :cond_3

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    invoke-direct {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getTargetScreenNail(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-nez v4, :cond_2

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->newPlaceholderScreenNail(Lcom/android/gallery3d/data/MediaItem;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v4

    iput-object v4, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-nez p1, :cond_2

    invoke-direct {p0, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    :cond_2
    iget-object v3, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getStereoScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getStereoScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method

.method public getTile(IIIIILcom/android/gallery3d/data/BitmapPool;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/android/gallery3d/data/BitmapPool;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getTile(IIIIILcom/android/gallery3d/data/BitmapPool;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getTotalFrameCount()I
    .locals 5

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v3}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    :cond_0
    if-eqz v1, :cond_1

    iget v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoTotalCount:I

    :cond_1
    return v3

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCamera(I)Z
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDeletable(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v2, p1

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMav(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v2, p1

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->getSubType()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    goto :goto_0
.end method

.method public isMavLoadingFinished()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavLoadingFinished:Z

    return v0
.end method

.method public isPanorama(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsPanorama:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStaticCamera(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStaticCamera:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideo(I)Z
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v2, p1

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public moveTo(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateCurrentIndex(I)V

    return-void
.end method

.method public pause()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v3}, Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->terminate()V

    iput-object v6, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/android/gallery3d/data/MediaSet;

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceListener:Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;

    invoke-virtual {v3, v4}, Lcom/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/android/gallery3d/data/ContentListener;)V

    sget-boolean v3, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsDrmSupported:Z

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->saveDrmConsumeStatus()V

    :cond_0
    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v3}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_2
    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v3, :cond_3

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v3}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_3
    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_4

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v3}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_4
    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isGifSupported()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v3, :cond_5

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->gifDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v3}, Lcom/android/gallery3d/util/Future;->cancel()V

    :cond_5
    sget-boolean v3, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v3, :cond_7

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    if-eqz v3, :cond_7

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNailTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v3}, Lcom/android/gallery3d/util/Future;->cancel()V

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_6

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v3}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_6
    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->firstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_7

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->secondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v3}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    :cond_7
    invoke-virtual {p0, v5}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-boolean v3, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getSubType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v3, :cond_8

    const-string v3, "TAG"

    const-string v4, "cancel decoder task when pause"

    invoke-static {v3, v4}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v3}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-object v6, v1, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoDecoderTask:Lcom/android/gallery3d/util/Future;

    :cond_8
    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    monitor-enter v4

    const/4 v3, 0x0

    :try_start_0
    iput-boolean v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderRequested:Z

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v4

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_9
    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v3}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->clear()V

    iget-object v3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mUploader:Lcom/android/gallery3d/ui/TiledTexture$Uploader;

    invoke-virtual {v3}, Lcom/android/gallery3d/ui/TiledTexture$Uploader;->clear()V

    invoke-static {}, Lcom/android/gallery3d/ui/TiledTexture;->freeResources()V

    return-void
.end method

.method public resume()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    invoke-static {}, Lcom/android/gallery3d/ui/TiledTexture;->prepareResources()V

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mSourceListener:Lcom/android/gallery3d/app/PhotoDataAdapter$SourceListener;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/data/MediaSet;->addContentListener(Lcom/android/gallery3d/data/ContentListener;)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    new-instance v0, Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;-><init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/app/PhotoDataAdapter$1;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    const-string v1, "reloadtask-PDA"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    return-void
.end method

.method public setCurrentPhoto(Lcom/android/gallery3d/data/Path;I)V
    .locals 2
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # I

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    if-ne v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/android/gallery3d/data/Path;

    iput p2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->updateDrmScreenNail()V

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v1

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v1}, Lcom/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->notifyDirty()V

    goto :goto_0
.end method

.method public setDataListener(Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/android/gallery3d/app/PhotoDataAdapter$DataListener;

    return-void
.end method

.method public setFocusHintDirection(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintDirection:I

    return-void
.end method

.method public setFocusHintPath(Lcom/android/gallery3d/data/Path;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/data/Path;

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/android/gallery3d/data/Path;

    return-void
.end method

.method public setImageBitmap(I)V
    .locals 8
    .param p1    # I

    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v2

    :goto_0
    if-nez v2, :cond_0

    const-string v5, "Gallery2/PhotoDataAdapter"

    const-string v6, "setImageBitmap: the currentPath is null"

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    if-nez v3, :cond_2

    const-string v5, "Gallery2/PhotoDataAdapter"

    const-string v6, "setImageBitmap: the mavEntry is null"

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    iget-object v5, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    if-nez v5, :cond_3

    const-string v5, "Gallery2/PhotoDataAdapter"

    const-string v6, "setImageBitmap: the mpoFrames of current entry is null"

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v5, v3, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    array-length v0, v5

    if-ltz p1, :cond_4

    if-ge p1, v0, :cond_4

    iput p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mCurrentMpoIndex:I

    const-string v5, "Gallery2/PhotoDataAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get current mpo frame, index: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-boolean v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavStereoMode:Z

    if-eqz v5, :cond_7

    const/4 v5, 0x1

    if-le v0, v5, :cond_5

    add-int/lit8 v4, p1, 0x1

    if-gez v4, :cond_6

    move v4, p1

    :cond_5
    :goto_2
    iput v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mNextMpoIndex:I

    const-string v5, "Gallery2/PhotoDataAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get next mpo frame, index: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter;->requestRender()V

    goto :goto_1

    :cond_6
    add-int/lit8 v5, v0, -0x1

    if-le v4, v5, :cond_5

    add-int/lit8 v4, v0, -0x1

    goto :goto_2

    :cond_7
    move v4, p1

    goto :goto_2
.end method

.method public setMavListener(Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    return-void
.end method

.method public setNeedFullImage(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mNeedFullImage:Z

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public updateMavStereoMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter;->mIsMavStereoMode:Z

    return-void
.end method
