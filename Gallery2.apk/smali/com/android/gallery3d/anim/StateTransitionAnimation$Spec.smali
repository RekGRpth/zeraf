.class public Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;
.super Ljava/lang/Object;
.source "StateTransitionAnimation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/anim/StateTransitionAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Spec"
.end annotation


# static fields
.field private static final DEFAULT_INTERPOLATOR:Landroid/view/animation/Interpolator;

.field public static final INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

.field public static final OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

.field public static final PHOTO_INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;


# instance fields
.field public backgroundAlphaFrom:F

.field public backgroundAlphaTo:F

.field public backgroundScaleFrom:F

.field public backgroundScaleTo:F

.field public contentAlphaFrom:F

.field public contentAlphaTo:F

.field public contentScaleFrom:F

.field public contentScaleTo:F

.field public duration:I

.field public interpolator:Landroid/view/animation/Interpolator;

.field public overlayAlphaFrom:F

.field public overlayAlphaTo:F

.field public overlayScaleFrom:F

.field public overlayScaleTo:F


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/high16 v4, 0x40400000

    const/high16 v3, 0x3f000000

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->DEFAULT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    new-instance v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    invoke-direct {v0}, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;-><init>()V

    sput-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v3, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundAlphaFrom:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v1, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundAlphaTo:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v2, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundScaleFrom:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v1, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundScaleTo:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v3, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentAlphaFrom:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v2, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentAlphaTo:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v4, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentScaleFrom:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v2, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentScaleTo:F

    new-instance v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    invoke-direct {v0}, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;-><init>()V

    sput-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v2, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayAlphaFrom:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v1, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayAlphaTo:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v2, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayScaleFrom:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v4, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayScaleTo:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v1, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentAlphaFrom:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v2, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentAlphaTo:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    const/high16 v1, 0x3e800000

    iput v1, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentScaleFrom:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    iput v2, v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentScaleTo:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    sput-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->PHOTO_INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/high16 v2, 0x3f800000

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x14a

    iput v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->duration:I

    iput v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundAlphaFrom:F

    iput v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundAlphaTo:F

    iput v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundScaleFrom:F

    iput v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->backgroundScaleTo:F

    iput v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentAlphaFrom:F

    iput v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentAlphaTo:F

    iput v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentScaleFrom:F

    iput v2, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->contentScaleTo:F

    iput v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayAlphaFrom:F

    iput v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayAlphaTo:F

    iput v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayScaleFrom:F

    iput v1, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->overlayScaleTo:F

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->DEFAULT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    iput-object v0, p0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->interpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method static synthetic access$000(Lcom/android/gallery3d/anim/StateTransitionAnimation$Transition;)Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;
    .locals 1
    .param p0    # Lcom/android/gallery3d/anim/StateTransitionAnimation$Transition;

    invoke-static {p0}, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->specForTransition(Lcom/android/gallery3d/anim/StateTransitionAnimation$Transition;)Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    move-result-object v0

    return-object v0
.end method

.method private static specForTransition(Lcom/android/gallery3d/anim/StateTransitionAnimation$Transition;)Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;
    .locals 2
    .param p0    # Lcom/android/gallery3d/anim/StateTransitionAnimation$Transition;

    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$1;->$SwitchMap$com$android$gallery3d$anim$StateTransitionAnimation$Transition:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->OUTGOING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;->PHOTO_INCOMING:Lcom/android/gallery3d/anim/StateTransitionAnimation$Spec;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
