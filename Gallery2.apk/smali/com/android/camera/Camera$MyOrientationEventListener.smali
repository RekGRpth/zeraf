.class Lcom/android/camera/Camera$MyOrientationEventListener;
.super Landroid/view/OrientationEventListener;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyOrientationEventListener"
.end annotation


# instance fields
.field private mLock:Z

.field private mRestoreOrientation:I

.field final synthetic this$0:Lcom/android/camera/Camera;


# direct methods
.method public constructor <init>(Lcom/android/camera/Camera;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->mLock:Z

    return-void
.end method


# virtual methods
.method public getLock()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->mLock:Z

    return v0
.end method

.method public onOrientationChanged(I)V
    .locals 5
    .param p1    # I

    const/16 v4, 0x10e

    const/16 v3, 0x5a

    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->mLock:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {p1, v3}, Lcom/android/camera/Util;->roundOrientation(II)I

    move-result v2

    if-eq v2, v3, :cond_1

    invoke-static {p1, v4}, Lcom/android/camera/Util;->roundOrientation(II)I

    move-result v2

    if-ne v2, v4, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/actor/CameraActor;->onDisplayRotate()V

    :cond_2
    iput p1, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->mRestoreOrientation:I

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v2

    iget-object v3, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v3}, Lcom/android/camera/Camera;->access$500(Lcom/android/camera/Camera;)I

    move-result v3

    if-eq v2, v3, :cond_4

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$600(Lcom/android/camera/Camera;)V

    :cond_4
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraOnOrientChange()V

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$700(Lcom/android/camera/Camera;)I

    move-result v2

    invoke-static {p1, v2}, Lcom/android/camera/Util;->roundOrientation(II)I

    move-result v0

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$700(Lcom/android/camera/Camera;)I

    move-result v2

    if-eq v2, v0, :cond_5

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/camera/actor/CameraActor;->onOrientationChanged(I)V

    :cond_5
    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2, v0}, Lcom/android/camera/Camera;->access$702(Lcom/android/camera/Camera;I)I

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$700(Lcom/android/camera/Camera;)I

    move-result v2

    iget-object v3, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v3}, Lcom/android/camera/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v3

    add-int/2addr v2, v3

    rem-int/lit16 v1, v2, 0x168

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$4600(Lcom/android/camera/Camera;)I

    move-result v2

    if-eq v2, v1, :cond_7

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2, v1}, Lcom/android/camera/Camera;->access$4602(Lcom/android/camera/Camera;I)I

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$1100(Lcom/android/camera/Camera;)V

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->isVideoWallPaperIntent()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$4700(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ReviewManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$3700(Lcom/android/camera/Camera;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$1400(Lcom/android/camera/Camera;Z)V

    :cond_6
    iget-object v2, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/Camera$MyOrientationEventListener$1;

    invoke-direct {v3, p0}, Lcom/android/camera/Camera$MyOrientationEventListener$1;-><init>(Lcom/android/camera/Camera$MyOrientationEventListener;)V

    invoke-virtual {v2, v3}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    :cond_7
    invoke-static {}, Lcom/android/camera/Camera;->access$000()Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "Camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onOrientationChanged("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") mOrientation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$700(Lcom/android/camera/Camera;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mOrientationCompensation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$4600(Lcom/android/camera/Camera;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraOnOrientChange()V

    goto/16 :goto_0
.end method

.method public setLock(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->mLock:Z

    iget-boolean v0, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->mLock:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/camera/Camera$MyOrientationEventListener;->mRestoreOrientation:I

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera$MyOrientationEventListener;->onOrientationChanged(I)V

    :cond_0
    return-void
.end method
