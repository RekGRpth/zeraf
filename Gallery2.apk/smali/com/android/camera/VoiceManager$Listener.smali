.class public interface abstract Lcom/android/camera/VoiceManager$Listener;
.super Ljava/lang/Object;
.source "VoiceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/VoiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# static fields
.field public static final VOICE_COMMAND_CAPTURE:I = 0x3

.field public static final VOICE_COMMAND_CHEESE:I = 0x4


# virtual methods
.method public abstract onUserGuideUpdated(Ljava/lang/String;)V
.end method

.method public abstract onVoiceCommandReceive(I)V
.end method

.method public abstract onVoiceValueUpdated(Ljava/lang/String;)V
.end method
