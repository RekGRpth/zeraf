.class public Lcom/android/camera/ModeChecker;
.super Ljava/lang/Object;
.source "ModeChecker.java"


# static fields
.field private static final LOG:Z

.field private static final MATRIX_NORMAL_ENABLE:[[Z

.field private static final MATRIX_PREVIEW3D_ENABLE:[[Z

.field private static final MATRIX_SINGLE3D_ENABLE:[[Z

.field private static final MODE_STRING_NORMAL:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "ModeChecker"


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/ModeChecker;->LOG:Z

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    const/16 v0, 0xa

    new-array v0, v0, [[Z

    sput-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    const/16 v0, 0xa

    new-array v0, v0, [[Z

    sput-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    const/16 v0, 0xa

    new-array v0, v0, [[Z

    sput-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    sget-object v0, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    const-string v1, "normal"

    aput-object v1, v0, v4

    sget-object v0, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    const-string v1, "hdr"

    aput-object v1, v0, v5

    sget-object v0, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    const-string v1, "face_beauty"

    aput-object v1, v0, v3

    sget-object v0, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    const-string v1, "autorama"

    aput-object v1, v0, v6

    sget-object v0, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    const-string v1, "mav"

    aput-object v1, v0, v7

    sget-object v0, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    const/4 v1, 0x5

    const-string v2, "asd"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    const/4 v1, 0x6

    const-string v2, "smileshot"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    const/4 v1, 0x7

    const-string v2, "bestshot"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    const/16 v1, 0x8

    const-string v2, "evbracketshot"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_4

    aput-object v1, v0, v7

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    const/4 v1, 0x5

    new-array v2, v3, [Z

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    const/4 v1, 0x6

    new-array v2, v3, [Z

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    const/4 v1, 0x7

    new-array v2, v3, [Z

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    const/16 v1, 0x8

    new-array v2, v3, [Z

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    const/16 v1, 0x9

    new-array v2, v3, [Z

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_a

    aput-object v1, v0, v4

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_b

    aput-object v1, v0, v5

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_c

    aput-object v1, v0, v3

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_d

    aput-object v1, v0, v6

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_e

    aput-object v1, v0, v7

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    const/4 v1, 0x5

    new-array v2, v3, [Z

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    const/4 v1, 0x6

    new-array v2, v3, [Z

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    const/4 v1, 0x7

    new-array v2, v3, [Z

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    const/16 v1, 0x8

    new-array v2, v3, [Z

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    const/16 v1, 0x9

    new-array v2, v3, [Z

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_14

    aput-object v1, v0, v4

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_15

    aput-object v1, v0, v5

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_16

    aput-object v1, v0, v3

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_17

    aput-object v1, v0, v6

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    new-array v1, v3, [Z

    fill-array-data v1, :array_18

    aput-object v1, v0, v7

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    const/4 v1, 0x5

    new-array v2, v3, [Z

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    const/4 v1, 0x6

    new-array v2, v3, [Z

    fill-array-data v2, :array_1a

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    const/4 v1, 0x7

    new-array v2, v3, [Z

    fill-array-data v2, :array_1b

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    const/16 v1, 0x8

    new-array v2, v3, [Z

    fill-array-data v2, :array_1c

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    const/16 v1, 0x9

    new-array v2, v3, [Z

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    return-void

    :array_0
    .array-data 1
        0x1t
        0x1t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x1t
        0x1t
    .end array-data

    nop

    :array_3
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_4
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_5
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_6
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_7
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_8
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_9
    .array-data 1
        0x1t
        0x1t
    .end array-data

    nop

    :array_a
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_b
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_c
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_d
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_e
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_f
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_10
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_11
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_12
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_13
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_14
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_15
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_16
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_17
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_18
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_19
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_1a
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_1b
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_1c
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_1d
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCameraPickerVisible(Lcom/android/camera/Camera;)Z
    .locals 9
    .param p0    # Lcom/android/camera/Camera;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/camera/Camera;->getCameraCount()I

    move-result v0

    const/4 v7, 0x2

    if-ge v0, v7, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isBothCameraSupportLiveEffect()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p0}, Lcom/android/camera/Camera;->effectsActive()Z

    move-result v7

    if-nez v7, :cond_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isStereoMode()Z

    move-result v4

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isStereoSingle3d()Z

    move-result v7

    if-eqz v7, :cond_4

    if-eqz v4, :cond_4

    sget-object v2, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    :goto_1
    rem-int/lit8 v1, v3, 0x64

    aget-object v7, v2, v1

    aget-boolean v7, v7, v6

    if-eqz v7, :cond_6

    aget-object v7, v2, v1

    aget-boolean v7, v7, v5

    if-eqz v7, :cond_6

    :goto_2
    sget-boolean v6, Lcom/android/camera/ModeChecker;->LOG:Z

    if-eqz v6, :cond_3

    const-string v6, "ModeChecker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getCameraPickerVisible("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") return "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move v6, v5

    goto :goto_0

    :cond_4
    if-eqz v4, :cond_5

    sget-object v2, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    goto :goto_1

    :cond_5
    sget-object v2, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    goto :goto_1

    :cond_6
    move v5, v6

    goto :goto_2
.end method

.method public static getModePickerVisible(Lcom/android/camera/Camera;II)Z
    .locals 7
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isStereoMode()Z

    move-result v2

    invoke-virtual {p0}, Lcom/android/camera/Camera;->effectsActive()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isStereoSingle3d()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v2, :cond_2

    sget-object v1, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    :goto_0
    rem-int/lit8 v0, p2, 0x64

    aget-object v4, v1, v0

    aget-boolean v3, v4, p1

    :cond_0
    :goto_1
    sget-boolean v4, Lcom/android/camera/ModeChecker;->LOG:Z

    if-eqz v4, :cond_1

    const-string v4, "ModeChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getModePickerVisible("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") return "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v3

    :cond_2
    if-eqz v2, :cond_3

    sget-object v1, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    goto :goto_0

    :cond_4
    const/16 v4, 0x9

    if-eq v4, p2, :cond_5

    const/16 v4, 0x6d

    if-ne v4, p2, :cond_0

    :cond_5
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static getStereoPickerVisibile(Lcom/android/camera/Camera;)Z
    .locals 8
    .param p0    # Lcom/android/camera/Camera;

    const/4 v5, 0x0

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isStereo3dEnable()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_0
    return v5

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v0

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isStereoSingle3d()Z

    move-result v6

    if-eqz v6, :cond_2

    sget-object v2, Lcom/android/camera/ModeChecker;->MATRIX_SINGLE3D_ENABLE:[[Z

    :goto_1
    rem-int/lit8 v1, v3, 0x64

    aget-object v6, v2, v1

    aget-boolean v6, v6, v0

    if-eqz v6, :cond_3

    sget-object v6, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    aget-object v6, v6, v1

    aget-boolean v6, v6, v0

    if-eqz v6, :cond_3

    const/4 v4, 0x1

    :goto_2
    sget-boolean v5, Lcom/android/camera/ModeChecker;->LOG:Z

    if-eqz v5, :cond_1

    const-string v5, "ModeChecker"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getStereoPickerVisibile("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") return "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move v5, v4

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/android/camera/ModeChecker;->MATRIX_PREVIEW3D_ENABLE:[[Z

    goto :goto_1

    :cond_3
    move v4, v5

    goto :goto_2
.end method

.method public static updateModeMatrix(Lcom/android/camera/Camera;I)V
    .locals 8
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # I

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSupportedCaptureMode()Ljava/util/List;

    move-result-object v2

    sget-boolean v3, Lcom/android/camera/ModeChecker;->LOG:Z

    if-eqz v3, :cond_0

    const-string v3, "ModeChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateModeMatrix: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSupportedSceneModes()Ljava/util/List;

    move-result-object v1

    sget-boolean v3, Lcom/android/camera/ModeChecker;->LOG:Z

    if-eqz v3, :cond_1

    const-string v3, "ModeChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateModeMatrix: scenemode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isStereo3dEnable()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isStereoMode()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v0, 0x0

    :goto_1
    const/16 v3, 0x9

    if-ge v0, v3, :cond_7

    sget-object v3, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    aget-object v3, v3, v0

    aget-boolean v3, v3, p1

    if-eqz v3, :cond_4

    sget-object v3, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_4

    if-eq v0, v7, :cond_6

    sget-object v3, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    aget-object v3, v3, v0

    aput-boolean v6, v3, p1

    :cond_4
    :goto_2
    sget-boolean v3, Lcom/android/camera/ModeChecker;->LOG:Z

    if-eqz v3, :cond_5

    const-string v3, "ModeChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Camera "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'s "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    aget-object v5, v5, v0

    aget-boolean v5, v5, p1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    sget-object v3, Lcom/android/camera/ModeChecker;->MODE_STRING_NORMAL:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-interface {v1, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_4

    sget-object v3, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    aget-object v3, v3, v0

    aput-boolean v6, v3, p1

    goto :goto_2

    :cond_7
    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isLcaRAM()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    const/4 v4, 0x2

    aget-object v3, v3, v4

    aput-boolean v6, v3, p1

    sget-object v3, Lcom/android/camera/ModeChecker;->MATRIX_NORMAL_ENABLE:[[Z

    aget-object v3, v3, v7

    aput-boolean v6, v3, p1

    goto/16 :goto_0
.end method
