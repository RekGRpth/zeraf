.class Lcom/android/camera/Camera$22;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/camera/Camera;->onVoiceCommandReceive(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/Camera;

.field final synthetic val$commandId:I


# direct methods
.method constructor <init>(Lcom/android/camera/Camera;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/Camera$22;->this$0:Lcom/android/camera/Camera;

    iput p2, p0, Lcom/android/camera/Camera$22;->val$commandId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/4 v0, 0x3

    iget v1, p0, Lcom/android/camera/Camera$22;->val$commandId:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x4

    iget v1, p0, Lcom/android/camera/Camera$22;->val$commandId:I

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera$22;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0}, Lcom/android/camera/Camera;->access$6200(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera$22;->this$0:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isVideoMode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera$22;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0}, Lcom/android/camera/Camera;->access$6100(Lcom/android/camera/Camera;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera$22;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0}, Lcom/android/camera/Camera;->access$6200(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/manager/ShutterManager;->performPhotoShutter()Z

    :cond_1
    return-void
.end method
