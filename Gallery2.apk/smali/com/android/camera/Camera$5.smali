.class Lcom/android/camera/Camera$5;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Lcom/android/camera/manager/ModePicker$OnModeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/Camera;


# direct methods
.method constructor <init>(Lcom/android/camera/Camera;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(I)V
    .locals 5
    .param p1    # I

    invoke-static {}, Lcom/android/camera/Camera;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onModeChanged("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") current mode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$3700(Lcom/android/camera/Camera;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v2

    if-eq v2, p1, :cond_1

    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$3800(Lcom/android/camera/Camera;)V

    sparse-switch p1, :sswitch_data_0

    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/PhotoActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/PhotoActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    :goto_0
    :sswitch_0
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    iget-boolean v2, v2, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-eqz v2, :cond_2

    :cond_1
    :goto_1
    return-void

    :sswitch_1
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/PhotoActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/PhotoActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/HdrActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/HdrActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    goto :goto_0

    :sswitch_3
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/FaceBeautyActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/FaceBeautyActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    goto :goto_0

    :sswitch_4
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/PanoramaActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/PanoramaActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    goto :goto_0

    :sswitch_5
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/MavActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/MavActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    goto :goto_0

    :sswitch_6
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/AsdActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/AsdActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    goto :goto_0

    :sswitch_7
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/SmileActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/SmileActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    goto :goto_0

    :sswitch_8
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/BestActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/BestActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    goto :goto_0

    :sswitch_9
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/EvActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/EvActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    goto :goto_0

    :sswitch_a
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/VideoActor;

    iget-object v4, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-direct {v3, v4}, Lcom/android/camera/actor/VideoActor;-><init>(Lcom/android/camera/Camera;)V

    invoke-static {v2, v3}, Lcom/android/camera/Camera;->access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;

    goto/16 :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$2800(Lcom/android/camera/Camera;)V

    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$3000(Lcom/android/camera/Camera;)V

    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$3100(Lcom/android/camera/Camera;)V

    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$3200(Lcom/android/camera/Camera;)V

    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$3300(Lcom/android/camera/Camera;)V

    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$1100(Lcom/android/camera/Camera;)V

    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$3900(Lcom/android/camera/Camera;)I

    move-result v2

    invoke-static {v2}, Lcom/android/camera/SettingChecker;->getCameraMode(I)I

    move-result v1

    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v2

    invoke-static {v2}, Lcom/android/camera/SettingChecker;->getCameraMode(I)I

    move-result v0

    iget-object v3, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    if-eq v1, v0, :cond_3

    const/4 v2, 0x1

    :goto_2
    invoke-static {v3, v2}, Lcom/android/camera/Camera;->access$1400(Lcom/android/camera/Camera;Z)V

    iget-object v2, p0, Lcom/android/camera/Camera$5;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$3400(Lcom/android/camera/Camera;)Lcom/android/camera/SettingChecker;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/SettingChecker;->applyParametersToUIImmediately()V

    goto/16 :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x5 -> :sswitch_6
        0x6 -> :sswitch_7
        0x7 -> :sswitch_8
        0x8 -> :sswitch_9
        0x9 -> :sswitch_a
        0x64 -> :sswitch_0
        0x6d -> :sswitch_0
        0xc8 -> :sswitch_0
        0xcb -> :sswitch_0
    .end sparse-switch
.end method
