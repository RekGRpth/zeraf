.class public Lcom/android/camera/SettingChecker;
.super Ljava/lang/Object;
.source "SettingChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/SettingChecker$PictureSizeMappingFinder;,
        Lcom/android/camera/SettingChecker$FlashMappingFinder;
    }
.end annotation


# static fields
.field public static final CAMERA_BACK_ID:I = 0x0

.field public static final CAMERA_COUNT:I = 0x2

.field public static final CAMERA_FRONT_ID:I = 0x1

.field private static final CAPABILITIES:[Lcom/android/camera/Restriction;

.field private static final COLUM_SCENE_MODE_AUTO:I = 0xe

.field private static final COLUM_SCENE_MODE_BEACH:I = 0x6

.field private static final COLUM_SCENE_MODE_CANDLELIGHT:I = 0xd

.field private static final COLUM_SCENE_MODE_FIREWORKS:I = 0xa

.field private static final COLUM_SCENE_MODE_LANDSCAPE:I = 0x2

.field private static final COLUM_SCENE_MODE_NIGHT:I = 0x3

.field private static final COLUM_SCENE_MODE_NIGHT_PORTRAIT:I = 0x4

.field private static final COLUM_SCENE_MODE_NORMAL:I = 0x0

.field private static final COLUM_SCENE_MODE_PARTY:I = 0xc

.field private static final COLUM_SCENE_MODE_PORTRAIT:I = 0x1

.field private static final COLUM_SCENE_MODE_SNOW:I = 0x7

.field private static final COLUM_SCENE_MODE_SPORT:I = 0xb

.field private static final COLUM_SCENE_MODE_STEADYPHOTO:I = 0x9

.field private static final COLUM_SCENE_MODE_SUNSET:I = 0x8

.field private static final COLUM_SCENE_MODE_THEATRE:I = 0x5

.field private static final DEFAULT_VALUE_FOR_SETTING:[Ljava/lang/String;

.field private static final DEFAULT_VALUE_FOR_SETTING_ID:[I

.field private static final KEYS_FOR_SCENE:[Ljava/lang/String;

.field public static final KEYS_FOR_SETTING:[Ljava/lang/String;

.field private static final LOG:Z

.field public static final MAPPING_FINDER_FLASH:Lcom/android/camera/Restriction$MappingFinder;

.field public static final MAPPING_FINDER_PICTURE_SIZE:Lcom/android/camera/Restriction$MappingFinder;

.field private static final MATRIX_FOCUS_MODE_CONTINUOUS:[Ljava/lang/String;

.field private static final MATRIX_FOCUS_MODE_DEFAULT_ARRAY:[I

.field private static final MATRIX_MODE_STATE:[[I

.field private static final MATRIX_SCENE_STATE:[[I

.field private static final MATRIX_SETTING_VISIBLE:[[Z

.field private static final MATRIX_ZOOM_ENABLE:[Z

.field public static final MODE_VIDEO_LIVE_EFFECT:I = 0xc

.field private static final RESET_SETTING_ITEMS:[I

.field private static final RESET_STATE_VALUE:[[Ljava/lang/String;

.field private static final RESTRICTIOINS:[Lcom/android/camera/Restriction;

.field public static final ROW_SETTING_ANTI_FLICKER:I = 0xe

.field public static final ROW_SETTING_AUDIO_MODE:I = 0x11

.field public static final ROW_SETTING_BRIGHTNESS:I = 0x21

.field public static final ROW_SETTING_CAMERA_FACE_DETECT:I = 0x32

.field public static final ROW_SETTING_CAMERA_MODE:I = 0x28

.field public static final ROW_SETTING_CAPTURE_MODE:I = 0x29

.field public static final ROW_SETTING_COLOR_EFFECT:I = 0x6

.field public static final ROW_SETTING_CONTINUOUS:I = 0x9

.field public static final ROW_SETTING_CONTINUOUS_NUM:I = 0x2a

.field public static final ROW_SETTING_CONTRAST:I = 0x22

.field public static final ROW_SETTING_DUAL_CAMERA:I = 0x1

.field public static final ROW_SETTING_EXPOSURE:I = 0x2

.field public static final ROW_SETTING_FACEBEAUTY_PROPERTIES:I = 0x2e

.field public static final ROW_SETTING_FACEBEAUTY_SHARP:I = 0x31

.field public static final ROW_SETTING_FACEBEAUTY_SKIN_COLOR:I = 0x30

.field public static final ROW_SETTING_FACEBEAUTY_SMOOTH:I = 0x2f

.field public static final ROW_SETTING_FLASH:I = 0x0

.field public static final ROW_SETTING_GEO_TAG:I = 0xa

.field public static final ROW_SETTING_HUE:I = 0x1f

.field public static final ROW_SETTING_IMAGE_PROPERTIES:I = 0x5

.field public static final ROW_SETTING_ISO:I = 0xc

.field public static final ROW_SETTING_JPEG_QUALITY:I = 0x2c

.field public static final ROW_SETTING_LIVE_EFFECT:I = 0x13

.field public static final ROW_SETTING_MICROPHONE:I = 0x10

.field public static final ROW_SETTING_PICTURE_RATIO:I = 0x15

.field public static final ROW_SETTING_PICTURE_SIZE:I = 0xb

.field public static final ROW_SETTING_RECORDING_HINT:I = 0x2b

.field public static final ROW_SETTING_SATURATION:I = 0x20

.field public static final ROW_SETTING_SCENCE_MODE:I = 0x3

.field public static final ROW_SETTING_SELF_TIMER:I = 0x7

.field public static final ROW_SETTING_SHARPNESS:I = 0x1e

.field public static final ROW_SETTING_STEREO_MODE:I = 0x2d

.field public static final ROW_SETTING_TIME_LAPSE:I = 0x12

.field public static final ROW_SETTING_VIDEO_QUALITY:I = 0x14

.field public static final ROW_SETTING_VIDEO_STABLE:I = 0xf

.field public static final ROW_SETTING_VOICE:I = 0x16

.field public static final ROW_SETTING_WHITE_BALANCE:I = 0x4

.field public static final ROW_SETTING_ZSD:I = 0x8

.field public static final SETTING_GROUP_ALL_IN_SCREEN:[I

.field public static final SETTING_GROUP_ALL_IN_SETTING:[I

.field public static final SETTING_GROUP_CAMERA_FOR_PARAMETERS:[I

.field public static final SETTING_GROUP_CAMERA_FOR_TAB:[I

.field public static final SETTING_GROUP_CAMERA_FOR_TAB_NO_PREVIEW:[I

.field public static final SETTING_GROUP_CAMERA_FOR_UI:[I

.field public static final SETTING_GROUP_COMMON_FOR_TAB:[I

.field public static final SETTING_GROUP_COMMON_FOR_TAB_PREVIEW:[I

.field public static final SETTING_GROUP_IMAGE_FOR_TAB:[I

.field public static final SETTING_GROUP_NOT_IN_PREFERENCE:[I

.field public static final SETTING_GROUP_VIDEO_FOR_PARAMETERS:[I

.field public static final SETTING_GROUP_VIDEO_FOR_TAB:[I

.field public static final SETTING_GROUP_VIDEO_FOR_TAB_NO_PREVIEW:[I

.field public static final SETTING_GROUP_VIDEO_FOR_UI:[I

.field public static final SETTING_ROW_COUNT:I = 0x33

.field private static final STATE_D0:I = 0x64

.field private static final STATE_E0:I = 0xc8

.field private static final STATE_OFFSET:I = 0x64

.field private static final STATE_R0:I = 0x12c

.field private static final STATE_R1:I = 0x12d

.field private static final STATE_R2:I = 0x12e

.field private static final STATE_R3:I = 0x12f

.field private static final STATE_R4:I = 0x130

.field private static final STATE_R5:I = 0x131

.field private static final STATE_R6:I = 0x132

.field private static final STATE_R7:I = 0x133

.field private static final TAG:Ljava/lang/String; = "SettingChecker"

.field public static final UNKNOWN:I = -0x1


# instance fields
.field private mContext:Lcom/android/camera/Camera;

.field private mListPrefs:[Lcom/android/camera/ListPreference;

.field private final mOverrideSettingValues:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/16 v11, 0x11

    const/4 v10, 0x2

    const/16 v9, 0xd

    const/4 v8, 0x0

    const/4 v7, 0x1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/SettingChecker;->LOG:Z

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/camera/SettingChecker;->RESET_SETTING_ITEMS:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_COMMON_FOR_TAB:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_TAB:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_VIDEO_FOR_TAB:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_COMMON_FOR_TAB_PREVIEW:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_TAB_NO_PREVIEW:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_VIDEO_FOR_TAB_NO_PREVIEW:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_IMAGE_FOR_TAB:[I

    const/16 v0, 0x16

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_PARAMETERS:[I

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_VIDEO_FOR_PARAMETERS:[I

    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_UI:[I

    new-array v0, v11, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_VIDEO_FOR_UI:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_ALL_IN_SCREEN:[I

    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_ALL_IN_SETTING:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_NOT_IN_PREFERENCE:[I

    new-array v0, v11, [Ljava/lang/String;

    const-string v1, "action"

    aput-object v1, v0, v8

    const-string v1, "portrait"

    aput-object v1, v0, v7

    const-string v1, "landscape"

    aput-object v1, v0, v10

    const/4 v1, 0x3

    const-string v2, "night"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "night-portrait"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "theatre"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "beach"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "snow"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "sunset"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "steadyphoto"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "fireworks"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "sports"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "party"

    aput-object v2, v0, v1

    const-string v1, "candlelight"

    aput-object v1, v0, v9

    const/16 v1, 0xe

    const-string v2, "auto"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "normal"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "hdr"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SCENE:[Ljava/lang/String;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/camera/Restriction;

    new-instance v1, Lcom/android/camera/Restriction;

    const/16 v2, 0x13

    invoke-direct {v1, v2}, Lcom/android/camera/Restriction;-><init>(I)V

    new-array v2, v7, [Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v1, v2}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v1

    new-array v2, v7, [Lcom/android/camera/Restriction;

    new-instance v3, Lcom/android/camera/Restriction;

    const/16 v4, 0x14

    invoke-direct {v3, v4}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v3, v8}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "fine"

    aput-object v5, v4, v8

    invoke-virtual {v3, v4}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v1, v2}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v1

    aput-object v1, v0, v8

    new-instance v1, Lcom/android/camera/Restriction;

    const/16 v2, 0x12

    invoke-direct {v1, v2}, Lcom/android/camera/Restriction;-><init>(I)V

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "1000"

    aput-object v3, v2, v8

    const-string v3, "1500"

    aput-object v3, v2, v7

    const-string v3, "2000"

    aput-object v3, v2, v10

    const/4 v3, 0x3

    const-string v4, "2500"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "3000"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "5000"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "10000"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v1

    new-array v2, v10, [Lcom/android/camera/Restriction;

    new-instance v3, Lcom/android/camera/Restriction;

    const/16 v4, 0x10

    invoke-direct {v3, v4}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v3, v8}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "off"

    aput-object v5, v4, v8

    invoke-virtual {v3, v4}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v3

    aput-object v3, v2, v8

    new-instance v3, Lcom/android/camera/Restriction;

    invoke-direct {v3, v11}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v3, v8}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "normal"

    aput-object v5, v4, v8

    invoke-virtual {v3, v4}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v1

    aput-object v1, v0, v7

    new-instance v1, Lcom/android/camera/Restriction;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lcom/android/camera/Restriction;-><init>(I)V

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "off"

    aput-object v3, v2, v8

    invoke-virtual {v1, v2}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v1

    new-array v2, v7, [Lcom/android/camera/Restriction;

    new-instance v3, Lcom/android/camera/Restriction;

    invoke-direct {v3, v11}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v3, v8}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "normal"

    aput-object v5, v4, v8

    invoke-virtual {v3, v4}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v1, v2}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v1

    aput-object v1, v0, v10

    sput-object v0, Lcom/android/camera/SettingChecker;->RESTRICTIOINS:[Lcom/android/camera/Restriction;

    new-instance v0, Lcom/android/camera/SettingChecker$PictureSizeMappingFinder;

    invoke-direct {v0}, Lcom/android/camera/SettingChecker$PictureSizeMappingFinder;-><init>()V

    sput-object v0, Lcom/android/camera/SettingChecker;->MAPPING_FINDER_PICTURE_SIZE:Lcom/android/camera/Restriction$MappingFinder;

    new-instance v0, Lcom/android/camera/SettingChecker$FlashMappingFinder;

    invoke-direct {v0}, Lcom/android/camera/SettingChecker$FlashMappingFinder;-><init>()V

    sput-object v0, Lcom/android/camera/SettingChecker;->MAPPING_FINDER_FLASH:Lcom/android/camera/Restriction$MappingFinder;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/android/camera/Restriction;

    new-instance v1, Lcom/android/camera/Restriction;

    const/4 v2, 0x6

    invoke-direct {v1, v2}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v1, v7}, Lcom/android/camera/Restriction;->setType(I)Lcom/android/camera/Restriction;

    move-result-object v1

    new-array v2, v7, [Lcom/android/camera/Restriction;

    new-instance v3, Lcom/android/camera/Restriction;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v3, v7}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v3

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "auto"

    aput-object v5, v4, v8

    const-string v5, "portrait"

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v1, v2}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v1

    aput-object v1, v0, v8

    new-instance v1, Lcom/android/camera/Restriction;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v1, v7}, Lcom/android/camera/Restriction;->setType(I)Lcom/android/camera/Restriction;

    move-result-object v1

    new-array v2, v7, [Lcom/android/camera/Restriction;

    new-instance v3, Lcom/android/camera/Restriction;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v3, v7}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v3

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "auto"

    aput-object v5, v4, v8

    const-string v5, "night"

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v1, v2}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v1

    aput-object v1, v0, v7

    new-instance v1, Lcom/android/camera/Restriction;

    const/16 v2, 0x2b

    invoke-direct {v1, v2}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v1, v8}, Lcom/android/camera/Restriction;->setType(I)Lcom/android/camera/Restriction;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v1, v2}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v1

    new-array v2, v7, [Lcom/android/camera/Restriction;

    new-instance v3, Lcom/android/camera/Restriction;

    invoke-direct {v3, v8}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v3, v7}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v3

    sget-object v4, Lcom/android/camera/SettingChecker;->MAPPING_FINDER_FLASH:Lcom/android/camera/Restriction$MappingFinder;

    invoke-virtual {v3, v4}, Lcom/android/camera/Restriction;->setMappingFinder(Lcom/android/camera/Restriction$MappingFinder;)Lcom/android/camera/Restriction;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "auto"

    aput-object v5, v4, v8

    const-string v5, "on"

    aput-object v5, v4, v7

    const-string v5, "off"

    aput-object v5, v4, v10

    invoke-virtual {v3, v4}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v1, v2}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v1

    aput-object v1, v0, v10

    const/4 v1, 0x3

    new-instance v2, Lcom/android/camera/Restriction;

    const/16 v3, 0x2b

    invoke-direct {v2, v3}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v2, v8}, Lcom/android/camera/Restriction;->setType(I)Lcom/android/camera/Restriction;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v2

    new-array v3, v7, [Lcom/android/camera/Restriction;

    new-instance v4, Lcom/android/camera/Restriction;

    invoke-direct {v4, v8}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v4, v7}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v4

    sget-object v5, Lcom/android/camera/SettingChecker;->MAPPING_FINDER_FLASH:Lcom/android/camera/Restriction$MappingFinder;

    invoke-virtual {v4, v5}, Lcom/android/camera/Restriction;->setMappingFinder(Lcom/android/camera/Restriction$MappingFinder;)Lcom/android/camera/Restriction;

    move-result-object v4

    new-array v5, v10, [Ljava/lang/String;

    const-string v6, "torch"

    aput-object v6, v5, v8

    const-string v6, "off"

    aput-object v6, v5, v7

    invoke-virtual {v4, v5}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/android/camera/Restriction;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v2, v8}, Lcom/android/camera/Restriction;->setType(I)Lcom/android/camera/Restriction;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "1.3333"

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v2

    new-array v3, v7, [Lcom/android/camera/Restriction;

    new-instance v4, Lcom/android/camera/Restriction;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v4, v7}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v4

    sget-object v5, Lcom/android/camera/SettingChecker;->MAPPING_FINDER_PICTURE_SIZE:Lcom/android/camera/Restriction$MappingFinder;

    invoke-virtual {v4, v5}, Lcom/android/camera/Restriction;->setMappingFinder(Lcom/android/camera/Restriction$MappingFinder;)Lcom/android/camera/Restriction;

    move-result-object v4

    sget-object v5, Lcom/android/camera/CameraSettings;->PICTURE_SIZE_4_3:[Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/android/camera/Restriction;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v2, v8}, Lcom/android/camera/Restriction;->setType(I)Lcom/android/camera/Restriction;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "1.7778"

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v2

    new-array v3, v7, [Lcom/android/camera/Restriction;

    new-instance v4, Lcom/android/camera/Restriction;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v4, v7}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v4

    sget-object v5, Lcom/android/camera/SettingChecker;->MAPPING_FINDER_PICTURE_SIZE:Lcom/android/camera/Restriction$MappingFinder;

    invoke-virtual {v4, v5}, Lcom/android/camera/Restriction;->setMappingFinder(Lcom/android/camera/Restriction$MappingFinder;)Lcom/android/camera/Restriction;

    move-result-object v4

    sget-object v5, Lcom/android/camera/CameraSettings;->PICTURE_SIZE_16_9:[Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/android/camera/Restriction;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v2, v8}, Lcom/android/camera/Restriction;->setType(I)Lcom/android/camera/Restriction;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "1.6667"

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v2

    new-array v3, v7, [Lcom/android/camera/Restriction;

    new-instance v4, Lcom/android/camera/Restriction;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v4, v7}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v4

    sget-object v5, Lcom/android/camera/SettingChecker;->MAPPING_FINDER_PICTURE_SIZE:Lcom/android/camera/Restriction$MappingFinder;

    invoke-virtual {v4, v5}, Lcom/android/camera/Restriction;->setMappingFinder(Lcom/android/camera/Restriction$MappingFinder;)Lcom/android/camera/Restriction;

    move-result-object v4

    sget-object v5, Lcom/android/camera/CameraSettings;->PICTURE_SIZE_5_3:[Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/android/camera/Restriction;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v2, v8}, Lcom/android/camera/Restriction;->setType(I)Lcom/android/camera/Restriction;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "1.5"

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v2

    new-array v3, v7, [Lcom/android/camera/Restriction;

    new-instance v4, Lcom/android/camera/Restriction;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lcom/android/camera/Restriction;-><init>(I)V

    invoke-virtual {v4, v7}, Lcom/android/camera/Restriction;->setEnable(Z)Lcom/android/camera/Restriction;

    move-result-object v4

    sget-object v5, Lcom/android/camera/SettingChecker;->MAPPING_FINDER_PICTURE_SIZE:Lcom/android/camera/Restriction$MappingFinder;

    invoke-virtual {v4, v5}, Lcom/android/camera/Restriction;->setMappingFinder(Lcom/android/camera/Restriction$MappingFinder;)Lcom/android/camera/Restriction;

    move-result-object v4

    sget-object v5, Lcom/android/camera/CameraSettings;->PICTURE_SIZE_3_2:[Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/android/camera/Restriction;->setValues([Ljava/lang/String;)Lcom/android/camera/Restriction;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/camera/Restriction;->setRestrictions([Lcom/android/camera/Restriction;)Lcom/android/camera/Restriction;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/camera/SettingChecker;->CAPABILITIES:[Lcom/android/camera/Restriction;

    const/16 v0, 0x33

    new-array v0, v0, [[I

    sput-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v0, 0x33

    new-array v0, v0, [[I

    sput-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    const/16 v0, 0x33

    new-array v0, v0, [I

    sput-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v0, 0x33

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING:[Ljava/lang/String;

    new-array v0, v10, [[Z

    sput-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    const/16 v0, 0x33

    new-array v0, v0, [[Ljava/lang/String;

    sput-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v0, 0x33

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v0, 0xc

    new-array v0, v0, [Z

    fill-array-data v0, :array_f

    sput-object v0, Lcom/android/camera/SettingChecker;->MATRIX_ZOOM_ENABLE:[Z

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/android/camera/SettingChecker;->MATRIX_FOCUS_MODE_DEFAULT_ARRAY:[I

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "continuous-picture"

    aput-object v1, v0, v8

    const-string v1, "continuous-picture"

    aput-object v1, v0, v7

    const-string v1, "continuous-picture"

    aput-object v1, v0, v10

    const/4 v1, 0x3

    const-string v2, "continuous-picture"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "continuous-picture"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "continuous-picture"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "continuous-picture"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "continuous-picture"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "continuous-picture"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "continuous-video"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "continuous-picture"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "continuous-picture"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/camera/SettingChecker;->MATRIX_FOCUS_MODE_CONTINUOUS:[Ljava/lang/String;

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    new-array v1, v9, [I

    fill-array-data v1, :array_11

    aput-object v1, v0, v8

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    new-array v1, v9, [I

    fill-array-data v1, :array_12

    aput-object v1, v0, v7

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    new-array v1, v9, [I

    fill-array-data v1, :array_13

    aput-object v1, v0, v10

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/4 v1, 0x3

    new-array v2, v9, [I

    fill-array-data v2, :array_14

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/4 v1, 0x4

    new-array v2, v9, [I

    fill-array-data v2, :array_15

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/4 v1, 0x6

    new-array v2, v9, [I

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/4 v1, 0x7

    new-array v2, v9, [I

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x8

    new-array v2, v9, [I

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x9

    new-array v2, v9, [I

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0xa

    new-array v2, v9, [I

    fill-array-data v2, :array_1a

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0xb

    new-array v2, v9, [I

    fill-array-data v2, :array_1b

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0xc

    new-array v2, v9, [I

    fill-array-data v2, :array_1c

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0xe

    new-array v2, v9, [I

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0xf

    new-array v2, v9, [I

    fill-array-data v2, :array_1e

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x10

    new-array v2, v9, [I

    fill-array-data v2, :array_1f

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    new-array v1, v9, [I

    fill-array-data v1, :array_20

    aput-object v1, v0, v11

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x12

    new-array v2, v9, [I

    fill-array-data v2, :array_21

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x13

    new-array v2, v9, [I

    fill-array-data v2, :array_22

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x14

    new-array v2, v9, [I

    fill-array-data v2, :array_23

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x15

    new-array v2, v9, [I

    fill-array-data v2, :array_24

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x16

    new-array v2, v9, [I

    fill-array-data v2, :array_25

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x1e

    new-array v2, v9, [I

    fill-array-data v2, :array_26

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x1f

    new-array v2, v9, [I

    fill-array-data v2, :array_27

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x20

    new-array v2, v9, [I

    fill-array-data v2, :array_28

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x21

    new-array v2, v9, [I

    fill-array-data v2, :array_29

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x22

    new-array v2, v9, [I

    fill-array-data v2, :array_2a

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x28

    new-array v2, v9, [I

    fill-array-data v2, :array_2b

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x29

    new-array v2, v9, [I

    fill-array-data v2, :array_2c

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x2a

    new-array v2, v9, [I

    fill-array-data v2, :array_2d

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x2b

    new-array v2, v9, [I

    fill-array-data v2, :array_2e

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x2c

    new-array v2, v9, [I

    fill-array-data v2, :array_2f

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x2f

    new-array v2, v9, [I

    fill-array-data v2, :array_30

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x30

    new-array v2, v9, [I

    fill-array-data v2, :array_31

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x31

    new-array v2, v9, [I

    fill-array-data v2, :array_32

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v1, 0x32

    new-array v2, v9, [I

    fill-array-data v2, :array_33

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    new-array v1, v11, [I

    fill-array-data v1, :array_34

    aput-object v1, v0, v10

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    const/4 v1, 0x4

    new-array v2, v11, [I

    fill-array-data v2, :array_35

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    const/16 v1, 0xc

    new-array v2, v11, [I

    fill-array-data v2, :array_36

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    const/16 v1, 0x1e

    new-array v2, v11, [I

    fill-array-data v2, :array_37

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    const/16 v1, 0x1f

    new-array v2, v11, [I

    fill-array-data v2, :array_38

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    const/16 v1, 0x20

    new-array v2, v11, [I

    fill-array-data v2, :array_39

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    const/16 v1, 0x21

    new-array v2, v11, [I

    fill-array-data v2, :array_3a

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    const/16 v1, 0x22

    new-array v2, v11, [I

    fill-array-data v2, :array_3b

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    new-array v1, v11, [I

    fill-array-data v1, :array_3c

    aput-object v1, v0, v8

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "off"

    aput-object v2, v1, v8

    aput-object v1, v0, v8

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "0"

    aput-object v2, v1, v8

    aput-object v1, v0, v7

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    new-array v1, v10, [Ljava/lang/String;

    const-string v2, "0"

    aput-object v2, v1, v8

    const-string v2, "1"

    aput-object v2, v1, v7

    aput-object v1, v0, v10

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/4 v1, 0x3

    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "hdr"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/4 v1, 0x4

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "daylight"

    aput-object v3, v2, v7

    const-string v3, "incandescent"

    aput-object v3, v2, v10

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "none"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/4 v1, 0x7

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "0"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x8

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "off"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x9

    const/4 v2, 0x0

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0xa

    const/4 v2, 0x0

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0xb

    const/4 v2, 0x0

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0xc

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "auto"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0xe

    const/4 v2, 0x0

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0xf

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "off"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x10

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "on"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "normal"

    aput-object v2, v1, v8

    aput-object v1, v0, v11

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x12

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "off"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x13

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "off"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x14

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "9"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x1e

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "middle"

    aput-object v3, v2, v8

    const-string v3, "low"

    aput-object v3, v2, v7

    const-string v3, "high"

    aput-object v3, v2, v10

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x1f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "middle"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x20

    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "middle"

    aput-object v3, v2, v8

    const-string v3, "low"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x21

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "middle"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x22

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "middle"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x2f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "0"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x30

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "0"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x31

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "0"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x32

    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "off"

    aput-object v3, v2, v8

    const-string v3, "on"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x2b

    new-array v2, v10, [Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x29

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "normal"

    aput-object v3, v2, v8

    const-string v3, "hdr"

    aput-object v3, v2, v7

    const-string v3, "face_beauty"

    aput-object v3, v2, v10

    const/4 v3, 0x3

    const-string v4, "asd"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "smileshot"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "bestshot"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "evbracketshot"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "autorama"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x2a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "1"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x2c

    new-array v2, v10, [Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x28

    new-array v2, v10, [Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    const/16 v1, 0x16

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "off"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const-string v1, "pref_camera_flashmode_key"

    aput-object v1, v0, v8

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const-string v1, "pref_camera_id_key"

    aput-object v1, v0, v7

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const-string v1, "pref_camera_exposure_key"

    aput-object v1, v0, v10

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/4 v1, 0x3

    const-string v2, "pref_camera_scenemode_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/4 v1, 0x4

    const-string v2, "pref_camera_whitebalance_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/4 v1, 0x5

    const-string v2, "pref_camera_image_properties_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/4 v1, 0x6

    const-string v2, "pref_camera_coloreffect_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/4 v1, 0x7

    const-string v2, "pref_camera_self_timer_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x8

    const-string v2, "pref_camera_zsd_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x9

    const-string v2, "pref_camera_shot_number"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0xa

    const-string v2, "pref_camera_recordlocation_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0xb

    const-string v2, "pref_camera_picturesize_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0xc

    const-string v2, "pref_camera_iso_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0xe

    const-string v2, "pref_camera_antibanding_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0xf

    const-string v2, "pref_video_eis_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x10

    const-string v2, "pref_camera_recordaudio_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const-string v1, "pref_camera_video_hd_recording_key"

    aput-object v1, v0, v11

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x12

    const-string v2, "pref_video_time_lapse_frame_interval_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x13

    const-string v2, "pref_video_effect_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x14

    const-string v2, "pref_video_quality_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x2d

    const-string v2, "pref_stereo3d_mode_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x1e

    const-string v2, "pref_camera_edge_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x1f

    const-string v2, "pref_camera_hue_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x20

    const-string v2, "pref_camera_saturation_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x21

    const-string v2, "pref_camera_brightness_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x22

    const-string v2, "pref_camera_contrast_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x15

    const-string v2, "pref_camera_picturesize_ratio_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x16

    const-string v2, "pref_voice_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x2e

    const-string v2, "pref_camera_facebeauty_properties_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x2f

    const-string v2, "pref_facebeauty_smooth_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x30

    const-string v2, "pref_facebeauty_skin_color_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x31

    const-string v2, "pref_facebeauty_sharp_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v1, 0x32

    const-string v2, "pref_face_detect_key"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    const/16 v1, 0x33

    new-array v1, v1, [Z

    aput-object v1, v0, v8

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    aput-boolean v7, v0, v8

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    aput-boolean v7, v0, v7

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    aput-boolean v7, v0, v10

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/4 v1, 0x3

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/4 v1, 0x4

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/4 v1, 0x5

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/4 v1, 0x6

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/4 v1, 0x7

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x8

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x9

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0xa

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0xb

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0xc

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0xe

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0xf

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x10

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    aput-boolean v7, v0, v11

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x12

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x13

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x14

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x2d

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x1e

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x1f

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x20

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x21

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x22

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x15

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x16

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x2e

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x2f

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x30

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x31

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v8

    const/16 v1, 0x32

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    const/16 v1, 0x33

    new-array v1, v1, [Z

    aput-object v1, v0, v7

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    aput-boolean v7, v0, v8

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    aput-boolean v7, v0, v7

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    aput-boolean v7, v0, v10

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/4 v1, 0x3

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/4 v1, 0x4

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/4 v1, 0x5

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/4 v1, 0x6

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/4 v1, 0x7

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x8

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x9

    aput-boolean v8, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0xa

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0xb

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0xc

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0xe

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0xf

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x10

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    aput-boolean v7, v0, v11

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x12

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x13

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x14

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x2d

    aput-boolean v8, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x1e

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x1f

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x20

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x21

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x22

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x15

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x16

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x2e

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x2f

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x30

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x31

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v0, v0, v7

    const/16 v1, 0x32

    aput-boolean v7, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const v1, 0x7f0c0123

    aput v1, v0, v8

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const v1, 0x7f0c0107

    aput v1, v0, v7

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const v1, 0x7f0c0054

    aput v1, v0, v10

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/4 v1, 0x3

    const v2, 0x7f0c0131

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/4 v1, 0x4

    const v2, 0x7f0c012a

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/4 v1, 0x6

    const v2, 0x7f0c0042

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/4 v1, 0x7

    const v2, 0x7f0c0055

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x8

    const v2, 0x7f0c0079

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x9

    const v2, 0x7f0c007b

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0xa

    const v2, 0x7f0c010b

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0xb

    const/4 v2, -0x1

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0xc

    const v2, 0x7f0c002d

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0xe

    const v2, 0x7f0c005c

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0xf

    const v2, 0x7f0c006d

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x10

    const v2, 0x7f0c0070

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const v1, 0x7f0c0074

    aput v1, v0, v11

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x12

    const v2, 0x7f0c0114

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x13

    const v2, 0x7f0c0150

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x14

    const v2, 0x7f0c010f

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x2d

    const v2, 0x7f0c00c2

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x1e

    const v2, 0x7f0c0035

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x1f

    const v2, 0x7f0c0037

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x20

    const v2, 0x7f0c0039

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x21

    const v2, 0x7f0c003b

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x22

    const v2, 0x7f0c003d

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x15

    const/4 v2, -0x1

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x16

    const v2, 0x7f0c0089

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x2f

    const v2, 0x7f0c0093

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x30

    const v2, 0x7f0c0095

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x31

    const v2, 0x7f0c0097

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    const/16 v1, 0x32

    const v2, 0x7f0c009a

    aput v2, v0, v1

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x6
        0x7
        0x1e
        0x1f
        0x20
        0x21
        0x22
        0xc
        0x12
        0x13
        0x11
    .end array-data

    :array_1
    .array-data 4
        0xa
        0x2
        0x6
        0x3
        0x4
        0x5
        0xe
    .end array-data

    :array_2
    .array-data 4
        0x8
        0x16
        0x32
        0x7
        0x9
        0xb
        0x15
        0xc
        0x2e
    .end array-data

    :array_3
    .array-data 4
        0xf
        0x10
        0x11
        0x12
        0x13
        0x14
    .end array-data

    :array_4
    .array-data 4
        0xb
        0x15
        0x14
    .end array-data

    :array_5
    .array-data 4
        0x8
        0x16
        0x32
        0x7
        0x9
        0xc
    .end array-data

    :array_6
    .array-data 4
        0xf
        0x10
        0x11
        0x12
        0x13
    .end array-data

    :array_7
    .array-data 4
        0x1e
        0x1f
        0x20
        0x21
        0x22
    .end array-data

    :array_8
    .array-data 4
        0x3
        0xb
        0x0
        0x2
        0x4
        0x6
        0xe
        0x8
        0xc
        0x2f
        0x30
        0x31
        0x1e
        0x1f
        0x20
        0x21
        0x22
        0x28
        0x29
        0x2a
        0x2b
        0x2c
    .end array-data

    :array_9
    .array-data 4
        0x3
        0x14
        0x0
        0x2
        0x4
        0x6
        0xe
        0xf
        0x1e
        0x1f
        0x20
        0x21
        0x22
        0x28
        0x29
        0x2a
        0x2b
        0x2c
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x2
        0x3
        0x4
        0x6
        0xa
        0xe
        0x7
        0x8
        0x9
        0xb
        0xc
        0x2f
        0x30
        0x31
        0x1e
        0x1f
        0x20
        0x21
        0x22
        0x15
        0x16
        0x32
    .end array-data

    :array_b
    .array-data 4
        0x0
        0x2
        0x3
        0x4
        0x6
        0xa
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
        0x1e
        0x1f
        0x20
        0x21
        0x22
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x1
        0x7
        0x32
        0x8
    .end array-data

    :array_d
    .array-data 4
        0x0
        0x2
        0x3
        0x4
        0x6
        0xa
        0xe
        0x7
        0x8
        0x9
        0xb
        0xc
        0x2f
        0x30
        0x31
        0x32
        0xf
        0x10
        0x11
        0x12
        0x13
        0x14
        0x1e
        0x1f
        0x20
        0x21
        0x22
        0x15
        0x16
    .end array-data

    :array_e
    .array-data 4
        0x28
        0x29
        0x2a
        0x2b
        0x2c
        0x5
        0x2e
        0x16
    .end array-data

    :array_f
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    :array_10
    .array-data 4
        0x7f08001e
        0x7f08001e
        0x7f08001e
        0x7f08001e
        0x7f08001e
        0x7f08001e
        0x7f08001e
        0x7f08001e
        0x7f08001e
        0x7f08001f
        0x7f08001e
        0x7f08001e
    .end array-data

    :array_11
    .array-data 4
        0xc8
        0x12c
        0x12c
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
        0x12c
        0xc8
        0x12c
        0x12c
        0xc8
    .end array-data

    :array_12
    .array-data 4
        0xc8
        0x64
        0xc8
        0x64
        0x64
        0x64
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
        0x64
        0xc8
    .end array-data

    :array_13
    .array-data 4
        0xc8
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x12c
    .end array-data

    :array_14
    .array-data 4
        0xc8
        0x12d
        0x12c
        0x12c
        0x12c
        0x12c
        0xc8
        0x12c
        0x12c
        0xc8
        0xc8
        0x12c
        0x64
    .end array-data

    :array_15
    .array-data 4
        0xc8
        0x12c
        0x12c
        0xc8
        0xc8
        0x12c
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
    .end array-data

    :array_16
    .array-data 4
        0xc8
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x12c
        0x64
    .end array-data

    :array_17
    .array-data 4
        0xc8
        0xc8
        0xc8
        0x12c
        0x12c
        0xc8
        0x12c
        0x12c
        0x12c
        0xc8
        0x12c
        0x12c
        0x64
    .end array-data

    :array_18
    .array-data 4
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
    .end array-data

    :array_19
    .array-data 4
        0xc8
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0xc8
        0x64
        0x64
        0x64
    .end array-data

    :array_1a
    .array-data 4
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_1b
    .array-data 4
        0xc8
        0xc8
        0xc8
        0x64
        0x64
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
        0x64
    .end array-data

    :array_1c
    .array-data 4
        0xc8
        0x12c
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
    .end array-data

    :array_1d
    .array-data 4
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
    .end array-data

    :array_1e
    .array-data 4
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x12c
        0x12c
    .end array-data

    :array_1f
    .array-data 4
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_20
    .array-data 4
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x12c
    .end array-data

    :array_21
    .array-data 4
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_22
    .array-data 4
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_23
    .array-data 4
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x12c
    .end array-data

    :array_24
    .array-data 4
        0xc8
        0xc8
        0xc8
        0x64
        0x64
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
        0x64
    .end array-data

    :array_25
    .array-data 4
        0xc8
        0xc8
        0xc8
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x12c
        0x64
    .end array-data

    :array_26
    .array-data 4
        0xc8
        0x12c
        0x64
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
    .end array-data

    :array_27
    .array-data 4
        0xc8
        0x12c
        0x64
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
    .end array-data

    :array_28
    .array-data 4
        0xc8
        0x12c
        0x64
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
    .end array-data

    :array_29
    .array-data 4
        0xc8
        0x12c
        0x64
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
    .end array-data

    :array_2a
    .array-data 4
        0xc8
        0x12c
        0x64
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x64
    .end array-data

    :array_2b
    .array-data 4
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12d
        0x12c
        0x12c
        0x12d
    .end array-data

    :array_2c
    .array-data 4
        0x12c
        0x12c
        0x12e
        0x133
        0x12c
        0x12f
        0x130
        0x131
        0x132
        0x12c
        0x12c
        0x12c
        0x12c
    .end array-data

    :array_2d
    .array-data 4
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
    .end array-data

    :array_2e
    .array-data 4
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12d
        0x12c
        0x12c
        0x12d
    .end array-data

    :array_2f
    .array-data 4
        0x12c
        0x12c
        0x12c
        0x12d
        0x12d
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
    .end array-data

    :array_30
    .array-data 4
        0x64
        0x64
        0xc8
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
    .end array-data

    :array_31
    .array-data 4
        0x64
        0x64
        0xc8
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
    .end array-data

    :array_32
    .array-data 4
        0x64
        0x64
        0xc8
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
        0x64
    .end array-data

    :array_33
    .array-data 4
        0xc8
        0xc8
        0x12d
        0x12c
        0x12c
        0x12d
        0x12d
        0xc8
        0xc8
        0x12c
        0xc8
        0xc8
        0x64
    .end array-data

    :array_34
    .array-data 4
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12d
        0x12d
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_35
    .array-data 4
        0x12c
        0x12c
        0x12d
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12d
        0x12c
        0x12c
        0x12c
        0x12c
        0x12e
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_36
    .array-data 4
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_37
    .array-data 4
        0x12c
        0x12d
        0x12e
        0x12d
        0x12d
        0x12e
        0x12e
        0x12e
        0x12e
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_38
    .array-data 4
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_39
    .array-data 4
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12d
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_3a
    .array-data 4
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_3b
    .array-data 4
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0x12c
        0xc8
        0xc8
        0xc8
    .end array-data

    :array_3c
    .array-data 4
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0x12c
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
        0xc8
    .end array-data
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;

    const/16 v1, 0x33

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/camera/SettingChecker;->mOverrideSettingValues:[Ljava/lang/String;

    new-array v0, v1, [Lcom/android/camera/ListPreference;

    iput-object v0, p0, Lcom/android/camera/SettingChecker;->mListPrefs:[Lcom/android/camera/ListPreference;

    iput-object p1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/SettingChecker;)Lcom/android/camera/Camera;
    .locals 1
    .param p0    # Lcom/android/camera/SettingChecker;

    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/SettingChecker;->LOG:Z

    return v0
.end method

.method private static applyModeTableToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[II)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # [I
    .param p4    # I

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x0

    sget-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_NOT_IN_PREFERENCE:[I

    array-length v7, v0

    :goto_0
    if-ge v6, v7, :cond_1

    sget-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_NOT_IN_PREFERENCE:[I

    aget v9, v0, v6

    invoke-static {p3, v9}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v10

    new-array v5, v10, [I

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v10, :cond_2

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v5, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    sget-object v3, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/android/camera/SettingChecker;->applyTableToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[[II[I)V

    return-void
.end method

.method private static applyPreferenceToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;II)Landroid/hardware/Camera$Parameters;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # I
    .param p4    # I

    sget-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_NOT_IN_PREFERENCE:[I

    invoke-static {v0, p4}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getSettingChecker()Lcom/android/camera/SettingChecker;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/android/camera/SettingChecker;->getOverrideSettingValue(I)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    invoke-static {v0, p3, p4}, Lcom/android/camera/SettingChecker;->getMatrixValue([[III)Ljava/lang/String;

    move-result-object v5

    :cond_0
    if-nez v5, :cond_1

    invoke-static {p0, p1, p4}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v5

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/android/camera/SettingChecker;->applyValueToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;IILjava/lang/String;)Landroid/hardware/Camera$Parameters;

    move-result-object p2

    :cond_2
    return-object p2
.end method

.method private static applyPreferenceToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[II)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # [I
    .param p4    # I

    const/4 v0, 0x0

    array-length v1, p3

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p3, v0

    invoke-static {p0, p1, p2, p4, v2}, Lcom/android/camera/SettingChecker;->applyPreferenceToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;II)Landroid/hardware/Camera$Parameters;

    move-result-object p2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static applyRestrictionsToParameters(Landroid/content/Context;Landroid/hardware/Camera$Parameters;[I)V
    .locals 18
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/hardware/Camera$Parameters;
    .param p2    # [I

    const/4 v5, 0x0

    sget-object v15, Lcom/android/camera/SettingChecker;->RESTRICTIOINS:[Lcom/android/camera/Restriction;

    array-length v8, v15

    :goto_0
    if-ge v5, v8, :cond_7

    sget-object v15, Lcom/android/camera/SettingChecker;->RESTRICTIOINS:[Lcom/android/camera/Restriction;

    aget-object v2, v15, v5

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v15

    move-object/from16 v0, p2

    invoke-static {v0, v15}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v15

    if-nez v15, :cond_1

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/android/camera/SettingChecker;->getParameterValue(Landroid/hardware/Camera$Parameters;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/camera/Restriction;->getValues()Ljava/util/List;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-interface {v10, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-virtual {v2}, Lcom/android/camera/Restriction;->getRestrictioins()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/camera/Restriction;

    invoke-virtual {v12}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v9

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v15

    if-eqz v15, :cond_2

    const/4 v14, 0x0

    invoke-virtual {v12}, Lcom/android/camera/Restriction;->getValues()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-le v15, v0, :cond_4

    move-object/from16 v0, p0

    instance-of v15, v0, Lcom/android/camera/Camera;

    if-eqz v15, :cond_4

    move-object/from16 v15, p0

    check-cast v15, Lcom/android/camera/Camera;

    invoke-virtual {v15, v9}, Lcom/android/camera/Camera;->getListPreference(I)Lcom/android/camera/ListPreference;

    move-result-object v11

    if-eqz v11, :cond_4

    invoke-virtual {v12}, Lcom/android/camera/Restriction;->getValues()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v11}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v15

    invoke-static {v15, v13}, Lcom/android/camera/SettingUtils;->contains([Ljava/lang/CharSequence;Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_3

    move-object v14, v13

    :cond_4
    if-nez v14, :cond_5

    invoke-virtual {v12}, Lcom/android/camera/Restriction;->getValues()Ljava/util/List;

    move-result-object v15

    const/16 v16, 0x0

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    :cond_5
    move-object/from16 v0, p1

    invoke-static {v0, v9, v14}, Lcom/android/camera/SettingChecker;->isParametersSupportedValue(Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v9, v14}, Lcom/android/camera/SettingChecker;->setParameterValue(Landroid/content/Context;Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    goto :goto_1

    :cond_6
    const-string v15, "SettingChecker"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "applyRestrictionsToParameters() not support limitedRow="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", value="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_7
    return-void
.end method

.method private static applySceneTableToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[I)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # [I

    sget-object v0, Lcom/android/camera/SettingChecker;->KEYS_FOR_SCENE:[Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {p2, v1}, Lcom/android/camera/SettingChecker;->getParameterValue(Landroid/hardware/Camera$Parameters;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/SettingUtils;->index([Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    sget-object v3, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/android/camera/SettingChecker;->applyTableToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[[II[I)V

    return-void
.end method

.method private static applyTableToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[[II[I)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # [[I
    .param p4    # I
    .param p5    # [I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move-object/from16 v0, p5

    array-length v4, v0

    :goto_0
    if-ge v1, v4, :cond_3

    aget v5, p5, v1

    aget-object v6, p3, v5

    if-eqz v6, :cond_0

    move-object v9, p0

    check-cast v9, Lcom/android/camera/Camera;

    invoke-virtual {v9}, Lcom/android/camera/Camera;->getSettingChecker()Lcom/android/camera/SettingChecker;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/android/camera/SettingChecker;->getOverrideSettingValue(I)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    sget-object v9, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v2, v9, v5

    aget v7, v6, p4

    const/4 v8, 0x0

    sparse-switch v7, :sswitch_data_0

    :goto_2
    :sswitch_0
    if-eqz v8, :cond_0

    invoke-static {p2, v5, v8}, Lcom/android/camera/SettingChecker;->isParametersSupportedValue(Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-static {p0, p2, v5, v8}, Lcom/android/camera/SettingChecker;->setParameterValue(Landroid/content/Context;Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    goto :goto_1

    :sswitch_1
    sget-object v9, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v9, v9, v5

    const/4 v10, 0x0

    aget-object v8, v9, v10

    goto :goto_2

    :sswitch_2
    sget-object v9, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v9, v9, v5

    const/4 v10, 0x1

    aget-object v8, v9, v10

    goto :goto_2

    :sswitch_3
    sget-object v9, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v9, v9, v5

    const/4 v10, 0x2

    aget-object v8, v9, v10

    goto :goto_2

    :sswitch_4
    sget-object v9, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v9, v9, v5

    const/4 v10, 0x3

    aget-object v8, v9, v10

    goto :goto_2

    :sswitch_5
    sget-object v9, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v9, v9, v5

    const/4 v10, 0x4

    aget-object v8, v9, v10

    goto :goto_2

    :sswitch_6
    sget-object v9, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v9, v9, v5

    const/4 v10, 0x5

    aget-object v8, v9, v10

    goto :goto_2

    :sswitch_7
    sget-object v9, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v9, v9, v5

    const/4 v10, 0x6

    aget-object v8, v9, v10

    goto :goto_2

    :sswitch_8
    sget-object v9, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v9, v9, v5

    const/4 v10, 0x7

    aget-object v8, v9, v10

    goto :goto_2

    :cond_2
    const-string v9, "SettingChecker"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "applyTableToParameters() not support row="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", value="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_0
        0x12c -> :sswitch_1
        0x12d -> :sswitch_2
        0x12e -> :sswitch_3
        0x12f -> :sswitch_4
        0x130 -> :sswitch_5
        0x131 -> :sswitch_6
        0x132 -> :sswitch_7
        0x133 -> :sswitch_8
    .end sparse-switch
.end method

.method private static applyValueToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;IILjava/lang/String;)Landroid/hardware/Camera$Parameters;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    invoke-static {p2, p4, p5}, Lcom/android/camera/SettingChecker;->isParametersSupportedValue(Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static/range {p0 .. p5}, Lcom/android/camera/SettingChecker;->getCapabilitySupportedValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;IILjava/lang/String;)Ljava/lang/String;

    move-result-object p5

    invoke-static {p0, p2, p4, p5}, Lcom/android/camera/SettingChecker;->setParameterValue(Landroid/content/Context;Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->applyParametersToServer()V

    check-cast p0, Lcom/android/camera/Camera;

    invoke-virtual {p0}, Lcom/android/camera/Camera;->fetchParametersFromServer()Landroid/hardware/Camera$Parameters;

    move-result-object p2

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    const-string v0, "SettingChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyValueToParameters() not support mode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", row="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", value="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private clearOverrideSettings([I)V
    .locals 5
    .param p1    # [I

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getPreferenceGroup()Lcom/android/camera/PreferenceGroup;

    move-result-object v0

    const/4 v1, 0x0

    array-length v3, p1

    :goto_0
    if-ge v1, v3, :cond_1

    aget v4, p1, v1

    invoke-virtual {p0, v4}, Lcom/android/camera/SettingChecker;->getListPreference(I)Lcom/android/camera/ListPreference;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/android/camera/ListPreference;->setOverrideValue(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static computeDesiredPreviewSize(Lcom/android/camera/Camera;Landroid/media/CamcorderProfile;Landroid/hardware/Camera$Parameters;)Landroid/graphics/Point;
    .locals 14
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Landroid/media/CamcorderProfile;
    .param p2    # Landroid/hardware/Camera$Parameters;

    const/4 v11, -0x1

    const/4 v10, -0x1

    invoke-virtual/range {p2 .. p2}, Landroid/hardware/Camera$Parameters;->getSupportedVideoSizes()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/Camera;->effectsActive()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget v11, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v10, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    :goto_0
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6, v11, v10}, Landroid/graphics/Point;-><init>(II)V

    sget-boolean v0, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v0, :cond_1

    const-string v0, "SettingChecker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "computeDesiredPreviewSize("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/camera/Camera;->effectsActive()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v6

    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Landroid/hardware/Camera$Parameters;->getPreferredPreviewSizeForVideo()Landroid/hardware/Camera$Size;

    move-result-object v9

    iget v0, v9, Landroid/hardware/Camera$Size;->width:I

    iget v2, v9, Landroid/hardware/Camera$Size;->height:I

    mul-int v12, v0, v2

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/hardware/Camera$Size;

    iget v0, v13, Landroid/hardware/Camera$Size;->width:I

    iget v2, v13, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v0, v2

    if-le v0, v12, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_4
    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    int-to-double v2, v0

    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    int-to-double v4, v0

    div-double/2addr v2, v4

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isVideoWallPaperIntent()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v4, 0x1

    :goto_2
    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/android/camera/Util;->getOptimalPreviewSize(Landroid/app/Activity;Ljava/util/List;DZZ)Landroid/hardware/Camera$Size;

    move-result-object v8

    if-eqz v8, :cond_6

    iget v11, v8, Landroid/hardware/Camera$Size;->width:I

    iget v10, v8, Landroid/hardware/Camera$Size;->height:I

    goto :goto_0

    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    :cond_6
    iget v11, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v10, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    goto :goto_0
.end method

.method private static fillCapabilityKeyValues(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[ILjava/util/List;Lcom/android/camera/Restriction;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # [I
    .param p5    # Lcom/android/camera/Restriction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/android/camera/ComboPreferences;",
            "Landroid/hardware/Camera$Parameters;",
            "[I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/camera/Restriction;",
            ")V"
        }
    .end annotation

    invoke-virtual {p5}, Lcom/android/camera/Restriction;->getRestrictioins()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/camera/Restriction;

    invoke-virtual {v3}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v1

    invoke-static {p3, v1}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x0

    sget-object v4, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_PARAMETERS:[I

    invoke-static {v4, v1}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {p2, v1}, Lcom/android/camera/SettingChecker;->getParameterValue(Landroid/hardware/Camera$Parameters;I)Ljava/lang/String;

    move-result-object v2

    :cond_1
    :goto_1
    sget-object v4, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-interface {p4, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Lcom/android/camera/Restriction;->getValues()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/android/camera/SettingUtils;->buildEnableList([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p4, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    sget-object v4, Lcom/android/camera/SettingChecker;->SETTING_GROUP_NOT_IN_PREFERENCE:[I

    invoke-static {v4, v1}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {p0, p1, v1}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    return-void
.end method

.method public static filterUnsuportedPreference(Lcom/android/camera/CameraSettings;Lcom/android/camera/PreferenceGroup;I)Lcom/android/camera/PreferenceGroup;
    .locals 4
    .param p0    # Lcom/android/camera/CameraSettings;
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # I

    const/4 v0, 0x0

    sget-object v2, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v2, v2, p2

    array-length v1, v2

    :goto_0
    if-ge v0, v1, :cond_1

    sget-object v2, Lcom/android/camera/SettingChecker;->MATRIX_SETTING_VISIBLE:[[Z

    aget-object v2, v2, p2

    aget-boolean v2, v2, v0

    if-nez v2, :cond_0

    sget-object v2, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p0, p1, v2, v0}, Lcom/android/camera/CameraSettings;->removePreferenceFromScreen(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING:[Ljava/lang/String;

    array-length v1, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    sget-object v2, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING:[Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return-object p1
.end method

.method public static getCameraMode(I)I
    .locals 4
    .param p0    # I

    sget-object v1, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    const/16 v2, 0x28

    aget-object v1, v1, v2

    aget v0, v1, p0

    sget-boolean v1, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "SettingChecker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCameraMode("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v0
.end method

.method private static getCapabilityKeyValues(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[II)[Ljava/lang/String;
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # [I
    .param p4    # I

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    sget-object v1, Lcom/android/camera/SettingChecker;->CAPABILITIES:[Lcom/android/camera/Restriction;

    array-length v10, v1

    :goto_0
    if-ge v9, v10, :cond_7

    sget-object v1, Lcom/android/camera/SettingChecker;->CAPABILITIES:[Lcom/android/camera/Restriction;

    aget-object v6, v1, v9

    if-nez v6, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v6}, Lcom/android/camera/Restriction;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    invoke-virtual {v6}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v11

    const/4 v1, -0x1

    if-eq v11, v1, :cond_2

    move/from16 v0, p4

    if-ne v11, v0, :cond_0

    :cond_2
    invoke-virtual {v6}, Lcom/android/camera/Restriction;->getRestrictioins()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-static/range {v1 .. v6}, Lcom/android/camera/SettingChecker;->fillCapabilityKeyValues(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[ILjava/util/List;Lcom/android/camera/Restriction;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v6}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v7

    const/4 v8, 0x0

    const/16 v1, 0x2b

    if-ne v1, v7, :cond_5

    invoke-static/range {p4 .. p4}, Lcom/android/camera/SettingChecker;->isVideoMode(I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    :cond_4
    :goto_2
    invoke-virtual {v6}, Lcom/android/camera/Restriction;->getValues()Ljava/util/List;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-interface {v12, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-static/range {v1 .. v6}, Lcom/android/camera/SettingChecker;->fillCapabilityKeyValues(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[ILjava/util/List;Lcom/android/camera/Restriction;)V

    goto :goto_1

    :cond_5
    sget-object v1, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_PARAMETERS:[I

    invoke-static {v1, v7}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v0, p2

    invoke-static {v0, v7}, Lcom/android/camera/SettingChecker;->getParameterValue(Landroid/hardware/Camera$Parameters;I)Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    :cond_6
    sget-object v1, Lcom/android/camera/SettingChecker;->SETTING_GROUP_NOT_IN_PREFERENCE:[I

    invoke-static {v1, v7}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {p0, p1, v7}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    :cond_7
    check-cast p0, Lcom/android/camera/Camera;

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v1, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "off"

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v13

    if-lez v13, :cond_9

    new-array v1, v13, [Ljava/lang/String;

    invoke-interface {v5, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    :goto_3
    return-object v1

    :cond_9
    const/4 v1, 0x0

    goto :goto_3
.end method

.method private static getCapabilitySupportedValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;IILjava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    move-object/from16 v10, p5

    const/4 v4, 0x0

    sget-object v11, Lcom/android/camera/SettingChecker;->CAPABILITIES:[Lcom/android/camera/Restriction;

    array-length v6, v11

    :goto_0
    if-ge v4, v6, :cond_a

    sget-object v11, Lcom/android/camera/SettingChecker;->CAPABILITIES:[Lcom/android/camera/Restriction;

    aget-object v1, v11, v4

    invoke-virtual {v1}, Lcom/android/camera/Restriction;->getType()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_4

    invoke-virtual {v1}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v7

    const/4 v11, -0x1

    if-eq v7, v11, :cond_0

    move/from16 v0, p3

    if-ne v7, v0, :cond_2

    :cond_0
    invoke-virtual {v1}, Lcom/android/camera/Restriction;->getRestrictioins()Ljava/util/List;

    move-result-object v11

    if-eqz v11, :cond_2

    invoke-virtual {v1}, Lcom/android/camera/Restriction;->getRestrictioins()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/camera/Restriction;

    invoke-virtual {v9}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v11

    move/from16 v0, p4

    if-ne v11, v0, :cond_1

    move-object/from16 v0, p5

    invoke-virtual {v9, v0}, Lcom/android/camera/Restriction;->findSupported(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    :cond_2
    sget-boolean v11, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v11, :cond_3

    const-string v11, "SettingChecker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getCapabilitySupportedValue() limitedMode="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", mode="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", support="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v2

    const/4 v3, 0x0

    const/16 v11, 0x2b

    if-ne v11, v2, :cond_8

    invoke-static/range {p3 .. p3}, Lcom/android/camera/SettingChecker;->isVideoMode(I)Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    :cond_5
    :goto_2
    invoke-virtual {v1}, Lcom/android/camera/Restriction;->getValues()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-interface {v8, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-virtual {v1}, Lcom/android/camera/Restriction;->getRestrictioins()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/camera/Restriction;

    invoke-virtual {v9}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v11

    move/from16 v0, p4

    if-ne v11, v0, :cond_6

    move-object/from16 v0, p5

    invoke-virtual {v9, v0}, Lcom/android/camera/Restriction;->findSupported(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    :cond_7
    sget-boolean v11, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v11, :cond_3

    const-string v11, "SettingChecker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getCapabilitySupportedValue() curRow="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", curValue="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", support="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_8
    sget-object v11, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_PARAMETERS:[I

    invoke-static {v11, v2}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v11

    if-eqz v11, :cond_9

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/camera/SettingChecker;->getParameterValue(Landroid/hardware/Camera$Parameters;I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_9
    sget-object v11, Lcom/android/camera/SettingChecker;->SETTING_GROUP_NOT_IN_PREFERENCE:[I

    invoke-static {v11, v2}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v11

    if-nez v11, :cond_5

    invoke-static {p0, p1, v2}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_a
    sget-boolean v11, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v11, :cond_b

    const-string v11, "SettingChecker"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getCapabilitySupportedValue("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p4

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p5

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ") return "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    return-object v10
.end method

.method private static getDefaultValueFromXml(Landroid/content/Context;I)Ljava/lang/String;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # I

    sget-object v4, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING:[Ljava/lang/String;

    aget-object v3, v4, p1

    const/4 v0, 0x0

    if-nez v3, :cond_2

    const/4 v1, 0x0

    instance-of v4, p0, Lcom/android/camera/Camera;

    if-eqz v4, :cond_0

    move-object v4, p0

    check-cast v4, Lcom/android/camera/Camera;

    invoke-virtual {v4, p1}, Lcom/android/camera/Camera;->getListPreference(I)Lcom/android/camera/ListPreference;

    move-result-object v1

    :cond_0
    sget-boolean v4, Lcom/android/camera/CameraSettings;->SUPPORTED_SHOW_CONINUOUS_SHOT_NUMBER:Z

    if-nez v4, :cond_4

    const/16 v4, 0x9

    if-ne p1, v4, :cond_4

    const-string v3, "20"

    :cond_1
    :goto_0
    sget-object v4, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING:[Ljava/lang/String;

    aput-object v3, v4, p1

    sget-boolean v4, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v4, :cond_2

    const-string v4, "SettingChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getDefaultValueFromXml("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    sget-boolean v4, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v4, :cond_3

    const-string v4, "SettingChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getDefaultValueFromXml("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") return "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-object v3

    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/android/camera/ListPreference;->findSupportedDefaultValue()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_5

    invoke-virtual {v1}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v1}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_5

    invoke-virtual {v1}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    if-nez v3, :cond_1

    sget-object v4, Lcom/android/camera/SettingChecker;->DEFAULT_VALUE_FOR_SETTING_ID:[I

    aget v2, v4, p1

    const/4 v4, -0x1

    if-eq v2, v4, :cond_1

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0
.end method

.method private static getJpegQuality(Landroid/content/Context;I)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    check-cast p0, Lcom/android/camera/Camera;

    invoke-virtual {p0}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v0

    invoke-static {v0, p1}, Landroid/media/CameraProfile;->getJpegEncodingQualityParameter(II)I

    move-result v1

#    return v1
    const/16 p0, 0x64

    return p0
.end method

.method private static getMatrixValue([[III)Ljava/lang/String;
    .locals 6
    .param p0    # [[I
    .param p1    # I
    .param p2    # I

    aget-object v0, p0, p2

    aget v1, v0, p1

    const/4 v2, 0x0

    sparse-switch v1, :sswitch_data_0

    :goto_0
    :sswitch_0
    sget-boolean v3, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v3, :cond_0

    const-string v3, "SettingChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMatrixValue("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") return "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v2

    :sswitch_1
    sget-object v3, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v3, v3, p2

    const/4 v4, 0x0

    aget-object v2, v3, v4

    goto :goto_0

    :sswitch_2
    sget-object v3, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v3, v3, p2

    const/4 v4, 0x1

    aget-object v2, v3, v4

    goto :goto_0

    :sswitch_3
    sget-object v3, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v3, v3, p2

    const/4 v4, 0x2

    aget-object v2, v3, v4

    goto :goto_0

    :sswitch_4
    sget-object v3, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v3, v3, p2

    const/4 v4, 0x3

    aget-object v2, v3, v4

    goto :goto_0

    :sswitch_5
    sget-object v3, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v3, v3, p2

    const/4 v4, 0x4

    aget-object v2, v3, v4

    goto :goto_0

    :sswitch_6
    sget-object v3, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v3, v3, p2

    const/4 v4, 0x5

    aget-object v2, v3, v4

    goto :goto_0

    :sswitch_7
    sget-object v3, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v3, v3, p2

    const/4 v4, 0x6

    aget-object v2, v3, v4

    goto :goto_0

    :sswitch_8
    sget-object v3, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v3, v3, p2

    const/4 v4, 0x7

    aget-object v2, v3, v4

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_0
        0x12c -> :sswitch_1
        0x12d -> :sswitch_2
        0x12e -> :sswitch_3
        0x12f -> :sswitch_4
        0x130 -> :sswitch_5
        0x131 -> :sswitch_6
        0x132 -> :sswitch_7
        0x133 -> :sswitch_8
    .end sparse-switch
.end method

.method private static getMaxSupportedPreviewFrameRate(Ljava/util/List;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_1
    sget-boolean v3, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v3, :cond_2

    const-string v3, "SettingChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMaxSupportedPreviewFrameRate() return "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return v1
.end method

.method public static getModeContinousFocusMode(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    const/16 v0, 0x9

    if-ne p0, v0, :cond_0

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isContinuousFocusEnabledWhenTouch()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_FOCUS_MODE_CONTINUOUS:[Ljava/lang/String;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static getModeDefaultFocusModes(Landroid/content/Context;I)[Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/android/camera/SettingChecker;->MATRIX_FOCUS_MODE_DEFAULT_ARRAY:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getModeSettingGroupForParameters(I)[I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/android/camera/SettingChecker;->isVideoMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_VIDEO_FOR_PARAMETERS:[I

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_PARAMETERS:[I

    goto :goto_0
.end method

.method private static getModeSettingGroupForUI(I)[I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/android/camera/SettingChecker;->isVideoMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_VIDEO_FOR_UI:[I

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_UI:[I

    goto :goto_0
.end method

.method private static getModeTableKeyValues(I[I)[Ljava/lang/String;
    .locals 1
    .param p0    # I
    .param p1    # [I

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    invoke-static {v0, p0, p1}, Lcom/android/camera/SettingChecker;->getSettingKeyValues([[II[I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getParameterValue(Landroid/hardware/Camera$Parameters;I)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/hardware/Camera$Parameters;
    .param p1    # I

    if-nez p0, :cond_1

    const-string v2, "SettingChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getParameterValue("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") parameters=null!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v2, v3, v4}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_1
    :pswitch_0
    sget-boolean v2, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "SettingChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getParameterValue("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_2
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get dual camera from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_3
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getExposureCompensation()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSceneMode()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getWhiteBalance()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_6
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get image adjustment from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_7
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getHueMode()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_8
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getContrastMode()Ljava/lang/String;

    goto :goto_1

    :pswitch_9
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getEdgeMode()Ljava/lang/String;

    goto :goto_1

    :pswitch_a
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSaturationMode()Ljava/lang/String;

    goto :goto_1

    :pswitch_b
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getBrightnessMode()Ljava/lang/String;

    goto :goto_1

    :pswitch_c
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getColorEffect()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_d
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get geo tag from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_e
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getAntibanding()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_f
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get self timer from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_10
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getZSDMode()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_11
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get continuous number from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_12
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x78

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_13
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getISOSpeed()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_14
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get facebeauty adjustment from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_15
    const-string v2, "fb-smooth-level"

    invoke-virtual {p0, v2}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_16
    const-string v2, "fb-skin-color"

    invoke-virtual {p0, v2}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_17
    const-string v2, "fb-sharp"

    invoke-virtual {p0, v2}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_18
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getVideoStabilization()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_19
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get microphone from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_1a
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get audio mode from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_1b
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get time lapse from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_1c
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get live effect from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_1d
    const-string v2, "recording-hint"

    invoke-virtual {p0, v2}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_1e
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get picture ratio from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_1f
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get voice from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_20
    new-instance v2, Lcom/android/camera/CameraSettingException;

    const-string v3, "Cannot get face detection from parameters."

    invoke-direct {v2, v3}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_c
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_d
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_e
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1e
        :pswitch_1f
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1d
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_20
    .end packed-switch
.end method

.method public static getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # I

    sget-object v2, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v0, v2, p2

    invoke-static {p0, p2}, Lcom/android/camera/SettingChecker;->getDefaultValueFromXml(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x15

    if-ne p2, v2, :cond_0

    if-nez v1, :cond_0

    const-string v1, "1.3333"

    :cond_0
    sget-boolean v2, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v2, :cond_1

    const-string v2, "SettingChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getPreferenceValue("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v1
.end method

.method private static getRestrictionsKeyValues(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[I)[Ljava/lang/String;
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # [I

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    sget-object v10, Lcom/android/camera/SettingChecker;->RESTRICTIOINS:[Lcom/android/camera/Restriction;

    array-length v6, v10

    :goto_0
    if-ge v3, v6, :cond_5

    sget-object v10, Lcom/android/camera/SettingChecker;->RESTRICTIOINS:[Lcom/android/camera/Restriction;

    aget-object v0, v10, v3

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v10

    invoke-static {p3, v10}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v10

    if-nez v10, :cond_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v1

    const/4 v2, 0x0

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_PARAMETERS:[I

    invoke-static {v10, v1}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-static {p2, v1}, Lcom/android/camera/SettingChecker;->getParameterValue(Landroid/hardware/Camera$Parameters;I)Ljava/lang/String;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-virtual {v0}, Lcom/android/camera/Restriction;->getValues()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-interface {v7, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v0}, Lcom/android/camera/Restriction;->getRestrictioins()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/camera/Restriction;

    invoke-virtual {v8}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v10

    invoke-static {p3, v10}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v10

    if-eqz v10, :cond_3

    sget-object v10, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    invoke-virtual {v8}, Lcom/android/camera/Restriction;->getIndex()I

    move-result v11

    aget-object v10, v10, v11

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8}, Lcom/android/camera/Restriction;->getValues()Ljava/util/List;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_NOT_IN_PREFERENCE:[I

    invoke-static {v10, v1}, Lcom/android/camera/SettingUtils;->contains([II)Z

    move-result v10

    if-nez v10, :cond_2

    invoke-static {p0, p1, v1}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_5
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_6

    new-array v10, v9, [Ljava/lang/String;

    invoke-interface {v5, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    :goto_3
    return-object v10

    :cond_6
    const/4 v10, 0x0

    goto :goto_3
.end method

.method private static getSceneTableKeyValues(Landroid/hardware/Camera$Parameters;[I)[Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/hardware/Camera$Parameters;
    .param p1    # [I

    sget-object v1, Lcom/android/camera/SettingChecker;->KEYS_FOR_SCENE:[Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {p0, v2}, Lcom/android/camera/SettingChecker;->getParameterValue(Landroid/hardware/Camera$Parameters;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/SettingUtils;->index([Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lcom/android/camera/SettingChecker;->MATRIX_SCENE_STATE:[[I

    invoke-static {v1, v0, p1}, Lcom/android/camera/SettingChecker;->getSettingKeyValues([[II[I)[Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getSettingKeyValues([[II[I)[Ljava/lang/String;
    .locals 9
    .param p0    # [[I
    .param p1    # I
    .param p2    # [I

    sget-boolean v6, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v6, :cond_0

    const-string v6, "SettingChecker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSettingKeyValues("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    array-length v2, p2

    :goto_0
    if-ge v0, v2, :cond_2

    aget v3, p2, v0

    aget-object v4, p0, v3

    if-nez v4, :cond_1

    :goto_1
    :sswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    aget-object v6, p0, v3

    aget v5, v6, p1

    sparse-switch v5, :sswitch_data_0

    goto :goto_1

    :sswitch_1
    sget-object v6, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v6, "disable-value"

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_2
    sget-object v6, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v6, v6, v3

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_3
    sget-object v6, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v6, v6, v3

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_4
    sget-object v6, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v6, v6, v3

    const/4 v7, 0x2

    aget-object v6, v6, v7

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_5
    sget-object v6, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v6, v6, v3

    const/4 v7, 0x3

    aget-object v6, v6, v7

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_6
    sget-object v6, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v6, v6, v3

    const/4 v7, 0x4

    aget-object v6, v6, v7

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_7
    sget-object v6, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v6, v6, v3

    const/4 v7, 0x5

    aget-object v6, v6, v7

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_8
    sget-object v6, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v6, v6, v3

    const/4 v7, 0x6

    aget-object v6, v6, v7

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_9
    sget-object v6, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v6, v6, v3

    const/4 v7, 0x7

    aget-object v6, v6, v7

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v1, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    return-object v6

    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_0
        0x12c -> :sswitch_2
        0x12d -> :sswitch_3
        0x12e -> :sswitch_4
        0x12f -> :sswitch_5
        0x130 -> :sswitch_6
        0x131 -> :sswitch_7
        0x132 -> :sswitch_8
        0x133 -> :sswitch_9
    .end sparse-switch
.end method

.method public static getSettingKeys([I)[Ljava/lang/String;
    .locals 5
    .param p0    # [I

    if-eqz p0, :cond_0

    array-length v2, p0

    new-array v1, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    sget-object v3, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget v4, p0, v0

    aget-object v3, v3, v4

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1
.end method

.method private getVirtualMode(I)I
    .locals 4
    .param p1    # I

    move v0, p1

    const/16 v1, 0x9

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->effectsActive()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xc

    :cond_0
    sget-boolean v1, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "SettingChecker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVirtualMode("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0
.end method

.method private static isParametersSupportedValue(Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z
    .locals 20
    .param p0    # Landroid/hardware/Camera$Parameters;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p0, :cond_1

    const-string v17, "SettingChecker"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "isParametersSupportedValue("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ") parameters=null!!!"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/Throwable;

    invoke-direct/range {v19 .. v19}, Ljava/lang/Throwable;-><init>()V

    invoke-static/range {v17 .. v19}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v14, 0x0

    :cond_0
    :goto_0
    return v14

    :cond_1
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_2
    :goto_1
    :pswitch_0
    if-nez v15, :cond_3

    :goto_2
    sget-boolean v17, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v17, :cond_0

    const-string v17, "SettingChecker"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "isParametersSupportedValue("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ") supportedList="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " return "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v15

    goto :goto_1

    :pswitch_2
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot get dual camera capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getMaxExposureCompensation()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getMinExposureCompensation()I

    move-result v7

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-lt v2, v7, :cond_2

    if-gt v2, v3, :cond_2

    const/4 v14, 0x1

    goto :goto_1

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedSceneModes()Ljava/util/List;

    move-result-object v15

    goto :goto_1

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedWhiteBalance()Ljava/util/List;

    move-result-object v15

    goto :goto_1

    :pswitch_6
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot get image adjustment capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedHueMode()Ljava/util/List;

    move-result-object v15

    goto :goto_1

    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedContrastMode()Ljava/util/List;

    move-result-object v15

    goto/16 :goto_1

    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedEdgeMode()Ljava/util/List;

    move-result-object v15

    goto/16 :goto_1

    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedSaturationMode()Ljava/util/List;

    move-result-object v15

    goto/16 :goto_1

    :pswitch_b
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedBrightnessMode()Ljava/util/List;

    move-result-object v15

    goto/16 :goto_1

    :pswitch_c
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedColorEffects()Ljava/util/List;

    move-result-object v15

    goto/16 :goto_1

    :pswitch_d
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot get geo tag capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :pswitch_e
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedAntibanding()Ljava/util/List;

    move-result-object v15

    goto/16 :goto_1

    :pswitch_f
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot get self timer capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :pswitch_10
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedZSDMode()Ljava/util/List;

    move-result-object v15

    goto/16 :goto_1

    :pswitch_11
    const/4 v14, 0x1

    goto/16 :goto_1

    :pswitch_12
    const/4 v14, 0x1

    goto/16 :goto_1

    :pswitch_13
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->getSupportedISOSpeed()Ljava/util/List;

    move-result-object v15

    goto/16 :goto_1

    :pswitch_14
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot get facebeauty adjustment capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :pswitch_15
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/camera/ParametersHelper;->getMaxLevel(Landroid/hardware/Camera$Parameters;I)I

    move-result v6

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/camera/ParametersHelper;->getMinLevel(Landroid/hardware/Camera$Parameters;I)I

    move-result v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    if-lt v13, v10, :cond_2

    if-gt v13, v6, :cond_2

    const/4 v14, 0x1

    goto/16 :goto_1

    :pswitch_16
    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/camera/ParametersHelper;->getMaxLevel(Landroid/hardware/Camera$Parameters;I)I

    move-result v5

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/camera/ParametersHelper;->getMinLevel(Landroid/hardware/Camera$Parameters;I)I

    move-result v9

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    if-lt v12, v9, :cond_2

    if-gt v12, v5, :cond_2

    const/4 v14, 0x1

    goto/16 :goto_1

    :pswitch_17
    const/16 v17, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/camera/ParametersHelper;->getMaxLevel(Landroid/hardware/Camera$Parameters;I)I

    move-result v4

    const/16 v17, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/camera/ParametersHelper;->getMinLevel(Landroid/hardware/Camera$Parameters;I)I

    move-result v8

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    if-lt v11, v8, :cond_2

    if-gt v11, v4, :cond_2

    const/4 v14, 0x1

    goto/16 :goto_1

    :pswitch_18
    invoke-virtual/range {p0 .. p0}, Landroid/hardware/Camera$Parameters;->isVideoStabilizationSupported()Z

    move-result v14

    goto/16 :goto_1

    :pswitch_19
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot get microphone capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :pswitch_1a
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot get audio mode capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :pswitch_1b
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot time lapse capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :pswitch_1c
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot get live effect capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :pswitch_1d
    const/4 v14, 0x1

    goto/16 :goto_1

    :pswitch_1e
    const/4 v14, 0x1

    goto/16 :goto_1

    :pswitch_1f
    const/4 v14, 0x1

    goto/16 :goto_1

    :pswitch_20
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot get voice capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :pswitch_21
    new-instance v17, Lcom/android/camera/CameraSettingException;

    const-string v18, "Cannot get fd capability."

    invoke-direct/range {v17 .. v18}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v17

    :cond_3
    move-object/from16 v0, p2

    invoke-interface {v15, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v17

    if-ltz v17, :cond_4

    const/4 v14, 0x1

    goto/16 :goto_2

    :cond_4
    const/4 v14, 0x0

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_c
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_d
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_e
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1f
        :pswitch_20
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_21
    .end packed-switch
.end method

.method public static isSupported(Ljava/lang/Object;Ljava/util/List;)Z
    .locals 2
    .param p0    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<*>;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isVideoMode(I)Z
    .locals 7
    .param p0    # I

    const/16 v5, 0x2b

    sget-object v4, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    aget-object v4, v4, v5

    aget v1, v4, p0

    rem-int/lit8 v0, v1, 0x64

    sget-object v4, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v4, v4, v5

    aget-object v2, v4, v0

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    sget-boolean v4, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v4, :cond_0

    const-string v4, "SettingChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isVideoMode("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") return "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v3
.end method

.method public static isZoomEnable(I)Z
    .locals 1
    .param p0    # I

    sget-object v0, Lcom/android/camera/SettingChecker;->MATRIX_ZOOM_ENABLE:[Z

    aget-boolean v0, v0, p0

    return v0
.end method

.method private varargs overrideSettings([Ljava/lang/String;)V
    .locals 7
    .param p1    # [Ljava/lang/String;

    if-eqz p1, :cond_0

    array-length v4, p1

    const/4 v5, 0x2

    if-ge v4, v5, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_0

    aget-object v1, p1, v0

    add-int/lit8 v4, v0, 0x1

    aget-object v3, p1, v4

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4, v1}, Lcom/android/camera/Camera;->getListPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, v3}, Lcom/android/camera/ListPreference;->setOverrideValue(Ljava/lang/String;)V

    sget-boolean v4, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v4, :cond_2

    const-string v4, "SettingChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "overrideSettings() key="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", value="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    add-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method private resetSettings(Lcom/android/camera/ComboPreferences;)V
    .locals 8
    .param p1    # Lcom/android/camera/ComboPreferences;

    invoke-virtual {p1}, Lcom/android/camera/ComboPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v5, Lcom/android/camera/SettingChecker;->RESET_SETTING_ITEMS:[I

    array-length v3, v5

    :goto_0
    if-ge v1, v3, :cond_1

    sget-object v5, Lcom/android/camera/SettingChecker;->RESET_SETTING_ITEMS:[I

    aget v4, v5, v1

    sget-object v5, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v2, v5, v4

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    sget-boolean v5, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v5, :cond_0

    const-string v5, "SettingChecker"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "resetSettings() remove key["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static reviseVideoCapability(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;Landroid/media/CamcorderProfile;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # Landroid/media/CamcorderProfile;

    const/4 v6, 0x3

    sget-boolean v3, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v3, :cond_0

    const-string v3, "SettingChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reviseVideoCapability() begin profile.videoFrameRate="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p3, Landroid/media/CamcorderProfile;->videoFrameRate:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFrameRates()Ljava/util/List;

    move-result-object v2

    iget v3, p3, Landroid/media/CamcorderProfile;->videoFrameRate:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/android/camera/SettingChecker;->isSupported(Ljava/lang/Object;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v2}, Lcom/android/camera/SettingChecker;->getMaxSupportedPreviewFrameRate(Ljava/util/List;)I

    move-result v0

    iget v3, p3, Landroid/media/CamcorderProfile;->videoBitRate:I

    mul-int/2addr v3, v0

    iget v4, p3, Landroid/media/CamcorderProfile;->videoFrameRate:I

    div-int/2addr v3, v4

    iput v3, p3, Landroid/media/CamcorderProfile;->videoBitRate:I

    iput v0, p3, Landroid/media/CamcorderProfile;->videoFrameRate:I

    :cond_1
    invoke-static {p0, p1, v6}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v6, v1}, Lcom/android/camera/SettingChecker;->isParametersSupportedValue(Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "night"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p3, Landroid/media/CamcorderProfile;->videoFrameRate:I

    div-int/lit8 v3, v3, 0x2

    iput v3, p3, Landroid/media/CamcorderProfile;->videoFrameRate:I

    iget v3, p3, Landroid/media/CamcorderProfile;->videoBitRate:I

    div-int/lit8 v3, v3, 0x2

    iput v3, p3, Landroid/media/CamcorderProfile;->videoBitRate:I

    :cond_2
    sget-boolean v3, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v3, :cond_3

    const-string v3, "SettingChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reviseVideoCapability() end profile.videoFrameRate="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p3, Landroid/media/CamcorderProfile;->videoFrameRate:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private static setParameterValue(Landroid/content/Context;Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/hardware/Camera$Parameters;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x0

    if-nez p1, :cond_0

    const-string v6, "SettingChecker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setParameterValue("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") parameters=null!!!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/Throwable;

    invoke-direct {v8}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v6, v7, v8}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return v5

    :cond_0
    const/4 v3, 0x0

    sget-object v6, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    aget-object v2, v6, p2

    packed-switch p2, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    sget-boolean v6, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v6, :cond_2

    const-string v6, "SettingChecker"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setParameterValue("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") return "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v5, v3

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set dual camera to parameters."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_3
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/hardware/Camera$Parameters;->setExposureCompensation(I)V

    goto :goto_1

    :pswitch_4
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSceneMode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setSceneMode(Ljava/lang/String;)V

    const/4 v3, 0x1

    goto :goto_1

    :pswitch_5
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setWhiteBalance(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_6
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set total image adjustement, Please use Hue, Contrast, Edge, staturation and Brightness."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_7
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setHueMode(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_8
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setContrastMode(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_9
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setEdgeMode(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_a
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setSaturationMode(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_b
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setBrightnessMode(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_c
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setColorEffect(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_d
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set geo tag to parameters."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_e
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setAntibanding(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_f
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set self timer to parameters."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_10
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setZSDMode(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_11
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Please use ROW_SETTING_CONTINUOUS_NUM to set shot number."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_12
    invoke-static {p0, p1, p3}, Lcom/android/camera/SettingChecker;->setPicturePreview(Landroid/content/Context;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_13
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setISOSpeed(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_14
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set total facebeauty adjustement, Please use Smooth, SkinColor and Sharp."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_15
    const-string v6, "fb-smooth-level"

    invoke-virtual {p1, v6, p3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_16
    const-string v6, "fb-skin-color"

    invoke-virtual {p1, v6, p3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_17
    const-string v6, "fb-sharp"

    invoke-virtual {p1, v6, p3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_18
    const-string v6, "on"

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v5, 0x1

    :cond_3
    invoke-virtual {p1, v5}, Landroid/hardware/Camera$Parameters;->setVideoStabilization(Z)V

    goto/16 :goto_1

    :pswitch_19
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set microphone to parameters."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_1a
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set audio mode to parameters."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_1b
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set timelapse to parameters."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_1c
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set live effect to parameters."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_1d
    invoke-static {p0, p1, p3}, Lcom/android/camera/SettingChecker;->setVideoPreview(Landroid/content/Context;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_1e
    invoke-static {p3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {p1, v6}, Landroid/hardware/Camera$Parameters;->setRecordingHint(Z)V

    goto/16 :goto_1

    :pswitch_1f
    invoke-virtual {p1, p3}, Landroid/hardware/Camera$Parameters;->setCaptureMode(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_20
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/hardware/Camera$Parameters;->setBurstShotNum(I)V

    goto/16 :goto_1

    :pswitch_21
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {p0, v6}, Lcom/android/camera/SettingChecker;->getJpegQuality(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/hardware/Camera$Parameters;->setJpegQuality(I)V

    goto/16 :goto_1

    :pswitch_22
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/hardware/Camera$Parameters;->setCameraMode(I)V

    goto/16 :goto_1

    :pswitch_23
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set picture ratiot to parameters."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_24
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set voice to parameters."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_25
    new-instance v6, Lcom/android/camera/CameraSettingException;

    const-string v7, "Cannot set face detection to parameters."

    invoke-direct {v6, v7}, Lcom/android/camera/CameraSettingException;-><init>(Ljava/lang/String;)V

    throw v6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_c
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_d
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_e
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_23
        :pswitch_24
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_22
        :pswitch_1f
        :pswitch_20
        :pswitch_1e
        :pswitch_21
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_25
    .end packed-switch
.end method

.method private static setPicturePreview(Landroid/content/Context;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)V
    .locals 18
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/hardware/Camera$Parameters;
    .param p2    # Ljava/lang/String;

    sget-boolean v7, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v7, :cond_0

    const-string v7, "SettingChecker"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "setPicturePreview("

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v16, ")"

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v3, p0

    check-cast v3, Lcom/android/camera/Camera;

    if-nez p2, :cond_4

    invoke-static/range {p0 .. p1}, Lcom/android/camera/CameraSettings;->initialCameraPictureSize(Landroid/content/Context;Landroid/hardware/Camera$Parameters;)V

    :goto_0
    const-string v7, "1.3333"

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v7

    const/16 v8, 0x15

    move-object/from16 v0, p0

    invoke-static {v0, v7, v8}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "1.3333"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v11

    iget v7, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v8, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v7, v8, :cond_5

    iget v7, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v7, v7

    iget v0, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v5, v7, v16

    :cond_1
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-static/range {v3 .. v8}, Lcom/android/camera/Util;->getOptimalPreviewSize(Landroid/app/Activity;Ljava/util/List;DZZ)Landroid/hardware/Camera$Size;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v13

    invoke-virtual {v13, v12}, Landroid/hardware/Camera$Size;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    iget v7, v12, Landroid/hardware/Camera$Size;->width:I

    iget v8, v12, Landroid/hardware/Camera$Size;->height:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFrameRates()Ljava/util/List;

    move-result-object v9

    if-eqz v9, :cond_3

    invoke-static {v9}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    :cond_3
    return-void

    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v14

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v7

    const/16 v8, 0x15

    move-object/from16 v0, p0

    invoke-static {v0, v7, v8}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    move-object/from16 v2, p0

    invoke-static {v0, v14, v1, v15, v2}, Lcom/android/camera/CameraSettings;->setCameraPictureSize(Ljava/lang/String;Ljava/util/List;Landroid/hardware/Camera$Parameters;Ljava/lang/String;Landroid/content/Context;)Z

    goto/16 :goto_0

    :cond_5
    iget v7, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v7, v7

    iget v0, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v5, v7, v16

    goto :goto_1
.end method

.method private static setVideoPreview(Landroid/content/Context;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)V
    .locals 23
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/hardware/Camera$Parameters;
    .param p2    # Ljava/lang/String;

    move-object/from16 v3, p0

    check-cast v3, Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v4

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v9

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0c010f

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v4, v0}, Lcom/android/camera/CameraSettings;->getDefaultVideoQuality(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v19, "pref_video_quality_key"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0, v5}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v15

    const-string v19, "android.intent.extra.videoQuality"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    const-string v19, "android.intent.extra.videoQuality"

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    if-lez v6, :cond_6

    invoke-static {v4, v6}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v19

    if-eqz v19, :cond_4

    move v15, v6

    :cond_0
    :goto_0
    invoke-virtual {v3}, Lcom/android/camera/Camera;->effectsActive()Z

    move-result v19

    if-eqz v19, :cond_1

    if-nez v17, :cond_1

    const/16 v19, 0x10

    move/from16 v0, v19

    invoke-static {v4, v0}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v19

    if-eqz v19, :cond_7

    const/16 v15, 0x10

    :cond_1
    :goto_1
    const/16 v19, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v9, v1}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v3, v15, v0}, Lcom/android/camera/Camera;->fetchProfile(II)Landroid/media/CamcorderProfile;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v3, v14, v0}, Lcom/android/camera/SettingChecker;->computeDesiredPreviewSize(Lcom/android/camera/Camera;Landroid/media/CamcorderProfile;Landroid/hardware/Camera$Parameters;)Landroid/graphics/Point;

    move-result-object v13

    new-instance v12, Landroid/graphics/Point;

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/hardware/Camera$Size;->width:I

    move/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v12, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v12, v13}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_2

    iget v0, v13, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Point;->y:I

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v9, v1, v14}, Lcom/android/camera/SettingChecker;->reviseVideoCapability(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;Landroid/media/CamcorderProfile;)V

    iget v0, v14, Landroid/media/CamcorderProfile;->videoFrameRate:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->isVideoSnapshotSupported()Z

    move-result v19

    if-eqz v19, :cond_9

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v16

    iget v0, v13, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v19, v0

    iget v0, v13, Landroid/graphics/Point;->y:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v21, v0

    div-double v19, v19, v21

    move-object/from16 v0, v16

    move-wide/from16 v1, v19

    invoke-static {v0, v1, v2}, Lcom/android/camera/Util;->getOptimalVideoSnapshotPictureSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v11

    invoke-virtual {v11, v10}, Landroid/hardware/Camera$Size;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_3

    iget v0, v10, Landroid/hardware/Camera$Size;->width:I

    move/from16 v19, v0

    iget v0, v10, Landroid/hardware/Camera$Size;->height:I

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    const/16 v19, 0x9

    move/from16 v0, v19

    invoke-static {v4, v0}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v19

    if-eqz v19, :cond_5

    const/16 v15, 0x9

    goto/16 :goto_0

    :cond_5
    const/16 v15, 0xa

    goto/16 :goto_0

    :cond_6
    const/4 v15, 0x0

    goto/16 :goto_0

    :cond_7
    if-nez v4, :cond_8

    const/16 v15, 0x9

    goto/16 :goto_1

    :cond_8
    const/16 v15, 0xa

    goto/16 :goto_1

    :cond_9
    iget v0, v13, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/Point;->y:I

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    goto :goto_2
.end method


# virtual methods
.method public applyFocusCapabilities(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getAeLockSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getAeLock()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setAutoExposureLock(Z)V

    :cond_0
    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getAwbLockSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getAwbLock()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setAutoWhiteBalanceLock(Z)V

    :cond_1
    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getFocusAreaSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getFocusAreas()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    :cond_2
    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getMeteringAreaSupported()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getMeteringAreas()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    :cond_3
    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraActor()Lcom/android/camera/actor/CameraActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->handleFocus()Z

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getFocusMode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->applyContinousCallback()V

    return-void
.end method

.method public applyLimitToParameters()V
    .locals 4

    sget-boolean v0, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyLimitToParameters() mContext.getParameters()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    const/16 v2, 0x8

    const-string v3, "off"

    invoke-static {v0, v1, v2, v3}, Lcom/android/camera/SettingChecker;->setParameterValue(Landroid/content/Context;Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    :cond_1
    return-void
.end method

.method public applyParametersToUI()V
    .locals 6

    sget-boolean v4, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v4, :cond_0

    const-string v4, "SettingChecker"

    const-string v5, "applyParametersToUI()"

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v2

    sget-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_ALL_IN_SETTING:[I

    invoke-direct {p0, v0}, Lcom/android/camera/SettingChecker;->clearOverrideSettings([I)V

    invoke-direct {p0, v2}, Lcom/android/camera/SettingChecker;->getVirtualMode(I)I

    move-result v4

    invoke-static {v4, v0}, Lcom/android/camera/SettingChecker;->getModeTableKeyValues(I[I)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/camera/SettingChecker;->overrideSettings([Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/android/camera/SettingChecker;->getSceneTableKeyValues(Landroid/hardware/Camera$Parameters;[I)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/camera/SettingChecker;->overrideSettings([Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v5, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-static {v4, v3, v5, v0}, Lcom/android/camera/SettingChecker;->getRestrictionsKeyValues(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[I)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/camera/SettingChecker;->overrideSettings([Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v5, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-static {v4, v3, v5, v0, v2}, Lcom/android/camera/SettingChecker;->getCapabilityKeyValues(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[II)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/camera/SettingChecker;->overrideSettings([Ljava/lang/String;)V

    return-void
.end method

.method public applyParametersToUIImmediately()V
    .locals 6

    sget-boolean v4, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v4, :cond_0

    const-string v4, "SettingChecker"

    const-string v5, "applyParametersToUIImmediately()"

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v0, Lcom/android/camera/SettingChecker;->SETTING_GROUP_ALL_IN_SCREEN:[I

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v2

    invoke-direct {p0, v0}, Lcom/android/camera/SettingChecker;->clearOverrideSettings([I)V

    invoke-direct {p0, v2}, Lcom/android/camera/SettingChecker;->getVirtualMode(I)I

    move-result v4

    invoke-static {v4, v0}, Lcom/android/camera/SettingChecker;->getModeTableKeyValues(I[I)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/camera/SettingChecker;->overrideSettings([Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/android/camera/SettingChecker;->getSceneTableKeyValues(Landroid/hardware/Camera$Parameters;[I)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/camera/SettingChecker;->overrideSettings([Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v5, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-static {v4, v3, v5, v0}, Lcom/android/camera/SettingChecker;->getRestrictionsKeyValues(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[I)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/camera/SettingChecker;->overrideSettings([Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v5, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-static {v4, v3, v5, v0, v2}, Lcom/android/camera/SettingChecker;->getCapabilityKeyValues(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[II)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/camera/SettingChecker;->overrideSettings([Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    new-instance v5, Lcom/android/camera/SettingChecker$1;

    invoke-direct {v5, p0}, Lcom/android/camera/SettingChecker$1;-><init>(Lcom/android/camera/SettingChecker;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public applyPreferenceToParameters()V
    .locals 6

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v1

    invoke-static {v1}, Lcom/android/camera/SettingChecker;->getModeSettingGroupForParameters(I)[I

    move-result-object v0

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-direct {p0, v1}, Lcom/android/camera/SettingChecker;->getVirtualMode(I)I

    move-result v5

    invoke-static {v3, v2, v4, v0, v5}, Lcom/android/camera/SettingChecker;->applyPreferenceToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[II)V

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-direct {p0, v1}, Lcom/android/camera/SettingChecker;->getVirtualMode(I)I

    move-result v5

    invoke-static {v3, v2, v4, v0, v5}, Lcom/android/camera/SettingChecker;->applyModeTableToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[II)V

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-static {v3, v2, v4, v0}, Lcom/android/camera/SettingChecker;->applySceneTableToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;[I)V

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/android/camera/SettingChecker;->applyRestrictionsToParameters(Landroid/content/Context;Landroid/hardware/Camera$Parameters;[I)V

    invoke-virtual {p0}, Lcom/android/camera/SettingChecker;->applyLimitToParameters()V

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/android/camera/SettingChecker;->applyFocusCapabilities(Z)V

    return-void
.end method

.method public applyValueToParameters(ILjava/lang/String;)V
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyValueToParameters("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v3

    move v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/android/camera/SettingChecker;->applyValueToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;IILjava/lang/String;)Landroid/hardware/Camera$Parameters;

    return-void
.end method

.method public declared-synchronized clearListPreference()V
    .locals 4

    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "SettingChecker"

    const-string v3, "clearListPreference()"

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mListPrefs:[Lcom/android/camera/ListPreference;

    array-length v1, v2

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mListPrefs:[Lcom/android/camera/ListPreference;

    const/4 v3, 0x0

    aput-object v3, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public clearOverrideValues()V
    .locals 4

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mOverrideSettingValues:[Ljava/lang/String;

    array-length v1, v2

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mOverrideSettingValues:[Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public disableContinuousShot()V
    .locals 3

    const/16 v2, 0x2a

    sget-object v0, Lcom/android/camera/SettingChecker;->RESET_STATE_VALUE:[[Ljava/lang/String;

    aget-object v0, v0, v2

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {p0, v2, v0}, Lcom/android/camera/SettingChecker;->applyValueToParameters(ILjava/lang/String;)V

    const/16 v0, 0x29

    const-string v1, "normal"

    invoke-virtual {p0, v0, v1}, Lcom/android/camera/SettingChecker;->applyValueToParameters(ILjava/lang/String;)V

    return-void
.end method

.method public enableContinuousShot()V
    .locals 2

    const/16 v0, 0x2a

    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/android/camera/SettingChecker;->getPreferenceValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/camera/SettingChecker;->applyValueToParameters(ILjava/lang/String;)V

    const/16 v0, 0x29

    const-string v1, "continuousshot"

    invoke-virtual {p0, v0, v1}, Lcom/android/camera/SettingChecker;->applyValueToParameters(ILjava/lang/String;)V

    return-void
.end method

.method public getDefaultValue(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-static {v1, p1}, Lcom/android/camera/SettingChecker;->getDefaultValueFromXml(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "SettingChecker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDefaultValue("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0
.end method

.method public declared-synchronized getListPreference(I)Lcom/android/camera/ListPreference;
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mListPrefs:[Lcom/android/camera/ListPreference;

    aget-object v0, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getOverrideSettingValue(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    sget-boolean v0, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getOverrideSettingValue("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mOverrideSettingValues:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mOverrideSettingValues:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOverrideValues(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mOverrideSettingValues:[Ljava/lang/String;

    array-length v1, v1

    if-gt p1, v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mOverrideSettingValues:[Ljava/lang/String;

    aget-object v0, v1, p1

    :cond_0
    sget-boolean v1, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "SettingChecker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getOverrideValues("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v0
.end method

.method public getParameterValue(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/camera/SettingChecker;->getParameterValue(Landroid/hardware/Camera$Parameters;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreferenceValue(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    const/16 v1, 0x16

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getVoiceManager()Lcom/android/camera/VoiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/VoiceManager;->getVoiceValue()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-boolean v1, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "SettingChecker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPreferenceValue("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v2

    invoke-static {v1, v2, p1}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSettingCurrentValue(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const/4 v1, 0x0

    const/16 v2, 0x16

    if-ne p1, v2, :cond_2

    sget-object v2, Lcom/android/camera/SettingChecker;->MATRIX_MODE_STATE:[[I

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v3

    invoke-static {v2, v3, p1}, Lcom/android/camera/SettingChecker;->getMatrixValue([[III)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getVoiceManager()Lcom/android/camera/VoiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/VoiceManager;->getVoiceValue()Ljava/lang/String;

    move-result-object v1

    :cond_0
    :goto_0
    sget-boolean v2, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v2, :cond_1

    const-string v2, "SettingChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSettingCurrentValue("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v1

    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/camera/SettingChecker;->getListPreference(I)Lcom/android/camera/ListPreference;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-static {v2, p1}, Lcom/android/camera/SettingChecker;->getDefaultValueFromXml(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    :cond_3
    :goto_1
    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v3

    invoke-static {v2, v3, p1}, Lcom/android/camera/SettingChecker;->getPreferenceValue(Landroid/content/Context;Lcom/android/camera/ComboPreferences;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/android/camera/ListPreference;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lcom/android/camera/ListPreference;->getOverrideValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public isParametersSupportedValue(ILjava/lang/String;)Z
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/android/camera/SettingChecker;->isParametersSupportedValue(Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public resetSettings()V
    .locals 14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v10, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v10}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v3

    iget-object v10, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v10}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-direct {p0, v5}, Lcom/android/camera/SettingChecker;->resetSettings(Lcom/android/camera/ComboPreferences;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v10}, Lcom/android/camera/Camera;->getCameraCount()I

    move-result v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_1

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_3

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v10, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v5, v10, v0}, Lcom/android/camera/ComboPreferences;->setLocalId(Landroid/content/Context;I)V

    invoke-direct {p0, v5}, Lcom/android/camera/SettingChecker;->resetSettings(Lcom/android/camera/ComboPreferences;)V

    sget-boolean v10, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v10, :cond_2

    const-string v10, "SettingChecker"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "resetSettings() reset cameraId="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sget-boolean v10, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v10, :cond_4

    const-string v10, "SettingChecker"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "resetSettings() consume:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sub-long v12, v8, v6

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void
.end method

.method public declared-synchronized setListPreference(ILcom/android/camera/ListPreference;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/camera/ListPreference;

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setListPreference("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mListPrefs:[Lcom/android/camera/ListPreference;

    aput-object p2, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setOverrideValues(ILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOverrideValues("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mOverrideSettingValues:[Ljava/lang/String;

    array-length v0, v0

    if-gt p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/SettingChecker;->mOverrideSettingValues:[Ljava/lang/String;

    aput-object p2, v0, p1

    :cond_1
    return-void
.end method

.method public turnOffWhenHide()V
    .locals 6

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    sget-boolean v2, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "SettingChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "turnOffWhenHide() mContext.getParameters()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v2, "off"

    invoke-virtual {p0, v5, v2}, Lcom/android/camera/SettingChecker;->setOverrideValues(ILjava/lang/String;)V

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "infinity"

    invoke-virtual {v0, v2}, Lcom/android/camera/FocusManager;->overrideFocusMode(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getFocusMode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    const-string v3, "off"

    invoke-static {v2, v1, v5, v3}, Lcom/android/camera/SettingChecker;->setParameterValue(Landroid/content/Context;Landroid/hardware/Camera$Parameters;ILjava/lang/String;)Z

    iget-object v2, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->applyParametersToServer()V

    :cond_2
    return-void
.end method

.method public turnOnWhenShown()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getPreferences()Lcom/android/camera/ComboPreferences;

    move-result-object v2

    sget-boolean v3, Lcom/android/camera/SettingChecker;->LOG:Z

    if-eqz v3, :cond_0

    const-string v3, "SettingChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "turnOnWhenShown() mContext.getParameters()="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mContext.getPreferences()="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0, v6, v7}, Lcom/android/camera/SettingChecker;->setOverrideValues(ILjava/lang/String;)V

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v7}, Lcom/android/camera/FocusManager;->overrideFocusMode(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getFocusMode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    :cond_1
    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    iget-object v4, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/camera/SettingChecker;->getVirtualMode(I)I

    move-result v4

    invoke-static {v3, v2, v1, v4, v6}, Lcom/android/camera/SettingChecker;->applyPreferenceToParameters(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;II)Landroid/hardware/Camera$Parameters;

    iget-object v3, p0, Lcom/android/camera/SettingChecker;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->applyParametersToServer()V

    invoke-virtual {p0}, Lcom/android/camera/SettingChecker;->applyParametersToUIImmediately()V

    :cond_2
    return-void
.end method
