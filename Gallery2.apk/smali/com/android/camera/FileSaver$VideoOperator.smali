.class Lcom/android/camera/FileSaver$VideoOperator;
.super Lcom/android/camera/FileSaver$RequestOperator;
.source "FileSaver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/FileSaver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VideoOperator"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/FileSaver;


# direct methods
.method private constructor <init>(Lcom/android/camera/FileSaver;ILjava/lang/String;)V
    .locals 1
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/camera/FileSaver$VideoOperator;->this$0:Lcom/android/camera/FileSaver;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/camera/FileSaver$RequestOperator;-><init>(Lcom/android/camera/FileSaver;Lcom/android/camera/FileSaver$1;)V

    iput p2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTempOutputFileFormat:I

    iput-object p3, p0, Lcom/android/camera/FileSaver$RequestOperator;->mResolution:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/camera/FileSaver;ILjava/lang/String;Lcom/android/camera/FileSaver$1;)V
    .locals 0
    .param p1    # Lcom/android/camera/FileSaver;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/android/camera/FileSaver$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/camera/FileSaver$VideoOperator;-><init>(Lcom/android/camera/FileSaver;ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addRequest()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/FileSaver$VideoOperator;->this$0:Lcom/android/camera/FileSaver;

    invoke-static {v0, p0}, Lcom/android/camera/FileSaver;->access$700(Lcom/android/camera/FileSaver;Lcom/android/camera/SaveRequest;)V

    return-void
.end method

.method public createThumbnail(I)Lcom/android/camera/Thumbnail;
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/android/camera/Thumbnail;->createVideoThumbnailBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mUri:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/android/camera/Thumbnail;->createThumbnail(Landroid/net/Uri;Landroid/graphics/Bitmap;I)Lcom/android/camera/Thumbnail;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public prepareRequest()V
    .locals 4

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFileType:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDateTaken:J

    iget-object v0, p0, Lcom/android/camera/FileSaver$VideoOperator;->this$0:Lcom/android/camera/FileSaver;

    iget v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFileType:I

    iget-wide v2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDateTaken:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/camera/FileSaver;->access$800(Lcom/android/camera/FileSaver;IJ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTitle:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/FileSaver$VideoOperator;->this$0:Lcom/android/camera/FileSaver;

    iget v2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTempOutputFileFormat:I

    invoke-static {v1, v2}, Lcom/android/camera/FileSaver;->access$900(Lcom/android/camera/FileSaver;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDisplayName:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/FileSaver$VideoOperator;->this$0:Lcom/android/camera/FileSaver;

    iget v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTempOutputFileFormat:I

    invoke-static {v0, v1}, Lcom/android/camera/FileSaver;->access$1000(Lcom/android/camera/FileSaver;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/FileSaver$RequestOperator;->mMimeType:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDisplayName:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/camera/Storage;->generateFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    return-void
.end method

.method public saveRequest()V
    .locals 8

    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTempFilePath:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDataSize:J

    new-instance v3, Landroid/content/ContentValues;

    const/16 v4, 0xb

    invoke-direct {v3, v4}, Landroid/content/ContentValues;-><init>(I)V

    const-string v4, "title"

    iget-object v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTitle:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "_display_name"

    iget-object v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "datetaken"

    iget-wide v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDateTaken:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "mime_type"

    iget-object v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mMimeType:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "_data"

    iget-object v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "_size"

    iget-wide v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDataSize:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "stereo_type"

    iget v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mStereoType:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v4, p0, Lcom/android/camera/FileSaver$RequestOperator;->mLocation:Landroid/location/Location;

    if-eqz v4, :cond_0

    const-string v4, "latitude"

    iget-object v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mLocation:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v4, "longitude"

    iget-object v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mLocation:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    :cond_0
    const-string v4, "resolution"

    iget-object v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mResolution:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "duration"

    iget-wide v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDuration:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v4, p0, Lcom/android/camera/FileSaver$VideoOperator;->this$0:Lcom/android/camera/FileSaver;

    invoke-static {v4}, Lcom/android/camera/FileSaver;->access$400(Lcom/android/camera/FileSaver;)Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/android/camera/FileSaver$RequestOperator;->mUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/camera/FileSaver$VideoOperator;->this$0:Lcom/android/camera/FileSaver;

    invoke-static {v4}, Lcom/android/camera/FileSaver;->access$500(Lcom/android/camera/FileSaver;)Lcom/android/camera/Camera;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.hardware.action.NEW_VIDEO"

    iget-object v7, p0, Lcom/android/camera/FileSaver$RequestOperator;->mUri:Landroid/net/Uri;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v4, v5}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    const-string v4, "FileSaver"

    const-string v5, "Failed to write MediaStore"

    invoke-static {v4, v5, v2}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
