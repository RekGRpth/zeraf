.class public abstract Lcom/android/camera/actor/CameraActor;
.super Ljava/lang/Object;
.source "CameraActor.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CameraActor"


# instance fields
.field protected final mContext:Lcom/android/camera/Camera;

.field protected mFocusManager:Lcom/android/camera/FocusManager;


# direct methods
.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 0
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/camera/actor/CameraActor;->mContext:Lcom/android/camera/Camera;

    return-void
.end method


# virtual methods
.method public getASDCallback()Landroid/hardware/Camera$ASDCallback;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getAUTORAMACallback()Landroid/hardware/Camera$AUTORAMACallback;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getAUTORAMAMVCallback()Landroid/hardware/Camera$AUTORAMAMVCallback;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getAutoFocusMoveCallback()Landroid/hardware/Camera$AutoFocusMoveCallback;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCancelListener()Landroid/view/View$OnClickListener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getContext()Lcom/android/camera/Camera;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/CameraActor;->mContext:Lcom/android/camera/Camera;

    return-object v0
.end method

.method public getContinuousShotDone()Landroid/hardware/Camera$ContinuousShotDone;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getErrorCallback()Landroid/hardware/Camera$ErrorCallback;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getFaceDetectionListener()Landroid/hardware/Camera$FaceDetectionListener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getFocusManagerListener()Lcom/android/camera/FocusManager$Listener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getMAVCallback()Landroid/hardware/Camera$MAVCallback;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getMode()I
.end method

.method public getOkListener()Landroid/view/View$OnClickListener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getOnZoomChangeListener()Landroid/hardware/Camera$OnZoomChangeListener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPhotoShutterButtonListener()Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayListener()Landroid/view/View$OnClickListener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPreviewCallback()Landroid/hardware/Camera$PreviewCallback;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getRetakeListener()Landroid/view/View$OnClickListener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSmileCallback()Landroid/hardware/Camera$SmileCallback;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getVideoShutterButtonListener()Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getonSingleTapUpListener()Lcom/android/camera/Camera$OnSingleTapUpListener;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public handleFocus()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected isFromInternal()Z
    .locals 5

    iget-object v2, p0, Lcom/android/camera/actor/CameraActor;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "CameraActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Check action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "android.media.action.STILL_IMAGE_CAMERA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    return v2
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCameraClose()V
    .locals 0

    return-void
.end method

.method public onCameraDisabled()V
    .locals 0

    return-void
.end method

.method public onCameraOpenDone()V
    .locals 0

    return-void
.end method

.method public onCameraOpenFailed()V
    .locals 0

    return-void
.end method

.method public onCameraParameterReady(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public onDisplayRotate()V
    .locals 0

    return-void
.end method

.method public onEffectsDeactive()V
    .locals 0

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onMediaEject()V
    .locals 0

    return-void
.end method

.method public onOrientationChanged(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onRestoreSettings()V
    .locals 0

    return-void
.end method

.method public onUserInteraction()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public release()V
    .locals 0

    return-void
.end method

.method public stopPreview()V
    .locals 0

    return-void
.end method
