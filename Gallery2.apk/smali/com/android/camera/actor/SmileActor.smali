.class public Lcom/android/camera/actor/SmileActor;
.super Lcom/android/camera/actor/PhotoActor;
.source "SmileActor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/actor/SmileActor$SmileCameraCategory;,
        Lcom/android/camera/actor/SmileActor$ActorSmileCallback;
    }
.end annotation


# static fields
.field private static final LOG:Z

.field private static final SAVE_ORIGINAL_PICTURE:Z = true

.field private static final SMILESHOT_INTERVAL:I = 0x1

.field private static final SMILESHOT_IN_PROGRESS:I = 0x2

.field private static final SMILESHOT_STANDBY:I = 0x0

.field private static final SMILE_SHOT_INTERVAL:I = 0x5dc

.field private static final TAG:Ljava/lang/String; = "SmileActor"


# instance fields
.field private mDoSmileSnapRunnable:Ljava/lang/Runnable;

.field private mFullScreenChangedListener:Lcom/android/camera/Camera$OnFullScreenChangedListener;

.field private mOriginalSaveRequest:Lcom/android/camera/SaveRequest;

.field private final mSmileCallback:Lcom/android/camera/actor/SmileActor$ActorSmileCallback;

.field private mStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/actor/SmileActor;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0, p1}, Lcom/android/camera/actor/PhotoActor;-><init>(Lcom/android/camera/Camera;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    new-instance v0, Lcom/android/camera/actor/SmileActor$ActorSmileCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/actor/SmileActor$ActorSmileCallback;-><init>(Lcom/android/camera/actor/SmileActor;Lcom/android/camera/actor/SmileActor$1;)V

    iput-object v0, p0, Lcom/android/camera/actor/SmileActor;->mSmileCallback:Lcom/android/camera/actor/SmileActor$ActorSmileCallback;

    new-instance v0, Lcom/android/camera/actor/SmileActor$1;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/SmileActor$1;-><init>(Lcom/android/camera/actor/SmileActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/SmileActor;->mDoSmileSnapRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/camera/actor/SmileActor$2;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/SmileActor$2;-><init>(Lcom/android/camera/actor/SmileActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/SmileActor;->mFullScreenChangedListener:Lcom/android/camera/Camera$OnFullScreenChangedListener;

    sget-boolean v0, Lcom/android/camera/actor/SmileActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SmileActor"

    const-string v1, "SmileActor initialize"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/camera/actor/SmileActor$SmileCameraCategory;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/SmileActor$SmileCameraCategory;-><init>(Lcom/android/camera/actor/SmileActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    return-void
.end method

.method static synthetic access$100()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/actor/SmileActor;->LOG:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/camera/actor/SmileActor;)I
    .locals 1
    .param p0    # Lcom/android/camera/actor/SmileActor;

    iget v0, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    return v0
.end method

.method static synthetic access$202(Lcom/android/camera/actor/SmileActor;I)I
    .locals 0
    .param p0    # Lcom/android/camera/actor/SmileActor;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    return p1
.end method

.method static synthetic access$300(Lcom/android/camera/actor/SmileActor;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/camera/actor/SmileActor;

    iget-object v0, p0, Lcom/android/camera/actor/SmileActor;->mDoSmileSnapRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/camera/actor/SmileActor;)Lcom/android/camera/Camera$OnFullScreenChangedListener;
    .locals 1
    .param p0    # Lcom/android/camera/actor/SmileActor;

    iget-object v0, p0, Lcom/android/camera/actor/SmileActor;->mFullScreenChangedListener:Lcom/android/camera/Camera$OnFullScreenChangedListener;

    return-object v0
.end method

.method private openSmileShutterMode()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/actor/SmileActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SmileActor"

    const-string v1, "openSmileShutterMode "

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "SmileActor"

    const-string v1, "CameraDevice is null, ignore"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    iget-object v0, p0, Lcom/android/camera/actor/SmileActor;->mSmileCallback:Lcom/android/camera/actor/SmileActor$ActorSmileCallback;

    invoke-virtual {p0, v0}, Lcom/android/camera/actor/SmileActor;->startSmileDetection(Landroid/hardware/Camera$SmileCallback;)V

    goto :goto_0
.end method


# virtual methods
.method public doSmileShutter()Z
    .locals 3

    sget-boolean v0, Lcom/android/camera/actor/SmileActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SmileActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doSmileShutter mStatus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->capture()Z

    invoke-virtual {p0}, Lcom/android/camera/actor/SmileActor;->stopSmileDetection()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ensureFDState(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    sget-boolean v1, Lcom/android/camera/actor/SmileActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "SmileActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ensureFDState enable="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "CameraState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v1

    if-eq v1, v4, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->startFaceDetection()V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v2

    aget-object v0, v1, v2

    iget v1, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v1, v4, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->stopFaceDetection()V

    goto :goto_0
.end method

.method public getMode()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public getPhotoShutterButtonListener()Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    .locals 0

    return-object p0
.end method

.method public handleSDcardUnmount()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/actor/SmileActor;->stopSmileDetection()V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V

    goto :goto_0
.end method

.method public isInShutterProgress()Z
    .locals 2

    iget v0, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCameraParameterReady(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/actor/PhotoActor;->onCameraParameterReady(Z)V

    sget-boolean v0, Lcom/android/camera/actor/SmileActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SmileActor"

    const-string v1, "SmileActor onCameraParameterReady"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/camera/actor/SmileActor;->ensureFDState(Z)V

    iget v0, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/camera/actor/SmileActor;->mDoSmileSnapRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/camera/actor/SmileActor;->mDoSmileSnapRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public onShutterButtonLongPressed(Lcom/android/camera/ui/ShutterButton;)V
    .locals 4
    .param p1    # Lcom/android/camera/ui/ShutterButton;

    sget-boolean v0, Lcom/android/camera/actor/SmileActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SmileActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Smile.onShutterButtonLongPressed("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const v3, 0x7f0c0028

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const v3, 0x7f0c000d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->showInfo(Ljava/lang/String;)V

    return-void
.end method

.method public readyToCapture()Z
    .locals 3

    sget-boolean v0, Lcom/android/camera/actor/SmileActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SmileActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " readyToCapture? mStatus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/camera/actor/SmileActor;->openSmileShutterMode()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public release()V
    .locals 2

    invoke-super {p0}, Lcom/android/camera/actor/PhotoActor;->release()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/actor/SmileActor;->mFullScreenChangedListener:Lcom/android/camera/Camera$OnFullScreenChangedListener;

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->removeOnFullScreenChangedListener(Lcom/android/camera/Camera$OnFullScreenChangedListener;)Z

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->doCancelCapture()Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/camera/actor/SmileActor;->ensureFDState(Z)V

    return-void
.end method

.method public startSmileDetection(Landroid/hardware/Camera$SmileCallback;)V
    .locals 1
    .param p1    # Landroid/hardware/Camera$SmileCallback;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/camera/CameraManager$CameraProxy;->setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->startSDPreview()V

    return-void
.end method

.method public stopSmileDetection()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->cancelSDPreview()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/actor/SmileActor;->mStatus:I

    return-void
.end method
