.class Lcom/android/camera/actor/EvActor$1;
.super Ljava/lang/Object;
.source "EvActor.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/EvActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/EvActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/EvActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/EvActor$1;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 3
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    const-string v0, "EvActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EV picture taken mCameraClosed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/EvActor$1;->this$0:Lcom/android/camera/actor/EvActor;

    iget-boolean v2, v2, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCurrentEVNum="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/EvActor$1;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v2}, Lcom/android/camera/actor/EvActor;->access$000(Lcom/android/camera/actor/EvActor;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$1;->this$0:Lcom/android/camera/actor/EvActor;

    iget-boolean v0, v0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/EvActor$1;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v0}, Lcom/android/camera/actor/EvActor;->access$008(Lcom/android/camera/actor/EvActor;)I

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$1;->this$0:Lcom/android/camera/actor/EvActor;

    iget-object v1, p0, Lcom/android/camera/actor/EvActor$1;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v1}, Lcom/android/camera/actor/EvActor;->access$000(Lcom/android/camera/actor/EvActor;)I

    move-result v1

    invoke-static {v0, p1, v1}, Lcom/android/camera/actor/EvActor;->access$100(Lcom/android/camera/actor/EvActor;[BI)V

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/android/camera/actor/EvActor$1;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v1}, Lcom/android/camera/actor/EvActor;->access$000(Lcom/android/camera/actor/EvActor;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$1;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v0}, Lcom/android/camera/actor/EvActor;->access$200(Lcom/android/camera/actor/EvActor;)V

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$1;->this$0:Lcom/android/camera/actor/EvActor;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/camera/actor/EvActor;->access$002(Lcom/android/camera/actor/EvActor;I)I

    goto :goto_0
.end method
