.class final Lcom/android/camera/actor/SmileActor$ActorSmileCallback;
.super Ljava/lang/Object;
.source "SmileActor.java"

# interfaces
.implements Landroid/hardware/Camera$SmileCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/SmileActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ActorSmileCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/SmileActor;


# direct methods
.method private constructor <init>(Lcom/android/camera/actor/SmileActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/SmileActor$ActorSmileCallback;->this$0:Lcom/android/camera/actor/SmileActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/camera/actor/SmileActor;Lcom/android/camera/actor/SmileActor$1;)V
    .locals 0
    .param p1    # Lcom/android/camera/actor/SmileActor;
    .param p2    # Lcom/android/camera/actor/SmileActor$1;

    invoke-direct {p0, p1}, Lcom/android/camera/actor/SmileActor$ActorSmileCallback;-><init>(Lcom/android/camera/actor/SmileActor;)V

    return-void
.end method


# virtual methods
.method public onSmile()V
    .locals 3

    iget-object v0, p0, Lcom/android/camera/actor/SmileActor$ActorSmileCallback;->this$0:Lcom/android/camera/actor/SmileActor;

    invoke-static {v0}, Lcom/android/camera/actor/SmileActor;->access$200(Lcom/android/camera/actor/SmileActor;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/android/camera/actor/SmileActor;->access$100()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SmileActor"

    const-string v1, "Smile callback in error state, please check"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/android/camera/actor/SmileActor;->access$100()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "SmileActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "smile detected, mstat:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/SmileActor$ActorSmileCallback;->this$0:Lcom/android/camera/actor/SmileActor;

    invoke-static {v2}, Lcom/android/camera/actor/SmileActor;->access$200(Lcom/android/camera/actor/SmileActor;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/android/camera/actor/SmileActor$ActorSmileCallback;->this$0:Lcom/android/camera/actor/SmileActor;

    iget-boolean v0, v0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/SmileActor$ActorSmileCallback;->this$0:Lcom/android/camera/actor/SmileActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor;->capture()Z

    iget-object v0, p0, Lcom/android/camera/actor/SmileActor$ActorSmileCallback;->this$0:Lcom/android/camera/actor/SmileActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/SmileActor;->stopSmileDetection()V

    goto :goto_0
.end method
