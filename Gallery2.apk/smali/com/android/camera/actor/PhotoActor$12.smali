.class Lcom/android/camera/actor/PhotoActor$12;
.super Ljava/lang/Object;
.source "PhotoActor.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/PhotoActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/PhotoActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/PhotoActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 6
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-static {}, Lcom/android/camera/actor/PhotoActor;->access$400()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    const-string v1, "PhotoActor.ContinuousShot.onPictureTaken"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileStorePicture()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$800(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez p1, :cond_4

    invoke-static {}, Lcom/android/camera/actor/PhotoActor;->access$400()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPictureTaken("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") stop shot!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-virtual {v0, v4, v3}, Lcom/android/camera/actor/PhotoActor;->onShutterButtonFocus(Lcom/android/camera/ui/ShutterButton;Z)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0, v5}, Lcom/android/camera/actor/PhotoActor;->access$1502(Lcom/android/camera/actor/PhotoActor;Z)Z

    :cond_2
    :goto_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileStorePicture()V

    invoke-static {}, Lcom/android/camera/actor/PhotoActor;->access$400()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Continuous Shot, onPictureTaken: mCurrentShotsNum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v2}, Lcom/android/camera/actor/PhotoActor;->access$1700(Lcom/android/camera/actor/PhotoActor;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mContinuousShotPerformed = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v2}, Lcom/android/camera/actor/PhotoActor;->access$800(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Continuous Shot, speed = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v2}, Lcom/android/camera/actor/PhotoActor;->access$1800(Lcom/android/camera/actor/PhotoActor;)Lcom/android/camera/actor/PhotoActor$MemoryManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/actor/PhotoActor$MemoryManager;->getSuitableContinuousShotSpeed()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$800(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-boolean v0, v0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->canshot()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-virtual {v0, v4, v3}, Lcom/android/camera/actor/PhotoActor;->onShutterButtonFocus(Lcom/android/camera/ui/ShutterButton;Z)V

    :cond_5
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0, p1}, Lcom/android/camera/actor/PhotoActor;->access$1600(Lcom/android/camera/actor/PhotoActor;[B)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v1, v1, Lcom/android/camera/actor/CameraActor;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getFileSaver()Lcom/android/camera/FileSaver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v2, v2, Lcom/android/camera/actor/PhotoActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-virtual {v1, v2}, Lcom/android/camera/FileSaver;->copyPhotoRequest(Lcom/android/camera/SaveRequest;)Lcom/android/camera/SaveRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/android/camera/actor/PhotoActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$1708(Lcom/android/camera/actor/PhotoActor;)I

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$1800(Lcom/android/camera/actor/PhotoActor;)Lcom/android/camera/actor/PhotoActor$MemoryManager;

    move-result-object v0

    array-length v1, p1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/android/camera/actor/PhotoActor$MemoryManager;->refresh(J)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$1700(Lcom/android/camera/actor/PhotoActor;)I

    move-result v0

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v1}, Lcom/android/camera/actor/PhotoActor;->access$1900(Lcom/android/camera/actor/PhotoActor;)I

    move-result v1

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$1800(Lcom/android/camera/actor/PhotoActor;)Lcom/android/camera/actor/PhotoActor$MemoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$MemoryManager;->isNeedStopCapture()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-virtual {v0, v4, v3}, Lcom/android/camera/actor/PhotoActor;->onShutterButtonFocus(Lcom/android/camera/ui/ShutterButton;Z)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0, v5}, Lcom/android/camera/actor/PhotoActor;->access$1502(Lcom/android/camera/actor/PhotoActor;Z)Z

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$1800(Lcom/android/camera/actor/PhotoActor;)Lcom/android/camera/actor/PhotoActor$MemoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v1, v1, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getFileSaver()Lcom/android/camera/FileSaver;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/FileSaver;->getWaitingDataSize()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/android/camera/actor/PhotoActor$MemoryManager;->isNeedSlowDown(J)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v1}, Lcom/android/camera/actor/PhotoActor;->access$1800(Lcom/android/camera/actor/PhotoActor;)Lcom/android/camera/actor/PhotoActor$MemoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/actor/PhotoActor$MemoryManager;->getSuitableContinuousShotSpeed()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setContinuousShotSpeed(I)V

    goto/16 :goto_0

    :cond_8
    invoke-static {}, Lcom/android/camera/actor/PhotoActor;->access$400()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "received onPictureTaken, but mCameraClosed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-boolean v2, v2, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " or mContinuousShotPerformed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor$12;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v2}, Lcom/android/camera/actor/PhotoActor;->access$800(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ignore it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
