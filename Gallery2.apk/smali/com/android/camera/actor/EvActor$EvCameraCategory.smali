.class Lcom/android/camera/actor/EvActor$EvCameraCategory;
.super Lcom/android/camera/actor/PhotoActor$CameraCategory;
.source "EvActor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/EvActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EvCameraCategory"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/EvActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/EvActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-direct {p0, p1}, Lcom/android/camera/actor/PhotoActor$CameraCategory;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    return-void
.end method


# virtual methods
.method public animateCapture(Lcom/android/camera/Camera;)V
    .locals 0
    .param p1    # Lcom/android/camera/Camera;

    return-void
.end method

.method public applySpecialCapture()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public canshot()Z
    .locals 4

    const-wide/16 v0, 0x3

    invoke-static {}, Lcom/android/camera/Storage;->getLeftSpace()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public doOnPictureTaken()V
    .locals 0

    return-void
.end method

.method public ensureCaptureTempPath()V
    .locals 4

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/android/camera/SaveRequest;

    invoke-static {v0, v1}, Lcom/android/camera/actor/EvActor;->access$502(Lcom/android/camera/actor/EvActor;[Lcom/android/camera/SaveRequest;)[Lcom/android/camera/SaveRequest;

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    iget-object v1, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    iget-object v1, v1, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/camera/Camera;->preparePhotoRequest(II)Lcom/android/camera/SaveRequest;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/actor/EvActor;->access$1002(Lcom/android/camera/actor/EvActor;Lcom/android/camera/SaveRequest;)Lcom/android/camera/SaveRequest;

    return-void
.end method

.method public getJpegPictureCallback()Landroid/hardware/Camera$PictureCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v0}, Lcom/android/camera/actor/EvActor;->access$1100(Lcom/android/camera/actor/EvActor;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v0

    return-object v0
.end method

.method public initializeFirstTime()V
    .locals 4

    iget-object v1, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    new-instance v2, Lcom/android/camera/manager/PickImageViewManager;

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    iget-object v3, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v2, v3, v0}, Lcom/android/camera/manager/PickImageViewManager;-><init>(Lcom/android/camera/Camera;I)V

    invoke-static {v1, v2}, Lcom/android/camera/actor/EvActor;->access$702(Lcom/android/camera/actor/EvActor;Lcom/android/camera/manager/PickImageViewManager;)Lcom/android/camera/manager/PickImageViewManager;

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v0}, Lcom/android/camera/actor/EvActor;->access$800(Lcom/android/camera/actor/EvActor;)V

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v0}, Lcom/android/camera/actor/EvActor;->access$700(Lcom/android/camera/actor/EvActor;)Lcom/android/camera/manager/PickImageViewManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v1}, Lcom/android/camera/actor/EvActor;->access$900(Lcom/android/camera/actor/EvActor;)Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/PickImageViewManager;->setSelectedChangedListener(Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLeaveActor()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->restoreViewState()V

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v0}, Lcom/android/camera/actor/EvActor;->access$000(Lcom/android/camera/actor/EvActor;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/camera/actor/EvActor;->access$002(Lcom/android/camera/actor/EvActor;I)I

    new-instance v0, Lcom/android/camera/actor/EvActor$EvCameraCategory$1;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/EvActor$EvCameraCategory$1;-><init>(Lcom/android/camera/actor/EvActor$EvCameraCategory;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method public supportContinuousShot()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public switchShutterButton()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v0}, Lcom/android/camera/actor/EvActor;->access$500(Lcom/android/camera/actor/EvActor;)[Lcom/android/camera/SaveRequest;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$EvCameraCategory;->this$0:Lcom/android/camera/actor/EvActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->switchShutter(I)V

    :cond_0
    return-void
.end method
