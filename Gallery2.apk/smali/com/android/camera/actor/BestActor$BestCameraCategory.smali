.class Lcom/android/camera/actor/BestActor$BestCameraCategory;
.super Lcom/android/camera/actor/PhotoActor$CameraCategory;
.source "BestActor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/BestActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BestCameraCategory"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/BestActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/BestActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/BestActor$BestCameraCategory;->this$0:Lcom/android/camera/actor/BestActor;

    invoke-direct {p0, p1}, Lcom/android/camera/actor/PhotoActor$CameraCategory;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    return-void
.end method


# virtual methods
.method public animateCapture(Lcom/android/camera/Camera;)V
    .locals 0
    .param p1    # Lcom/android/camera/Camera;

    return-void
.end method

.method public applySpecialCapture()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public doOnPictureTaken()V
    .locals 2

    invoke-static {}, Lcom/android/camera/actor/BestActor;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BestActor"

    const-string v1, "BestActor.doOnPictureTaken"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/BestActor$BestCameraCategory;->this$0:Lcom/android/camera/actor/BestActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-super {p0, v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->animateCapture(Lcom/android/camera/Camera;)V

    return-void
.end method

.method public initializeFirstTime()V
    .locals 0

    return-void
.end method

.method public supportContinuousShot()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
