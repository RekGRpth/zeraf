.class public Lcom/android/camera/WfdManagerLocal;
.super Ljava/lang/Object;
.source "WfdManagerLocal.java"

# interfaces
.implements Lcom/android/camera/Camera$Resumable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/WfdManagerLocal$Listener;
    }
.end annotation


# static fields
.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String; = "WfdManagerLocal"


# instance fields
.field private mContext:Lcom/android/camera/Camera;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/camera/WfdManagerLocal$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mResumed:Z

.field private mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/WfdManagerLocal;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/camera/WfdManagerLocal$1;

    invoke-direct {v0, p0}, Lcom/android/camera/WfdManagerLocal$1;-><init>(Lcom/android/camera/WfdManagerLocal;)V

    iput-object v0, p0, Lcom/android/camera/WfdManagerLocal;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/WfdManagerLocal;->mListeners:Ljava/util/List;

    iput-object p1, p0, Lcom/android/camera/WfdManagerLocal;->mContext:Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/WfdManagerLocal;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, p0}, Lcom/android/camera/Camera;->addResumable(Lcom/android/camera/Camera$Resumable;)Z

    iget-object v0, p0, Lcom/android/camera/WfdManagerLocal;->mContext:Lcom/android/camera/Camera;

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/android/camera/WfdManagerLocal;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/WfdManagerLocal;->LOG:Z

    return v0
.end method

.method static synthetic access$102(Lcom/android/camera/WfdManagerLocal;Landroid/hardware/display/WifiDisplayStatus;)Landroid/hardware/display/WifiDisplayStatus;
    .locals 0
    .param p0    # Lcom/android/camera/WfdManagerLocal;
    .param p1    # Landroid/hardware/display/WifiDisplayStatus;

    iput-object p1, p0, Lcom/android/camera/WfdManagerLocal;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/camera/WfdManagerLocal;Z)V
    .locals 0
    .param p0    # Lcom/android/camera/WfdManagerLocal;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/camera/WfdManagerLocal;->notifyWfdStateChanged(Z)V

    return-void
.end method

.method private notifyWfdStateChanged(Z)V
    .locals 5
    .param p1    # Z

    sget-boolean v2, Lcom/android/camera/WfdManagerLocal;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "WfdManagerLocal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notifyWfdStateChanged("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/android/camera/WfdManagerLocal;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/WfdManagerLocal$Listener;

    if-eqz v1, :cond_1

    invoke-interface {v1, p1}, Lcom/android/camera/WfdManagerLocal$Listener;->onStateChanged(Z)V

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public addListener(Lcom/android/camera/WfdManagerLocal$Listener;)Z
    .locals 1
    .param p1    # Lcom/android/camera/WfdManagerLocal$Listener;

    iget-object v0, p0, Lcom/android/camera/WfdManagerLocal;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/WfdManagerLocal;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public begin()V
    .locals 0

    return-void
.end method

.method public finish()V
    .locals 0

    return-void
.end method

.method public declared-synchronized isWfdEnabled()Z
    .locals 5

    monitor-enter p0

    const/4 v1, 0x0

    const/4 v0, -0x1

    :try_start_0
    iget-object v2, p0, Lcom/android/camera/WfdManagerLocal;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/WfdManagerLocal;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v2}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/WfdManagerLocal;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    :cond_0
    iget-object v2, p0, Lcom/android/camera/WfdManagerLocal;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v2}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    sget-boolean v2, Lcom/android/camera/WfdManagerLocal;->LOG:Z

    if-eqz v2, :cond_1

    const-string v2, "WfdManagerLocal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isWfdEnabled() mWifiDisplayStatus="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/WfdManagerLocal;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public pause()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/WfdManagerLocal;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "WfdManagerLocal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause() mResumed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/WfdManagerLocal;->mResumed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/WfdManagerLocal;->mResumed:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/WfdManagerLocal;->mContext:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/WfdManagerLocal;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/WfdManagerLocal;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/WfdManagerLocal;->mResumed:Z

    :cond_1
    return-void
.end method

.method public removeListener(Lcom/android/camera/WfdManagerLocal$Listener;)Z
    .locals 1
    .param p1    # Lcom/android/camera/WfdManagerLocal$Listener;

    iget-object v0, p0, Lcom/android/camera/WfdManagerLocal;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public resume()V
    .locals 4

    sget-boolean v1, Lcom/android/camera/WfdManagerLocal;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "WfdManagerLocal"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resume() mResumed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/WfdManagerLocal;->mResumed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v1, p0, Lcom/android/camera/WfdManagerLocal;->mResumed:Z

    if-nez v1, :cond_1

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/camera/WfdManagerLocal;->mContext:Lcom/android/camera/Camera;

    iget-object v2, p0, Lcom/android/camera/WfdManagerLocal;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/WfdManagerLocal;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {p0}, Lcom/android/camera/WfdManagerLocal;->isWfdEnabled()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/camera/WfdManagerLocal;->notifyWfdStateChanged(Z)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/camera/WfdManagerLocal;->mResumed:Z

    :cond_1
    return-void
.end method
