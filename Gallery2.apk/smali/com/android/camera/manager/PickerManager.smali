.class public Lcom/android/camera/manager/PickerManager;
.super Lcom/android/camera/manager/ViewManager;
.source "PickerManager.java"

# interfaces
.implements Lcom/android/camera/Camera$OnParametersReadyListener;
.implements Lcom/android/camera/Camera$OnPreferenceReadyListener;
.implements Lcom/android/camera/ui/PickerButton$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/manager/PickerManager$PickerListener;
    }
.end annotation


# static fields
.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String; = "PickerManager"


# instance fields
.field private mCameraPicker:Lcom/android/camera/ui/PickerButton;

.field private mFlashPicker:Lcom/android/camera/ui/PickerButton;

.field private mListener:Lcom/android/camera/manager/PickerManager$PickerListener;

.field private mNeedUpdate:Z

.field private mPreferenceReady:Z

.field private mStereoPicker:Lcom/android/camera/ui/PickerButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/PickerManager;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 0
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;)V

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addOnPreferenceReadyListener(Lcom/android/camera/Camera$OnPreferenceReadyListener;)Z

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addOnParametersReadyListener(Lcom/android/camera/Camera$OnParametersReadyListener;)Z

    return-void
.end method

.method private applyListeners()V
    .locals 3

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/PickerButton;->setListener(Lcom/android/camera/ui/PickerButton$Listener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/PickerButton;->setListener(Lcom/android/camera/ui/PickerButton$Listener;)V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/PickerButton;->setListener(Lcom/android/camera/ui/PickerButton$Listener;)V

    :cond_2
    sget-boolean v0, Lcom/android/camera/manager/PickerManager;->LOG:Z

    if-eqz v0, :cond_3

    const-string v0, "PickerManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyListeners() mFlashPicker="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCameraPicker="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mStereoPicker="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private clearListeners()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/PickerButton;->setListener(Lcom/android/camera/ui/PickerButton$Listener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/PickerButton;->setListener(Lcom/android/camera/ui/PickerButton$Listener;)V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/PickerButton;->setListener(Lcom/android/camera/ui/PickerButton$Listener;)V

    :cond_2
    return-void
.end method


# virtual methods
.method protected getView()Landroid/view/View;
    .locals 2

    const v1, 0x7f04002c

    invoke-virtual {p0, v1}, Lcom/android/camera/manager/ViewManager;->inflate(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b0092

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/PickerButton;

    iput-object v1, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    const v1, 0x7f0b0093

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/PickerButton;

    iput-object v1, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    const v1, 0x7f0b0091

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/PickerButton;

    iput-object v1, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    invoke-direct {p0}, Lcom/android/camera/manager/PickerManager;->applyListeners()V

    return-object v0
.end method

.method public onCameraParameterReady()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/manager/PickerManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PickerManager"

    const-string v1, "onCameraParameterReady()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0}, Lcom/android/camera/ui/PickerButton;->reloadPreference()V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0}, Lcom/android/camera/ui/PickerButton;->reloadPreference()V

    :cond_2
    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0}, Lcom/android/camera/ui/PickerButton;->reloadPreference()V

    :cond_3
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    return-void
.end method

.method public onPicked(Lcom/android/camera/ui/PickerButton;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1    # Lcom/android/camera/ui/PickerButton;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mListener:Lcom/android/camera/manager/PickerManager$PickerListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    if-ne v1, p1, :cond_2

    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mListener:Lcom/android/camera/manager/PickerManager$PickerListener;

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/android/camera/manager/PickerManager$PickerListener;->onCameraPicked(I)Z

    move-result v0

    :cond_0
    :goto_0
    sget-boolean v1, Lcom/android/camera/manager/PickerManager;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "PickerManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPicked("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") mListener="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/manager/PickerManager;->mListener:Lcom/android/camera/manager/PickerManager$PickerListener;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0

    :cond_2
    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    if-ne v1, p1, :cond_3

    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mListener:Lcom/android/camera/manager/PickerManager$PickerListener;

    invoke-interface {v1, p3}, Lcom/android/camera/manager/PickerManager$PickerListener;->onFlashPicked(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    if-ne v1, p1, :cond_0

    iget-object v2, p0, Lcom/android/camera/manager/PickerManager;->mListener:Lcom/android/camera/manager/PickerManager$PickerListener;

    const-string v1, "1"

    invoke-virtual {v1, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    invoke-interface {v2, v1}, Lcom/android/camera/manager/PickerManager$PickerListener;->onStereoPicked(Z)Z

    move-result v0

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onPreferenceReady()V
    .locals 3

    const/4 v2, 0x1

    sget-boolean v0, Lcom/android/camera/manager/PickerManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PickerManager"

    const-string v1, "onPreferenceReady()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean v2, p0, Lcom/android/camera/manager/PickerManager;->mNeedUpdate:Z

    iput-boolean v2, p0, Lcom/android/camera/manager/PickerManager;->mPreferenceReady:Z

    return-void
.end method

.method public onRefresh()V
    .locals 6

    const/16 v3, 0x8

    const/4 v2, 0x0

    sget-boolean v1, Lcom/android/camera/manager/PickerManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "PickerManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onRefresh() mPreferenceReady="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/camera/manager/PickerManager;->mPreferenceReady:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mNeedUpdate="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/camera/manager/PickerManager;->mNeedUpdate:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v1, p0, Lcom/android/camera/manager/PickerManager;->mPreferenceReady:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/camera/manager/PickerManager;->mNeedUpdate:Z

    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->getListPreference(I)Lcom/android/camera/ListPreference;

    move-result-object v1

    check-cast v1, Lcom/android/camera/IconListPreference;

    invoke-virtual {v4, v1}, Lcom/android/camera/ui/PickerButton;->initialize(Lcom/android/camera/IconListPreference;)V

    iget-object v4, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lcom/android/camera/Camera;->getListPreference(I)Lcom/android/camera/ListPreference;

    move-result-object v1

    check-cast v1, Lcom/android/camera/IconListPreference;

    invoke-virtual {v4, v1}, Lcom/android/camera/ui/PickerButton;->initialize(Lcom/android/camera/IconListPreference;)V

    iget-object v4, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    const/16 v5, 0x2d

    invoke-virtual {v1, v5}, Lcom/android/camera/Camera;->getListPreference(I)Lcom/android/camera/ListPreference;

    move-result-object v1

    check-cast v1, Lcom/android/camera/IconListPreference;

    invoke-virtual {v4, v1}, Lcom/android/camera/ui/PickerButton;->initialize(Lcom/android/camera/IconListPreference;)V

    iput-boolean v2, p0, Lcom/android/camera/manager/PickerManager;->mNeedUpdate:Z

    :cond_1
    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v1}, Lcom/android/camera/ui/PickerButton;->updateView()V

    :cond_2
    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-static {v1}, Lcom/android/camera/ModeChecker;->getCameraPickerVisible(Lcom/android/camera/Camera;)Z

    move-result v0

    iget-object v4, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_5

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-static {v1}, Lcom/android/camera/ModeChecker;->getStereoPickerVisibile(Lcom/android/camera/Camera;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_6

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_4
    return-void

    :cond_5
    move v1, v3

    goto :goto_0

    :cond_6
    move v2, v3

    goto :goto_1
.end method

.method protected onRelease()V
    .locals 1

    invoke-super {p0}, Lcom/android/camera/manager/ViewManager;->onRelease()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/manager/PickerManager;->mNeedUpdate:Z

    return-void
.end method

.method public setCameraId(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/PickerButton;->setValue(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mFlashPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mCameraPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/manager/PickerManager;->mStereoPicker:Lcom/android/camera/ui/PickerButton;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    :cond_2
    return-void
.end method

.method public setListener(Lcom/android/camera/manager/PickerManager$PickerListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/manager/PickerManager$PickerListener;

    iput-object p1, p0, Lcom/android/camera/manager/PickerManager;->mListener:Lcom/android/camera/manager/PickerManager$PickerListener;

    return-void
.end method
