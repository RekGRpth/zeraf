.class public Lcom/android/camera/manager/ModePicker;
.super Lcom/android/camera/manager/ViewManager;
.source "ModePicker.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/android/camera/Camera$OnFullScreenChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/manager/ModePicker$OnModeChangedListener;
    }
.end annotation


# static fields
.field private static final DELAY_MSG_HIDE_MS:I = 0xbb8

.field private static final LOG:Z

.field public static final MODE_ASD:I = 0x5

.field public static final MODE_BEST:I = 0x7

.field public static final MODE_EV:I = 0x8

.field public static final MODE_FACE_BEAUTY:I = 0x2

.field public static final MODE_HDR:I = 0x1

.field private static final MODE_ICONS_HIGHTLIGHT:[I

.field private static final MODE_ICONS_NORMAL:[I

.field public static final MODE_MAV:I = 0x4

.field public static final MODE_NUM_ALL:I = 0xc

.field public static final MODE_NUM_ICON:I = 0xa

.field public static final MODE_PANORAMA:I = 0x3

.field public static final MODE_PANORAMA_SINGLE_3D:I = 0xcb

.field public static final MODE_PHOTO:I = 0x0

.field public static final MODE_PHOTO_3D:I = 0x64

.field public static final MODE_PHOTO_SGINLE_3D:I = 0xc8

.field public static final MODE_SMILE_SHOT:I = 0x6

.field public static final MODE_VIDEO:I = 0x9

.field public static final MODE_VIDEO_3D:I = 0x6d

.field private static final OFFSET:I = 0x64

.field private static final OFFSET_STEREO_PREVIEW:I = 0x64

.field private static final OFFSET_STEREO_SINGLE:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "ModePicker"


# instance fields
.field private mCurrentMode:I

.field private mModeChangeListener:Lcom/android/camera/manager/ModePicker$OnModeChangedListener;

.field private mModeToast:Lcom/android/camera/manager/OnScreenToast;

.field private final mModeViews:[Lcom/android/camera/ui/RotateImageView;

.field private mScrollView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x9

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/ModePicker;->LOG:Z

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/camera/manager/ModePicker;->MODE_ICONS_HIGHTLIGHT:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/camera/manager/ModePicker;->MODE_ICONS_NORMAL:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0200df
        0x7f0200d9
        0x7f0200d7
        0x7f0200dd
        0x7f0200db
        0x7f0200d1
        0x7f0200e1
        0x7f0200d3
        0x7f0200d5
    .end array-data

    :array_1
    .array-data 4
        0x7f0200e0
        0x7f0200da
        0x7f0200d8
        0x7f0200de
        0x7f0200dc
        0x7f0200d2
        0x7f0200e2
        0x7f0200d4
        0x7f0200d6
    .end array-data
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 1
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;)V

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/android/camera/ui/RotateImageView;

    iput-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/camera/manager/ModePicker;->mCurrentMode:I

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addOnFullScreenChangedListener(Lcom/android/camera/Camera$OnFullScreenChangedListener;)Z

    return-void
.end method

.method private applyListener()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private clearListener()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private highlightCurrentMode()V
    .locals 4

    iget v2, p0, Lcom/android/camera/manager/ModePicker;->mCurrentMode:I

    invoke-virtual {p0, v2}, Lcom/android/camera/manager/ModePicker;->getModeIndex(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    if-ne v0, v1, :cond_1

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v2, v2, v0

    sget-object v3, Lcom/android/camera/manager/ModePicker;->MODE_ICONS_HIGHTLIGHT:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v2, v2, v0

    sget-object v3, Lcom/android/camera/manager/ModePicker;->MODE_ICONS_NORMAL:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private notifyModeChanged()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeChangeListener:Lcom/android/camera/manager/ModePicker$OnModeChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeChangeListener:Lcom/android/camera/manager/ModePicker$OnModeChangedListener;

    invoke-virtual {p0}, Lcom/android/camera/manager/ModePicker;->getCurrentMode()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/camera/manager/ModePicker$OnModeChangedListener;->onModeChanged(I)V

    :cond_0
    return-void
.end method

.method private setRealMode(I)V
    .locals 3
    .param p1    # I

    sget-boolean v0, Lcom/android/camera/manager/ModePicker;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ModePicker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRealMode("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mCurrentMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/manager/ModePicker;->mCurrentMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/camera/manager/ModePicker;->mCurrentMode:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lcom/android/camera/manager/ModePicker;->mCurrentMode:I

    invoke-direct {p0}, Lcom/android/camera/manager/ModePicker;->highlightCurrentMode()V

    invoke-direct {p0}, Lcom/android/camera/manager/ModePicker;->notifyModeChanged()V

    iget-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeToast:Lcom/android/camera/manager/OnScreenToast;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeToast:Lcom/android/camera/manager/OnScreenToast;

    invoke-virtual {v0}, Lcom/android/camera/manager/OnScreenToast;->cancel()V

    :cond_1
    return-void
.end method


# virtual methods
.method public getCurrentMode()I
    .locals 1

    iget v0, p0, Lcom/android/camera/manager/ModePicker;->mCurrentMode:I

    return v0
.end method

.method public getModeIndex(I)I
    .locals 4
    .param p1    # I

    rem-int/lit8 v0, p1, 0x64

    sget-boolean v1, Lcom/android/camera/manager/ModePicker;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "ModePicker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getModeIndex("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v0
.end method

.method protected getView()Landroid/view/View;
    .locals 4

    invoke-direct {p0}, Lcom/android/camera/manager/ModePicker;->clearListener()V

    const v1, 0x7f040026

    invoke-virtual {p0, v1}, Lcom/android/camera/manager/ViewManager;->inflate(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b0077

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/manager/ModePicker;->mScrollView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    const/4 v3, 0x0

    const v1, 0x7f0b0079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    const/4 v3, 0x1

    const v1, 0x7f0b007a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    const/4 v3, 0x2

    const v1, 0x7f0b007b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    const/4 v3, 0x3

    const v1, 0x7f0b007c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    const/4 v3, 0x4

    const v1, 0x7f0b007d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    const/4 v3, 0x5

    const v1, 0x7f0b007e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    const/4 v3, 0x6

    const v1, 0x7f0b007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    const/4 v3, 0x7

    const v1, 0x7f0b0080

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    const/16 v3, 0x8

    const v1, 0x7f0b0081

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    aput-object v1, v2, v3

    invoke-direct {p0}, Lcom/android/camera/manager/ModePicker;->applyListener()V

    invoke-direct {p0}, Lcom/android/camera/manager/ModePicker;->highlightCurrentMode()V

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    sget-boolean v1, Lcom/android/camera/manager/ModePicker;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "ModePicker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") isEnabled()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", view.isEnabled()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", getContext().isFullScreen()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/camera/manager/ModePicker;->setCurrentMode(I)V

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onFullScreenChanged(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/manager/ModePicker;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ModePicker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFullScreenChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mModeToast="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ModePicker;->mModeToast:Lcom/android/camera/manager/OnScreenToast;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeToast:Lcom/android/camera/manager/OnScreenToast;

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeToast:Lcom/android/camera/manager/OnScreenToast;

    invoke-virtual {v0}, Lcom/android/camera/manager/OnScreenToast;->cancel()V

    :cond_1
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1    # Landroid/view/View;

    sget-boolean v0, Lcom/android/camera/manager/ModePicker;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ModePicker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLongClick("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeToast:Lcom/android/camera/manager/OnScreenToast;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/manager/OnScreenToast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/android/camera/manager/OnScreenToast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeToast:Lcom/android/camera/manager/OnScreenToast;

    :goto_0
    iget-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeToast:Lcom/android/camera/manager/OnScreenToast;

    invoke-virtual {v0}, Lcom/android/camera/manager/OnScreenToast;->showToast()V

    :cond_1
    const/4 v0, 0x0

    return v0

    :cond_2
    iget-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeToast:Lcom/android/camera/manager/OnScreenToast;

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/OnScreenToast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onRefresh()V
    .locals 8

    const/16 v5, 0x8

    const/4 v4, 0x0

    sget-boolean v3, Lcom/android/camera/manager/ModePicker;->LOG:Z

    if-eqz v3, :cond_0

    const-string v3, "ModePicker"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onRefresh() mCurrentMode="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/camera/manager/ModePicker;->mCurrentMode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0xa

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v6

    invoke-static {v3, v6, v0}, Lcom/android/camera/ModeChecker;->getModePickerVisible(Lcom/android/camera/Camera;II)Z

    move-result v1

    iget-object v3, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v6, v3, v0

    if-eqz v1, :cond_2

    move v3, v4

    :goto_1
    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v1, :cond_1

    add-int/lit8 v2, v2, 0x1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v3, v5

    goto :goto_1

    :cond_3
    const/4 v3, 0x1

    if-gt v2, v3, :cond_4

    iget-object v3, p0, Lcom/android/camera/manager/ModePicker;->mScrollView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    invoke-direct {p0}, Lcom/android/camera/manager/ModePicker;->highlightCurrentMode()V

    return-void

    :cond_4
    iget-object v3, p0, Lcom/android/camera/manager/ModePicker;->mScrollView:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method protected onRelease()V
    .locals 1

    invoke-super {p0}, Lcom/android/camera/manager/ViewManager;->onRelease()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/manager/ModePicker;->mModeToast:Lcom/android/camera/manager/OnScreenToast;

    return-void
.end method

.method public setCurrentMode(I)V
    .locals 4
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/camera/manager/ModePicker;->getModeIndex(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isStereoMode()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isStereoSingle3d()Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit16 v0, v0, 0xc8

    :cond_0
    :goto_0
    sget-boolean v1, Lcom/android/camera/manager/ModePicker;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "ModePicker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setCurrentMode("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") realmode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-direct {p0, v0}, Lcom/android/camera/manager/ModePicker;->setRealMode(I)V

    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x64

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mScrollView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mScrollView:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/camera/manager/ModePicker;->mModeViews:[Lcom/android/camera/ui/RotateImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/view/View;->setClickable(Z)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public setListener(Lcom/android/camera/manager/ModePicker$OnModeChangedListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/manager/ModePicker$OnModeChangedListener;

    iput-object p1, p0, Lcom/android/camera/manager/ModePicker;->mModeChangeListener:Lcom/android/camera/manager/ModePicker$OnModeChangedListener;

    return-void
.end method
