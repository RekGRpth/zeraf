.class public Lcom/android/camera/manager/RemainingManager;
.super Lcom/android/camera/manager/ViewManager;
.source "RemainingManager.java"

# interfaces
.implements Lcom/android/camera/Camera$OnFullScreenChangedListener;
.implements Lcom/android/camera/Camera$OnParametersReadyListener;
.implements Lcom/android/camera/Camera$Resumable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/manager/RemainingManager$Holder;,
        Lcom/android/camera/manager/RemainingManager$WorkerHandler;
    }
.end annotation


# static fields
.field private static final DELAY_MSG_MS:I = 0x5dc

.field private static final LOG:Z

.field private static final MATRIX_REMAINING_TYPE:[I

.field private static final MSG_UPDATE_STORAGE:I = 0x0

.field private static final REMAIND_THRESHOLD:Ljava/lang/Long;

.field private static final TAG:Ljava/lang/String; = "RemainingManager"

.field private static final TYPE_COUNT:I = 0x0

.field private static final TYPE_TIME:I = 0x1


# instance fields
.field private mAvaliableSpace:Ljava/lang/Long;

.field private mMainHandler:Landroid/os/Handler;

.field private mParametersReady:Z

.field private mRemainingText:Ljava/lang/String;

.field private mRemainingView:Landroid/widget/TextView;

.field private mResumed:Z

.field private mStorageHint:Lcom/android/camera/manager/OnScreenHint;

.field private mType:I

.field private mWorkerHandler:Lcom/android/camera/manager/RemainingManager$WorkerHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    const-wide/16 v0, 0x64

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/android/camera/manager/RemainingManager;->REMAIND_THRESHOLD:Ljava/lang/Long;

    const/16 v0, 0xa

    new-array v0, v0, [I

    sput-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    sget-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    aput v2, v0, v2

    sget-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    aput v2, v0, v3

    sget-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    const/4 v1, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    const/4 v1, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    const/4 v1, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    const/4 v1, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    const/4 v1, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    const/4 v1, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    const/16 v1, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    const/16 v1, 0x9

    aput v3, v0, v1

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;)V

    iput v1, p0, Lcom/android/camera/manager/RemainingManager;->mType:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addResumable(Lcom/android/camera/Camera$Resumable;)Z

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addOnFullScreenChangedListener(Lcom/android/camera/Camera$OnFullScreenChangedListener;)Z

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addOnParametersReadyListener(Lcom/android/camera/Camera$OnParametersReadyListener;)Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/camera/manager/ViewManager;->setAnimationEnabled(ZZ)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/manager/RemainingManager;)Ljava/lang/Long;
    .locals 1
    .param p0    # Lcom/android/camera/manager/RemainingManager;

    iget-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/camera/manager/RemainingManager;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0
    .param p0    # Lcom/android/camera/manager/RemainingManager;
    .param p1    # Ljava/lang/Long;

    iput-object p1, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/camera/manager/RemainingManager;J)J
    .locals 2
    .param p0    # Lcom/android/camera/manager/RemainingManager;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/camera/manager/RemainingManager;->computeStorage(J)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$200(Lcom/android/camera/manager/RemainingManager;J)V
    .locals 0
    .param p0    # Lcom/android/camera/manager/RemainingManager;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/camera/manager/RemainingManager;->updateStorageHint(J)V

    return-void
.end method

.method private computeStorage(J)J
    .locals 4
    .param p1    # J

    sget-wide v0, Lcom/android/camera/Storage;->LOW_STORAGE_THRESHOLD:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_3

    sget-wide v0, Lcom/android/camera/Storage;->LOW_STORAGE_THRESHOLD:J

    sub-long v2, p1, v0

    iget v0, p0, Lcom/android/camera/manager/RemainingManager;->mType:I

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/camera/manager/RemainingManager;->pictureSize()J

    move-result-wide v0

    :goto_0
    div-long p1, v2, v0

    :cond_0
    :goto_1
    invoke-static {p1, p2}, Lcom/android/camera/Storage;->setLeftSpace(J)V

    sget-boolean v0, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v0, :cond_1

    const-string v0, "RemainingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "computeStorage("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-wide p1

    :cond_2
    invoke-virtual {p0}, Lcom/android/camera/manager/RemainingManager;->videoFrameRate()J

    move-result-wide v0

    goto :goto_0

    :cond_3
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const-wide/16 p1, 0x0

    goto :goto_1
.end method

.method private static stringForCount(J)Ljava/lang/String;
    .locals 4
    .param p0    # J

    const-string v0, "%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static stringForTime(J)Ljava/lang/String;
    .locals 11
    .param p0    # J

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    long-to-int v4, p0

    div-int/lit16 v3, v4, 0x3e8

    rem-int/lit8 v2, v3, 0x3c

    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    div-int/lit16 v0, v3, 0xe10

    if-lez v0, :cond_0

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v5, "%d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_0
    return-object v4

    :cond_0
    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v5, "%02d:%02d"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private updateRemainingView(JJ)V
    .locals 4
    .param p1    # J
    .param p3    # J

    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    iget v1, p0, Lcom/android/camera/manager/RemainingManager;->mType:I

    if-nez v1, :cond_0

    cmp-long v1, p3, p1

    if-gtz v1, :cond_3

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    cmp-long v1, p3, v2

    if-gez v1, :cond_5

    iget v1, p0, Lcom/android/camera/manager/RemainingManager;->mType:I

    if-nez v1, :cond_4

    invoke-static {v2, v3}, Lcom/android/camera/manager/RemainingManager;->stringForCount(J)Ljava/lang/String;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mRemainingText:Ljava/lang/String;

    :goto_2
    invoke-super {p0}, Lcom/android/camera/manager/ViewManager;->show()V

    :cond_1
    sget-boolean v1, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v1, :cond_2

    const-string v1, "RemainingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateRemainingView("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") mType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/manager/RemainingManager;->mType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    invoke-static {v2, v3}, Lcom/android/camera/manager/RemainingManager;->stringForTime(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_5
    iget v1, p0, Lcom/android/camera/manager/RemainingManager;->mType:I

    if-nez v1, :cond_6

    invoke-static {p3, p4}, Lcom/android/camera/manager/RemainingManager;->stringForCount(J)Ljava/lang/String;

    move-result-object v1

    :goto_3
    iput-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mRemainingText:Ljava/lang/String;

    goto :goto_2

    :cond_6
    invoke-static {p3, p4}, Lcom/android/camera/manager/RemainingManager;->stringForTime(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method private updateStorageHint(J)V
    .locals 4
    .param p1    # J

    sget-boolean v1, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "RemainingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateStorageHint("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") isFullScreen="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    const-wide/16 v1, -0x1

    cmp-long v1, p1, v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    const v2, 0x7f0c00fb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_0
    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isAcceptFloatingInfo()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mStorageHint:Lcom/android/camera/manager/OnScreenHint;

    if-nez v1, :cond_6

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/camera/manager/OnScreenHint;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/android/camera/manager/OnScreenHint;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mStorageHint:Lcom/android/camera/manager/OnScreenHint;

    :goto_1
    iget-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mStorageHint:Lcom/android/camera/manager/OnScreenHint;

    invoke-virtual {v1}, Lcom/android/camera/manager/OnScreenHint;->show()V

    :cond_2
    :goto_2
    return-void

    :cond_3
    const-wide/16 v1, -0x2

    cmp-long v1, p1, v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    const v2, 0x7f0c00fd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-wide/16 v1, -0x3

    cmp-long v1, p1, v1

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    const v2, 0x7f0c00fe

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-gtz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-static {}, Lcom/android/camera/Util;->getNotEnoughSpaceAlertMessageId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mStorageHint:Lcom/android/camera/manager/OnScreenHint;

    invoke-virtual {v1, v0}, Lcom/android/camera/manager/OnScreenHint;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mStorageHint:Lcom/android/camera/manager/OnScreenHint;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mStorageHint:Lcom/android/camera/manager/OnScreenHint;

    invoke-virtual {v1}, Lcom/android/camera/manager/OnScreenHint;->cancel()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mStorageHint:Lcom/android/camera/manager/OnScreenHint;

    goto :goto_2
.end method


# virtual methods
.method public begin()V
    .locals 3

    iget-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mWorkerHandler:Lcom/android/camera/manager/RemainingManager$WorkerHandler;

    if-nez v1, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "thumbnail-creation-thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v1, Lcom/android/camera/manager/RemainingManager$WorkerHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/camera/manager/RemainingManager$WorkerHandler;-><init>(Lcom/android/camera/manager/RemainingManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mWorkerHandler:Lcom/android/camera/manager/RemainingManager$WorkerHandler;

    iget-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mWorkerHandler:Lcom/android/camera/manager/RemainingManager$WorkerHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public clearAvaliableSpace()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "RemainingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearAvaliableSpace() mAvaliableSpace="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mParametersReady="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/manager/RemainingManager;->mParametersReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    return-void
.end method

.method public finish()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mWorkerHandler:Lcom/android/camera/manager/RemainingManager$WorkerHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mWorkerHandler:Lcom/android/camera/manager/RemainingManager$WorkerHandler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_0
    return-void
.end method

.method protected getView()Landroid/view/View;
    .locals 2

    const v1, 0x7f04004e

    invoke-virtual {p0, v1}, Lcom/android/camera/manager/ViewManager;->inflate(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b0117

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mRemainingView:Landroid/widget/TextView;

    return-object v0
.end method

.method public onCameraParameterReady()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/manager/RemainingManager;->mParametersReady:Z

    return-void
.end method

.method public onFullScreenChanged(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "RemainingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFullScreenChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/RemainingManager;->resume()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/camera/manager/RemainingManager;->pause()V

    goto :goto_0
.end method

.method protected onRefresh()V
    .locals 2

    invoke-super {p0}, Lcom/android/camera/manager/ViewManager;->onRefresh()V

    iget-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mRemainingView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mRemainingView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/camera/manager/RemainingManager;->mRemainingText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public pause()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "RemainingManager"

    const-string v1, "pause()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/manager/RemainingManager;->mResumed:Z

    iget-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mStorageHint:Lcom/android/camera/manager/OnScreenHint;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mStorageHint:Lcom/android/camera/manager/OnScreenHint;

    invoke-virtual {v0}, Lcom/android/camera/manager/OnScreenHint;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mStorageHint:Lcom/android/camera/manager/OnScreenHint;

    :cond_1
    return-void
.end method

.method public pictureSize()J
    .locals 8

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getCameraActor()Lcom/android/camera/actor/CameraActor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v4

    iget v5, v4, Landroid/hardware/Camera$Size;->width:I

    iget v6, v4, Landroid/hardware/Camera$Size;->height:I

    if-le v5, v6, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, v4, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "x"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "superfine"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lcom/android/camera/Storage;->getSize(Ljava/lang/String;)I

    move-result v5

    int-to-long v2, v5

    sget-boolean v5, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v5, :cond_0

    const-string v5, "RemainingManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "pictureSize() pictureFormat="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " return "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-wide v2

    :pswitch_0
    const-string v0, "autorama"

    goto :goto_1

    :pswitch_1
    const-string v0, "mav"

    goto :goto_1

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "x"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v4, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public resume()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "RemainingManager"

    const-string v1, "resume()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/manager/RemainingManager;->mResumed:Z

    invoke-virtual {p0}, Lcom/android/camera/manager/RemainingManager;->showHint()V

    return-void
.end method

.method public showAways()Z
    .locals 6

    iget-boolean v2, p0, Lcom/android/camera/manager/RemainingManager;->mParametersReady:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    :cond_0
    invoke-static {}, Lcom/android/camera/Storage;->getAvailableSpace()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    :cond_1
    iget-object v2, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v3

    aget v2, v2, v3

    iput v2, p0, Lcom/android/camera/manager/RemainingManager;->mType:I

    invoke-direct {p0, v0, v1}, Lcom/android/camera/manager/RemainingManager;->computeStorage(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/camera/manager/RemainingManager;->updateStorageHint(J)V

    const-wide v2, 0x7fffffffffffffffL

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/camera/manager/RemainingManager;->updateRemainingView(JJ)V

    :cond_2
    sget-boolean v2, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v2, :cond_3

    const-string v2, "RemainingManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showAways() return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mParametersReady="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/camera/manager/RemainingManager;->mParametersReady:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v2

    return v2
.end method

.method public showHint()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "RemainingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showHint() mAvaliableSpace="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mParametersReady="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/manager/RemainingManager;->mParametersReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/manager/RemainingManager;->mParametersReady:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/RemainingManager;->mMainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/camera/manager/RemainingManager$1;

    invoke-direct {v1, p0}, Lcom/android/camera/manager/RemainingManager$1;-><init>(Lcom/android/camera/manager/RemainingManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public showIfNeed()Z
    .locals 6

    iget-boolean v2, p0, Lcom/android/camera/manager/RemainingManager;->mParametersReady:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    :cond_0
    invoke-static {}, Lcom/android/camera/Storage;->getAvailableSpace()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    :cond_1
    iget-object v2, p0, Lcom/android/camera/manager/RemainingManager;->mAvaliableSpace:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Lcom/android/camera/manager/RemainingManager;->MATRIX_REMAINING_TYPE:[I

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v3

    aget v2, v2, v3

    iput v2, p0, Lcom/android/camera/manager/RemainingManager;->mType:I

    invoke-direct {p0, v0, v1}, Lcom/android/camera/manager/RemainingManager;->computeStorage(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/camera/manager/RemainingManager;->updateStorageHint(J)V

    sget-object v2, Lcom/android/camera/manager/RemainingManager;->REMAIND_THRESHOLD:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/camera/manager/RemainingManager;->updateRemainingView(JJ)V

    :cond_2
    sget-boolean v2, Lcom/android/camera/manager/RemainingManager;->LOG:Z

    if-eqz v2, :cond_3

    const-string v2, "RemainingManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showIfNeed() return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mParametersReady="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/camera/manager/RemainingManager;->mParametersReady:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v2

    return v2
.end method

.method public videoFrameRate()J
    .locals 5

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getProfile()Landroid/media/CamcorderProfile;

    move-result-object v2

    iget v3, v2, Landroid/media/CamcorderProfile;->videoBitRate:I

    iget v4, v2, Landroid/media/CamcorderProfile;->audioBitRate:I

    add-int/2addr v3, v4

    shr-int/lit8 v3, v3, 0x3

    div-int/lit16 v3, v3, 0x3e8

    int-to-long v0, v3

    return-wide v0
.end method
