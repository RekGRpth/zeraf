.class public Lcom/android/camera/manager/MMProfileManager;
.super Ljava/lang/Object;
.source "MMProfileManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;
    }
.end annotation


# static fields
.field private static final EVENT_ANIMATE_CAPTURE:I

.field private static final EVENT_ANIMATE_SWITCH_CAMERA:I

.field private static final EVENT_APPLY_PARAMETERS:I

.field private static final EVENT_APPLY_PARAM_UI_IMMEDIAT:I

.field private static final EVENT_CAMERA_ACTIVITY:I

.field private static final EVENT_CAMERA_ACTIVITY_DETAIL:I

.field private static final EVENT_CAMERA_HANDLE_MSG:I

.field private static final EVENT_CAMERA_HARDWARE:I

.field private static final EVENT_CAMERA_ON_CONFIG_CHANGE:I

.field private static final EVENT_CAMERA_ON_CREATE:I

.field private static final EVENT_CAMERA_ON_DESTROY:I

.field private static final EVENT_CAMERA_ON_ORIENT_CHANGE:I

.field private static final EVENT_CAMERA_ON_PAUSE:I

.field private static final EVENT_CAMERA_ON_RESUME:I

.field private static final EVENT_CAMERA_OPEN:I

.field private static final EVENT_CAMERA_PARAMETER_COPY:I

.field private static final EVENT_CAMERA_PREVIEW_PRE_READY_BLOCK:I

.field private static final EVENT_CAMERA_PREVIEW_PRE_READY_OPEN:I

.field private static final EVENT_CAMERA_RELEASE:I

.field private static final EVENT_CAMERA_ROOT:I

.field private static final EVENT_CAMERA_SCREEN_NAIL:I

.field private static final EVENT_CAMERA_SEND_MSG:I

.field private static final EVENT_CAMERA_START_UP:I

.field private static final EVENT_CAMERA_VIEW_OPERATION:I

.field private static final EVENT_DRAW_SCREEN_NAIL:I

.field private static final EVENT_FIRST_FRAME_AVAILABLE:I

.field private static final EVENT_FRAME_AVAILABLE:I

.field private static final EVENT_GET_PARAMETERS:I

.field private static final EVENT_INIT_CAMERA_PREF:I

.field private static final EVENT_INIT_FOCUS_MGR:I

.field private static final EVENT_INIT_OPEN_PROCESS:I

.field private static final EVENT_LAYOUT_CHANGE:I

.field private static final EVENT_NOTIFY_ORIENT_CHANGED:I

.field private static final EVENT_NOTIFY_SERVER_SELF_CHANGE:I

.field private static final EVENT_PHOTO_ACTOR:I

.field private static final EVENT_PHOTO_SHUTTER_CLICK:I

.field private static final EVENT_PHOTO_STORE_PICTURE:I

.field private static final EVENT_PHOTO_TAKE_PICTURE:I

.field private static final EVENT_REQUEST_RENDER:I

.field private static final EVENT_RE_INFLATE_VIEW_MGR:I

.field private static final EVENT_SETTING_CHECKER:I

.field private static final EVENT_SET_DISP_ORIENT:I

.field private static final EVENT_SET_PARAMETERS:I

.field private static final EVENT_SET_PREVIEW_ASPECT_RATIO:I

.field private static final EVENT_SET_PREVIEW_TEXT:I

.field private static final EVENT_START_PREVIEW:I

.field private static final EVENT_STOP_PREVIEW:I

.field private static final EVENT_SWITCH_CAMERA:I

.field private static final EVENT_UPDATE_SURFACE_TEXTURE:I

.field private static final EVENT_VIDEO_ACTOR:I

.field private static final EVENT_VIDEO_SHUTTER_CLICK:I

.field private static final EVENT_VIDEO_START_RECORD:I

.field private static final EVENT_VIDEO_STOP_RECORD:I

.field private static final EVENT_VIDEO_STORE_VIDEO:I

.field private static final NAME_ANIMATE_CAPTURE:Ljava/lang/String; = "AnimateCapture"

.field private static final NAME_ANIMATE_SWITCH_CAMERA:Ljava/lang/String; = "AnimateSwitchCamera"

.field private static final NAME_APPLY_PARAMETERS:Ljava/lang/String; = "ApplyParameters"

.field private static final NAME_APPLY_PARAM_UI_IMMEDIAT:Ljava/lang/String; = "ApplyParametersToUiImmediately"

.field private static final NAME_CAMERA_ACTIVITY:Ljava/lang/String; = "CameraActivity"

.field private static final NAME_CAMERA_ACTIVITY_DETAIL:Ljava/lang/String; = "CameraActivityDetail"

.field private static final NAME_CAMERA_HANDLE_MSG:Ljava/lang/String; = "handleMessage"

.field private static final NAME_CAMERA_HARDWARE:Ljava/lang/String; = "CameraHardWare"

.field private static final NAME_CAMERA_ON_CONFIG_CHANGE:Ljava/lang/String; = "OnConfigChange"

.field private static final NAME_CAMERA_ON_CREATE:Ljava/lang/String; = "CameraOnCreate"

.field private static final NAME_CAMERA_ON_DESTROY:Ljava/lang/String; = "CameraOnDestroy"

.field private static final NAME_CAMERA_ON_ORIENT_CHANGE:Ljava/lang/String; = "OnOrientChange"

.field private static final NAME_CAMERA_ON_PAUSE:Ljava/lang/String; = "CameraOnPause"

.field private static final NAME_CAMERA_ON_RESUME:Ljava/lang/String; = "CameraOnResume"

.field private static final NAME_CAMERA_OPEN:Ljava/lang/String; = "Open"

.field private static final NAME_CAMERA_PARAMETER_COPY:Ljava/lang/String; = "CameraParameterCopy"

.field private static final NAME_CAMERA_PREVIEW_PRE_READY_BLOCK:Ljava/lang/String; = "CameraPreviewPreReadyBlock"

.field private static final NAME_CAMERA_PREVIEW_PRE_READY_OPEN:Ljava/lang/String; = "CameraPreviewPreReadyOpen"

.field private static final NAME_CAMERA_RELEASE:Ljava/lang/String; = "Release"

.field private static final NAME_CAMERA_ROOT:Ljava/lang/String; = "CameraApp"

.field private static final NAME_CAMERA_SCREEN_NAIL:Ljava/lang/String; = "CameraScreenNail"

.field private static final NAME_CAMERA_SEND_MSG:Ljava/lang/String; = "sendMessage"

.field private static final NAME_CAMERA_START_PREVIEW:Ljava/lang/String; = "StartPreview"

.field private static final NAME_CAMERA_START_UP:Ljava/lang/String; = "CameraStartUp"

.field private static final NAME_CAMERA_STOP_PREVIEW:Ljava/lang/String; = "StopPreview"

.field private static final NAME_CAMERA_VIEW_OPERATION:Ljava/lang/String; = "CameraViewOperation"

.field private static final NAME_DRAW_SCREEN_NAIL:Ljava/lang/String; = "DrawScreenNail"

.field private static final NAME_FIRST_FRAME_AVAILABLE:Ljava/lang/String; = "FirstFrameAvailable"

.field private static final NAME_FRAME_AVAILABLE:Ljava/lang/String; = "FrameAvailable"

.field private static final NAME_GET_PARAMETERS:Ljava/lang/String; = "getParameters"

.field private static final NAME_INIT_CAMERA_PREF:Ljava/lang/String; = "InitCameraPref"

.field private static final NAME_INIT_FOCUS_MGR:Ljava/lang/String; = "InitFocusManager"

.field private static final NAME_INIT_OPEN_PROCESS:Ljava/lang/String; = "InitForOpeningProcess"

.field private static final NAME_LAYOUT_CHANGE:Ljava/lang/String; = "onLayoutChange"

.field private static final NAME_NOTIFY_ORIENT_CHANGED:Ljava/lang/String; = "NotifyOrientChanged"

.field private static final NAME_NOTIFY_SERVER_SELF_CHANGE:Ljava/lang/String; = "NotifyServerSelfChange"

.field private static final NAME_PHOTO_ACTOR:Ljava/lang/String; = "PhotoActor"

.field private static final NAME_PHOTO_SHUTTER_CLICK:Ljava/lang/String; = "ClickPhotoShutter"

.field private static final NAME_PHOTO_STORE_PICTURE:Ljava/lang/String; = "StorePicture"

.field private static final NAME_PHOTO_TAKE_PICTURE:Ljava/lang/String; = "TakePicture"

.field private static final NAME_REQUEST_RENDER:Ljava/lang/String; = "RequestRender"

.field private static final NAME_RE_INFLATE_VIEW_MGR:Ljava/lang/String; = "ReInflateViewManager"

.field private static final NAME_SETTING_CHECKER:Ljava/lang/String; = "SettingChecker"

.field private static final NAME_SET_DISP_ORIENT:Ljava/lang/String; = "SetDispOrient"

.field private static final NAME_SET_PARAMETERS:Ljava/lang/String; = "setParameters"

.field private static final NAME_SET_PREVIEW_ASPECT_RATIO:Ljava/lang/String; = "SetPreviewAspectRatio"

.field private static final NAME_SET_PREVIEW_TEXT:Ljava/lang/String; = "SetPreviewTexture"

.field private static final NAME_SWITCH_CAMERA:Ljava/lang/String; = "SwitchCamera"

.field private static final NAME_UPDATE_SURFACE_TEXTURE:Ljava/lang/String; = "UpdateSurfaceTexture"

.field private static final NAME_VIDEO_ACTOR:Ljava/lang/String; = "VideoActor"

.field private static final NAME_VIDEO_SHUTTER_CLICK:Ljava/lang/String; = "ClickVideoShutter"

.field private static final NAME_VIDEO_START_RECORD:Ljava/lang/String; = "VideoStartRecord"

.field private static final NAME_VIDEO_STOP_RECORD:Ljava/lang/String; = "VideoStopRecord"

.field private static final NAME_VIDEO_STORE_VIDEO:Ljava/lang/String; = "StoreVideo"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x1

    const-string v1, "CameraApp"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ROOT:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ROOT:I

    const-string v1, "CameraHardWare"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_HARDWARE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_HARDWARE:I

    const-string v1, "Open"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_OPEN:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_HARDWARE:I

    const-string v1, "StartPreview"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_START_PREVIEW:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_HARDWARE:I

    const-string v1, "StopPreview"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_STOP_PREVIEW:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_HARDWARE:I

    const-string v1, "Release"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_RELEASE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_HARDWARE:I

    const-string v1, "getParameters"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_GET_PARAMETERS:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_HARDWARE:I

    const-string v1, "setParameters"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SET_PARAMETERS:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ROOT:I

    const-string v1, "PhotoActor"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_ACTOR:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_ACTOR:I

    const-string v1, "ClickPhotoShutter"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_SHUTTER_CLICK:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_ACTOR:I

    const-string v1, "TakePicture"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_TAKE_PICTURE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_ACTOR:I

    const-string v1, "StorePicture"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_STORE_PICTURE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ROOT:I

    const-string v1, "SwitchCamera"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SWITCH_CAMERA:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ROOT:I

    const-string v1, "VideoActor"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_ACTOR:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_ACTOR:I

    const-string v1, "ClickVideoShutter"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_SHUTTER_CLICK:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_ACTOR:I

    const-string v1, "VideoStartRecord"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_START_RECORD:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_ACTOR:I

    const-string v1, "VideoStopRecord"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_STOP_RECORD:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_ACTOR:I

    const-string v1, "StoreVideo"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_STORE_VIDEO:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ROOT:I

    const-string v1, "CameraScreenNail"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_SCREEN_NAIL:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_SCREEN_NAIL:I

    const-string v1, "FrameAvailable"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_FRAME_AVAILABLE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_SCREEN_NAIL:I

    const-string v1, "FirstFrameAvailable"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_FIRST_FRAME_AVAILABLE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_SCREEN_NAIL:I

    const-string v1, "RequestRender"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_REQUEST_RENDER:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_SCREEN_NAIL:I

    const-string v1, "DrawScreenNail"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_DRAW_SCREEN_NAIL:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_SCREEN_NAIL:I

    const-string v1, "NotifyServerSelfChange"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_NOTIFY_SERVER_SELF_CHANGE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_SCREEN_NAIL:I

    const-string v1, "AnimateCapture"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_ANIMATE_CAPTURE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_SCREEN_NAIL:I

    const-string v1, "AnimateSwitchCamera"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_ANIMATE_SWITCH_CAMERA:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ROOT:I

    const-string v1, "CameraActivity"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    const-string v1, "CameraOnCreate"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_CREATE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    const-string v1, "CameraOnResume"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_RESUME:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    const-string v1, "CameraOnPause"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_PAUSE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    const-string v1, "CameraOnDestroy"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_DESTROY:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    const-string v1, "CameraStartUp"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_START_UP:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    const-string v1, "OnConfigChange"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_CONFIG_CHANGE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    const-string v1, "OnOrientChange"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_ORIENT_CHANGE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    const-string v1, "handleMessage"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_HANDLE_MSG:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    const-string v1, "sendMessage"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_SEND_MSG:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY:I

    const-string v1, "CameraActivityDetail"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "CameraViewOperation"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_VIEW_OPERATION:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "CameraParameterCopy"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_PARAMETER_COPY:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "CameraPreviewPreReadyBlock"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_PREVIEW_PRE_READY_BLOCK:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "CameraPreviewPreReadyOpen"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_PREVIEW_PRE_READY_OPEN:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "InitForOpeningProcess"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_INIT_OPEN_PROCESS:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "ApplyParameters"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_APPLY_PARAMETERS:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "InitCameraPref"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_INIT_CAMERA_PREF:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "SetDispOrient"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SET_DISP_ORIENT:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "SetPreviewAspectRatio"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SET_PREVIEW_ASPECT_RATIO:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "NotifyOrientChanged"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_NOTIFY_ORIENT_CHANGED:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "SetPreviewTexture"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SET_PREVIEW_TEXT:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "ReInflateViewManager"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_RE_INFLATE_VIEW_MGR:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "UpdateSurfaceTexture"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_UPDATE_SURFACE_TEXTURE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "InitFocusManager"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_INIT_FOCUS_MGR:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ACTIVITY_DETAIL:I

    const-string v1, "onLayoutChange"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_LAYOUT_CHANGE:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ROOT:I

    const-string v1, "SettingChecker"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SETTING_CHECKER:I

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SETTING_CHECKER:I

    const-string v1, "ApplyParametersToUiImmediately"

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_APPLY_PARAM_UI_IMMEDIAT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static startProfileAnimateCapture()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_ANIMATE_CAPTURE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileAnimateSwitchCamera()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_ANIMATE_SWITCH_CAMERA:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileApplyParameters()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_APPLY_PARAMETERS:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileApplyParamsToUiImmediately()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_APPLY_PARAM_UI_IMMEDIAT:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraOnConfigChange()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_CONFIG_CHANGE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraOnCreate()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_CREATE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraOnDestroy()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_DESTROY:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraOnOrientChange()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_ORIENT_CHANGE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraOnPause()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_PAUSE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraOnResume()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_RESUME:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraOpen()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_OPEN:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraParameterCopy()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_PARAMETER_COPY:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraPreviewPreReadyBlock()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_PREVIEW_PRE_READY_BLOCK:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraPreviewPreReadyOpen()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_PREVIEW_PRE_READY_OPEN:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraRelease()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_RELEASE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraStartUp()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_START_UP:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileCameraViewOperation()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_VIEW_OPERATION:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileDrawScreenNail()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_DRAW_SCREEN_NAIL:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileFirstFrameAvailable()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_FIRST_FRAME_AVAILABLE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileGetParameters()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_GET_PARAMETERS:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileHandleMessage(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_HANDLE_MSG:I

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLogMetaString(IILjava/lang/String;)I

    return-void
.end method

.method public static startProfileInitFocusManager()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_INIT_FOCUS_MGR:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileInitOpeningProcess()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_INIT_OPEN_PROCESS:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileInitPref()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_INIT_CAMERA_PREF:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileNotifyOrientChanged()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_NOTIFY_ORIENT_CHANGED:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileReInflateViewManager()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_RE_INFLATE_VIEW_MGR:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileSetDisplayOrientation()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SET_DISP_ORIENT:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileSetParameters()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SET_PARAMETERS:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileStartPreview()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_START_PREVIEW:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileStartVideoRecording()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_START_RECORD:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileStopPreview()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_STOP_PREVIEW:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileStopVideoRecording()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_STOP_RECORD:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileStorePicture()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_STORE_PICTURE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileStoreVideo()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_STORE_VIDEO:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileSwitchCamera()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SWITCH_CAMERA:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileTakePicture()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_TAKE_PICTURE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static startProfileUpdateSurfaceTexture()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_UPDATE_SURFACE_TEXTURE:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileAnimateCapture()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_ANIMATE_CAPTURE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileAnimateSwitchCamera()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_ANIMATE_SWITCH_CAMERA:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileApplyParameters()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_APPLY_PARAMETERS:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileApplyParamsToUiImmediately()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_APPLY_PARAM_UI_IMMEDIAT:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraOnConfigChange()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_CONFIG_CHANGE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraOnCreate()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_CREATE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraOnDestroy()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_DESTROY:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraOnOrientChange()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_ORIENT_CHANGE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraOnPause()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_PAUSE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraOnResume()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_ON_RESUME:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraOpen()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_OPEN:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraParameterCopy()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_PARAMETER_COPY:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraPreviewPreReadyBlock()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_PREVIEW_PRE_READY_BLOCK:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraPreviewPreReadyOpen()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_PREVIEW_PRE_READY_OPEN:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraRelease()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_RELEASE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraStartUp()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_START_UP:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileCameraViewOperation()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_VIEW_OPERATION:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileDrawScreenNail()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_DRAW_SCREEN_NAIL:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileFirstFrameAvailable()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_FIRST_FRAME_AVAILABLE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileGetParameters()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_GET_PARAMETERS:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileHandleMessage()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_HANDLE_MSG:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileInitFocusManager()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_INIT_FOCUS_MGR:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileInitOpeningProcess()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_INIT_OPEN_PROCESS:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileInitPref()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_INIT_CAMERA_PREF:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileNotifyOrientChanged()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_NOTIFY_ORIENT_CHANGED:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileReInflateViewManager()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_RE_INFLATE_VIEW_MGR:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileSetDisplayOrientation()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SET_DISP_ORIENT:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileSetParameters()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SET_PARAMETERS:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileStartPreview()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_START_PREVIEW:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileStartVideoRecording()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_START_RECORD:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileStopPreview()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_STOP_PREVIEW:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileStopVideoRecording()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_STOP_RECORD:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileStorePicture()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_STORE_PICTURE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileStoreVideo()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_STORE_VIDEO:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileSwitchCamera()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SWITCH_CAMERA:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileTakePicture()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_TAKE_PICTURE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static stopProfileUpdateSurfaceTexture()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_UPDATE_SURFACE_TEXTURE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static triggerFirstFrameAvailable()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_FIRST_FRAME_AVAILABLE:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static triggerFrameAvailable()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_FRAME_AVAILABLE:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static triggerNotifyServerSelfChange()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_NOTIFY_SERVER_SELF_CHANGE:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static triggerPhotoShutterClick()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_PHOTO_SHUTTER_CLICK:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static triggerProfileLayoutChange(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_LAYOUT_CHANGE:I

    const/4 v1, 0x4

    invoke-static {v0, v1, p0}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLogMetaString(IILjava/lang/String;)I

    return-void
.end method

.method public static triggerRequestRender()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_REQUEST_RENDER:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static triggerSetPreviewAspectRatio()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SET_PREVIEW_ASPECT_RATIO:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static triggerSetPreviewTexture()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_SET_PREVIEW_TEXT:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static triggerVideoShutterClick()V
    .locals 2

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_VIDEO_SHUTTER_CLICK:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLog(II)V

    return-void
.end method

.method public static triggersSendMessage(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    sget v0, Lcom/android/camera/manager/MMProfileManager;->EVENT_CAMERA_SEND_MSG:I

    const/4 v1, 0x4

    invoke-static {v0, v1, p0}, Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;->doMMProfileLogMetaString(IILjava/lang/String;)I

    return-void
.end method
