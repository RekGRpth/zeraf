.class public Lcom/android/camera/manager/RotateProgress;
.super Lcom/android/camera/manager/ViewManager;
.source "RotateProgress.java"


# static fields
.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String; = "RotateProgress"


# instance fields
.field private mMessage:Ljava/lang/String;

.field private mRotateDialogSpinner:Landroid/widget/ProgressBar;

.field private mRotateDialogText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/RotateProgress;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 1
    .param p1    # Lcom/android/camera/Camera;

    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;I)V

    return-void
.end method


# virtual methods
.method protected getView()Landroid/view/View;
    .locals 4

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    const v2, 0x7f040051

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getViewLayer()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/camera/Camera;->inflate(II)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b0125

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/android/camera/manager/RotateProgress;->mRotateDialogSpinner:Landroid/widget/ProgressBar;

    const v1, 0x7f0b0121

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/camera/manager/RotateProgress;->mRotateDialogText:Landroid/widget/TextView;

    return-object v0
.end method

.method protected onRefresh()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/camera/manager/RotateProgress;->mRotateDialogText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/camera/manager/RotateProgress;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/camera/manager/RotateProgress;->mRotateDialogText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/manager/RotateProgress;->mRotateDialogSpinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    sget-boolean v0, Lcom/android/camera/manager/RotateProgress;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "RotateProgress"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRefresh() mMessage="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/RotateProgress;->mMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public showProgress(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/camera/manager/RotateProgress;->mMessage:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->show()V

    sget-boolean v0, Lcom/android/camera/manager/RotateProgress;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "RotateProgress"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showProgress("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
