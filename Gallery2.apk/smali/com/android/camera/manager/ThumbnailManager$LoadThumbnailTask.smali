.class Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;
.super Landroid/os/AsyncTask;
.source "ThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/manager/ThumbnailManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadThumbnailTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/android/camera/Thumbnail;",
        ">;"
    }
.end annotation


# instance fields
.field private mLookAtCache:Z

.field final synthetic this$0:Lcom/android/camera/manager/ThumbnailManager;


# direct methods
.method public constructor <init>(Lcom/android/camera/manager/ThumbnailManager;Z)V
    .locals 0
    .param p2    # Z

    iput-object p1, p0, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;->this$0:Lcom/android/camera/manager/ThumbnailManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-boolean p2, p0, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;->mLookAtCache:Z

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/android/camera/Thumbnail;
    .locals 9
    .param p1    # [Ljava/lang/Void;

    const/4 v8, 0x1

    const/4 v4, 0x0

    invoke-static {}, Lcom/android/camera/manager/ThumbnailManager;->access$000()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "ThumbnailManager"

    const-string v6, "doInBackground() begin."

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v5, p0, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;->this$0:Lcom/android/camera/manager/ThumbnailManager;

    invoke-virtual {v5}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    iget-boolean v5, p0, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;->mLookAtCache:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;->this$0:Lcom/android/camera/manager/ThumbnailManager;

    invoke-virtual {v5}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContextWrapper;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/android/camera/Thumbnail;->getLastThumbnailFromFile(Ljava/io/File;Landroid/content/ContentResolver;)Lcom/android/camera/Thumbnail;

    move-result-object v3

    :cond_1
    invoke-static {}, Lcom/android/camera/manager/ThumbnailManager;->access$000()Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "ThumbnailManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "doInBackground() get from thumbnail. thumbnail="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", isCancelled()="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_4

    move-object v3, v4

    :cond_3
    :goto_0
    return-object v3

    :cond_4
    if-nez v3, :cond_6

    invoke-static {}, Lcom/android/camera/Storage;->isStorageReady()Z

    move-result v5

    if-eqz v5, :cond_6

    new-array v2, v8, [Lcom/android/camera/Thumbnail;

    invoke-static {v1, v2}, Lcom/android/camera/Thumbnail;->getLastThumbnailFromContentResolver(Landroid/content/ContentResolver;[Lcom/android/camera/Thumbnail;)I

    move-result v0

    invoke-static {}, Lcom/android/camera/manager/ThumbnailManager;->access$000()Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "ThumbnailManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getLastThumbnailFromContentResolver code = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    packed-switch v0, :pswitch_data_0

    move-object v3, v4

    goto :goto_0

    :pswitch_0
    const/4 v4, 0x0

    aget-object v3, v2, v4

    goto :goto_0

    :pswitch_1
    move-object v3, v4

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v8}, Landroid/os/AsyncTask;->cancel(Z)Z

    move-object v3, v4

    goto :goto_0

    :cond_6
    invoke-static {}, Lcom/android/camera/manager/ThumbnailManager;->access$000()Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "ThumbnailManager"

    const-string v5, "getLastThumbnailFromFile = true"

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;->doInBackground([Ljava/lang/Void;)Lcom/android/camera/Thumbnail;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/android/camera/Thumbnail;)V
    .locals 3
    .param p1    # Lcom/android/camera/Thumbnail;

    invoke-static {}, Lcom/android/camera/manager/ThumbnailManager;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ThumbnailManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPostExecute() thumbnail="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isCancelled()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;->this$0:Lcom/android/camera/manager/ThumbnailManager;

    invoke-static {v0, p1}, Lcom/android/camera/manager/ThumbnailManager;->access$502(Lcom/android/camera/manager/ThumbnailManager;Lcom/android/camera/Thumbnail;)Lcom/android/camera/Thumbnail;

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;->this$0:Lcom/android/camera/manager/ThumbnailManager;

    invoke-static {v0}, Lcom/android/camera/manager/ThumbnailManager;->access$400(Lcom/android/camera/manager/ThumbnailManager;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/android/camera/Thumbnail;

    invoke-virtual {p0, p1}, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;->onPostExecute(Lcom/android/camera/Thumbnail;)V

    return-void
.end method
