.class Lcom/android/camera/manager/SettingManager$MyPagerAdapter;
.super Lvedroid/support/v4/view/PagerAdapter;
.source "SettingManager.java"

# interfaces
.implements Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/manager/SettingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyPagerAdapter"
.end annotation


# instance fields
.field private final mPageViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/camera/ui/SettingListLayout;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/camera/manager/SettingManager;


# direct methods
.method public constructor <init>(Lcom/android/camera/manager/SettingManager;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/camera/ui/SettingListLayout;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->this$0:Lcom/android/camera/manager/SettingManager;

    invoke-direct {p0}, Lvedroid/support/v4/view/PagerAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->mPageViews:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public collapse(Z)Z
    .locals 7
    .param p1    # Z

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->mPageViews:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->mPageViews:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/camera/ui/SettingListLayout;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/android/camera/ui/SettingListLayout;->collapseChild()Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/SettingManager;->access$000()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "SettingManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MyPagerAdapter.collapse("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") return "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    invoke-static {}, Lcom/android/camera/manager/SettingManager;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SettingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MyPagerAdapter.destroyItem("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    check-cast p1, Lvedroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->mPageViews:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lvedroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public finishUpdate(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->mPageViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/View;I)Ljava/lang/Object;
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-static {}, Lcom/android/camera/manager/SettingManager;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SettingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MyPagerAdapter.instantiateItem("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    check-cast p1, Lvedroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->mPageViews:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->mPageViews:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/Object;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 3

    invoke-super {p0}, Lvedroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    iget-object v2, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->mPageViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/SettingListLayout;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->this$0:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v1, v2}, Lcom/android/camera/ui/SettingListLayout;->setSettingChangedListener(Lcom/android/camera/ui/SettingListLayout$Listener;)V

    invoke-virtual {v1}, Lcom/android/camera/ui/SettingListLayout;->reloadPreference()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1
    .param p1    # I
    .param p2    # F
    .param p3    # I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->collapse(Z)Z

    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->this$0:Lcom/android/camera/manager/SettingManager;

    invoke-static {v0, p1}, Lcom/android/camera/manager/SettingManager;->access$200(Lcom/android/camera/manager/SettingManager;I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->collapse(Z)Z

    return-void
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
    .param p2    # Ljava/lang/ClassLoader;

    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public startUpdate(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method
