.class Lcom/android/camera/manager/PickImageViewManager$1;
.super Ljava/lang/Object;
.source "PickImageViewManager.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/manager/PickImageViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/manager/PickImageViewManager;


# direct methods
.method constructor <init>(Lcom/android/camera/manager/PickImageViewManager;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/manager/PickImageViewManager$1;->this$0:Lcom/android/camera/manager/PickImageViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-static {}, Lcom/android/camera/manager/PickImageViewManager;->access$000()Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "PickImageViewManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onClick id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    packed-switch v4, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v7, p0, Lcom/android/camera/manager/PickImageViewManager$1;->this$0:Lcom/android/camera/manager/PickImageViewManager;

    invoke-static {v7}, Lcom/android/camera/manager/PickImageViewManager;->access$100(Lcom/android/camera/manager/PickImageViewManager;)I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    iget-object v7, p0, Lcom/android/camera/manager/PickImageViewManager$1;->this$0:Lcom/android/camera/manager/PickImageViewManager;

    invoke-static {v7}, Lcom/android/camera/manager/PickImageViewManager;->access$200(Lcom/android/camera/manager/PickImageViewManager;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/camera/ui/EVPickerItem;

    invoke-virtual {v7}, Landroid/view/View;->isSelected()Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v7, 0x3

    new-array v1, v7, [I

    fill-array-data v1, :array_0

    move-object v0, v1

    array-length v5, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_2

    aget v2, v0, v3

    iget-object v7, p0, Lcom/android/camera/manager/PickImageViewManager$1;->this$0:Lcom/android/camera/manager/PickImageViewManager;

    invoke-static {v7}, Lcom/android/camera/manager/PickImageViewManager;->access$200(Lcom/android/camera/manager/PickImageViewManager;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/camera/ui/EVPickerItem;

    if-eq v2, v4, :cond_1

    invoke-virtual {v6}, Landroid/view/View;->isSelected()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6}, Lcom/android/camera/ui/EVPickerItem;->performClick()Z

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    iget-object v7, p0, Lcom/android/camera/manager/PickImageViewManager$1;->this$0:Lcom/android/camera/manager/PickImageViewManager;

    invoke-static {v7}, Lcom/android/camera/manager/PickImageViewManager;->access$400(Lcom/android/camera/manager/PickImageViewManager;)Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;

    move-result-object v7

    iget-object v8, p0, Lcom/android/camera/manager/PickImageViewManager$1;->this$0:Lcom/android/camera/manager/PickImageViewManager;

    invoke-static {v8}, Lcom/android/camera/manager/PickImageViewManager;->access$300(Lcom/android/camera/manager/PickImageViewManager;)Z

    move-result v8

    invoke-interface {v7, v8}, Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;->onSelectedChanged(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0b0128
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        0x7f0b0128
        0x7f0b012a
        0x7f0b0129
    .end array-data
.end method
