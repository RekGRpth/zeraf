.class Lcom/android/camera/manager/ThumbnailManager$SaveThumbnailTask;
.super Landroid/os/AsyncTask;
.source "ThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/manager/ThumbnailManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveThumbnailTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/android/camera/Thumbnail;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/manager/ThumbnailManager;


# direct methods
.method private constructor <init>(Lcom/android/camera/manager/ThumbnailManager;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/manager/ThumbnailManager$SaveThumbnailTask;->this$0:Lcom/android/camera/manager/ThumbnailManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/camera/manager/ThumbnailManager;Lcom/android/camera/manager/ThumbnailManager$1;)V
    .locals 0
    .param p1    # Lcom/android/camera/manager/ThumbnailManager;
    .param p2    # Lcom/android/camera/manager/ThumbnailManager$1;

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ThumbnailManager$SaveThumbnailTask;-><init>(Lcom/android/camera/manager/ThumbnailManager;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Lcom/android/camera/Thumbnail;

    invoke-virtual {p0, p1}, Lcom/android/camera/manager/ThumbnailManager$SaveThumbnailTask;->doInBackground([Lcom/android/camera/Thumbnail;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/android/camera/Thumbnail;)Ljava/lang/Void;
    .locals 4
    .param p1    # [Lcom/android/camera/Thumbnail;

    array-length v2, p1

    iget-object v3, p0, Lcom/android/camera/manager/ThumbnailManager$SaveThumbnailTask;->this$0:Lcom/android/camera/manager/ThumbnailManager;

    invoke-virtual {v3}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    invoke-virtual {v3, v0}, Lcom/android/camera/Thumbnail;->saveLastThumbnailToFile(Ljava/io/File;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    return-object v3
.end method
