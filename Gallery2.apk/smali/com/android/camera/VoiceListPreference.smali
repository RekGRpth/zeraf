.class public Lcom/android/camera/VoiceListPreference;
.super Lcom/android/camera/IconListPreference;
.source "VoiceListPreference.java"

# interfaces
.implements Lcom/android/camera/VoiceManager$Listener;


# static fields
.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String; = "VoiceListPreference"


# instance fields
.field private mCamera:Lcom/android/camera/Camera;

.field private mDefaultValue:Ljava/lang/String;

.field private mVoiceManager:Lcom/android/camera/VoiceManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/VoiceListPreference;->LOG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/IconListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    check-cast p1, Lcom/android/camera/Camera;

    iput-object p1, p0, Lcom/android/camera/VoiceListPreference;->mCamera:Lcom/android/camera/Camera;

    return-void
.end method

.method private getSupportedDefaultValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mDefaultValue:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/ListPreference;->findSupportedDefaultValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/VoiceListPreference;->mDefaultValue:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mDefaultValue:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v0}, Lcom/android/camera/VoiceManager;->getVoiceValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/camera/VoiceListPreference;->getSupportedDefaultValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    :cond_0
    :goto_0
    sget-boolean v0, Lcom/android/camera/VoiceListPreference;->LOG:Z

    if-eqz v0, :cond_1

    const-string v0, "VoiceListPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getValue() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    return-object v0

    :cond_2
    invoke-direct {p0}, Lcom/android/camera/VoiceListPreference;->getSupportedDefaultValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 3

    sget-boolean v0, Lcom/android/camera/VoiceListPreference;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceListPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isEnabled() mVoiceManager="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v0}, Lcom/android/camera/VoiceManager;->getVoiceValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    invoke-super {p0}, Lcom/android/camera/ListPreference;->isEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public onUserGuideUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onVoiceCommandReceive(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onVoiceValueUpdated(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/VoiceListPreference;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceListPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onVoiceValueUpdated("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/android/camera/VoiceListPreference;->getSupportedDefaultValue()Ljava/lang/String;

    move-result-object p1

    :cond_1
    iget-object v0, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    if-nez v0, :cond_2

    if-eqz p1, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    iput-object p1, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getSettingManager()Lcom/android/camera/manager/SettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    :cond_4
    return-void
.end method

.method protected persistStringValue(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/VoiceListPreference;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceListPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "persistStringValue("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mVoiceManager="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v0, p1}, Lcom/android/camera/VoiceManager;->setVoiceValue(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/VoiceListPreference;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceListPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setValue("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mValue="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/camera/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iput-object p1, p0, Lcom/android/camera/ListPreference;->mValue:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/camera/VoiceListPreference;->persistStringValue(Ljava/lang/String;)V

    return-void
.end method

.method public setVoiceManager(Lcom/android/camera/VoiceManager;)V
    .locals 3
    .param p1    # Lcom/android/camera/VoiceManager;

    sget-boolean v0, Lcom/android/camera/VoiceListPreference;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceListPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVoiceManager("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mVoiceManager="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    if-eq v0, p1, :cond_2

    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v0, p0}, Lcom/android/camera/VoiceManager;->removeListener(Lcom/android/camera/VoiceManager$Listener;)Z

    :cond_1
    iput-object p1, p0, Lcom/android/camera/VoiceListPreference;->mVoiceManager:Lcom/android/camera/VoiceManager;

    if-eqz p1, :cond_2

    invoke-virtual {p1, p0}, Lcom/android/camera/VoiceManager;->addListener(Lcom/android/camera/VoiceManager$Listener;)Z

    :cond_2
    return-void
.end method
