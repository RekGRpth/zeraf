.class public Lcom/android/camera/ui/SettingListLayout;
.super Landroid/widget/FrameLayout;
.source "SettingListLayout.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/android/camera/ui/InLineSettingItem$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ui/SettingListLayout$SettingsListAdapter;,
        Lcom/android/camera/ui/SettingListLayout$Listener;
    }
.end annotation


# static fields
.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String; = "SettingListLayout"


# instance fields
.field private mLastItem:Lcom/android/camera/ui/InLineSettingItem;

.field private mListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/camera/ListPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mListItemAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/camera/ListPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/android/camera/ui/SettingListLayout$Listener;

.field private mSettingList:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/ui/SettingListLayout;->LOG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mListItem:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/ui/SettingListLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/camera/ui/SettingListLayout;

    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mListItem:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/camera/ui/SettingListLayout;Lcom/android/camera/ListPreference;)Z
    .locals 1
    .param p0    # Lcom/android/camera/ui/SettingListLayout;
    .param p1    # Lcom/android/camera/ListPreference;

    invoke-direct {p0, p1}, Lcom/android/camera/ui/SettingListLayout;->isSwitchSettingItem(Lcom/android/camera/ListPreference;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/android/camera/ui/SettingListLayout;Lcom/android/camera/ListPreference;)Z
    .locals 1
    .param p0    # Lcom/android/camera/ui/SettingListLayout;
    .param p1    # Lcom/android/camera/ListPreference;

    invoke-direct {p0, p1}, Lcom/android/camera/ui/SettingListLayout;->isVirtualSettingItem(Lcom/android/camera/ListPreference;)Z

    move-result v0

    return v0
.end method

.method private isSwitchSettingItem(Lcom/android/camera/ListPreference;)Z
    .locals 2
    .param p1    # Lcom/android/camera/ListPreference;

    const-string v0, "pref_camera_recordlocation_key"

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "pref_camera_recordaudio_key"

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "pref_video_eis_key"

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "pref_camera_zsd_key"

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "pref_voice_key"

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "pref_face_detect_key"

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isVirtualSettingItem(Lcom/android/camera/ListPreference;)Z
    .locals 2
    .param p1    # Lcom/android/camera/ListPreference;

    const-string v0, "pref_camera_image_properties_key"

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "pref_camera_facebeauty_properties_key"

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public collapseChild()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    invoke-virtual {v1}, Lcom/android/camera/ui/InLineSettingItem;->collapseChild()Z

    move-result v0

    :cond_0
    sget-boolean v1, Lcom/android/camera/ui/SettingListLayout;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "SettingListLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "collapseChild() return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0
.end method

.method public initialize([Ljava/lang/String;Z)V
    .locals 5
    .param p1    # [Ljava/lang/String;
    .param p2    # Z

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/camera/Camera;

    iget-object v3, p0, Lcom/android/camera/ui/SettingListLayout;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    const/4 v1, 0x0

    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_1

    aget-object v3, p1, v1

    invoke-virtual {v0, v3}, Lcom/android/camera/Camera;->getListPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/android/camera/ui/SettingListLayout;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    iget-object v3, p0, Lcom/android/camera/ui/SettingListLayout;->mListItem:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    new-instance v3, Lcom/android/camera/ui/SettingListLayout$SettingsListAdapter;

    invoke-direct {v3, p0}, Lcom/android/camera/ui/SettingListLayout$SettingsListAdapter;-><init>(Lcom/android/camera/ui/SettingListLayout;)V

    iput-object v3, p0, Lcom/android/camera/ui/SettingListLayout;->mListItemAdapter:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/android/camera/ui/SettingListLayout;->mSettingList:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/android/camera/ui/SettingListLayout;->mListItemAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v3, p0, Lcom/android/camera/ui/SettingListLayout;->mSettingList:Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v3, p0, Lcom/android/camera/ui/SettingListLayout;->mSettingList:Landroid/widget/ListView;

    const v4, 0x106000d

    invoke-virtual {v3, v4}, Landroid/widget/AbsListView;->setSelector(I)V

    iget-object v3, p0, Lcom/android/camera/ui/SettingListLayout;->mSettingList:Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method public onDismiss(Lcom/android/camera/ui/InLineSettingItem;)V
    .locals 3
    .param p1    # Lcom/android/camera/ui/InLineSettingItem;

    const-string v0, "SettingListLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDismiss("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mLastItem="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0b0130

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mSettingList:Landroid/widget/ListView;

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mListener:Lcom/android/camera/ui/SettingListLayout$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mListener:Lcom/android/camera/ui/SettingListLayout$Listener;

    invoke-interface {v0}, Lcom/android/camera/ui/SettingListLayout$Listener;->onRestorePreferencesClicked()V

    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 3
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sget-boolean v0, Lcom/android/camera/ui/SettingListLayout;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingListLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScroll("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    sget-boolean v0, Lcom/android/camera/ui/SettingListLayout;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingListLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScrollStateChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/ui/SettingListLayout;->collapseChild()Z

    :cond_1
    return-void
.end method

.method public onSettingChanged(Lcom/android/camera/ui/InLineSettingItem;)V
    .locals 1
    .param p1    # Lcom/android/camera/ui/InLineSettingItem;

    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    invoke-virtual {v0}, Lcom/android/camera/ui/InLineSettingItem;->collapseChild()Z

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mListener:Lcom/android/camera/ui/SettingListLayout$Listener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mListener:Lcom/android/camera/ui/SettingListLayout$Listener;

    invoke-interface {v0, p0}, Lcom/android/camera/ui/SettingListLayout$Listener;->onSettingChanged(Lcom/android/camera/ui/SettingListLayout;)V

    :cond_1
    return-void
.end method

.method public onShow(Lcom/android/camera/ui/InLineSettingItem;)V
    .locals 3
    .param p1    # Lcom/android/camera/ui/InLineSettingItem;

    sget-boolean v0, Lcom/android/camera/ui/SettingListLayout;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingListLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onShow("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mLastItem="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    invoke-virtual {v0}, Lcom/android/camera/ui/InLineSettingItem;->collapseChild()Z

    :cond_1
    iput-object p1, p0, Lcom/android/camera/ui/SettingListLayout;->mLastItem:Lcom/android/camera/ui/InLineSettingItem;

    return-void
.end method

.method public reloadPreference()V
    .locals 5

    iget-object v4, p0, Lcom/android/camera/ui/SettingListLayout;->mSettingList:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v4, p0, Lcom/android/camera/ui/SettingListLayout;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/camera/ListPreference;

    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/android/camera/ui/SettingListLayout;->mSettingList:Landroid/widget/ListView;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/InLineSettingItem;

    invoke-virtual {v3}, Lcom/android/camera/ui/InLineSettingItem;->reloadPreference()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setSettingChangedListener(Lcom/android/camera/ui/SettingListLayout$Listener;)V
    .locals 0
    .param p1    # Lcom/android/camera/ui/SettingListLayout$Listener;

    iput-object p1, p0, Lcom/android/camera/ui/SettingListLayout;->mListener:Lcom/android/camera/ui/SettingListLayout$Listener;

    return-void
.end method
