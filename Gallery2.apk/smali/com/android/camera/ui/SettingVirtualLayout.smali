.class public Lcom/android/camera/ui/SettingVirtualLayout;
.super Lcom/android/camera/ui/RotateLayout;
.source "SettingVirtualLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ui/SettingVirtualLayout$1;,
        Lcom/android/camera/ui/SettingVirtualLayout$ViewHolder;,
        Lcom/android/camera/ui/SettingVirtualLayout$MyAdapter;,
        Lcom/android/camera/ui/SettingVirtualLayout$Listener;
    }
.end annotation


# static fields
.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String; = "SettingVirtualLayout"


# instance fields
.field private mAdapter:Lcom/android/camera/ui/SettingVirtualLayout$MyAdapter;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mListener:Lcom/android/camera/ui/SettingVirtualLayout$Listener;

.field private mPrefs:[Lcom/android/camera/ListPreference;

.field private mSettingList:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/ui/SettingVirtualLayout;->LOG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/RotateLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/ui/SettingVirtualLayout;)[Lcom/android/camera/ListPreference;
    .locals 1
    .param p0    # Lcom/android/camera/ui/SettingVirtualLayout;

    iget-object v0, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mPrefs:[Lcom/android/camera/ListPreference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/camera/ui/SettingVirtualLayout;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0    # Lcom/android/camera/ui/SettingVirtualLayout;

    iget-object v0, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method public initialize([Lcom/android/camera/ListPreference;)V
    .locals 4
    .param p1    # [Lcom/android/camera/ListPreference;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    array-length v1, p1

    new-array v2, v1, [Lcom/android/camera/ListPreference;

    iput-object v2, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mPrefs:[Lcom/android/camera/ListPreference;

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mPrefs:[Lcom/android/camera/ListPreference;

    aget-object v3, p1, v0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v2, Lcom/android/camera/ui/SettingVirtualLayout$MyAdapter;

    invoke-direct {v2, p0}, Lcom/android/camera/ui/SettingVirtualLayout$MyAdapter;-><init>(Lcom/android/camera/ui/SettingVirtualLayout;)V

    iput-object v2, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mAdapter:Lcom/android/camera/ui/SettingVirtualLayout$MyAdapter;

    iget-object v2, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mSettingList:Landroid/view/ViewGroup;

    check-cast v2, Landroid/widget/AbsListView;

    iget-object v3, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mAdapter:Lcom/android/camera/ui/SettingVirtualLayout$MyAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/android/camera/ui/SettingVirtualLayout;->reloadPreference()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1    # Landroid/view/View;

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/SettingVirtualLayout$ViewHolder;

    iget-object v3, v1, Lcom/android/camera/ui/SettingVirtualLayout$ViewHolder;->mPref:Lcom/android/camera/ListPreference;

    sget-boolean v5, Lcom/android/camera/ui/SettingVirtualLayout;->LOG:Z

    if-eqz v5, :cond_0

    const-string v5, "SettingVirtualLayout"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onClick("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") pref="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez v3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v4, v1, Lcom/android/camera/ui/SettingVirtualLayout$ViewHolder;->mRadio1:Landroid/widget/RadioButton;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :goto_1
    new-array v0, v11, [Landroid/widget/RadioButton;

    iget-object v5, v1, Lcom/android/camera/ui/SettingVirtualLayout$ViewHolder;->mRadio1:Landroid/widget/RadioButton;

    aput-object v5, v0, v8

    iget-object v5, v1, Lcom/android/camera/ui/SettingVirtualLayout$ViewHolder;->mRadio2:Landroid/widget/RadioButton;

    aput-object v5, v0, v9

    iget-object v5, v1, Lcom/android/camera/ui/SettingVirtualLayout$ViewHolder;->mRadio3:Landroid/widget/RadioButton;

    aput-object v5, v0, v10

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v11, :cond_4

    aget-object v5, v0, v2

    if-eq v4, v5, :cond_3

    aget-object v5, v0, v2

    invoke-virtual {v5, v8}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :sswitch_0
    invoke-virtual {v3, v8}, Lcom/android/camera/ListPreference;->setValueIndex(I)V

    iget-object v4, v1, Lcom/android/camera/ui/SettingVirtualLayout$ViewHolder;->mRadio1:Landroid/widget/RadioButton;

    goto :goto_1

    :sswitch_1
    invoke-virtual {v3, v9}, Lcom/android/camera/ListPreference;->setValueIndex(I)V

    iget-object v4, v1, Lcom/android/camera/ui/SettingVirtualLayout$ViewHolder;->mRadio2:Landroid/widget/RadioButton;

    goto :goto_1

    :sswitch_2
    invoke-virtual {v3, v10}, Lcom/android/camera/ListPreference;->setValueIndex(I)V

    iget-object v4, v1, Lcom/android/camera/ui/SettingVirtualLayout$ViewHolder;->mRadio3:Landroid/widget/RadioButton;

    goto :goto_1

    :cond_4
    iget-object v5, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mListener:Lcom/android/camera/ui/SettingVirtualLayout$Listener;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mListener:Lcom/android/camera/ui/SettingVirtualLayout$Listener;

    invoke-interface {v5}, Lcom/android/camera/ui/SettingVirtualLayout$Listener;->onSettingChanged()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0134 -> :sswitch_0
        0x7f0b0137 -> :sswitch_1
        0x7f0b013a -> :sswitch_2
    .end sparse-switch
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/android/camera/ui/RotateLayout;->onFinishInflate()V

    const v0, 0x7f0b0130

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mSettingList:Landroid/view/ViewGroup;

    return-void
.end method

.method public reloadPreference()V
    .locals 3

    iget-object v2, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mPrefs:[Lcom/android/camera/ListPreference;

    array-length v1, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mPrefs:[Lcom/android/camera/ListPreference;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mPrefs:[Lcom/android/camera/ListPreference;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/android/camera/ListPreference;->reloadValue()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mAdapter:Lcom/android/camera/ui/SettingVirtualLayout$MyAdapter;

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setSettingChangedListener(Lcom/android/camera/ui/SettingVirtualLayout$Listener;)V
    .locals 0
    .param p1    # Lcom/android/camera/ui/SettingVirtualLayout$Listener;

    iput-object p1, p0, Lcom/android/camera/ui/SettingVirtualLayout;->mListener:Lcom/android/camera/ui/SettingVirtualLayout$Listener;

    return-void
.end method
