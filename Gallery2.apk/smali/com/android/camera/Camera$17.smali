.class Lcom/android/camera/Camera$17;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/camera/Camera;->doShowRemaining(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/Camera;

.field final synthetic val$showAways:Z


# direct methods
.method constructor <init>(Lcom/android/camera/Camera;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/Camera$17;->this$0:Lcom/android/camera/Camera;

    iput-boolean p2, p0, Lcom/android/camera/Camera$17;->val$showAways:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/camera/Camera$17;->val$showAways:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/Camera$17;->this$0:Lcom/android/camera/Camera;

    invoke-static {v1}, Lcom/android/camera/Camera;->access$5300(Lcom/android/camera/Camera;)Lcom/android/camera/manager/RemainingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/RemainingManager;->showAways()Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/camera/Camera$17;->this$0:Lcom/android/camera/Camera;

    invoke-static {v1}, Lcom/android/camera/Camera;->access$5600(Lcom/android/camera/Camera;)Lcom/android/camera/manager/IndicatorManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v1, p0, Lcom/android/camera/Camera$17;->this$0:Lcom/android/camera/Camera;

    invoke-static {v1}, Lcom/android/camera/Camera;->access$5800(Lcom/android/camera/Camera;)Lcom/android/camera/manager/InfoManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v1, p0, Lcom/android/camera/Camera$17;->this$0:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isNormalViewState()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/Camera$17;->this$0:Lcom/android/camera/Camera;

    invoke-static {v1}, Lcom/android/camera/Camera;->access$5700(Lcom/android/camera/Camera;)Lcom/android/camera/manager/PickerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/ViewManager;->show()V

    :cond_0
    iget-object v1, p0, Lcom/android/camera/Camera$17;->this$0:Lcom/android/camera/Camera;

    const/16 v2, 0xbb8

    invoke-static {v1, v2}, Lcom/android/camera/Camera;->access$5900(Lcom/android/camera/Camera;I)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/camera/Camera$17;->this$0:Lcom/android/camera/Camera;

    invoke-static {v1}, Lcom/android/camera/Camera;->access$5300(Lcom/android/camera/Camera;)Lcom/android/camera/manager/RemainingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/RemainingManager;->showIfNeed()Z

    move-result v0

    goto :goto_0
.end method
