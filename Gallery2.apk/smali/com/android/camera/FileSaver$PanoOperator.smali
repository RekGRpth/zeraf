.class Lcom/android/camera/FileSaver$PanoOperator;
.super Lcom/android/camera/FileSaver$RequestOperator;
.source "FileSaver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/FileSaver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PanoOperator"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/FileSaver;


# direct methods
.method private constructor <init>(Lcom/android/camera/FileSaver;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/android/camera/FileSaver$PanoOperator;->this$0:Lcom/android/camera/FileSaver;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/camera/FileSaver$RequestOperator;-><init>(Lcom/android/camera/FileSaver;Lcom/android/camera/FileSaver$1;)V

    iput p2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTempPictureType:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/camera/FileSaver;ILcom/android/camera/FileSaver$1;)V
    .locals 0
    .param p1    # Lcom/android/camera/FileSaver;
    .param p2    # I
    .param p3    # Lcom/android/camera/FileSaver$1;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/FileSaver$PanoOperator;-><init>(Lcom/android/camera/FileSaver;I)V

    return-void
.end method


# virtual methods
.method public addRequest()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/FileSaver$PanoOperator;->this$0:Lcom/android/camera/FileSaver;

    invoke-static {v0, p0}, Lcom/android/camera/FileSaver;->access$700(Lcom/android/camera/FileSaver;Lcom/android/camera/SaveRequest;)V

    return-void
.end method

.method public createThumbnail(I)Lcom/android/camera/Thumbnail;
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/camera/FileSaver$RequestOperator;->mUri:Landroid/net/Uri;

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/android/camera/FileSaver$RequestOperator;->mWidth:I

    int-to-double v4, v4

    iget-object v6, p0, Lcom/android/camera/FileSaver$PanoOperator;->this$0:Lcom/android/camera/FileSaver;

    invoke-static {v6}, Lcom/android/camera/FileSaver;->access$500(Lcom/android/camera/FileSaver;)Lcom/android/camera/Camera;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/camera/Camera;->getPreviewFrameWidth()I

    move-result v6

    int-to-double v6, v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    iget v4, p0, Lcom/android/camera/FileSaver$RequestOperator;->mWidth:I

    int-to-double v4, v4

    iget-object v6, p0, Lcom/android/camera/FileSaver$PanoOperator;->this$0:Lcom/android/camera/FileSaver;

    invoke-static {v6}, Lcom/android/camera/FileSaver;->access$500(Lcom/android/camera/FileSaver;)Lcom/android/camera/Camera;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/camera/Camera;->getPreviewFrameHeight()I

    move-result v6

    int-to-double v6, v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v1

    iget-object v4, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    iget v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mOrientation:I

    iget-object v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mUri:Landroid/net/Uri;

    iget v7, p0, Lcom/android/camera/FileSaver$RequestOperator;->mStereoType:I

    invoke-static {v4, v5, v1, v6, v7}, Lcom/android/camera/Thumbnail;->createThumbnail(Ljava/lang/String;IILandroid/net/Uri;I)Lcom/android/camera/Thumbnail;

    move-result-object v2

    :cond_0
    invoke-static {}, Lcom/android/camera/FileSaver;->access$300()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "FileSaver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createThumbnail("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") mOrientation="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mOrientation:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mUri="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mFilePath="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", return "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v2
.end method

.method public prepareRequest()V
    .locals 5

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFileType:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDateTaken:J

    iget-object v1, p0, Lcom/android/camera/FileSaver$PanoOperator;->this$0:Lcom/android/camera/FileSaver;

    invoke-static {v1}, Lcom/android/camera/FileSaver;->access$500(Lcom/android/camera/FileSaver;)Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getLocationManager()Lcom/android/camera/LocationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/LocationManager;->getCurrentLocation()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mLocation:Landroid/location/Location;

    :cond_0
    iget-object v1, p0, Lcom/android/camera/FileSaver$PanoOperator;->this$0:Lcom/android/camera/FileSaver;

    iget v2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFileType:I

    iget-wide v3, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDateTaken:J

    invoke-static {v1, v2, v3, v4}, Lcom/android/camera/FileSaver;->access$800(Lcom/android/camera/FileSaver;IJ)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTitle:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTitle:Ljava/lang/String;

    iget v2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTempPictureType:I

    invoke-static {v1, v2}, Lcom/android/camera/Storage;->generateFileName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDisplayName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDisplayName:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/camera/Storage;->generateFilepath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".tmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTempFilePath:Ljava/lang/String;

    return-void
.end method

.method public saveRequest()V
    .locals 10

    new-instance v4, Ljava/io/File;

    iget-object v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTempFilePath:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/android/camera/FileSaver$RequestOperator;->setIgnoreThumbnail(Z)V

    const-string v6, "FileSaver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bad file created by native layer, delete it! "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mDataSize:J

    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    iget-object v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mFilePath:Ljava/lang/String;

    invoke-direct {v1, v6}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/android/camera/Util;->getExifOrientation(Landroid/media/ExifInterface;)I

    move-result v3

    const-string v6, "ImageWidth"

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v5

    const-string v6, "ImageLength"

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v2

    iput v5, p0, Lcom/android/camera/FileSaver$RequestOperator;->mWidth:I

    iput v2, p0, Lcom/android/camera/FileSaver$RequestOperator;->mHeight:I

    iput v3, p0, Lcom/android/camera/FileSaver$RequestOperator;->mOrientation:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTitle:Ljava/lang/String;

    iget v7, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTempPictureType:I

    invoke-static {v6, v7}, Lcom/android/camera/Storage;->generateMimetype(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mMimeType:Ljava/lang/String;

    iget v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mTempPictureType:I

    invoke-static {v6}, Lcom/android/camera/Storage;->generateMpoType(I)I

    move-result v6

    iput v6, p0, Lcom/android/camera/FileSaver$RequestOperator;->mMpoType:I

    invoke-virtual {p0, p0}, Lcom/android/camera/FileSaver$RequestOperator;->saveImageToDatabase(Lcom/android/camera/FileSaver$RequestOperator;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v6, "FileSaver"

    const-string v7, "cannot read exif"

    invoke-static {v6, v7, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
