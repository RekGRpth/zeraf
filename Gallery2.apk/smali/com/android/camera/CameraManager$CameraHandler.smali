.class Lcom/android/camera/CameraManager$CameraHandler;
.super Landroid/os/Handler;
.source "CameraManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/CameraManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CameraHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/CameraManager;


# direct methods
.method constructor <init>(Lcom/android/camera/CameraManager;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v5, 0x0

    invoke-static {}, Lcom/android/camera/CameraManager;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "CameraManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget v2, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    sparse-switch v2, :sswitch_data_0

    :goto_0
    :sswitch_0
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$500(Lcom/android/camera/CameraManager;)V

    :goto_1
    return-void

    :sswitch_1
    :try_start_1
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraRelease()V

    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->release()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraRelease()V

    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/camera/CameraManager;->access$102(Lcom/android/camera/CameraManager;Lcom/mediatek/camera/ICamera;)Lcom/mediatek/camera/ICamera;

    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/camera/CameraManager;->access$202(Lcom/android/camera/CameraManager;Lcom/android/camera/CameraManager$CameraProxy;)Lcom/android/camera/CameraManager$CameraProxy;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget v2, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    if-eqz v2, :cond_1

    :try_start_2
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->release()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2, v5}, Lcom/android/camera/CameraManager;->access$102(Lcom/android/camera/CameraManager;Lcom/mediatek/camera/ICamera;)Lcom/mediatek/camera/ICamera;

    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2, v5}, Lcom/android/camera/CameraManager;->access$202(Lcom/android/camera/CameraManager;Lcom/android/camera/CameraManager$CameraProxy;)Lcom/android/camera/CameraManager$CameraProxy;

    :cond_1
    throw v0

    :sswitch_2
    :try_start_3
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/camera/CameraManager;->access$302(Lcom/android/camera/CameraManager;Ljava/io/IOException;)Ljava/io/IOException;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->reconnect()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_5
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2, v1}, Lcom/android/camera/CameraManager;->access$302(Lcom/android/camera/CameraManager;Ljava/io/IOException;)Ljava/io/IOException;

    goto :goto_0

    :sswitch_3
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->unlock()V

    goto :goto_0

    :sswitch_4
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->lock()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_0

    :sswitch_5
    :try_start_6
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/graphics/SurfaceTexture;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_7
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :sswitch_6
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileStartPreview()V

    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->startPreview()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileStartPreview()V

    goto/16 :goto_1

    :sswitch_7
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileStopPreview()V

    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->stopPreview()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileStopPreview()V

    goto/16 :goto_0

    :sswitch_8
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$PreviewCallback;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->addCallbackBuffer([B)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$AutoFocusCallback;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->cancelAutoFocus()V

    goto/16 :goto_0

    :sswitch_c
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$AutoFocusMoveCallback;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/mediatek/camera/ICamera;->setDisplayOrientation(I)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$OnZoomChangeListener;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setZoomChangeListener(Landroid/hardware/Camera$OnZoomChangeListener;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$FaceDetectionListener;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setFaceDetectionListener(Landroid/hardware/Camera$FaceDetectionListener;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->startFaceDetection()V

    goto/16 :goto_0

    :sswitch_11
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->stopFaceDetection()V

    goto/16 :goto_0

    :sswitch_12
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$ErrorCallback;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    goto/16 :goto_0

    :sswitch_13
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileSetParameters()V

    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$Parameters;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileSetParameters()V

    goto/16 :goto_0

    :sswitch_14
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileGetParameters()V

    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    iget-object v3, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v3}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    invoke-interface {v3}, Lcom/mediatek/camera/ICamera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/CameraManager;->access$402(Lcom/android/camera/CameraManager;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileGetParameters()V

    goto/16 :goto_0

    :sswitch_15
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileSetParameters()V

    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$Parameters;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileSetParameters()V

    goto/16 :goto_1

    :sswitch_16
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/mediatek/camera/ICamera;->startSmoothZoom(I)V

    goto/16 :goto_0

    :sswitch_17
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$AUTORAMACallback;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setAUTORAMACallback(Landroid/hardware/Camera$AUTORAMACallback;)V

    goto/16 :goto_0

    :sswitch_18
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$AUTORAMAMVCallback;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setAUTORAMAMVCallback(Landroid/hardware/Camera$AUTORAMAMVCallback;)V

    goto/16 :goto_0

    :sswitch_19
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/mediatek/camera/ICamera;->startAUTORAMA(I)V

    goto/16 :goto_0

    :sswitch_1a
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/mediatek/camera/ICamera;->stopAUTORAMA(I)V

    goto/16 :goto_0

    :sswitch_1b
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$MAVCallback;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setMAVCallback(Landroid/hardware/Camera$MAVCallback;)V

    goto/16 :goto_0

    :sswitch_1c
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/mediatek/camera/ICamera;->startMAV(I)V

    goto/16 :goto_0

    :sswitch_1d
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/mediatek/camera/ICamera;->stopMAV(I)V

    goto/16 :goto_0

    :sswitch_1e
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$ASDCallback;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setASDCallback(Landroid/hardware/Camera$ASDCallback;)V

    goto/16 :goto_0

    :sswitch_1f
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$SmileCallback;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V

    goto/16 :goto_0

    :sswitch_20
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->startSDPreview()V

    goto/16 :goto_0

    :sswitch_21
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->cancelSDPreview()V

    goto/16 :goto_0

    :sswitch_22
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/camera/ICamera;->cancelContinuousShot()V

    goto/16 :goto_0

    :sswitch_23
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/mediatek/camera/ICamera;->setContinuousShotSpeed(I)V

    goto/16 :goto_0

    :sswitch_24
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$ZSDPreviewDone;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setPreviewDoneCallback(Landroid/hardware/Camera$ZSDPreviewDone;)V

    goto/16 :goto_0

    :sswitch_25
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/hardware/Camera$ContinuousShotDone;

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->setCSDoneCallback(Landroid/hardware/Camera$ContinuousShotDone;)V

    goto/16 :goto_0

    :sswitch_26
    iget-object v2, p0, Lcom/android/camera/CameraManager$CameraHandler;->this$0:Lcom/android/camera/CameraManager;

    invoke-static {v2}, Lcom/android/camera/CameraManager;->access$100(Lcom/android/camera/CameraManager;)Lcom/mediatek/camera/ICamera;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/mediatek/camera/ICamera;->addRawImageCallbackBuffer([B)V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_0

    :catch_3
    move-exception v1

    const-string v2, "CameraManager"

    const-string v3, "Fail to release the camera."

    invoke-static {v2, v3}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_0
        0x64 -> :sswitch_16
        0x65 -> :sswitch_17
        0x66 -> :sswitch_18
        0x67 -> :sswitch_19
        0x68 -> :sswitch_1a
        0x69 -> :sswitch_1b
        0x6a -> :sswitch_1c
        0x6b -> :sswitch_1d
        0x6c -> :sswitch_1e
        0x6d -> :sswitch_1f
        0x6e -> :sswitch_20
        0x6f -> :sswitch_21
        0x70 -> :sswitch_22
        0x71 -> :sswitch_23
        0x72 -> :sswitch_24
        0x73 -> :sswitch_25
        0x74 -> :sswitch_26
    .end sparse-switch
.end method
