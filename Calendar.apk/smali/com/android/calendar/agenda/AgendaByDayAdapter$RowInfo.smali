.class Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;
.super Ljava/lang/Object;
.source "AgendaByDayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/agenda/AgendaByDayAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RowInfo"
.end annotation


# instance fields
.field final mAllDay:Z

.field final mDay:I

.field final mEventEndTimeMilli:J

.field final mEventId:J

.field final mEventStartTimeMilli:J

.field mFirstDayAfterYesterday:Z

.field final mInstanceId:J

.field final mPosition:I

.field final mType:I


# direct methods
.method constructor <init>(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mType:I

    iput p2, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mDay:I

    iput v2, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mPosition:I

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mEventId:J

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mEventStartTimeMilli:J

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mEventEndTimeMilli:J

    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mFirstDayAfterYesterday:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mInstanceId:J

    iput-boolean v2, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mAllDay:Z

    return-void
.end method

.method constructor <init>(IIIJJJJZ)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # J
    .param p6    # J
    .param p8    # J
    .param p10    # J
    .param p12    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mType:I

    iput p2, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mDay:I

    iput p3, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mPosition:I

    iput-wide p4, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mEventId:J

    iput-wide p6, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mEventStartTimeMilli:J

    iput-wide p8, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mEventEndTimeMilli:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mFirstDayAfterYesterday:Z

    iput-wide p10, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mInstanceId:J

    iput-boolean p12, p0, Lcom/android/calendar/agenda/AgendaByDayAdapter$RowInfo;->mAllDay:Z

    return-void
.end method
