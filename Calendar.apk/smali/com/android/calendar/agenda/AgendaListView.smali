.class public Lcom/android/calendar/agenda/AgendaListView;
.super Landroid/widget/ListView;
.source "AgendaListView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final DEBUG:Z = false

.field private static final EVENT_UPDATE_TIME:I = 0x493e0

.field private static final TAG:Ljava/lang/String; = "AgendaListView"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeleteEventHelper:Lcom/android/calendar/DeleteEventHelper;

.field private mHandler:Landroid/os/Handler;

.field private final mMidnightUpdater:Ljava/lang/Runnable;

.field private final mPastEventUpdater:Ljava/lang/Runnable;

.field private mShowEventDetailsWithAgenda:Z

.field private final mTZUpdater:Ljava/lang/Runnable;

.field private mTime:Landroid/text/format/Time;

.field private mTimeZone:Ljava/lang/String;

.field private mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/calendar/agenda/AgendaListView$1;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/AgendaListView$1;-><init>(Lcom/android/calendar/agenda/AgendaListView;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mTZUpdater:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/calendar/agenda/AgendaListView$2;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/AgendaListView$2;-><init>(Lcom/android/calendar/agenda/AgendaListView;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mMidnightUpdater:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/calendar/agenda/AgendaListView$3;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/AgendaListView$3;-><init>(Lcom/android/calendar/agenda/AgendaListView;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mPastEventUpdater:Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaListView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/calendar/agenda/AgendaListView;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaListView;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/calendar/agenda/AgendaListView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaListView;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaListView;->mTimeZone:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/calendar/agenda/AgendaListView;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaListView;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/calendar/agenda/AgendaListView;)Landroid/text/format/Time;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaListView;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/calendar/agenda/AgendaListView;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaListView;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/calendar/agenda/AgendaListView;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaListView;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mMidnightUpdater:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/calendar/agenda/AgendaListView;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/agenda/AgendaListView;

    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->updatePastEvents()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/calendar/agenda/AgendaListView;)V
    .locals 0
    .param p0    # Lcom/android/calendar/agenda/AgendaListView;

    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->setPastEventsUpdater()V

    return-void
.end method

.method private initView(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x0

    const v4, 0x7f090003

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/android/calendar/agenda/AgendaListView;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mTZUpdater:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mTimeZone:Ljava/lang/String;

    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->mTimeZone:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    invoke-virtual {p0, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0, v3}, Landroid/view/View;->setVerticalScrollBarEnabled(Z)V

    instance-of v0, p1, Lcom/mediatek/calendar/selectevent/AgendaChoiceActivity;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-direct {v0, p1, p0, v3}, Lcom/android/calendar/agenda/AgendaWindowAdapter;-><init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;Z)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    :goto_0
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->setSelectedInstanceId(J)V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setCacheColorHint(I)V

    new-instance v0, Lcom/android/calendar/DeleteEventHelper;

    invoke-direct {v0, p1, v5, v3}, Lcom/android/calendar/DeleteEventHelper;-><init>(Landroid/content/Context;Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mDeleteEventHelper:Lcom/android/calendar/DeleteEventHelper;

    instance-of v0, p1, Lcom/mediatek/calendar/selectevent/AgendaChoiceActivity;

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lcom/android/calendar/agenda/AgendaListView;->mShowEventDetailsWithAgenda:Z

    :goto_1
    invoke-virtual {p0, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mHandler:Landroid/os/Handler;

    return-void

    :cond_0
    new-instance v0, Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-static {p1, v4}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v1

    invoke-direct {v0, p1, p0, v1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;-><init>(Landroid/content/Context;Lcom/android/calendar/agenda/AgendaListView;Z)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mContext:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mShowEventDetailsWithAgenda:Z

    goto :goto_1
.end method

.method private resetPastEventsUpdater()V
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->mPastEventUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method private setPastEventsUpdater()V
    .locals 10

    const-wide/32 v8, 0x493e0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    div-long v4, v0, v8

    mul-long v2, v4, v8

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaListView;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/calendar/agenda/AgendaListView;->mPastEventUpdater:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaListView;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/calendar/agenda/AgendaListView;->mPastEventUpdater:Ljava/lang/Runnable;

    sub-long v6, v0, v2

    sub-long v6, v8, v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private shiftPosition(I)V
    .locals 5
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    invoke-virtual {p0, v0}, Landroid/widget/AdapterView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    add-int v4, v1, p1

    iget v3, v2, Landroid/graphics/Rect;->top:I

    if-lez v3, :cond_1

    iget v3, v2, Landroid/graphics/Rect;->top:I

    neg-int v3, v3

    :goto_0
    invoke-virtual {p0, v4, v3}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v3, v2, Landroid/graphics/Rect;->top:I

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v3

    if-ltz v3, :cond_0

    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v3

    add-int/2addr v3, p1

    invoke-virtual {p0, v3}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_1
.end method

.method private updatePastEvents()Z
    .locals 12

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    new-instance v8, Landroid/text/format/Time;

    iget-object v10, p0, Lcom/android/calendar/agenda/AgendaListView;->mTimeZone:Ljava/lang/String;

    invoke-direct {v8, v10}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5, v6}, Landroid/text/format/Time;->set(J)V

    iget-wide v10, v8, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v5, v6, v10, v11}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v9

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    instance-of v10, v7, Lcom/android/calendar/agenda/AgendaByDayAdapter$ViewHolder;

    if-eqz v10, :cond_1

    move-object v1, v7

    check-cast v1, Lcom/android/calendar/agenda/AgendaByDayAdapter$ViewHolder;

    iget v10, v1, Lcom/android/calendar/agenda/AgendaByDayAdapter$ViewHolder;->julianDay:I

    if-gt v10, v9, :cond_4

    iget-boolean v10, v1, Lcom/android/calendar/agenda/AgendaByDayAdapter$ViewHolder;->grayed:Z

    if-nez v10, :cond_4

    const/4 v4, 0x1

    :cond_0
    :goto_1
    return v4

    :cond_1
    instance-of v10, v7, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    if-eqz v10, :cond_4

    move-object v1, v7

    check-cast v1, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    iget-boolean v10, v1, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->grayed:Z

    if-nez v10, :cond_4

    iget-boolean v10, v1, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->allDay:Z

    if-nez v10, :cond_2

    iget-wide v10, v1, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->startTimeMilli:J

    cmp-long v10, v10, v5

    if-lez v10, :cond_3

    :cond_2
    iget-boolean v10, v1, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->allDay:Z

    if-eqz v10, :cond_4

    iget v10, v1, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->julianDay:I

    if-gt v10, v9, :cond_4

    :cond_3
    const/4 v4, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public deleteSelectedEvent()V
    .locals 10

    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v9

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v0, v9}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getEventByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v8

    if-eqz v8, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mDeleteEventHelper:Lcom/android/calendar/DeleteEventHelper;

    iget-wide v1, v8, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    iget-wide v3, v8, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->end:J

    iget-wide v5, v8, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->id:J

    const/4 v7, -0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/DeleteEventHelper;->delete(JJJI)V

    :cond_0
    return-void
.end method

.method protected getEventIdByPosition(I)J
    .locals 3
    .param p1    # I

    if-lez p1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getCount()I

    move-result v1

    if-gt p1, v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v1, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getEventByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->id:J

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method public getFirstVisibleEvent()Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;
    .locals 5

    invoke-virtual {p0}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v0

    iget-boolean v3, p0, Lcom/android/calendar/agenda/AgendaListView;->mShowEventDetailsWithAgenda:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v2, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v4, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v4}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getStickyHeaderHeight()I

    move-result v4

    if-gt v3, v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    iget-object v3, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getEventByPosition(IZ)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v3

    return-object v3
.end method

.method public getFirstVisibleTime(Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;)J
    .locals 7
    .param p1    # Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-object v0, p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleEvent()Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/agenda/AgendaListView;->mTimeZone:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iget-wide v5, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    invoke-virtual {v4, v5, v6}, Landroid/text/format/Time;->set(J)V

    iget v1, v4, Landroid/text/format/Time;->hour:I

    iget v2, v4, Landroid/text/format/Time;->minute:I

    iget v3, v4, Landroid/text/format/Time;->second:I

    iget v5, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->startDay:I

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->setJulianDay(I)J

    iput v1, v4, Landroid/text/format/Time;->hour:I

    iput v2, v4, Landroid/text/format/Time;->minute:I

    iput v3, v4, Landroid/text/format/Time;->second:I

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v5

    :goto_0
    return-wide v5

    :cond_1
    const-wide/16 v5, 0x0

    goto :goto_0
.end method

.method public getFirstVisibleView()Landroid/view/View;
    .locals 5

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    iget v4, v3, Landroid/graphics/Rect;->top:I

    if-ltz v4, :cond_0

    :goto_1
    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getJulianDayFromPosition(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v1, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getAdapterInfoByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->dayAdapter:Lcom/android/calendar/agenda/AgendaByDayAdapter;

    iget v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$DayAdapterInfo;->offset:I

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/agenda/AgendaByDayAdapter;->findJulianDayFromPosition(I)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSelectedInstanceId()J
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getSelectedInstanceId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedTime()J
    .locals 4

    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v2, v1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getEventByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    :goto_0
    return-wide v2

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleTime(Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;)J

    move-result-wide v2

    goto :goto_0
.end method

.method public getSelectedViewHolder()Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getSelectedViewHolder()Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public goTo(Landroid/text/format/Time;JLjava/lang/String;ZZ)V
    .locals 9
    .param p1    # Landroid/text/format/Time;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/agenda/AgendaListView;->getFirstVisibleTime(Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;)J

    move-result-wide v7

    const-wide/16 v0, 0x0

    cmp-long v0, v7, v0

    if-gtz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    :cond_0
    invoke-virtual {p1, v7, v8}, Landroid/text/format/Time;->set(J)V

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->mTimeZone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->refresh(Landroid/text/format/Time;JLjava/lang/String;ZZ)V

    return-void
.end method

.method public isEventVisible(Landroid/text/format/Time;J)Z
    .locals 11
    .param p1    # Landroid/text/format/Time;
    .param p2    # J

    const-wide/16 v9, -0x1

    cmp-long v9, p2, v9

    if-eqz v9, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v9, 0x0

    :goto_0
    return v9

    :cond_1
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v9, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Landroid/widget/AdapterView;->getPositionForView(Landroid/view/View;)I

    move-result v8

    const/4 v9, 0x1

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    iget-object v9, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v9}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getCount()I

    move-result v3

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_3

    add-int v9, v4, v8

    if-lt v9, v3, :cond_4

    :cond_3
    const/4 v9, 0x0

    goto :goto_0

    :cond_4
    iget-object v9, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    add-int v10, v4, v8

    invoke-virtual {v9, v10}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getEventByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v2

    if-nez v2, :cond_6

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_6
    iget-wide v9, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->id:J

    cmp-long v9, v9, p2

    if-nez v9, :cond_5

    iget-wide v9, v2, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    cmp-long v9, v9, v6

    if-nez v9, :cond_5

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v10

    if-gt v9, v10, :cond_5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v9

    iget-object v10, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v10}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getStickyHeaderHeight()I

    move-result v10

    if-lt v9, v10, :cond_5

    const/4 v9, 0x1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/AbsListView;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->close()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 21
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const-wide/16 v2, -0x1

    cmp-long v2, p4, v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getEventByPosition(I)Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getSelectedInstanceId()J

    move-result-wide v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->setSelectedView(Landroid/view/View;)V

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v2}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->getSelectedInstanceId()J

    move-result-wide v2

    cmp-long v2, v19, v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/agenda/AgendaListView;->mShowEventDetailsWithAgenda:Z

    if-nez v2, :cond_2

    :cond_0
    move-object/from16 v0, v17

    iget-wide v7, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->begin:J

    move-object/from16 v0, v17

    iget-wide v9, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->end:J

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    instance-of v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    if-eqz v2, :cond_3

    check-cast v18, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    move-object/from16 v0, v18

    iget-wide v15, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->startTimeMilli:J

    :goto_0
    move-object/from16 v0, v17

    iget-boolean v2, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->allDay:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaListView;->mTimeZone:Ljava/lang/String;

    invoke-static {v2, v7, v8, v3}, Lcom/android/calendar/Utils;->convertAlldayLocalToUTC(Landroid/text/format/Time;JLjava/lang/String;)J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaListView;->mTimeZone:Ljava/lang/String;

    invoke-static {v2, v9, v10, v3}, Lcom/android/calendar/Utils;->convertAlldayLocalToUTC(Landroid/text/format/Time;JLjava/lang/String;)J

    move-result-wide v9

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    invoke-virtual {v2, v7, v8}, Landroid/text/format/Time;->set(J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaListView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v1

    const-wide/16 v3, 0x2

    move-object/from16 v0, v17

    iget-wide v5, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->id:J

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, v17

    iget-boolean v13, v0, Lcom/android/calendar/agenda/AgendaWindowAdapter$EventInfo;->allDay:Z

    invoke-static {v2, v13}, Lcom/android/calendar/CalendarController$EventInfo;->buildViewExtraLong(IZ)J

    move-result-wide v13

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v16}, Lcom/android/calendar/CalendarController;->sendEventRelatedEventWithExtra(Ljava/lang/Object;JJJJIIJJ)V

    :cond_2
    return-void

    :cond_3
    move-wide v15, v7

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->mMidnightUpdater:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->resetMidnightUpdater(Landroid/os/Handler;Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->resetPastEventsUpdater()V

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mTZUpdater:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->mMidnightUpdater:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/android/calendar/agenda/AgendaListView;->mTimeZone:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/calendar/Utils;->setMidnightUpdater(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/calendar/agenda/AgendaListView;->setPastEventsUpdater()V

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v0}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->onResume()V

    return-void
.end method

.method public refresh(Z)V
    .locals 7
    .param p1    # Z

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaListView;->mTime:Landroid/text/format/Time;

    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    move v5, p1

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->refresh(Landroid/text/format/Time;JLjava/lang/String;ZZ)V

    return-void
.end method

.method public setHideDeclinedEvents(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v0, p1}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->setHideDeclinedEvents(Z)V

    return-void
.end method

.method public setSelectedInstanceId(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaListView;->mWindowAdapter:Lcom/android/calendar/agenda/AgendaWindowAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/android/calendar/agenda/AgendaWindowAdapter;->setSelectedInstanceId(J)V

    return-void
.end method

.method public shiftSelection(I)V
    .locals 3
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/calendar/agenda/AgendaListView;->shiftPosition(I)V

    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    add-int v1, v0, p1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_0
    return-void
.end method
