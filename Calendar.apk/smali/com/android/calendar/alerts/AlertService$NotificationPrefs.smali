.class Lcom/android/calendar/alerts/AlertService$NotificationPrefs;
.super Ljava/lang/Object;
.source "AlertService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/alerts/AlertService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NotificationPrefs"
.end annotation


# static fields
.field private static final EMPTY_RINGTONE:Ljava/lang/String; = ""


# instance fields
.field private context:Landroid/content/Context;

.field private defaultVibrate:I

.field private doPopup:I

.field private prefs:Landroid/content/SharedPreferences;

.field quietUpdate:Z

.field private ringtone:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/SharedPreferences;
    .param p3    # Z

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->doPopup:I

    iput v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->defaultVibrate:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->ringtone:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->prefs:Landroid/content/SharedPreferences;

    iput-boolean p3, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->quietUpdate:Z

    return-void
.end method

.method static synthetic access$000(Lcom/android/calendar/alerts/AlertService$NotificationPrefs;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/alerts/AlertService$NotificationPrefs;

    invoke-direct {p0}, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->getDefaultVibrate()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/android/calendar/alerts/AlertService$NotificationPrefs;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/calendar/alerts/AlertService$NotificationPrefs;

    invoke-direct {p0}, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->getRingtoneAndSilence()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/calendar/alerts/AlertService$NotificationPrefs;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/alerts/AlertService$NotificationPrefs;

    invoke-direct {p0}, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->getDoPopup()Z

    move-result v0

    return v0
.end method

.method private getDefaultVibrate()Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->defaultVibrate:I

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->prefs:Landroid/content/SharedPreferences;

    invoke-static {v0, v3}, Lcom/android/calendar/Utils;->getDefaultVibrate(Landroid/content/Context;Landroid/content/SharedPreferences;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->defaultVibrate:I

    :cond_0
    iget v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->defaultVibrate:I

    if-ne v0, v1, :cond_2

    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private getDoPopup()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->doPopup:I

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->prefs:Landroid/content/SharedPreferences;

    const-string v3, "preferences_alerts_popup"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    iput v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->doPopup:I

    :cond_0
    :goto_0
    iget v2, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->doPopup:I

    if-ne v2, v0, :cond_2

    :goto_1
    return v0

    :cond_1
    iput v1, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->doPopup:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private getRingtoneAndSilence()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->ringtone:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->quietUpdate:Z

    if-eqz v1, :cond_1

    const-string v1, ""

    iput-object v1, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->ringtone:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->ringtone:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->ringtone:Ljava/lang/String;

    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->prefs:Landroid/content/SharedPreferences;

    const-string v2, "preferences_alerts_ringtone"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/alerts/AlertService$NotificationPrefs;->ringtone:Ljava/lang/String;

    goto :goto_0
.end method
