.class public final Lcom/android/calendar/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final agenda_item_bg_primary:I = 0x7f020000

.field public static final agenda_item_bg_secondary:I = 0x7f020001

.field public static final appwidget_header_selector:I = 0x7f020002

.field public static final bg_event_cal_widget_holo:I = 0x7f020003

.field public static final bg_separator:I = 0x7f020004

.field public static final bg_separator_inset:I = 0x7f020005

.field public static final cal_widget_bg:I = 0x7f020006

.field public static final cal_widget_date_bg:I = 0x7f020007

.field public static final calendar_widget_preview:I = 0x7f020008

.field public static final calendars_item:I = 0x7f020009

.field public static final calname_bottom_select_underselect:I = 0x7f02000a

.field public static final calname_bottom_select_underselect_holo_light:I = 0x7f02000b

.field public static final calname_bottom_select_underselect_pressed_holo_light:I = 0x7f02000c

.field public static final calname_bottom_select_underunselected:I = 0x7f02000d

.field public static final calname_bottom_select_underunselected_holo_light:I = 0x7f02000e

.field public static final calname_bottom_select_underunselected_pressed_holo_light:I = 0x7f02000f

.field public static final calname_bottom_unselected:I = 0x7f020010

.field public static final calname_bottom_unselected_holo_light:I = 0x7f020011

.field public static final calname_bottom_unselected_pressed_holo_light:I = 0x7f020012

.field public static final calname_bottom_unselected_underselect:I = 0x7f020013

.field public static final calname_bottom_unselected_underselect_holo_light:I = 0x7f020014

.field public static final calname_bottom_unselected_underselect_pressed_holo_light:I = 0x7f020015

.field public static final calname_select_underselect:I = 0x7f020016

.field public static final calname_select_underselect_holo_light:I = 0x7f020017

.field public static final calname_select_underselect_pressed_holo_light:I = 0x7f020018

.field public static final calname_select_underunselected:I = 0x7f020019

.field public static final calname_select_underunselected_holo_light:I = 0x7f02001a

.field public static final calname_select_underunselected_pressed_holo_light:I = 0x7f02001b

.field public static final calname_unselected:I = 0x7f02001c

.field public static final calname_unselected_holo_light:I = 0x7f02001d

.field public static final calname_unselected_pressed_holo_light:I = 0x7f02001e

.field public static final calname_unselected_underselect:I = 0x7f02001f

.field public static final calname_unselected_underselect_holo_light:I = 0x7f020020

.field public static final calname_unselected_underselect_pressed_holo_light:I = 0x7f020021

.field public static final chip_background:I = 0x7f020022

.field public static final chip_background_invalid:I = 0x7f020023

.field public static final chip_background_selected:I = 0x7f020024

.field public static final chip_checkmark:I = 0x7f020025

.field public static final chip_delete:I = 0x7f020026

.field public static final dayline_minical_holo_light:I = 0x7f020027

.field public static final event_info_mail_button:I = 0x7f020028

.field public static final frame_event_color_cal_widget_holo:I = 0x7f020029

.field public static final header_bg_cal_widget_focused_holo:I = 0x7f02002a

.field public static final header_bg_cal_widget_normal_holo:I = 0x7f02002b

.field public static final header_bg_cal_widget_pressed_holo:I = 0x7f02002c

.field public static final ic_alarm_dark:I = 0x7f02002d

.field public static final ic_alarm_holo_dark:I = 0x7f02002e

.field public static final ic_alarm_white:I = 0x7f02002f

.field public static final ic_collapse_holo_light:I = 0x7f020030

.field public static final ic_collapse_large_holo_light:I = 0x7f020031

.field public static final ic_collapse_small_holo_light:I = 0x7f020032

.field public static final ic_contact_picture:I = 0x7f020033

.field public static final ic_expand_holo_light:I = 0x7f020034

.field public static final ic_expand_large_holo_light:I = 0x7f020035

.field public static final ic_expand_small_holo_light:I = 0x7f020036

.field public static final ic_menu_add_event_holo_light:I = 0x7f020037

.field public static final ic_menu_add_field_holo_light:I = 0x7f020038

.field public static final ic_menu_cancel_holo_light:I = 0x7f020039

.field public static final ic_menu_compose_holo_dark:I = 0x7f02003a

.field public static final ic_menu_compose_holo_light:I = 0x7f02003b

.field public static final ic_menu_done_holo_light:I = 0x7f02003c

.field public static final ic_menu_email_holo_dark:I = 0x7f02003d

.field public static final ic_menu_email_holo_light:I = 0x7f02003e

.field public static final ic_menu_refresh_holo_light:I = 0x7f02003f

.field public static final ic_menu_remove_field_holo_light:I = 0x7f020040

.field public static final ic_menu_search_holo_light:I = 0x7f020041

.field public static final ic_menu_select_visible_calendars_holo_light:I = 0x7f020042

.field public static final ic_menu_settings_holo_light:I = 0x7f020043

.field public static final ic_menu_share_holo_dark:I = 0x7f020044

.field public static final ic_menu_share_holo_light:I = 0x7f020045

.field public static final ic_menu_today_holo_light:I = 0x7f020046

.field public static final ic_menu_today_no_date_holo_light:I = 0x7f020047

.field public static final ic_menu_trash_holo_dark:I = 0x7f020048

.field public static final ic_menu_trash_holo_light:I = 0x7f020049

.field public static final ic_repeat_dark:I = 0x7f02004a

.field public static final ic_repeat_white:I = 0x7f02004b

.field public static final list_focused_holo:I = 0x7f02004c

.field public static final list_item_font_primary:I = 0x7f02004d

.field public static final list_item_font_secondary:I = 0x7f02004e

.field public static final list_multi_left_activated_holo:I = 0x7f02004f

.field public static final list_multi_left_focused_holo:I = 0x7f020050

.field public static final list_multi_left_pressed_holo:I = 0x7f020051

.field public static final list_multi_left_primary_holo:I = 0x7f020052

.field public static final list_multi_left_secondary_holo:I = 0x7f020053

.field public static final list_pressed_holo:I = 0x7f020054

.field public static final list_primary_holo:I = 0x7f020055

.field public static final list_secondary_holo:I = 0x7f020056

.field public static final minical_bg_shadow_holo_light:I = 0x7f020057

.field public static final panel_month_event_holo_light:I = 0x7f020058

.field public static final snooze:I = 0x7f020059

.field public static final stat_notify_calendar:I = 0x7f02005a

.field public static final stat_notify_calendar_multiple:I = 0x7f02005b

.field public static final timeline_indicator_activated_holo_light:I = 0x7f02005c

.field public static final timeline_indicator_holo_light:I = 0x7f02005d

.field public static final today_blue_week_holo_light:I = 0x7f02005e

.field public static final today_icon:I = 0x7f02005f

.field public static final widget_chip_not_responded_bg:I = 0x7f020060

.field public static final widget_chip_responded_bg:I = 0x7f020061

.field public static final widget_show:I = 0x7f020062


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
