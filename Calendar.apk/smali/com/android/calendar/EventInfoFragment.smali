.class public Lcom/android/calendar/EventInfoFragment;
.super Landroid/app/DialogFragment;
.source "EventInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lcom/android/calendar/CalendarController$EventHandler;
.implements Lcom/android/calendar/DeleteEventHelper$DeleteNotifyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/EventInfoFragment$QueryHandler;
    }
.end annotation


# static fields
.field private static final ATTENDEES_INDEX_EMAIL:I = 0x2

.field private static final ATTENDEES_INDEX_ID:I = 0x0

.field private static final ATTENDEES_INDEX_IDENTITY:I = 0x5

.field private static final ATTENDEES_INDEX_ID_NAMESPACE:I = 0x6

.field private static final ATTENDEES_INDEX_NAME:I = 0x1

.field private static final ATTENDEES_INDEX_RELATIONSHIP:I = 0x3

.field private static final ATTENDEES_INDEX_STATUS:I = 0x4

.field private static final ATTENDEES_PROJECTION:[Ljava/lang/String;

.field private static final ATTENDEES_SORT_ORDER:Ljava/lang/String; = "attendeeName ASC, attendeeEmail ASC"

.field private static final ATTENDEES_WHERE:Ljava/lang/String; = "event_id=?"

.field protected static final BUNDLE_KEY_ATTENDEE_RESPONSE:Ljava/lang/String; = "key_attendee_response"

.field protected static final BUNDLE_KEY_DELETE_DIALOG_VISIBLE:Ljava/lang/String; = "key_delete_dialog_visible"

.field protected static final BUNDLE_KEY_END_MILLIS:Ljava/lang/String; = "key_end_millis"

.field protected static final BUNDLE_KEY_EVENT_ID:Ljava/lang/String; = "key_event_id"

.field protected static final BUNDLE_KEY_IS_DIALOG:Ljava/lang/String; = "key_fragment_is_dialog"

.field protected static final BUNDLE_KEY_ORIGINAL_REMINDERS:Ljava/lang/String; = "key_original_reminders"

.field protected static final BUNDLE_KEY_REMINDERS:Ljava/lang/String; = "key_reminders"

.field protected static final BUNDLE_KEY_START_MILLIS:Ljava/lang/String; = "key_start_millis"

.field protected static final BUNDLE_KEY_WHICH_DELETE:Ljava/lang/String; = "key_which_delete"

.field protected static final BUNDLE_KEY_WHICH_EVENTS:Ljava/lang/String; = "key_which_events"

.field protected static final BUNDLE_KEY_WINDOW_STYLE:Ljava/lang/String; = "key_window_style"

.field static final CALENDARS_DUPLICATE_NAME_WHERE:Ljava/lang/String; = "calendar_displayName=?"

.field static final CALENDARS_INDEX_ACCOUNT_NAME:I = 0x4

.field static final CALENDARS_INDEX_DISPLAY_NAME:I = 0x1

.field static final CALENDARS_INDEX_OWNER_ACCOUNT:I = 0x2

.field static final CALENDARS_INDEX_OWNER_CAN_RESPOND:I = 0x3

.field static final CALENDARS_PROJECTION:[Ljava/lang/String;

.field static final CALENDARS_VISIBLE_WHERE:Ljava/lang/String; = "visible=?"

.field static final CALENDARS_WHERE:Ljava/lang/String; = "_id=?"

.field public static final DEBUG:Z = false

.field private static DIALOG_TOP_MARGIN:I = 0x0

.field public static final DIALOG_WINDOW_STYLE:I = 0x1

.field private static final EVENT_INDEX_ACCESS_LEVEL:I = 0xa

.field private static final EVENT_INDEX_ACCOUNT_TYPE:I = 0x14

.field private static final EVENT_INDEX_ALLOWED_REMINDERS:I = 0x10

.field private static final EVENT_INDEX_ALL_DAY:I = 0x3

.field private static final EVENT_INDEX_CALENDAR_ID:I = 0x4

.field private static final EVENT_INDEX_COLOR:I = 0xb

.field private static final EVENT_INDEX_CUSTOM_APP_PACKAGE:I = 0x11

.field private static final EVENT_INDEX_CUSTOM_APP_URI:I = 0x12

.field private static final EVENT_INDEX_DESCRIPTION:I = 0x8

.field private static final EVENT_INDEX_EVENT_LOCATION:I = 0x9

.field private static final EVENT_INDEX_EVENT_TIMEZONE:I = 0x7

.field private static final EVENT_INDEX_HAS_ALARM:I = 0xe

.field private static final EVENT_INDEX_HAS_ATTENDEE_DATA:I = 0xc

.field private static final EVENT_INDEX_ID:I = 0x0

.field private static final EVENT_INDEX_MAX_REMINDERS:I = 0xf

.field private static final EVENT_INDEX_ORGANIZER:I = 0xd

.field private static final EVENT_INDEX_RRULE:I = 0x2

.field private static final EVENT_INDEX_SYNC_ID:I = 0x6

.field private static final EVENT_INDEX_TITLE:I = 0x1

.field private static final EVENT_PROJECTION:[Ljava/lang/String;

.field private static final FADE_IN_TIME:I = 0x12c

.field public static final FRAGMENT_TAG:Ljava/lang/String; = "fragment_tag"

.field public static final FULL_WINDOW_STYLE:I = 0x0

.field private static final LOADING_MSG_DELAY:I = 0x258

.field private static final LOADING_MSG_MIN_DISPLAY_TIME:I = 0x258

.field private static final NANP_ALLOWED_SYMBOLS:Ljava/lang/String; = "()+-*#."

.field private static final NANP_MAX_DIGITS:I = 0xb

.field private static final NANP_MIN_DIGITS:I = 0x7

.field private static final PERIOD_SPACE:Ljava/lang/String; = ". "

.field private static final REMINDERS_INDEX_ID:I = 0x0

.field private static final REMINDERS_METHOD_ID:I = 0x2

.field private static final REMINDERS_MINUTES_ID:I = 0x1

.field private static final REMINDERS_PROJECTION:[Ljava/lang/String;

.field private static final REMINDERS_WHERE:Ljava/lang/String; = "event_id=?"

.field public static final TAG:Ljava/lang/String; = "EventInfoFragment"

.field private static final TOKEN_INSERT_EXCEPTION_EVENT:I = 0x40

.field private static final TOKEN_QUERY_ALL:I = 0x3f

.field private static final TOKEN_QUERY_ATTENDEES:I = 0x4

.field private static final TOKEN_QUERY_CALENDARS:I = 0x2

.field private static final TOKEN_QUERY_DUPLICATE_CALENDARS:I = 0x8

.field private static final TOKEN_QUERY_EVENT:I = 0x1

.field private static final TOKEN_QUERY_REMINDERS:I = 0x10

.field private static final TOKEN_QUERY_VISIBLE_CALENDARS:I = 0x20

.field static final UPDATE_ALL:I = 0x1

.field static final UPDATE_SINGLE:I

.field private static final WILDCARD_PATTERN:Ljava/util/regex/Pattern;

.field private static mCustomAppIconSize:I

.field private static mDialogHeight:I

.field private static mDialogWidth:I

.field private static mScale:F


# instance fields
.field private emailAttendeesButton:Landroid/widget/Button;

.field mAcceptedAttendees:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$Attendee;",
            ">;"
        }
    .end annotation
.end field

.field private mActivity:Landroid/app/Activity;

.field private mAddReminderBtn:Landroid/widget/Button;

.field private mAllDay:Z

.field private mAnimateAlpha:Landroid/animation/ObjectAnimator;

.field private mAttendeeResponseFromIntent:I

.field private mAttendeesCursor:Landroid/database/Cursor;

.field private mCalendarAllowedReminders:Ljava/lang/String;

.field private mCalendarOwnerAccount:Ljava/lang/String;

.field private mCalendarOwnerAttendeeId:J

.field private mCalendarsCursor:Landroid/database/Cursor;

.field private mCanModifyCalendar:Z

.field private mCanModifyEvent:Z

.field mCcEmails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mColor:I

.field private mContext:Landroid/content/Context;

.field private mCurrentQuery:I

.field mDeclinedAttendees:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$Attendee;",
            ">;"
        }
    .end annotation
.end field

.field private mDefaultReminderMinutes:I

.field private mDeleteDialogVisible:Z

.field private mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

.field private mDesc:Lcom/android/calendar/ExpandableTextView;

.field private mDismissOnResume:Z

.field private mEditResponseHelper:Lcom/android/calendar/EditResponseHelper;

.field private mEndMillis:J

.field private mEventCursor:Landroid/database/Cursor;

.field private mEventDeletionStarted:Z

.field private mEventId:J

.field private mEventOrganizerDisplayName:Ljava/lang/String;

.field private mEventOrganizerEmail:Ljava/lang/String;

.field private mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

.field private mHasAlarm:Z

.field private mHasAttendeeData:Z

.field private mHeadlines:Landroid/view/View;

.field private mIsBusyFreeCalendar:Z

.field private mIsDialog:Z

.field private mIsOrganizer:Z

.field private mIsPaused:Z

.field private mIsRepeating:Z

.field private mIsTabletConfig:Z

.field private final mLoadingMsgAlphaUpdater:Ljava/lang/Runnable;

.field private mLoadingMsgStartTime:J

.field private mLoadingMsgView:Landroid/view/View;

.field private mLongAttendees:Lcom/android/calendar/event/AttendeesView;

.field private mMaxReminders:I

.field private mMenu:Landroid/view/Menu;

.field private mMinTop:I

.field private mNeedSaveEvent:Z

.field private mNoCrossFade:Z

.field mNoResponseAttendees:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$Attendee;",
            ">;"
        }
    .end annotation
.end field

.field private mNumOfAttendees:I

.field mOptionsMenuExt:Lcom/mediatek/calendar/extension/IOptionsMenuExt;

.field private mOriginalAttendeeResponse:I

.field public mOriginalReminders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$ReminderEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mOwnerCanRespond:Z

.field private mReminderChangeListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mReminderMethodLabels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReminderMethodValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mReminderMinuteLabels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReminderMinuteValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mReminderViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public mReminders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$ReminderEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mRemindersCursor:Landroid/database/Cursor;

.field private mSavedReminders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$ReminderEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollView:Landroid/widget/ScrollView;

.field private mStartMillis:J

.field private mSyncAccountName:Ljava/lang/String;

.field private final mTZUpdater:Ljava/lang/Runnable;

.field mTentativeAttendees:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$Attendee;",
            ">;"
        }
    .end annotation
.end field

.field private mTitle:Landroid/widget/TextView;

.field mToEmails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mUnsupportedReminders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$ReminderEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mUri:Landroid/net/Uri;

.field private mUserModifiedReminders:Z

.field private mUserSetResponse:I

.field private mView:Landroid/view/View;

.field private mWhenDateTime:Landroid/widget/TextView;

.field private mWhere:Landroid/widget/TextView;

.field private mWhichDelete:I

.field private mWindowStyle:I

.field private mX:I

.field private mY:I

.field private final onDeleteRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "rrule"

    aput-object v1, v0, v5

    const-string v1, "allDay"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "calendar_id"

    aput-object v2, v0, v1

    const-string v1, "dtstart"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "calendar_access_level"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "displayColor"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "hasAttendeeData"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "maxReminders"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "allowedReminders"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "customAppPackage"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "customAppUri"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "original_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "account_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/EventInfoFragment;->EVENT_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "attendeeName"

    aput-object v1, v0, v4

    const-string v1, "attendeeEmail"

    aput-object v1, v0, v5

    const-string v1, "attendeeRelationship"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "attendeeStatus"

    aput-object v2, v0, v1

    const-string v1, "attendeeIdentity"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    const-string v2, "attendeeIdNamespace"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/EventInfoFragment;->ATTENDEES_PROJECTION:[Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/Utils;->isJellybeanOrLater()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/calendar/EventInfoFragment;->EVENT_PROJECTION:[Ljava/lang/String;

    const/16 v1, 0xb

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/calendar/EventInfoFragment;->EVENT_PROJECTION:[Ljava/lang/String;

    const/16 v1, 0x11

    const-string v2, "_id"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/calendar/EventInfoFragment;->EVENT_PROJECTION:[Ljava/lang/String;

    const/16 v1, 0x12

    const-string v2, "_id"

    aput-object v2, v0, v1

    sget-object v0, Lcom/android/calendar/EventInfoFragment;->ATTENDEES_PROJECTION:[Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v7

    sget-object v0, Lcom/android/calendar/EventInfoFragment;->ATTENDEES_PROJECTION:[Ljava/lang/String;

    const/4 v1, 0x6

    const-string v2, "_id"

    aput-object v2, v0, v1

    :cond_0
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "minutes"

    aput-object v1, v0, v4

    const-string v1, "method"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/EventInfoFragment;->REMINDERS_PROJECTION:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "calendar_displayName"

    aput-object v1, v0, v4

    const-string v1, "ownerAccount"

    aput-object v1, v0, v5

    const-string v1, "canOrganizerRespond"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "account_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/EventInfoFragment;->CALENDARS_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/EventInfoFragment;->mScale:F

    const/16 v0, 0x20

    sput v0, Lcom/android/calendar/EventInfoFragment;->mCustomAppIconSize:I

    const-string v0, "^.*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/EventInfoFragment;->WILDCARD_PATTERN:Ljava/util/regex/Pattern;

    const/16 v0, 0x1f4

    sput v0, Lcom/android/calendar/EventInfoFragment;->mDialogWidth:I

    const/16 v0, 0x258

    sput v0, Lcom/android/calendar/EventInfoFragment;->mDialogHeight:I

    const/16 v0, 0x8

    sput v0, Lcom/android/calendar/EventInfoFragment;->DIALOG_TOP_MARGIN:I

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    iput v4, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    iput v2, p0, Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerDisplayName:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAttendeeId:J

    iput-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteDialogVisible:Z

    iput v2, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeeResponseFromIntent:I

    iput v2, p0, Lcom/android/calendar/EventInfoFragment;->mUserSetResponse:I

    iput-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mEventDeletionStarted:Z

    iput-object v5, p0, Lcom/android/calendar/EventInfoFragment;->mMenu:Landroid/view/Menu;

    iput-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mNoCrossFade:Z

    iput-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mNeedSaveEvent:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAcceptedAttendees:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeclinedAttendees:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mTentativeAttendees:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mNoResponseAttendees:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mToEmails:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCcEmails:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mUnsupportedReminders:Ljava/util/ArrayList;

    iput-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mUserModifiedReminders:Z

    new-instance v0, Lcom/android/calendar/EventInfoFragment$1;

    invoke-direct {v0, p0}, Lcom/android/calendar/EventInfoFragment$1;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mTZUpdater:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/calendar/EventInfoFragment$2;

    invoke-direct {v0, p0}, Lcom/android/calendar/EventInfoFragment$2;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLoadingMsgAlphaUpdater:Ljava/lang/Runnable;

    iput-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    iput-boolean v4, p0, Lcom/android/calendar/EventInfoFragment;->mIsPaused:Z

    iput-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mDismissOnResume:Z

    iput v3, p0, Lcom/android/calendar/EventInfoFragment;->mX:I

    iput v3, p0, Lcom/android/calendar/EventInfoFragment;->mY:I

    new-instance v0, Lcom/android/calendar/EventInfoFragment$9;

    invoke-direct {v0, p0}, Lcom/android/calendar/EventInfoFragment$9;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->onDeleteRunnable:Ljava/lang/Runnable;

    iput-object v5, p0, Lcom/android/calendar/EventInfoFragment;->mAddReminderBtn:Landroid/widget/Button;

    iput v3, p0, Lcom/android/calendar/EventInfoFragment;->mWhichDelete:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JJJIZI)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # J
    .param p4    # J
    .param p6    # J
    .param p8    # I
    .param p9    # Z
    .param p10    # I

    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p4

    move-wide/from16 v5, p6

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    invoke-direct/range {v0 .. v9}, Lcom/android/calendar/EventInfoFragment;-><init>(Landroid/content/Context;Landroid/net/Uri;JJIZI)V

    iput-wide p2, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;JJIZI)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # J
    .param p5    # J
    .param p7    # I
    .param p8    # Z
    .param p9    # I

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I

    const-string v1, ""

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerDisplayName:Ljava/lang/String;

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAttendeeId:J

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteDialogVisible:Z

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeeResponseFromIntent:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mUserSetResponse:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventDeletionStarted:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mMenu:Landroid/view/Menu;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mNoCrossFade:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mNeedSaveEvent:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mAcceptedAttendees:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mDeclinedAttendees:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mTentativeAttendees:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mNoResponseAttendees:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mToEmails:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mCcEmails:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mUnsupportedReminders:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mUserModifiedReminders:Z

    new-instance v1, Lcom/android/calendar/EventInfoFragment$1;

    invoke-direct {v1, p0}, Lcom/android/calendar/EventInfoFragment$1;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mTZUpdater:Ljava/lang/Runnable;

    new-instance v1, Lcom/android/calendar/EventInfoFragment$2;

    invoke-direct {v1, p0}, Lcom/android/calendar/EventInfoFragment$2;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mLoadingMsgAlphaUpdater:Ljava/lang/Runnable;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsPaused:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mDismissOnResume:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mX:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mY:I

    new-instance v1, Lcom/android/calendar/EventInfoFragment$9;

    invoke-direct {v1, p0}, Lcom/android/calendar/EventInfoFragment$9;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->onDeleteRunnable:Ljava/lang/Runnable;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mAddReminderBtn:Landroid/widget/Button;

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mWhichDelete:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/calendar/EventInfoFragment;->mScale:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    sput v1, Lcom/android/calendar/EventInfoFragment;->mScale:F

    sget v1, Lcom/android/calendar/EventInfoFragment;->mScale:F

    const/high16 v2, 0x3f800000

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    sget v1, Lcom/android/calendar/EventInfoFragment;->mCustomAppIconSize:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/EventInfoFragment;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/EventInfoFragment;->mCustomAppIconSize:I

    if-eqz p8, :cond_0

    sget v1, Lcom/android/calendar/EventInfoFragment;->DIALOG_TOP_MARGIN:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/EventInfoFragment;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/EventInfoFragment;->DIALOG_TOP_MARGIN:I

    :cond_0
    if-eqz p8, :cond_1

    invoke-direct {p0, v0}, Lcom/android/calendar/EventInfoFragment;->setDialogSize(Landroid/content/res/Resources;)V

    :cond_1
    iput-boolean p8, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/app/DialogFragment;->setStyle(II)V

    iput-object p2, p0, Lcom/android/calendar/EventInfoFragment;->mUri:Landroid/net/Uri;

    iput-wide p3, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    iput-wide p5, p0, Lcom/android/calendar/EventInfoFragment;->mEndMillis:J

    iput p7, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeeResponseFromIntent:I

    iput p9, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/calendar/EventInfoFragment;->updateEvent(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/calendar/EventInfoFragment;)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/calendar/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/calendar/EventInfoFragment;->updateCalendar(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/calendar/EventInfoFragment;)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->updateTitle()V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/calendar/EventInfoFragment;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsBusyFreeCalendar:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/android/calendar/EventInfoFragment;)J
    .locals 2
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-wide v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    return-wide v0
.end method

.method static synthetic access$1500()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/calendar/EventInfoFragment;->ATTENDEES_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/calendar/EventInfoFragment;I)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/calendar/EventInfoFragment;->sendAccessibilityEventIfQueryDone(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/android/calendar/EventInfoFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mSavedReminders:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/android/calendar/EventInfoFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment;->mSavedReminders:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/android/calendar/EventInfoFragment;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mHasAlarm:Z

    return v0
.end method

.method static synthetic access$1900()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/calendar/EventInfoFragment;->REMINDERS_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/calendar/EventInfoFragment;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAnimateAlpha:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/android/calendar/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/calendar/EventInfoFragment;->initAttendeesCursor(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/android/calendar/EventInfoFragment;)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mRemindersCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/android/calendar/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment;->mRemindersCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/android/calendar/EventInfoFragment;)Lcom/android/calendar/EventInfoFragment$QueryHandler;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;II)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    return-void
.end method

.method static synthetic access$2500(Lcom/android/calendar/EventInfoFragment;)I
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I

    return v0
.end method

.method static synthetic access$2576(Lcom/android/calendar/EventInfoFragment;I)I
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # I

    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I

    return v0
.end method

.method static synthetic access$2600(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;ILjava/lang/CharSequence;)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/EventInfoFragment;->setTextCommon(Landroid/view/View;ILjava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/android/calendar/EventInfoFragment;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mNoCrossFade:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/android/calendar/EventInfoFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/EventInfoFragment;->mNoCrossFade:Z

    return p1
.end method

.method static synthetic access$2802(Lcom/android/calendar/EventInfoFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/EventInfoFragment;->mUserModifiedReminders:Z

    return p1
.end method

.method static synthetic access$2900(Lcom/android/calendar/EventInfoFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLoadingMsgAlphaUpdater:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/calendar/EventInfoFragment;)Landroid/widget/ScrollView;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/calendar/EventInfoFragment;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyCalendar:Z

    return v0
.end method

.method static synthetic access$3100(Lcom/android/calendar/EventInfoFragment;)Lcom/android/calendar/DeleteEventHelper;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/android/calendar/EventInfoFragment;Lcom/android/calendar/DeleteEventHelper;)Lcom/android/calendar/DeleteEventHelper;
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Lcom/android/calendar/DeleteEventHelper;

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/android/calendar/EventInfoFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/calendar/EventInfoFragment;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/calendar/EventInfoFragment;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    return v0
.end method

.method static synthetic access$3500(Lcom/android/calendar/EventInfoFragment;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsTabletConfig:Z

    return v0
.end method

.method static synthetic access$3600(Lcom/android/calendar/EventInfoFragment;)Landroid/content/DialogInterface$OnDismissListener;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->createDeleteOnDismissListener()Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3702(Lcom/android/calendar/EventInfoFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteDialogVisible:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/android/calendar/EventInfoFragment;)J
    .locals 2
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-wide v0, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    return-wide v0
.end method

.method static synthetic access$3900(Lcom/android/calendar/EventInfoFragment;)J
    .locals 2
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-wide v0, p0, Lcom/android/calendar/EventInfoFragment;->mEndMillis:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/android/calendar/EventInfoFragment;)J
    .locals 2
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-wide v0, p0, Lcom/android/calendar/EventInfoFragment;->mLoadingMsgStartTime:J

    return-wide v0
.end method

.method static synthetic access$4000(Lcom/android/calendar/EventInfoFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->onDeleteRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/calendar/EventInfoFragment;J)J
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/calendar/EventInfoFragment;->mLoadingMsgStartTime:J

    return-wide p1
.end method

.method static synthetic access$4100(Lcom/android/calendar/EventInfoFragment;)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->emailAttendees()V

    return-void
.end method

.method static synthetic access$4200(Lcom/android/calendar/EventInfoFragment;)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->addReminder()V

    return-void
.end method

.method static synthetic access$4300(Lcom/android/calendar/EventInfoFragment;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsPaused:Z

    return v0
.end method

.method static synthetic access$4402(Lcom/android/calendar/EventInfoFragment;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/EventInfoFragment;->mDismissOnResume:Z

    return p1
.end method

.method static synthetic access$4500(Lcom/android/calendar/EventInfoFragment;)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->doEdit()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLoadingMsgView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/calendar/EventInfoFragment;J)Z
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/calendar/EventInfoFragment;->saveRemindersForEvent(J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/android/calendar/EventInfoFragment;)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$702(Lcom/android/calendar/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;
    .param p1    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$800(Lcom/android/calendar/EventInfoFragment;)Z
    .locals 1
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->initEventCursor()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/android/calendar/EventInfoFragment;)V
    .locals 0
    .param p0    # Lcom/android/calendar/EventInfoFragment;

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->prepareReminders()V

    return-void
.end method

.method private addFieldToAccessibilityEvent(Ljava/util/List;Landroid/widget/TextView;Lcom/android/calendar/ExpandableTextView;)V
    .locals 2
    .param p2    # Landroid/widget/TextView;
    .param p3    # Lcom/android/calendar/ExpandableTextView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Landroid/widget/TextView;",
            "Lcom/android/calendar/ExpandableTextView;",
            ")V"
        }
    .end annotation

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, ". "

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/android/calendar/ExpandableTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private addReminder()V
    .locals 11

    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mDefaultReminderMinutes:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteLabels:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodLabels:Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v2}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->valueOf(I)Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-result-object v8

    iget v9, p0, Lcom/android/calendar/EventInfoFragment;->mMaxReminders:I

    iget-object v10, p0, Lcom/android/calendar/EventInfoFragment;->mReminderChangeListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object v2, p0

    invoke-static/range {v0 .. v10}, Lcom/android/calendar/event/EventViewUtils;->addReminder(Landroid/app/Activity;Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/calendar/CalendarEventModel$ReminderEntry;ILandroid/widget/AdapterView$OnItemSelectedListener;)Z

    :goto_0
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    iget v2, p0, Lcom/android/calendar/EventInfoFragment;->mMaxReminders:I

    invoke-static {v0, v1, v2}, Lcom/android/calendar/event/EventViewUtils;->updateAddReminderButton(Landroid/view/View;Ljava/util/ArrayList;I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteLabels:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodLabels:Ljava/util/ArrayList;

    iget v2, p0, Lcom/android/calendar/EventInfoFragment;->mDefaultReminderMinutes:I

    invoke-static {v2}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->valueOf(I)Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-result-object v8

    iget v9, p0, Lcom/android/calendar/EventInfoFragment;->mMaxReminders:I

    iget-object v10, p0, Lcom/android/calendar/EventInfoFragment;->mReminderChangeListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object v2, p0

    invoke-static/range {v0 .. v10}, Lcom/android/calendar/event/EventViewUtils;->addReminder(Landroid/app/Activity;Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/calendar/CalendarEventModel$ReminderEntry;ILandroid/widget/AdapterView$OnItemSelectedListener;)Z

    goto :goto_0
.end method

.method private applyDialogParams()V
    .locals 5

    const/4 v4, -0x1

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const v3, 0x3ecccccd

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    sget v3, Lcom/android/calendar/EventInfoFragment;->mDialogWidth:I

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    sget v3, Lcom/android/calendar/EventInfoFragment;->mDialogHeight:I

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v3, p0, Lcom/android/calendar/EventInfoFragment;->mX:I

    if-ne v3, v4, :cond_0

    iget v3, p0, Lcom/android/calendar/EventInfoFragment;->mY:I

    if-eq v3, v4, :cond_2

    :cond_0
    iget v3, p0, Lcom/android/calendar/EventInfoFragment;->mX:I

    sget v4, Lcom/android/calendar/EventInfoFragment;->mDialogWidth:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v3, p0, Lcom/android/calendar/EventInfoFragment;->mY:I

    sget v4, Lcom/android/calendar/EventInfoFragment;->mDialogHeight:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v4, p0, Lcom/android/calendar/EventInfoFragment;->mMinTop:I

    if-ge v3, v4, :cond_1

    iget v3, p0, Lcom/android/calendar/EventInfoFragment;->mMinTop:I

    sget v4, Lcom/android/calendar/EventInfoFragment;->DIALOG_TOP_MARGIN:I

    add-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    :cond_1
    const/16 v3, 0x33

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    :cond_2
    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method private createDeleteOnDismissListener()Landroid/content/DialogInterface$OnDismissListener;
    .locals 1

    new-instance v0, Lcom/android/calendar/EventInfoFragment$14;

    invoke-direct {v0, p0}, Lcom/android/calendar/EventInfoFragment$14;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    return-object v0
.end method

.method private createExceptionResponse(JI)V
    .locals 9
    .param p1    # J
    .param p3    # I

    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "originalInstanceTime"

    iget-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "selfAttendeeStatus"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "eventStatus"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_EXCEPTION_URI:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    const/16 v1, 0x40

    const/4 v2, 0x0

    const-string v3, "com.android.calendar"

    const-wide/16 v5, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/AsyncQueryService;->startBatch(ILjava/lang/Object;Ljava/lang/String;Ljava/util/ArrayList;J)V

    return-void
.end method

.method private doEdit()V
    .locals 15

    const/4 v10, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    if-eqz v14, :cond_0

    invoke-static {v14}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v0

    const-wide/16 v2, 0x8

    iget-wide v4, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    iget-wide v6, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    iget-wide v8, p0, Lcom/android/calendar/EventInfoFragment;->mEndMillis:J

    const-wide/16 v12, -0x1

    move-object v1, p0

    move v11, v10

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/CalendarController;->sendEventRelatedEvent(Ljava/lang/Object;JJJJIIJ)V

    :cond_0
    return-void
.end method

.method private emailAttendees()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/android/calendar/alerts/QuickResponseActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "eventId"

    iget-wide v2, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static findButtonIdForResponse(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f10006f

    goto :goto_0

    :pswitch_2
    const v0, 0x7f100070

    goto :goto_0

    :pswitch_3
    const v0, 0x7f100071

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static findNanpMatchEnd(Ljava/lang/CharSequence;I)I
    .locals 12
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # I

    const/16 v11, 0xb

    const/4 v10, 0x7

    const/4 v6, -0x1

    const/16 v9, 0x31

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v7

    add-int/lit8 v8, p1, 0x4

    if-le v7, v8, :cond_0

    add-int/lit8 v7, p1, 0x4

    invoke-interface {p0, p1, v7}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "tel:"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    add-int/lit8 p1, p1, 0x4

    :cond_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    move v1, p1

    const/4 v4, 0x0

    const/16 v3, 0x78

    const/4 v5, 0x0

    :goto_0
    if-gt v1, v2, :cond_b

    if-ge v1, v2, :cond_3

    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    :goto_1
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v7

    if-eqz v7, :cond_4

    if-nez v4, :cond_1

    move v3, v0

    :cond_1
    add-int/lit8 v4, v4, 0x1

    if-le v4, v11, :cond_7

    move v1, v6

    :cond_2
    :goto_2
    return v1

    :cond_3
    const/16 v0, 0x1b

    goto :goto_1

    :cond_4
    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v7

    if-eqz v7, :cond_e

    if-ne v3, v9, :cond_5

    const/4 v7, 0x4

    if-eq v4, v7, :cond_6

    :cond_5
    const/4 v7, 0x3

    if-ne v4, v7, :cond_8

    :cond_6
    const/4 v5, 0x1

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_8
    if-ne v3, v9, :cond_9

    const/4 v7, 0x1

    if-eq v4, v7, :cond_7

    :cond_9
    if-eqz v5, :cond_b

    if-ne v3, v9, :cond_a

    if-eq v4, v10, :cond_7

    :cond_a
    const/4 v7, 0x6

    if-eq v4, v7, :cond_7

    :cond_b
    :goto_3
    if-eq v3, v9, :cond_c

    if-eq v4, v10, :cond_2

    const/16 v7, 0xa

    if-eq v4, v7, :cond_2

    :cond_c
    if-ne v3, v9, :cond_d

    if-eq v4, v11, :cond_2

    :cond_d
    move v1, v6

    goto :goto_2

    :cond_e
    const-string v7, "()+-*#."

    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    if-ne v7, v6, :cond_7

    goto :goto_3
.end method

.method static findNanpPhoneNumbers(Ljava/lang/CharSequence;)[I
    .locals 7
    .param p0    # Ljava/lang/CharSequence;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x7

    add-int/lit8 v0, v6, 0x1

    if-gez v0, :cond_2

    const/4 v6, 0x0

    new-array v4, v6, [I

    :cond_0
    return-object v4

    :cond_1
    invoke-static {p0, v5}, Lcom/android/calendar/EventInfoFragment;->findNanpMatchEnd(Ljava/lang/CharSequence;I)I

    move-result v3

    if-le v3, v5, :cond_5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v5, v3

    :cond_2
    if-ge v5, v0, :cond_4

    :goto_0
    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_3

    if-ge v5, v0, :cond_3

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    if-ne v5, v0, :cond_1

    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v4, v6, [I

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v1, v6, -0x1

    :goto_1
    if-ltz v1, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput v6, v4, v1

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_5
    :goto_2
    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-nez v6, :cond_2

    if-ge v5, v0, :cond_2

    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method private formatAttendees(Ljava/util/ArrayList;Landroid/text/SpannableStringBuilder;I)V
    .locals 8
    .param p2    # Landroid/text/SpannableStringBuilder;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$Attendee;",
            ">;",
            "Landroid/text/SpannableStringBuilder;",
            "I)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gtz v5, :cond_0

    :goto_0
    :pswitch_0
    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_1

    add-int/lit8 v1, v1, 0x2

    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v0}, Lcom/android/calendar/CalendarEventModel$Attendee;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    const-string v5, ", "

    invoke-virtual {p2, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_3

    :cond_4
    packed-switch p3, :pswitch_data_0

    :goto_4
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    const v6, -0x666667

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    const/16 v7, 0x22

    invoke-virtual {p2, v5, v1, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :pswitch_1
    new-instance v5, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v5}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-virtual {p2, v5, v1, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getResponseFromButtonId(I)I
    .locals 2
    .param p0    # I

    const v1, 0x7f10006f

    if-ne p0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const v1, 0x7f100070

    if-ne p0, v1, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    const v1, 0x7f100071

    if-ne p0, v1, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasEmailableAttendees()Z
    .locals 5

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mAcceptedAttendees:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v3, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/calendar/EventInfoFragment;->mSyncAccountName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/android/calendar/Utils;->isEmailableFrom(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mTentativeAttendees:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v3, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/calendar/EventInfoFragment;->mSyncAccountName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/android/calendar/Utils;->isEmailableFrom(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mNoResponseAttendees:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v3, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/calendar/EventInfoFragment;->mSyncAccountName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/android/calendar/Utils;->isEmailableFrom(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mDeclinedAttendees:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v3, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/calendar/EventInfoFragment;->mSyncAccountName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/android/calendar/Utils;->isEmailableFrom(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private hasEmailableOrganizer()Z
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerEmail:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerEmail:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mSyncAccountName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/calendar/Utils;->isEmailableFrom(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static indexFirstNonWhitespaceChar(Ljava/lang/CharSequence;)I
    .locals 2
    .param p0    # Ljava/lang/CharSequence;

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static indexLastNonWhitespaceChar(Ljava/lang/CharSequence;)I
    .locals 2
    .param p0    # Ljava/lang/CharSequence;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private initAttendeesCursor(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/EventInfoFragment;->mOriginalAttendeeResponse:I

    const-wide/16 v7, -0x1

    iput-wide v7, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAttendeeId:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/EventInfoFragment;->mNumOfAttendees:I

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/EventInfoFragment;->mNumOfAttendees:I

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAcceptedAttendees:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeclinedAttendees:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mTentativeAttendees:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mNoResponseAttendees:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    const/4 v3, 0x4

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    const/4 v3, 0x2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    const/4 v3, 0x3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerDisplayName:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsOrganizer:Z

    if-nez v0, :cond_1

    const v0, 0x7f10009b

    const/4 v3, 0x0

    invoke-direct {p0, p1, v0, v3}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    const v0, 0x7f100074

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerDisplayName:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v3}, Lcom/android/calendar/EventInfoFragment;->setTextCommon(Landroid/view/View;ILjava/lang/CharSequence;)V

    :cond_1
    iget-wide v7, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAttendeeId:J

    const-wide/16 v9, -0x1

    cmp-long v0, v7, v9

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAccount:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v7, v0

    iput-wide v7, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAttendeeId:J

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    const/4 v3, 0x4

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/EventInfoFragment;->mOriginalAttendeeResponse:I

    :goto_0
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-direct {p0, p1}, Lcom/android/calendar/EventInfoFragment;->updateAttendees(Landroid/view/View;)V

    :cond_2
    return-void

    :cond_3
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {}, Lcom/android/calendar/Utils;->isJellybeanOrLater()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    const/4 v3, 0x5

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    const/4 v3, 0x6

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_4
    packed-switch v6, :pswitch_data_0

    :pswitch_0
    iget-object v7, p0, Lcom/android/calendar/EventInfoFragment;->mNoResponseAttendees:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/CalendarEventModel$Attendee;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_1
    iget-object v7, p0, Lcom/android/calendar/EventInfoFragment;->mAcceptedAttendees:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    const/4 v3, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/CalendarEventModel$Attendee;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_2
    iget-object v7, p0, Lcom/android/calendar/EventInfoFragment;->mDeclinedAttendees:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    const/4 v3, 0x2

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/CalendarEventModel$Attendee;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_3
    iget-object v7, p0, Lcom/android/calendar/EventInfoFragment;->mTentativeAttendees:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    const/4 v3, 0x4

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/CalendarEventModel$Attendee;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private initEventCursor()Z
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move v3, v2

    :goto_0
    return v3

    :cond_1
    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-long v4, v1

    iput-wide v4, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/4 v4, 0x2

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsRepeating:Z

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v4, 0xe

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v2, :cond_3

    :goto_2
    iput-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mHasAlarm:Z

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v2, 0xf

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mMaxReminders:I

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v2, 0x10

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarAllowedReminders:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2
.end method

.method private static linkifyTextView(Landroid/widget/TextView;)V
    .locals 27
    .param p0    # Landroid/widget/TextView;

    const-string v24, "user.region"

    const-string v25, "US"

    invoke-static/range {v24 .. v25}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v24, "US"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_2

    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v15

    const/16 v24, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v24

    move-object/from16 v0, v24

    instance-of v0, v0, Landroid/text/Spannable;

    move/from16 v24, v0

    if-eqz v24, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v20

    check-cast v20, Landroid/text/Spannable;

    const/16 v24, 0x0

    invoke-interface/range {v20 .. v20}, Landroid/text/Spannable;->length()I

    move-result v25

    const-class v26, Landroid/text/style/URLSpan;

    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v21

    check-cast v21, [Landroid/text/style/URLSpan;

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_1

    const/16 v24, 0x0

    aget-object v24, v21, v24

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v11

    const/16 v24, 0x0

    aget-object v24, v21, v24

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v10

    invoke-static {v15}, Lcom/android/calendar/EventInfoFragment;->indexFirstNonWhitespaceChar(Ljava/lang/CharSequence;)I

    move-result v24

    move/from16 v0, v24

    if-gt v11, v0, :cond_1

    invoke-static {v15}, Lcom/android/calendar/EventInfoFragment;->indexLastNonWhitespaceChar(Ljava/lang/CharSequence;)I

    move-result v24

    add-int/lit8 v24, v24, 0x1

    move/from16 v0, v24

    if-lt v10, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v24, Lcom/android/calendar/EventInfoFragment;->WILDCARD_PATTERN:Ljava/util/regex/Pattern;

    const-string v25, "geo:0,0?q="

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/16 v24, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    move-result v12

    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/android/calendar/EventInfoFragment;->findNanpPhoneNumbers(Ljava/lang/CharSequence;)[I

    move-result-object v17

    move-object/from16 v0, v23

    instance-of v0, v0, Landroid/text/SpannableString;

    move/from16 v24, v0

    if-eqz v24, :cond_4

    move-object/from16 v20, v23

    check-cast v20, Landroid/text/SpannableString;

    :goto_1
    const/16 v24, 0x0

    invoke-interface/range {v20 .. v20}, Landroid/text/Spannable;->length()I

    move-result v25

    const-class v26, Landroid/text/style/URLSpan;

    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Landroid/text/style/URLSpan;

    const/16 v16, 0x0

    const/4 v13, 0x0

    :goto_2
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    move/from16 v0, v24

    if-ge v13, v0, :cond_9

    mul-int/lit8 v24, v13, 0x2

    aget v22, v17, v24

    mul-int/lit8 v24, v13, 0x2

    add-int/lit8 v24, v24, 0x1

    aget v7, v17, v24

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-static {v0, v8, v1, v7}, Lcom/android/calendar/EventInfoFragment;->spanWillOverlap(Landroid/text/Spannable;[Landroid/text/style/URLSpan;II)Z

    move-result v24

    if-eqz v24, :cond_5

    const-string v24, "EventInfoFragment"

    const/16 v25, 0x2

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v24

    if-eqz v24, :cond_3

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-interface {v0, v1, v7}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v18

    const-string v24, "EventInfoFragment"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Not linkifying "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " as phone number due to overlap"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    :cond_4
    invoke-static/range {v23 .. v23}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v20

    goto :goto_1

    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v9, v22

    :goto_4
    if-ge v9, v7, :cond_8

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/text/Spannable;->charAt(I)C

    move-result v4

    const/16 v24, 0x2b

    move/from16 v0, v24

    if-eq v4, v0, :cond_6

    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v24

    if-eqz v24, :cond_7

    :cond_6
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_7
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_8
    new-instance v19, Landroid/text/style/URLSpan;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "tel:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    const/16 v24, 0x21

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move/from16 v2, v22

    move/from16 v3, v24

    invoke-interface {v0, v1, v2, v7, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    :cond_9
    if-eqz v16, :cond_c

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_a

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v14

    if-eqz v14, :cond_b

    instance-of v0, v14, Landroid/text/method/LinkMovementMethod;

    move/from16 v24, v0

    if-nez v24, :cond_c

    :cond_b
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getLinksClickable()Z

    move-result v24

    if-eqz v24, :cond_c

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_c
    if-nez v12, :cond_0

    if-nez v16, :cond_0

    const-string v24, "EventInfoFragment"

    const/16 v25, 0x2

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v24

    if-eqz v24, :cond_d

    const-string v24, "EventInfoFragment"

    const-string v25, "No linkification matches, using geo default"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    sget-object v24, Lcom/android/calendar/EventInfoFragment;->WILDCARD_PATTERN:Ljava/util/regex/Pattern;

    const-string v25, "geo:0,0?q="

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static loadIntegerArray(Landroid/content/res/Resources;I)Ljava/util/ArrayList;
    .locals 5
    .param p0    # Landroid/content/res/Resources;
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v3

    array-length v2, v3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v4, v3, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static loadStringArray(Landroid/content/res/Resources;I)Ljava/util/ArrayList;
    .locals 3
    .param p0    # Landroid/content/res/Resources;
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private declared-synchronized prepareReminders()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteLabels:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodLabels:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarAllowedReminders:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070003

    invoke-static {v0, v1}, Lcom/android/calendar/EventInfoFragment;->loadIntegerArray(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    const v1, 0x7f070002

    invoke-static {v0, v1}, Lcom/android/calendar/EventInfoFragment;->loadStringArray(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteLabels:Ljava/util/ArrayList;

    const v1, 0x7f070001

    invoke-static {v0, v1}, Lcom/android/calendar/EventInfoFragment;->loadIntegerArray(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    const/high16 v1, 0x7f070000

    invoke-static {v0, v1}, Lcom/android/calendar/EventInfoFragment;->loadStringArray(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodLabels:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarAllowedReminders:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodLabels:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarAllowedReminders:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/android/calendar/event/EventViewUtils;->reduceMethodList(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->updateAddReminderBtnVisibility()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private removeDuplicate(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$ReminderEntry;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x2

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    invoke-virtual {v2, v0}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_2
    move-object v2, v0

    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method private saveReminders()Z
    .locals 3

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/android/calendar/event/EventViewUtils;->reminderItemsToReminders(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    iget-wide v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/EventInfoFragment;->saveRemindersForEvent(J)Z

    move-result v0

    return v0
.end method

.method private saveRemindersForEvent(J)Z
    .locals 16
    .param p1    # J

    const-string v3, "EventInfoFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Save reminders for event: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/EventInfoFragment;->removeDuplicate(Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/EventInfoFragment;->removeDuplicate(Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/EventInfoFragment;->mUnsupportedReminders:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/EventInfoFragment;->mUnsupportedReminders:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    const/4 v7, 0x0

    move-wide/from16 v3, p1

    invoke-static/range {v2 .. v7}, Lcom/android/calendar/event/EditEventHelper;->saveReminders(Ljava/util/ArrayList;JLjava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v12

    if-nez v12, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v8, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v6

    const-wide/16 v8, 0x0

    move-object v7, v2

    invoke-virtual/range {v3 .. v9}, Lcom/android/calendar/AsyncQueryService;->startBatch(ILjava/lang/Object;Ljava/lang/String;Ljava/util/ArrayList;J)V

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, p1

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_2

    const/4 v13, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/EventInfoFragment;->mHasAlarm:Z

    if-eq v13, v3, :cond_1

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "hasAlarm"

    if-eqz v13, :cond_3

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    invoke-virtual/range {v3 .. v11}, Lcom/android/calendar/AsyncQueryService;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v13, 0x0

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method private saveResponse()Z
    .locals 9

    const/4 v8, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f10006e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioGroup;

    invoke-virtual {v6}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    invoke-static {v1}, Lcom/android/calendar/EventInfoFragment;->getResponseFromButtonId(I)I

    move-result v5

    if-eqz v5, :cond_0

    iget v1, p0, Lcom/android/calendar/EventInfoFragment;->mOriginalAttendeeResponse:I

    if-eq v5, v1, :cond_0

    iget-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAttendeeId:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsRepeating:Z

    if-nez v1, :cond_2

    iget-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    iget-wide v3, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAttendeeId:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/EventInfoFragment;->updateResponse(JJI)V

    move v0, v8

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEditResponseHelper:Lcom/android/calendar/EditResponseHelper;

    invoke-virtual {v1}, Lcom/android/calendar/EditResponseHelper;->getWhichEvents()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    const-string v1, "EventInfoFragment"

    const-string v2, "Unexpected choice for updating invitation response"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    iget-wide v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    invoke-direct {p0, v0, v1, v5}, Lcom/android/calendar/EventInfoFragment;->createExceptionResponse(JI)V

    move v0, v8

    goto :goto_0

    :pswitch_2
    iget-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    iget-wide v3, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAttendeeId:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/EventInfoFragment;->updateResponse(JJI)V

    move v0, v8

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private sendAccessibilityEvent()V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "accessibility"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v5, 0x8

    invoke-static {v5}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/view/accessibility/AccessibilityRecord;->setClassName(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityRecord;->getText()Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/EventInfoFragment;->mTitle:Landroid/widget/TextView;

    invoke-direct {p0, v4, v5, v7}, Lcom/android/calendar/EventInfoFragment;->addFieldToAccessibilityEvent(Ljava/util/List;Landroid/widget/TextView;Lcom/android/calendar/ExpandableTextView;)V

    iget-object v5, p0, Lcom/android/calendar/EventInfoFragment;->mWhenDateTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v5, v7}, Lcom/android/calendar/EventInfoFragment;->addFieldToAccessibilityEvent(Ljava/util/List;Landroid/widget/TextView;Lcom/android/calendar/ExpandableTextView;)V

    iget-object v5, p0, Lcom/android/calendar/EventInfoFragment;->mWhere:Landroid/widget/TextView;

    invoke-direct {p0, v4, v5, v7}, Lcom/android/calendar/EventInfoFragment;->addFieldToAccessibilityEvent(Ljava/util/List;Landroid/widget/TextView;Lcom/android/calendar/ExpandableTextView;)V

    iget-object v5, p0, Lcom/android/calendar/EventInfoFragment;->mDesc:Lcom/android/calendar/ExpandableTextView;

    invoke-direct {p0, v4, v7, v5}, Lcom/android/calendar/EventInfoFragment;->addFieldToAccessibilityEvent(Ljava/util/List;Landroid/widget/TextView;Lcom/android/calendar/ExpandableTextView;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f10006e

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioGroup;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v2

    const/4 v5, -0x1

    if-eq v2, v5, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f10006d

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioButton;

    check-cast v5, Landroid/widget/RadioButton;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto/16 :goto_0
.end method

.method private sendAccessibilityEventIfQueryDone(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I

    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I

    const/16 v1, 0x3f

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->sendAccessibilityEvent()V

    :cond_0
    return-void
.end method

.method private setDialogSize(Landroid/content/res/Resources;)V
    .locals 1
    .param p1    # Landroid/content/res/Resources;

    const v0, 0x7f0a0023

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/EventInfoFragment;->mDialogWidth:I

    const v0, 0x7f0a0024

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/calendar/EventInfoFragment;->mDialogHeight:I

    return-void
.end method

.method private setSNSData(Landroid/view/View;Landroid/widget/TextView;)V
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/widget/TextView;

    iget-object v8, p0, Lcom/android/calendar/EventInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/mediatek/calendar/SNSCalendarDataHelper;->initialize(Landroid/content/Context;)Z

    iget-object v8, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v9, 0x14

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v8, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/4 v9, 0x6

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Lcom/mediatek/calendar/SNSCalendarDataHelper;->isAccountTypeSupported(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/android/calendar/EventInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v8, v0}, Lcom/mediatek/calendar/SNSCalendarDataHelper;->getAccountIcon(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v8, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/4 v9, 0x4

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    long-to-int v2, v8

    iget-object v8, p0, Lcom/android/calendar/EventInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v8, v2}, Lcom/mediatek/calendar/SNSCalendarDataHelper;->getBirthdayString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v8, " "

    invoke-virtual {p2, v8}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    :cond_0
    const/4 v5, 0x1

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/lit8 v6, v8, -0x1

    const v8, 0x7f100093

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    if-eqz v4, :cond_1

    invoke-virtual {v4, v6}, Landroid/view/View;->setMinimumWidth(I)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setMinimumHeight(I)V

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v8, Lcom/android/calendar/EventInfoFragment$13;

    invoke-direct {v8, p0, v0, v7}, Lcom/android/calendar/EventInfoFragment$13;-><init>(Lcom/android/calendar/EventInfoFragment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method private setTextCommon(Landroid/view/View;ILjava/lang/CharSequence;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mAllDay:Z

    if-eqz v1, :cond_0

    const v1, 0x7f100014

    if-ne p2, v1, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/EventInfoFragment;->setSNSData(Landroid/view/View;Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method private setVisibilityCommon(Landroid/view/View;II)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private static spanWillOverlap(Landroid/text/Spannable;[Landroid/text/style/URLSpan;II)Z
    .locals 7
    .param p0    # Landroid/text/Spannable;
    .param p1    # [Landroid/text/style/URLSpan;
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x0

    if-ne p2, p3, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    move-object v0, p1

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    invoke-interface {p0, v5}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    invoke-interface {p0, v5}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    if-lt p2, v2, :cond_2

    if-lt p2, v1, :cond_3

    :cond_2
    if-le p3, v2, :cond_4

    if-gt p3, v1, :cond_4

    :cond_3
    const/4 v6, 0x1

    goto :goto_0

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private updateAddReminderBtnVisibility()V
    .locals 2

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/EventInfoFragment;->mMaxReminders:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAddReminderBtn:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAddReminderBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateAttendees(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/16 v4, 0x8

    const v3, 0x7f10009d

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAcceptedAttendees:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mDeclinedAttendees:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mTentativeAttendees:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mNoResponseAttendees:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLongAttendees:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->clearAttendees()V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLongAttendees:Lcom/android/calendar/event/AttendeesView;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mAcceptedAttendees:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/AttendeesView;->addAttendees(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLongAttendees:Lcom/android/calendar/event/AttendeesView;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mDeclinedAttendees:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/AttendeesView;->addAttendees(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLongAttendees:Lcom/android/calendar/event/AttendeesView;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mTentativeAttendees:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/AttendeesView;->addAttendees(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLongAttendees:Lcom/android/calendar/event/AttendeesView;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mNoResponseAttendees:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/AttendeesView;->addAttendees(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLongAttendees:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, v2}, Lcom/android/calendar/event/AttendeesView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLongAttendees:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->hasEmailableAttendees()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    invoke-direct {p0, v0, v3, v2}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->emailAttendeesButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->emailAttendeesButton:Landroid/widget/Button;

    const v1, 0x7f0c007e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mLongAttendees:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->hasEmailableOrganizer()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    invoke-direct {p0, v0, v3, v2}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->emailAttendeesButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->emailAttendeesButton:Landroid/widget/Button;

    const v1, 0x7f0c007f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    invoke-direct {p0, v0, v3, v4}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    goto :goto_1
.end method

.method private updateCalendar(Landroid/view/View;)V
    .locals 13
    .param p1    # Landroid/view/View;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAccount:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_0

    const-string v11, ""

    :cond_0
    iput-object v11, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAccount:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mOwnerCanRespond:Z

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mSyncAccountName:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    const/16 v1, 0x20

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/EventInfoFragment;->CALENDARS_PROJECTION:[Ljava/lang/String;

    const-string v5, "visible=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v12, "1"

    aput-object v12, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/AsyncQueryService;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerEmail:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAccount:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerEmail:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsOrganizer:Z

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerEmail:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerEmail:Ljava/lang/String;

    const-string v1, "calendar.google.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerEmail:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerDisplayName:Ljava/lang/String;

    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsOrganizer:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerDisplayName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    const v0, 0x7f100074

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventOrganizerDisplayName:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/android/calendar/EventInfoFragment;->setTextCommon(Landroid/view/View;ILjava/lang/CharSequence;)V

    const v0, 0x7f10009b

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    :goto_1
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v1, 0xc

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mHasAttendeeData:Z

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x1f4

    if-lt v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyCalendar:Z

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyCalendar:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsOrganizer:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyEvent:Z

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_d

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsBusyFreeCalendar:Z

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsBusyFreeCalendar:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v1, 0x7f100095

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Landroid/view/View;->setEnabled(Z)V

    new-instance v0, Lcom/android/calendar/EventInfoFragment$12;

    invoke-direct {v0, p0}, Lcom/android/calendar/EventInfoFragment$12;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyCalendar:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v1, 0x7f100096

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_3

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/view/View;->setEnabled(Z)V

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyEvent:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v1, 0x7f100095

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_4

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/view/View;->setEnabled(Z)V

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsTabletConfig:Z

    if-eqz v0, :cond_6

    :cond_5
    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    if-nez v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mMenu:Landroid/view/Menu;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_7
    :goto_6
    return-void

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    const v0, 0x7f10009b

    const/16 v1, 0x8

    invoke-direct {p0, p1, v0, v1}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    goto/16 :goto_1

    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_d
    const/4 v0, 0x0

    goto :goto_5

    :cond_e
    const v0, 0x7f100032

    const/16 v1, 0x8

    invoke-direct {p0, p1, v0, v1}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/android/calendar/EventInfoFragment;->sendAccessibilityEventIfQueryDone(I)V

    goto :goto_6
.end method

.method private updateCustomAppButton()V
    .locals 15

    iget-object v11, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v12, 0x7f1000a0

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    iget-object v11, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v12, 0x7f10009f

    const/16 v13, 0x8

    invoke-direct {p0, v11, v12, v13}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    :goto_1
    return-void

    :cond_1
    iget-object v11, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v12, 0x11

    invoke-interface {v11, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v11, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v12, 0x12

    invoke-interface {v11, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    iget-object v11, p0, Lcom/android/calendar/EventInfoFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    if-eqz v9, :cond_0

    const/4 v11, 0x0

    :try_start_0
    invoke-virtual {v9, v0, v11}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    if-eqz v5, :cond_0

    sget-object v11, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v12, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    invoke-static {v11, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    new-instance v6, Landroid/content/Intent;

    const-string v11, "android.provider.calendar.action.HANDLE_CUSTOM_EVENT"

    invoke-direct {v6, v11, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "customAppUri"

    invoke-virtual {v6, v11, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "beginTime"

    iget-wide v12, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    invoke-virtual {v6, v11, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/4 v11, 0x0

    invoke-virtual {v9, v6, v11}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-virtual {v9, v5}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v8}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v11, 0x0

    const/4 v12, 0x0

    sget v13, Lcom/android/calendar/EventInfoFragment;->mCustomAppIconSize:I

    sget v14, Lcom/android/calendar/EventInfoFragment;->mCustomAppIconSize:I

    invoke-virtual {v4, v11, v12, v13, v14}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    const/4 v11, 0x1

    aget-object v11, v2, v11

    const/4 v12, 0x2

    aget-object v12, v2, v12

    const/4 v13, 0x3

    aget-object v13, v2, v13

    invoke-virtual {v8, v4, v11, v12, v13}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_2
    invoke-virtual {v9, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    new-instance v11, Lcom/android/calendar/EventInfoFragment$11;

    invoke-direct {v11, p0, v6}, Lcom/android/calendar/EventInfoFragment$11;-><init>(Lcom/android/calendar/EventInfoFragment;Landroid/content/Intent;)V

    invoke-virtual {v8, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v11, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v12, 0x7f10009f

    const/4 v13, 0x0

    invoke-direct {p0, v11, v12, v13}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    goto/16 :goto_1

    :catch_0
    move-exception v3

    goto/16 :goto_0

    :cond_4
    if-nez v4, :cond_3

    goto/16 :goto_0
.end method

.method private updateEvent(Landroid/view/View;)V
    .locals 28
    .param p1    # Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    if-eqz v3, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v11

    if-eqz v11, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0c0047

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/4 v4, 0x3

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_b

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/calendar/EventInfoFragment;->mAllDay:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v4, 0x9

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v4, 0x8

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/4 v4, 0x2

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/4 v4, 0x7

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v4, 0xb

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/android/calendar/Utils;->getDisplayColorFromColor(I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/EventInfoFragment;->mColor:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mHeadlines:Landroid/view/View;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/EventInfoFragment;->mColor:I

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    if-eqz v16, :cond_4

    const v3, 0x7f100014

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v3, v2}, Lcom/android/calendar/EventInfoFragment;->setTextCommon(Landroid/view/View;ILjava/lang/CharSequence;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/EventInfoFragment;->mTZUpdater:Ljava/lang/Runnable;

    invoke-static {v3, v4}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/calendar/EventInfoFragment;->mEndMillis:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/android/calendar/EventInfoFragment;->mAllDay:Z

    invoke-static/range {v3 .. v11}, Lcom/android/calendar/Utils;->getDisplayedDatetime(JJJLjava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/EventInfoFragment;->mAllDay:Z

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    move-object/from16 v0, v18

    invoke-static {v3, v4, v9, v0}, Lcom/android/calendar/Utils;->getDisplayedTimezone(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    :cond_5
    if-nez v15, :cond_c

    const v3, 0x7f100097

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v14}, Lcom/android/calendar/EventInfoFragment;->setTextCommon(Landroid/view/View;ILjava/lang/CharSequence;)V

    :goto_2
    const/16 v22, 0x0

    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    new-instance v17, Lcom/android/calendarcommon2/EventRecurrence;

    invoke-direct/range {v17 .. v17}, Lcom/android/calendarcommon2/EventRecurrence;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/calendarcommon2/EventRecurrence;->parse(Ljava/lang/String;)V

    new-instance v12, Landroid/text/format/Time;

    invoke-direct {v12, v9}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    invoke-virtual {v12, v3, v4}, Landroid/text/format/Time;->set(J)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/calendar/EventInfoFragment;->mAllDay:Z

    if-eqz v3, :cond_6

    const-string v3, "UTC"

    iput-object v3, v12, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    :cond_6
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/android/calendarcommon2/EventRecurrence;->setStartDate(Landroid/text/format/Time;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/calendar/EventRecurrenceFormatter;->getRepeatString(Landroid/content/res/Resources;Lcom/android/calendarcommon2/EventRecurrence;)Ljava/lang/String;

    move-result-object v22

    :cond_7
    if-nez v22, :cond_d

    const v3, 0x7f100098

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    if-eqz v20, :cond_8

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_e

    :cond_8
    const v3, 0x7f100016

    const/16 v4, 0x8

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    :cond_9
    :goto_4
    if-eqz v13, :cond_a

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mDesc:Lcom/android/calendar/ExpandableTextView;

    invoke-virtual {v3, v13}, Lcom/android/calendar/ExpandableTextView;->setText(Ljava/lang/String;)V

    :cond_a
    invoke-static {}, Lcom/android/calendar/Utils;->isJellybeanOrLater()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/EventInfoFragment;->updateCustomAppButton()V

    goto/16 :goto_0

    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_c
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v26

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    new-instance v24, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v24

    invoke-direct {v0, v14}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v27, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f080044

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    move-object/from16 v0, v27

    invoke-direct {v0, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x12

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    const v3, 0x7f100097

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v3, v2}, Lcom/android/calendar/EventInfoFragment;->setTextCommon(Landroid/view/View;ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_d
    const v3, 0x7f100098

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v3, v2}, Lcom/android/calendar/EventInfoFragment;->setTextCommon(Landroid/view/View;ILjava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/EventInfoFragment;->mWhere:Landroid/widget/TextView;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    const/4 v3, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :try_start_0
    invoke-static/range {v25 .. v25}, Lcom/android/calendar/EventInfoFragment;->linkifyTextView(Landroid/widget/TextView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_5
    new-instance v3, Lcom/android/calendar/EventInfoFragment$10;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/android/calendar/EventInfoFragment$10;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_4

    :catch_0
    move-exception v19

    const-string v3, "EventInfoFragment"

    const-string v4, "Linkification failed"

    move-object/from16 v0, v19

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5
.end method

.method private updateMenu()V
    .locals 4

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mMenu:Landroid/view/Menu;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mMenu:Landroid/view/Menu;

    const v3, 0x7f1000d0

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mMenu:Landroid/view/Menu;

    const v3, 0x7f1000cf

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v0, :cond_2

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyCalendar:Z

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyCalendar:Z

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_2
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyEvent:Z

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyEvent:Z

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private updateResponse(JJI)V
    .locals 9
    .param p1    # J
    .param p3    # J
    .param p5    # I

    const/4 v2, 0x0

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAccount:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "attendeeEmail"

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAccount:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "attendeeStatus"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p3, p4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    invoke-virtual {v1}, Lcom/android/calendar/AsyncQueryService;->getNextToken()I

    move-result v1

    const-wide/16 v7, 0x0

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/AsyncQueryService;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    return-void
.end method

.method private updateTitle()V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyCalendar:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsOrganizer:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0c006a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0c0069

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateUIReminder()V
    .locals 14

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/android/calendar/event/EventViewUtils;->reminderItemsToReminders(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/android/calendar/EventInfoFragment;->removeDuplicate(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    const v1, 0x7f10007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    if-eqz v12, :cond_0

    invoke-virtual {v12}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    iget-object v13, p0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteLabels:Ljava/util/ArrayList;

    invoke-virtual {v8}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->getMinutes()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/event/EventViewUtils;->addMinutesToList(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteLabels:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodLabels:Ljava/util/ArrayList;

    const v9, 0x7fffffff

    iget-object v10, p0, Lcom/android/calendar/EventInfoFragment;->mReminderChangeListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object v2, p0

    invoke-static/range {v0 .. v10}, Lcom/android/calendar/event/EventViewUtils;->addReminder(Landroid/app/Activity;Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/calendar/CalendarEventModel$ReminderEntry;ILandroid/widget/AdapterView$OnItemSelectedListener;)Z

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public eventsChanged()V
    .locals 0

    return-void
.end method

.method public getEndMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/android/calendar/EventInfoFragment;->mEndMillis:J

    return-wide v0
.end method

.method public getEventId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    return-wide v0
.end method

.method public getStartMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    return-wide v0
.end method

.method public getSupportedEventTypes()J
    .locals 2

    const-wide/16 v0, 0x80

    return-wide v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public handleEvent(Lcom/android/calendar/CalendarController$EventInfo;)V
    .locals 4
    .param p1    # Lcom/android/calendar/CalendarController$EventInfo;

    iget-wide v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/EventInfoFragment;->reloadEvents()V

    :cond_0
    return-void
.end method

.method public initReminders(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 17
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mUnsupportedReminders:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    :goto_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const/4 v1, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mUnsupportedReminders:Ljava/util/ArrayList;

    invoke-static {v14, v13}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->valueOf(II)Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    invoke-static {v14, v13}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->valueOf(II)Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/EventInfoFragment;->mUserModifiedReminders:Z

    if-eqz v1, :cond_3

    :cond_2
    :goto_1
    return-void

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    const v2, 0x7f10007f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/LinearLayout;

    if-eqz v15, :cond_4

    invoke-virtual {v15}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/calendar/EventInfoFragment;->mHasAlarm:Z

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteLabels:Ljava/util/ArrayList;

    invoke-virtual {v9}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->getMinutes()I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/android/calendar/event/EventViewUtils;->addMinutesToList(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    goto :goto_2

    :cond_6
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteLabels:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodLabels:Ljava/util/ArrayList;

    const v10, 0x7fffffff

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/EventInfoFragment;->mReminderChangeListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object/from16 v3, p0

    invoke-static/range {v1 .. v11}, Lcom/android/calendar/event/EventViewUtils;->addReminder(Landroid/app/Activity;Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/calendar/CalendarEventModel$ReminderEntry;ILandroid/widget/AdapterView$OnItemSelectedListener;)Z

    goto :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/EventInfoFragment;->mMaxReminders:I

    invoke-static {v1, v2, v3}, Lcom/android/calendar/event/EventViewUtils;->updateAddReminderButton(Landroid/view/View;Ljava/util/ArrayList;I)V

    goto/16 :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/calendar/EventInfoFragment$3;

    invoke-direct {v0, p0}, Lcom/android/calendar/EventInfoFragment$3;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mReminderChangeListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz p1, :cond_0

    const-string v0, "key_fragment_is_dialog"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    const-string v0, "key_window_style"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->applyDialogParams()V

    :cond_1
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    new-instance v0, Lcom/android/calendar/EditResponseHelper;

    invoke-direct {v0, p1}, Lcom/android/calendar/EditResponseHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEditResponseHelper:Lcom/android/calendar/EditResponseHelper;

    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeeResponseFromIntent:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEditResponseHelper:Lcom/android/calendar/EditResponseHelper;

    invoke-virtual {v0, v1}, Lcom/android/calendar/EditResponseHelper;->setWhichEvents(I)V

    :cond_0
    new-instance v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/EventInfoFragment$QueryHandler;-><init>(Lcom/android/calendar/EventInfoFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, v1}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    :cond_1
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2
    .param p1    # Landroid/widget/RadioGroup;
    .param p2    # I

    invoke-static {p2}, Lcom/android/calendar/EventInfoFragment;->getResponseFromButtonId(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/EventInfoFragment;->mUserSetResponse:I

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsRepeating:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mOriginalAttendeeResponse:I

    invoke-static {v0}, Lcom/android/calendar/EventInfoFragment;->findButtonIdForResponse(I)I

    move-result v0

    if-eq p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEditResponseHelper:Lcom/android/calendar/EditResponseHelper;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEditResponseHelper:Lcom/android/calendar/EditResponseHelper;

    invoke-virtual {v1}, Lcom/android/calendar/EditResponseHelper;->getWhichEvents()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/EditResponseHelper;->showDialog(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mUserModifiedReminders:Z

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    iget v4, p0, Lcom/android/calendar/EventInfoFragment;->mMaxReminders:I

    invoke-static {v2, v3, v4}, Lcom/android/calendar/event/EventViewUtils;->updateAddReminderButton(Landroid/view/View;Ljava/util/ArrayList;I)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsTabletConfig:Z

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    if-nez v0, :cond_2

    :cond_1
    const v0, 0x7f0f0002

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment;->mMenu:Landroid/view/Menu;

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->updateMenu()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    invoke-static {v0, v1, v2}, Lcom/mediatek/calendar/extension/ExtensionFactory;->getEventInfoOptionsMenuExt(Landroid/content/Context;J)Lcom/mediatek/calendar/extension/IOptionsMenuExt;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mOptionsMenuExt:Lcom/mediatek/calendar/extension/IOptionsMenuExt;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mOptionsMenuExt:Lcom/mediatek/calendar/extension/IOptionsMenuExt;

    invoke-interface {v0, p1}, Lcom/mediatek/calendar/extension/IOptionsMenuExt;->onCreateOptionsMenu(Landroid/view/Menu;)V

    :cond_2
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 14
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    if-eqz p3, :cond_0

    const-string v1, "key_fragment_is_dialog"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    const-string v1, "key_window_style"

    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    const-string v1, "key_delete_dialog_visible"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteDialogVisible:Z

    const-string v1, "key_attendee_response"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mUserSetResponse:I

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mEditResponseHelper:Lcom/android/calendar/EditResponseHelper;

    const-string v2, "key_which_events"

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/EditResponseHelper;->setWhichEvents(I)V

    const-string v1, "key_original_reminders"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    const-string v1, "key_reminders"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mSavedReminders:Ljava/util/ArrayList;

    const-string v1, "key_which_delete"

    const/4 v2, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mWhichDelete:I

    :cond_0
    iget v1, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    const v1, 0x7f040027

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    :goto_0
    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f100091

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f10008f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mLoadingMsgView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f100014

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f100097

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mWhenDateTime:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f100016

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mWhere:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f10007a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/ExpandableTextView;

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mDesc:Lcom/android/calendar/ExpandableTextView;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f100092

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mHeadlines:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f1000a1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/event/AttendeesView;

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mLongAttendees:Lcom/android/calendar/event/AttendeesView;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    const v2, 0x7f090006

    invoke-static {v1, v2}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsTabletConfig:Z

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mUri:Landroid/net/Uri;

    if-nez v1, :cond_1

    const-string v1, "key_event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mUri:Landroid/net/Uri;

    const-string v1, "key_start_millis"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    const-string v1, "key_end_millis"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mEndMillis:J

    :cond_1
    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    const-string v2, "Alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mAnimateAlpha:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mAnimateAlpha:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mAnimateAlpha:Landroid/animation/ObjectAnimator;

    new-instance v2, Lcom/android/calendar/EventInfoFragment$4;

    invoke-direct {v2, p0}, Lcom/android/calendar/EventInfoFragment$4;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mLoadingMsgView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mLoadingMsgView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mLoadingMsgAlphaUpdater:Ljava/lang/Runnable;

    const-wide/16 v3, 0x258

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/EventInfoFragment;->mUri:Landroid/net/Uri;

    sget-object v5, Lcom/android/calendar/EventInfoFragment;->EVENT_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/android/calendar/AsyncQueryService;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f100096

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    new-instance v1, Lcom/android/calendar/EventInfoFragment$5;

    invoke-direct {v1, p0}, Lcom/android/calendar/EventInfoFragment$5;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    invoke-virtual {v10, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsTabletConfig:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f1000a2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    new-instance v1, Lcom/android/calendar/EventInfoFragment$6;

    invoke-direct {v1, p0}, Lcom/android/calendar/EventInfoFragment$6;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    invoke-virtual {v11, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsTabletConfig:Z

    if-eqz v1, :cond_4

    :cond_3
    iget v1, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    if-nez v1, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f100094

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f10009e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->emailAttendeesButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->emailAttendeesButton:Landroid/widget/Button;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->emailAttendeesButton:Landroid/widget/Button;

    new-instance v2, Lcom/android/calendar/EventInfoFragment$7;

    invoke-direct {v2, p0}, Lcom/android/calendar/EventInfoFragment$7;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    const v2, 0x7f100080

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mAddReminderBtn:Landroid/widget/Button;

    new-instance v9, Lcom/android/calendar/EventInfoFragment$8;

    invoke-direct {v9, p0}, Lcom/android/calendar/EventInfoFragment$8;-><init>(Lcom/android/calendar/EventInfoFragment;)V

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mAddReminderBtn:Landroid/widget/Button;

    invoke-virtual {v1, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/calendar/GeneralPreferences;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v13

    const-string v1, "preferences_default_reminder"

    const-string v2, "-1"

    invoke-interface {v13, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/EventInfoFragment;->mDefaultReminderMinutes:I

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->prepareReminders()V

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    return-object v1

    :cond_7
    const v1, 0x7f040026

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    goto/16 :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data
.end method

.method public onDeleteStarted()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventDeletionStarted:Z

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 7

    const v6, 0x7f0c0064

    const/4 v5, 0x0

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mEventDeletionStarted:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mNeedSaveEvent:Z

    if-eqz v2, :cond_0

    iput-boolean v5, p0, Lcom/android/calendar/EventInfoFragment;->mNeedSaveEvent:Z

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->saveResponse()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mIsRepeating:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mEditResponseHelper:Lcom/android/calendar/EditResponseHelper;

    invoke-virtual {v2}, Lcom/android/calendar/EditResponseHelper;->getWhichEvents()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    invoke-static {v2, v3, v4}, Lcom/android/calendar/event/EventViewUtils;->reminderItemsToReminders(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->saveReminders()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 12
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v10

    const v2, 0x102002c

    if-ne v10, v2, :cond_1

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/Utils;->returnToCalendarHome(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    move v0, v1

    goto :goto_0

    :cond_1
    const v2, 0x7f1000cf

    if-ne v10, v2, :cond_4

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->saveReminders()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0c0064

    invoke-static {v2, v3, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v11

    new-instance v9, Landroid/content/Intent;

    const-string v0, "android.intent.action.EDIT"

    invoke-direct {v9, v0, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v0, "beginTime"

    iget-wide v2, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    invoke-virtual {v9, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v0, "endTime"

    iget-wide v2, p0, Lcom/android/calendar/EventInfoFragment;->mEndMillis:J

    invoke-virtual {v9, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v0, "allDay"

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mAllDay:Z

    invoke-virtual {v9, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v9, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v0, "editMode"

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v9}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_3
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    :cond_4
    const v0, 0x7f1000d0

    if-ne v10, v0, :cond_5

    new-instance v0, Lcom/android/calendar/DeleteEventHelper;

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v2, v3, v1}, Lcom/android/calendar/DeleteEventHelper;-><init>(Landroid/content/Context;Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    invoke-virtual {v0, p0}, Lcom/android/calendar/DeleteEventHelper;->setDeleteNotificationListener(Lcom/android/calendar/DeleteEventHelper$DeleteNotifyListener;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->createDeleteOnDismissListener()Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/calendar/DeleteEventHelper;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iput-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteDialogVisible:Z

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    iget-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    iget-wide v3, p0, Lcom/android/calendar/EventInfoFragment;->mEndMillis:J

    iget-wide v5, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    const/4 v7, -0x1

    iget-object v8, p0, Lcom/android/calendar/EventInfoFragment;->onDeleteRunnable:Ljava/lang/Runnable;

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/DeleteEventHelper;->delete(JJJILjava/lang/Runnable;)V

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsTabletConfig:Z

    if-eqz v0, :cond_7

    :cond_6
    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    if-nez v0, :cond_3

    :cond_7
    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mOptionsMenuExt:Lcom/mediatek/calendar/extension/IOptionsMenuExt;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/mediatek/calendar/extension/IOptionsMenuExt;->onOptionsItemSelected(I)Z

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsPaused:Z

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->onDeleteRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteDialogVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    invoke-virtual {v0}, Lcom/android/calendar/DeleteEventHelper;->getWhichDelete()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/EventInfoFragment;->mWhichDelete:I

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    invoke-virtual {v0}, Lcom/android/calendar/DeleteEventHelper;->dismissAlertDialog()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 9

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/calendar/EventInfoFragment;->setDialogSize(Landroid/content/res/Resources;)V

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->applyDialogParams()V

    :cond_0
    iget v1, p0, Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I

    const/16 v2, 0x3f

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mIsBusyFreeCalendar:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/android/calendar/EventInfoFragment;->initAttendeesCursor(Landroid/view/View;)V

    :cond_1
    iput-boolean v0, p0, Lcom/android/calendar/EventInfoFragment;->mIsPaused:Z

    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mDismissOnResume:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->onDeleteRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    iget-boolean v1, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteDialogVisible:Z

    if-eqz v1, :cond_4

    new-instance v1, Lcom/android/calendar/DeleteEventHelper;

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    iget-boolean v4, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    if-nez v4, :cond_3

    iget-boolean v4, p0, Lcom/android/calendar/EventInfoFragment;->mIsTabletConfig:Z

    if-nez v4, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-direct {v1, v2, v3, v0}, Lcom/android/calendar/DeleteEventHelper;-><init>(Landroid/content/Context;Landroid/app/Activity;Z)V

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    invoke-direct {p0}, Lcom/android/calendar/EventInfoFragment;->createDeleteOnDismissListener()Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/DeleteEventHelper;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteHelper:Lcom/android/calendar/DeleteEventHelper;

    iget-wide v1, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    iget-wide v3, p0, Lcom/android/calendar/EventInfoFragment;->mEndMillis:J

    iget-wide v5, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    iget v7, p0, Lcom/android/calendar/EventInfoFragment;->mWhichDelete:I

    iget-object v8, p0, Lcom/android/calendar/EventInfoFragment;->onDeleteRunnable:Ljava/lang/Runnable;

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/DeleteEventHelper;->delete(JJJILjava/lang/Runnable;)V

    :cond_4
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const-string v1, "key_event_id"

    iget-wide v2, p0, Lcom/android/calendar/EventInfoFragment;->mEventId:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "key_start_millis"

    iget-wide v2, p0, Lcom/android/calendar/EventInfoFragment;->mStartMillis:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "key_end_millis"

    iget-wide v2, p0, Lcom/android/calendar/EventInfoFragment;->mEndMillis:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "key_fragment_is_dialog"

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mIsDialog:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "key_window_style"

    iget v2, p0, Lcom/android/calendar/EventInfoFragment;->mWindowStyle:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "key_delete_dialog_visible"

    iget-boolean v2, p0, Lcom/android/calendar/EventInfoFragment;->mDeleteDialogVisible:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget v1, p0, Lcom/android/calendar/EventInfoFragment;->mUserSetResponse:I

    if-eqz v1, :cond_0

    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mUserSetResponse:I

    :goto_0
    const-string v1, "key_attendee_response"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "key_which_events"

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mEditResponseHelper:Lcom/android/calendar/EditResponseHelper;

    invoke-virtual {v2}, Lcom/android/calendar/EditResponseHelper;->getWhichEvents()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "key_original_reminders"

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mOriginalReminders:Ljava/util/ArrayList;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    iget-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    invoke-static {v1, v2, v3}, Lcom/android/calendar/event/EventViewUtils;->reminderItemsToReminders(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    const-string v1, "key_reminders"

    iget-object v2, p0, Lcom/android/calendar/EventInfoFragment;->mReminders:Ljava/util/ArrayList;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "key_which_delete"

    iget v2, p0, Lcom/android/calendar/EventInfoFragment;->mWhichDelete:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void

    :cond_0
    iget v1, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeeResponseFromIntent:I

    if-eqz v1, :cond_1

    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeeResponseFromIntent:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/calendar/EventInfoFragment;->mOriginalAttendeeResponse:I

    goto :goto_0
.end method

.method public reloadEvents()V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/android/calendar/EventInfoFragment;->mUri:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/EventInfoFragment;->EVENT_PROJECTION:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/AsyncQueryService;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public restoreReminders(Ljava/util/ArrayList;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$ReminderEntry;",
            ">;)V"
        }
    .end annotation

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mUnsupportedReminders:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-nez v14, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v12, 0x0

    :goto_1
    if-ge v12, v14, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->getMinutes()I

    move-result v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    invoke-virtual {v1}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->getMethod()I

    move-result v15

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mUnsupportedReminders:Ljava/util/ArrayList;

    move/from16 v0, v16

    invoke-static {v0, v15}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->valueOf(II)Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_1
    move/from16 v0, v16

    invoke-static {v0, v15}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->valueOf(II)Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-static/range {v18 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    const v2, 0x7f10007f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    if-eqz v17, :cond_3

    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    :cond_4
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteLabels:Ljava/util/ArrayList;

    invoke-virtual {v9}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->getMinutes()I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/android/calendar/event/EventViewUtils;->addMinutesToList(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    goto :goto_3

    :cond_5
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteValues:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMinuteLabels:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodValues:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/EventInfoFragment;->mReminderMethodLabels:Ljava/util/ArrayList;

    const v10, 0x7fffffff

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/EventInfoFragment;->mReminderChangeListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object/from16 v3, p0

    invoke-static/range {v1 .. v11}, Lcom/android/calendar/event/EventViewUtils;->addReminder(Landroid/app/Activity;Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/calendar/CalendarEventModel$ReminderEntry;ILandroid/widget/AdapterView$OnItemSelectedListener;)Z

    goto :goto_4

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment;->mReminderViews:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/EventInfoFragment;->mMaxReminders:I

    invoke-static {v1, v2, v3}, Lcom/android/calendar/event/EventViewUtils;->updateAddReminderButton(Landroid/view/View;Ljava/util/ArrayList;I)V

    goto/16 :goto_0
.end method

.method public setDialogParams(III)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iput p1, p0, Lcom/android/calendar/EventInfoFragment;->mX:I

    iput p2, p0, Lcom/android/calendar/EventInfoFragment;->mY:I

    iput p3, p0, Lcom/android/calendar/EventInfoFragment;->mMinTop:I

    return-void
.end method

.method public setNeedSaveEvent(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/EventInfoFragment;->mNeedSaveEvent:Z

    return-void
.end method

.method public showContactInfo(Lcom/android/calendar/CalendarEventModel$Attendee;Landroid/graphics/Rect;)V
    .locals 11
    .param p1    # Lcom/android/calendar/CalendarEventModel$Attendee;
    .param p2    # Landroid/graphics/Rect;

    const/4 v10, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v0, p1, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    sget-object v8, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/provider/ContactsContract$Data;->getContactLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const/4 v9, 0x2

    invoke-static {v8, p2, v3, v9, v10}, Landroid/provider/ContactsContract$QuickContact;->showQuickContact(Landroid/content/Context;Landroid/graphics/Rect;Landroid/net/Uri;I[Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v8, "mailto"

    invoke-static {v8, v0, v10}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    new-instance v2, Landroid/content/Intent;

    const-string v8, "com.android.contacts.action.SHOW_OR_CREATE_CONTACT"

    invoke-direct {v2, v8, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    new-instance v6, Landroid/text/util/Rfc822Token;

    iget-object v8, p1, Lcom/android/calendar/CalendarEventModel$Attendee;->mName:Ljava/lang/String;

    iget-object v9, p1, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    invoke-direct {v6, v8, v9, v10}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "com.android.contacts.action.CREATE_DESCRIPTION"

    invoke-virtual {v6}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v7, p1, Lcom/android/calendar/CalendarEventModel$Attendee;->mName:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "name"

    invoke-virtual {v2, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0, v2}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method updateResponse(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const v9, 0x7f10009c

    iget-object v5, p0, Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;

    const/16 v6, 0x14

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v5, "LOCAL"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iget-boolean v5, p0, Lcom/android/calendar/EventInfoFragment;->mCanModifyCalendar:Z

    if-eqz v5, :cond_2

    iget-boolean v5, p0, Lcom/android/calendar/EventInfoFragment;->mHasAttendeeData:Z

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/android/calendar/EventInfoFragment;->mIsOrganizer:Z

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/android/calendar/EventInfoFragment;->mNumOfAttendees:I

    const/4 v6, 0x1

    if-le v5, v6, :cond_2

    :cond_0
    iget-boolean v5, p0, Lcom/android/calendar/EventInfoFragment;->mIsOrganizer:Z

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/android/calendar/EventInfoFragment;->mOwnerCanRespond:Z

    if-eqz v5, :cond_2

    :cond_1
    if-nez v2, :cond_2

    iget-wide v5, p0, Lcom/android/calendar/EventInfoFragment;->mCalendarOwnerAttendeeId:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-nez v5, :cond_3

    :cond_2
    const/16 v5, 0x8

    invoke-direct {p0, p1, v9, v5}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    :goto_0
    return-void

    :cond_3
    const/4 v5, 0x0

    invoke-direct {p0, p1, v9, v5}, Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V

    iget v5, p0, Lcom/android/calendar/EventInfoFragment;->mUserSetResponse:I

    if-eqz v5, :cond_4

    iget v4, p0, Lcom/android/calendar/EventInfoFragment;->mUserSetResponse:I

    :goto_1
    invoke-static {v4}, Lcom/android/calendar/EventInfoFragment;->findButtonIdForResponse(I)I

    move-result v1

    const v5, 0x7f10006e

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioGroup;

    invoke-virtual {v3, v1}, Landroid/widget/RadioGroup;->check(I)V

    invoke-virtual {v3, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto :goto_0

    :cond_4
    iget v5, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeeResponseFromIntent:I

    if-eqz v5, :cond_5

    iget v4, p0, Lcom/android/calendar/EventInfoFragment;->mAttendeeResponseFromIntent:I

    goto :goto_1

    :cond_5
    iget v4, p0, Lcom/android/calendar/EventInfoFragment;->mOriginalAttendeeResponse:I

    goto :goto_1
.end method
