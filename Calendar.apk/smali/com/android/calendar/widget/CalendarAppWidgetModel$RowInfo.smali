.class Lcom/android/calendar/widget/CalendarAppWidgetModel$RowInfo;
.super Ljava/lang/Object;
.source "CalendarAppWidgetModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/widget/CalendarAppWidgetModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RowInfo"
.end annotation


# static fields
.field static final TYPE_DAY:I = 0x0

.field static final TYPE_MEETING:I = 0x1


# instance fields
.field final mIndex:I

.field final mType:I


# direct methods
.method constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel$RowInfo;->mType:I

    iput p2, p0, Lcom/android/calendar/widget/CalendarAppWidgetModel$RowInfo;->mIndex:I

    return-void
.end method
