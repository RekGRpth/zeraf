.class public final Lcom/android/calendar/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_date_text_size:I = 0x7f0a0013

.field public static final agenda_item_right_margin:I = 0x7f0a0021

.field public static final all_day_bottom_margin:I = 0x7f0a0001

.field public static final allday_text_size:I = 0x7f0a0006

.field public static final ampm_text_size:I = 0x7f0a0007

.field public static final calendar_controls_height:I = 0x7f0a0020

.field public static final calendar_controls_width:I = 0x7f0a001d

.field public static final chip_height:I = 0x7f0a0026

.field public static final chip_padding:I = 0x7f0a0025

.field public static final chip_text_size:I = 0x7f0a0027

.field public static final date_header_text_size:I = 0x7f0a0004

.field public static final day_header_bottom_margin:I = 0x7f0a0003

.field public static final day_header_height:I = 0x7f0a0010

.field public static final day_label_text_size:I = 0x7f0a0000

.field public static final day_view_event_text_size:I = 0x7f0a0008

.field public static final edit_event_view_padding_left:I = 0x7f0a0016

.field public static final edit_event_view_padding_right:I = 0x7f0a0017

.field public static final edit_event_view_width:I = 0x7f0a0015

.field public static final edit_reminder_min_size:I = 0x7f0a0018

.field public static final event_info_desc_margin_left:I = 0x7f0a001a

.field public static final event_info_desc_margin_right:I = 0x7f0a001b

.field public static final event_info_dialog_height:I = 0x7f0a0024

.field public static final event_info_dialog_width:I = 0x7f0a0023

.field public static final event_info_padding:I = 0x7f0a001c

.field public static final event_info_text_size:I = 0x7f0a0019

.field public static final event_min_height:I = 0x7f0a000c

.field public static final event_text_horizontal_margin:I = 0x7f0a000b

.field public static final event_text_vertical_margin:I = 0x7f0a000a

.field public static final hours_left_margin:I = 0x7f0a000e

.field public static final hours_right_margin:I = 0x7f0a000f

.field public static final hours_text_size:I = 0x7f0a0005

.field public static final line_spacing_extra:I = 0x7f0a0028

.field public static final max_portrait_calendar_controls_width:I = 0x7f0a001f

.field public static final min_hours_width:I = 0x7f0a000d

.field public static final min_portrait_calendar_controls_width:I = 0x7f0a001e

.field public static final new_event_hint_text_size:I = 0x7f0a0014

.field public static final one_day_header_height:I = 0x7f0a0002

.field public static final today_icon_text_size:I = 0x7f0a0022

.field public static final week_view_event_text_size:I = 0x7f0a0009

.field public static final widget_day_num_size:I = 0x7f0a0011

.field public static final widget_day_num_top_padding:I = 0x7f0a0012


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
