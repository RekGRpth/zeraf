.class public Lcom/android/calendar/AsyncQueryService;
.super Landroid/os/Handler;
.source "AsyncQueryService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/AsyncQueryService$Operation;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AsyncQuery"

.field static final localLOGV:Z

.field private static mUniqueToken:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/android/calendar/AsyncQueryService;->mUniqueToken:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p0, p0, Lcom/android/calendar/AsyncQueryService;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final cancelOperation(I)I
    .locals 1
    .param p1    # I

    invoke-static {p1}, Lcom/android/calendar/AsyncQueryServiceHelper;->cancelOperation(I)I

    move-result v0

    return v0
.end method

.method public final getLastCancelableOperation()Lcom/android/calendar/AsyncQueryService$Operation;
    .locals 1

    invoke-static {}, Lcom/android/calendar/AsyncQueryServiceHelper;->getLastCancelableOperation()Lcom/android/calendar/AsyncQueryService$Operation;

    move-result-object v0

    return-object v0
.end method

.method public final getNextToken()I
    .locals 1

    sget-object v0, Lcom/android/calendar/AsyncQueryService;->mUniqueToken:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v1, p1, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v4, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cookie:Ljava/lang/Object;

    iget-object v3, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->result:Ljava/lang/Object;

    check-cast v3, Landroid/database/Cursor;

    invoke-virtual {p0, v2, v4, v3}, Lcom/android/calendar/AsyncQueryService;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_1
    iget-object v4, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cookie:Ljava/lang/Object;

    iget-object v3, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->result:Ljava/lang/Object;

    check-cast v3, Landroid/net/Uri;

    invoke-virtual {p0, v2, v4, v3}, Lcom/android/calendar/AsyncQueryService;->onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V

    goto :goto_0

    :pswitch_2
    iget-object v4, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cookie:Ljava/lang/Object;

    iget-object v3, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->result:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v2, v4, v3}, Lcom/android/calendar/AsyncQueryService;->onUpdateComplete(ILjava/lang/Object;I)V

    goto :goto_0

    :pswitch_3
    iget-object v4, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cookie:Ljava/lang/Object;

    iget-object v3, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->result:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v2, v4, v3}, Lcom/android/calendar/AsyncQueryService;->onDeleteComplete(ILjava/lang/Object;I)V

    goto :goto_0

    :pswitch_4
    iget-object v4, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cookie:Ljava/lang/Object;

    iget-object v3, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->result:Ljava/lang/Object;

    check-cast v3, [Landroid/content/ContentProviderResult;

    check-cast v3, [Landroid/content/ContentProviderResult;

    invoke-virtual {p0, v2, v4, v3}, Lcom/android/calendar/AsyncQueryService;->onBatchComplete(ILjava/lang/Object;[Landroid/content/ContentProviderResult;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onBatchComplete(ILjava/lang/Object;[Landroid/content/ContentProviderResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # [Landroid/content/ContentProviderResult;

    return-void
.end method

.method protected onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    return-void
.end method

.method protected onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/net/Uri;

    return-void
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    return-void
.end method

.method protected onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    return-void
.end method

.method protected setTestHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/calendar/AsyncQueryService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public startBatch(ILjava/lang/Object;Ljava/lang/String;Ljava/util/ArrayList;J)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/String;
    .param p5    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;J)V"
        }
    .end annotation

    new-instance v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;

    invoke-direct {v0}, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;-><init>()V

    const/4 v1, 0x5

    iput v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->op:I

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->resolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mHandler:Landroid/os/Handler;

    iput-object v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->handler:Landroid/os/Handler;

    iput p1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->token:I

    iput-object p2, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cookie:Ljava/lang/Object;

    iput-object p3, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->authority:Ljava/lang/String;

    iput-object p4, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cpo:Ljava/util/ArrayList;

    iput-wide p5, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->delayMillis:J

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/AsyncQueryServiceHelper;->queueOperation(Landroid/content/Context;Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;)V

    return-void
.end method

.method public startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;J)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/net/Uri;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # J

    new-instance v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;

    invoke-direct {v0}, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;-><init>()V

    const/4 v1, 0x4

    iput v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->op:I

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->resolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mHandler:Landroid/os/Handler;

    iput-object v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->handler:Landroid/os/Handler;

    iput p1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->token:I

    iput-object p2, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cookie:Ljava/lang/Object;

    iput-object p3, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->uri:Landroid/net/Uri;

    iput-object p4, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->selection:Ljava/lang/String;

    iput-object p5, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->selectionArgs:[Ljava/lang/String;

    iput-wide p6, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->delayMillis:J

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/AsyncQueryServiceHelper;->queueOperation(Landroid/content/Context;Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;)V

    return-void
.end method

.method public startInsert(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;J)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/net/Uri;
    .param p4    # Landroid/content/ContentValues;
    .param p5    # J

    new-instance v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;

    invoke-direct {v0}, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;-><init>()V

    const/4 v1, 0x2

    iput v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->op:I

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->resolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mHandler:Landroid/os/Handler;

    iput-object v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->handler:Landroid/os/Handler;

    iput p1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->token:I

    iput-object p2, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cookie:Ljava/lang/Object;

    iput-object p3, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->uri:Landroid/net/Uri;

    iput-object p4, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->values:Landroid/content/ContentValues;

    iput-wide p5, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->delayMillis:J

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/AsyncQueryServiceHelper;->queueOperation(Landroid/content/Context;Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;)V

    return-void
.end method

.method public startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/net/Uri;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # [Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    new-instance v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;

    invoke-direct {v0}, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->op:I

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->resolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mHandler:Landroid/os/Handler;

    iput-object v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->handler:Landroid/os/Handler;

    iput p1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->token:I

    iput-object p2, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cookie:Ljava/lang/Object;

    iput-object p3, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->uri:Landroid/net/Uri;

    iput-object p4, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->projection:[Ljava/lang/String;

    iput-object p5, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->selection:Ljava/lang/String;

    iput-object p6, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->selectionArgs:[Ljava/lang/String;

    iput-object p7, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->orderBy:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/AsyncQueryServiceHelper;->queueOperation(Landroid/content/Context;Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;)V

    return-void
.end method

.method public startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/net/Uri;
    .param p4    # Landroid/content/ContentValues;
    .param p5    # Ljava/lang/String;
    .param p6    # [Ljava/lang/String;
    .param p7    # J

    new-instance v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;

    invoke-direct {v0}, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;-><init>()V

    const/4 v1, 0x3

    iput v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->op:I

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->resolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mHandler:Landroid/os/Handler;

    iput-object v1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->handler:Landroid/os/Handler;

    iput p1, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->token:I

    iput-object p2, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->cookie:Ljava/lang/Object;

    iput-object p3, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->uri:Landroid/net/Uri;

    iput-object p4, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->values:Landroid/content/ContentValues;

    iput-object p5, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->selection:Ljava/lang/String;

    iput-object p6, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->selectionArgs:[Ljava/lang/String;

    iput-wide p7, v0, Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;->delayMillis:J

    iget-object v1, p0, Lcom/android/calendar/AsyncQueryService;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/calendar/AsyncQueryServiceHelper;->queueOperation(Landroid/content/Context;Lcom/android/calendar/AsyncQueryServiceHelper$OperationInfo;)V

    return-void
.end method
