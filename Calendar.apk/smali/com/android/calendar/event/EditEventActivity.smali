.class public Lcom/android/calendar/event/EditEventActivity;
.super Lcom/android/calendar/AbstractCalendarActivity;
.source "EditEventActivity.java"


# static fields
.field private static final BUNDLE_KEY_EVENT_ID:Ljava/lang/String; = "key_event_id"

.field static final DATE_TIME_IDENTIFIER:Ljava/lang/String; = "date_time_identifier"

.field private static final DATE_TIME_TAG:Ljava/lang/String; = "EditEventActivity::date_time_debug_tag"

.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "EditEventActivity"

.field private static mIsMultipane:Z


# instance fields
.field private mDateTimeIdentifier:I

.field private mEditFragment:Lcom/android/calendar/event/EditEventFragment;

.field private mEventInfo:Lcom/android/calendar/CalendarController$EventInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/calendar/AbstractCalendarActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/event/EditEventActivity;->mDateTimeIdentifier:I

    return-void
.end method

.method private getEventInfoFromIntent(Landroid/os/Bundle;)Lcom/android/calendar/CalendarController$EventInfo;
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v12, -0x1

    new-instance v8, Lcom/android/calendar/CalendarController$EventInfo;

    invoke-direct {v8}, Lcom/android/calendar/CalendarController$EventInfo;-><init>()V

    const-wide/16 v6, -0x1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_5

    :try_start_0
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    :cond_0
    :goto_0
    const-string v10, "allDay"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v10, "beginTime"

    invoke-virtual {v9, v10, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    const-string v10, "endTime"

    invoke-virtual {v9, v10, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v10, v4, v12

    if-eqz v10, :cond_2

    new-instance v10, Landroid/text/format/Time;

    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    iput-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    if-eqz v0, :cond_1

    iget-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    const-string v11, "UTC"

    iput-object v11, v10, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    :cond_1
    iget-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    invoke-virtual {v10, v4, v5}, Landroid/text/format/Time;->set(J)V

    :cond_2
    cmp-long v10, v1, v12

    if-eqz v10, :cond_4

    new-instance v10, Landroid/text/format/Time;

    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    iput-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    if-eqz v0, :cond_3

    iget-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const-string v11, "UTC"

    iput-object v11, v10, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    :cond_3
    iget-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    invoke-virtual {v10, v1, v2}, Landroid/text/format/Time;->set(J)V

    :cond_4
    iput-wide v6, v8, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    if-eqz v0, :cond_6

    const-wide/16 v10, 0x10

    iput-wide v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->extraLong:J

    :goto_1
    return-object v8

    :cond_5
    if-eqz p1, :cond_0

    const-string v10, "key_event_id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v10, "key_event_id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    goto :goto_0

    :cond_6
    const-wide/16 v10, 0x0

    iput-wide v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->extraLong:J

    goto :goto_1

    :catch_0
    move-exception v10

    goto :goto_0
.end method


# virtual methods
.method public getDateTimeOnDateSetListener()Landroid/app/DatePickerDialog$OnDateSetListener;
    .locals 3

    const-string v1, "EditEventActivity::date_time_debug_tag"

    const-string v2, "getDateTimeOnDateSetListener()"

    invoke-static {v1, v2}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v1}, Lcom/android/calendar/event/EditEventFragment;->getEditEventView()Lcom/android/calendar/event/EditEventView;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/event/EditEventActivity;->mDateTimeIdentifier:I

    invoke-virtual {v1, v2}, Lcom/android/calendar/event/EditEventView;->getDateTimeView(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v1}, Lcom/android/calendar/event/EditEventFragment;->getEditEventView()Lcom/android/calendar/event/EditEventView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/calendar/event/EditEventView;->getOnDateSetListener(Landroid/view/View;)Landroid/app/DatePickerDialog$OnDateSetListener;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDateTimeOnDismissListener()Landroid/content/DialogInterface$OnDismissListener;
    .locals 2

    const-string v0, "EditEventActivity::date_time_debug_tag"

    const-string v1, "getDateTimeOnDismissListener()"

    invoke-static {v0, v1}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventFragment;->getEditEventView()Lcom/android/calendar/event/EditEventView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventView;->getDateTimeOnDismissListener()Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v0

    return-object v0
.end method

.method public getDateTimeOnTimeSetListener()Landroid/app/TimePickerDialog$OnTimeSetListener;
    .locals 3

    const-string v1, "EditEventActivity::date_time_debug_tag"

    const-string v2, "getDateTimeOnTimeSetListener()"

    invoke-static {v1, v2}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v1}, Lcom/android/calendar/event/EditEventFragment;->getEditEventView()Lcom/android/calendar/event/EditEventView;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/event/EditEventActivity;->mDateTimeIdentifier:I

    invoke-virtual {v1, v2}, Lcom/android/calendar/event/EditEventView;->getDateTimeView(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v1}, Lcom/android/calendar/event/EditEventFragment;->getEditEventView()Lcom/android/calendar/event/EditEventView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/calendar/event/EditEventView;->getDateTimeOnTimeSetListener(Landroid/view/View;)Landroid/app/TimePickerDialog$OnTimeSetListener;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isAnyDialogShown()Z
    .locals 2

    const-string v0, "EditEventActivity::date_time_debug_tag"

    const-string v1, "isAnyDialogShown()"

    invoke-static {v0, v1}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventFragment;->getEditEventView()Lcom/android/calendar/event/EditEventView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventView;->isAnyDialogShown()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v8, -0x1

    const v7, 0x7f1000c0

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f040038

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0, p1}, Lcom/android/calendar/event/EditEventActivity;->getEventInfoFromIntent(Landroid/os/Bundle;)Lcom/android/calendar/CalendarController$EventInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEventInfo:Lcom/android/calendar/CalendarController$EventInfo;

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/event/EditEventFragment;

    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    const v2, 0x7f090007

    invoke-static {p0, v2}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v2

    sput-boolean v2, Lcom/android/calendar/event/EditEventActivity;->mIsMultipane:Z

    sget-boolean v2, Lcom/android/calendar/event/EditEventActivity;->mIsMultipane:Z

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/16 v3, 0x8

    const/16 v4, 0xe

    invoke-virtual {v2, v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEventInfo:Lcom/android/calendar/CalendarController$EventInfo;

    iget-wide v4, v2, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    cmp-long v2, v4, v8

    if-nez v2, :cond_3

    const v2, 0x7f0c004f

    :goto_0
    invoke-virtual {v3, v2}, Landroid/app/ActionBar;->setTitle(I)V

    :goto_1
    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    if-nez v2, :cond_1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEventInfo:Lcom/android/calendar/CalendarController$EventInfo;

    iget-wide v2, v2, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    cmp-long v2, v2, v8

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    :cond_0
    new-instance v2, Lcom/android/calendar/event/EditEventFragment;

    iget-object v3, p0, Lcom/android/calendar/event/EditEventActivity;->mEventInfo:Lcom/android/calendar/CalendarController$EventInfo;

    invoke-direct {v2, v3, v6, v1}, Lcom/android/calendar/event/EditEventFragment;-><init>(Lcom/android/calendar/CalendarController$EventInfo;ZLandroid/content/Intent;)V

    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "editMode"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v2, Lcom/android/calendar/event/EditEventFragment;->mShowModifyDialogOnLaunch:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v0, v7, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_1
    if-eqz p1, :cond_2

    const-string v2, "date_time_identifier"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/event/EditEventActivity;->mDateTimeIdentifier:I

    const-string v2, "EditEventActivity::date_time_debug_tag"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate(), mDateTimeIdentifier: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/calendar/event/EditEventActivity;->mDateTimeIdentifier:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    const v2, 0x7f0c0050

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/16 v3, 0x10

    const/16 v4, 0x1e

    invoke-virtual {v2, v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    invoke-static {p0}, Lcom/android/calendar/CalendarController;->removeInstance(Landroid/content/Context;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/android/calendar/Utils;->returnToCalendarHome(Landroid/content/Context;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "date_time_identifier"

    iget v1, p0, Lcom/android/calendar/event/EditEventActivity;->mDateTimeIdentifier:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "EditEventActivity::date_time_debug_tag"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState(), mDateTimeIdentifier: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/event/EditEventActivity;->mDateTimeIdentifier:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method setDateTimeViewId(I)V
    .locals 3
    .param p1    # I

    const-string v0, "EditEventActivity::date_time_debug_tag"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDateTimeViewId(), id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput p1, p0, Lcom/android/calendar/event/EditEventActivity;->mDateTimeIdentifier:I

    return-void
.end method

.method public setDialogShown()V
    .locals 2

    const-string v0, "EditEventActivity::date_time_debug_tag"

    const-string v1, "setDialogShown()"

    invoke-static {v0, v1}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventFragment;->getEditEventView()Lcom/android/calendar/event/EditEventView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventView;->setDialogShown()V

    return-void
.end method
