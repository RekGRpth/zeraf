.class Lcom/android/calendar/event/EditEventView$TimeListener;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/event/EditEventView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimeListener"
.end annotation


# instance fields
.field private mView:Landroid/view/View;

.field final synthetic this$0:Lcom/android/calendar/event/EditEventView;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/EditEventView;Landroid/view/View;)V
    .locals 0
    .param p2    # Landroid/view/View;

    iput-object p1, p0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/calendar/event/EditEventView$TimeListener;->mView:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 19
    .param p1    # Landroid/widget/TimePicker;
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    invoke-static {v15}, Lcom/android/calendar/event/EditEventView;->access$100(Lcom/android/calendar/event/EditEventView;)Landroid/text/format/Time;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    invoke-static {v15}, Lcom/android/calendar/event/EditEventView;->access$200(Lcom/android/calendar/event/EditEventView;)Landroid/text/format/Time;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->mView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/calendar/event/EditEventView;->mStartTimeButton:Landroid/widget/Button;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_1

    iget v15, v6, Landroid/text/format/Time;->hour:I

    iget v0, v14, Landroid/text/format/Time;->hour:I

    move/from16 v16, v0

    sub-int v7, v15, v16

    iget v15, v6, Landroid/text/format/Time;->minute:I

    iget v0, v14, Landroid/text/format/Time;->minute:I

    move/from16 v16, v0

    sub-int v8, v15, v16

    move/from16 v0, p2

    iput v0, v14, Landroid/text/format/Time;->hour:I

    move/from16 v0, p3

    iput v0, v14, Landroid/text/format/Time;->minute:I

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v12

    add-int v15, p2, v7

    iput v15, v6, Landroid/text/format/Time;->hour:I

    add-int v15, p3, v8

    iput v15, v6, Landroid/text/format/Time;->minute:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    invoke-static {v15, v12, v13}, Lcom/android/calendar/event/EditEventView;->access$300(Lcom/android/calendar/event/EditEventView;J)V

    :cond_0
    :goto_0
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/calendar/event/EditEventView;->mEndDateButton:Landroid/widget/Button;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v15, v0, v4, v5}, Lcom/android/calendar/event/EditEventView;->access$600(Lcom/android/calendar/event/EditEventView;Landroid/widget/TextView;J)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/calendar/event/EditEventView;->mStartTimeButton:Landroid/widget/Button;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v15, v0, v12, v13}, Lcom/android/calendar/event/EditEventView;->access$700(Lcom/android/calendar/event/EditEventView;Landroid/widget/TextView;J)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/calendar/event/EditEventView;->mEndTimeButton:Landroid/widget/Button;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v15, v0, v4, v5}, Lcom/android/calendar/event/EditEventView;->access$700(Lcom/android/calendar/event/EditEventView;Landroid/widget/TextView;J)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    invoke-static {v15}, Lcom/android/calendar/event/EditEventView;->access$800(Lcom/android/calendar/event/EditEventView;)V

    return-void

    :cond_1
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v15

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v17

    sub-long v9, v15, v17

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    invoke-static {v15, v9, v10}, Lcom/android/calendar/event/EditEventView;->access$400(Lcom/android/calendar/event/EditEventView;J)I

    move-result v11

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v12

    move/from16 v0, p2

    iput v0, v6, Landroid/text/format/Time;->hour:I

    move/from16 v0, p3

    iput v0, v6, Landroid/text/format/Time;->minute:I

    invoke-virtual {v6, v14}, Landroid/text/format/Time;->before(Landroid/text/format/Time;)Z

    move-result v15

    if-eqz v15, :cond_2

    iget v15, v14, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v15, v15, 0x1

    iput v15, v6, Landroid/text/format/Time;->monthDay:I

    :cond_2
    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v15

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v17

    sub-long v1, v15, v17

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    invoke-static {v15, v1, v2}, Lcom/android/calendar/event/EditEventView;->access$400(Lcom/android/calendar/event/EditEventView;J)I

    move-result v3

    if-eq v11, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/EditEventView$TimeListener;->this$0:Lcom/android/calendar/event/EditEventView;

    invoke-static {v15}, Lcom/android/calendar/event/EditEventView;->access$500(Lcom/android/calendar/event/EditEventView;)V

    goto/16 :goto_0
.end method
