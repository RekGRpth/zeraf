.class public final Lcom/android/ex/chips/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final BackgroundOnlyTheme:I = 0x7f0d0000

.field public static final CalendarActionBarStyle:I = 0x7f0d0019

.field public static final CalendarTheme:I = 0x7f0d0017

.field public static final CalendarTheme_WithActionBar:I = 0x7f0d0018

.field public static final CalendarTheme_WithActionBarWallpaper:I = 0x7f0d001a

.field public static final EditEventCustomActionButton:I = 0x7f0d001d

.field public static final EditEventCustomActionButtonImage:I = 0x7f0d001e

.field public static final EditEventCustomActionButtonText:I = 0x7f0d001f

.field public static final EditEventSeparator:I = 0x7f0d001c

.field public static final EditEvent_Layout:I = 0x7f0d0015

.field public static final MinusButton:I = 0x7f0d0001

.field public static final MonthView_DayLabel:I = 0x7f0d0002

.field public static final MonthView_MiniMonthLabel:I = 0x7f0d0003

.field public static final MultiStateButton:I = 0x7f0d001b

.field public static final NotificationPrimaryText:I = 0x7f0d0022

.field public static final NotificationSecondaryText:I = 0x7f0d0023

.field public static final RecipientEditTextView:I = 0x7f0d0024

.field public static final TextAppearance:I = 0x7f0d0004

.field public static final TextAppearance_AgendaView_ValueLabel:I = 0x7f0d0008

.field public static final TextAppearance_Date_Range_Title:I = 0x7f0d0016

.field public static final TextAppearance_EditEvent:I = 0x7f0d000d

.field public static final TextAppearance_EditEventCalSpinner:I = 0x7f0d000f

.field public static final TextAppearance_EditEvent_Button:I = 0x7f0d000a

.field public static final TextAppearance_EditEvent_CalSpinnerValue:I = 0x7f0d0010

.field public static final TextAppearance_EditEvent_Label:I = 0x7f0d0009

.field public static final TextAppearance_EditEvent_LabelSmall:I = 0x7f0d000b

.field public static final TextAppearance_EditEvent_Small:I = 0x7f0d000c

.field public static final TextAppearance_EditEvent_Spinner:I = 0x7f0d0011

.field public static final TextAppearance_EditEvent_SpinnerButton:I = 0x7f0d0012

.field public static final TextAppearance_EditEvent_Value:I = 0x7f0d000e

.field public static final TextAppearance_EditEvent_homeTime:I = 0x7f0d0013

.field public static final TextAppearance_EventInfo_Label:I = 0x7f0d0014

.field public static final TextAppearance_MonthView_DayLabel:I = 0x7f0d0007

.field public static final TextAppearance_MonthView_MiniDayLabel:I = 0x7f0d0006

.field public static final TextAppearance_SelectCalendar_Name:I = 0x7f0d0005

.field public static final WidgetDateStyle:I = 0x7f0d0021

.field public static final WidgetDayOfWeekStyle:I = 0x7f0d0020


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
