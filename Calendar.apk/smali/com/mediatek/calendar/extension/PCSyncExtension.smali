.class public Lcom/mediatek/calendar/extension/PCSyncExtension;
.super Ljava/lang/Object;
.source "PCSyncExtension.java"

# interfaces
.implements Lcom/mediatek/calendar/extension/IAccountExt;


# static fields
.field private static final ACCOUNT_UNIQUE_KEY:Ljava/lang/String; = "ACCOUNT_KEY"


# instance fields
.field private final mActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/calendar/extension/PCSyncExtension;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method private queryAccountsExceptionPCSync([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1    # [Ljava/lang/String;

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "LOCAL"

    aput-object v1, v4, v0

    iget-object v0, p0, Lcom/mediatek/calendar/extension/PCSyncExtension;->mActivity:Landroid/app/Activity;

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "account_type!=?1) GROUP BY (ACCOUNT_KEY"

    const-string v5, "account_name"

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    return-object v6
.end method


# virtual methods
.method public accountQuery([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/calendar/extension/PCSyncExtension;->queryAccountsExceptionPCSync([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
