.class Lcom/mediatek/calendar/lunar/LunarDatePicker$1;
.super Ljava/lang/Object;
.source "LunarDatePicker.java"

# interfaces
.implements Landroid/widget/NumberPicker$OnValueChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/calendar/lunar/LunarDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;


# direct methods
.method constructor <init>(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValueChange(Landroid/widget/NumberPicker;II)V
    .locals 16
    .param p1    # Landroid/widget/NumberPicker;
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$000(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v12}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$100(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->get(I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->get(I)I

    move-result v11

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x5

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->get(I)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    invoke-virtual {v11, v4, v3, v2}, Lcom/mediatek/calendar/lunar/LunarUtil;->calculateLunarByGregorian(III)[I

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$400(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Landroid/widget/NumberPicker;

    move-result-object v11

    move-object/from16 v0, p1

    if-ne v0, v11, :cond_2

    const/16 v11, 0x1b

    move/from16 v0, p2

    if-le v0, v11, :cond_0

    const/4 v11, 0x1

    move/from16 v0, p3

    if-ne v0, v11, :cond_0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x5

    const/4 v13, 0x1

    invoke-virtual {v11, v12, v13}, Ljava/util/Calendar;->add(II)V

    :goto_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->get(I)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->get(I)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x5

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$700()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "onValueChange(), year: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", month: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", day: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/calendar/LogUtil;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11, v10, v7, v1}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$800(Lcom/mediatek/calendar/lunar/LunarDatePicker;III)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$900(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$1000(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$1100(Lcom/mediatek/calendar/lunar/LunarDatePicker;)V

    return-void

    :cond_0
    const/4 v11, 0x1

    move/from16 v0, p2

    if-ne v0, v11, :cond_1

    const/16 v11, 0x1b

    move/from16 v0, p3

    if-le v0, v11, :cond_1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x5

    const/4 v13, -0x1

    invoke-virtual {v11, v12, v13}, Ljava/util/Calendar;->add(II)V

    goto/16 :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x5

    sub-int v13, p3, p2

    invoke-virtual {v11, v12, v13}, Ljava/util/Calendar;->add(II)V

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$500(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Landroid/widget/NumberPicker;

    move-result-object v11

    move-object/from16 v0, p1

    if-ne v0, v11, :cond_e

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/16 v11, 0xa

    move/from16 v0, p2

    if-le v0, v11, :cond_4

    if-nez p3, :cond_4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    invoke-virtual {v11, v12}, Lcom/mediatek/calendar/lunar/LunarUtil;->leapMonth(I)I

    move-result v5

    const/16 v11, 0xc

    if-ne v5, v11, :cond_3

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    invoke-virtual {v11, v12}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfLeapMonthInLunarYear(I)I

    move-result v8

    :goto_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x5

    invoke-virtual {v11, v12, v8}, Ljava/util/Calendar;->add(II)V

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    const/16 v13, 0xc

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v8

    goto :goto_1

    :cond_4
    if-nez p2, :cond_6

    const/16 v11, 0xa

    move/from16 v0, p3

    if-le v0, v11, :cond_6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v11, v12}, Lcom/mediatek/calendar/lunar/LunarUtil;->leapMonth(I)I

    move-result v5

    const/16 v11, 0xc

    if-ne v5, v11, :cond_5

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    invoke-virtual {v11, v12}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfLeapMonthInLunarYear(I)I

    move-result v8

    :goto_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x5

    neg-int v13, v8

    invoke-virtual {v11, v12, v13}, Ljava/util/Calendar;->add(II)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    add-int/lit8 v12, v12, -0x1

    const/16 v13, 0xc

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v8

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    invoke-virtual {v11, v12}, Lcom/mediatek/calendar/lunar/LunarUtil;->leapMonth(I)I

    move-result v5

    sub-int v11, p3, p2

    if-gez v11, :cond_a

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    add-int/lit8 v13, p3, 0x1

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v8

    :goto_3
    neg-int v8, v8

    :goto_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x5

    invoke-virtual {v11, v12, v8}, Ljava/util/Calendar;->add(II)V

    goto/16 :goto_0

    :cond_7
    move/from16 v0, p3

    if-ge v0, v5, :cond_8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    add-int/lit8 v13, p3, 0x1

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v8

    goto :goto_3

    :cond_8
    move/from16 v0, p3

    if-ne v0, v5, :cond_9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    invoke-virtual {v11, v12}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfLeapMonthInLunarYear(I)I

    move-result v8

    goto :goto_3

    :cond_9
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    move/from16 v0, p3

    invoke-virtual {v11, v12, v0}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v8

    goto :goto_3

    :cond_a
    if-nez v5, :cond_b

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    add-int/lit8 v13, p2, 0x1

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v8

    goto :goto_4

    :cond_b
    move/from16 v0, p2

    if-ge v0, v5, :cond_c

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    add-int/lit8 v13, p2, 0x1

    invoke-virtual {v11, v12, v13}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v8

    goto :goto_4

    :cond_c
    move/from16 v0, p2

    if-ne v0, v5, :cond_d

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    invoke-virtual {v11, v12}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfLeapMonthInLunarYear(I)I

    move-result v8

    goto/16 :goto_4

    :cond_d
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v11

    const/4 v12, 0x0

    aget v12, v6, v12

    move/from16 v0, p2

    invoke-virtual {v11, v12, v0}, Lcom/mediatek/calendar/lunar/LunarUtil;->daysOfALunarMonth(II)I

    move-result v8

    goto/16 :goto_4

    :cond_e
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v11}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$600(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Landroid/widget/NumberPicker;

    move-result-object v11

    move-object/from16 v0, p1

    if-ne v0, v11, :cond_10

    sub-int v11, p3, p2

    if-lez v11, :cond_f

    const/4 v9, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v12}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$300(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Lcom/mediatek/calendar/lunar/LunarUtil;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/calendar/lunar/LunarDatePicker$1;->this$0:Lcom/mediatek/calendar/lunar/LunarDatePicker;

    invoke-static {v13}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$200(Lcom/mediatek/calendar/lunar/LunarDatePicker;)Ljava/util/Calendar;

    move-result-object v13

    const/4 v14, 0x1

    aget v14, v6, v14

    const/4 v15, 0x2

    aget v15, v6, v15

    invoke-virtual {v12, v13, v14, v15, v9}, Lcom/mediatek/calendar/lunar/LunarUtil;->decreaseOrIncreaseALunarYear(Ljava/util/Calendar;III)Ljava/util/Calendar;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/mediatek/calendar/lunar/LunarDatePicker;->access$202(Lcom/mediatek/calendar/lunar/LunarDatePicker;Ljava/util/Calendar;)Ljava/util/Calendar;

    goto/16 :goto_0

    :cond_f
    const/4 v9, -0x1

    goto :goto_5

    :cond_10
    new-instance v11, Ljava/lang/IllegalArgumentException;

    invoke-direct {v11}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v11
.end method
