.class public Lcom/google/android/setupwizard/DateTimeSetupActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "DateTimeSetupActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private mDatePicker:Landroid/app/DatePickerDialog;

.field private mDateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private mDateTextView:Landroid/widget/TextView;

.field private mSelectedTimeZone:Ljava/util/TimeZone;

.field private mTimePicker:Landroid/app/TimePickerDialog;

.field private mTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field private mTimeTextView:Landroid/widget/TextView;

.field private mTimeZoneAdapter:Landroid/widget/SimpleAdapter;

.field private mTimeZoneButton:Landroid/widget/Button;

.field private mTimeZonePopup:Landroid/widget/ListPopupWindow;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    new-instance v0, Lcom/google/android/setupwizard/DateTimeSetupActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/DateTimeSetupActivity$1;-><init>(Lcom/google/android/setupwizard/DateTimeSetupActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mDateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    new-instance v0, Lcom/google/android/setupwizard/DateTimeSetupActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/DateTimeSetupActivity$2;-><init>(Lcom/google/android/setupwizard/DateTimeSetupActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/DateTimeSetupActivity;IIIII)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/DateTimeSetupActivity;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-direct/range {p0 .. p5}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->setDateTime(IIIII)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/DateTimeSetupActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/DateTimeSetupActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->updateDateDisplay()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/DateTimeSetupActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/DateTimeSetupActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->updateTimeDisplay()V

    return-void
.end method

.method private setDateTime(IIIII)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    if-ltz p1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Ljava/util/Calendar;->set(II)V

    :cond_0
    if-ltz p2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p2}, Ljava/util/Calendar;->set(II)V

    :cond_1
    if-ltz p3, :cond_2

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p3}, Ljava/util/Calendar;->set(II)V

    :cond_2
    if-ltz p4, :cond_3

    const/16 v2, 0xb

    invoke-virtual {v1, v2, p4}, Ljava/util/Calendar;->set(II)V

    :cond_3
    if-ltz p5, :cond_4

    const/16 v2, 0xc

    invoke-virtual {v1, v2, p5}, Ljava/util/Calendar;->set(II)V

    :cond_4
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/app/AlarmManager;->setTime(J)V

    :goto_0
    return-void

    :cond_5
    const-string v2, "SetupWizard"

    const-string v3, "failed to set the system time"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setTimeZone(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to set the system timezone to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showTimezonePicker(I)V
    .locals 4
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to find zone picker anchor view "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/widget/ListPopupWindow;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZonePopup:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZonePopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZonePopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZonePopup:Landroid/widget/ListPopupWindow;

    iget-object v2, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZoneAdapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZonePopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, p0}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZonePopup:Landroid/widget/ListPopupWindow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZonePopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->show()V

    goto :goto_0
.end method

.method private updateDateDisplay()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mDateTextView:Landroid/widget/TextView;

    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateTimeDisplay()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeTextView:Landroid/widget/TextView;

    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public initView()V
    .locals 1

    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->setContentView(I)V

    const v0, 0x7f0d0014

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZoneButton:Landroid/widget/Button;

    const v0, 0x7f0d0017

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mDateTextView:Landroid/widget/TextView;

    const v0, 0x7f0d001a

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeTextView:Landroid/widget/TextView;

    const v0, 0x7f0d0010

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->setBackButton(Landroid/view/View;)V

    const v0, 0x7f0d000d

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    const v0, 0x7f0d0014

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->showTimezonePicker(I)V

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->showDialog(I)V

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->showDialog(I)V

    goto :goto_0

    :sswitch_3
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->finish()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d000d -> :sswitch_3
        0x7f0d0014 -> :sswitch_0
        0x7f0d0015 -> :sswitch_1
        0x7f0d0018 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x0

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->initView()V

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    const v0, 0x7f03000a

    invoke-static {p0, v8, v0}, Lcom/google/android/setupwizard/ZonePicker;->constructTimezoneAdapter(Landroid/content/Context;ZI)Landroid/widget/SimpleAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZoneAdapter:Landroid/widget/SimpleAdapter;

    iget-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZoneButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZoneAdapter:Landroid/widget/SimpleAdapter;

    iget-object v2, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    invoke-static {v1, v2}, Lcom/google/android/setupwizard/ZonePicker;->getTimeZoneName(Landroid/widget/SimpleAdapter;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mDateSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v1, 0x2

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v1, 0x5

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    iput-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mDatePicker:Landroid/app/DatePickerDialog;

    new-instance v0, Landroid/app/TimePickerDialog;

    iget-object v2, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    const/16 v1, 0xb

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v1, 0xc

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    move-object v1, p0

    move v5, v8

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iput-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimePicker:Landroid/app/TimePickerDialog;

    invoke-direct {p0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->updateDateDisplay()V

    invoke-direct {p0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->updateTimeDisplay()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mDatePicker:Landroid/app/DatePickerDialog;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimePicker:Landroid/app/TimePickerDialog;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/setupwizard/ZonePicker;->obtainTimeZoneFromItem(Ljava/lang/Object;)Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    iget-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->setTimeZone(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->updateDateDisplay()V

    invoke-direct {p0}, Lcom/google/android/setupwizard/DateTimeSetupActivity;->updateTimeDisplay()V

    iget-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZoneButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZoneAdapter:Landroid/widget/SimpleAdapter;

    iget-object v2, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    invoke-static {v1, v2}, Lcom/google/android/setupwizard/ZonePicker;->getTimeZoneName(Landroid/widget/SimpleAdapter;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimeZonePopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 5
    .param p1    # I
    .param p2    # Landroid/app/Dialog;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mDatePicker:Landroid/app/DatePickerDialog;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/DatePickerDialog;->updateDate(III)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/setupwizard/DateTimeSetupActivity;->mTimePicker:Landroid/app/TimePickerDialog;

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/app/TimePickerDialog;->updateTime(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
