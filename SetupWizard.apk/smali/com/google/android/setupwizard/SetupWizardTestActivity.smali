.class public Lcom/google/android/setupwizard/SetupWizardTestActivity;
.super Lcom/google/android/setupwizard/SetupWizardActivity;
.source "SetupWizardTestActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/SetupWizardTestActivity;->mOverrideExit:Z

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardTestActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "device_provisioned"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_5

    move v2, v4

    :goto_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    if-nez v6, :cond_6

    move v1, v4

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardTestActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "device_provisioned"

    invoke-static {v4, v6, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardTestActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "user_setup_complete"

    invoke-static {v4, v6, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    sget-boolean v4, Lcom/google/android/setupwizard/SetupWizardTestActivity;->LOCAL_LOGV:Z

    if-eqz v4, :cond_1

    const-string v4, "SetupWizard"

    const-string v5, "SetupWizardTestActivity - starting ..."

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v4, Lcom/google/android/setupwizard/SetupWizardTestActivity;->mUserData:Ljava/util/HashMap;

    const-string v5, "startedFromTestActivity"

    const-string v6, "true"

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardTestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string v4, "testMode"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    if-eqz v3, :cond_4

    sget-boolean v4, Lcom/google/android/setupwizard/SetupWizardTestActivity;->LOCAL_LOGV:Z

    if-eqz v4, :cond_2

    const-string v4, "SetupWizard"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SetupWizardTestActivity : testMode = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v4, "enterpriseLogin"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    sget-boolean v4, Lcom/google/android/setupwizard/SetupWizardTestActivity;->LOCAL_LOGV:Z

    if-eqz v4, :cond_3

    const-string v4, "SetupWizard"

    const-string v5, "SetupWizardTestActivity - testing enterprise"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    sget-object v4, Lcom/google/android/setupwizard/SetupWizardTestActivity;->mUserData:Ljava/util/HashMap;

    const-string v5, "testMode"

    const-string v6, "true"

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/SetupWizardActivity;->onCreate(Landroid/os/Bundle;)V

    return-void

    :cond_5
    move v2, v5

    goto :goto_0

    :cond_6
    move v1, v5

    goto :goto_1

    :cond_7
    const-string v3, "no intent"

    goto :goto_2
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/SetupWizardActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/SetupWizardTestActivity;->setIntent(Landroid/content/Intent;)V

    const-string v1, "completed"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "completed"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, -0x1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/SetupWizardTestActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardTestActivity;->finish()V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x7

    goto :goto_0
.end method
