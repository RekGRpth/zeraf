.class public Lcom/google/android/setupwizard/SetupCompleteActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "SetupCompleteActivity.java"


# instance fields
.field private mBackupAssistantCheckbox:Landroid/widget/CompoundButton;

.field private mNextButton:Landroid/view/View;

.field private mSpecialNotificationMsg:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method

.method private backupAssistantExists()Z
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupCompleteActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sncr.action.BAEULA"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x10000

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v0, 0x2710

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SetupCompleteActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupCompleteActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SetupCompleteActivity;->setContentView(I)V

    const v0, 0x7f0d0024

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SetupCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mSpecialNotificationMsg:Landroid/widget/TextView;

    sget-object v0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "specialNotificationMsgHtml"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "specialNotificationMsgHtml"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mSpecialNotificationMsg:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mSpecialNotificationMsg:Landroid/widget/TextView;

    sget-object v0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mUserData:Ljava/util/HashMap;

    const-string v2, "specialNotificationMsgHtml"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v0, 0x7f0d000d

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SetupCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mNextButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mNextButton:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/SetupCompleteActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v0, 0x7f0d0026

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SetupCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mBackupAssistantCheckbox:Landroid/widget/CompoundButton;

    invoke-direct {p0}, Lcom/google/android/setupwizard/SetupCompleteActivity;->backupAssistantExists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mBackupAssistantCheckbox:Landroid/widget/CompoundButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mSpecialNotificationMsg:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public start()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->start()V

    iget-object v1, p0, Lcom/google/android/setupwizard/SetupCompleteActivity;->mBackupAssistantCheckbox:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sncr.action.BAEULA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2710

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/SetupCompleteActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/SetupCompleteActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupCompleteActivity;->finish()V

    goto :goto_0
.end method
