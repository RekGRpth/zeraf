.class public Lcom/google/android/setupwizard/BaseActivity;
.super Lcom/google/android/setupwizard/NetworkMonitoringActivity;
.source "BaseActivity.java"

# interfaces
.implements Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/BaseActivity$ProportionalOuterFrame;,
        Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;
    }
.end annotation


# static fields
.field protected static final LOCAL_LOGV:Z

.field private static final ROTATION_LOCKED:Z

.field protected static final WAIT_FOR_GSERVICES:Z

.field private static final WIFI_REQUIRED:Z

.field private static final WIFI_REQUIRED_INFO:Z

.field private static mForceCdma:Z

.field protected static mUserData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final sWirelessSettingsIntent:Landroid/content/Intent;


# instance fields
.field mAgreementView:Landroid/app/AlertDialog;

.field private mAllNetworkSetupSkipped:Z

.field protected mAllowBackHardKey:Z

.field private final mBackButtonClickListener:Landroid/view/View$OnClickListener;

.field protected mBootstrapMode:Ljava/lang/Boolean;

.field private mDefaultButtonShouldScroll:Z

.field private mEnableBypass:Z

.field private mFrameLayout:Landroid/widget/FrameLayout;

.field private mIsFirstRun:Z

.field private mIsHomeActivity:Z

.field private mIsPessimisticMode:Z

.field private mIsSecondaryUser:Z

.field private final mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

.field private mOtaspMode:I

.field private final mOtaspPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPhoneServiceState:I

.field private mPrimaryButton:Landroid/view/View;

.field private mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

.field mScreenSize:I

.field protected mScrollView:Lcom/google/android/setupwizard/BottomScrollView;

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field private mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;

.field private mTouchEventState:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "SetupWizard"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.phone"

    const-string v2, "com.android.phone.Settings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/setupwizard/BaseActivity;->sWirelessSettingsIntent:Landroid/content/Intent;

    const-string v0, "setupwizard.force_cdma"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->mForceCdma:Z

    const-string v0, "ro.setupwizard.wifi_required"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->WIFI_REQUIRED:Z

    const-string v0, "ro.setupwizard.wifi_reqd_info"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->WIFI_REQUIRED_INFO:Z

    const-string v0, "ro.setupwizard.rotation_locked"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->ROTATION_LOCKED:Z

    const-string v0, "ro.setupwizard.gservices_wait"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->WAIT_FOR_GSERVICES:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;-><init>()V

    iput v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mTouchEventState:I

    iput-boolean v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsHomeActivity:Z

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->OPTIONAL:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    iput-boolean v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllNetworkSetupSkipped:Z

    iput-boolean v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mEnableBypass:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mBootstrapMode:Ljava/lang/Boolean;

    iput v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mPhoneServiceState:I

    iput v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspMode:I

    iput-boolean v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    new-instance v0, Lcom/google/android/setupwizard/BaseActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/BaseActivity$1;-><init>(Lcom/google/android/setupwizard/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspPhoneStateListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/google/android/setupwizard/BaseActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/BaseActivity$2;-><init>(Lcom/google/android/setupwizard/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mBackButtonClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/setupwizard/BaseActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/BaseActivity$3;-><init>(Lcom/google/android/setupwizard/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/BaseActivity;)I
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/BaseActivity;

    iget v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mPhoneServiceState:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/setupwizard/BaseActivity;I)I
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/BaseActivity;
    .param p1    # I

    iput p1, p0, Lcom/google/android/setupwizard/BaseActivity;->mPhoneServiceState:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/BaseActivity;)I
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/BaseActivity;

    iget v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspMode:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/setupwizard/BaseActivity;I)I
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/BaseActivity;
    .param p1    # I

    iput p1, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspMode:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/BaseActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/BaseActivity;

    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/setupwizard/BaseActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/BaseActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mPrimaryButton:Landroid/view/View;

    return-object v0
.end method

.method private allNetworkSetupSkipped()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllNetworkSetupSkipped:Z

    return v0
.end method

.method private getLteOnCdmaMode()I
    .locals 3

    const/4 v2, -0x1

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLteOnCdmaMode()I

    move-result v1

    if-ne v1, v2, :cond_1

    :cond_0
    const-string v0, "telephony.lteOnCdmaDevice"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLteOnCdmaMode()I

    move-result v0

    goto :goto_0
.end method

.method private hasNoAccounts()Z
    .locals 2

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchAccountSetup()V
    .locals 3

    const/4 v2, 0x1

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "startAccountSetup()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.accounts.AccountIntro"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "allowSkip"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0x2717

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "accountSetupLaunched"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private launchAccountSetupOrSkip()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->allNetworkSetupSkipped()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isNetworkConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isLTE()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->onAccountSetupCompleted(ZLandroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetup()V

    goto :goto_0
.end method

.method private launchActivationActivity()V
    .locals 7

    const/high16 v6, 0x7f090000

    const v5, 0x111002f

    const/4 v4, 0x0

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "doOtasp() E"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "launchActivationActivity() voice: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", lte: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isLTE()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", cdma_always: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mcc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->mcc:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mnc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->mnc:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isLTE()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.phone.PERFORM_CDMA_PROVISIONING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v2, "otasp_result_code_pending_intent"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/16 v1, 0x2713

    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_2

    const-string v0, "SetupWizard"

    const-string v1, "doOtasp() X RESULT_OK"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SetupWizard"

    const-string v2, "Failed to launch 10003"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityLaunchFailed()V

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/ActivationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2712

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private launchDateTimeSetupOrSkip()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "launchDateTimeSetupOrSkip()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isTimeSet()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isTimezoneSet()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startDateTimeSetup()V

    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onDateTimeSetupCompleted(Z)V

    goto :goto_0
.end method

.method private launchNameActivity()V
    .locals 4

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "launchNameActivity()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v0, "com.google.android.gsf.login"

    const-string v2, "com.google.android.gsf.login.NameActivity"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v2, "firstName"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v2, "firstName"

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "firstName"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v2, "lastName"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v2, "lastName"

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "lastName"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isSecondaryUser()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "nameType"

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_0
    const/16 v0, 0x271a

    invoke-direct {p0, v1, v0}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_3
    const-string v0, "nameType"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private launchOtaUpdateCheck()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "launchOtaUpdateCheck()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/OtaUpdateActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2723

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private launchSimMissingActivity()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/SimMissingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2719

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private launchSystemUpdateActivity()V
    .locals 3

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "launchSystemUpdateActivity()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gsf"

    const-string v2, "com.google.android.gsf.update.SystemUpdateActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x2724

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private launchWalledGardenCheck()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "launchWalledGardenCheck()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/WalledGardenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2721

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private launchWifiSetup()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-boolean v2, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v2, :cond_0

    const-string v2, "SetupWizard"

    const-string v3, "launchWifiSetup()"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.net.wifi.SETUP_WIFI_NETWORK"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->tryEnablingWifi(Z)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "only_access_points"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    const-string v3, "extra_prefs_show_button_bar"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v3, "extra_prefs_set_back_text"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "extra_prefs_show_skip"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v3, "wifi_show_custom_button"

    sget-boolean v4, Lcom/google/android/setupwizard/BaseActivity;->WIFI_REQUIRED:Z

    if-nez v4, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "wifi_auto_finish_on_connect"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "wifi_show_wifi_required_info"

    sget-boolean v1, Lcom/google/android/setupwizard/BaseActivity;->WIFI_REQUIRED_INFO:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v0, 0x2714

    invoke-direct {p0, v2, v0}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private maybeStartPrepaySetup()Z
    .locals 5

    const/4 v1, 0x0

    const-string v0, "SetupWizard"

    const-string v2, "mabyeStartPrepaySetup()"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isLTE()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v2, "is not LTE"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const-string v0, "ro.setupwizard.prepay_setup_url"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "SetupWizard"

    const-string v3, "prepay setup URL system property override found"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "setup_prepaid_data_service_url"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const-string v2, "SetupWizard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "prepay setup URL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPrepaySetupStatus()I

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prepay setup not required ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPrepaySetupStatus()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "setup_prepaid_data_service_url"

    invoke-static {v0, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, "SetupWizard"

    const-string v2, "no prepay setup URL from Gservices; falling back on default"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070002

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, "SetupWizard"

    const-string v2, "no prepay setup URL found anywhere"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/PrepaySetupActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2718

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private noName()Z
    .locals 2

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "doShowNameActivity"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "doShowNameActivity"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onAccountSetupCompleted(ZLandroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    const-string v0, "specialNotificationMsgHtml"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "specialNotificationMsgHtml"

    const-string v2, "specialNotificationMsgHtml"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    if-eqz p2, :cond_1

    const-string v0, "nameCompleted"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    if-eqz p2, :cond_2

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "firstName"

    const-string v2, "firstName"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "lastName"

    const-string v2, "lastName"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "doShowNameActivity"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startLocationSharingActivity()V

    :goto_1
    return-void

    :cond_3
    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "doShowNameActivity"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->startWelcomeActivity()V

    goto :goto_1
.end method

.method private onDateTimeSetupCompleted(Z)V
    .locals 3

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDateTimeSetupCompleted("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->noName()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchNameActivity()V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onNameActivityCompleted(Z)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startLocationSharingActivity()V

    goto :goto_0
.end method

.method private onNameActivityCompleted(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasNoAccounts()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startNoAccountTosActivity()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startSetupCompleteActivity()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isTimeSet()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isTimezoneSet()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startDateTimeSetup()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startLocationSharingActivity()V

    goto :goto_0
.end method

.method private onNoAccountTosCompleted(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startSetupCompleteActivity()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->noName()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchNameActivity()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onNameActivityCompleted(Z)V

    goto :goto_0
.end method

.method private onPhotoActivityCompleted(Z)V
    .locals 0
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startLocationSharingActivity()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchNameActivity()V

    goto :goto_0
.end method

.method private onSimMissingActivityCompleted(I)V
    .locals 3

    const-string v0, "SetupWizard"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSimMissingActivityCompleted("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onWelcomeScreenCompleted()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0
.end method

.method private startDateTimeSetup()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "startDateTimeSetup()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/DateTimeSetupActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2722

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startFirstRunActivityForResult(Landroid/content/Intent;I)V
    .locals 3

    const-string v0, "SetupWizard"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "starting activity "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "firstRun"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isFirstRun()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "hasMultipleUsers"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasMultipleUsers()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->setLastActivity(I)V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/setupwizard/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startLocationSharingActivity()V
    .locals 3

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "startLocationSharingActivity()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/setupwizard/LocationSharingActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "allNetworkSetupSkipped"

    iget-boolean v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllNetworkSetupSkipped:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v2, "accountSetupSkipped"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const-string v2, "no_back"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v0, 0x2715

    invoke-direct {p0, v1, v0}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startNoAccountTosActivity()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "startNoAccountTosActivity()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/NoAccountTosActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x271d

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startSetupCompleteActivity()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "startLocationSharingActivity()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/SetupCompleteActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x271c

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private updateLastSetupShown()V
    .locals 5

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v3, "android.SETUP_VERSION"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "last_setup_shown"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method protected canActivate()Z
    .locals 2

    iget v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mPhoneServiceState:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mPhoneServiceState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected disableNotifications()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mStatusBarManager:Landroid/app/StatusBarManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isHomeActivity()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mStatusBarManager:Landroid/app/StatusBarManager;

    const/high16 v1, 0x3e40000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "SetupWizard"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "skip disabling notfictions due to isHomeActivity() == "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isHomeActivity()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mStatusBarManager:Landroid/app/StatusBarManager;

    if-nez v0, :cond_1

    const-string v0, " and/or status bar manager is NULL"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # Landroid/view/KeyEvent;

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_2

    move v0, v3

    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    const/16 v6, 0x18

    if-ne v5, v6, :cond_3

    move v2, v3

    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    const/16 v6, 0x19

    if-ne v5, v6, :cond_4

    move v1, v3

    :goto_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v4

    if-eqz v4, :cond_0

    if-nez v2, :cond_0

    if-eqz v1, :cond_5

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    :cond_1
    :goto_3
    return v3

    :cond_2
    move v0, v4

    goto :goto_0

    :cond_3
    move v2, v4

    goto :goto_1

    :cond_4
    move v1, v4

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isHomeActivity()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->placeEmergencyCall()V

    goto :goto_3

    :cond_6
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_3
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mEnableBypass:Z

    if-eqz v8, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget-object v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getRight()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    const/high16 v9, 0x40400000

    div-float v5, v8, v9

    iget-object v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getBottom()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v9}, Landroid/widget/FrameLayout;->getTop()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    const/high16 v9, 0x40400000

    div-float v7, v8, v9

    iget-object v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v8

    int-to-float v8, v8

    sub-float v8, v4, v8

    cmpg-float v8, v8, v5

    if-gez v8, :cond_1

    const/4 v1, 0x1

    :goto_0
    iget-object v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getRight()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v8, v4

    cmpg-float v8, v8, v5

    if-gez v8, :cond_2

    const/4 v2, 0x1

    :goto_1
    iget-object v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getTop()I

    move-result v8

    int-to-float v8, v8

    sub-float v8, v6, v8

    cmpg-float v8, v8, v7

    if-gez v8, :cond_3

    const/4 v3, 0x1

    :goto_2
    iget-object v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getBottom()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v8, v6

    cmpg-float v8, v8, v7

    if-gez v8, :cond_4

    const/4 v0, 0x1

    :goto_3
    if-eqz v1, :cond_5

    if-eqz v3, :cond_5

    const/4 v8, 0x1

    iput v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mTouchEventState:I

    :goto_4
    iget v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mTouchEventState:I

    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    iget-boolean v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mEnableBypass:Z

    if-eqz v8, :cond_0

    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/google/android/setupwizard/BaseActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->enableNotifications()V

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {p0, v8, v9}, Lcom/google/android/setupwizard/BaseActivity;->onSetupComplete(ZZ)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    return v8

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    iget v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mTouchEventState:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    if-eqz v2, :cond_6

    if-eqz v3, :cond_6

    const/4 v8, 0x2

    iput v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mTouchEventState:I

    goto :goto_4

    :cond_6
    iget v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mTouchEventState:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_7

    if-eqz v2, :cond_7

    if-eqz v0, :cond_7

    const/4 v8, 0x3

    iput v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mTouchEventState:I

    goto :goto_4

    :cond_7
    iget v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mTouchEventState:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_8

    if-eqz v1, :cond_8

    if-eqz v0, :cond_8

    const/4 v8, 0x4

    iput v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mTouchEventState:I

    goto :goto_4

    :cond_8
    const/4 v8, 0x0

    iput v8, p0, Lcom/google/android/setupwizard/BaseActivity;->mTouchEventState:I

    goto :goto_4
.end method

.method protected enableNotifications()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mStatusBarManager:Landroid/app/StatusBarManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isHomeActivity()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mStatusBarManager:Landroid/app/StatusBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    :cond_0
    return-void
.end method

.method public getContentView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method protected getLastActivity()I
    .locals 3

    const-string v0, "SetupWizardPrefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "lastActivity"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method protected getOtaspMode()I
    .locals 1

    iget v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspMode:I

    return v0
.end method

.method protected getPrepaySetupStatus()I
    .locals 3

    const-string v0, "SetupWizardPrefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "prepay_status"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method protected getTelephonyInterface()Lcom/android/internal/telephony/ITelephony;
    .locals 2

    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;

    if-nez v1, :cond_0

    const-string v1, "phone"

    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;

    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;

    return-object v1
.end method

.method public hasMultipleUsers()Z
    .locals 2

    const/4 v1, 0x1

    const-string v0, "user"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected hasSimProblem()Z
    .locals 4

    const/4 v1, 0x1

    const-string v2, "phone"

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    move-result v2

    if-ne v2, v1, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected isFirstRun()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsFirstRun:Z

    return v0
.end method

.method protected isHomeActivity()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsHomeActivity:Z

    return v0
.end method

.method protected isLTE()Z
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLteOnCdmaMode()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isNetworkConnected()Z
    .locals 2

    const/4 v1, 0x0

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method protected isPessimisticMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsPessimisticMode:Z

    return v0
.end method

.method protected isSecondaryUser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsSecondaryUser:Z

    return v0
.end method

.method protected isWifiOnlyBuild()Z
    .locals 2

    const/4 v1, 0x0

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected lockRotation()V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "SetupWizardPrefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "user_rotation"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "setupwizard.accelerometer_rotation"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "setupwizard.user_rotation"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "user_rotation"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method protected lteUnknown()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLteOnCdmaMode()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected maybeRegisterOtaspPhoneStateListener()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isWifiOnlyBuild()Z

    move-result v1

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "SetupWizard"

    const-string v2, "Register our PhoneStateListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x201

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "SetupWizard"

    const-string v2, "Cannot register OTASP listener as TelephonyManager is null."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected maybeUnregisterOtaspPhoneStateListener()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isWifiOnlyBuild()Z

    move-result v1

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "SetupWizard"

    const-string v2, "Unregister our PhoneStateListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    :cond_1
    return-void
.end method

.method protected onActivationActivityCompleted(I)V
    .locals 3

    const-string v0, "SetupWizard"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivationActivityCompleted("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllNetworkSetupSkipped:Z

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWifiSetup()V

    return-void
.end method

.method protected onActivationActivityLaunchFailed()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v5, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    const-string v2, "SetupWizard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActivityResult("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") lastActivity="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLastActivity()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v0, "SetupWizard"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult() is called with unsupported requestCode. requestCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void

    :pswitch_1
    iput p2, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspMode:I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onWelcomeScreenCompleted()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onWelcomeUserScreenCompleted()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onOldActivationActivityCompleted(I)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onWifiSetupCompleted(I)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onWalledGardenCompleted(I)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onOtaUpdateCompleted(I)V

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onSystemUpdateActivityCompleted(I)V

    goto :goto_0

    :pswitch_9
    if-ne p2, v5, :cond_0

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onDateTimeSetupCompleted(Z)V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :pswitch_a
    const-string v2, "SetupWizard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "account setup completed with resultCode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v4, "accountSetupSkipped"

    const/4 v2, 0x7

    if-ne p2, v2, :cond_1

    move v2, v0

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_2

    :goto_3
    invoke-direct {p0, v0, p3}, Lcom/google/android/setupwizard/BaseActivity;->onAccountSetupCompleted(ZLandroid/content/Intent;)V

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_3

    :pswitch_b
    if-ne p2, v5, :cond_3

    :goto_4
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onNameActivityCompleted(Z)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_4

    :pswitch_c
    if-ne p2, v5, :cond_4

    :goto_5
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onPhotoActivityCompleted(Z)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_5

    :pswitch_d
    if-ne p2, v5, :cond_5

    move v1, v0

    :cond_5
    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->onSetupComplete(ZZ)V

    goto :goto_0

    :pswitch_e
    invoke-direct {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onSimMissingActivityCompleted(I)V

    goto :goto_0

    :pswitch_f
    if-ne p2, v5, :cond_6

    :goto_6
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onLocationSharingActivityCompleted(Z)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_6

    :pswitch_10
    if-ne p2, v5, :cond_7

    :goto_7
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onNoAccountTosCompleted(Z)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_7

    :pswitch_11
    if-ne p2, v5, :cond_8

    :goto_8
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onSetupCompleteCompleted(Z)V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x2711
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_f
        :pswitch_0
        :pswitch_a
        :pswitch_d
        :pswitch_e
        :pswitch_b
        :pswitch_c
        :pswitch_11
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_9
        :pswitch_7
        :pswitch_8
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->onCreate(Landroid/os/Bundle;)V

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate: LOCAL_LOGV="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isLoggable="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "SetupWizard"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "setupwizard.pessimistic"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsPessimisticMode:Z

    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsPessimisticMode:Z

    if-eqz v0, :cond_1

    const-string v0, "SetupWizard"

    const-string v3, "In pessimistic mode."

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    iput v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mScreenSize:I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v3, "firstRun"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "firstRun"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsFirstRun:Z

    :cond_2
    const-string v3, "allNetworkSetupSkipped"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "allNetworkSetupSkipped"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllNetworkSetupSkipped:Z

    :cond_3
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsSecondaryUser:Z

    const-string v0, "SetupWizard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BaseActivity.onCreate() mIsFirstRun="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsFirstRun:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mAllNetworkSetupSkipped="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllNetworkSetupSkipped:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mIsSecondaryUser="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsSecondaryUser:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/setupwizard/SetupWizardActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v0

    if-eqz v0, :cond_4

    if-ne v0, v1, :cond_b

    :cond_4
    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsHomeActivity:Z

    const-string v0, "ro.setupwizard.mode"

    sget-object v3, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->OPTIONAL:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    invoke-virtual {v3}, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->valueOf(Ljava/lang/String;)Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isHomeActivity()Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllowBackHardKey:Z

    const-string v0, "ro.debuggable"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.setupwizard.enable_bypass"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    move v2, v1

    :cond_6
    iput-boolean v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mEnableBypass:Z

    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mEnableBypass:Z

    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->DISABLED:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    :cond_7
    const-string v0, "statusbar"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mStatusBarManager:Landroid/app/StatusBarManager;

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mStatusBarManager:Landroid/app/StatusBarManager;

    if-nez v0, :cond_8

    const-string v0, "SetupWizard"

    const-string v1, "StatusBarManager isn\'t available."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->disableNotifications()V

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    const/high16 v1, 0x400000

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setSystemUiVisibility(I)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-super {p0, v0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->setContentView(Landroid/view/View;)V

    return-void

    :cond_a
    move v0, v2

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->OPTIONAL:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    goto :goto_2

    :cond_c
    move v0, v2

    goto :goto_3
.end method

.method protected onLocationSharingActivityCompleted(Z)V
    .locals 0
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchDateTimeSetupOrSkip()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetup()V

    goto :goto_0
.end method

.method protected onOldActivationActivityCompleted(I)V
    .locals 4

    const/4 v0, 0x0

    const-string v1, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onOldActivationActivityCompleted("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getTelephonyInterface()Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->needsOtaServiceProvisioning()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    return-void

    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected onOtaUpdateCompleted(I)V
    .locals 1

    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetupOrSkip()V

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_2

    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    check-cast v0, Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->disconnect()Z

    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWifiSetup()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchSystemUpdateActivity()V

    goto :goto_0
.end method

.method protected onOtaspChanged()V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->onPause()V

    return-void
.end method

.method public onRequiresScroll()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v3, -0x1

    const/4 v6, 0x1

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v2, "currentFocus"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    :cond_0
    const-string v2, "screenTimeout"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "screenTimeout"

    const-string v4, "screenTimeout"

    const v5, 0x1d8a8

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v2, "doShowNameActivity"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "doShowNameActivity"

    const-string v4, "doShowNameActivity"

    invoke-virtual {p1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    const-string v2, "accountSetupSkipped"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "accountSetupSkipped"

    const-string v4, "accountSetupSkipped"

    invoke-virtual {p1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    const-string v2, "accountSetupLaunched"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "accountSetupLaunched"

    const-string v4, "accountSetupLaunched"

    invoke-virtual {p1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    const-string v2, "specialNotificationMsgHtml"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v2, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "specialNotificationMsgHtml"

    const-string v4, "specialNotificationMsgHtml"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->onResume()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    const-string v2, "currentFocus"

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    :goto_0
    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v1, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v2, "screenTimeout"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v2, "screenTimeout"

    sget-object v1, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "screenTimeout"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    sget-object v1, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v2, "doShowNameActivity"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v2, "doShowNameActivity"

    sget-object v1, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "doShowNameActivity"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    sget-object v1, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v2, "accountSetupSkipped"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v2, "accountSetupSkipped"

    sget-object v1, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "accountSetupSkipped"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    sget-object v1, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v2, "accountSetupLaunched"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v2, "accountSetupLaunched"

    sget-object v1, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "accountSetupLaunched"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_3
    sget-object v1, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v2, "specialNotificationMsgHtml"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v2, "specialNotificationMsgHtml"

    sget-object v1, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v3, "specialNotificationMsgHtml"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void

    :cond_5
    const/4 v1, -0x1

    goto/16 :goto_0
.end method

.method public onScrolledToBottom()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    return-void
.end method

.method protected onServiceStateChanged()V
    .locals 0

    return-void
.end method

.method protected onSetupComplete(ZZ)V
    .locals 7

    const/high16 v6, 0x10200000

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->enableNotifications()V

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v2, "screenTimeout"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_off_timeout"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-lez v0, :cond_0

    const v3, 0x1d8a8

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_off_timeout"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->ROTATION_LOCKED:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->unlockRotation()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "device_provisioned"

    invoke-static {v0, v2, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "user_setup_complete"

    invoke-static {v0, v2, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->updateLastSetupShown()V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isHomeActivity()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "SetupWizard"

    const-string v2, "System thinks we\'re the home activity. Removing SetupWizard..."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/google/android/setupwizard/SetupWizardActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "android.intent.category.HOME"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->startActivity(Landroid/content/Intent;)V

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "setup_prepaid_data_service_url"

    invoke-static {v0, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPrepaySetupStatus()I

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const v0, 0x8000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->startActivity(Landroid/content/Intent;)V

    :cond_3
    :goto_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/PrepayDetectionService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->stopService(Landroid/content/Intent;)Z

    const-string v0, "SetupWizard"

    const-string v1, "*** SetupWizard completed ***"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    return-void

    :cond_4
    new-array v1, v4, [Ljava/lang/CharSequence;

    aput-object v0, v1, v5

    invoke-static {v2, v1}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    const-string v0, "SetupWizard"

    const-string v2, "Setup completion notification is sent when SetupWizard isn\'t treated as home activity."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method protected onSetupCompleteCompleted(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->maybeStartPrepaySetup()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->onSetupComplete(ZZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasNoAccounts()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startNoAccountTosActivity()V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->onNoAccountTosCompleted(Z)V

    goto :goto_0
.end method

.method protected onSetupStart()V
    .locals 5

    const v4, 0x1d8a8

    sget-object v0, Lcom/google/android/setupwizard/BaseActivity;->mUserData:Ljava/util/HashMap;

    const-string v1, "screenTimeout"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_off_timeout"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_off_timeout"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->ROTATION_LOCKED:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->lockRotation()V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/setupwizard/NetworkMonitoringActivity;->onStart()V

    const v0, 0x7f0d001b

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/BottomScrollView;

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mScrollView:Lcom/google/android/setupwizard/BottomScrollView;

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mScrollView:Lcom/google/android/setupwizard/BottomScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mScrollView:Lcom/google/android/setupwizard/BottomScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/setupwizard/BottomScrollView;->setBottomScrollListener(Lcom/google/android/setupwizard/BottomScrollView$BottomScrollListener;)V

    :cond_0
    return-void
.end method

.method protected onSystemUpdateActivityCompleted(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetupOrSkip()V

    return-void
.end method

.method protected onWalledGardenCompleted(I)V
    .locals 1

    if-nez p1, :cond_1

    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    check-cast v0, Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->disconnect()Z

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWifiSetup()V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchOtaUpdateCheck()V

    goto :goto_0
.end method

.method protected onWelcomeScreenCompleted()V
    .locals 3

    const/4 v2, -0x1

    const-string v0, "SetupWizard"

    const-string v1, "onWelcomeScreenCompleted()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isPessimisticMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard"

    const-string v1, "Be pessimistic. Launch ActivationActivity anyway."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchActivationActivity()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isWifiOnlyBuild()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "SetupWizard"

    const-string v1, "skipping activation for wifi-only build"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPrepaySetupStatus()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0

    :cond_1
    const-string v0, "SetupWizard"

    const-string v1, "no network or lte prepay required: launching wifi"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWifiSetup()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->simMissing()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_3

    const-string v0, "SetupWizard"

    const-string v1, "missing SIM; launching SimMissingActivity"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchSimMissingActivity()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasSimProblem()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "SetupWizard"

    const-string v1, "bad SIM; launching wifi"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWifiSetup()V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->otaspStateIsKnown()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->otaspIsNeeded()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    const-string v0, "SetupWizard"

    const-string v1, "otasp not needed or not known, skipping activation"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->lteUnknown()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "SetupWizard"

    const-string v1, "LTE state still unknown, skipping activation"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0

    :cond_8
    const-string v0, "SetupWizard"

    const-string v1, "falling through to launch activation"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchActivationActivity()V

    goto/16 :goto_0
.end method

.method protected onWelcomeUserScreenCompleted()V
    .locals 2

    const-string v0, "SetupWizard"

    const-string v1, "onWelcomeUserScreenCompleted()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetup()V

    return-void
.end method

.method protected onWifiSetupCompleted(I)V
    .locals 1
    .param p1    # I

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllNetworkSetupSkipped:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetupOrSkip()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchOtaUpdateCheck()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllNetworkSetupSkipped:Z

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWalledGardenCheck()V

    goto :goto_0
.end method

.method protected otaspIsNeeded()Z
    .locals 2

    iget v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected otaspIsNotNeeded()Z
    .locals 2

    iget v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected otaspStateIsKnown()Z
    .locals 2

    iget v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspMode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOtaspMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected overrideAllowBackHardkey()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllowBackHardKey:Z

    return-void
.end method

.method protected placeEmergencyCall()V
    .locals 4

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.phone.EmergencyDialer.DIAL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10800000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "SetupWizard"

    const-string v3, "Can\'t find the emergency dialer: com.android.phone.EmergencyDialer.DIAL"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected provisioningDisabled()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    sget-object v1, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->DISABLED:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    sget-object v1, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->EMULATOR:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setBackButton(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mBackButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllowBackHardKey:Z

    :cond_0
    return-void
.end method

.method public setContentView(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected final setDefaultButton(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iput-object p1, p0, Lcom/google/android/setupwizard/BaseActivity;->mPrimaryButton:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected setLastActivity(I)V
    .locals 2

    const-string v0, "SetupWizardPrefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastActivity"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method protected showAgreement(Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;)V
    .locals 1
    .param p1    # Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;

    new-instance v0, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;

    invoke-direct {v0, p0, p1}, Lcom/google/android/setupwizard/LinkSpan$WebViewDialog;-><init>(Lcom/google/android/setupwizard/BaseActivity;Lcom/google/android/setupwizard/LinkSpan$AndroidPolicy;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAgreementView:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAgreementView:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method protected simMissing()Z
    .locals 5

    const/4 v1, 0x1

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    const-string v2, "SetupWizard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSimState() == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    if-ne v2, v1, :cond_1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isLTE()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected simStateUnknown()Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v3, "phone"

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    move-result v3

    if-eq v3, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isLTE()Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method protected start()V
    .locals 0

    return-void
.end method

.method protected startPrepayDetection()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/PrepayDetectionService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method protected startWelcomeActivity()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isSecondaryUser()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/WelcomeUserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2725

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLastActivity()I

    move-result v0

    const-string v1, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startWelcomeActivity() lastActivity="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x2724

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mAllNetworkSetupSkipped:Z

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetupOrSkip()V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/WelcomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2711

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected tryEnablingWifi(Z)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "wifi"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1}, Landroid/net/wifi/IWifiManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/wifi/IWifiManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/net/wifi/IWifiManager;->getWifiEnabledState()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eq p1, v1, :cond_0

    invoke-interface {v2, p1}, Landroid/net/wifi/IWifiManager;->setWifiEnabled(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return p1

    :cond_1
    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "SetupWizard"

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    move p1, v0

    goto :goto_1
.end method

.method protected unlockRotation()V
    .locals 4

    const-string v0, "SetupWizardPrefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "setupwizard.accelerometer_rotation"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "setupwizard.user_rotation"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "user_rotation"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method
