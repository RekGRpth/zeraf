.class public Lcom/google/android/setupwizard/LocationSharingActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "LocationSharingActivity.java"


# instance fields
.field private mAgreeSharing:Landroid/widget/CompoundButton;

.field private mAgreeUseSearch:Landroid/widget/CompoundButton;

.field private mBackButton:Landroid/view/View;

.field private mExplanationText:Landroid/widget/TextView;

.field private mNextButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method

.method private initViews()V
    .locals 3

    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/LocationSharingActivity;->setContentView(I)V

    const v0, 0x7f0d000d

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/LocationSharingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mNextButton:Landroid/view/View;

    const v0, 0x7f0d0010

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/LocationSharingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mBackButton:Landroid/view/View;

    const v0, 0x7f0d001c

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/LocationSharingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mExplanationText:Landroid/widget/TextView;

    const v0, 0x7f0d001d

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/LocationSharingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mAgreeSharing:Landroid/widget/CompoundButton;

    const v0, 0x7f0d001e

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/LocationSharingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mAgreeUseSearch:Landroid/widget/CompoundButton;

    iget-object v0, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mBackButton:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/LocationSharingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "no_back"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mBackButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mNextButton:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/LocationSharingActivity;->setDefaultButton(Landroid/view/View;Z)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/LocationSharingActivity;->isWifiOnlyBuild()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mExplanationText:Landroid/widget/TextView;

    const v1, 0x7f070040

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/LocationSharingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mBackButton:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/LocationSharingActivity;->setBackButton(Landroid/view/View;)V

    goto :goto_0
.end method

.method private loadPreferences()V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/LocationSharingActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mAgreeSharing:Landroid/widget/CompoundButton;

    const-string v2, "agreeSharing"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mAgreeUseSearch:Landroid/widget/CompoundButton;

    const-string v2, "agreeUseSearch"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-void
.end method

.method private savePreferences()V
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/LocationSharingActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "agreeSharing"

    iget-object v3, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mAgreeSharing:Landroid/widget/CompoundButton;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v2, "agreeUseSearch"

    iget-object v3, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mAgreeUseSearch:Landroid/widget/CompoundButton;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/LocationSharingActivity;->initViews()V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/LocationSharingActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/setupwizard/LocationSharingActivity;->loadPreferences()V

    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    invoke-direct {p0}, Lcom/google/android/setupwizard/LocationSharingActivity;->savePreferences()V

    return-void
.end method

.method protected start()V
    .locals 5

    iget-object v3, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mAgreeSharing:Landroid/widget/CompoundButton;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/LocationSharingActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/setupwizard/LocationSharingActivity;->mAgreeUseSearch:Landroid/widget/CompoundButton;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v0, :cond_0

    const-string v3, "network_location_opt_in"

    const-string v4, "1"

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    const-string v3, "network"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    invoke-static {p0, v1}, Lcom/google/android/gsf/UseLocationForServices;->setUseLocationForServices(Landroid/content/Context;Z)Z

    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/LocationSharingActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/LocationSharingActivity;->finish()V

    return-void
.end method
