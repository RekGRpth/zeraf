.class public Lcom/google/android/setupwizard/ActivationActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "ActivationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final GENERIC_TEST_SUCCESS_DELAY:I

.field private final STATE_ACTIVATING:I

.field private final STATE_CANCEL_COMPLETED:I

.field private final STATE_CANCEL_REQUESTED:I

.field private final STATE_FAILED:I

.field private final STATE_NOT_STARTED:I

.field private final STATE_SHUTTING_DOWN:I

.field private final STATE_SUCCESS:I

.field private final STATE_WAITING_FOR_ACTIVATION_STATE:I

.field private final WAITING_FOR_CANCEL_TIMEOUT:I

.field private mActivationText:Landroid/widget/TextView;

.field private mActivationTitle:Landroid/widget/TextView;

.field private mAllowDontShowMode:Z

.field private mCancelButton:Landroid/widget/Button;

.field private mCancelEndtime:J

.field private mContinueSkipButton:Landroid/widget/Button;

.field private mCountdownText:Landroid/widget/TextView;

.field private mDoExitToHome:Z

.field private mFilter:Landroid/content/IntentFilter;

.field private mGenericEndtime:J

.field private mGenericMode:Z

.field private mGenericOtaspDefinitelyNeeded:Z

.field private final mGenericTestSuccess:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private mNetworkBars:Landroid/widget/ImageView;

.field private mNetworkName:Ljava/lang/CharSequence;

.field private mNetworkNotifier:Landroid/view/View;

.field private mNextButton:Landroid/widget/Button;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mServiceState:Landroid/telephony/ServiceState;

.field private mSharedPrefs:Landroid/content/SharedPreferences;

.field private mSignalStrength:Landroid/telephony/SignalStrength;

.field private mSignalStrengthImages:Landroid/content/res/TypedArray;

.field private mSignalStrengthPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mSkipButton:Landroid/widget/Button;

.field private mSkipButtonForStandAlone:Landroid/widget/Button;

.field private mState:I

.field private final mTimeOutWaitingForCancel:Ljava/lang/Runnable;

.field private final mTimeOutWaitingForGenericActivation:Ljava/lang/Runnable;

.field private mTopDivider:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x2710

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mAllowDontShowMode:Z

    iput-boolean v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    new-instance v0, Lcom/google/android/setupwizard/ActivationActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/ActivationActivity$1;-><init>(Lcom/google/android/setupwizard/ActivationActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTimeOutWaitingForGenericActivation:Ljava/lang/Runnable;

    iput v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->GENERIC_TEST_SUCCESS_DELAY:I

    new-instance v0, Lcom/google/android/setupwizard/ActivationActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/ActivationActivity$2;-><init>(Lcom/google/android/setupwizard/ActivationActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericTestSuccess:Ljava/lang/Runnable;

    iput v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->STATE_NOT_STARTED:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->STATE_WAITING_FOR_ACTIVATION_STATE:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->STATE_ACTIVATING:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->STATE_CANCEL_REQUESTED:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->STATE_CANCEL_COMPLETED:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->STATE_SUCCESS:I

    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->STATE_FAILED:I

    const/4 v0, 0x7

    iput v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->STATE_SHUTTING_DOWN:I

    iput v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    new-instance v0, Lcom/google/android/setupwizard/ActivationActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/ActivationActivity$3;-><init>(Lcom/google/android/setupwizard/ActivationActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSignalStrengthPhoneStateListener:Landroid/telephony/PhoneStateListener;

    iput v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->WAITING_FOR_CANCEL_TIMEOUT:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/setupwizard/ActivationActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/ActivationActivity$4;-><init>(Lcom/google/android/setupwizard/ActivationActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTimeOutWaitingForCancel:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/setupwizard/ActivationActivity;J)J
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericEndtime:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/ActivationActivity;)I
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;

    iget v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    return v0
.end method

.method static synthetic access$1000(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getNetworkNameFromIntent(Landroid/content/Intent;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/setupwizard/ActivationActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->shouldChangeTitleUsingNetworkName()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/setupwizard/ActivationActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/setupwizard/ActivationActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCountdownText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/setupwizard/ActivationActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->doShutdown()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/ActivationActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onFailure()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/setupwizard/ActivationActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onSuccess()V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/setupwizard/ActivationActivity;Landroid/telephony/SignalStrength;)Landroid/telephony/SignalStrength;
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;
    .param p1    # Landroid/telephony/SignalStrength;

    iput-object p1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSignalStrength:Landroid/telephony/SignalStrength;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/setupwizard/ActivationActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->updateSignalStrength()V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/setupwizard/ActivationActivity;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;
    .param p1    # Landroid/telephony/ServiceState;

    iput-object p1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mServiceState:Landroid/telephony/ServiceState;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/android/setupwizard/ActivationActivity;J)J
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelEndtime:J

    return-wide p1
.end method

.method static synthetic access$800(Lcom/google/android/setupwizard/ActivationActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onCancelCompleted()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/setupwizard/ActivationActivity;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/setupwizard/ActivationActivity;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/ActivationActivity;
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    return-object p1
.end method

.method private animateProgressBar(Z)V
    .locals 4
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/setupwizard/ActivationActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTopDivider:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1
.end method

.method private static calculateSignalStrength(Landroid/telephony/SignalStrength;Landroid/telephony/ServiceState;I)I
    .locals 1
    .param p0    # Landroid/telephony/SignalStrength;
    .param p1    # Landroid/telephony/ServiceState;
    .param p2    # I

    invoke-static {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isCdma(Landroid/telephony/SignalStrength;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getGsmLevel(Landroid/telephony/SignalStrength;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    if-nez p2, :cond_1

    invoke-static {p1}, Lcom/google/android/setupwizard/ActivationActivity;->isEvdo(Landroid/telephony/ServiceState;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getEvdoLevel(Landroid/telephony/SignalStrength;)I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getCdmaLevel(Landroid/telephony/SignalStrength;)I

    move-result v0

    goto :goto_0
.end method

.method private doShutdown()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.extra.KEY_CONFIRM"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/ActivationActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static getCdmaLevel(Landroid/telephony/SignalStrength;)I
    .locals 5
    .param p0    # Landroid/telephony/SignalStrength;

    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaEcio()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, -0x4b

    if-lt v0, v4, :cond_0

    const/4 v2, 0x4

    :goto_0
    const/16 v4, -0x5a

    if-lt v1, v4, :cond_4

    const/4 v3, 0x4

    :goto_1
    if-ge v2, v3, :cond_8

    :goto_2
    return v2

    :cond_0
    const/16 v4, -0x55

    if-lt v0, v4, :cond_1

    const/4 v2, 0x3

    goto :goto_0

    :cond_1
    const/16 v4, -0x5f

    if-lt v0, v4, :cond_2

    const/4 v2, 0x2

    goto :goto_0

    :cond_2
    const/16 v4, -0x64

    if-lt v0, v4, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const/16 v4, -0x6e

    if-lt v1, v4, :cond_5

    const/4 v3, 0x3

    goto :goto_1

    :cond_5
    const/16 v4, -0x82

    if-lt v1, v4, :cond_6

    const/4 v3, 0x2

    goto :goto_1

    :cond_6
    const/16 v4, -0x96

    if-lt v1, v4, :cond_7

    const/4 v3, 0x1

    goto :goto_1

    :cond_7
    const/4 v3, 0x0

    goto :goto_1

    :cond_8
    move v2, v3

    goto :goto_2
.end method

.method private static getEvdoLevel(Landroid/telephony/SignalStrength;)I
    .locals 5
    .param p0    # Landroid/telephony/SignalStrength;

    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoDbm()I

    move-result v0

    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoSnr()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, -0x41

    if-lt v0, v4, :cond_0

    const/4 v2, 0x4

    :goto_0
    const/4 v4, 0x7

    if-lt v1, v4, :cond_4

    const/4 v3, 0x4

    :goto_1
    if-ge v2, v3, :cond_8

    :goto_2
    return v2

    :cond_0
    const/16 v4, -0x4b

    if-lt v0, v4, :cond_1

    const/4 v2, 0x3

    goto :goto_0

    :cond_1
    const/16 v4, -0x5a

    if-lt v0, v4, :cond_2

    const/4 v2, 0x2

    goto :goto_0

    :cond_2
    const/16 v4, -0x69

    if-lt v0, v4, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const/4 v4, 0x5

    if-lt v1, v4, :cond_5

    const/4 v3, 0x3

    goto :goto_1

    :cond_5
    const/4 v4, 0x3

    if-lt v1, v4, :cond_6

    const/4 v3, 0x2

    goto :goto_1

    :cond_6
    const/4 v4, 0x1

    if-lt v1, v4, :cond_7

    const/4 v3, 0x1

    goto :goto_1

    :cond_7
    const/4 v3, 0x0

    goto :goto_1

    :cond_8
    move v2, v3

    goto :goto_2
.end method

.method private getGenericActivationTimeout()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    const-string v1, "ro.activation.timeout.millis"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static getGsmLevel(Landroid/telephony/SignalStrength;)I
    .locals 3
    .param p0    # Landroid/telephony/SignalStrength;

    const/4 v1, 0x2

    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    if-le v0, v1, :cond_0

    const/16 v2, 0x63

    if-ne v0, v2, :cond_2

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1

    :cond_2
    const/16 v2, 0xc

    if-lt v0, v2, :cond_3

    const/4 v1, 0x4

    goto :goto_0

    :cond_3
    const/16 v2, 0x8

    if-lt v0, v2, :cond_4

    const/4 v1, 0x3

    goto :goto_0

    :cond_4
    const/4 v2, 0x5

    if-ge v0, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static getNetworkNameFromIntent(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "showPlmn"

    const-string v3, "plmn"

    invoke-static {p0, v0, v3}, Lcom/google/android/setupwizard/ActivationActivity;->getSpecificNetworkName(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    const-string v0, "showSpn"

    const-string v4, "spn"

    invoke-static {p0, v0, v4}, Lcom/google/android/setupwizard/ActivationActivity;->getSpecificNetworkName(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    move-object v0, v3

    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    if-nez v0, :cond_4

    if-eqz v1, :cond_4

    move-object v0, v4

    goto :goto_2

    :cond_4
    const-string v0, ""

    goto :goto_2
.end method

.method private static getSpecificNetworkName(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Landroid/content/Intent;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private goNextOrFinish(I)V
    .locals 3

    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "goNextOrFinish("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mDoExitToHome:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/ActivationActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->finish()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/ActivationActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/ActivationActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->finish()V

    goto :goto_0
.end method

.method private initStandaloneView()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->overrideAllowBackHardkey()V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f07001d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/ActivationActivity;->animateProgressBar(Z)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    const v1, 0x7f070020

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method private static isCdma(Landroid/telephony/SignalStrength;)Z
    .locals 1
    .param p0    # Landroid/telephony/SignalStrength;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isEvdo(Landroid/telephony/ServiceState;)Z
    .locals 3
    .param p0    # Landroid/telephony/ServiceState;

    const/4 v1, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Landroid/telephony/ServiceState;->getNetworkType()I

    move-result v0

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private maybeActivateOrExit()V
    .locals 3

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "maybeActivateOrExit() mState == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->otaspStateIsKnown()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->otaspIsNeeded()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "ActivationActivity"

    const-string v1, "maybeActivateOrExit(): activation not needed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isPessimisticMode()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "ActivationActivity"

    const-string v1, "Be pessimistic. Show failure screen anyway."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onFailure()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onSuccess()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->canActivate()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ActivationActivity"

    const-string v1, "maybeActivateOrExit(): activation required"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->performActivationCall()V

    goto :goto_0
.end method

.method private maybeEndGenericActivation()V
    .locals 3

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "maybeEndGenericActivation() mState == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->otaspIsNeeded()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->otaspIsNotNeeded()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onSuccess()V

    :cond_2
    return-void
.end method

.method private onActualSkip()V
    .locals 3

    const-string v0, "ActivationActivity"

    const-string v1, "onActualSkip"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mAllowDontShowMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dont_show_again"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/ActivationActivity;->goNextOrFinish(I)V

    return-void
.end method

.method private onCancelCompleted()V
    .locals 6

    const/4 v5, 0x4

    iget v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const-string v0, "ActivationActivity"

    const-string v1, "Unexpected state migration request (%d -> %d)."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "onCancelCompleted()"

    invoke-direct {p0, v5, v0}, Lcom/google/android/setupwizard/ActivationActivity;->setState(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showCancelCompletedScreen()V

    return-void
.end method

.method private onCancelRequested()V
    .locals 7

    const/4 v6, 0x3

    const/4 v2, 0x2

    const/4 v5, 0x1

    iget v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    if-eq v0, v5, :cond_0

    iget v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    if-eq v0, v2, :cond_0

    const-string v0, "ActivationActivity"

    const-string v1, "Unexpected state migration request (%d -> %d). Ignored."

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v0, "onCancelRequested()"

    invoke-direct {p0, v6, v0}, Lcom/google/android/setupwizard/ActivationActivity;->setState(ILjava/lang/String;)V

    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->endCall()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showCancelRequestedScreen()V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ActivationActivity"

    const-string v2, "Exception hanging up activation call: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private onFailure()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    const-string v1, "onFailure"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x6

    const-string v1, "onFailure()"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/ActivationActivity;->setState(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showFailureScreen()V

    return-void
.end method

.method private onSpcRetriesFailure()V
    .locals 6

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    const-string v1, "onSpcRetriesFailure"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x7

    const-string v1, "onSpcRetriesFailure()"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/ActivationActivity;->setState(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showShuttingDownScreen()V

    new-instance v0, Lcom/google/android/setupwizard/ActivationActivity$6;

    const-wide/32 v2, 0xea60

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/setupwizard/ActivationActivity$6;-><init>(Lcom/google/android/setupwizard/ActivationActivity;JJ)V

    invoke-virtual {v0}, Lcom/google/android/setupwizard/ActivationActivity$6;->start()Landroid/os/CountDownTimer;

    return-void
.end method

.method private onStateChanges()V
    .locals 4

    const/4 v3, 0x1

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChanges() mState == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->otaspIsNeeded()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ActivationActivity"

    const-string v1, "onStateChanges(): generic otasp definitely needed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    :cond_1
    iget v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->otaspIsNotNeeded()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ActivationActivity"

    const-string v1, "onStateChanges(): generic otasp succeeded"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onSuccess()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    if-ne v0, v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->otaspStateIsKnown()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->otaspIsNeeded()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "ActivationActivity"

    const-string v1, "onStateChanges(): activation not needed after all"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onSuccess()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->canActivate()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ActivationActivity"

    const-string v1, "onStateChanges(): activation call now possible"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->performActivationCall()V

    goto :goto_0
.end method

.method private onSuccess()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    const-string v1, "onSuccess"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x5

    const-string v1, "onSuccess"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/ActivationActivity;->setState(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showSuccessScreen()V

    return-void
.end method

.method private performActivationCall()V
    .locals 7

    sget-boolean v4, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v4, :cond_0

    const-string v4, "ActivationActivity"

    const-string v5, "startActivationCall"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v4, 0x2

    const-string v5, "performActivationCall()"

    invoke-direct {p0, v4, v5}, Lcom/google/android/setupwizard/ActivationActivity;->setState(ILjava/lang/String;)V

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.android.phone.PERFORM_CDMA_PROVISIONING"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {p0, v4, v3, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const-string v4, "otasp_result_code_pending_intent"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/16 v4, 0x4e21

    invoke-virtual {p0, v1, v4}, Lcom/google/android/setupwizard/ActivationActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v4, "ActivationActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Couldn\'t start activation call; ActivityNotFoundException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerOtaspListener()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    const-string v1, "registering otasp listener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->maybeRegisterOtaspPhoneStateListener()V

    return-void
.end method

.method private setState(ILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setState(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " changing mState from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    return-void

    :cond_0
    const-string p2, ""

    goto :goto_0
.end method

.method private shouldChangeTitleUsingNetworkName()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showActivationProcessScreen()V
    .locals 7

    const/4 v6, 0x0

    const v2, 0x7f07001a

    const/4 v5, 0x1

    const/16 v4, 0x8

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    const-string v1, "showProgress()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/ActivationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f070018

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/ActivationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-direct {p0, v5}, Lcom/google/android/setupwizard/ActivationActivity;->animateProgressBar(Z)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    :cond_1
    :goto_1
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070023

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f070019

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    aput-object v3, v2, v6

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/ActivationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    const v1, 0x7f07000c

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_1

    :cond_4
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    const v1, 0x7f07001a

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/ActivationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070021

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    :try_start_2
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070022

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    const v1, 0x7f07001a

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/ActivationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070024

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070025

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private showCancelCompletedScreen()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/ActivationActivity;->animateProgressBar(Z)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f070030

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    const v1, 0x7f07000b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    const v1, 0x7f070009

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070031

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    const v1, 0x7f070020

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070032

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private showCancelRequestedScreen()V
    .locals 6

    const-wide/16 v4, 0x2710

    const/4 v3, 0x0

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f07002e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f07002f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/ActivationActivity;->animateProgressBar(Z)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelEndtime:J

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTimeOutWaitingForCancel:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private showFailureScreen()V
    .locals 5

    const v1, 0x7f07002c

    const/16 v4, 0x8

    const/4 v3, 0x0

    const-string v0, "ActivationActivity"

    const-string v2, "showFailureScreen()"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkNotifier:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/ActivationActivity;->animateProgressBar(Z)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v2, 0x7f07002a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_0

    const v0, 0x7f070010

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_1

    const v0, 0x7f07000c

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    return-void

    :cond_0
    const v0, 0x7f07000b

    goto :goto_0

    :cond_1
    const v0, 0x7f070009

    goto :goto_1

    :cond_2
    const v0, 0x7f07002b

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    const v2, 0x7f070011

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v2, :cond_4

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :cond_4
    const v1, 0x7f07002d

    goto :goto_4
.end method

.method private showShuttingDownScreen()V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkNotifier:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/ActivationActivity;->animateProgressBar(Z)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    const v1, 0x7f070034

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f07002a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070033

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const-string v0, "statusbar"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/ActivationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    if-eqz v0, :cond_0

    const/high16 v1, 0x1a40000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    :goto_0
    const v0, 0x7f0d0009

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCountdownText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCountdownText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const-string v0, "ActivationActivity"

    const-string v1, "Unable to instantiate status bar manager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showSuccessScreen()V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0x8

    const-string v0, "ActivationActivity"

    const-string v1, "showSuccessScreen"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/ActivationActivity;->animateProgressBar(Z)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    if-eqz v0, :cond_0

    const v0, 0x7f070035

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f070026

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    if-eqz v0, :cond_1

    const v0, 0x7f070028

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    return-void

    :cond_0
    const v0, 0x7f070009

    goto :goto_0

    :cond_1
    const v0, 0x7f070027

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070029

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2
.end method

.method private startActivationProcess()V
    .locals 5

    iget-boolean v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    :goto_0
    const-string v2, "startActivationProcess()"

    invoke-direct {p0, v1, v2}, Lcom/google/android/setupwizard/ActivationActivity;->setState(ILjava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getGenericActivationTimeout()I

    move-result v0

    const-string v1, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setting generic timeout in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    int-to-long v3, v0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericEndtime:J

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTimeOutWaitingForGenericActivation:Ljava/lang/Runnable;

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showActivationProcessScreen()V

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onStateChanges()V

    return-void

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private unregisterOtaspListener()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    const-string v1, "unregistering otasp listener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->maybeUnregisterOtaspPhoneStateListener()V

    return-void
.end method

.method private updateSignalStrength()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSignalStrength:Landroid/telephony/SignalStrength;

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mServiceState:Landroid/telephony/ServiceState;

    invoke-static {v0, v1, v4}, Lcom/google/android/setupwizard/ActivationActivity;->calculateSignalStrength(Landroid/telephony/SignalStrength;Landroid/telephony/ServiceState;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkBars:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSignalStrengthImages:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkBars:Landroid/widget/ImageView;

    const v2, 0x7f07001b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/setupwizard/ActivationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v2, 0x4e21

    if-ne p1, v2, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    iget v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onFailure()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eq p1, v2, :cond_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x5

    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->doShutdown()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    if-eq p1, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    if-ne p1, v1, :cond_3

    :cond_2
    const-string v1, "ActivationActivity"

    const-string v2, "onClickView: Activiting is not needed, skipping"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onActualSkip()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    if-ne p1, v1, :cond_8

    iget-boolean v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    if-ne v1, v3, :cond_4

    iget-boolean v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    if-nez v1, :cond_5

    :cond_4
    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_6

    :cond_5
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/ActivationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    if-ne v1, v3, :cond_7

    const-string v1, "ActivationActivity"

    const-string v2, "onClickView: exiting after success"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/ActivationActivity;->goNextOrFinish(I)V

    goto :goto_0

    :cond_7
    const-string v1, "ActivationActivity"

    const-string v2, "onClickView: Activiting, next button pressed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->startActivationProcess()V

    goto :goto_0

    :cond_8
    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mContinueSkipButton:Landroid/widget/Button;

    if-ne p1, v1, :cond_9

    const-string v1, "ActivationActivity"

    const-string v2, "onClickView: Skip button"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onActualSkip()V

    goto :goto_0

    :cond_9
    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v1, :cond_a

    const-string v1, "ActivationActivity"

    const-string v2, "onClickView: cancel button"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onCancelRequested()V

    goto :goto_0

    :cond_a
    const-string v1, "ActivationActivity"

    const-string v2, "onClickView: ignorning"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/high16 v9, 0x7f090000

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string v8, "SetupWizard"

    const-string v9, "CDMA always: launching CDMA_PROVISIONING from ActivationActivity"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.android.phone.PERFORM_CDMA_PROVISIONING"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v8, 0x10000000

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/ActivationActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getContentView()Landroid/view/View;

    move-result-object v7

    const/high16 v8, 0x1e40000

    invoke-virtual {v7, v8}, Landroid/view/View;->setSystemUiVisibility(I)V

    sget-boolean v8, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v8, :cond_2

    const-string v8, "ActivationActivity"

    const-string v9, "onCreate: E"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isLTE()Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    const/4 v8, 0x0

    const-string v9, "onCreate()"

    invoke-direct {p0, v8, v9}, Lcom/google/android/setupwizard/ActivationActivity;->setState(ILjava/lang/String;)V

    if-nez p1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "exitToHome"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mDoExitToHome:Z

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "com.android.phone.VOICELESS_PROVISIONING_OFFER_DONTSHOW"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mAllowDontShowMode:Z

    sget-boolean v8, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v8, :cond_3

    const-string v8, "ActivationActivity"

    const-string v9, "onCreate: mDoExitToHome: %b, mAllowDontShowMode: %b"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-boolean v12, p0, Lcom/google/android/setupwizard/ActivationActivity;->mDoExitToHome:Z

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-boolean v12, p0, Lcom/google/android/setupwizard/ActivationActivity;->mAllowDontShowMode:Z

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    iget-boolean v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mAllowDontShowMode:Z

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v9, "dont_show_again"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "SetupWizard"

    const-string v9, "skipping due to don\'t show mode"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x7

    invoke-direct {p0, v8}, Lcom/google/android/setupwizard/ActivationActivity;->goNextOrFinish(I)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/high16 v9, 0x7f050000

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSignalStrengthImages:Landroid/content/res/TypedArray;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const/high16 v8, 0x7f030000

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/ActivationActivity;->setContentView(Landroid/view/View;)V

    const v8, 0x7f0d000e

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    iget-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v8, 0x7f0d000c

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    iget-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSkipButtonForStandAlone:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v8, 0x7f0d000d

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    iget-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v8, 0x7f0d000b

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    iget-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/high16 v8, 0x7f0d0000

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v8, 0x7f0d0007

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v8, 0x7f0d0003

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ProgressBar;

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const v8, 0x7f0d0002

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTopDivider:Landroid/view/View;

    const v8, 0x7f0d0004

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkNotifier:Landroid/view/View;

    const v8, 0x7f0d0005

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkBars:Landroid/widget/ImageView;

    const v8, 0x7f07001a

    invoke-virtual {p0, v8}, Lcom/google/android/setupwizard/ActivationActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    const v8, 0x7f0d000f

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mContinueSkipButton:Landroid/widget/Button;

    iget-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mContinueSkipButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mFilter:Landroid/content/IntentFilter;

    iget-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mFilter:Landroid/content/IntentFilter;

    const-string v9, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v8, Lcom/google/android/setupwizard/ActivationActivity$5;

    invoke-direct {v8, p0}, Lcom/google/android/setupwizard/ActivationActivity$5;-><init>(Lcom/google/android/setupwizard/ActivationActivity;)V

    iput-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    const-string v8, "phone"

    invoke-virtual {p0, v8}, Lcom/google/android/setupwizard/ActivationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    if-eqz v6, :cond_a

    iget-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mSignalStrengthPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v9, 0x100

    invoke-virtual {v6, v8, v9}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    :goto_2
    if-nez p1, :cond_c

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "autoStart"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isFirstRun()Z

    move-result v8

    if-nez v8, :cond_5

    if-nez v0, :cond_5

    iget-boolean v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v8, :cond_b

    :cond_5
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->startActivationProcess()V

    :goto_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->registerOtaspListener()V

    sget-boolean v8, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v8, :cond_0

    const-string v8, "ActivationActivity"

    const-string v9, "onCreate: X"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    const-string v8, "exitToHome"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mDoExitToHome:Z

    const-string v8, "com.android.phone.VOICELESS_PROVISIONING_OFFER_DONTSHOW"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mAllowDontShowMode:Z

    const-string v8, "prevState"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v9, "onCreate() icicle"

    invoke-direct {p0, v8, v9}, Lcom/google/android/setupwizard/ActivationActivity;->setState(ILjava/lang/String;)V

    const-string v8, "isGenMode"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    const-string v8, "cancelEndtime"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelEndtime:J

    const-string v8, "genericEndtime"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericEndtime:J

    const-string v8, "genericActivationNeeded"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    iget-wide v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelEndtime:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_7

    iget-wide v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelEndtime:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long v4, v8, v10

    iget-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTimeOutWaitingForCancel:Ljava/lang/Runnable;

    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-lez v10, :cond_9

    :goto_4
    invoke-virtual {v8, v9, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_7
    iget-wide v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericEndtime:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_3

    iget-wide v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericEndtime:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long v4, v8, v10

    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-gez v8, :cond_8

    const-wide/16 v4, 0x0

    :cond_8
    const-string v8, "SetupWizard"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setting generic timeout in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ms"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTimeOutWaitingForGenericActivation:Ljava/lang/Runnable;

    invoke-virtual {v8, v9, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    :cond_9
    const-wide/16 v4, 0x0

    goto :goto_4

    :cond_a
    const-string v8, "ActivationActivity"

    const-string v9, "telephony manager null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_b
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->initStandaloneView()V

    goto/16 :goto_3

    :cond_c
    const-string v8, "ActivationActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "restoring UI state to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    packed-switch v8, :pswitch_data_0

    const-string v8, "ActivationActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "unknown state: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->initStandaloneView()V

    goto/16 :goto_3

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "autoStart"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isFirstRun()Z

    move-result v8

    if-nez v8, :cond_d

    if-eqz v0, :cond_e

    :cond_d
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->startActivationProcess()V

    goto/16 :goto_3

    :cond_e
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->initStandaloneView()V

    goto/16 :goto_3

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showActivationProcessScreen()V

    goto/16 :goto_3

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showCancelRequestedScreen()V

    goto/16 :goto_3

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showCancelCompletedScreen()V

    goto/16 :goto_3

    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onSuccess()V

    goto/16 :goto_3

    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showFailureScreen()V

    goto/16 :goto_3

    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->showShuttingDownScreen()V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onDestroy()V

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->unregisterOtaspListener()V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTimeOutWaitingForGenericActivation:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTimeOutWaitingForCancel:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v4, 0x4

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    const-string v1, "ActivationActivity"

    const-string v2, "onNewIntent: E"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "otasp_result_code"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_6

    const-string v1, "otasp_result_code"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sget-boolean v1, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "ActivationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got OTASP result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    if-ne v1, v4, :cond_5

    :cond_1
    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const-string v1, "ActivationActivity"

    const-string v2, "OTASP_USER_SKIPPED not returned."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    if-ne v1, v4, :cond_3

    const-string v1, "ActivationActivity"

    const-string v2, "mState is already STATE_CANCEL_COMPLETED"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mTimeOutWaitingForCancel:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onCancelCompleted()V

    :cond_4
    :goto_0
    return-void

    :cond_5
    packed-switch v0, :pswitch_data_0

    const-string v1, "ActivationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown otasp activation result"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    :goto_1
    sget-boolean v1, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_4

    const-string v1, "ActivationActivity"

    const-string v2, "onNewIntent: X"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onActualSkip()V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->isPessimisticMode()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "ActivationActivity"

    const-string v2, "Be pessimistic. Show failure screen anyway."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onFailure()V

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onSuccess()V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onFailure()V

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->onSpcRetriesFailure()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onOtaspChanged()V
    .locals 3

    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onOtaspChanged() mState == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", otasp state == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/ActivationActivity;->getOtaspMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->maybeEndGenericActivation()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->maybeActivateOrExit()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/ActivationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/ActivationActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/ActivationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v0, "ActivationActivity"

    const-string v1, "onSaveInstanceState()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "exitToHome"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mDoExitToHome:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "com.android.phone.VOICELESS_PROVISIONING_OFFER_DONTSHOW"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mAllowDontShowMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "prevState"

    iget v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "isGenMode"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "cancelEndtime"

    iget-wide v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mCancelEndtime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "genericEndtime"

    iget-wide v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericEndtime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "genericActivationNeeded"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected onServiceStateChanged()V
    .locals 3

    sget-boolean v0, Lcom/google/android/setupwizard/ActivationActivity;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceStateChanged() mState == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/ActivationActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/ActivationActivity;->maybeActivateOrExit()V

    return-void
.end method
