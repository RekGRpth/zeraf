.class Lcom/google/android/setupwizard/WelcomeActivity$3;
.super Ljava/lang/Object;
.source "WelcomeActivity.java"

# interfaces
.implements Lcom/google/android/setupwizard/AccessibilityGestureHelper$AccessibilityCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/WelcomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/WelcomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/WelcomeActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/setupwizard/WelcomeActivity$3;->this$0:Lcom/google/android/setupwizard/WelcomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccessibilityStateChanged(Z)V
    .locals 3
    .param p1    # Z

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.speech.tts.engine.CHECK_TTS_DATA"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity$3;->this$0:Lcom/google/android/setupwizard/WelcomeActivity;

    # getter for: Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;
    invoke-static {v2}, Lcom/google/android/setupwizard/WelcomeActivity;->access$600(Lcom/google/android/setupwizard/WelcomeActivity;)Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->getTtsPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/WelcomeActivity$3;->this$0:Lcom/google/android/setupwizard/WelcomeActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/setupwizard/WelcomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
