.class public Lcom/google/android/setupwizard/MultitouchLongPressDetector;
.super Ljava/lang/Object;
.source "MultitouchLongPressDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/MultitouchLongPressDetector$LongPressListener;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mInterceptEvents:Z

.field private mIsCanceled:Z

.field private mListener:Lcom/google/android/setupwizard/MultitouchLongPressDetector$LongPressListener;

.field private final mLongPressDelay:I

.field private final mLongPressRunnable:Ljava/lang/Runnable;

.field private final mMinimumPointerCount:I

.field private mOriginEvent:Landroid/view/MotionEvent;

.field private mPointerDownCount:I

.field private final mTouchSlop:F


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/setupwizard/MultitouchLongPressDetector$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/MultitouchLongPressDetector$1;-><init>(Lcom/google/android/setupwizard/MultitouchLongPressDetector;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mLongPressRunnable:Ljava/lang/Runnable;

    iput p1, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mLongPressDelay:I

    iput p2, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mMinimumPointerCount:I

    int-to-float v0, p3

    iput v0, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mTouchSlop:F

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/MultitouchLongPressDetector;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/MultitouchLongPressDetector;

    invoke-direct {p0}, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->onLongPress()V

    return-void
.end method

.method private static getPointerDistance(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)F
    .locals 4
    .param p0    # Landroid/view/MotionEvent;
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # I

    invoke-virtual {p0, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p0, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/util/MathUtils;->dist(FFFF)F

    move-result v0

    return v0
.end method

.method private onLongPress()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mListener:Lcom/google/android/setupwizard/MultitouchLongPressDetector$LongPressListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    invoke-static {v1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mListener:Lcom/google/android/setupwizard/MultitouchLongPressDetector$LongPressListener;

    invoke-interface {v1, v0}, Lcom/google/android/setupwizard/MultitouchLongPressDetector$LongPressListener;->onLongPress(Landroid/view/MotionEvent;)V

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->setOriginEvent(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private pointersWithinTouchSlop(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1

    :cond_2
    iget v3, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mTouchSlop:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-static {p1, p2, v0}, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->getPointerDistance(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)F

    move-result v3

    iget v4, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mTouchSlop:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private setOriginEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object p1

    :cond_1
    iput-object p1, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    return-void
.end method


# virtual methods
.method public getPointerCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mPointerDownCount:I

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    iput v5, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mPointerDownCount:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v4, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mIsCanceled:Z

    :cond_0
    iget-boolean v5, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mIsCanceled:Z

    if-eqz v5, :cond_1

    :goto_0
    return v4

    :cond_1
    const/4 v2, 0x0

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    :pswitch_0
    if-eqz v1, :cond_3

    iput-boolean v3, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mIsCanceled:Z

    iput-boolean v4, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mInterceptEvents:Z

    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->setOriginEvent(Landroid/view/MotionEvent;)V

    iget-object v5, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_3
    if-eqz v2, :cond_5

    iget-boolean v5, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mInterceptEvents:Z

    if-nez v5, :cond_5

    iput-boolean v3, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mInterceptEvents:Z

    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->setOriginEvent(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    iget v6, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mMinimumPointerCount:I

    if-lt v5, v6, :cond_2

    iget-object v5, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v5, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mLongPressRunnable:Ljava/lang/Runnable;

    iget v7, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mLongPressDelay:I

    int-to-long v7, v7

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v2, 0x1

    goto :goto_1

    :pswitch_2
    iget-object v5, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    invoke-direct {p0, p1, v5}, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->pointersWithinTouchSlop(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v5

    if-nez v5, :cond_4

    move v1, v3

    :goto_2
    goto :goto_1

    :cond_4
    move v1, v4

    goto :goto_2

    :pswitch_3
    const/4 v1, 0x1

    goto :goto_1

    :cond_5
    iget-boolean v4, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mInterceptEvents:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setListener(Lcom/google/android/setupwizard/MultitouchLongPressDetector$LongPressListener;)V
    .locals 0
    .param p1    # Lcom/google/android/setupwizard/MultitouchLongPressDetector$LongPressListener;

    iput-object p1, p0, Lcom/google/android/setupwizard/MultitouchLongPressDetector;->mListener:Lcom/google/android/setupwizard/MultitouchLongPressDetector$LongPressListener;

    return-void
.end method
