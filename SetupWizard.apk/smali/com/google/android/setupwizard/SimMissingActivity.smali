.class public Lcom/google/android/setupwizard/SimMissingActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "SimMissingActivity.java"


# instance fields
.field private mSkipButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method

.method private initView()V
    .locals 2

    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SimMissingActivity;->setContentView(I)V

    const v0, 0x7f0d000e

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SimMissingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/SimMissingActivity;->mSkipButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/setupwizard/SimMissingActivity;->mSkipButton:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/SimMissingActivity;->setDefaultButton(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/SimMissingActivity;->initView()V

    return-void
.end method

.method public start()V
    .locals 1

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SimMissingActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SimMissingActivity;->finish()V

    return-void
.end method
