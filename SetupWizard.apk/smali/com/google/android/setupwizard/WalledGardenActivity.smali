.class public Lcom/google/android/setupwizard/WalledGardenActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "WalledGardenActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;,
        Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;,
        Lcom/google/android/setupwizard/WalledGardenActivity$MyWebViewClient;,
        Lcom/google/android/setupwizard/WalledGardenActivity$MyChromeClient;
    }
.end annotation


# instance fields
.field private mFirstCheck:Z

.field private mGservicesLatch:Ljava/util/concurrent/CountDownLatch;

.field private mGservicesReceiver:Landroid/content/BroadcastReceiver;

.field private mUrlConnection:Ljava/net/HttpURLConnection;

.field private mWalledGardenUrl:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/util/concurrent/CountDownLatch;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/WalledGardenActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mGservicesLatch:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/setupwizard/WalledGardenActivity;)Landroid/webkit/WebView;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/WalledGardenActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/setupwizard/WalledGardenActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/WalledGardenActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/WalledGardenActivity;->onPageLoadFinished(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/setupwizard/WalledGardenActivity;)Ljava/net/HttpURLConnection;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/WalledGardenActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/setupwizard/WalledGardenActivity;Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/WalledGardenActivity;
    .param p1    # Ljava/net/HttpURLConnection;

    iput-object p1, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mUrlConnection:Ljava/net/HttpURLConnection;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/setupwizard/WalledGardenActivity;Z)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/WalledGardenActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/WalledGardenActivity;->onWalledGarden(Z)V

    return-void
.end method

.method private connected()V
    .locals 2

    sget-boolean v0, Lcom/google/android/setupwizard/WalledGardenActivity;->WAIT_FOR_GSERVICES:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;-><init>(Lcom/google/android/setupwizard/WalledGardenActivity;Lcom/google/android/setupwizard/WalledGardenActivity$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/WalledGardenActivity$GservicesWaiterTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WalledGardenActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WalledGardenActivity;->finish()V

    goto :goto_0
.end method

.method private static getSettingsGlobalStr(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method private initWaitScreen()Landroid/view/View;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WalledGardenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private initWebScreen()Landroid/view/View;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WalledGardenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03000c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d0029

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Lcom/google/android/setupwizard/WalledGardenActivity$2;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/WalledGardenActivity$2;-><init>(Lcom/google/android/setupwizard/WalledGardenActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/WalledGardenActivity;->setupOptionsAndCallbacks()V

    return-object v1
.end method

.method private onPageLoadFinished(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;-><init>(Lcom/google/android/setupwizard/WalledGardenActivity;Lcom/google/android/setupwizard/WalledGardenActivity$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWalledGardenUrl:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private onWalledGarden(Z)V
    .locals 3
    .param p1    # Z

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mFirstCheck:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mFirstCheck:Z

    invoke-direct {p0}, Lcom/google/android/setupwizard/WalledGardenActivity;->initWebScreen()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WalledGardenActivity;->getContentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WalledGardenActivity;->setContentView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWalledGardenUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/WalledGardenActivity;->connected()V

    goto :goto_0
.end method

.method private setupOptionsAndCallbacks()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/google/android/setupwizard/WalledGardenActivity$MyWebViewClient;

    invoke-direct {v2, p0, v5}, Lcom/google/android/setupwizard/WalledGardenActivity$MyWebViewClient;-><init>(Lcom/google/android/setupwizard/WalledGardenActivity;Lcom/google/android/setupwizard/WalledGardenActivity$1;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/google/android/setupwizard/WalledGardenActivity$MyChromeClient;

    invoke-direct {v2, p0, v5}, Lcom/google/android/setupwizard/WalledGardenActivity$MyChromeClient;-><init>(Lcom/google/android/setupwizard/WalledGardenActivity;Lcom/google/android/setupwizard/WalledGardenActivity$1;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setLightTouchEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setNeedInitialFocus(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    iget-object v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setMapTrackballToArrowKeys(Z)V

    iget-object v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setFocusableInTouchMode(Z)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x1

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/WalledGardenActivity;->initWaitScreen()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/WalledGardenActivity;->setContentView(Landroid/view/View;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "http://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WalledGardenActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "captive_portal_server"

    const-string v5, "clients3.google.com"

    invoke-static {v3, v4, v5}, Lcom/google/android/setupwizard/WalledGardenActivity;->getSettingsGlobalStr(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/generate_204"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWalledGardenUrl:Ljava/lang/String;

    iput-boolean v6, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mFirstCheck:Z

    sget-boolean v2, Lcom/google/android/setupwizard/WalledGardenActivity;->WAIT_FOR_GSERVICES:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v2, v6}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mGservicesLatch:Ljava/util/concurrent/CountDownLatch;

    new-instance v2, Lcom/google/android/setupwizard/WalledGardenActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/WalledGardenActivity$1;-><init>(Lcom/google/android/setupwizard/WalledGardenActivity;)V

    iput-object v2, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mGservicesReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mGservicesReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/setupwizard/WalledGardenActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    new-instance v2, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;-><init>(Lcom/google/android/setupwizard/WalledGardenActivity;Lcom/google/android/setupwizard/WalledGardenActivity$1;)V

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mWalledGardenUrl:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/setupwizard/WalledGardenActivity$CheckConnectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mGservicesReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mGservicesReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WalledGardenActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/WalledGardenActivity;->mGservicesReceiver:Landroid/content/BroadcastReceiver;

    :cond_0
    return-void
.end method
