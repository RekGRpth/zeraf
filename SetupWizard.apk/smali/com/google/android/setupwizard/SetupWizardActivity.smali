.class public Lcom/google/android/setupwizard/SetupWizardActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "SetupWizardActivity.java"


# instance fields
.field private mClearAccountsDone:Z

.field protected mOverrideExit:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/SetupWizardActivity;->mClearAccountsDone:Z

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v5, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SetupWizardActivity.onCreate("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez p1, :cond_1

    const-string v2, "null"

    :goto_0
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ") LTE == "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->isLTE()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "true"

    :goto_1
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " userID="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "firstRun"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "firstRun"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "device_provisioned"

    invoke-static {v2, v5, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_3

    move v1, v3

    :goto_2
    const-string v2, "SetupWizard"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DEVICE_PROVISIONED="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/google/android/setupwizard/SetupWizardActivity;->mOverrideExit:Z

    if-nez v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->provisioningDisabled()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0, v4, v4}, Lcom/google/android/setupwizard/SetupWizardActivity;->onSetupComplete(ZZ)V

    :goto_3
    return-void

    :cond_1
    const-string v2, "icicle"

    goto :goto_0

    :cond_2
    const-string v2, "false"

    goto :goto_1

    :cond_3
    move v1, v4

    goto :goto_2

    :cond_4
    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->isHomeActivity()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    iput-boolean v3, p0, Lcom/google/android/setupwizard/SetupWizardActivity;->mClearAccountsDone:Z

    :cond_6
    if-nez p1, :cond_7

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->onSetupStart()V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->startPrepayDetection()V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->startWelcomeActivity()V

    :cond_7
    const-string v2, "SetupWizard"

    const-string v3, "SetupWizardActivity.onCreate() completed"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public onGlsConnected(Lcom/google/android/gsf/IGoogleLoginService;)V
    .locals 3
    .param p1    # Lcom/google/android/gsf/IGoogleLoginService;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onGlsConnected(Lcom/google/android/gsf/IGoogleLoginService;)V

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/setupwizard/SetupWizardActivity;->mClearAccountsDone:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->provisioningDisabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gsf/IGoogleLoginService;->deleteAllAccounts()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/setupwizard/SetupWizardActivity;->mClearAccountsDone:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SetupWizard"

    const-string v2, "GLS died.  There might be spurious accounts."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onNetworkConnected()V
    .locals 3

    sget-boolean v1, Lcom/google/android/setupwizard/SetupWizardActivity;->WAIT_FOR_GSERVICES:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.server.checkin.CHECKIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "force"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SetupWizardActivity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/SetupWizardActivity;->setIntent(Landroid/content/Intent;)V

    const-string v1, "completed"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "completed"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sget-boolean v1, Lcom/google/android/setupwizard/SetupWizardActivity;->LOCAL_LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "SetupWizard"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SetupWizard completion detected, completed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz v0, :cond_2

    const/4 v1, -0x1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/SetupWizardActivity;->setResult(I)V

    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->finish()V

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x7

    goto :goto_0
.end method
