.class Lcom/google/android/setupwizard/AccessibilityGestureHelper$2;
.super Ljava/lang/Object;
.source "AccessibilityGestureHelper.java"

# interfaces
.implements Lcom/google/android/setupwizard/MultitouchLongPressDetector$LongPressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/AccessibilityGestureHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/AccessibilityGestureHelper;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/AccessibilityGestureHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/setupwizard/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    # getter for: Lcom/google/android/setupwizard/AccessibilityGestureHelper;->mAccessibilityEnabled:Z
    invoke-static {v0}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->access$100(Lcom/google/android/setupwizard/AccessibilityGestureHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    # getter for: Lcom/google/android/setupwizard/AccessibilityGestureHelper;->mStartedWarning:Z
    invoke-static {v0}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->access$200(Lcom/google/android/setupwizard/AccessibilityGestureHelper;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/setupwizard/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/setupwizard/AccessibilityGestureHelper;->mStartedWarning:Z
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->access$202(Lcom/google/android/setupwizard/AccessibilityGestureHelper;Z)Z

    iget-object v0, p0, Lcom/google/android/setupwizard/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    # invokes: Lcom/google/android/setupwizard/AccessibilityGestureHelper;->speakWarning()V
    invoke-static {v0}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->access$300(Lcom/google/android/setupwizard/AccessibilityGestureHelper;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    # getter for: Lcom/google/android/setupwizard/AccessibilityGestureHelper;->mFinishedWarning:Z
    invoke-static {v0}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->access$400(Lcom/google/android/setupwizard/AccessibilityGestureHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    # invokes: Lcom/google/android/setupwizard/AccessibilityGestureHelper;->enableAllAccessibilityServices()V
    invoke-static {v0}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->access$500(Lcom/google/android/setupwizard/AccessibilityGestureHelper;)V

    goto :goto_0
.end method
