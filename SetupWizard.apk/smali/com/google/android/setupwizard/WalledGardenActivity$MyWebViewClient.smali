.class Lcom/google/android/setupwizard/WalledGardenActivity$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "WalledGardenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/WalledGardenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/WalledGardenActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/setupwizard/WalledGardenActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/setupwizard/WalledGardenActivity$MyWebViewClient;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/setupwizard/WalledGardenActivity;Lcom/google/android/setupwizard/WalledGardenActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/setupwizard/WalledGardenActivity;
    .param p2    # Lcom/google/android/setupwizard/WalledGardenActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/WalledGardenActivity$MyWebViewClient;-><init>(Lcom/google/android/setupwizard/WalledGardenActivity;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/setupwizard/WalledGardenActivity$MyWebViewClient;->this$0:Lcom/google/android/setupwizard/WalledGardenActivity;

    # invokes: Lcom/google/android/setupwizard/WalledGardenActivity;->onPageLoadFinished(Ljava/lang/String;)V
    invoke-static {v0, p2}, Lcom/google/android/setupwizard/WalledGardenActivity;->access$600(Lcom/google/android/setupwizard/WalledGardenActivity;Ljava/lang/String;)V

    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/webkit/WebView;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const-string v0, "SetupWizard"

    const-string v1, "onReceivedError: errorCode %d, description: %s, url: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onTooManyRedirects(Landroid/webkit/WebView;Landroid/os/Message;Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/os/Message;
    .param p3    # Landroid/os/Message;

    const-string v0, "SetupWizard"

    const-string v1, "onTooManyRedirects"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method
