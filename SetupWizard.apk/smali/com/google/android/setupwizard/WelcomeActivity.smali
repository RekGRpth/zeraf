.class public Lcom/google/android/setupwizard/WelcomeActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "WelcomeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final WAITING_MODE_PERIOD:I

.field private final WAITING_MODE_TIMEOUT_NANOS:J

.field private final mAccessibilityCallback:Lcom/google/android/setupwizard/AccessibilityGestureHelper$AccessibilityCallback;

.field private mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

.field private mAdapterIndices:[I

.field private mClearAccountsDone:Z

.field private mCurrentLocale:Ljava/util/Locale;

.field protected mEmergencyCallButton:Landroid/widget/Button;

.field private final mEnableWifi:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private mInitialLocale:Ljava/util/Locale;

.field protected mLanguagePicker:Landroid/widget/NumberPicker;

.field private mLocaleAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/internal/app/LocalePicker$LocaleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mStartButton:Landroid/widget/Button;

.field private mTopDivider:Landroid/view/View;

.field private mTtsLocales:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mUpdateLocale:Ljava/lang/Runnable;

.field private mWaitMsg:Landroid/widget/TextView;

.field private mWaitStartTime:J

.field private mWaiting:Z

.field private final mWaitingModePoll:Ljava/lang/Runnable;

.field private mWelcomeTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mClearAccountsDone:Z

    const-wide v0, 0xdf8475800L

    iput-wide v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->WAITING_MODE_TIMEOUT_NANOS:J

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->WAITING_MODE_PERIOD:I

    new-instance v0, Lcom/google/android/setupwizard/WelcomeActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/WelcomeActivity$1;-><init>(Lcom/google/android/setupwizard/WelcomeActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaitingModePoll:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/setupwizard/WelcomeActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/WelcomeActivity$2;-><init>(Lcom/google/android/setupwizard/WelcomeActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mEnableWifi:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/setupwizard/WelcomeActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/WelcomeActivity$3;-><init>(Lcom/google/android/setupwizard/WelcomeActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityCallback:Lcom/google/android/setupwizard/AccessibilityGestureHelper$AccessibilityCallback;

    new-instance v0, Lcom/google/android/setupwizard/WelcomeActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/WelcomeActivity$4;-><init>(Lcom/google/android/setupwizard/WelcomeActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mUpdateLocale:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/WelcomeActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/WelcomeActivity;

    iget-boolean v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaiting:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/WelcomeActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/WelcomeActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->waitIsRequired()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/WelcomeActivity;)J
    .locals 2
    .param p0    # Lcom/google/android/setupwizard/WelcomeActivity;

    iget-wide v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaitStartTime:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/setupwizard/WelcomeActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/WelcomeActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->endWaitingMode()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/setupwizard/WelcomeActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/WelcomeActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaitingModePoll:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/setupwizard/WelcomeActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/WelcomeActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/setupwizard/WelcomeActivity;)Lcom/google/android/setupwizard/AccessibilityGestureHelper;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/WelcomeActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/setupwizard/WelcomeActivity;)Ljava/util/Locale;
    .locals 1
    .param p0    # Lcom/google/android/setupwizard/WelcomeActivity;

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mCurrentLocale:Ljava/util/Locale;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/setupwizard/WelcomeActivity;Ljava/util/Locale;)Ljava/util/Locale;
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/WelcomeActivity;
    .param p1    # Ljava/util/Locale;

    iput-object p1, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mInitialLocale:Ljava/util/Locale;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/setupwizard/WelcomeActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/setupwizard/WelcomeActivity;

    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->setLocaleFromPicker()V

    return-void
.end method

.method private endWaitingMode()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaiting:Z

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaitingModePoll:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->finishAndReturnResult()V

    return-void
.end method

.method private finishAndReturnResult()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->getOtaspMode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WelcomeActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->finish()V

    return-void
.end method

.method private initViews()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WelcomeActivity;->setContentView(Landroid/view/View;)V

    const v2, 0x7f0d002e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mStartButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0d0003

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const v2, 0x7f0d0002

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mTopDivider:Landroid/view/View;

    const v2, 0x7f0d002c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaitMsg:Landroid/widget/TextView;

    const v2, 0x7f0d002b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWelcomeTitle:Landroid/widget/TextView;

    const v2, 0x7f0d002d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/NumberPicker;

    iput-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->isSecondaryUser()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->loadLanguages()V

    :goto_0
    const v2, 0x7f0d002f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x111002f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/NumberPicker;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method private isTtsLocaleSupported(Ljava/util/Locale;)Z
    .locals 6
    .param p1    # Ljava/util/Locale;

    const/16 v5, 0x2d

    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mTtsLocales:Ljava/util/HashSet;

    if-nez v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    return v4

    :cond_0
    invoke-virtual {p1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mTtsLocales:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_0

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    move-object v2, v1

    goto :goto_1
.end method

.method private loadLanguages()V
    .locals 12

    const/4 v8, 0x0

    const v9, 0x7f030002

    const v10, 0x7f0d0011

    invoke-static {p0, v9, v10}, Lcom/android/internal/app/LocalePicker;->constructAdapter(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mInitialLocale:Ljava/util/Locale;

    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mInitialLocale:Ljava/util/Locale;

    iput-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mCurrentLocale:Ljava/util/Locale;

    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    invoke-virtual {v9}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->isAccessibilityEnabled()Z

    move-result v3

    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v9}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v9

    new-array v9, v9, [I

    iput-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAdapterIndices:[I

    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v9}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v9

    new-array v6, v9, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v9}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v9

    if-ge v0, v9, :cond_3

    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v9, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    invoke-virtual {v2}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLocale()Ljava/util/Locale;

    move-result-object v5

    if-nez v1, :cond_0

    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mInitialLocale:Ljava/util/Locale;

    invoke-virtual {v5, v9}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v4, 0x1

    :goto_1
    if-nez v4, :cond_1

    if-eqz v3, :cond_1

    invoke-direct {p0, v5}, Lcom/google/android/setupwizard/WelcomeActivity;->isTtsLocaleSupported(Ljava/util/Locale;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "SetupWizard"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Skipping locale (not supported by TTS): "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v4, v8

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLabel()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v7

    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAdapterIndices:[I

    aput v0, v9, v7

    if-eqz v4, :cond_2

    const-string v9, "SetupWizard"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "initializing locale to "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " == "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mInitialLocale:Ljava/util/Locale;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " of "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v11}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v7

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_3
    array-length v9, v6

    if-le v9, v7, :cond_4

    invoke-static {v6, v7}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAdapterIndices:[I

    invoke-static {v9, v7}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAdapterIndices:[I

    :cond_4
    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    invoke-virtual {v9, v8}, Landroid/widget/NumberPicker;->setValue(I)V

    iget-object v9, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    invoke-virtual {v9, v8}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    iget-object v8, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    invoke-virtual {v8, v6}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    iget-object v8, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    array-length v9, v6

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v9}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    iget-object v8, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    invoke-virtual {v8, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    iget-object v8, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    const/high16 v9, 0x60000

    invoke-virtual {v8, v9}, Landroid/widget/NumberPicker;->setDescendantFocusability(I)V

    iget-object v8, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    new-instance v9, Lcom/google/android/setupwizard/WelcomeActivity$5;

    invoke-direct {v9, p0}, Lcom/google/android/setupwizard/WelcomeActivity$5;-><init>(Lcom/google/android/setupwizard/WelcomeActivity;)V

    invoke-virtual {v8, v9}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    if-eqz v3, :cond_5

    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->setLocaleFromPicker()V

    :cond_5
    return-void
.end method

.method private onLocaleChanged(Ljava/util/Locale;Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/util/Locale;
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V

    iput-object p1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3, v1, v8}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWelcomeTitle:Landroid/widget/TextView;

    const v5, 0x7f070004

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mStartButton:Landroid/widget/Button;

    const v5, 0x7f070015

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    :cond_0
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "lockscreen_emergency_call"

    const-string v6, "string"

    const-string v7, "android"

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setText(I)V

    :cond_1
    invoke-virtual {v3, v0, v8}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mUpdateLocale:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object p1, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mCurrentLocale:Ljava/util/Locale;

    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    invoke-virtual {v4}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->isAccessibilityEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mUpdateLocale:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    return-void
.end method

.method private resumeWaitingMode()V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaitMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mTopDivider:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mTopDivider:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaiting:Z

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaitingModePoll:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private setLocaleFromPicker()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAdapterIndices:[I

    iget-object v3, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    invoke-virtual {v3}, Landroid/widget/NumberPicker;->getValue()I

    move-result v3

    aget v0, v2, v3

    iget-object v2, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    invoke-virtual {v1}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/setupwizard/WelcomeActivity;->onLocaleChanged(Ljava/util/Locale;Ljava/lang/String;)V

    return-void
.end method

.method private startWaitingMode()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaitStartTime:J

    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->resumeWaitingMode()V

    return-void
.end method

.method private waitIsRequired()Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/WelcomeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v3, "phone"

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/WelcomeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->isWifiOnlyBuild()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->simMissing()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->lteUnknown()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->simStateUnknown()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->otaspStateIsKnown()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->mnc:I

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->mcc:I

    if-nez v3, :cond_1

    :cond_3
    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v3, "availableVoices"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mTtsLocales:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mTtsLocales:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->loadLanguages()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mStartButton:Landroid/widget/Button;

    if-ne p1, v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->isSecondaryUser()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAdapterIndices:[I

    iget-object v4, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLanguagePicker:Landroid/widget/NumberPicker;

    invoke-virtual {v4}, Landroid/widget/NumberPicker;->getValue()I

    move-result v4

    aget v0, v3, v4

    iget-object v3, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    invoke-virtual {v2}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/app/LocalePicker;->updateLocale(Ljava/util/Locale;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->waitIsRequired()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->startWaitingMode()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->finishAndReturnResult()V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    if-ne p1, v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->placeEmergencyCall()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->isAccessibilityEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityCallback:Lcom/google/android/setupwizard/AccessibilityGestureHelper$AccessibilityCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/setupwizard/AccessibilityGestureHelper$AccessibilityCallback;->onAccessibilityStateChanged(Z)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->initViews()V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->maybeRegisterOtaspPhoneStateListener()V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->isWifiOnlyBuild()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mEnableWifi:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    iget-object v1, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityCallback:Lcom/google/android/setupwizard/AccessibilityGestureHelper$AccessibilityCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->setCallback(Lcom/google/android/setupwizard/AccessibilityGestureHelper$AccessibilityCallback;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "SetupWizard"

    const-string v1, "SetupWizardActivity.onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->maybeUnregisterOtaspPhoneStateListener()V

    iget-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/AccessibilityGestureHelper;->onDestroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/AccessibilityGestureHelper;

    return-void
.end method

.method public onGlsConnected(Lcom/google/android/gsf/IGoogleLoginService;)V
    .locals 3
    .param p1    # Lcom/google/android/gsf/IGoogleLoginService;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onGlsConnected(Lcom/google/android/gsf/IGoogleLoginService;)V

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mClearAccountsDone:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->provisioningDisabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gsf/IGoogleLoginService;->deleteAllAccounts()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mClearAccountsDone:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SetupWizard"

    const-string v2, "GLS died.  There might be spurious accounts."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "WaitingMode"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaiting:Z

    const-string v0, "WaitingModeStartTime"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaitStartTime:J

    iget-boolean v0, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaiting:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/WelcomeActivity;->resumeWaitingMode()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "WaitingMode"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaiting:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "WaitingModeStartTime"

    iget-wide v1, p0, Lcom/google/android/setupwizard/WelcomeActivity;->mWaitStartTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method
