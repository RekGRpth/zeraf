.class public Lcom/google/android/setupwizard/LoginAccountsMissingReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LoginAccountsMissingReceiver.java"


# static fields
.field private static SETUP_WIZARD_ACTIVITY:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/google/android/setupwizard/SetupWizardActivity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final LDEBUG:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/setupwizard/SetupWizardActivity;

    sput-object v0, Lcom/google/android/setupwizard/LoginAccountsMissingReceiver;->SETUP_WIZARD_ACTIVITY:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/LoginAccountsMissingReceiver;->LDEBUG:Z

    return-void
.end method

.method private doNotification(Landroid/content/Context;)V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/SetupWizardTestActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/app/Notification;

    const v2, 0x108008a

    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const/16 v2, 0x30

    iput v2, v1, Landroid/app/Notification;->flags:I

    const v2, 0x7f070039

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const v3, 0x7f07003a

    invoke-virtual {p1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    const/high16 v4, 0x10000000

    invoke-static {p1, v6, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, p1, v2, v3, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v6, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    new-instance v3, Landroid/content/ComponentName;

    sget-object v7, Lcom/google/android/setupwizard/LoginAccountsMissingReceiver;->SETUP_WIZARD_ACTIVITY:Ljava/lang/Class;

    invoke-direct {v3, p1, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "ro.setupwizard.mode"

    sget-object v8, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->REQUIRED:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    invoke-virtual {v8}, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->name()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "device_provisioned"

    invoke-static {v7, v8}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    move v1, v5

    :goto_0
    invoke-virtual {v4, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v7

    if-eq v7, v5, :cond_2

    move v0, v5

    :goto_1
    if-eqz v2, :cond_3

    sget-object v5, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->REQUIRED:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "SetupWizard"

    const-string v6, "Ignoring LOGIN_ACCOUNTS_MISSING because SetupWizard isn\'t required"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v1, v6

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_1

    :cond_3
    if-eqz v1, :cond_4

    const-string v5, "SetupWizard"

    const-string v6, "Ignoring LOGIN_ACCOUNTS_MISSING because this is first boot."

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    if-eqz v0, :cond_5

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "device_provisioned"

    invoke-static {v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "SetupWizard"

    const-string v6, "Sending notification to re-run SetupWizard"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/LoginAccountsMissingReceiver;->doNotification(Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v5

    goto :goto_2

    :cond_5
    const-string v5, "SetupWizard"

    const-string v6, "Not re-enabling SetupWizard because component already enabled"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
