.class public Lcom/android/music/CreatePlaylist;
.super Landroid/app/Activity;
.source "CreatePlaylist.java"


# static fields
.field private static final ALERT_DIALOG_KEY:I = 0x0

.field private static final TAG:Ljava/lang/String; = "CreatePlaylist"


# instance fields
.field private mButtonClicked:Landroid/content/DialogInterface$OnClickListener;

.field private mDialog:Lcom/android/music/MusicDialog;

.field private mPlaylist:Landroid/widget/EditText;

.field private mPrompt:Ljava/lang/String;

.field private mSaveButton:Landroid/widget/Button;

.field private mScanListener:Landroid/content/BroadcastReceiver;

.field private mSelectItemId:Ljava/lang/String;

.field private mStartActivityTab:I

.field mTextWatcher:Landroid/text/TextWatcher;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/music/CreatePlaylist;->mSelectItemId:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/music/CreatePlaylist;->mStartActivityTab:I

    new-instance v0, Lcom/android/music/CreatePlaylist$1;

    invoke-direct {v0, p0}, Lcom/android/music/CreatePlaylist$1;-><init>(Lcom/android/music/CreatePlaylist;)V

    iput-object v0, p0, Lcom/android/music/CreatePlaylist;->mTextWatcher:Landroid/text/TextWatcher;

    new-instance v0, Lcom/android/music/CreatePlaylist$2;

    invoke-direct {v0, p0}, Lcom/android/music/CreatePlaylist$2;-><init>(Lcom/android/music/CreatePlaylist;)V

    iput-object v0, p0, Lcom/android/music/CreatePlaylist;->mButtonClicked:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/android/music/CreatePlaylist$3;

    invoke-direct {v0, p0}, Lcom/android/music/CreatePlaylist$3;-><init>(Lcom/android/music/CreatePlaylist;)V

    iput-object v0, p0, Lcom/android/music/CreatePlaylist;->mScanListener:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/music/CreatePlaylist;)V
    .locals 0
    .param p0    # Lcom/android/music/CreatePlaylist;

    invoke-direct {p0}, Lcom/android/music/CreatePlaylist;->setSaveButton()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/music/CreatePlaylist;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/android/music/CreatePlaylist;

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/music/CreatePlaylist;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/music/CreatePlaylist;

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mSelectItemId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/music/CreatePlaylist;)I
    .locals 1
    .param p0    # Lcom/android/music/CreatePlaylist;

    iget v0, p0, Lcom/android/music/CreatePlaylist;->mStartActivityTab:I

    return v0
.end method

.method private setSaveButton()V
    .locals 4

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    if-nez v1, :cond_0

    const-string v1, "CreatePlaylist"

    const-string v2, "setSaveButton with dialog is null return!"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    invoke-virtual {v1}, Lcom/android/music/MusicDialog;->getPositiveButton()Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    :cond_1
    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_2
    :goto_1
    const-string v1, "CreatePlaylist"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSaveButton "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/music/MusicUtils;->idForplaylist(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_4

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    const v2, 0x7f070053

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    const v2, 0x7f070052

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setVolumeControlStream(I)V

    invoke-virtual {p0, v8}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030005

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/android/music/CreatePlaylist;->mView:Landroid/view/View;

    iget-object v4, p0, Lcom/android/music/CreatePlaylist;->mView:Landroid/view/View;

    const v5, 0x7f0c0022

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    if-eqz p1, :cond_1

    const-string v4, "defaultname"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f070050

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicUtils;->makePlaylistName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v4, 0x7f070072

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/music/CreatePlaylist;->mPrompt:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v4, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/android/music/CreatePlaylist;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "file"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/music/CreatePlaylist;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    if-nez p1, :cond_3

    invoke-virtual {p0, v7}, Landroid/app/Activity;->showDialog(I)V

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v4, "add_to_playlist_item_id"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/music/CreatePlaylist;->mSelectItemId:Ljava/lang/String;

    const-string v4, "start_activity_tab_id"

    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/music/CreatePlaylist;->mStartActivityTab:I

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    if-nez p1, :cond_0

    new-instance v0, Lcom/android/music/MusicDialog;

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mButtonClicked:Landroid/content/DialogInterface$OnClickListener;

    iget-object v2, p0, Lcom/android/music/CreatePlaylist;->mView:Landroid/view/View;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/music/MusicDialog;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mPrompt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/music/MusicDialog;->setPositiveButton(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/music/MusicDialog;->setNeutralButton(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    invoke-virtual {v0, v3}, Lcom/android/music/MusicDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    invoke-virtual {v0}, Lcom/android/music/MusicDialog;->setSearchKeyListener()V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/android/music/CreatePlaylist;->setSaveButton()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "defaultname"

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
