.class Lcom/android/music/MusicBrowserActivity$4;
.super Landroid/content/BroadcastReceiver;
.source "MusicBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/MusicBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/music/MusicBrowserActivity;


# direct methods
.method constructor <init>(Lcom/android/music/MusicBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const v9, 0x7f0c003e

    const v8, 0x7f0c002b

    const v7, 0x7f0c002a

    const/16 v6, 0x8

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    const-string v4, "onoff"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v3, v4}, Lcom/android/music/MusicBrowserActivity;->access$802(Lcom/android/music/MusicBrowserActivity;Z)Z

    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v3}, Lcom/android/music/MusicBrowserActivity;->access$800(Lcom/android/music/MusicBrowserActivity;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "MusicBrowser"

    const-string v4, "Sdcard normal"

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-virtual {v3, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    const v4, 0x7f0c003f

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-virtual {v3, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-virtual {v3, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v3}, Lcom/android/music/MusicBrowserActivity;->access$500(Lcom/android/music/MusicBrowserActivity;)Lcom/android/music/IMediaPlaybackService;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    iget-object v4, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v4}, Lcom/android/music/MusicBrowserActivity;->access$600(Lcom/android/music/MusicBrowserActivity;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/android/music/MusicUtils;->updateNowPlaying(Landroid/app/Activity;I)V

    :cond_4
    :goto_0
    return-void

    :cond_5
    const-string v3, "MusicBrowser"

    const-string v4, "Sdcard error"

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-virtual {v3, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-virtual {v3, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    const v4, 0x7f0c003f

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_8

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    const-string v3, "message"

    const v4, 0x7f070041

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :cond_8
    iget-object v3, p0, Lcom/android/music/MusicBrowserActivity$4;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-virtual {v3, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
