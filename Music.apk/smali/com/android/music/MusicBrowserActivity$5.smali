.class Lcom/android/music/MusicBrowserActivity$5;
.super Ljava/lang/Object;
.source "MusicBrowserActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/music/MusicBrowserActivity;->initSearchButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/music/MusicBrowserActivity;

.field final synthetic val$blankView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/music/MusicBrowserActivity;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/music/MusicBrowserActivity$5;->this$0:Lcom/android/music/MusicBrowserActivity;

    iput-object p2, p0, Lcom/android/music/MusicBrowserActivity$5;->val$blankView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity$5;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->onSearchRequested()Z

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity$5;->this$0:Lcom/android/music/MusicBrowserActivity;

    iget-object v0, v0, Lcom/android/music/MusicBrowserActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity$5;->val$blankView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity$5;->val$blankView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method
