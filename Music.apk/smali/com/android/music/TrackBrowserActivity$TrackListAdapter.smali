.class Lcom/android/music/TrackBrowserActivity$TrackListAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "TrackBrowserActivity.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/TrackBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TrackListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;,
        Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private mActivity:Lcom/android/music/TrackBrowserActivity;

.field mArtistIdx:I

.field mAudioIdIdx:I

.field private final mBuilder:Ljava/lang/StringBuilder;

.field private mConstraint:Ljava/lang/String;

.field private mConstraintIsValid:Z

.field mDisableNowPlayingIndicator:Z

.field mDrmMethodIdx:I

.field mDurationIdx:I

.field private mIndexer:Landroid/widget/AlphabetIndexer;

.field mIsDrmIdx:I

.field mIsNowPlaying:Z

.field private mQueryHandler:Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;

.field mTitleIdx:I

.field mTitlePinyinIdx:I

.field private mUnknownAlbum:Ljava/lang/String;

.field private mUnknownArtist:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/music/TrackBrowserActivity;ILandroid/database/Cursor;[Ljava/lang/String;[IZZ)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/music/TrackBrowserActivity;
    .param p3    # I
    .param p4    # Landroid/database/Cursor;
    .param p5    # [Ljava/lang/String;
    .param p6    # [I
    .param p7    # Z
    .param p8    # Z

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mBuilder:Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mConstraint:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mConstraintIsValid:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIsDrmIdx:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mDrmMethodIdx:I

    iput-object p2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    invoke-direct {p0, p4}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->getColumnIndices(Landroid/database/Cursor;)V

    iput-boolean p7, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIsNowPlaying:Z

    iput-boolean p8, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mDisableNowPlayingIndicator:Z

    const v0, 0x7f070042

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mUnknownArtist:Ljava/lang/String;

    const v0, 0x7f070043

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mUnknownAlbum:Ljava/lang/String;

    new-instance v0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;-><init>(Lcom/android/music/TrackBrowserActivity$TrackListAdapter;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mQueryHandler:Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;

    return-void
.end method

.method static synthetic access$1500(Lcom/android/music/TrackBrowserActivity$TrackListAdapter;)Lcom/android/music/TrackBrowserActivity;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    return-object v0
.end method

.method private getColumnIndices(Landroid/database/Cursor;)V
    .locals 4
    .param p1    # Landroid/database/Cursor;

    if-eqz p1, :cond_0

    const-string v2, "title"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mTitleIdx:I

    const-string v2, "artist"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mArtistIdx:I

    const-string v2, "duration"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mDurationIdx:I

    :try_start_0
    const-string v2, "audio_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mAudioIdIdx:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v2, "title_pinyin_key"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mTitlePinyinIdx:I

    const-string v2, "is_drm"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIsDrmIdx:I

    const-string v2, "drm_method"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mDrmMethodIdx:I

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIndexer:Landroid/widget/AlphabetIndexer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIndexer:Landroid/widget/AlphabetIndexer;

    invoke-virtual {v2, p1}, Landroid/widget/AlphabetIndexer;->setCursor(Landroid/database/Cursor;)V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v1

    const-string v2, "_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mAudioIdIdx:I

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v2}, Lcom/android/music/TrackBrowserActivity;->access$1600(Lcom/android/music/TrackBrowserActivity;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v2}, Lcom/android/music/TrackBrowserActivity;->access$1700(Lcom/android/music/TrackBrowserActivity;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    const v3, 0x7f070070

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/android/music/MusicAlphabetIndexer;

    iget v3, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mTitlePinyinIdx:I

    invoke-direct {v2, p1, v3, v0}, Lcom/android/music/MusicAlphabetIndexer;-><init>(Landroid/database/Cursor;ILjava/lang/String;)V

    iput-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIndexer:Landroid/widget/AlphabetIndexer;

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v2}, Lcom/android/music/TrackBrowserActivity;->access$800(Lcom/android/music/TrackBrowserActivity;)Landroid/widget/ListView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    goto :goto_1
.end method

.method private updateDrmLockIcon(Landroid/widget/ImageView;Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Landroid/database/Cursor;

    const/4 v5, 0x1

    const/16 v0, 0x8

    iget v4, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIsDrmIdx:I

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iget v4, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mDrmMethodIdx:I

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v2, v5, :cond_0

    if-eq v1, v5, :cond_0

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    const/4 v5, 0x0

    invoke-static {v4, p2, v5}, Lcom/android/music/TrackBrowserActivity;->access$1800(Lcom/android/music/TrackBrowserActivity;Landroid/database/Cursor;Z)I

    move-result v3

    if-gez v3, :cond_1

    const/16 v0, 0x8

    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_1
    if-nez v3, :cond_2

    const v4, 0x202003f

    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const v4, 0x2020040

    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 14
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;

    iget v10, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mTitleIdx:I

    iget-object v11, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->buffer1:Landroid/database/CharArrayBuffer;

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    iget-object v10, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->line1:Landroid/widget/TextView;

    iget-object v11, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->buffer1:Landroid/database/CharArrayBuffer;

    iget-object v11, v11, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v12, 0x0

    iget-object v13, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->buffer1:Landroid/database/CharArrayBuffer;

    iget v13, v13, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v10, v11, v12, v13}, Landroid/widget/TextView;->setText([CII)V

    iget v10, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mDurationIdx:I

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    div-int/lit16 v8, v10, 0x3e8

    iget-object v10, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    int-to-long v11, v8

    move-object/from16 v0, p2

    invoke-static {v0, v11, v12}, Lcom/android/music/MusicUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mBuilder:Ljava/lang/StringBuilder;

    const/4 v10, 0x0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    iget v10, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mArtistIdx:I

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    const-string v10, "<unknown>"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    :cond_0
    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mUnknownArtist:Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    iget-object v10, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->buffer2:[C

    array-length v10, v10

    if-ge v10, v6, :cond_1

    new-array v10, v6, [C

    iput-object v10, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->buffer2:[C

    :cond_1
    const/4 v10, 0x0

    iget-object v11, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->buffer2:[C

    const/4 v12, 0x0

    invoke-virtual {v1, v10, v6, v11, v12}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    iget-object v10, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->line2:Landroid/widget/TextView;

    iget-object v11, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->buffer2:[C

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12, v6}, Landroid/widget/TextView;->setText([CII)V

    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v10}, Lcom/android/music/TrackBrowserActivity;->access$1600(Lcom/android/music/TrackBrowserActivity;)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v5, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->editIcon:Landroid/widget/ImageView;

    const v10, 0x7f020032

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    iget-object v4, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->play_indicator:Landroid/widget/ImageView;

    const-wide/16 v2, -0x1

    sget-object v10, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v10, :cond_3

    :try_start_0
    iget-boolean v10, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIsNowPlaying:Z

    if-eqz v10, :cond_7

    sget-object v10, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v10}, Lcom/android/music/IMediaPlaybackService;->getQueuePosition()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    int-to-long v2, v10

    :cond_3
    :goto_1
    iget-boolean v10, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIsNowPlaying:Z

    if-eqz v10, :cond_4

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    int-to-long v10, v10

    cmp-long v10, v10, v2

    if-eqz v10, :cond_5

    :cond_4
    iget-boolean v10, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIsNowPlaying:Z

    if-nez v10, :cond_8

    iget-boolean v10, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mDisableNowPlayingIndicator:Z

    if-nez v10, :cond_8

    iget v10, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mAudioIdIdx:I

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    cmp-long v10, v10, v2

    if-nez v10, :cond_8

    :cond_5
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    iget-object v10, v9, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->drmLock:Landroid/widget/ImageView;

    move-object/from16 v0, p3

    invoke-direct {p0, v10, v0}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->updateDrmLockIcon(Landroid/widget/ImageView;Landroid/database/Cursor;)V

    return-void

    :cond_6
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_7
    :try_start_1
    sget-object v10, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v10}, Lcom/android/music/IMediaPlaybackService;->getAudioId()J
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v2

    goto :goto_1

    :cond_8
    const/16 v10, 0x8

    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :catch_0
    move-exception v10

    goto :goto_1
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    const/4 p1, 0x0

    :cond_0
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v0}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v0

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v0, p1}, Lcom/android/music/TrackBrowserActivity;->access$702(Lcom/android/music/TrackBrowserActivity;Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-super {p0, p1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-direct {p0, p1}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->getColumnIndices(Landroid/database/Cursor;)V

    :cond_1
    return-void
.end method

.method public getPositionForSection(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIndexer:Landroid/widget/AlphabetIndexer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIndexer:Landroid/widget/AlphabetIndexer;

    invoke-virtual {v0, p1}, Landroid/widget/AlphabetIndexer;->getPositionForSection(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getQueryHandler()Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;
    .locals 1

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mQueryHandler:Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;

    return-object v0
.end method

.method public getSectionForPosition(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIndexer:Landroid/widget/AlphabetIndexer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIndexer:Landroid/widget/AlphabetIndexer;

    invoke-virtual {v0, p1}, Landroid/widget/AlphabetIndexer;->getSectionForPosition(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIndexer:Landroid/widget/AlphabetIndexer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mIndexer:Landroid/widget/AlphabetIndexer;

    invoke-virtual {v0}, Landroid/widget/AlphabetIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, " "

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ResourceCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;

    invoke-direct {v1}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;-><init>()V

    const v2, 0x7f0c0018

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->line1:Landroid/widget/TextView;

    const v2, 0x7f0c0019

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->line2:Landroid/widget/TextView;

    const v2, 0x7f0c0024

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    const v2, 0x7f0c0026

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->play_indicator:Landroid/widget/ImageView;

    new-instance v2, Landroid/database/CharArrayBuffer;

    const/16 v3, 0x64

    invoke-direct {v2, v3}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v2, v1, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->buffer1:Landroid/database/CharArrayBuffer;

    const/16 v2, 0xc8

    new-array v2, v2, [C

    iput-object v2, v1, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->buffer2:[C

    const v2, 0x7f0c0027

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->drmLock:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v2}, Lcom/android/music/TrackBrowserActivity;->access$1600(Lcom/android/music/TrackBrowserActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0c0023

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$ViewHolder;->editIcon:Landroid/widget/ImageView;

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method

.method public reloadStringOnLocaleChanges()V
    .locals 4

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    const v3, 0x7f070042

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    const v3, 0x7f070043

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mUnknownArtist:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mUnknownArtist:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-object v1, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mUnknownArtist:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mUnknownAlbum:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mUnknownAlbum:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mUnknownAlbum:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public runQueryOnBackgroundThread(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 5
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mConstraintIsValid:Z

    if-eqz v2, :cond_2

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mConstraint:Ljava/lang/String;

    if-eqz v2, :cond_1

    :cond_0
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mConstraint:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    invoke-virtual {p0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mQueryHandler:Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4}, Lcom/android/music/TrackBrowserActivity;->access$500(Lcom/android/music/TrackBrowserActivity;Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    iput-object v1, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mConstraint:Ljava/lang/String;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mConstraintIsValid:Z

    goto :goto_0
.end method

.method public setActivity(Lcom/android/music/TrackBrowserActivity;)V
    .locals 0
    .param p1    # Lcom/android/music/TrackBrowserActivity;

    iput-object p1, p0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->mActivity:Lcom/android/music/TrackBrowserActivity;

    return-void
.end method
