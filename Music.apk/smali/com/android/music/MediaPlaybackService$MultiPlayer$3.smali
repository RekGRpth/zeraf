.class Lcom/android/music/MediaPlaybackService$MultiPlayer$3;
.super Ljava/lang/Object;
.source "MediaPlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/MediaPlaybackService$MultiPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;


# direct methods
.method constructor <init>(Lcom/android/music/MediaPlaybackService$MultiPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$3;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_0

    const-string v1, "MusicService"

    const-string v2, "onInfo with null media player!"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    iget-object v2, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$3;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-static {v2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->access$4400(Lcom/android/music/MediaPlaybackService$MultiPlayer;)Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$3;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v2, v2, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v2, v0}, Lcom/android/music/MediaPlaybackService;->access$1402(Lcom/android/music/MediaPlaybackService;Z)Z

    :goto_1
    const-string v0, "MusicService"

    const-string v2, "onInfo, Disable the seeking for this media"

    invoke-static {v0, v2}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$3;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v2, v2, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v2, v0}, Lcom/android/music/MediaPlaybackService;->access$1502(Lcom/android/music/MediaPlaybackService;Z)Z

    goto :goto_1

    :sswitch_1
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$3;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-static {v0}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->access$4400(Lcom/android/music/MediaPlaybackService$MultiPlayer;)Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$3;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v0, v0, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v0, v1}, Lcom/android/music/MediaPlaybackService;->access$1402(Lcom/android/music/MediaPlaybackService;Z)Z

    :goto_2
    const-string v0, "MusicService"

    const-string v2, "onInfo, current track is seekable now!"

    invoke-static {v0, v2}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$3;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v0, v0, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v0, v1}, Lcom/android/music/MediaPlaybackService;->access$1502(Lcom/android/music/MediaPlaybackService;Z)Z

    goto :goto_2

    :sswitch_2
    const-string v0, "MusicService"

    const-string v2, "onInfo, Don\'t support the audio Format!"

    invoke-static {v0, v2}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$3;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-static {v0, p1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->access$4900(Lcom/android/music/MediaPlaybackService$MultiPlayer;Landroid/media/MediaPlayer;)V

    move v0, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x321 -> :sswitch_0
        0x324 -> :sswitch_1
        0x35e -> :sswitch_2
    .end sparse-switch
.end method
