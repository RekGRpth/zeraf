.class public Lcom/android/music/MediaPlaybackService;
.super Landroid/app/Service;
.source "MediaPlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/music/MediaPlaybackService$AlbumArtWorker;,
        Lcom/android/music/MediaPlaybackService$ServiceStub;,
        Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;,
        Lcom/android/music/MediaPlaybackService$MultiPlayer;,
        Lcom/android/music/MediaPlaybackService$Shuffler;
    }
.end annotation


# static fields
.field private static final ACTION_SHUTDOWN_IPO:Ljava/lang/String; = "android.intent.action.ACTION_SHUTDOWN_IPO"

.field public static final ATTACH_AUX_AUDIO_EFFECT:Ljava/lang/String; = "com.android.music.attachauxaudioeffect"

.field private static final AUX_AUDIO_EFFECT_ID:Ljava/lang/String; = "auxaudioeffectid"

.field private static final BOOKMARKCOLIDX:I = 0x9

.field private static final CHANGE_SETTING_MODE:I = 0x65

.field public static final CMDFORWARD:Ljava/lang/String; = "forward"

.field public static final CMDNAME:Ljava/lang/String; = "command"

.field public static final CMDNEXT:Ljava/lang/String; = "next"

.field public static final CMDPAUSE:Ljava/lang/String; = "pause"

.field public static final CMDPLAY:Ljava/lang/String; = "play"

.field public static final CMDPREVIOUS:Ljava/lang/String; = "previous"

.field public static final CMDREWIND:Ljava/lang/String; = "rewind"

.field public static final CMDSTOP:Ljava/lang/String; = "stop"

.field public static final CMDTOGGLEPAUSE:Ljava/lang/String; = "togglepause"

.field public static final DELTATIME:Ljava/lang/String; = "deltatime"

.field public static final DETACH_AUX_AUDIO_EFFECT:Ljava/lang/String; = "com.android.music.detachauxaudioeffect"

.field private static final FADEDOWN:I = 0x5

.field private static final FADEUP:I = 0x6

.field private static final FOCUSCHANGE:I = 0x4

.field private static final IDCOLIDX:I = 0x0

.field private static final IDLE_DELAY:I = 0xea60

.field public static final LAST:I = 0x3

.field private static final MAX_HISTORY_SIZE:I = 0x64

.field public static final MEDIA_ERROR_MUSICFX_DIED:I = -0x7fffff01

.field public static final META_CHANGED:Ljava/lang/String; = "com.android.music.metachanged"

.field public static final NEXT:I = 0x2

.field public static final NEXT_ACTION:Ljava/lang/String; = "com.android.music.musicservicecommand.next"

.field public static final NOW:I = 0x1

.field private static final OPEN_FAILED:I = 0x8

.field private static final OPEN_FAILED_MAX_COUNT:I = 0x2

.field public static final PAUSE_ACTION:Ljava/lang/String; = "com.android.music.musicservicecommand.pause"

.field public static final PLAYBACKSERVICE_STATUS:I = 0x1

.field public static final PLAYBACK_COMPLETE:Ljava/lang/String; = "com.android.music.playbackcomplete"

.field public static final PLAYSTATE_CHANGED:Ljava/lang/String; = "com.android.music.playstatechanged"

.field private static final PODCASTCOLIDX:I = 0x8

.field private static final POSITION_FOR_SPEED_FAST:I = 0x1388

.field public static final PREVIOUS_ACTION:Ljava/lang/String; = "com.android.music.musicservicecommand.previous"

.field public static final QUEUE_CHANGED:Ljava/lang/String; = "com.android.music.queuechanged"

.field public static final QUIT_PLAYBACK:Ljava/lang/String; = "com.android.music.quitplayback"

.field public static final REPEAT_ALL:I = 0x2

.field public static final REPEAT_CURRENT:I = 0x1

.field public static final REPEAT_NONE:I = 0x0

.field private static final SERVER_DIED:I = 0x3

.field public static final SERVICECMD:Ljava/lang/String; = "com.android.music.musicservicecommand"

.field private static final SETTING_MODE_REPEAT:I = 0x3

.field private static final SETTING_MODE_SHUFFLE:I = 0x2

.field public static final SHUFFLE_AUTO:I = 0x2

.field public static final SHUFFLE_NONE:I = 0x0

.field public static final SHUFFLE_NORMAL:I = 0x1

.field private static final SPEED_FAST:I = 0x28

.field private static final SPEED_NORMAL:I = 0x10

.field private static final TAG:Ljava/lang/String; = "MusicService"

.field public static final TOGGLEPAUSE_ACTION:Ljava/lang/String; = "com.android.music.musicservicecommand.togglepause"

.field private static final TRACK_ENDED:I = 0x1

.field private static final TRACK_WENT_TO_NEXT:I = 0x7

.field static mTrackCompleted:Z


# instance fields
.field private final hexdigits:[C

.field private mAlbumArt:Landroid/graphics/Bitmap;

.field private mAppWidgetProvider:Lcom/android/music/MediaAppWidgetProvider;

.field private mAsyncAlbumArtWorker:Lcom/android/music/MediaPlaybackService$AlbumArtWorker;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAutoShuffleList:[J

.field private mAuxEffectId:I

.field private final mBinder:Landroid/os/IBinder;

.field private final mBinderAvrcp:Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;

.field private final mContentObserver:Landroid/database/ContentObserver;

.field private mCurrentVolume:F

.field private mCursor:Landroid/database/Cursor;

.field mCursorCols:[Ljava/lang/String;

.field private mDelayedStopHandler:Landroid/os/Handler;

.field private mDoSeekWhenPrepared:Z

.field private mDurationOverride:J

.field private mEjectingCardPath:Ljava/lang/String;

.field private mFileToPlay:Ljava/lang/String;

.field private mHistory:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsPlayerReady:Z

.field private mIsPlaylistCompleted:Z

.field private mIsPrev:Z

.field private mIsReloadSuccess:Z

.field private mIsSupposedToBePlaying:Z

.field private mMediaMountedCount:I

.field private mMediaSeekable:Z

.field private mMediaplayerHandler:Landroid/os/Handler;

.field private mNextMediaSeekable:Z

.field private mNextPlayPos:I

.field private mOpenFailedCounter:I

.field private mPausedByTransientLossOfFocus:Z

.field private mPlayList:[J

.field private mPlayListLen:I

.field private mPlayPos:I

.field private mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

.field private mPreAudioId:J

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mQueueIsSaveable:Z

.field private mQuietMode:Z

.field private final mRand:Lcom/android/music/MediaPlaybackService$Shuffler;

.field private mReceiverUnregistered:Z

.field private mRemoteControlClient:Landroid/media/RemoteControlClient;

.field private mRepeatMode:I

.field private mSeekPositionForAnotherSong:J

.field private mServiceInUse:Z

.field private mServiceStartId:I

.field private mShuffleMode:I

.field private final mSkinChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mToast:Landroid/widget/Toast;

.field private mUnmountReceiver:Landroid/content/BroadcastReceiver;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWhetherAttachWhenPause:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/music/MediaPlaybackService;->mTrackCompleted:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    const-wide/16 v7, -0x1

    const/4 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mMediaMountedCount:I

    iput-object v4, p0, Lcom/android/music/MediaPlaybackService;->mAutoShuffleList:[J

    iput-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    new-instance v0, Ljava/util/Vector;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    iput v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    iput v6, p0, Lcom/android/music/MediaPlaybackService;->mNextPlayPos:I

    new-instance v0, Lcom/android/music/MediaPlaybackService$Shuffler;

    invoke-direct {v0, v4}, Lcom/android/music/MediaPlaybackService$Shuffler;-><init>(Lcom/android/music/MediaPlaybackService$1;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mRand:Lcom/android/music/MediaPlaybackService$Shuffler;

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mOpenFailedCounter:I

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "audio._id AS _id"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_podcast"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bookmark"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "is_drm"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "drm_method"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursorCols:[Ljava/lang/String;

    iput-object v4, p0, Lcom/android/music/MediaPlaybackService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    iput v6, p0, Lcom/android/music/MediaPlaybackService;->mServiceStartId:I

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mServiceInUse:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mQuietMode:Z

    iput-boolean v5, p0, Lcom/android/music/MediaPlaybackService;->mQueueIsSaveable:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mPausedByTransientLossOfFocus:Z

    invoke-static {}, Lcom/android/music/MediaAppWidgetProvider;->getInstance()Lcom/android/music/MediaAppWidgetProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mAppWidgetProvider:Lcom/android/music/MediaAppWidgetProvider;

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mDoSeekWhenPrepared:Z

    iput-boolean v5, p0, Lcom/android/music/MediaPlaybackService;->mMediaSeekable:Z

    iput-boolean v5, p0, Lcom/android/music/MediaPlaybackService;->mNextMediaSeekable:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mIsPlaylistCompleted:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mReceiverUnregistered:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mIsPrev:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mIsReloadSuccess:Z

    iput-wide v7, p0, Lcom/android/music/MediaPlaybackService;->mPreAudioId:J

    iput-wide v7, p0, Lcom/android/music/MediaPlaybackService;->mDurationOverride:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/music/MediaPlaybackService;->mSeekPositionForAnotherSong:J

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mAuxEffectId:I

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mWhetherAttachWhenPause:Z

    iput-object v4, p0, Lcom/android/music/MediaPlaybackService;->mStorageManager:Landroid/os/storage/StorageManager;

    iput-object v4, p0, Lcom/android/music/MediaPlaybackService;->mAsyncAlbumArtWorker:Lcom/android/music/MediaPlaybackService$AlbumArtWorker;

    iput-object v4, p0, Lcom/android/music/MediaPlaybackService;->mAlbumArt:Landroid/graphics/Bitmap;

    iput-object v4, p0, Lcom/android/music/MediaPlaybackService;->mEjectingCardPath:Ljava/lang/String;

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mCurrentVolume:F

    new-instance v0, Lcom/android/music/MediaPlaybackService$1;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackService$1;-><init>(Lcom/android/music/MediaPlaybackService;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/music/MediaPlaybackService$2;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackService$2;-><init>(Lcom/android/music/MediaPlaybackService;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/music/MediaPlaybackService$3;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackService$3;-><init>(Lcom/android/music/MediaPlaybackService;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    new-instance v0, Lcom/android/music/MediaPlaybackService$4;

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/music/MediaPlaybackService$4;-><init>(Lcom/android/music/MediaPlaybackService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mContentObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/music/MediaPlaybackService$5;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackService$5;-><init>(Lcom/android/music/MediaPlaybackService;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mSkinChangedReceiver:Landroid/content/BroadcastReceiver;

    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->hexdigits:[C

    new-instance v0, Lcom/android/music/MediaPlaybackService$6;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackService$6;-><init>(Lcom/android/music/MediaPlaybackService;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/music/MediaPlaybackService$ServiceStub;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackService$ServiceStub;-><init>(Lcom/android/music/MediaPlaybackService;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mBinder:Landroid/os/IBinder;

    new-instance v0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;-><init>(Lcom/android/music/MediaPlaybackService;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mBinderAvrcp:Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;

    return-void

    nop

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method static synthetic access$100(Lcom/android/music/MediaPlaybackService;)F
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mCurrentVolume:F

    return v0
.end method

.method static synthetic access$1000(Lcom/android/music/MediaPlaybackService;)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/music/MediaPlaybackService;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$102(Lcom/android/music/MediaPlaybackService;F)F
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # F

    iput p1, p0, Lcom/android/music/MediaPlaybackService;->mCurrentVolume:F

    return p1
.end method

.method static synthetic access$1100(Lcom/android/music/MediaPlaybackService;)I
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    return v0
.end method

.method static synthetic access$116(Lcom/android/music/MediaPlaybackService;F)F
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # F

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mCurrentVolume:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mCurrentVolume:F

    return v0
.end method

.method static synthetic access$1200(Lcom/android/music/MediaPlaybackService;)[J
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    return-object v0
.end method

.method static synthetic access$124(Lcom/android/music/MediaPlaybackService;F)F
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # F

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mCurrentVolume:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mCurrentVolume:F

    return v0
.end method

.method static synthetic access$1300(Lcom/android/music/MediaPlaybackService;J)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/music/MediaPlaybackService;->getCursorForId(J)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/music/MediaPlaybackService;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mMediaSeekable:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/android/music/MediaPlaybackService;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MediaPlaybackService;->mMediaSeekable:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/android/music/MediaPlaybackService;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mNextMediaSeekable:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/android/music/MediaPlaybackService;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MediaPlaybackService;->mNextMediaSeekable:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/android/music/MediaPlaybackService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/android/music/MediaPlaybackService;Landroid/content/Context;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, p2}, Lcom/android/music/MediaPlaybackService;->updateNotification(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/music/MediaPlaybackService;)J
    .locals 2
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-wide v0, p0, Lcom/android/music/MediaPlaybackService;->mPreAudioId:J

    return-wide v0
.end method

.method static synthetic access$1802(Lcom/android/music/MediaPlaybackService;J)J
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/music/MediaPlaybackService;->mPreAudioId:J

    return-wide p1
.end method

.method static synthetic access$1900(Lcom/android/music/MediaPlaybackService;)Lcom/android/music/MediaPlaybackService$AlbumArtWorker;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mAsyncAlbumArtWorker:Lcom/android/music/MediaPlaybackService$AlbumArtWorker;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/android/music/MediaPlaybackService;Lcom/android/music/MediaPlaybackService$AlbumArtWorker;)Lcom/android/music/MediaPlaybackService$AlbumArtWorker;
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Lcom/android/music/MediaPlaybackService$AlbumArtWorker;

    iput-object p1, p0, Lcom/android/music/MediaPlaybackService;->mAsyncAlbumArtWorker:Lcom/android/music/MediaPlaybackService$AlbumArtWorker;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/music/MediaPlaybackService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/music/MediaPlaybackService;Z)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/music/MediaPlaybackService;->addPlayedTrackToHistory(Z)V

    return-void
.end method

.method static synthetic access$2200(Lcom/android/music/MediaPlaybackService;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->setNextTrack()V

    return-void
.end method

.method static synthetic access$2300(Lcom/android/music/MediaPlaybackService;)I
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    return v0
.end method

.method static synthetic access$2400(Lcom/android/music/MediaPlaybackService;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mPausedByTransientLossOfFocus:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/android/music/MediaPlaybackService;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MediaPlaybackService;->mPausedByTransientLossOfFocus:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/android/music/MediaPlaybackService;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Ljava/lang/CharSequence;

    invoke-direct {p0, p1}, Lcom/android/music/MediaPlaybackService;->showToast(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$2600(Lcom/android/music/MediaPlaybackService;II)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/music/MediaPlaybackService;->handleSettingModeChange(II)V

    return-void
.end method

.method static synthetic access$2700(Lcom/android/music/MediaPlaybackService;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mReceiverUnregistered:Z

    return v0
.end method

.method static synthetic access$2800(Lcom/android/music/MediaPlaybackService;)Landroid/database/ContentObserver;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mContentObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/music/MediaPlaybackService;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/android/music/MediaPlaybackService;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/music/MediaPlaybackService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/music/MediaPlaybackService;)Lcom/android/music/MediaPlaybackService$MultiPlayer;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/music/MediaPlaybackService;Z)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/music/MediaPlaybackService;->saveQueue(Z)V

    return-void
.end method

.method static synthetic access$3100(Lcom/android/music/MediaPlaybackService;Z)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/music/MediaPlaybackService;->clearQueue(Z)V

    return-void
.end method

.method static synthetic access$3200(Lcom/android/music/MediaPlaybackService;)Lcom/android/music/MediaAppWidgetProvider;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mAppWidgetProvider:Lcom/android/music/MediaAppWidgetProvider;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/music/MediaPlaybackService;)I
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mAuxEffectId:I

    return v0
.end method

.method static synthetic access$3302(Lcom/android/music/MediaPlaybackService;I)I
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # I

    iput p1, p0, Lcom/android/music/MediaPlaybackService;->mAuxEffectId:I

    return p1
.end method

.method static synthetic access$3402(Lcom/android/music/MediaPlaybackService;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MediaPlaybackService;->mWhetherAttachWhenPause:Z

    return p1
.end method

.method static synthetic access$3500(Lcom/android/music/MediaPlaybackService;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mAlbumArt:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/android/music/MediaPlaybackService;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mServiceInUse:Z

    return v0
.end method

.method static synthetic access$3700(Lcom/android/music/MediaPlaybackService;)I
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mServiceStartId:I

    return v0
.end method

.method static synthetic access$3800(Lcom/android/music/MediaPlaybackService;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mEjectingCardPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/android/music/MediaPlaybackService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/music/MediaPlaybackService;->mEjectingCardPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3900(Lcom/android/music/MediaPlaybackService;)I
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mMediaMountedCount:I

    return v0
.end method

.method static synthetic access$3908(Lcom/android/music/MediaPlaybackService;)I
    .locals 2
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mMediaMountedCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/music/MediaPlaybackService;->mMediaMountedCount:I

    return v0
.end method

.method static synthetic access$3910(Lcom/android/music/MediaPlaybackService;)I
    .locals 2
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mMediaMountedCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/music/MediaPlaybackService;->mMediaMountedCount:I

    return v0
.end method

.method static synthetic access$400(Lcom/android/music/MediaPlaybackService;)I
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    return v0
.end method

.method static synthetic access$4002(Lcom/android/music/MediaPlaybackService;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MediaPlaybackService;->mQueueIsSaveable:Z

    return p1
.end method

.method static synthetic access$402(Lcom/android/music/MediaPlaybackService;I)I
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # I

    iput p1, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    return p1
.end method

.method static synthetic access$4100(Lcom/android/music/MediaPlaybackService;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->reloadQueue()V

    return-void
.end method

.method static synthetic access$4200(Lcom/android/music/MediaPlaybackService;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->reloadQueueAfterScan()V

    return-void
.end method

.method static synthetic access$4300(Lcom/android/music/MediaPlaybackService;)I
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mNextPlayPos:I

    return v0
.end method

.method static synthetic access$4700(Lcom/android/music/MediaPlaybackService;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/music/MediaPlaybackService;Z)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/music/MediaPlaybackService;->sendSessionIdToAudioEffect(Z)V

    return-void
.end method

.method static synthetic access$5000(Lcom/android/music/MediaPlaybackService;)I
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mOpenFailedCounter:I

    return v0
.end method

.method static synthetic access$5002(Lcom/android/music/MediaPlaybackService;I)I
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # I

    iput p1, p0, Lcom/android/music/MediaPlaybackService;->mOpenFailedCounter:I

    return p1
.end method

.method static synthetic access$5008(Lcom/android/music/MediaPlaybackService;)I
    .locals 2
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mOpenFailedCounter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/music/MediaPlaybackService;->mOpenFailedCounter:I

    return v0
.end method

.method static synthetic access$5100(Lcom/android/music/MediaPlaybackService;Z)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    return-void
.end method

.method static synthetic access$5200(Lcom/android/music/MediaPlaybackService;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    return v0
.end method

.method static synthetic access$5202(Lcom/android/music/MediaPlaybackService;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    return p1
.end method

.method static synthetic access$5300(Lcom/android/music/MediaPlaybackService;)I
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    return v0
.end method

.method static synthetic access$5400(Lcom/android/music/MediaPlaybackService;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/android/music/MediaPlaybackService;)J
    .locals 2
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-wide v0, p0, Lcom/android/music/MediaPlaybackService;->mSeekPositionForAnotherSong:J

    return-wide v0
.end method

.method static synthetic access$5502(Lcom/android/music/MediaPlaybackService;J)J
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/music/MediaPlaybackService;->mSeekPositionForAnotherSong:J

    return-wide p1
.end method

.method static synthetic access$5600(Lcom/android/music/MediaPlaybackService;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->isPodcast()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5700(Lcom/android/music/MediaPlaybackService;)J
    .locals 2
    .param p0    # Lcom/android/music/MediaPlaybackService;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->getBookmark()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$600(Lcom/android/music/MediaPlaybackService;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    return v0
.end method

.method static synthetic access$700(Lcom/android/music/MediaPlaybackService;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mDoSeekWhenPrepared:Z

    return v0
.end method

.method static synthetic access$702(Lcom/android/music/MediaPlaybackService;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MediaPlaybackService;->mDoSeekWhenPrepared:Z

    return p1
.end method

.method static synthetic access$800(Lcom/android/music/MediaPlaybackService;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackService;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mQuietMode:Z

    return v0
.end method

.method static synthetic access$802(Lcom/android/music/MediaPlaybackService;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MediaPlaybackService;->mQuietMode:Z

    return p1
.end method

.method static synthetic access$900(Lcom/android/music/MediaPlaybackService;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackService;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->openCurrentAndNext()V

    return-void
.end method

.method private addPlayedTrackToHistory(Z)V
    .locals 3
    .param p1    # Z

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-ltz v0, :cond_1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addPlayedTrackToHistory: mPlayPos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mHistory = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElementAt(I)V

    :cond_2
    return-void
.end method

.method private addToPlayList([JI)V
    .locals 7
    .param p1    # [J
    .param p2    # I

    array-length v0, p1

    if-gez p2, :cond_0

    const/4 v3, 0x0

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    const/4 p2, 0x0

    :cond_0
    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/2addr v3, v0

    invoke-direct {p0, v3}, Lcom/android/music/MediaPlaybackService;->ensurePlayListCapacity(I)V

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-le p2, v3, :cond_1

    iget p2, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    :cond_1
    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    sub-int v2, v3, p2

    move v1, v2

    :goto_0
    if-lez v1, :cond_2

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    add-int v4, p2, v1

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    add-int v6, p2, v1

    sub-int/2addr v6, v0

    aget-wide v5, v5, v6

    aput-wide v5, v3, v4

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_3

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    add-int v4, p2, v1

    aget-wide v5, p1, v1

    aput-wide v5, v3, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/2addr v3, v0

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->clear()V

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const-string v3, "com.android.music.metachanged"

    invoke-direct {p0, v3}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method private canGoToNext(JJ)Z
    .locals 7
    .param p1    # J
    .param p3    # J

    const/4 v0, 0x1

    const-wide/16 v5, 0x3e8

    const-wide/16 v3, 0x0

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    if-eq v1, v0, :cond_3

    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsPlaylistCompleted:Z

    if-nez v1, :cond_2

    cmp-long v1, p1, v3

    if-eqz v1, :cond_2

    cmp-long v1, p1, v3

    if-lez v1, :cond_0

    cmp-long v1, p1, v5

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->position()J

    move-result-wide v1

    cmp-long v1, v1, v3

    if-gtz v1, :cond_2

    :cond_0
    cmp-long v1, p1, v5

    if-lez v1, :cond_1

    cmp-long v1, p1, p3

    if-gtz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->position()J

    move-result-wide v1

    sub-long v1, p1, v1

    const-wide/16 v3, 0xa

    div-long v3, p1, v3

    cmp-long v1, v1, v3

    if-ltz v1, :cond_2

    :cond_1
    cmp-long v1, p1, p3

    if-lez v1, :cond_3

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->position()J

    move-result-wide v1

    sub-long v1, p1, v1

    cmp-long v1, v1, v5

    if-gtz v1, :cond_3

    :cond_2
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkDrmWhenOpenTrack(Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/database/Cursor;

    const/4 v5, 0x1

    if-eqz p1, :cond_1

    const-string v2, "is_drm"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-string v2, "drm_method"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v2, "MusicService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isDrm="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", drmMethod="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v1, v5, :cond_0

    if-ne v1, v5, :cond_1

    if-ne v0, v5, :cond_1

    :cond_0
    iput-boolean v5, p0, Lcom/android/music/MediaPlaybackService;->mQuietMode:Z

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->openCurrentAndNext()V

    iput-boolean v5, p0, Lcom/android/music/MediaPlaybackService;->mIsReloadSuccess:Z

    :cond_1
    return-void
.end method

.method private clearQueue(Z)V
    .locals 5
    .param p1    # Z

    const/4 v2, 0x0

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "queue"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "history"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "curpos"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "seekpos"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "repeatmode"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "shufflemode"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/music/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    :cond_0
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearQueue(): needClearQueue "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private doAutoShuffleUpdate()V
    .locals 10

    const/4 v9, 0x0

    const/4 v3, 0x0

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    const/16 v6, 0xa

    if-le v5, v6, :cond_0

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    add-int/lit8 v5, v5, -0x9

    invoke-virtual {p0, v9, v5}, Lcom/android/music/MediaPlaybackService;->removeTracks(II)I

    const/4 v3, 0x1

    :cond_0
    iget v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-gez v5, :cond_2

    const/4 v5, -0x1

    :goto_0
    sub-int v5, v6, v5

    rsub-int/lit8 v4, v5, 0x7

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_4

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v1, -0x1

    :goto_2
    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mRand:Lcom/android/music/MediaPlaybackService$Shuffler;

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mAutoShuffleList:[J

    array-length v6, v6

    invoke-virtual {v5, v6}, Lcom/android/music/MediaPlaybackService$Shuffler;->nextInt(I)I

    move-result v1

    invoke-direct {p0, v1, v2}, Lcom/android/music/MediaPlaybackService;->wasRecentlyUsed(II)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    const/16 v6, 0x64

    if-le v5, v6, :cond_1

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v5, v9}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    :cond_1
    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/lit8 v5, v5, 0x1

    invoke-direct {p0, v5}, Lcom/android/music/MediaPlaybackService;->ensurePlayListCapacity(I)V

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    iget v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService;->mAutoShuffleList:[J

    aget-wide v7, v7, v1

    aput-wide v7, v5, v6

    const/4 v3, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    goto :goto_0

    :cond_3
    div-int/lit8 v2, v2, 0x2

    goto :goto_2

    :cond_4
    if-eqz v3, :cond_5

    const-string v5, "com.android.music.queuechanged"

    invoke-direct {p0, v5}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method private ensurePlayListCapacity(I)V
    .locals 5
    .param p1    # I

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    array-length v3, v3

    if-le p1, v3, :cond_3

    :cond_0
    mul-int/lit8 v3, p1, 0x2

    new-array v2, v3, [J

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    array-length v1, v3

    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    aget-wide v3, v3, v0

    aput-wide v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    goto :goto_0

    :cond_2
    iput-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    :cond_3
    return-void
.end method

.method private getBookmark()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    monitor-exit p0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getCursorForId(J)Landroid/database/Cursor;
    .locals 8
    .param p1    # J

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mCursorCols:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_0
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCursorForId is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v6
.end method

.method private getNextPosition(Z)I
    .locals 12
    .param p1    # Z

    const/4 v11, 0x2

    const/4 v0, -0x1

    const-string v8, "MusicService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getNextPosition("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_7

    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    new-array v7, v4, [I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aput v1, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v8, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v3

    move v5, v4

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v8, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v8, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v4, :cond_1

    aget v8, v7, v2

    if-ltz v8, :cond_1

    add-int/lit8 v5, v5, -0x1

    aput v0, v7, v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    if-gtz v5, :cond_4

    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    if-eq v8, v11, :cond_3

    if-eqz p1, :cond_6

    :cond_3
    move v5, v4

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v4, :cond_4

    aput v1, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    iget-object v8, p0, Lcom/android/music/MediaPlaybackService;->mRand:Lcom/android/music/MediaPlaybackService$Shuffler;

    invoke-virtual {v8, v5}, Lcom/android/music/MediaPlaybackService$Shuffler;->nextInt(I)I

    move-result v6

    const/4 v0, -0x1

    :cond_5
    add-int/lit8 v0, v0, 0x1

    aget v8, v7, v0

    if-ltz v8, :cond_5

    add-int/lit8 v6, v6, -0x1

    if-gez v6, :cond_5

    :cond_6
    :goto_3
    return v0

    :cond_7
    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    if-ne v8, v11, :cond_8

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->doAutoShuffleUpdate()V

    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    add-int/lit8 v0, v8, 0x1

    goto :goto_3

    :cond_8
    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    iget v9, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/lit8 v9, v9, -0x1

    if-lt v8, v9, :cond_b

    const-string v8, "MusicService"

    const-string v9, "next: end of list..."

    invoke-static {v8, v9}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    if-nez v8, :cond_9

    if-eqz p1, :cond_6

    :cond_9
    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    if-eq v8, v11, :cond_a

    if-eqz p1, :cond_6

    :cond_a
    const/4 v0, 0x0

    goto :goto_3

    :cond_b
    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    add-int/lit8 v0, v8, 0x1

    goto :goto_3
.end method

.method private gotoIdleState()V
    .locals 4

    const/4 v3, 0x0

    const-string v1, "MusicService"

    const-string v2, "gotoIdleState"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v3, p0, Lcom/android/music/MediaPlaybackService;->mAlbumArt:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const-wide/32 v2, 0xea60

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Service;->stopForeground(Z)V

    return-void
.end method

.method private handleSettingModeChange(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const-string v1, "MusicService"

    const-string v2, "[AVRCP] CHANGE_SETTING_MODE setting:%d newMode:%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v1, "MusicService"

    const-string v2, "Unsupport AVRCP setting mode!"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getShuffleMode()I

    move-result v0

    if-eq v0, p2, :cond_0

    invoke-virtual {p0, p2}, Lcom/android/music/MediaPlaybackService;->setShuffleMode(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getRepeatMode()I

    move-result v0

    if-eq v0, p2, :cond_0

    invoke-virtual {p0, p2}, Lcom/android/music/MediaPlaybackService;->setRepeatMode(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private isDrmCanPlay(Landroid/database/Cursor;)Z
    .locals 7
    .param p1    # Landroid/database/Cursor;

    const/4 v6, 0x1

    if-nez p1, :cond_0

    const-string v3, "MusicService"

    const-string v4, "isDrmCanPlay to Check given drm with null cursor."

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    const-string v3, "is_drm"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-string v3, "drm_method"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v3, "MusicService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDrmCanPlay: isDrm = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", drmMethod = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v1, v6, :cond_1

    if-eq v0, v6, :cond_1

    const/4 v2, 0x0

    :cond_1
    const-string v3, "MusicService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDrmCanPlay "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isEventFromMonkey()Z
    .locals 4

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isEventFromMonkey "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method private isPodcast()Z
    .locals 3

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_0

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const/16 v2, 0x8

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private makeAutoShuffleList()Z
    .locals 12

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v6, 0x0

    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "is_music=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v1, v11

    :goto_0
    return v1

    :cond_2
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v8

    new-array v9, v8, [J

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v8, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    aput-wide v1, v9, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_3
    iput-object v9, p0, Lcom/android/music/MediaPlaybackService;->mAutoShuffleList:[J
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    move v1, v10

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1

    :catch_0
    move-exception v1

    if-eqz v6, :cond_6

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    move v1, v11

    goto :goto_0
.end method

.method private mediaCanSeek()Z
    .locals 9

    const/4 v4, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v5, :cond_0

    monitor-exit p0

    :goto_0
    return v4

    :cond_0
    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const-string v7, "_data"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide/32 v1, 0x7fffffff

    const-string v0, ".imy"

    if-eqz v3, :cond_2

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    const-string v6, ".imy"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->duration()J

    move-result-wide v5

    const-wide/32 v7, 0x7fffffff

    cmp-long v5, v5, v7

    if-eqz v5, :cond_2

    :cond_1
    :goto_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private notifyChange(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyChange("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "id"

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getAudioId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "artist"

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getArtistName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "album"

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getAlbumName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "track"

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "playing"

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->isPlaying()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.android.music.quitplayback"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    iput v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    iput v4, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    iput v4, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    const-string v1, "com.android.music.queuechanged"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->saveQueue(Z)V

    :goto_1
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mAppWidgetProvider:Lcom/android/music/MediaAppWidgetProvider;

    invoke-virtual {v1, p0, p1}, Lcom/android/music/MediaAppWidgetProvider;->notifyChange(Lcom/android/music/MediaPlaybackService;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/android/music/MediaPlaybackService;->notifyBTAvrcp(Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendStickyBroadcast(Landroid/content/Intent;)V

    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.android.music.metachanged"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v4}, Lcom/android/music/MediaPlaybackService;->saveQueue(Z)V

    goto :goto_1
.end method

.method private openCurrentAndNext()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    :cond_0
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/android/music/MediaPlaybackService;->mDurationOverride:J

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    monitor-exit p0

    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    iget v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    aget-wide v1, v1, v2

    invoke-direct {p0, v1, v2}, Lcom/android/music/MediaPlaybackService;->getCursorForId(J)Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    :goto_1
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/music/MediaPlaybackService;->open(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    :cond_3
    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mOpenFailedCounter:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/android/music/MediaPlaybackService;->mOpenFailedCounter:I

    const/4 v2, 0x2

    if-ge v1, v2, :cond_8

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    const/4 v2, 0x1

    if-le v1, v2, :cond_8

    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsPrev:Z

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->prev()V

    :cond_4
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_5
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->getNextPosition(Z)I

    move-result v0

    if-gez v0, :cond_7

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->gotoIdleState()V

    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    const-string v1, "com.android.music.playstatechanged"

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_6
    monitor-exit p0

    goto :goto_0

    :cond_7
    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    iget v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    aget-wide v1, v1, v2

    invoke-direct {p0, v1, v2}, Lcom/android/music/MediaPlaybackService;->getCursorForId(J)Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    goto :goto_1

    :cond_8
    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mQuietMode:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_9
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clear palylist and position when open failed: mPlayListLen = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/music/MediaPlaybackService;->mNextPlayPos:I

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->gotoIdleState()V

    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    if-eqz v1, :cond_a

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    const-string v1, "com.android.music.playstatechanged"

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_a
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/music/MediaPlaybackService;->mOpenFailedCounter:I

    const-string v1, "MusicService"

    const-string v2, "Failed to open file for playback"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method private reloadQueue()V
    .locals 15

    const-string v12, "MusicService"

    const-string v13, "reloadQueue"

    invoke-static {v12, v13}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Lcom/android/music/MusicUtils;->hasMountedSDcard(Landroid/content/Context;)Z

    move-result v2

    const-string v12, "MusicService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "reloadQueue: hasCard = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v2, :cond_0

    const-string v12, "MusicService"

    const-string v13, "reloadQueue: no sd card!"

    invoke-static {v12, v13}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v13, "queue"

    const-string v14, ""

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    :goto_1
    const/4 v12, 0x1

    if-le v8, v12, :cond_13

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v10, 0x0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v8, :cond_5

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v12, 0x3b

    if-ne v0, v12, :cond_2

    add-int/lit8 v12, v5, 0x1

    invoke-direct {p0, v12}, Lcom/android/music/MediaPlaybackService;->ensurePlayListCapacity(I)V

    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    int-to-long v13, v4

    aput-wide v13, v12, v5

    add-int/lit8 v5, v5, 0x1

    const/4 v4, 0x0

    const/4 v10, 0x0

    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    :cond_2
    const/16 v12, 0x30

    if-lt v0, v12, :cond_3

    const/16 v12, 0x39

    if-gt v0, v12, :cond_3

    add-int/lit8 v12, v0, -0x30

    shl-int/2addr v12, v10

    add-int/2addr v4, v12

    :goto_4
    add-int/lit8 v10, v10, 0x4

    goto :goto_3

    :cond_3
    const/16 v12, 0x61

    if-lt v0, v12, :cond_4

    const/16 v12, 0x66

    if-gt v0, v12, :cond_4

    add-int/lit8 v12, v0, 0xa

    add-int/lit8 v12, v12, -0x61

    shl-int/2addr v12, v10

    add-int/2addr v4, v12

    goto :goto_4

    :cond_4
    const/4 v5, 0x0

    :cond_5
    iput v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v13, "curpos"

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    const-string v12, "MusicService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "reloadQueue: mPlayListLen="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", curpos="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz v6, :cond_6

    iget v12, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-lt v6, v12, :cond_7

    :cond_6
    const/4 v12, 0x0

    iput v12, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    goto/16 :goto_0

    :cond_7
    iput v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    const-string v12, "MusicService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "reloadQueue: mPlayPos="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v12, 0x14

    iput v12, p0, Lcom/android/music/MediaPlaybackService;->mOpenFailedCounter:I

    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    iget v13, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    aget-wide v12, v12, v13

    invoke-direct {p0, v12, v13}, Lcom/android/music/MediaPlaybackService;->getCursorForId(J)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->checkDrmWhenOpenTrack(Landroid/database/Cursor;)V

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x0

    :cond_8
    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v12}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v12

    if-nez v12, :cond_9

    const-string v12, "MusicService"

    const-string v13, "reloadQueue: open failed! not inited!"

    invoke-static {v12, v13}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x0

    iput v12, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    const/4 v12, -0x1

    iput v12, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    goto/16 :goto_0

    :cond_9
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/android/music/MediaPlaybackService;->mDoSeekWhenPrepared:Z

    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v13, "repeatmode"

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    const/4 v12, 0x2

    if-eq v9, v12, :cond_a

    const/4 v12, 0x1

    if-eq v9, v12, :cond_a

    const/4 v9, 0x0

    :cond_a
    iput v9, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v13, "shufflemode"

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v11

    const/4 v12, 0x2

    if-eq v11, v12, :cond_b

    const/4 v12, 0x1

    if-eq v11, v12, :cond_b

    const/4 v11, 0x0

    :cond_b
    if-eqz v11, :cond_c

    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v13, "history"

    const-string v14, ""

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_e

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    :goto_5
    const/4 v12, 0x1

    if-le v8, v12, :cond_c

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v10, 0x0

    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v12}, Ljava/util/Vector;->clear()V

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v8, :cond_c

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v12, 0x3b

    if-ne v0, v12, :cond_10

    iget v12, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-lt v4, v12, :cond_f

    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v12}, Ljava/util/Vector;->clear()V

    :cond_c
    :goto_7
    const/4 v12, 0x2

    if-ne v11, v12, :cond_d

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->makeAutoShuffleList()Z

    move-result v12

    if-nez v12, :cond_d

    const/4 v11, 0x0

    :cond_d
    iput v11, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    goto/16 :goto_0

    :cond_e
    const/4 v8, 0x0

    goto :goto_5

    :cond_f
    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x0

    const/4 v10, 0x0

    :goto_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_10
    const/16 v12, 0x30

    if-lt v0, v12, :cond_11

    const/16 v12, 0x39

    if-gt v0, v12, :cond_11

    add-int/lit8 v12, v0, -0x30

    shl-int/2addr v12, v10

    add-int/2addr v4, v12

    :goto_9
    add-int/lit8 v10, v10, 0x4

    goto :goto_8

    :cond_11
    const/16 v12, 0x61

    if-lt v0, v12, :cond_12

    const/16 v12, 0x66

    if-gt v0, v12, :cond_12

    add-int/lit8 v12, v0, 0xa

    add-int/lit8 v12, v12, -0x61

    shl-int/2addr v12, v10

    add-int/2addr v4, v12

    goto :goto_9

    :cond_12
    iget-object v12, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v12}, Ljava/util/Vector;->clear()V

    goto :goto_7

    :cond_13
    const/4 v12, 0x0

    iput v12, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    goto/16 :goto_0
.end method

.method private reloadQueueAfterScan()V
    .locals 4

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/music/MusicUtils;->hasMountedSDcard(Landroid/content/Context;)Z

    move-result v0

    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reloadQueueAfterScan: hasCard = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isReloadSuccess = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mIsReloadSuccess:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsReloadSuccess:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    array-length v1, v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->reloadQueue()V

    const-string v1, "com.android.music.queuechanged"

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    const-string v1, "com.android.music.metachanged"

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private removeTracksInternal(II)I
    .locals 9
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    monitor-enter p0

    if-ge p2, p1, :cond_0

    :try_start_0
    monitor-exit p0

    :goto_0
    return v3

    :cond_0
    if-gez p1, :cond_1

    const/4 p1, 0x0

    :cond_1
    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-lt p2, v5, :cond_2

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/lit8 p2, v5, -0x1

    :cond_2
    const/4 v0, 0x0

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-gt p1, v5, :cond_4

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-gt v5, p2, :cond_4

    iput p1, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    const/4 v0, 0x1

    :cond_3
    :goto_1
    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    sub-int/2addr v5, p2

    add-int/lit8 v2, v5, -0x1

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_5

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    add-int v6, p1, v1

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    add-int/lit8 v8, p2, 0x1

    add-int/2addr v8, v1

    aget-wide v7, v7, v8

    aput-wide v7, v5, v6

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-le v5, p2, :cond_3

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    sub-int v6, p2, p1

    add-int/lit8 v6, v6, 0x1

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_5
    :try_start_1
    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    sub-int v6, p2, p1

    add-int/lit8 v6, v6, 0x1

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    const-string v5, "MusicService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "removeTracksInternal need goto gotoNext("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_7

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-nez v5, :cond_8

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->clear()V

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    const/4 v3, -0x1

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    :cond_6
    :goto_3
    const-string v3, "com.android.music.metachanged"

    invoke-direct {p0, v3}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_7
    const-string v3, "MusicService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removeTracksInternal end: mPlayListLen = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mPlayPos = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    sub-int v3, p2, p1

    add-int/lit8 v3, v3, 0x1

    monitor-exit p0

    goto/16 :goto_0

    :cond_8
    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    iget v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-lt v5, v6, :cond_9

    const/4 v5, 0x0

    iput v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    :cond_9
    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->isPlaying()Z

    move-result v5

    if-nez v5, :cond_a

    move v3, v4

    :cond_a
    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackService;->mQuietMode:Z

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->openCurrentAndNext()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method private saveBookmarkIfNeeded()V
    .locals 12

    const-wide/16 v10, 0x2710

    :try_start_0
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->isPodcast()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->position()J

    move-result-wide v4

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->getBookmark()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->duration()J

    move-result-wide v2

    cmp-long v8, v4, v0

    if-gez v8, :cond_0

    add-long v8, v4, v10

    cmp-long v8, v8, v0

    if-gtz v8, :cond_1

    :cond_0
    cmp-long v8, v4, v0

    if-lez v8, :cond_2

    sub-long v8, v4, v10

    cmp-long v8, v8, v0

    if-gez v8, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-wide/16 v8, 0x3a98

    cmp-long v8, v4, v8

    if-ltz v8, :cond_3

    add-long v8, v4, v10

    cmp-long v8, v8, v2

    if-lez v8, :cond_4

    :cond_3
    const-wide/16 v4, 0x0

    :cond_4
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v8, "bookmark"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v8, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v9, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-static {v8, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v6, v7, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    goto :goto_0
.end method

.method private saveQueue(Z)V
    .locals 12
    .param p1    # Z

    const-wide/16 v10, 0x0

    const-string v7, "MusicService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saveQueue("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v7, p0, Lcom/android/music/MediaPlaybackService;->mQueueIsSaveable:Z

    if-nez v7, :cond_0

    const-string v7, "MusicService"

    const-string v8, "saveQueue: queue NOT savable!!"

    invoke-static {v7, v8}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v7, p0, Lcom/android/music/MediaPlaybackService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    if-eqz p1, :cond_8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_4

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    aget-wide v4, v7, v2

    cmp-long v7, v4, v10

    if-gez v7, :cond_1

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    cmp-long v7, v4, v10

    if-nez v7, :cond_2

    const-string v7, "0;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    :goto_3
    cmp-long v7, v4, v10

    if-eqz v7, :cond_3

    const-wide/16 v7, 0xf

    and-long/2addr v7, v4

    long-to-int v0, v7

    const/4 v7, 0x4

    ushr-long/2addr v4, v7

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService;->hexdigits:[C

    aget-char v7, v7, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_3
    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    const-string v7, "queue"

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v7, "MusicService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saveQueue: queue="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v7, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v3

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v3, :cond_7

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v7, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_5

    const-string v7, "0;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    :goto_6
    if-eqz v4, :cond_6

    and-int/lit8 v0, v4, 0xf

    ushr-int/lit8 v4, v4, 0x4

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService;->hexdigits:[C

    aget-char v7, v7, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_6
    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_7
    const-string v7, "history"

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_8
    const-string v7, "curpos"

    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v7, "MusicService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saveQueue: mPlayPos="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mPlayListLen="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v7}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v7

    if-eqz v7, :cond_9

    iget-boolean v7, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    if-eqz v7, :cond_9

    const-string v7, "seekpos"

    iget-object v8, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v8}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->position()J

    move-result-wide v8

    invoke-interface {v1, v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_9
    const-string v7, "repeatmode"

    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v7, "shufflemode"

    iget v8, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-static {v1}, Lcom/android/music/SharedPreferencesCompat;->apply(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_0
.end method

.method private scanBackward(J)V
    .locals 14
    .param p1    # J

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->position()J

    move-result-wide v6

    const-wide/16 v4, 0x0

    const-string v8, "MusicService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "startSeekPos: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v8, 0x1388

    cmp-long v8, p1, v8

    if-gez v8, :cond_1

    const-wide/16 v8, 0x10

    mul-long v4, p1, v8

    :goto_0
    sub-long v2, v6, v4

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-gez v8, :cond_2

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->prev()V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->duration()J

    move-result-wide v0

    const-string v8, "MusicService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "duration: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-long/2addr v2, v0

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-lez v8, :cond_0

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/music/MediaPlaybackService;->mDoSeekWhenPrepared:Z

    iput-wide v2, p0, Lcom/android/music/MediaPlaybackService;->mSeekPositionForAnotherSong:J

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-wide/16 v8, 0x1388

    const-wide/16 v10, 0x1388

    sub-long v10, p1, v10

    const-wide/16 v12, 0x28

    mul-long/2addr v10, v12

    add-long v4, v8, v10

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v2, v3}, Lcom/android/music/MediaPlaybackService;->seek(J)J

    goto :goto_1
.end method

.method private scanForward(J)V
    .locals 14
    .param p1    # J

    const-wide/16 v4, 0x0

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->position()J

    move-result-wide v6

    const-wide/16 v8, 0x1388

    cmp-long v8, p1, v8

    if-gez v8, :cond_1

    const-wide/16 v8, 0x10

    mul-long v4, p1, v8

    :goto_0
    add-long v2, v6, v4

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->duration()J

    move-result-wide v0

    cmp-long v8, v2, v0

    if-ltz v8, :cond_2

    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/android/music/MediaPlaybackService;->gotoNext(Z)V

    sub-long/2addr v2, v0

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->duration()J

    move-result-wide v0

    cmp-long v8, v2, v0

    if-lez v8, :cond_0

    move-wide v2, v0

    :cond_0
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/music/MediaPlaybackService;->mDoSeekWhenPrepared:Z

    iput-wide v2, p0, Lcom/android/music/MediaPlaybackService;->mSeekPositionForAnotherSong:J

    :goto_1
    return-void

    :cond_1
    const-wide/16 v8, 0x1388

    const-wide/16 v10, 0x1388

    sub-long v10, p1, v10

    const-wide/16 v12, 0x28

    mul-long/2addr v10, v12

    add-long v4, v8, v10

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v2, v3}, Lcom/android/music/MediaPlaybackService;->seek(J)J

    goto :goto_1
.end method

.method private sendSessionIdToAudioEffect(Z)V
    .locals 3
    .param p1    # Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.OPEN_AUDIO_EFFECT_CONTROL_SESSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.media.extra.AUDIO_SESSION"

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getAudioSessionId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "android.media.extra.PACKAGE_NAME"

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "reset_reverb"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private setNextTrack()V
    .locals 5

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "MusicService"

    const-string v3, "setNextTrack with player not initialized!"

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v2, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    if-ne v2, v3, :cond_1

    iget v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-ne v2, v3, :cond_1

    const-string v2, "MusicService"

    const-string v3, "playlist\'s length is 1 with shuffle nomal mode,need not set nextplayer."

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/music/MediaPlaybackService;->getNextPosition(Z)I

    move-result v2

    iput v2, p0, Lcom/android/music/MediaPlaybackService;->mNextPlayPos:I

    iget v2, p0, Lcom/android/music/MediaPlaybackService;->mNextPlayPos:I

    if-ltz v2, :cond_2

    iget v2, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mNextPlayPos:I

    aget-wide v0, v2, v3

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setNextDataSource(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setNextDataSource(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showToast(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mToast:Landroid/widget/Toast;

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private stop(Z)V
    .locals 3
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->stop()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mDoSeekWhenPrepared:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mMediaSeekable:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mFileToPlay:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    :cond_1
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/music/MediaPlaybackService;->mDurationOverride:J

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->gotoIdleState()V

    :cond_2
    if-eqz p1, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateNotification(Landroid/content/Context;Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/graphics/Bitmap;

    new-instance v7, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f03000e

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getArtistName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v8, "<unknown>"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    const v8, 0x7f070042

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v5

    if-eqz v5, :cond_2

    const v8, 0x7f0c0035

    invoke-virtual {v7, v8, v5}, Landroid/widget/RemoteViews;->setTextColor(II)V

    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const v8, 0x7f0c0035

    invoke-virtual {v7, v8, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    new-instance v1, Landroid/content/Intent;

    const-string v8, "com.android.music.PLAYBACK_VIEWER"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v8, "collapse_statusbar"

    const/4 v9, 0x1

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {p1, v8, v1, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const v8, 0x7f0c0034

    invoke-virtual {v7, v8, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v1, Landroid/content/Intent;

    const-string v8, "com.android.music.musicservicecommand.previous"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v8, Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v1, p1, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {p1, v8, v1, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const v8, 0x7f0c0036

    invoke-virtual {v7, v8, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v1, Landroid/content/Intent;

    const-string v8, "com.android.music.musicservicecommand.pause"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v8, Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v1, p1, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {p1, v8, v1, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const v8, 0x7f0c0037

    invoke-virtual {v7, v8, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v1, Landroid/content/Intent;

    const-string v8, "com.android.music.musicservicecommand.next"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v8, Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v1, p1, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {p1, v8, v1, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const v8, 0x7f0c0038

    invoke-virtual {v7, v8, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v1, Landroid/content/Intent;

    const-string v8, "my.nullaction"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {p1, v8, v1, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const v8, 0x7f0c0033

    invoke-virtual {v7, v8, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    if-eqz p2, :cond_3

    const v8, 0x7f0c0034

    invoke-virtual {v7, v8, p2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    iput-object p2, p0, Lcom/android/music/MediaPlaybackService;->mAlbumArt:Landroid/graphics/Bitmap;

    :cond_3
    new-instance v4, Landroid/app/Notification;

    invoke-direct {v4}, Landroid/app/Notification;-><init>()V

    iput-object v7, v4, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    iget v8, v4, Landroid/app/Notification;->flags:I

    or-int/lit8 v8, v8, 0x2

    iput v8, v4, Landroid/app/Notification;->flags:I

    const v8, 0x7f02004c

    iput v8, v4, Landroid/app/Notification;->icon:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {p1, v8, v1, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    iput-object v8, v4, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    const/4 v8, 0x1

    invoke-virtual {p0, v8, v4}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    return-void
.end method

.method private wasRecentlyUsed(II)Z
    .locals 9
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x0

    if-nez p2, :cond_0

    move v5, v6

    :goto_0
    return v5

    :cond_0
    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v2, p2, :cond_1

    const-string v5, "MusicService"

    const-string v7, "lookback too big"

    invoke-static {v5, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    move p2, v2

    :cond_1
    add-int/lit8 v4, v2, -0x1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, p2, :cond_3

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    sub-int v7, v4, v3

    invoke-virtual {v5, v7}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v0, v5

    int-to-long v7, p1

    cmp-long v5, v0, v7

    if-nez v5, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v5, v6

    goto :goto_0
.end method


# virtual methods
.method public canUseAsRingtone()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->isDrmCanPlay(Landroid/database/Cursor;)Z

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public closeExternalStorageFiles(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    const-string v0, "com.android.music.queuechanged"

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    const-string v0, "com.android.music.metachanged"

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    const-string v0, "com.android.music.playstatechanged"

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/io/FileDescriptor;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " items in queue, currently at index "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v0, "Currently loaded:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getArtistName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "playing: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "actual: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-static {v1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->access$4400(Lcom/android/music/MediaPlaybackService$MultiPlayer;)Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "shuffle mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/android/music/MusicUtils;->debugDump(Ljava/io/PrintWriter;)V

    return-void
.end method

.method public duration()J
    .locals 5

    iget-wide v1, p0, Lcom/android/music/MediaPlaybackService;->mDurationOverride:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "duration from override is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/music/MediaPlaybackService;->mDurationOverride:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v1, p0, Lcom/android/music/MediaPlaybackService;->mDurationOverride:J

    :goto_0
    return-wide v1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const-string v2, "duration"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "duration from database is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->duration()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/music/MediaPlaybackService;->mDurationOverride:J

    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "duration from MediaPlayer is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/music/MediaPlaybackService;->mDurationOverride:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v1, p0, Lcom/android/music/MediaPlaybackService;->mDurationOverride:J

    goto :goto_0

    :cond_2
    const-wide/16 v1, 0x0

    goto :goto_0
.end method

.method public enqueue([JI)V
    .locals 3
    .param p1    # [J
    .param p2    # I

    const/4 v2, 0x2

    monitor-enter p0

    if-ne p2, v2, :cond_2

    :try_start_0
    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/music/MediaPlaybackService;->addToPlayList([JI)V

    const-string v0, "com.android.music.queuechanged"

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-gez v0, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->openCurrentAndNext()V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->play()V

    const-string v0, "com.android.music.metachanged"

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_1
    monitor-exit p0

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getRepeatMode()I

    move-result v0

    if-ne v0, v2, :cond_3

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setNextDataSource(Ljava/lang/String;)V

    :cond_3
    const v0, 0x7fffffff

    invoke-direct {p0, p1, v0}, Lcom/android/music/MediaPlaybackService;->addToPlayList([JI)V

    const-string v0, "com.android.music.queuechanged"

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    array-length v1, p1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->openCurrentAndNext()V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->play()V

    const-string v0, "com.android.music.metachanged"

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAlbumId()J
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    monitor-exit p0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const-string v2, "album_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAlbumName()Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const-string v2, "album"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getArtistId()J
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    monitor-exit p0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const-string v2, "artist_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const-string v2, "artist"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAudioId()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    aget-wide v0, v0, v1

    monitor-exit p0

    :goto_0
    return-wide v0

    :cond_0
    monitor-exit p0

    const-wide/16 v0, -0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAudioSessionId()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->getAudioSessionId()I

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMIMEType()Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const-string v2, "mime_type"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMediaMountedCount()I
    .locals 1

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mMediaMountedCount:I

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mFileToPlay:Ljava/lang/String;

    return-object v0
.end method

.method public getQueue()[J
    .locals 5

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    new-array v2, v1, [J

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    aget-wide v3, v3, v0

    aput-wide v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-object v2

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getQueuePosition()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getRepeatMode()I
    .locals 1

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    return v0
.end method

.method public getShuffleMode()I
    .locals 1

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    return v0
.end method

.method public getTrackName()Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const-string v2, "title"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public gotoNext(Z)V
    .locals 6
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    const-string v3, "MusicService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ">> gotoNext("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-gtz v3, :cond_0

    const-string v1, "MusicService"

    const-string v2, "No play queue"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_3

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-lt v3, v4, :cond_3

    :goto_1
    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->addPlayedTrackToHistory(Z)V

    invoke-direct {p0, p1}, Lcom/android/music/MediaPlaybackService;->getNextPosition(Z)I

    move-result v0

    if-gez v0, :cond_4

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->gotoIdleState()V

    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    const-string v1, "com.android.music.playstatechanged"

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_1
    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsPlaylistCompleted:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsPlaylistCompleted:Z

    const-string v1, "com.android.music.playbackcomplete"

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    const/high16 v2, 0x3f800000

    invoke-virtual {v1, v2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setVolume(F)V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    :try_start_1
    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->saveBookmarkIfNeeded()V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->openCurrentAndNext()V

    const-string v1, "com.android.music.metachanged"

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<< gotoNext("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public isCursorNull()Z
    .locals 1

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    return v0
.end method

.method public moveQueueItem(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v3}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setNextDataSource(Ljava/lang/String;)V

    :cond_0
    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-lt p1, v3, :cond_1

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/lit8 p1, v3, -0x1

    :cond_1
    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-lt p2, v3, :cond_2

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/lit8 p2, v3, -0x1

    :cond_2
    if-ge p1, p2, :cond_6

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    aget-wide v1, v3, p1

    move v0, p1

    :goto_0
    if-ge v0, p2, :cond_3

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    add-int/lit8 v5, v0, 0x1

    aget-wide v4, v4, v5

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    aput-wide v1, v3, p2

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-ne v3, p1, :cond_5

    iput p2, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    :cond_4
    :goto_1
    const-string v3, "com.android.music.queuechanged"

    invoke-direct {p0, v3}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    monitor-exit p0

    return-void

    :cond_5
    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-lt v3, p1, :cond_4

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-gt v3, p2, :cond_4

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_6
    if-ge p2, p1, :cond_4

    :try_start_1
    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    aget-wide v1, v3, p1

    move v0, p1

    :goto_2
    if-le v0, p2, :cond_7

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    add-int/lit8 v5, v0, -0x1

    aget-wide v4, v4, v5

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_7
    iget-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    aput-wide v1, v3, p2

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-ne v3, p1, :cond_8

    iput p2, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    goto :goto_1

    :cond_8
    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-lt v3, p2, :cond_4

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-gt v3, p1, :cond_4

    iget v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public notifyBTAvrcp(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mBinderAvrcp:Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->notifyBTAvrcp(Ljava/lang/String;)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 6
    .param p1    # Landroid/content/Intent;

    const/4 v5, 0x1

    const-string v0, "MediaPlaybackService"

    const-string v1, "intent %s stubname:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const-class v3, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-class v0, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MISC_AVRCP"

    const-string v1, "MediaPlayer returns IBTAvrcpMusic"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mBinderAvrcp:Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.android.music.IMediaPlaybackService"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "MISC_AVRCP"

    const-string v1, "MediaPlayer returns ServiceAvrcp inetrface"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mBinderAvrcp:Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-boolean v5, p0, Lcom/android/music/MediaPlaybackService;->mServiceInUse:Z

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mBinder:Landroid/os/IBinder;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v6, "MusicService"

    const-string v7, ">> onCreate"

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v10}, Landroid/app/Service;->stopForeground(Z)V

    const-string v6, "audio"

    invoke-virtual {p0, v6}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    iput-object v6, p0, Lcom/android/music/MediaPlaybackService;->mAudioManager:Landroid/media/AudioManager;

    new-instance v5, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-class v7, Lcom/android/music/MediaButtonIntentReceiver;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v6, v5}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    const-string v6, "Music"

    const/4 v7, 0x3

    invoke-virtual {p0, v6, v7}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    iput-object v6, p0, Lcom/android/music/MediaPlaybackService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v6, "storage"

    invoke-virtual {p0, v6}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/storage/StorageManager;

    iput-object v6, p0, Lcom/android/music/MediaPlaybackService;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/android/music/MusicUtils;->hasMountedSDcard(Landroid/content/Context;)Z

    move-result v1

    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onCreate: hasCard = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->registerExternalStorageListener()V

    new-instance v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-direct {v6, p0}, Lcom/android/music/MediaPlaybackService$MultiPlayer;-><init>(Lcom/android/music/MediaPlaybackService;)V

    iput-object v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-virtual {v6, v7}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setHandler(Landroid/os/Handler;)V

    invoke-direct {p0, v9}, Lcom/android/music/MediaPlaybackService;->sendSessionIdToAudioEffect(Z)V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->reloadQueue()V

    const-string v6, "com.android.music.queuechanged"

    invoke-direct {p0, v6}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    const-string v6, "com.android.music.metachanged"

    invoke-direct {p0, v6}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v6, "com.android.music.musicservicecommand"

    invoke-virtual {v0, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "com.android.music.musicservicecommand.togglepause"

    invoke-virtual {v0, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "com.android.music.musicservicecommand.pause"

    invoke-virtual {v0, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "com.android.music.musicservicecommand.next"

    invoke-virtual {v0, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "com.android.music.musicservicecommand.previous"

    invoke-virtual {v0, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v0, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-virtual {v0, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "com.android.music.attachauxaudioeffect"

    invoke-virtual {v0, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "com.android.music.detachauxaudioeffect"

    invoke-virtual {v0, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v6, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v8, p0, Lcom/android/music/MediaPlaybackService;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v6, v7, v9, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mSkinChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v6, v2}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v6, "power"

    invoke-virtual {p0, v6}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PowerManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v10, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v6

    iput-object v6, p0, Lcom/android/music/MediaPlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v6, v9}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const-wide/32 v7, 0xea60

    invoke-virtual {v6, v3, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v6, "MusicService"

    const-string v7, "<< onCreate"

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v1, "MusicService"

    const-string v2, ">> onDestroy"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MusicService"

    const-string v2, "Service being destroyed while still playing."

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->pause()V

    :cond_0
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/android/music/MediaPlaybackService;->mDurationOverride:J

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mAsyncAlbumArtWorker:Lcom/android/music/MediaPlaybackService$AlbumArtWorker;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mAsyncAlbumArtWorker:Lcom/android/music/MediaPlaybackService$AlbumArtWorker;

    invoke-virtual {v1, v4}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_1
    invoke-static {}, Lcom/android/music/MusicUtils;->resetStaticService()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.CLOSE_AUDIO_EFFECT_CONTROL_SESSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.media.extra.AUDIO_SESSION"

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getAudioSessionId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "android.media.extra.PACKAGE_NAME"

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->release()V

    iput-object v3, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iput-object v3, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    :cond_2
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v3, p0, Lcom/android/music/MediaPlaybackService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    :cond_3
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mSkinChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mReceiverUnregistered:Z

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v1, "MusicService"

    const-string v2, "<< onDestroy"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/music/MediaPlaybackService;->mServiceInUse:Z

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    iput p3, p0, Lcom/android/music/MediaPlaybackService;->mServiceStartId:I

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->isEventFromMonkey()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v6, "command"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onStartCommand "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/music/MusicUtils;->debugLog(Ljava/lang/Object;)V

    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onStartCommand: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "next"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "com.android.music.musicservicecommand.next"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/android/music/MusicUtils;->hasMountedSDcard(Landroid/content/Context;)Z

    move-result v4

    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onStartCommand hasCard = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_2

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackService;->mQuietMode:Z

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/android/music/MediaPlaybackService;->gotoNext(Z)V

    :cond_1
    :goto_0
    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v5

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const-wide/32 v7, 0xea60

    invoke-virtual {v6, v5, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v6, 0x1

    return v6

    :cond_2
    const-string v6, "com.android.music.quitplayback"

    invoke-direct {p0, v6}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v6, "previous"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    const-string v6, "com.android.music.musicservicecommand.previous"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    :cond_4
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackService;->mQuietMode:Z

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->prev()V

    goto :goto_0

    :cond_5
    const-string v6, "togglepause"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "com.android.music.musicservicecommand.togglepause"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_6
    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->isPlaying()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->pause()V

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackService;->mPausedByTransientLossOfFocus:Z

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->play()V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->isPlaying()Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackService;->mQuietMode:Z

    goto :goto_0

    :cond_8
    const-string v6, "pause"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    const-string v6, "com.android.music.musicservicecommand.pause"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    :cond_9
    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->pause()V

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackService;->mPausedByTransientLossOfFocus:Z

    goto :goto_0

    :cond_a
    const-string v6, "play"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->play()V

    goto :goto_0

    :cond_b
    const-string v6, "stop"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->pause()V

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackService;->mPausedByTransientLossOfFocus:Z

    const-wide/16 v6, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/android/music/MediaPlaybackService;->seek(J)J

    goto/16 :goto_0

    :cond_c
    const-string v6, "forward"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    const-string v6, "deltatime"

    const-wide/16 v7, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/music/MediaPlaybackService;->scanForward(J)V

    goto/16 :goto_0

    :cond_d
    const-string v6, "rewind"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "deltatime"

    const-wide/16 v7, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/music/MediaPlaybackService;->scanBackward(J)V

    goto/16 :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v4, 0x1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mServiceInUse:Z

    invoke-direct {p0, v4}, Lcom/android/music/MediaPlaybackService;->saveQueue(Z)V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mPausedByTransientLossOfFocus:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/music/MusicUtils;->hasBoundClient()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-gtz v1, :cond_2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const-wide/32 v2, 0xea60

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/android/music/MediaPlaybackService;->mServiceStartId:I

    invoke-virtual {p0, v1}, Landroid/app/Service;->stopSelf(I)V

    goto :goto_0
.end method

.method public open([JI)V
    .locals 10
    .param p1    # [J
    .param p2    # I

    monitor-enter p0

    :try_start_0
    iget v6, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    const/4 v6, 0x1

    iput v6, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    :cond_0
    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getAudioId()J

    move-result-wide v3

    array-length v1, p1

    const/4 v2, 0x1

    iget v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-ne v6, v1, :cond_1

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-wide v6, p1, v0

    iget-object v8, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    aget-wide v8, v8, v0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_4

    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_2

    const/4 v6, -0x1

    invoke-direct {p0, p1, v6}, Lcom/android/music/MediaPlaybackService;->addToPlayList([JI)V

    const-string v6, "com.android.music.queuechanged"

    invoke-direct {p0, v6}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_2
    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-ltz p2, :cond_5

    iput p2, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    :goto_1
    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->clear()V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->saveBookmarkIfNeeded()V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->openCurrentAndNext()V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getAudioId()J

    move-result-wide v6

    cmp-long v6, v3, v6

    if-eqz v6, :cond_3

    const-string v6, "com.android.music.metachanged"

    invoke-direct {p0, v6}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_3
    monitor-exit p0

    return-void

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mRand:Lcom/android/music/MediaPlaybackService$Shuffler;

    iget v7, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    invoke-virtual {v6, v7}, Lcom/android/music/MediaPlaybackService$Shuffler;->nextInt(I)I

    move-result v6

    iput v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    goto :goto_1

    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method public open(Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x1

    const/4 v6, 0x0

    monitor-enter p0

    :try_start_0
    const-string v2, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "open("

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ")"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    monitor-exit p0

    move v2, v6

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-nez v2, :cond_1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "content://media/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mCursorCols:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    iput-object v2, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_2
    :try_start_2
    iput-object p1, p0, Lcom/android/music/MediaPlaybackService;->mFileToPlay:Ljava/lang/String;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/music/MediaPlaybackService;->mMediaSeekable:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/music/MediaPlaybackService;->mIsPlaylistCompleted:Z

    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/music/MediaPlaybackService;->mTrackCompleted:Z

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mFileToPlay:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setDataSourceAsync(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/music/MediaPlaybackService;->mOpenFailedCounter:I

    monitor-exit p0

    move v2, v7

    goto :goto_0

    :cond_2
    invoke-static {p1}, Landroid/provider/MediaStore$Audio$Media;->getContentUriForPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v3, "_data=?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v4, v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :cond_3
    :try_start_3
    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/music/MediaPlaybackService;->ensurePlayListCapacity(I)V

    const/4 v2, 0x1

    iput v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/android/music/MediaPlaybackService;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    aput-wide v8, v2, v5

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I
    :try_end_3
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v2

    goto :goto_2

    :cond_4
    const/4 v2, 0x1

    :try_start_4
    invoke-direct {p0, v2}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    monitor-exit p0

    move v2, v6

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2
.end method

.method public pause()V
    .locals 5

    monitor-enter p0

    :try_start_0
    const-string v1, "MusicService"

    const-string v2, "pause"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    const/high16 v2, 0x3f800000

    invoke-virtual {v1, v2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setVolume(F)V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->pause()V

    :cond_0
    const/16 v0, 0x7d0

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->duration()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->position()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x7d0

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setNextDataSource(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->gotoIdleState()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    const-string v1, "com.android.music.playstatechanged"

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->saveBookmarkIfNeeded()V

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public play()V
    .locals 9

    monitor-enter p0

    const-wide/16 v2, 0x2710

    :try_start_0
    const-string v4, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ">> play: init="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v6}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", ready="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", listlen="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v6, 0x3

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v4

    if-nez v4, :cond_0

    const v4, 0x7f070001

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/music/MediaPlaybackService;->showToast(Ljava/lang/CharSequence;)V

    const-string v4, "MusicService"

    const-string v5, "<< play: phone call is ongoing, can not play music!"

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mAudioManager:Landroid/media/AudioManager;

    new-instance v5, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-class v7, Lcom/android/music/MediaButtonIntentReceiver;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mQuietMode:Z

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v4}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    if-eqz v4, :cond_6

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mIsPlaylistCompleted:Z

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v4}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->duration()J

    move-result-wide v0

    const-wide/16 v4, 0x2710

    invoke-direct {p0, v0, v1, v4, v5}, Lcom/android/music/MediaPlaybackService;->canGoToNext(JJ)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/android/music/MediaPlaybackService;->gotoNext(Z)V

    const-string v4, "com.android.music.playstatechanged"

    invoke-direct {p0, v4}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    const-string v4, "MusicService"

    const-string v5, "<< play: go to next song first"

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v4}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->start()V

    const-string v4, "MusicService"

    const-string v5, "MediaPlayer start done."

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mMediaplayerHandler:Landroid/os/Handler;

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v4, 0x0

    invoke-direct {p0, p0, v4}, Lcom/android/music/MediaPlaybackService;->updateNotification(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    iget-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    if-nez v4, :cond_2

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mIsSupposedToBePlaying:Z

    const-string v4, "com.android.music.playstatechanged"

    invoke-direct {p0, v4}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_2
    iget-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mWhetherAttachWhenPause:Z

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mAuxEffectId:I

    if-lez v4, :cond_5

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mAuxEffectId:I

    invoke-virtual {v4, v5}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->attachAuxEffect(I)V

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    const/high16 v5, 0x3f800000

    invoke-virtual {v4, v5}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setAuxEffectSendLevel(F)V

    :goto_1
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mWhetherAttachWhenPause:Z

    const-string v4, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Attach reverb when user start play again with mAuxEffectId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/music/MediaPlaybackService;->mAuxEffectId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-wide v4, p0, Lcom/android/music/MediaPlaybackService;->mPreAudioId:J

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getAudioId()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_4

    new-instance v4, Lcom/android/music/MediaPlaybackService$AlbumArtWorker;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/android/music/MediaPlaybackService$AlbumArtWorker;-><init>(Lcom/android/music/MediaPlaybackService;Lcom/android/music/MediaPlaybackService$1;)V

    iput-object v4, p0, Lcom/android/music/MediaPlaybackService;->mAsyncAlbumArtWorker:Lcom/android/music/MediaPlaybackService$AlbumArtWorker;

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mAsyncAlbumArtWorker:Lcom/android/music/MediaPlaybackService$AlbumArtWorker;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Long;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->getAlbumId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_4
    :goto_2
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/music/MediaPlaybackService;->mPreAudioId:J

    const-string v4, "MusicService"

    const-string v5, "<< play"

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    goto/16 :goto_0

    :cond_5
    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->attachAuxEffect(I)V

    goto :goto_1

    :cond_6
    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-gtz v4, :cond_4

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v4}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_4

    iget-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    if-nez v4, :cond_4

    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lcom/android/music/MediaPlaybackService;->setShuffleMode(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public position()J
    .locals 5

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->position()J

    move-result-wide v0

    const-string v2, "MusicService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public prev()V
    .locals 7

    const/4 v6, 0x1

    monitor-enter p0

    :try_start_0
    const-string v4, "MusicService"

    const-string v5, "prev"

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    if-ne v4, v6, :cond_3

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    add-int/lit8 v2, v1, -0x1

    :goto_1
    if-ltz v2, :cond_1

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-ge v4, v5, :cond_2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    const/4 v0, 0x1

    :cond_1
    const-string v4, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "prev: mPlayPos = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mHistory = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService;->mHistory:Ljava/util/Vector;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_4

    const-string v4, "MusicService"

    const-string v5, "prev with no valid position in history at shuffle mode!"

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_3
    :try_start_1
    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-lez v4, :cond_5

    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    :cond_4
    :goto_2
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mIsPrev:Z

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->saveBookmarkIfNeeded()V

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->openCurrentAndNext()V

    const-string v4, "com.android.music.metachanged"

    invoke-direct {p0, v4}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mIsPrev:Z

    monitor-exit p0

    goto :goto_0

    :cond_5
    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public registerExternalStorageListener()V
    .locals 2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/music/MediaPlaybackService$7;

    invoke-direct {v1, p0}, Lcom/android/music/MediaPlaybackService$7;-><init>(Lcom/android/music/MediaPlaybackService;)V

    iput-object v1, p0, Lcom/android/music/MediaPlaybackService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method public removeTrack(J)I
    .locals 7
    .param p1    # J

    const-string v4, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeTrack>>>: id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v4}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setNextDataSource(Ljava/lang/String;)V

    :cond_0
    monitor-enter p0

    :try_start_0
    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    add-int/lit8 v1, v4, -0x1

    :goto_0
    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    if-le v1, v4, :cond_2

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    aget-wide v4, v4, v1

    cmp-long v4, v4, p1

    if-nez v4, :cond_1

    const-string v4, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remove rewind("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "),Play position is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",removed num = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1, v1}, Lcom/android/music/MediaPlaybackService;->removeTracksInternal(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    iget v5, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-ge v4, v5, :cond_3

    iget v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    add-int/lit8 v2, v4, 0x1

    :goto_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_5

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayList:[J

    aget-wide v4, v4, v0

    cmp-long v4, v4, p1

    if-nez v4, :cond_4

    const-string v4, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remove forward("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "),Play position is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",removed num = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v0, v0}, Lcom/android/music/MediaPlaybackService;->removeTracksInternal(II)I

    move-result v4

    add-int/2addr v3, v4

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iget v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v3, :cond_6

    const-string v4, "com.android.music.queuechanged"

    invoke-direct {p0, v4}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_6
    const-string v4, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeTrack<<<: removed num = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v3

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public removeTracks(II)I
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setNextDataSource(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/music/MediaPlaybackService;->removeTracksInternal(II)I

    move-result v0

    if-lez v0, :cond_1

    const-string v1, "com.android.music.queuechanged"

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    :cond_1
    return v0
.end method

.method public seek(J)J
    .locals 9
    .param p1    # J

    const-wide/16 v7, 0x0

    const-wide/16 v2, -0x1

    const-string v4, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "seek("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v4}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->isInitialized()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mIsPlayerReady:Z

    if-eqz v4, :cond_1

    cmp-long v4, p1, v7

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/android/music/MediaPlaybackService;->mMediaSeekable:Z

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->mediaCanSeek()Z

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    const-string v4, "MusicService"

    const-string v5, "seek, sorry, seek is not supported"

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-wide v2

    :cond_2
    cmp-long v2, p1, v7

    if-gez v2, :cond_3

    const-wide/16 p1, 0x0

    :cond_3
    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->duration()J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_4

    move-wide p1, v0

    :goto_1
    iget-object v2, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v2, p1, p2}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->seek(J)J

    move-result-wide v2

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/music/MediaPlaybackService;->mIsPlaylistCompleted:Z

    goto :goto_1
.end method

.method public setAudioSessionId(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayer:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->setAudioSessionId(I)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setQueuePosition(I)V
    .locals 2
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    iput p1, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->openCurrentAndNext()V

    invoke-virtual {p0}, Lcom/android/music/MediaPlaybackService;->play()V

    const-string v0, "com.android.music.metachanged"

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->doAutoShuffleUpdate()V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setRepeatMode(I)V
    .locals 3
    .param p1    # I

    monitor-enter p0

    :try_start_0
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRepeatMode("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->setNextTrack()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->saveQueue(Z)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setShuffleMode(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x2

    monitor-enter p0

    :try_start_0
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setShuffleMode("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    if-lez v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mRepeatMode:I

    :cond_1
    iget v0, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    if-ne v0, v3, :cond_3

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->makeAutoShuffleList()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayListLen:I

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->doAutoShuffleUpdate()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mPlayPos:I

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackService;->openCurrentAndNext()V

    const-string v0, "com.android.music.metachanged"

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->notifyChange(Ljava/lang/String;)V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/android/music/MediaPlaybackService;->mShuffleMode:I

    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->saveQueue(Z)V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/music/MediaPlaybackService;->stop(Z)V

    return-void
.end method
