.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationPOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

.field private c:Z

.field private d:J

.field private e:Z

.field private f:Lcom/google/protobuf/ByteString;

.field private g:J


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->e:Z

    sget-object v0, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->f:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->q()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 4

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    :cond_1
    invoke-virtual {p1, v2, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->c:Z

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->d:J

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->f:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->g:J

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->e:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method static synthetic f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;-><init>()V

    return-object v0
.end method

.method private g()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->g()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->d:J

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v2

    if-eq v1, v2, :cond_7

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->h()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->j()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(J)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->l()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->n()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->p()J

    move-result-wide v0

    iget v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    iput-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->g:J

    goto :goto_0

    :cond_7
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    goto :goto_1
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    return-object p0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->c:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->g()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->f:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final b(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->e:Z

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->q()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->g()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->c:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Z)Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->d:J

    invoke-static {v2, v4, v5}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;J)J

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->e:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Z)Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->f:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-wide v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->g:J

    invoke-static {v2, v3, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;J)J

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;I)I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final e()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a:I

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->n()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->f:Lcom/google/protobuf/ByteString;

    return-object p0
.end method
