.class public final Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommandOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;


# instance fields
.field private b:I

.field private c:Z

.field private d:Ljava/util/List;

.field private e:Lcom/google/protobuf/ByteString;

.field private f:Z

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->a:Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    iput-boolean v2, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->c:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->d:Ljava/util/List;

    sget-object v1, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->e:Lcom/google/protobuf/ByteString;

    iput-boolean v2, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->f:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->g:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->h:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->g:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->h:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;-><init>(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->e:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method public static a([B)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->f:Z

    return p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->a:Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->c:Z

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->e:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->f:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_3
    return-void
.end method

.method public final c()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->h:I

    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->c:Z

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->e:Lcom/google/protobuf/ByteString;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v2, v0

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->f:Z

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v0

    add-int/2addr v2, v0

    :cond_3
    iput v2, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->c:Z

    return v0
.end method

.method public final g()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->d:Ljava/util/List;

    return-object v0
.end method

.method public final h()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->e:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final j()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->f:Z

    return v0
.end method

.method public final l()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->g:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->g:B

    goto :goto_0
.end method
