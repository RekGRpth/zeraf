.class public final Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncallOrBuilder;


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private d:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

.field private e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

.field private f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

.field private g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->q()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->b()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->b:J

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall$Builder;

    :cond_3
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_7

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall$Builder;

    :cond_6
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_3

    :sswitch_5
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_9

    move v0, v1

    :goto_4
    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;

    :cond_8
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    goto/16 :goto_0

    :cond_9
    move v0, v2

    goto :goto_4

    :sswitch_6
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_b

    move v0, v1

    :goto_5
    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    :cond_a
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto :goto_5

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;
    .locals 5

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v2

    if-eq v1, v2, :cond_5

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->f()J

    move-result-wide v2

    iget v4, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    iput-wide v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->b:J

    :cond_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v4

    if-eq v3, v4, :cond_6

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    :goto_0
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    :cond_1
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->j()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_7

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    move-result-object v4

    if-eq v3, v4, :cond_7

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    :goto_1
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    :cond_2
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->l()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_8

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    move-result-object v4

    if-eq v3, v4, :cond_8

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    :goto_2
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    :cond_3
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_9

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    move-result-object v4

    if-eq v3, v4, :cond_9

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    :goto_3
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    :cond_4
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->o()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->p()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v1

    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_a

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v3

    if-eq v2, v3, :cond_a

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    invoke-static {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    :goto_4
    iget v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    :cond_5
    return-object v0

    :cond_6
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    goto/16 :goto_0

    :cond_7
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    goto/16 :goto_1

    :cond_8
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    goto :goto_2

    :cond_9
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    goto :goto_3

    :cond_a
    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    goto :goto_4
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-wide v4, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->b:J

    invoke-static {v2, v4, v5}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;J)J

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StartDowncall;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;I)I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$AckDowncall;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$RegistrationDowncall;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$StopDowncall;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->a:I

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall;->q()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ClientDowncall$Builder;

    move-result-object v0

    return-object v0
.end method
