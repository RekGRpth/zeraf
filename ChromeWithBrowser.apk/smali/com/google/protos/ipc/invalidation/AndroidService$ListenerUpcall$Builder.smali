.class public final Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcallOrBuilder;


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private d:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

.field private e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

.field private f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

.field private g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

.field private h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

.field private i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->u()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->b()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->b:J

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall$Builder;

    :cond_3
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    :sswitch_4
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    :cond_5
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_3

    :sswitch_5
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;

    :cond_7
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto :goto_4

    :sswitch_6
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    :cond_9
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto :goto_5

    :sswitch_7
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_c

    move v0, v1

    :goto_6
    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;

    :cond_b
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto :goto_6

    :sswitch_8
    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_e

    move v0, v1

    :goto_7
    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    :cond_d
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    goto/16 :goto_0

    :cond_e
    move v0, v2

    goto :goto_7

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 5

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v2

    if-eq v1, v2, :cond_7

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->f()J

    move-result-wide v2

    iget v4, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    iput-wide v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->b:J

    :cond_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_8

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v4

    if-eq v3, v4, :cond_8

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    :goto_0
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    :cond_1
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->j()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_9

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    move-result-object v4

    if-eq v3, v4, :cond_9

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    :goto_1
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    :cond_2
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->l()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_a

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v4

    if-eq v3, v4, :cond_a

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    :goto_2
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    :cond_3
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->n()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_b

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    move-result-object v4

    if-eq v3, v4, :cond_b

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    :goto_3
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    :cond_4
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->o()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->p()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_c

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v4

    if-eq v3, v4, :cond_c

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    :goto_4
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    :cond_5
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->q()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->r()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_d

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    move-result-object v4

    if-eq v3, v4, :cond_d

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    :goto_5
    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    :cond_6
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->s()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->t()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v1

    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_e

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v3

    if-eq v2, v3, :cond_e

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    invoke-static {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall$Builder;->d()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    :goto_6
    iget v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    :cond_7
    return-object v0

    :cond_8
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    goto/16 :goto_0

    :cond_9
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    goto/16 :goto_1

    :cond_a
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    goto/16 :goto_2

    :cond_b
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    goto/16 :goto_3

    :cond_c
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    goto :goto_4

    :cond_d
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    goto :goto_5

    :cond_e
    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    goto :goto_6
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-wide v4, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->b:J

    invoke-static {v2, v4, v5}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;J)J

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;I)I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->i:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ErrorUpcall;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$InvalidateUpcall;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->d:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReadyUpcall;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->g:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationFailureUpcall;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$RegistrationStatusUpcall;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->h:Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$ReissueRegistrationsUpcall;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->a:I

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall;->u()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$ListenerUpcall$Builder;

    move-result-object v0

    return-object v0
.end method
