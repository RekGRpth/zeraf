.class public final Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientStateOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

.field private d:Lcom/google/protobuf/ByteString;

.field private e:Lcom/google/protobuf/ByteString;

.field private f:Z

.field private g:J

.field private h:Z

.field private i:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

.field private j:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

.field private k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

.field private l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

.field private m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

.field private n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

.field private o:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

.field private p:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

.field private q:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

.field private r:B

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$RunStateP;->d()Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->c:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    sget-object v1, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->d:Lcom/google/protobuf/ByteString;

    sget-object v1, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->e:Lcom/google/protobuf/ByteString;

    iput-boolean v3, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->f:Z

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->g:J

    iput-boolean v3, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->h:Z

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->i:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->o:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->d()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->p:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->q:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->r:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->s:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->r:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->s:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;-><init>(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->g:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->d:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->p:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/Client$RunStateP;)Lcom/google/protos/ipc/invalidation/Client$RunStateP;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->c:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    return-object p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->i:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;)Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;)Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->q:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->e:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->h:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->a:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    return-object v0
.end method

.method static synthetic d(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object p1
.end method

.method static synthetic e(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->o:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object p1
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->e()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState$Builder;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final B()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object v0
.end method

.method public final C()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final D()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->o:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object v0
.end method

.method public final E()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final F()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->p:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    return-object v0
.end method

.method public final G()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final H()Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->q:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    return-object v0
.end method

.method public final I()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->r:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->r:B

    goto :goto_0
.end method

.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->c:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->d:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->e:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->f:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->g:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->a(IJ)V

    :cond_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->i:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_6
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_7
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_8
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_9
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_a
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_b
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->o:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_c
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->p:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_d
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_e

    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->q:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_e
    return-void
.end method

.method public final c()I
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->s:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->c:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->d:Lcom/google/protobuf/ByteString;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->e:Lcom/google/protobuf/ByteString;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->f:Z

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->g:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->h:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->i:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->n:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_d

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->o:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_e

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->p:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_f

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->q:Lcom/google/protos/ipc/invalidation/JavaClient$StatisticsState;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->s:I

    goto/16 :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/Client$RunStateP;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->c:Lcom/google/protos/ipc/invalidation/Client$RunStateP;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->d:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->e:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->f:Z

    return v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->g:J

    return-wide v0
.end method

.method public final o()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->h:Z

    return v0
.end method

.method public final q()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->i:Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    return-object v0
.end method

.method public final s()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->j:Lcom/google/protos/ipc/invalidation/JavaClient$RegistrationManagerStateP;

    return-object v0
.end method

.method public final u()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->k:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object v0
.end method

.method public final w()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->l:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object v0
.end method

.method public final y()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->b:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->m:Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    return-object v0
.end method
