.class public final Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatusOrBuilder;


# instance fields
.field private a:I

.field private b:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->g()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->b:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->f()Z

    move-result v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->a:I

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->b:Z

    goto :goto_0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->b:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;Z)Z

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus;I)I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$NetworkStatus$Builder;

    move-result-object v0

    return-object v0
.end method
