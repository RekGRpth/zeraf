.class public final Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

.field private d:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

.field private e:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->a:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->d:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;->d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->e:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->f:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->g:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->f:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->g:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b:I

    return p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->e:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->d:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->a:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->d:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->e:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->d:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->e:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->g:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->d:Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->e:Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    return-object v0
.end method

.method public final k()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->f:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->f:B

    goto :goto_0
.end method
