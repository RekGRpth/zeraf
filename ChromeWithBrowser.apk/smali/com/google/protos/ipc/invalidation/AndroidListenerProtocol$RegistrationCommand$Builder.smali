.class public final Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommandOrBuilder;


# instance fields
.field private a:I

.field private b:Z

.field private c:Ljava/util/List;

.field private d:Lcom/google/protobuf/ByteString;

.field private e:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c:Ljava/util/List;

    sget-object v0, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->d:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->l()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->b:Z

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->d:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->e:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;
    .locals 4

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->d()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v2

    if-eq v1, v2, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->f()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    :cond_0
    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c:Ljava/util/List;

    iget v2, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->h()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->i()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    :cond_2
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->b(Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    :cond_3
    return-object v0

    :cond_4
    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->g()V

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;-><init>(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->b:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;Z)Z

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c:Ljava/util/List;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;Ljava/util/List;)Ljava/util/List;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->d:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->e:Z

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;Z)Z

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->a(Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;I)I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private g()V
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->g()V

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->b:Z

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->d:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final b(Z)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->a:I

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->e:Z

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->f()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->l()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand$Builder;

    move-result-object v0

    return-object v0
.end method
