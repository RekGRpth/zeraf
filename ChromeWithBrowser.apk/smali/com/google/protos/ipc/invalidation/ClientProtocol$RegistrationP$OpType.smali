.class public final enum Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# static fields
.field public static final enum a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

.field public static final enum b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

.field private static final synthetic d:[Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    const-string v1, "REGISTER"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    const-string v1, "UNREGISTER"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    new-array v0, v4, [Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->d:[Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType$1;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType$1;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->c:I

    return-void
.end method

.method public static a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a()[Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->d:[Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    invoke-virtual {v0}, [Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;

    return-object v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;->c:I

    return v0
.end method
