.class public final Lcom/google/protos/ipc/invalidation/Types$ClientType;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/Types$ClientTypeOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/Types$ClientType;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/Types$ClientType;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Types$ClientType;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->a:Lcom/google/protos/ipc/invalidation/Types$ClientType;

    sget-object v1, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->d:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->e:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->d:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->e:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/Types$ClientType;-><init>(Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Types$ClientType;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Types$ClientType;Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;)Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/Types$ClientType;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->a:Lcom/google/protos/ipc/invalidation/Types$ClientType;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;->c()Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/Types$ClientType;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Types$ClientType;->newBuilder()Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;->a(Lcom/google/protos/ipc/invalidation/Types$ClientType$Builder;)Lcom/google/protos/ipc/invalidation/Types$ClientType;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Types$ClientType;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->b(II)V

    :cond_0
    return-void
.end method

.method public final c()I
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;->a()I

    move-result v0

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iput v0, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->e:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->c:Lcom/google/protos/ipc/invalidation/Types$ClientType$Type;

    return-object v0
.end method

.method public final g()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->d:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/Types$ClientType;->d:B

    goto :goto_0
.end method
