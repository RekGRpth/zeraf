.class public final Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncodingOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->a:Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->b:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->c:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->b:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;-><init>(Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;)V

    return-void
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->a:Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;->c()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->newBuilder()Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;->a(Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding$Builder;)Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->c()I

    return-void
.end method

.method public final c()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->c:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iput v1, p0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->c:I

    move v0, v1

    goto :goto_0
.end method

.method public final e()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->b:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ChannelCommon$ChannelMessageEncoding;->b:B

    goto :goto_0
.end method
