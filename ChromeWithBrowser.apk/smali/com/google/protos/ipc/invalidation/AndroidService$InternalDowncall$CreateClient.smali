.class public final Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClientOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;


# instance fields
.field private b:I

.field private c:I

.field private d:Lcom/google/protobuf/ByteString;

.field private e:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

.field private f:Z

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->a:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    iput v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->c:I

    sget-object v1, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->d:Lcom/google/protobuf/ByteString;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    iput-boolean v2, v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->f:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->g:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->h:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->g:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->h:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;-><init>(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->c:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->d:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    return p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->a:Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->e()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient$Builder;)Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->d:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->f:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_3
    return-void
.end method

.method public final c()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->c:I

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->d:Lcom/google/protobuf/ByteString;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->f:Z

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->h:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->c:I

    return v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->d:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->f:Z

    return v0
.end method

.method public final m()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->g:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/AndroidService$InternalDowncall$CreateClient;->g:B

    goto :goto_0
.end method
