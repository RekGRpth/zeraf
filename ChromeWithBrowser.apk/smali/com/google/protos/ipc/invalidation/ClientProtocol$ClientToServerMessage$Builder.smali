.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessageOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

.field private e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

.field private f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

.field private g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->q()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    :cond_1
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->d()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-virtual {v0, v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    :cond_4
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_6

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;

    :cond_5
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_2

    :sswitch_4
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_8

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;

    :cond_7
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    move-result-object v0

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v0, v2

    goto :goto_3

    :cond_9
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    goto/16 :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_b

    move v0, v1

    :goto_4
    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;

    :cond_a
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto :goto_4

    :sswitch_6
    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v3

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_d

    move v0, v1

    :goto_5
    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-virtual {v3, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    :cond_c
    invoke-virtual {p1, v3, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    goto/16 :goto_0

    :cond_d
    move v0, v2

    goto :goto_5

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 5

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;

    move-result-object v1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;

    move-result-object v2

    if-eq v1, v2, :cond_5

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    move-result-object v4

    if-eq v3, v4, :cond_6

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    :goto_0
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    :cond_0
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_7

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v4

    if-eq v3, v4, :cond_7

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    :goto_1
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    :cond_1
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_8

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    move-result-object v4

    if-eq v3, v4, :cond_8

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    :goto_2
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    :cond_2
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->l()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_9

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    move-result-object v4

    if-eq v3, v4, :cond_9

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    :goto_3
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    :cond_3
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->n()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_a

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v4

    if-eq v3, v4, :cond_a

    iget-object v3, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    :goto_4
    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    :cond_4
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->o()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->p()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v1

    iget v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_b

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v3

    if-eq v2, v3, :cond_b

    iget-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-static {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    :goto_5
    iget v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    :cond_5
    return-object v0

    :cond_6
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    goto/16 :goto_0

    :cond_7
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    goto/16 :goto_1

    :cond_8
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    goto/16 :goto_2

    :cond_9
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    goto :goto_3

    :cond_a
    iput-object v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    goto :goto_4

    :cond_b
    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    goto :goto_5
.end method

.method private g()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;I)I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;
    .locals 2

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->g()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->q()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
