.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdPOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;


# instance fields
.field private b:I

.field private c:I

.field private d:Lcom/google/protobuf/ByteString;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->c:I

    sget-object v1, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->d:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->e:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->f:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->e:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->f:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->c:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->d:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->b:I

    return p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->d:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_1
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->c:I

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->d:Lcom/google/protobuf/ByteString;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->f:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->c:I

    return v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->d:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final i()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->e:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->e:B

    goto :goto_0
.end method
