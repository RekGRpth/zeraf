.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitPOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->c:I

    iput v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->d:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->e:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->f:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->e:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->f:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->c:I

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->d:I

    return p1
.end method

.method static synthetic c(Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->b:I

    return p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->d:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(II)V

    :cond_1
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->c:I

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->d:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->f:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->c:I

    return v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->d:I

    return v0
.end method

.method public final i()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->e:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$RateLimitP;->e:B

    goto :goto_0
.end method
