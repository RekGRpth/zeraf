.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationPOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

.field private d:Z

.field private e:J

.field private f:Z

.field private g:Lcom/google/protobuf/ByteString;

.field private h:J

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/16 v2, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->d:Z

    iput-wide v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->e:J

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->f:Z

    sget-object v1, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->g:Lcom/google/protobuf/ByteString;

    iput-wide v2, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->h:J

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->i:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->j:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->i:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->j:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->e:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->g:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->h:J

    return-wide p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->f:Z

    return p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->d:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->a(IJ)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->g:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->h:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->a(IJ)V

    :cond_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IZ)V

    :cond_5
    return-void
.end method

.method public final c()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->d:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->g:Lcom/google/protobuf/ByteString;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->h:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->f:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->j:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->d:Z

    return v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->e:J

    return-wide v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->f:Z

    return v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->g:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final o()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()J
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->h:J

    return-wide v0
.end method

.method public final q()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->i:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->i:B

    goto :goto_0
.end method

.method public final r()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP$Builder;

    move-result-object v0

    return-object v0
.end method
