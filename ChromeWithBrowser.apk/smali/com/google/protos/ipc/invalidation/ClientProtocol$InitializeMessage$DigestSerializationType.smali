.class public final enum Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# static fields
.field public static final enum a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

.field private static enum b:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    const-string v1, "BYTE_BASED"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    const-string v1, "NUMBER_BASED"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    new-array v0, v4, [Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    aput-object v1, v0, v2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType$1;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType$1;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->c:I

    return-void
.end method

.method public static a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->c:I

    return v0
.end method
