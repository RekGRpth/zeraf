.class public final Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerStateOrBuilder;


# instance fields
.field private a:I

.field private b:I

.field private c:J

.field private d:J

.field private e:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->e:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->m()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;
    .locals 3

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->c()I

    move-result v0

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->b:I

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->c:J

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->d:J

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    move-result-object v1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->e:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->a(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method static synthetic e()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;-><init>()V

    return-object v0
.end method

.method private f()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->f()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    iput p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->b:I

    return-object p0
.end method

.method public final a(J)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->c:J

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->e:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    return-object p0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a(I)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->h()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a(J)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->j()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->b(J)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->l()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    move-result-object v0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->e:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->d()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    move-result-object v2

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->e:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->e:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    :goto_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    goto :goto_0

    :cond_5
    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->e:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    goto :goto_1
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->f()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->d:J

    return-object p0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->d()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->m()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->f()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;-><init>(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->b:I

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;I)I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->c:J

    invoke-static {v2, v4, v5}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;J)J

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->d:J

    invoke-static {v2, v4, v5}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->b(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;J)J

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->e:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->a(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->b(Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;I)I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
