.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessageOrBuilder;


# instance fields
.field private a:I

.field private b:Lcom/google/protobuf/ByteString;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->b:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->g()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->a()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->a:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->e()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->b:Lcom/google/protobuf/ByteString;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic d()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;
    .locals 1

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;-><init>()V

    return-object v0
.end method

.method private e()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->f()Lcom/google/protobuf/ByteString;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->a:I

    iput-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->b:Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->c(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-direct {v2, p0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;B)V

    iget v3, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->b:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    invoke-static {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;I)I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage$Builder;

    move-result-object v0

    return-object v0
.end method
