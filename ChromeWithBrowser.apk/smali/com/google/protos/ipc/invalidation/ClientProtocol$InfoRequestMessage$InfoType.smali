.class public final enum Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# static fields
.field public static final enum a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;


# instance fields
.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;

    const-string v1, "GET_PERFORMANCE_COUNTERS"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;

    new-array v0, v3, [Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;

    aput-object v1, v0, v2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType$1;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType$1;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;->b:I

    return-void
.end method

.method public static a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage$InfoType;->b:I

    return v0
.end method
