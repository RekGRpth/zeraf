.class public final Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/Client$PersistentTiclStateOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;


# instance fields
.field private b:I

.field private c:Lcom/google/protobuf/ByteString;

.field private d:J

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->a:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    sget-object v1, Lcom/google/protobuf/ByteString;->a:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->c:Lcom/google/protobuf/ByteString;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->d:J

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->e:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->f:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->e:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->f:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;-><init>(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->d:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->c:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->a:Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->e()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->newBuilder()Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState$Builder;)Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->c:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ByteString;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->d:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(IJ)V

    :cond_1
    return-void
.end method

.method public final c()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->c:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-wide v1, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->d:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->f:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->c:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->d:J

    return-wide v0
.end method

.method public final i()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->e:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/Client$PersistentTiclState;->e:B

    goto :goto_0
.end method
