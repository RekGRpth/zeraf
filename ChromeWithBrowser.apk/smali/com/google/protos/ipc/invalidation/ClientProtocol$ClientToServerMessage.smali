.class public final Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessageOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;


# instance fields
.field private b:I

.field private c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

.field private d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

.field private e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

.field private f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

.field private g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

.field private h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->i:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->j:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->i:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->j:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->e()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->c()I

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_5
    return-void
.end method

.method public final c()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->j:I

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationMessage;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncMessage;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    return-object v0
.end method

.method public final q()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->i:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->i:B

    goto :goto_0
.end method
