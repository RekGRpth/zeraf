.class public final Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protos/ipc/invalidation/JavaClient$BatcherStateOrBuilder;


# static fields
.field private static final a:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;


# instance fields
.field private b:I

.field private c:Ljava/util/List;

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;

.field private g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

.field private h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    invoke-direct {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;-><init>()V

    sput-object v0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->a:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->c:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->d:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->e:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->f:Ljava/util/List;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->i:B

    iput v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->j:I

    return-void
.end method

.method private constructor <init>(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->i:B

    iput v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->j:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;-><init>(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;I)I
    .locals 0

    iput p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->b:I

    return p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    return-object p1
.end method

.method public static a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->c:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->e:Ljava/util/List;

    return-object p1
.end method

.method public static d()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;
    .locals 1

    sget-object v0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->a:Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    return-object v0
.end method

.method static synthetic d(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->f:Ljava/util/List;

    return-object p1
.end method

.method static synthetic e(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->f:Ljava/util/List;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->e()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;
    .locals 1

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->a(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;

    invoke-static {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState$Builder;)Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->c()I

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_2
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/MessageLite;)V

    :cond_5
    return-void
.end method

.method public final c()I
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->j:I

    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    :goto_0
    return v3

    :cond_0
    move v1, v2

    move v3, v2

    :goto_1
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_2
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v6, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    move v1, v2

    :goto_3
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    :goto_4
    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v1, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_4
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_5

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    :cond_5
    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v3, v0

    :cond_6
    iput v3, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->j:I

    goto/16 :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->c:Ljava/util/List;

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->d:Ljava/util/List;

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->e:Ljava/util/List;

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->f:Ljava/util/List;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->b:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->h:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    return-object v0
.end method

.method public final m()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->i:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;->i:B

    goto :goto_0
.end method
