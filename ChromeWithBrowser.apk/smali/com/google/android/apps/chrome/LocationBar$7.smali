.class Lcom/google/android/apps/chrome/LocationBar$7;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$7;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$7;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mClearSuggestionsOnDismiss:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$2500(Lcom/google/android/apps/chrome/LocationBar;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$7;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->hideSuggestions()V

    goto :goto_0
.end method
