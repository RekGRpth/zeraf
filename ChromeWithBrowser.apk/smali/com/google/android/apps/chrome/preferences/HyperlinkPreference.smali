.class public Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;
.super Landroid/preference/Preference;


# instance fields
.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcom/google/android/apps/chrome/R$styleable;->HyperlinkPreference:[I

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->mUrl:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->changeViewStyle(Landroid/view/View;Landroid/content/Context;)V

    return-void
.end method

.method protected onClick()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->mUrl:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->prepareUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->show(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected prepareUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    return-object p1
.end method
