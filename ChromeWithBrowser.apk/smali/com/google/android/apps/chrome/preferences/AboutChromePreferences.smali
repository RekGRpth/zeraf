.class public Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const/high16 v0, 0x7f060000

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->addPreferencesFromResource(I)V

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getAboutVersionStrings()Ljava/util/HashMap;

    move-result-object v1

    const-string v0, "application_version"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    const-string v0, "application_version"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$VersionListener;-><init>(Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;Lcom/google/android/apps/chrome/preferences/AboutChromePreferences$1;)V

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string v0, "webkit_version"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    const-string v0, "webkit_version"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    const-string v0, "javascript_version"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    const-string v0, "javascript_version"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    const-string v0, "os_version"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    const-string v0, "os_version"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    const-string v0, "executable_path"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getExecutablePathValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getExecutablePathValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    const-string v0, "profile_path"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getProfilePathValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getProfilePathValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method
