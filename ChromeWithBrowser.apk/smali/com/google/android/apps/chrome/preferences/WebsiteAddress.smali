.class public Lcom/google/android/apps/chrome/preferences/WebsiteAddress;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# static fields
.field private static final ANY_SUBDOMAIN_PATTERN:Ljava/lang/String; = "[*.]"

.field private static final HTTP_SCHEME:Ljava/lang/String; = "http"

.field private static final SCHEME_SUFFIX:Ljava/lang/String; = "://"


# instance fields
.field private final mHost:Ljava/lang/String;

.field private final mOmitProtocolAndPort:Z

.field private final mOrigin:Ljava/lang/String;

.field private final mScheme:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mScheme:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mHost:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOmitProtocolAndPort:Z

    return-void
.end method

.method public static create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/WebsiteAddress;
    .locals 9

    const/4 v8, -0x1

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, v2

    :goto_0
    return-object v0

    :cond_1
    const-string v1, "[*.]"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    const/4 v3, 0x4

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v2, v3, v0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    const-string v1, "://"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v8, :cond_5

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v1, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->trimTrailingBackslash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    const-string v6, "http"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v2}, Landroid/net/Uri;->getPort()I

    move-result v6

    if-eq v6, v8, :cond_3

    invoke-virtual {v2}, Landroid/net/Uri;->getPort()I

    move-result v2

    const/16 v6, 0x50

    if-ne v2, v6, :cond_4

    :cond_3
    :goto_1
    invoke-direct {v1, v3, v4, v5, v0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    new-instance v1, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    invoke-direct {v1, v2, v2, p0, v0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0
.end method

.method private getDomainAndRegistry()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/URLUtilities;->getDomainAndRegistry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/URLUtilities;->getDomainAndRegistry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getSubdomainsList()[Ljava/lang/String;
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-array v0, v2, [Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    add-int/lit8 v1, v0, 0x3

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getDomainAndRegistry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le v3, v1, :cond_2

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mHost:Ljava/lang/String;

    move v1, v2

    goto :goto_1

    :cond_2
    new-array v0, v2, [Ljava/lang/String;

    goto :goto_0
.end method

.method private static trimTrailingBackslash(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)I
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getDomainAndRegistry()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getDomainAndRegistry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    move v2, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mScheme:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iget-object v3, p1, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mScheme:Ljava/lang/String;

    if-nez v3, :cond_3

    move v2, v1

    :cond_3
    if-eq v0, v2, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mScheme:Ljava/lang/String;

    if-nez v0, :cond_5

    const/4 v2, -0x1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mScheme:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mScheme:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mScheme:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getSubdomainsList()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {p1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->getSubdomainsList()[Ljava/lang/String;

    move-result-object v5

    array-length v0, v4

    add-int/lit8 v1, v0, -0x1

    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v1, :cond_8

    if-ltz v0, :cond_8

    add-int/lit8 v3, v1, -0x1

    aget-object v2, v4, v1

    add-int/lit8 v1, v0, -0x1

    aget-object v0, v5, v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    move v1, v3

    goto :goto_2

    :cond_8
    sub-int v2, v1, v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->compareTo(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->compareTo(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mHost:Ljava/lang/String;

    return-object v0
.end method

.method public getOrigin()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOmitProtocolAndPort:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    goto :goto_0
.end method

.method public getPattern()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[*.]"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOmitProtocolAndPort:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mHost:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mScheme:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mHost:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mOrigin:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mScheme:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->mHost:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method
