.class public Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final PREF_DO_NOT_TRACK:Ljava/lang/String; = "do_not_track"

.field private static final PRIVACY_HELP_URL:Ljava/lang/String; = "https://support.google.com/chrome/?p=mobile_privacy"

.field public static final SHOW_CLEAR_BROWSING_DATA_EXTRA:Ljava/lang/String; = "ShowClearBrowsingData"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method private showClearBrowsingDialog()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f060012

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isMobileNetworkCapable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "crash_dump_upload_no_cellular"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_0
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "navigation_error"

    aput-object v0, v2, v1

    const-string v0, "search_suggestions"

    aput-object v0, v2, v5

    const/4 v0, 0x2

    const-string v3, "network_predictions"

    aput-object v3, v2, v0

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const-string v0, "crash_dump_upload"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "ShowClearBrowsingData"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->showClearBrowsingDialog()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->updateSummaries()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->showClearBrowsingDialog()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0700c9

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "https://support.google.com/chrome/?p=mobile_privacy"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->show(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0f010d
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPreferenceChanged(Landroid/preference/Preference;Ljava/lang/Object;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    const v0, 0x7f0f010d

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700f5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const v0, 0x7f0f010e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->updateSummaries()V

    return-void
.end method

.method public updateSummaries()V
    .locals 3

    const-string v0, "do_not_track"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isDoNotTrackEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07024e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07024f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
