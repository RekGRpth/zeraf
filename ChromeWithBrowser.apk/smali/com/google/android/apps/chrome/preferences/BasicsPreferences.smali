.class public Lcom/google/android/apps/chrome/preferences/BasicsPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;


# static fields
.field private static final PREF_AUTOFILL_SETTINGS:Ljava/lang/String; = "autofill_settings"

.field public static final PREF_CATEGORY_SYNC_ACCOUNT:Ljava/lang/String; = "sync_account"

.field private static final PREF_MANAGE_SAVED_PASSWORDS:Ljava/lang/String; = "manage_saved_passwords"

.field private static final PREF_SEARCH_ENGINE:Ljava/lang/String; = "search_engine"


# instance fields
.field private mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public addAccountHeader()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string v3, "sync_account"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->setKey(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-class v2, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->setFragment(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updatePreferences()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updatePreferences()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->addAccountHeader()V

    return-void
.end method

.method public updatePreferences()V
    .locals 1

    const v0, 0x7f060004

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updateSummaries()V

    return-void
.end method

.method public updateSummaries()V
    .locals 7

    const v3, 0x7f07024f

    const v2, 0x7f07024e

    const-string v0, "autofill_settings"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAutofillEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    const-string v0, "manage_saved_passwords"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isRememberPasswordsEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    :goto_1
    const-string v0, "search_engine"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefSearchEnginePreference()I

    move-result v3

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getLocalizedSearchEngines()[Ljava/util/HashMap;

    move-result-object v4

    array-length v5, v4

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    const-string v1, "searchEngineId"

    invoke-virtual {v6, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ne v3, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getSearchEngineNameAndDomain(Landroid/content/Context;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2
.end method
