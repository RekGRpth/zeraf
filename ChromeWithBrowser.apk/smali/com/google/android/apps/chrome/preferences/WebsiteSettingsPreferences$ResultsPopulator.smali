.class Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;
.super Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$CallChain;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;-><init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)V

    return-void
.end method


# virtual methods
.method run()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->next()V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    # getter for: Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByOrigin:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->access$900(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/Website;

    new-instance v5, Lcom/google/android/apps/chrome/preferences/WebsitePreference;

    iget-object v6, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/preferences/Website;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    # getter for: Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->mSitesByHost:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->access$1000(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/Website;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    new-instance v5, Lcom/google/android/apps/chrome/preferences/WebsitePreference;

    iget-object v6, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Lcom/google/android/apps/chrome/preferences/WebsitePreference;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/preferences/Website;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/WebsitePreference;

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->displayEmptyScreenMessage()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->access$1100(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$ResultsPopulator;->next()V

    goto/16 :goto_0
.end method
