.class public Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PasswordListObserver;


# static fields
.field private static final PREF_CATEGORY_EXCEPTIONS:Ljava/lang/String; = "exceptions"

.field private static final PREF_CATEGORY_SAVED_PASSWORDS:Ljava/lang/String; = "saved_passwords"


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mEmptyView:Landroid/widget/TextView;

.field private mPasswordEnabler:Lcom/google/android/apps/chrome/preferences/PasswordEnabler;

.field private mPasswordsAvailable:Z

.field private mRequestIsLive:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mPasswordsAvailable:Z

    return-void
.end method

.method private displayEmptyScreenMessage()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mEmptyView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mEmptyView:Landroid/widget/TextView;

    const v1, 0x7f0700f2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mEmptyView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    new-instance v0, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->addSwitchToActionBar(Landroid/widget/Switch;)V

    new-instance v0, Lcom/google/android/apps/chrome/preferences/PasswordEnabler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/PasswordEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mPasswordEnabler:Lcom/google/android/apps/chrome/preferences/PasswordEnabler;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mPasswordEnabler:Lcom/google/android/apps/chrome/preferences/PasswordEnabler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/PasswordEnabler;->attach()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mPasswordEnabler:Lcom/google/android/apps/chrome/preferences/PasswordEnabler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/PasswordEnabler;->destroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setVisibility(I)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->rebuildPasswordLists()V

    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->rebuildPasswordLists()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onStop()V

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->stopPasswordListRequest()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mRequestIsLive:Z

    return-void
.end method

.method public passwordExceptionListAvailable(I)V
    .locals 7

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mPasswordsAvailable:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->displayEmptyScreenMessage()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mPasswordsAvailable:Z

    new-instance v2, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    const-string v0, "exceptions"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->setKey(Ljava/lang/String;)V

    const v0, 0x7f0700f0

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p1, :cond_0

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSavedPasswordException(I)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v4

    const-string v0, "url"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    const-string v0, "com.google.android.apps.chrome.PasswordPrefEdit"

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->setFragment(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "url"

    const-string v0, "url"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "id"

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public passwordListAvailable(I)V
    .locals 7

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mPasswordsAvailable:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->displayEmptyScreenMessage()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mPasswordsAvailable:Z

    new-instance v2, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    const-string v0, "saved_passwords"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->setKey(Ljava/lang/String;)V

    const v0, 0x7f0700ef

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p1, :cond_0

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSavedNamePassword(I)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v4

    const-string v0, "url"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    const-string v0, "com.google.android.apps.chrome.PasswordPrefEdit"

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->setFragment(Ljava/lang/String;)V

    const-string v0, "name"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "name"

    const-string v0, "name"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "url"

    const-string v0, "url"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "id"

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method rebuildPasswordLists()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mRequestIsLive:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->stopPasswordListRequest()V

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->startPasswordListRequest(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PasswordListObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->mRequestIsLive:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManageSavedPasswordsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    return-void
.end method
