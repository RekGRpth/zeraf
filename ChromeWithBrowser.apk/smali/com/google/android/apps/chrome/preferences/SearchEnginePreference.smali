.class public Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$TemplateURLServiceLoadedListener;


# instance fields
.field private mNewValue:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method public static getSearchEngineNameAndDomain(Landroid/content/Context;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v0, "searchEngineShortName"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "searchEngineKeyword"

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0701ee

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    const-string v3, "searchEngineKeyword"

    invoke-virtual {p1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f060013

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->addPreferencesFromResource(I)V

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getLocalizedSearchEngines()[Ljava/util/HashMap;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    new-instance v5, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getSearchEngineNameAndDomain(Landroid/content/Context;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    const-string v0, "searchEngineId"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    const-string v0, "searchEngineId"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefSearchEnginePreference()I

    move-result v4

    if-ne v0, v4, :cond_0

    const v0, 0x7f0701c0

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setSummary(I)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->mNewValue:I

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isTemplateURLServiceLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->mNewValue:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSearchEngine(I)V

    :goto_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->registerTemplateURLServiceLoadedListener(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$TemplateURLServiceLoadedListener;)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0701c0

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(I)V

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onTemplateURLServiceLoaded()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->mNewValue:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSearchEngine(I)V

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->unregisterTemplateURLServiceLoadedListener(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$TemplateURLServiceLoadedListener;)V

    return-void
.end method
