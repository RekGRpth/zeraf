.class Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfoReadyCallback;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher$1;->this$1:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStorageInfoReady(Ljava/util/ArrayList;)V
    .locals 4

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/WebsiteAddress;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher$1;->this$1:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher;

    iget-object v3, v3, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher;->this$0:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->findOrCreateSitesByHost(Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Ljava/util/Set;
    invoke-static {v3, v1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;->access$700(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences;Lcom/google/android/apps/chrome/preferences/WebsiteAddress;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/preferences/Website;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/Website;->addStorageInfo(Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$StorageInfo;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher$1;->this$1:Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsPreferences$WebStorageInfoFetcher;->next()V

    return-void
.end method
