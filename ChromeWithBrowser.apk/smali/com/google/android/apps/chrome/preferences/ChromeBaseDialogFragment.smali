.class public Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.super Landroid/app/DialogFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v0, v1, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/PreferenceHeaders;->popFragmentList(Landroid/app/Fragment;)V

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method
