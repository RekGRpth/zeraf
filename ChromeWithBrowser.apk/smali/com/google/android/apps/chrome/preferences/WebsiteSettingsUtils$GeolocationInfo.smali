.class public Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private mEmbedder:Ljava/lang/String;

.field private mOrigin:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getEmbedderSafe()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->mEmbedder:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->mEmbedder:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->mOrigin:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getAllowed()Ljava/lang/Boolean;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->mOrigin:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->getEmbedderSafe()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeGetGeolocationSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getEmbedder()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->mEmbedder:Ljava/lang/String;

    return-object v0
.end method

.method public getOrigin()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->mOrigin:Ljava/lang/String;

    return-object v0
.end method

.method public setAllowed(Ljava/lang/Boolean;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->mOrigin:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->getEmbedderSafe()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->nativeSetGeolocationSettingForOrigin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils;->access$100(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method public setInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->mOrigin:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->mEmbedder:Ljava/lang/String;

    return-void
.end method
