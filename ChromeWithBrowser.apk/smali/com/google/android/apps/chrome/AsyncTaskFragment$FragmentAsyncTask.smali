.class public abstract Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/AsyncTaskFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/AsyncTaskFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->this$0:Lcom/google/android/apps/chrome/AsyncTaskFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private cleanUp()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->setDependentUIEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->this$0:Lcom/google/android/apps/chrome/AsyncTaskFragment;

    # invokes: Lcom/google/android/apps/chrome/AsyncTaskFragment;->taskFinished()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->access$400(Lcom/google/android/apps/chrome/AsyncTaskFragment;)V

    return-void
.end method

.method private finishTask()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->cleanUp()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->onTaskFinished()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->runBackgroundTask()V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->onCancelled(Ljava/lang/Void;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/Void;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->cleanUp()V

    return-void
.end method

.method onDialogStayedEnough()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->finishTask()V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->this$0:Lcom/google/android/apps/chrome/AsyncTaskFragment;

    # getter for: Lcom/google/android/apps/chrome/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->access$300(Lcom/google/android/apps/chrome/AsyncTaskFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->this$0:Lcom/google/android/apps/chrome/AsyncTaskFragment;

    # getter for: Lcom/google/android/apps/chrome/AsyncTaskFragment;->mHasDialogStayedEnough:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->access$200(Lcom/google/android/apps/chrome/AsyncTaskFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->finishTask()V

    :cond_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->setDependentUIEnabled(Z)V

    return-void
.end method

.method protected abstract onTaskFinished()V
.end method

.method protected abstract runBackgroundTask()V
.end method

.method protected abstract setDependentUIEnabled(Z)V
.end method

.method public updateDependentUI()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;->setDependentUIEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
