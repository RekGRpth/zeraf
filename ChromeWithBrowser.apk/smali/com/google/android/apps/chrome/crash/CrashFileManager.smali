.class public Lcom/google/android/apps/chrome/crash/CrashFileManager;
.super Ljava/lang/Object;


# static fields
.field static final CRASH_DUMP_DIR:Ljava/lang/String; = "Crash Reports"

.field private static FILE_COMPARATOR:Ljava/util/Comparator; = null

.field private static final MINIDUMP_PATTERN:Ljava/util/regex/Pattern;

.field private static final TAG:Ljava/lang/String;

.field private static final TMP_PATTERN:Ljava/util/regex/Pattern;

.field protected static final TMP_SUFFIX:Ljava/lang/String; = ".tmp"

.field private static final UPLOADED_MINIDUMP_PATTERN:Ljava/util/regex/Pattern;

.field private static final UPLOADED_MINIDUMP_SUFFIX:Ljava/lang/String; = ".up"


# instance fields
.field private mCacheDir:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;->TAG:Ljava/lang/String;

    const-string v0, "\\.dmp([0-9]*)\\z"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;->MINIDUMP_PATTERN:Ljava/util/regex/Pattern;

    const-string v0, "\\.up([0-9]*)\\z"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;->UPLOADED_MINIDUMP_PATTERN:Ljava/util/regex/Pattern;

    const-string v0, "\\.tmp\\z"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;->TMP_PATTERN:Ljava/util/regex/Pattern;

    new-instance v0, Lcom/google/android/apps/chrome/crash/CrashFileManager$1;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;->FILE_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Specified context cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a directory."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/crash/CrashFileManager;->mCacheDir:Ljava/io/File;

    return-void
.end method

.method static deleteFile(Ljava/io/File;)Z
    .locals 4

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/apps/chrome/crash/CrashFileManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to delete "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v0
.end method

.method private getAllTempFiles()[Ljava/io/File;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;->TMP_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getMatchingFiles(Ljava/util/regex/Pattern;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static tryMarkAsUploaded(Ljava/io/File;)Z
    .locals 4

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\.dmp"

    const-string v3, ".up"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public cleanOutAllNonFreshMinidumpFiles()V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getAllUploadedFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-static {v4}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->deleteFile(Ljava/io/File;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getAllTempFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-static {v3}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->deleteFile(Ljava/io/File;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public getAllMinidumpFiles()[Ljava/io/File;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;->MINIDUMP_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getMatchingFiles(Ljava/util/regex/Pattern;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getAllMinidumpFilesSorted()[Ljava/io/File;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getAllMinidumpFiles()[Ljava/io/File;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/crash/CrashFileManager;->FILE_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    return-object v0
.end method

.method getAllUploadedFiles()[Ljava/io/File;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;->UPLOADED_MINIDUMP_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getMatchingFiles(Ljava/util/regex/Pattern;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method getCrashDirectory()Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/chrome/crash/CrashFileManager;->mCacheDir:Ljava/io/File;

    const-string v2, "Crash Reports"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method getMatchingFiles(Ljava/util/regex/Pattern;)[Ljava/io/File;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getCrashDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/chrome/crash/CrashFileManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " does not exist!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v3, [Ljava/io/File;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/apps/chrome/crash/CrashFileManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a directory!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v3, [Ljava/io/File;

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/apps/chrome/crash/CrashFileManager$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/crash/CrashFileManager$2;-><init>(Lcom/google/android/apps/chrome/crash/CrashFileManager;Ljava/util/regex/Pattern;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method
