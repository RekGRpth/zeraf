.class public Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/sync/AddGoogleAccountDialogFragment$Listener;
.implements Lcom/google/android/apps/chrome/sync/SyncChooseAccountFragment$Listener;


# static fields
.field private static sAccountAdder:Lcom/google/android/apps/chrome/sync/AccountAdder;

.field private static sSyncSignInListener:Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

.field private mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/sync/AccountAdder;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/AccountAdder;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->sAccountAdder:Lcom/google/android/apps/chrome/sync/AccountAdder;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;Landroid/accounts/Account;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->getCloudPrintAuthTokenAndSetupSyncAndSendToDevice(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;Landroid/accounts/Account;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->doFinishSignIn(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private doFinishSignIn(Landroid/accounts/Account;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->doFinishSignInForSync(Landroid/accounts/Account;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->doFinishSignInForSendToDevice(Landroid/accounts/Account;)V

    new-instance v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;-><init>(Landroid/app/Activity;Landroid/accounts/Account;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->start()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener$3;-><init>(Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private doFinishSignInForSendToDevice(Landroid/accounts/Account;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sendToDevice(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    return-void
.end method

.method private doFinishSignInForSync(Landroid/accounts/Account;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v2, v1}, Lorg/chromium/sync/notifier/InvalidationController;->setRegisteredTypes(Landroid/accounts/Account;ZLjava/util/Set;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->syncSetupCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSetupInProgress(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->syncSignIn(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static get()Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->sSyncSignInListener:Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->sSyncSignInListener:Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->sSyncSignInListener:Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;

    return-object v0
.end method

.method private getAuthTokensAndSetupSyncAndSendToDevice(Landroid/app/Activity;Landroid/accounts/Account;)V
    .locals 3

    invoke-static {p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    const-string v1, "chromiumsync"

    new-instance v2, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener$1;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener$1;-><init>(Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;Landroid/accounts/Account;)V

    invoke-virtual {v0, p1, p2, v1, v2}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromForeground(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    return-void
.end method

.method private getCloudPrintAuthTokenAndSetupSyncAndSendToDevice(Landroid/accounts/Account;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    const-string v2, "cloudprint"

    new-instance v3, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener$2;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener$2;-><init>(Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;Landroid/accounts/Account;)V

    invoke-virtual {v0, v1, p1, v2, v3}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromForeground(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    return-void
.end method

.method public static overrideAccountAdderForTests(Lcom/google/android/apps/chrome/sync/AccountAdder;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->sAccountAdder:Lcom/google/android/apps/chrome/sync/AccountAdder;

    return-void
.end method


# virtual methods
.method public onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0, p2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->setSignedInAccountName(Ljava/lang/String;)V

    invoke-static {p2}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->getAuthTokensAndSetupSyncAndSendToDevice(Landroid/app/Activity;Landroid/accounts/Account;)V

    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lorg/chromium/sync/notifier/InvalidationController;->setRegisteredTypes(Landroid/accounts/Account;ZLjava/util/Set;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->autoLogin(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    return-void
.end method

.method public onAddAccount(Landroid/app/Activity;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->sAccountAdder:Lcom/google/android/apps/chrome/sync/AccountAdder;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSignInAccountListener;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/AccountAdder;->addAccount(Landroid/app/Activity;I)V

    return-void
.end method
