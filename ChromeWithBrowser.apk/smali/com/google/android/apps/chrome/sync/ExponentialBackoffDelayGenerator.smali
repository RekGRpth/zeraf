.class Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;
.super Ljava/lang/Object;


# instance fields
.field private mCurrentMaxDelay:I

.field private final mMaxDelayMs:I

.field private final mRng:Ljava/util/Random;


# direct methods
.method constructor <init>(Ljava/util/Random;II)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lez p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "max delay must be positive"

    invoke-static {v0, v3}, Lcom/google/a/a/a;->a(ZLjava/lang/Object;)V

    if-lez p3, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "initial delay must be positive"

    invoke-static {v0, v3}, Lcom/google/a/a/a;->a(ZLjava/lang/Object;)V

    if-gt p3, p2, :cond_2

    :goto_2
    const-string v0, "initial delay cannot be larger than max delay"

    invoke-static {v1, v0}, Lcom/google/a/a/a;->a(ZLjava/lang/Object;)V

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->mRng:Ljava/util/Random;

    iput p2, p0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->mMaxDelayMs:I

    iput p3, p0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->mCurrentMaxDelay:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method getNextDelayMs()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->mRng:Ljava/util/Random;

    iget v1, p0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->mCurrentMaxDelay:I

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->mCurrentMaxDelay:I

    iget v2, p0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->mMaxDelayMs:I

    if-gt v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->mCurrentMaxDelay:I

    shl-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->mMaxDelayMs:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/sync/ExponentialBackoffDelayGenerator;->mCurrentMaxDelay:I

    :cond_0
    return v0
.end method
