.class public Lcom/google/android/apps/chrome/sync/PassphraseActivity;
.super Landroid/app/Activity;

# interfaces
.implements Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final FRAGMENT_PASSWORD:Ljava/lang/String; = "password_fragment"

.field public static final FRAGMENT_SPINNER:Ljava/lang/String; = "spinner_fragment"

.field private static final TAG:Ljava/lang/String; = "PassphraseActivity"

.field private static mSyncStateChangedListener:Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/sync/PassphraseActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/PassphraseActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->removeSyncStateChangedListener()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/PassphraseActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->displayPasswordDialog()V

    return-void
.end method

.method private addSyncStateChangedListener()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->mSyncStateChangedListener:Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/sync/PassphraseActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity$1;-><init>(Lcom/google/android/apps/chrome/sync/PassphraseActivity;)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->mSyncStateChangedListener:Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;

    invoke-static {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->mSyncStateChangedListener:Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->addSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    goto :goto_0
.end method

.method private displayPasswordDialog()V
    .locals 4

    const/4 v1, 0x0

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isUsingSecondaryPassphrase()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->newInstance(Landroid/app/Fragment;ZZ)Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;

    move-result-object v0

    const-string v1, "password_fragment"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private displaySpinnerDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/sync/PassphraseActivity$SpinnerDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/sync/PassphraseActivity$SpinnerDialogFragment;-><init>()V

    const-string v2, "spinner_fragment"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/PassphraseActivity$SpinnerDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    return-void
.end method

.method private isShowingDialog(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeSyncStateChangedListener()V
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->mSyncStateChangedListener:Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->removeSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->mSyncStateChangedListener:Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Z)V
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    const v0, 0x7f040035

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->setContentView(I)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "PassphraseActivity"

    const-string v2, "Failed to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->finish()V

    goto :goto_0
.end method

.method public onPassphraseCanceled(ZZ)V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->getInstance()Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->syncStateChanged()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->finish()V

    return-void
.end method

.method public onPassphraseEntered(Ljava/lang/String;ZZ)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setDecryptionPassphrase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "password_fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->invalidPassphrase()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-static {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "password_fragment"

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->isShowingDialog(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->displayPasswordDialog()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->addSyncStateChangedListener()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/PassphraseActivity;->displaySpinnerDialog()V

    goto :goto_0
.end method
