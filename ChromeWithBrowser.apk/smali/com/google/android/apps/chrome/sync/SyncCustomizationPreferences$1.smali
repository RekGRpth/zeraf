.class Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$800(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;
    invoke-static {v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$700(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    return-void
.end method
