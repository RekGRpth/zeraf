.class public Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListenerController;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/base/ActivityStatus$StateListener;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListenerController;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onActivityStateChange(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListenerController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListenerController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationController;->stop()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationController;->start()V

    goto :goto_0
.end method
