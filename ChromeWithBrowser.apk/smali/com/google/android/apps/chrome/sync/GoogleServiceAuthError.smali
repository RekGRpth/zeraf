.class public Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError;
.super Ljava/lang/Object;


# instance fields
.field private mState:Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;


# direct methods
.method constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;->fromCode(I)Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError;->mState:Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    return-void
.end method


# virtual methods
.method getState()Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError;->mState:Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    return-object v0
.end method
