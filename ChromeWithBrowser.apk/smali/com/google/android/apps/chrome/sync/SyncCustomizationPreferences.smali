.class public Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment$Listener;
.implements Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment$Listener;
.implements Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final ARGUMENT_ACCOUNT:Ljava/lang/String; = "account"

.field private static final DASHBOARD_URL:Ljava/lang/String; = "http://www.google.com/dashboard"

.field public static final FRAGMENT_CUSTOM_PASSWORD:Ljava/lang/String; = "custom_password"

.field public static final FRAGMENT_ENTER_PASSWORD:Ljava/lang/String; = "enter_password"

.field public static final FRAGMENT_PASSWORD_TYPE:Ljava/lang/String; = "password_type"

.field private static final FRAGMENT_SPINNER:Ljava/lang/String; = "spinner"

.field public static final PREFERENCE_ENCRYPTION:Ljava/lang/String; = "encryption"

.field private static final PREFERENCE_GROUP_SYNC_DATA_TYPES:Ljava/lang/String; = "sync_data_types"

.field public static final PREFERENCE_SYNC_AUTOFILL:Ljava/lang/String; = "sync_autofill"

.field public static final PREFERENCE_SYNC_BOOKMARKS:Ljava/lang/String; = "sync_bookmarks"

.field public static final PREFERENCE_SYNC_EVERYTHING:Ljava/lang/String; = "sync_everything"

.field public static final PREFERENCE_SYNC_OMNIBOX:Ljava/lang/String; = "sync_omnibox"

.field public static final PREFERENCE_SYNC_PASSWORDS:Ljava/lang/String; = "sync_passwords"

.field public static final PREFERENCE_SYNC_RECENT_TABS:Ljava/lang/String; = "sync_recent_tabs"

.field private static final TAG:Ljava/lang/String; = "SyncCustomizationPreferences"

.field public static final prefsToSave:[Ljava/lang/String;


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mAllTypes:[Landroid/preference/CheckBoxPreference;

.field private mAndroidSyncSettingsObserverHandle:Ljava/lang/Object;

.field private mCheckboxesInitialized:Z

.field private mDoCustomAfterGaia:Z

.field private mPasswordSyncConfigurable:Z

.field private mSyncAutofill:Landroid/preference/CheckBoxPreference;

.field private mSyncBookmarks:Landroid/preference/CheckBoxPreference;

.field private mSyncEncryption:Landroid/preference/Preference;

.field private mSyncEverything:Landroid/preference/CheckBoxPreference;

.field private mSyncOmnibox:Landroid/preference/CheckBoxPreference;

.field private mSyncPasswords:Landroid/preference/CheckBoxPreference;

.field private mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

.field private mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

.field private mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

.field private mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->$assertionsDisabled:Z

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "sync_everything"

    aput-object v3, v0, v2

    const-string v2, "sync_autofill"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "sync_bookmarks"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "sync_omnibox"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "sync_passwords"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "sync_recent_tabs"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->prefsToSave:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Landroid/widget/Switch;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)[Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->setPreferencesEnabled(Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->destroySyncStartupHelper()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->closeDialogIfOpen(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->closePage()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->configureEncryption(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->notifyInvalidPassphrase()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Lorg/chromium/sync/notifier/SyncStatusHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Ljava/util/Set;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getSelectedCheckBoxPreferences()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private closeDialogIfOpen(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private closePage()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    invoke-virtual {v0}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method private configureEncryption(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->enableEncryptEverything()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setEncryptionPassphrase(Ljava/lang/String;Z)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->configureSyncDataTypes()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->showConfigure()V

    return-void
.end method

.method private configureSyncDataTypes()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getSelectedModelTypes()Ljava/util/Set;

    move-result-object v1

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setPreferredDataTypes(ZLjava/util/Set;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getAccountArgument()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lorg/chromium/sync/notifier/InvalidationController;->setRegisteredTypes(Landroid/accounts/Account;ZLjava/util/Set;)V

    goto :goto_0
.end method

.method private destroySyncStartupHelper()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    :cond_0
    return-void
.end method

.method private displayCustomPasswordDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;-><init>()V

    const/4 v2, -0x1

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    const-string v2, "custom_password"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/CustomPassphraseDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    return-void
.end method

.method private displayPasswordDialog(ZZ)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->newInstance(Landroid/app/Fragment;ZZ)Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;

    move-result-object v1

    const-string v2, "enter_password"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    return-void
.end method

.method private displayPasswordTypeDialog()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getCurrentlySelectedEncryptionType()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getAllowedEncryptionTypes()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncMigrated()Z

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->create(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;Ljava/util/ArrayList;Z)Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;

    move-result-object v1

    const-string v2, "password_type"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    const/4 v0, -0x1

    invoke-virtual {v1, p0, v0}, Lcom/google/android/apps/chrome/sync/PassphraseTypeDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    return-void
.end method

.method private displaySpinnerDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$SpinnerDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$SpinnerDialogFragment;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$SpinnerDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    const-string v2, "spinner"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$SpinnerDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    return-void
.end method

.method private getAccountArgument()Landroid/accounts/Account;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method private getAllowedEncryptionTypes()Ljava/util/ArrayList;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isEncryptEverythingEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->NONE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isUsingSecondaryPassphrase()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->IMPLICIT_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    sget-object v1, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->CUSTOM_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private getCurrentlySelectedEncryptionType()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isEncryptEverythingEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isUsingSecondaryPassphrase()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->CUSTOM_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->IMPLICIT_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->NONE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    goto :goto_0
.end method

.method private getSelectedCheckBoxPreferences()Ljava/util/Set;
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mPasswordSyncConfigurable:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v0
.end method

.method private getSelectedModelTypes()Ljava/util/Set;
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mPasswordSyncConfigurable:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->SESSION:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v0
.end method

.method private handleDecryption(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setDecryptionPassphrase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->showConfigure()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->notifyInvalidPassphrase()V

    goto :goto_0
.end method

.method private handleEncryptWithCustomPassphrase(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->configureEncryption(Ljava/lang/String;Z)V

    return-void
.end method

.method private handleEncryptWithGaia(Ljava/lang/String;)V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$4;

    invoke-direct {v4, p0, p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$4;-><init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Ljava/lang/String;)V

    invoke-static {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v5, "password"

    invoke-virtual {v2, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->confirmCredentials(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method private initCheckboxStates(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mCheckboxesInitialized:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mCheckboxesInitialized:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mSyncEverything:Z
    invoke-static {p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$100(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mAutofillEnabled:Z
    invoke-static {p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$200(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mBookmarksEnabled:Z
    invoke-static {p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$300(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mOpenTabsEnabled:Z
    invoke-static {p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$600(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mPasswordsEnabled:Z
    invoke-static {p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$500(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mOmniboxEnabled:Z
    invoke-static {p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$400(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->setPreferencesEnabled(Z)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v6, v4, v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v6, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method private isSyncTypePreference(Landroid/preference/Preference;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyInvalidPassphrase()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "enter_password"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/PassphraseDialogFragment;->invalidPassphrase()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "SyncCustomizationPreferences"

    const-string v1, "invalid passphrase but no dialog to notify"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private openDashboardTabInNewActivityStack()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "http://www.google.com/dashboard"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/google/android/apps/chrome/Main;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private setPreferencesEnabled(Z)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEncryption:Landroid/preference/Preference;

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    return-void
.end method

.method private updateEncryptionState()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEncryption:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isPassphraseRequiredForDecryption()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isUsingSecondaryPassphrase()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEncryption:Landroid/preference/Preference;

    const v1, 0x7f070070

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->errorSummary(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEncryption:Landroid/preference/Preference;

    const v1, 0x7f070071

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->errorSummary(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getSyncActionBarSwitch()Landroid/widget/Switch;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f060017

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->addPreferencesFromResource(I)V

    const-string v0, "sync_everything"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    const-string v0, "sync_autofill"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    const-string v0, "sync_bookmarks"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    const-string v0, "sync_omnibox"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    const-string v0, "sync_passwords"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    const-string v0, "sync_recent_tabs"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    const-string v0, "encryption"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEncryption:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEncryption:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const/4 v0, 0x5

    new-array v0, v0, [Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    aput-object v3, v0, v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    aput-object v3, v0, v6

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    aput-object v4, v0, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    aput-object v4, v0, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    aput-object v4, v0, v3

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    invoke-virtual {v5, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEncryption:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    invoke-virtual {v5, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    if-nez p3, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->displaySpinnerDialog()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isCryptographerReady()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v6, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mPasswordSyncConfigurable:Z

    :goto_2
    if-eqz p3, :cond_3

    const-string v0, "prefs"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "mDoCustomAfterGaia"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mDoCustomAfterGaia:Z

    if-eqz v0, :cond_3

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;-><init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$1;)V

    const-string v3, "sync_everything"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mSyncEverything:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$102(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    const-string v3, "sync_autofill"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mAutofillEnabled:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$202(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    const-string v3, "sync_bookmarks"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mBookmarksEnabled:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$302(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    const-string v3, "sync_omnibox"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mOmniboxEnabled:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$402(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    const-string v3, "sync_passwords"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mPasswordsEnabled:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$502(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    const-string v3, "sync_recent_tabs"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mOpenTabsEnabled:Z
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$602(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->initCheckboxStates(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)V

    :cond_3
    new-instance v0, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v3, 0x7f0c0008

    invoke-virtual {v0, v1, v3}, Landroid/widget/Switch;->setSwitchTextAppearance(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->addSwitchToActionBar(Landroid/widget/Switch;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$1;-><init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0, v6}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->setHasOptionsMenu(Z)V

    return-object v2

    :cond_4
    const-string v0, "sync_data_types"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setVisibility(I)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f0113

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->openDashboardTabInNewActivityStack()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onPassphraseCanceled(ZZ)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mDoCustomAfterGaia:Z

    return-void
.end method

.method public onPassphraseEntered(Ljava/lang/String;ZZ)V
    .locals 1

    if-eqz p3, :cond_1

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->handleEncryptWithGaia(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->configureEncryption(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->handleDecryption(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPassphraseTypeSelected(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isEncryptEverythingEnabled()Z

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isUsingSecondaryPassphrase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sget-object v4, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->IMPLICIT_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    if-ne p1, v4, :cond_4

    sget-boolean v4, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    if-nez v3, :cond_2

    move v2, v1

    :cond_2
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->displayPasswordDialog(ZZ)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    sget-object v3, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->CUSTOM_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    if-ne p1, v3, :cond_3

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getCurrentlySelectedEncryptionType()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->NONE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isPassphraseRequiredForExternalType()Z

    move-result v0

    if-eqz v0, :cond_5

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mDoCustomAfterGaia:Z

    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->displayPasswordDialog(ZZ)V

    goto :goto_1

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->displayCustomPasswordDialog()V

    goto :goto_1

    :cond_6
    invoke-direct {p0, v2, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->displayPasswordDialog(ZZ)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->destroySyncStartupHelper()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAndroidSyncSettingsObserverHandle:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAndroidSyncSettingsObserverHandle:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->unregisterContentResolverObserver(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->removeSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->configureSyncDataTypes()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSyncSetupCompleted()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSetupInProgress(Z)V

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-virtual {v4, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {p2, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->isSyncTypePreference(Landroid/preference/Preference;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$2;-><init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->isResumed()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEncryption:Landroid/preference/Preference;

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isPassphraseRequiredForDecryption()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isUsingSecondaryPassphrase()Z

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->displayPasswordDialog(ZZ)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->displayPasswordTypeDialog()V

    move v1, v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    const v0, 0x7f0f0113

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07006d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSetupInProgress(Z)V

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    new-instance v3, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;-><init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Landroid/app/Activity;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;-><init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getAccountArgument()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->startSync(Landroid/app/Activity;Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->addSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;-><init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$1;)V

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->registerContentResolverObserver(Landroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mAndroidSyncSettingsObserverHandle:Ljava/lang/Object;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 7

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mCheckboxesInitialized:Z

    if-eqz v0, :cond_2

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->prefsToSave:[Ljava/lang/String;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    instance-of v6, v0, Landroid/preference/CheckBoxPreference;

    if-eqz v6, :cond_0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-virtual {v2, v5, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "prefs"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    const-string v0, "mDoCustomAfterGaia"

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mDoCustomAfterGaia:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public showConfigure()V
    .locals 3

    const-string v0, "spinner"

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->closeDialogIfOpen(Ljava/lang/String;)V

    const-string v0, "custom_password"

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->closeDialogIfOpen(Ljava/lang/String;)V

    const-string v0, "enter_password"

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->closeDialogIfOpen(Ljava/lang/String;)V

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->updateEncryptionState()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isPassphraseRequiredForDecryption()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mDoCustomAfterGaia:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mDoCustomAfterGaia:Z

    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->CUSTOM_PASSPHRASE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->onPassphraseTypeSelected(Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncEncryption:Landroid/preference/Preference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getPreferredDataTypes()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;-><init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$1;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->hasKeepEverythingSynced()Z

    move-result v2

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mSyncEverything:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$102(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mAutofillEnabled:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$202(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mBookmarksEnabled:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$302(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mOmniboxEnabled:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$402(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mPasswordsEnabled:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$502(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->SESSION:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    # setter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->mOpenTabsEnabled:Z
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;->access$602(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;Z)Z

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->initCheckboxStates(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$EnabledSyncedTypes;)V

    return-void
.end method

.method public syncStateChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->updateEncryptionState()V

    return-void
.end method
