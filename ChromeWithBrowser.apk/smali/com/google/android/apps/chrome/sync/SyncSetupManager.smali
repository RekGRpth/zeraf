.class public Lcom/google/android/apps/chrome/sync/SyncSetupManager;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ACCOUNTS_CHANGED_PREFS_KEY:Ljava/lang/String; = "prefs_sync_accounts_changed"

.field public static final AUTH_TOKEN_TYPE_SYNC:Ljava/lang/String; = "chromiumsync"

.field private static final SYNC_PREVIOUS_MASTER_STATE_PREFS_KEY:Ljava/lang/String; = "prefs_sync_master_previous_state"

.field private static final SYNC_WANTED_STATE_PREFS_KEY:Ljava/lang/String; = "prefs_sync_wanted_state"

.field private static final TAG:Ljava/lang/String; = "SyncSetupManager"

.field private static sSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private final mListeners:Ljava/util/List;

.field private mNativeSyncSetupManager:I

.field private final mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

.field private final mUiHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mListeners:Ljava/util/List;

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeInit()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mUiHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setupSessionSyncId()V

    invoke-static {p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncSettingsObserver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncSettingsObserver;-><init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;)V

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->registerContentResolverObserver(Landroid/content/SyncStatusObserver;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->createNewInstance(Landroid/content/Context;Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lorg/chromium/sync/notifier/SyncStatusHelper;)Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->addSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->addListener(Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$Listener;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v1, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->addListener(Lorg/chromium/sync/notifier/SyncStatusHelper$Listener;)V

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncDisabler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/sync/SyncDisabler;-><init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lorg/chromium/sync/notifier/SyncStatusHelper;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->addSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->applyAndroidSyncStateOnUiThread()V

    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListenerController;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/sync/ChromeSyncInvalidationListenerController;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lorg/chromium/base/ActivityStatus;->registerStateListener(Lorg/chromium/base/ActivityStatus$StateListener;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Landroid/accounts/Account;Lcom/google/android/apps/chrome/sync/SyncStates;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSyncState(Landroid/accounts/Account;Lcom/google/android/apps/chrome/sync/SyncStates;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSendToDeviceState(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setAutologinState(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)Lorg/chromium/sync/notifier/SyncStatusHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getPreviousMasterSyncState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Landroid/accounts/Account;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getWantedSyncState(Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/sync/SyncSetupManager;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeTokenAvailable(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->applyAndroidSyncStateOnUiThread()V

    return-void
.end method

.method private accountExists(Landroid/accounts/Account;)Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private applyAndroidSyncStateOnUiThread()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager$2;-><init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private checkAndClearAccountsChangedPref(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "prefs_sync_accounts_changed"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "prefs_sync_accounts_changed"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;
    .locals 1

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->sSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->sSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->sSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    return-object v0
.end method

.method private getPreviousMasterSyncState()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "prefs_sync_master_previous_state"

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getSyncEnterCustomPassphraseBodyText()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeGetSyncEnterCustomPassphraseBodyText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getWantedSyncState(Landroid/accounts/Account;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "prefs_sync_wanted_state"

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabledForChrome(Landroid/accounts/Account;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private initializeGcm()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/contrib/MultiplexingGcmListener;->initializeGcm(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SyncSetupManager"

    const-string v1, "Already registered with GCM"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SyncSetupManager"

    const-string v2, "Application manifest does not correctly configure GCM; sync notifications will not work"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "SyncSetupManager"

    const-string v2, "Device does not support GCM; sync noticications will not work"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static isAutoLoginEnabled(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAutologinEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method private isStateChangeValid(Landroid/accounts/Account;Lcom/google/android/apps/chrome/sync/SyncStates;)Z
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->hasSync()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "sync = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->isSyncEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->hasWantedSyncState()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ", wantedSyncState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->isWantedSyncStateEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->hasSendToDevice()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ", sendToDevice = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->isSendToDeviceEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->hasAutoLoginSet()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, ", autoLogin = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->isAutoLoginEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "SyncSetupManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setState: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_4

    const-string v0, "SyncSetupManager"

    const-string v1, "No account supplied. Not changing states"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static markAccountsChangedPref(Landroid/content/Context;)V
    .locals 3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "prefs_sync_accounts_changed"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeDisableSync(I)V
.end method

.method private native nativeEnableEncryptEverything(I)V
.end method

.method private native nativeEnableSync(I)V
.end method

.method private native nativeGetAboutInfoForTest(I)Ljava/lang/String;
.end method

.method private native nativeGetAuthError(I)I
.end method

.method private native nativeGetPassphraseType(I)I
.end method

.method private static native nativeGetSyncEnterCustomPassphraseBodyText()Ljava/lang/String;
.end method

.method private native nativeGetSyncEnterCustomPassphraseBodyWithDateText(I)Ljava/lang/String;
.end method

.method private native nativeGetSyncEnterGooglePassphraseBodyWithDateText(I)Ljava/lang/String;
.end method

.method private native nativeHasExplicitPassphraseTime(I)Z
.end method

.method private native nativeHasKeepEverythingSynced(I)Z
.end method

.method private native nativeHasUnrecoverableError(I)Z
.end method

.method private native nativeInit()I
.end method

.method private native nativeIsAutofillSyncEnabled(I)Z
.end method

.method private native nativeIsBookmarkSyncEnabled(I)Z
.end method

.method private native nativeIsCryptographerReady(I)Z
.end method

.method private native nativeIsEncryptEverythingEnabled(I)Z
.end method

.method private native nativeIsFirstSetupInProgress(I)Z
.end method

.method private native nativeIsMigrated(I)Z
.end method

.method private native nativeIsPassphraseRequiredForDecryption(I)Z
.end method

.method private native nativeIsPassphraseRequiredForExternalType(I)Z
.end method

.method private native nativeIsPasswordSyncEnabled(I)Z
.end method

.method private native nativeIsSessionSyncEnabled(I)Z
.end method

.method private native nativeIsSyncInitialized(I)Z
.end method

.method private native nativeIsTypedUrlSyncEnabled(I)Z
.end method

.method private native nativeIsUsingSecondaryPassphrase(I)Z
.end method

.method private native nativeNudgeSyncer(ILjava/lang/String;JLjava/lang/String;)V
.end method

.method private native nativeQuerySyncStatusSummary(I)Ljava/lang/String;
.end method

.method private native nativeSetDecryptionPassphrase(ILjava/lang/String;)Z
.end method

.method private native nativeSetEncryptionPassphrase(ILjava/lang/String;Z)V
.end method

.method private native nativeSetPreferredDataTypes(IZZZZZZ)V
.end method

.method private native nativeSetSetupInProgress(IZ)V
.end method

.method private native nativeSetSyncSessionsId(ILjava/lang/String;)Z
.end method

.method private native nativeSetSyncSetupCompleted(I)V
.end method

.method private native nativeSignInSync(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeSignOutSync(I)V
.end method

.method private native nativeTokenAvailable(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method private runOnUiThread(Ljava/lang/Runnable;)V
    .locals 1

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private setAutologinState(Z)V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setAutologinEnabled(Z)V

    if-nez p1, :cond_0

    const/16 v0, 0x3a

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    :cond_0
    return-void
.end method

.method private setSendToDeviceState(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createSetEnabledIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->startServiceWithWakeLock(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private setSyncSessionsId()Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    sget-boolean v1, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const-string v1, "GSERVICES_ANDROID_ID"

    invoke-static {v1}, Lcom/google/android/apps/chrome/identity/UniqueIdentificationGeneratorFactory;->getInstance(Ljava/lang/String;)Lcom/google/android/apps/chrome/identity/UniqueIdentificationGenerator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/identity/UniqueIdentificationGenerator;->getUniqueId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "SyncSetupManager"

    const-string v2, "Unable to get unique tag for sync."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "session_sync"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeSetSyncSessionsId(ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "SyncSetupManager"

    const-string v2, "Unable to write session sync tag."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setSyncState(Landroid/accounts/Account;Lcom/google/android/apps/chrome/sync/SyncStates;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->storeSyncStatePreferences(Landroid/accounts/Account;Lcom/google/android/apps/chrome/sync/SyncStates;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->isSyncEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "SyncSetupManager"

    const-string v2, "Enabling sync"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationController;->start()V

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeEnableSync(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->enableAndroidSync(Landroid/accounts/Account;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->syncStateChanged()V

    return-void

    :cond_0
    const-string v0, "SyncSetupManager"

    const-string v1, "Unable to enable sync, since master sync is disabled. Displaying error notification."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->getInstance()Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->displayAndroidMasterSyncDisabledNotification()V

    goto :goto_0

    :cond_1
    const-string v1, "SyncSetupManager"

    const-string v2, "Disabling sync"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationController;->stop()V

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeDisableSync(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->disableAndroidSync(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method private setupSessionSyncId()V
    .locals 3

    const-string v0, "GSERVICES_ANDROID_ID"

    new-instance v1, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/identity/UniqueIdentificationGeneratorFactory;->registerGenerator(Ljava/lang/String;Lcom/google/android/apps/chrome/identity/UniqueIdentificationGenerator;Z)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSyncSessionsId()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SyncSetupManager"

    const-string v1, "Failed to set session ID for sync. This may lead to unexpected tab sync behavior."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private storeSyncStatePreferences(Landroid/accounts/Account;Lcom/google/android/apps/chrome/sync/SyncStates;)V
    .locals 3

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->hasWantedSyncState()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->isWantedSyncStateEnabled()Z

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "prefs_sync_wanted_state"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->hasMasterSyncState()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefs_sync_master_previous_state"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->isMasterSyncStateEnabled()Z

    move-result v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/sync/SyncStates;->isSyncEnabled()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public addSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public destroy()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeDestroy(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    return-void
.end method

.method public enableEncryptEverything()V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeEnableEncryptEverything(I)V

    return-void
.end method

.method public finishSyncFirstSetupIfNeeded()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isFirstSetupInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSyncSetupCompleted()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setSetupInProgress(Z)V

    :cond_0
    return-void
.end method

.method public getAuthError()Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeGetAuthError(I)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;->fromCode(I)Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    move-result-object v0

    return-object v0
.end method

.method public getNewAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const-string v0, "SyncSetupManager"

    const-string v1, "Handling request for auth token from sync engine"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v0, "SyncSetupManager"

    const-string v1, "username is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v3

    if-nez v3, :cond_1

    const-string v0, "SyncSetupManager"

    const-string v1, "Account not found for provided username."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;-><init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lorg/chromium/sync/signin/AccountManagerHelper;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public getPreferredDataTypes()Ljava/util/Set;
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsAutofillSyncEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsBookmarkSyncEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    iget v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsPasswordSyncEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    iget v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsTypedUrlSyncEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    iget v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsSessionSyncEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->SESSION:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v0
.end method

.method public getSyncDecryptionPassphraseType()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeGetPassphraseType(I)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->fromInternalValue(I)Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method public getSyncDecryptionPassphraseTypeIfRequired()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isPassphraseRequiredForDecryption()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getSyncDecryptionPassphraseType()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->NONE:Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    goto :goto_0
.end method

.method public getSyncEnterCustomPassphraseBodyWithDateText()Ljava/lang/String;
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeGetSyncEnterCustomPassphraseBodyWithDateText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSyncEnterGooglePassphraseBodyWithDateText()Ljava/lang/String;
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeGetSyncEnterGooglePassphraseBodyWithDateText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSyncInternalsInfoForTest()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeGetAboutInfoForTest(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasExplicitPassphraseTime()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeHasExplicitPassphraseTime(I)Z

    move-result v0

    return v0
.end method

.method public hasKeepEverythingSynced()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeHasKeepEverythingSynced(I)Z

    move-result v0

    return v0
.end method

.method public hasUnrecoverableError()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeHasUnrecoverableError(I)Z

    move-result v0

    return v0
.end method

.method public isCryptographerReady()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsCryptographerReady(I)Z

    move-result v0

    return v0
.end method

.method public isEncryptEverythingEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsEncryptEverythingEnabled(I)Z

    move-result v0

    return v0
.end method

.method public isFirstSetupInProgress()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsFirstSetupInProgress(I)Z

    move-result v0

    return v0
.end method

.method public isPassphraseRequiredForDecryption()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsPassphraseRequiredForDecryption(I)Z

    move-result v0

    return v0
.end method

.method public isPassphraseRequiredForExternalType()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsPassphraseRequiredForExternalType(I)Z

    move-result v0

    return v0
.end method

.method public isSyncInitialized()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsSyncInitialized(I)Z

    move-result v0

    return v0
.end method

.method public isSyncMigrated()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsMigrated(I)Z

    move-result v0

    return v0
.end method

.method public isUsingSecondaryPassphrase()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeIsUsingSecondaryPassphrase(I)Z

    move-result v0

    return v0
.end method

.method public onMainActivityResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->checkAndClearAccountsChangedPref(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->validateSyncSettings(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->finishSyncFirstSetupIfNeeded()V

    return-void
.end method

.method public querySyncStatus()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeQuerySyncStatusSummary(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public removeSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method requestSyncFromNativeChrome(Ljava/lang/String;JLjava/lang/String;)V
    .locals 6

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    move-object v0, p0

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeNudgeSyncer(ILjava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method public setDecryptionPassphrase(Ljava/lang/String;)Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeSetDecryptionPassphrase(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setEncryptionPassphrase(Ljava/lang/String;Z)V
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeSetEncryptionPassphrase(ILjava/lang/String;Z)V

    return-void
.end method

.method public setPreferredDataTypes(ZLjava/util/Set;)V
    .locals 8

    const/4 v0, 0x1

    const/4 v7, 0x0

    iget v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    if-nez p1, :cond_0

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_0
    move v3, v0

    :goto_0
    if-nez p1, :cond_1

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_1
    move v4, v0

    :goto_1
    if-nez p1, :cond_2

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_2
    move v5, v0

    :goto_2
    if-nez p1, :cond_3

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->SESSION:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_3
    move v6, v0

    :goto_3
    if-nez p1, :cond_4

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v7, v0

    :cond_5
    move-object v0, p0

    move v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeSetPreferredDataTypes(IZZZZZZ)V

    return-void

    :cond_6
    move v3, v7

    goto :goto_0

    :cond_7
    move v4, v7

    goto :goto_1

    :cond_8
    move v5, v7

    goto :goto_2

    :cond_9
    move v6, v7

    goto :goto_3
.end method

.method public setSetupInProgress(Z)V
    .locals 3

    const-string v0, "SyncSetupManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting sync setup_in_progress = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeSetSetupInProgress(IZ)V

    return-void
.end method

.method public setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V
    .locals 1

    invoke-direct {p0, p2, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isStateChangeValid(Landroid/accounts/Account;Lcom/google/android/apps/chrome/sync/SyncStates;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager$1;-><init>(Lcom/google/android/apps/chrome/sync/SyncSetupManager;Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setSyncSetupCompleted()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeSetSyncSetupCompleted(I)V

    return-void
.end method

.method public signOut()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    const-string v1, "SyncSetupManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Signing user out of Chrome: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->isEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sendToDevice(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isAutoLoginEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->autoLogin(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->clearSignedInUser()V

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeSignOutSync(I)V

    return-void
.end method

.method public syncSetupCompleted()Z
    .locals 1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->syncSetupCompleted()Z

    move-result v0

    return v0
.end method

.method public syncSignIn(Ljava/lang/String;)V
    .locals 1

    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->syncSignInWithAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public syncSignInWithAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mNativeSyncSetupManager:I

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->nativeSignInSync(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->syncStateChanged()V

    return-void
.end method

.method public syncStateChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mListeners:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;->syncStateChanged()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public validateSyncSettings(Z)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->accountExists(Landroid/accounts/Account;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v0, "SyncSetupManager"

    const-string v1, "Signed in account has been deleted. Signing out of Chrome."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->signOut()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->initializeGcm()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v1, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->syncSetupCompleted()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz p1, :cond_0

    const-string v0, ""

    const-wide/16 v1, 0x0

    const-string v3, ""

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->requestSyncFromNativeChrome(Ljava/lang/String;JLjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->syncSignIn(Ljava/lang/String;)V

    goto :goto_0
.end method
