.class public Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;


# static fields
.field private static final DELAYED_ACCOUNT_NAME:Ljava/lang/String; = "delayed_account"

.field private static final DELAYED_ACCOUNT_TYPE:Ljava/lang/String; = "delayed_account_type"

.field private static final TAG:Ljava/lang/String; = "ChromeBrowserSyncAdapter"


# instance fields
.field private final mApplication:Landroid/app/Application;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/app/Application;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;->mApplication:Landroid/app/Application;

    return-void
.end method

.method static clearDelayedSyncs(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, v0}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;->setDelayedSync(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static resumeDelayedSyncs(Landroid/content/Context;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "delayed_account"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "delayed_account_type"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    const-string v3, "ChromeBrowserSyncAdapter"

    const-string v4, "handling delayed sync"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Landroid/accounts/Account;

    invoke-direct {v3, v2, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$1;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$1;-><init>(Landroid/content/Context;Landroid/accounts/Account;)V

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v1, "ChromeBrowserSyncAdapter"

    const-string v2, "no delayed sync"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static final setDelayedSync(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "delayed_account"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "delayed_account_type"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method static shouldPerformSync(Landroid/content/Context;Landroid/os/Bundle;Landroid/accounts/Account;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "force"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "ChromeBrowserSyncAdapter"

    const-string v3, "manual sync requested"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeBaseActivity;->isChromeInForeground(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-static {p0, v4, v4}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;->setDelayedSync(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_1
    return v0

    :cond_1
    const-string v1, "ChromeBrowserSyncAdapter"

    const-string v2, "delaying sync"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;->setDelayedSync(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move v2, v0

    goto :goto_0
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2, p1}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;->shouldPerformSync(Landroid/content/Context;Landroid/os/Bundle;Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "objectId"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    const-string v3, ""

    :goto_2
    if-eqz v0, :cond_3

    const-wide/16 v4, 0x0

    :goto_3
    if-eqz v0, :cond_4

    const-string v6, ""

    :goto_4
    if-eqz v0, :cond_5

    const-string v0, "ChromeBrowserSyncAdapter"

    const-string v1, "Received sync tickle for all types in ChromeBrowserSyncAdapter"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;->mApplication:Landroid/app/Application;

    check-cast v2, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    :try_start_0
    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter$2;-><init>(Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;Lcom/google/android/apps/chrome/ChromeMobileApplication;Ljava/lang/String;JLjava/lang/String;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlocking(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ChromeBrowserSyncAdapter"

    const-string v2, "Got exception when trying to request a sync. Informing Android system."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const-string v1, "objectId"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_3
    const-string v1, "version"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    goto :goto_3

    :cond_4
    const-string v1, "payload"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    :cond_5
    const-string v0, "ChromeBrowserSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received sync tickle for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in ChromeBrowserSyncAdapter"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method
