.class Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;Landroid/app/Activity;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public tokenAvailable(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->access$000(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    # getter for: Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Could not create auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->access$200(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;->startupFailed()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->val$activity:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->val$activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    # getter for: Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Received auth token - starting sync setup"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->access$300(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->val$account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->syncSignInWithAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->access$300(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->addSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->syncStateChanged()V

    goto :goto_0
.end method
