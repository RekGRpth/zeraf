.class public Lcom/google/android/apps/chrome/sync/SyncStates;
.super Ljava/lang/Object;


# instance fields
.field private final mAutoLogin:Ljava/lang/Boolean;

.field private final mMasterSyncState:Ljava/lang/Boolean;

.field private final mSendToDevice:Ljava/lang/Boolean;

.field private final mSync:Ljava/lang/Boolean;

.field private final mWantedSyncState:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mSync:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mWantedSyncState:Ljava/lang/Boolean;

    iput-object p3, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mMasterSyncState:Ljava/lang/Boolean;

    iput-object p4, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mSendToDevice:Ljava/lang/Boolean;

    iput-object p5, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mAutoLogin:Ljava/lang/Boolean;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/chrome/sync/SyncStates$1;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/chrome/sync/SyncStates;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    return-void
.end method

.method public static create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public hasAutoLoginSet()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mAutoLogin:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMasterSyncState()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mMasterSyncState:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSendToDevice()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mSendToDevice:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSync()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mSync:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWantedSyncState()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mWantedSyncState:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAutoLoginEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mAutoLogin:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isMasterSyncStateEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mMasterSyncState:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isSendToDeviceEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mSendToDevice:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isSyncEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mSync:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isWantedSyncStateEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStates;->mWantedSyncState:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
