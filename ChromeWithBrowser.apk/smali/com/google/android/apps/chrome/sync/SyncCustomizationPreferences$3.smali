.class Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public startupComplete()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->destroySyncStartupHelper()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1400(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->showConfigure()V

    return-void
.end method

.method public startupFailed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->destroySyncStartupHelper()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1400(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;->val$activity:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;->val$activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    const-string v1, "spinner"

    # invokes: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->closeDialogIfOpen(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1500(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$3;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->closePage()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1600(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)V

    return-void
.end method
