.class Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver$1;->this$1:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver$1;->this$1:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;

    iget-object v0, v0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$700(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver$1;->this$1:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;

    iget-object v1, v1, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    # invokes: Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->setPreferencesEnabled(Z)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->access$1300(Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver$1;->this$1:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;

    iget-object v1, v1, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences$AndroidSyncSettingsObserver;->this$0:Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;->getSyncActionBarSwitch()Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    return-void
.end method
