.class public Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$Listener;
.implements Lcom/google/android/apps/chrome/sync/SignOutDialogFragment$Listener;
.implements Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;


# static fields
.field private static final ERROR_COLOR:Ljava/lang/String; = "red"

.field private static final PREFERENCE_DISABLE_DELAY:I = 0x7d0

.field public static final PREF_AUTO_LOGIN:Ljava/lang/String; = "auto_login"

.field public static final PREF_SEND_TO_DEVICE:Ljava/lang/String; = "send_to_device"

.field public static final PREF_SYNC:Ljava/lang/String; = "sync"

.field private static final SYNC_ACCOUNT_HELP_URL:Ljava/lang/String; = "https://support.google.com/chrome/?p=mobile_signin"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mApplicationContext:Landroid/content/Context;

.field private mAutologinPref:Landroid/preference/CheckBoxPreference;

.field private mEnablingSyncAndSendToDevice:Z

.field private mMainThreadHandler:Landroid/os/Handler;

.field private mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

.field private mSyncPref:Landroid/preference/Preference;

.field private mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

.field private mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;I)Landroid/text/Spannable;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->errorSummary(I)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method private displaySignOutDialog()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/sync/SignOutDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/SignOutDialogFragment;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/sync/SignOutDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/SignOutDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private errorSummary(I)Landroid/text/Spannable;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->errorSummary(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method static errorSummary(Ljava/lang/String;)Landroid/text/Spannable;
    .locals 4

    const/4 v3, 0x0

    const-string v0, "red"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    return-object v1
.end method

.method private listenForPreferenceChanges()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private setActionBarTitle()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method private setDefaultSyncPrefSummary()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f07024e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f07024f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setPreferencesEnabled(ZLandroid/accounts/Account;)V
    .locals 1

    const-string v0, "auto_login"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    const-string v0, "send_to_device"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    const-string v0, "sync"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    return-void
.end method

.method private stopListeningForPreferenceChanges()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private temporarilyDisablePreference(Landroid/preference/Preference;)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mMainThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$3;-><init>(Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;Landroid/preference/Preference;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private updateCheckedStatusForPreferences(Landroid/accounts/Account;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mEnablingSyncAndSendToDevice:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->isEnabled(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isAutoLoginEnabled(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method private updateSendToDeviceStatusDisplay()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f070084

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;-><init>(Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private updateSyncStatusDisplay()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    const v1, 0x7f07006f

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->errorSummary(I)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getAuthError()Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;->NONE:Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getAuthError()Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/GoogleServiceAuthError$State;->getMessage()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->errorSummary(I)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070092

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$4;->$SwitchMap$com$google$android$apps$chrome$sync$SyncDecryptionPassphraseType:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->getSyncDecryptionPassphraseTypeIfRequired()Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/SyncDecryptionPassphraseType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->setDefaultSyncPrefSummary()V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    const v1, 0x7f070071

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->errorSummary(I)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    const v1, 0x7f070070

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->errorSummary(I)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f060015

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mApplicationContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->setActionBarTitle()V

    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mMainThreadHandler:Landroid/os/Handler;

    const-string v0, "sync"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string v0, "send_to_device"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string v0, "auto_login"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f070086

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->updateSyncStatusDisplay()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->displaySignOutDialog()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0700c9

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "https://support.google.com/chrome/?p=mobile_signin"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->show(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0f0110
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPause()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->stopListeningForPreferenceChanges()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->removeSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->removeListener(Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$Listener;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    sget-object v0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPreferenceChange(...) "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " := "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/android/apps/chrome/sync/SyncStates;->create()Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    if-ne v1, p1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v1, v2, :cond_2

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->sendToDevice(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->build()Lcom/google/android/apps/chrome/sync/SyncStates;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->setStates(Lcom/google/android/apps/chrome/sync/SyncStates;Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    if-eq p1, v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->temporarilyDisablePreference(Landroid/preference/Preference;)V

    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v1, v2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;->autoLogin(Z)Lcom/google/android/apps/chrome/sync/SyncStates$SyncStatesBuilder;

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncPref:Landroid/preference/Preference;

    if-ne v0, p1, :cond_1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    const-class v1, Lcom/google/android/apps/chrome/sync/SyncCustomizationPreferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f07007f

    move-object v5, v4

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    move v6, v7

    :cond_0
    :goto_0
    return v6

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->startServiceWithWakeLock(Landroid/content/Context;Landroid/content/Intent;)V

    move v6, v7

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    const v0, 0x7f0f0110

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07008d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const v0, 0x7f0f0111

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->updateCheckedStatusForPreferences(Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->listenForPreferenceChanges()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->setPreferencesEnabled(ZLandroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->addSyncStateChangedListener(Lcom/google/android/apps/chrome/sync/SyncSetupManager$SyncStateChangedListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->updateSyncStatusDisplay()V

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->addListener(Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager$Listener;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->updateSendToDeviceStatusDisplay()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSignedOut()V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->setPreferencesEnabled(ZLandroid/accounts/Account;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->stopListeningForPreferenceChanges()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSyncSetupManager:Lcom/google/android/apps/chrome/sync/SyncSetupManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncSetupManager;->signOut()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$2;-><init>(Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->closeEditor(Landroid/app/Activity;Landroid/view/View;Landroid/app/FragmentManager;)V

    return-void
.end method

.method public snapshotStateChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->updateSendToDeviceStatusDisplay()V

    return-void
.end method

.method public syncStateChanged()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->updateSyncStatusDisplay()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mAccount:Landroid/accounts/Account;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->setPreferencesEnabled(ZLandroid/accounts/Account;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
