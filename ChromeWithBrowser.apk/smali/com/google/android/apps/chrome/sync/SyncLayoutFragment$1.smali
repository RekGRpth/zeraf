.class Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotListenerManager;->getSummaryResourceId(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->access$000(Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    const v1, 0x7f070084

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    # getter for: Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->access$000(Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->errorSummary(I)Landroid/text/Spannable;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;->access$100(Lcom/google/android/apps/chrome/sync/SyncLayoutFragment;I)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/SyncLayoutFragment$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
