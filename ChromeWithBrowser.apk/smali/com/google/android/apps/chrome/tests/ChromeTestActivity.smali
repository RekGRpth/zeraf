.class Lcom/google/android/apps/chrome/tests/ChromeTestActivity;
.super Landroid/app/Activity;


# static fields
.field private static final TAG:Ljava/lang/String; = "ChromeTestActivity"


# instance fields
.field protected mContentView:Lorg/chromium/content/browser/ContentView;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/data/local/chrome-command-line"

    invoke-static {v0}, Lorg/chromium/content/common/CommandLine;->initFromFile(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->ensureInitialized()V

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lorg/chromium/content/browser/AndroidBrowserProcess;->init(Landroid/content/Context;I)Z
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    invoke-static {v0}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(Z)I

    move-result v0

    new-instance v1, Lorg/chromium/ui/gfx/ActivityNativeWindow;

    invoke-direct {v1, p0}, Lorg/chromium/ui/gfx/ActivityNativeWindow;-><init>(Landroid/app/Activity;)V

    invoke-static {p0, v0, v1, v3}, Lorg/chromium/content/browser/ContentView;->newInstance(Landroid/content/Context;ILorg/chromium/ui/gfx/NativeWindow;I)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tests/ChromeTestActivity;->mContentView:Lorg/chromium/content/browser/ContentView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tests/ChromeTestActivity;->mContentView:Lorg/chromium/content/browser/ContentView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tests/ChromeTestActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ChromeTestActivity"

    const-string v2, "Can\'t initialize chromium browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tests/ChromeTestActivity;->finish()V

    goto :goto_0
.end method
