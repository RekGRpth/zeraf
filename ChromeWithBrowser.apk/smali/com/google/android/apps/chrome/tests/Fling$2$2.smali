.class Lcom/google/android/apps/chrome/tests/Fling$2$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/tests/Fling$2;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tests/Fling$2;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tests/Fling$2$2;->this$1:Lcom/google/android/apps/chrome/tests/Fling$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const-string v0, "ChromeTest"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fling fps: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/tests/Fling$2$2;->this$1:Lcom/google/android/apps/chrome/tests/Fling$2;

    iget-object v2, v2, Lcom/google/android/apps/chrome/tests/Fling$2;->this$0:Lcom/google/android/apps/chrome/tests/Fling;

    iget-object v2, v2, Lcom/google/android/apps/chrome/tests/Fling;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentView;->stopFpsProfiling()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
