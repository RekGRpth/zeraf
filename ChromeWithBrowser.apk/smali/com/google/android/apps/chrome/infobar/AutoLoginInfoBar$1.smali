.class Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;

.field final synthetic val$finalAccountName:Ljava/lang/String;

.field final synthetic val$finalAuthToken:Ljava/lang/String;

.field final synthetic val$finalResult:Ljava/lang/String;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;->this$0:Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar;

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;->val$finalAccountName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;->val$finalAuthToken:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;->val$success:Z

    iput-object p5, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;->val$finalResult:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "accountName"

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;->val$finalAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "authToken"

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;->val$finalAuthToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "success"

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;->val$success:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "result"

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginInfoBar$1;->val$finalResult:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x39

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(ILandroid/os/Bundle;)V

    return-void
.end method
