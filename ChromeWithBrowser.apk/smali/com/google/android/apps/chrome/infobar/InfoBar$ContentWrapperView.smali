.class Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;
.super Landroid/widget/LinearLayout;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/infobar/InfoBar;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/infobar/InfoBar;Landroid/content/Context;)V
    .locals 6

    const/16 v5, 0x10

    const/4 v2, 0x1

    const/4 v3, -0x2

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBar;

    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04001f

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;->setOrientation(I)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/infobar/InfoBar$1;->$SwitchMap$com$google$android$apps$chrome$infobar$InfoBar$BackgroundType:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/infobar/InfoBar;->getBackgroundType()Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/infobar/InfoBar$BackgroundType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    const v0, 0x7f0f0066

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/infobar/InfoBar;->getIconResourceId()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    :goto_1
    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v3, 0x3f800000

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {p1, p2}, Lcom/google/android/apps/chrome/infobar/InfoBar;->createContent(Landroid/content/Context;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v3, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/infobar/InfoBar;->shouldCenterIcon()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0f0065

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setGravity(I)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/infobar/InfoBar;->shouldShowCloseButton()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0f0067

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    new-instance v1, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView$1;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBar$ContentWrapperView;Lcom/google/android/apps/chrome/infobar/InfoBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    return-void

    :pswitch_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02008c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02008e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/infobar/InfoBar;->getIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
