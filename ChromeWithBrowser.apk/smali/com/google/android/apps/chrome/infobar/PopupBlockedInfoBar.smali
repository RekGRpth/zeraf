.class public Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;
.super Lcom/google/android/apps/chrome/infobar/InfoBar;


# instance fields
.field private mBlockedPopupFormatString:Ljava/lang/String;

.field private mListener:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;

.field private mPopupsBlockedTextView:Landroid/widget/TextView;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/infobar/InfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mUrl:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;)Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mUrl:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public createContent(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07013a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mBlockedPopupFormatString:Ljava/lang/String;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04001d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0f0061

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mPopupsBlockedTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->updateBlockedPopupText()V

    const v0, 0x7f0f005f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v2, 0x7f07013b

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    new-instance v2, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar$1;-><init>(Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method protected getIconResourceId()Ljava/lang/Integer;
    .locals 1

    const v0, 0x7f02000d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public updateBlockedPopupText()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mBlockedPopupFormatString:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mPopupsBlockedTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mBlockedPopupFormatString:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;

    invoke-interface {v4}, Lcom/google/android/apps/chrome/infobar/PopupBlockedInfoBarListener;->getBlockedPopupCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
