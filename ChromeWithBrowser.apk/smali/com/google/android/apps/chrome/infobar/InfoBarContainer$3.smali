.class Lcom/google/android/apps/chrome/infobar/InfoBarContainer$3;
.super Landroid/animation/AnimatorListenerAdapter;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

.field final synthetic val$animatorSet:Landroid/animation/AnimatorSet;

.field final synthetic val$cleanUpInfobarRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/infobar/InfoBarContainer;Landroid/animation/AnimatorSet;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$3;->this$0:Lcom/google/android/apps/chrome/infobar/InfoBarContainer;

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$3;->val$animatorSet:Landroid/animation/AnimatorSet;

    iput-object p3, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$3;->val$cleanUpInfobarRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$3;->val$animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    check-cast v0, Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/InfoBarContainer$3;->val$cleanUpInfobarRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method
