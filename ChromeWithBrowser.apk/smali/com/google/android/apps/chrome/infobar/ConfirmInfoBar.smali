.class public Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;
.super Lcom/google/android/apps/chrome/infobar/InfoBar;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCancelButtonText:Ljava/lang/String;

.field private mIsSingleLine:Z

.field private mListener:Lcom/google/android/apps/chrome/infobar/ConfirmInfoBarListener;

.field private mOkButtonText:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/infobar/ConfirmInfoBarListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/infobar/InfoBar;-><init>(Lcom/google/android/apps/chrome/infobar/InfoBarDismissedListener;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/ConfirmInfoBarListener;

    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mOkButtonText:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mCancelButtonText:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mTitle:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method private initButton(Landroid/view/View;ILjava/lang/String;Z)V
    .locals 2

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-nez p3, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar$1;

    invoke-direct {v1, p0, p4}, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar$1;-><init>(Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public createContent(Landroid/content/Context;)Landroid/view/View;
    .locals 4

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04001e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0f0061

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0f0064

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mOkButtonText:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->initButton(Landroid/view/View;ILjava/lang/String;Z)V

    const v0, 0x7f0f0063

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mCancelButtonText:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->initButton(Landroid/view/View;ILjava/lang/String;Z)V

    invoke-static {p1}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mIsSingleLine:Z

    return-object v1
.end method

.method protected getIconBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method notifyConfirmation(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/ConfirmInfoBarListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mListener:Lcom/google/android/apps/chrome/infobar/ConfirmInfoBarListener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBarListener;->onConfirmInfoBarButtonClicked(Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;Z)V

    :cond_0
    return-void
.end method

.method protected shouldCenterIcon()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/infobar/ConfirmInfoBar;->mIsSingleLine:Z

    return v0
.end method
