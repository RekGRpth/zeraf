.class public Lcom/google/android/apps/chrome/ToolbarDelegate;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/google/android/apps/chrome/OnNativeLibraryReadyUIListener;


# static fields
.field protected static final BACKGROUND_TRANSITION_DURATION_MS:I = 0x190

.field private static final MINIMUM_LOAD_PROGRESS:I = 0x5

.field private static notifications:[I


# instance fields
.field private mFullscreenFindInPageToken:I

.field private mFullscreenFocusToken:I

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

.field private mFullscreenMenuToken:I

.field private mFullscreenProgressToken:I

.field protected mIncognitoTab:Ljava/lang/Boolean;

.field protected mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

.field protected mMenuButton:Landroid/widget/ImageButton;

.field private mNativeLibraryReady:Z

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mPreselectedTabIndex:I

.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

.field private final mToolbarView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/ToolbarDelegate;->notifications:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0xf
        0x2
        0x3
        0x5
        0x6
        0x7
        0x8
        0x9
        0xb
        0xa
        0xc
        0x19
        0x15
        0x12
        0x2a
        0x3b
        0x2d
        0x36
        0x37
        0x3e
    .end array-data
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFocusToken:I

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenProgressToken:I

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFindInPageToken:I

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenMenuToken:I

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mPreselectedTabIndex:I

    new-instance v0, Lcom/google/android/apps/chrome/ToolbarDelegate$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ToolbarDelegate$1;-><init>(Lcom/google/android/apps/chrome/ToolbarDelegate;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mToolbarView:Landroid/view/View;

    const v0, 0x7f0f0074

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mMenuButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f00c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/LocationBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/LocationBar;->setToolbar(Lcom/google/android/apps/chrome/ToolbarDelegate;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mNativeLibraryReady:Z

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/ToolbarDelegate;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mPreselectedTabIndex:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ToolbarDelegate;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateCurrentTabDisplayStatus()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/ToolbarDelegate;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFindInPageToken:I

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/chrome/ToolbarDelegate;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFindInPageToken:I

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/ToolbarDelegate;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenMenuToken:I

    return v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/chrome/ToolbarDelegate;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenMenuToken:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ToolbarDelegate;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onTabCrash(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ToolbarDelegate;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateCurrentTabDisplayStatus()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ToolbarDelegate;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabLoadingState(IZZ)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ToolbarDelegate;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onPageLoadFinished(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ToolbarDelegate;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateLoadProgress(II)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ToolbarDelegate;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabTitle(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/TabModelSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/ToolbarDelegate;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    return-object v0
.end method

.method private onPageLoadFinished(I)V
    .locals 4

    const/16 v3, 0x64

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    if-eq p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabLoadingState(IZZ)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v0

    if-eq v0, v3, :cond_2

    const/4 v1, 0x5

    if-le v0, v1, :cond_3

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateLoadProgress(II)V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenProgressToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->hideControlsPersistent(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenProgressToken:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->clearLoadProgressIndicator()V

    goto :goto_1
.end method

.method private onTabCrash(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1, v1, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabLoadingState(IZZ)V

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateLoadProgress(II)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->clearLoadProgressIndicator()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenProgressToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsPersistentAndClearOldToken(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenProgressToken:I

    goto :goto_0
.end method

.method private onTabPrefetchCommitted()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateCurrentTabDisplayStatus()V

    return-void
.end method

.method private onTabSelected()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateCurrentTabDisplayStatus()V

    return-void
.end method

.method private updateCurrentTabDisplayStatus()V
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->setUrlToPageUrl()V

    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->clearLoadProgressIndicator()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->isShowingInterstitialPage()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v3

    invoke-direct {p0, v3, v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabLoadingState(IZZ)V

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabTitle(ILjava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-eq v0, v1, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->isShowingInterstitialPage()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->clearLoadProgressIndicator()V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/bridge/AutocompleteController;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/bridge/AutocompleteController;->setProfile(Lcom/google/android/apps/chrome/Tab;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateLoadProgress(II)V

    goto :goto_2
.end method

.method private updateLoadProgress(II)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x5

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/LocationBar;->updateLoadProgress(I)V

    const/16 v1, 0x64

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateReloadState(Z)V

    goto :goto_0
.end method

.method private updateReloadState(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateReloadButtonVisibility(Z)V

    return-void

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isShowingInterstitialPage()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mNativeLibraryReady:Z

    if-nez v0, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private updateTabLoadingState(IZZ)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    if-eq p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/apps/chrome/LocationBar;->updateLoadingState(ZZ)V

    if-eqz p2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getProgress()I

    move-result v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateLoadProgress(II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenProgressToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsPersistentAndClearOldToken(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenProgressToken:I

    goto :goto_0
.end method

.method private updateTabTitle(ILjava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    if-eq p1, v0, :cond_0

    :cond_0
    return-void
.end method


# virtual methods
.method public back()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->goBack()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected cancelPrerender()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->cancelPrerender()V

    :cond_0
    return-void
.end method

.method public forward()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->canGoForward()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->goForward()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getActivity()Lcom/google/android/apps/chrome/Main;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getToolbarView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Main;

    return-object v0
.end method

.method public getCurrentTab()Lcom/google/android/apps/chrome/Tab;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mPreselectedTabIndex:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mPreselectedTabIndex:I

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->index()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/TabModel;->getTab(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method protected getCurrentView()Lorg/chromium/content/browser/ContentView;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLocationBar()Lcom/google/android/apps/chrome/LocationBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    return-object v0
.end method

.method protected getToolbarView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mToolbarView:Landroid/view/View;

    return-object v0
.end method

.method protected handleFindToolbarStateChange(Z)V
    .locals 0

    return-void
.end method

.method protected initializeTabStackVisuals()V
    .locals 0

    return-void
.end method

.method protected isOverviewMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onAccessibilityStatusChanged(Z)V
    .locals 0

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onTabModelSelected(Z)V

    :cond_0
    return-void
.end method

.method protected onBookmarkUiVisibilityChange(Z)V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mMenuButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getActivity()Lcom/google/android/apps/chrome/Main;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/Main;->showPopupMenu(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->toolbarShowMenu()V

    :cond_0
    return-void
.end method

.method protected onDefaultSearchEngineChanged()V
    .locals 0

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/ToolbarDelegate;->notifications:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onNativeLibraryReady()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mNativeLibraryReady:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->onNativeLibraryReady()V

    sget-object v0, Lcom/google/android/apps/chrome/ToolbarDelegate;->notifications:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar;->updateMicButtonState()V

    return-void
.end method

.method protected onStateRestored()V
    .locals 0

    return-void
.end method

.method protected onTabModelSelected(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mIncognitoTab:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mIncognitoTab:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mIncognitoTab:Ljava/lang/Boolean;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateCurrentTabDisplayStatus()V

    goto :goto_0
.end method

.method protected onUrlFocusChange(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFocusToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsPersistentAndClearOldToken(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFocusToken:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFocusToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->hideControlsPersistent(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenFocusToken:I

    goto :goto_0
.end method

.method protected prerenderUrl(Ljava/lang/String;I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/Tab;->prerenderUrl(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public setActionModeCallbackForTextEdit(Landroid/view/ActionMode$Callback;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    return-void
.end method

.method public setBookmarkClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 0

    return-void
.end method

.method public setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    return-void
.end method

.method public setOnNewTabClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 0

    return-void
.end method

.method public setOnOverviewClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 0

    return-void
.end method

.method protected setOverviewMode(Z)V
    .locals 0

    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->onTabModelSelected(Z)V

    return-void
.end method

.method protected shouldShowMenuButton()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mToolbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected stopOrReloadCurrentTab()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->stopLoading()V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus()V

    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->reload()V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/UmaRecordAction;->reload(Z)V

    goto :goto_0
.end method

.method protected updateBackButtonVisibility(Z)V
    .locals 0

    return-void
.end method

.method protected updateBookmarkButtonVisibility(Z)V
    .locals 0

    return-void
.end method

.method public updateButtonStatus()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateButtonStatus(Z)V

    return-void
.end method

.method public updateButtonStatus(Z)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v4

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateBackButtonVisibility(Z)V

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentView;->canGoForward()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateForwardButtonVisibility(Z)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateReloadState(Z)V

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Tab;->getBookmarkId()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateBookmarkButtonVisibility(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->shouldShowMenuButton()Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    const/16 v2, 0x8

    goto :goto_3
.end method

.method protected updateForwardButtonVisibility(Z)V
    .locals 0

    return-void
.end method

.method protected updateReloadButtonVisibility(Z)V
    .locals 0

    return-void
.end method

.method public updateTabCount()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarDelegate;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ToolbarDelegate;->updateTabCountVisuals(I)V

    return-void
.end method

.method protected updateTabCountVisuals(I)V
    .locals 0

    return-void
.end method
