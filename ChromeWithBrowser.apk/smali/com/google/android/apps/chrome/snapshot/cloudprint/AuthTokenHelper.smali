.class public Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final AUTH_TOKEN_TYPE:Ljava/lang/String; = "cloudprint"

.field private static final BACKOFF_MAX_MS:I = 0x36ee80

.field private static final BACKOFF_MS:I = 0xea60

.field private static final PREF_ACQUIRING_AUTH_TOKEN:Ljava/lang/String; = "authTokenState"

.field private static final PREF_PACKAGE_NAME:Ljava/lang/String; = "AuthTokenHelper"

.field private static final TAG:Ljava/lang/String; = "AuthTokenHelper"

.field private static sAuthTokenState:Ljava/lang/Boolean;

.field private static sAuthTokenStateMonitor:Ljava/lang/Object;


# instance fields
.field protected mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->$assertionsDisabled:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->sAuthTokenState:Ljava/lang/Boolean;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->sAuthTokenStateMonitor:Ljava/lang/Object;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    const-string v1, "AuthTokenHelper"

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    const-wide/32 v3, 0xea60

    const-wide/32 v5, 0x36ee80

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->createBackoffScheduler(Ljava/lang/String;Landroid/content/Context;JJ)V

    return-void
.end method

.method public static getAcquiringAuthToken(Landroid/content/Context;)Z
    .locals 4

    sget-boolean v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->sAuthTokenStateMonitor:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->sAuthTokenState:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    const-string v0, "AuthTokenHelper"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "authTokenState"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->sAuthTokenState:Ljava/lang/Boolean;

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->sAuthTokenState:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static invalidateAndAcquireNewAuthToken(Landroid/content/Context;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "AuthTokenHelper"

    const-string v3, "Invalidating auth token for cloudprint"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v1, "AuthTokenHelper"

    const-string v2, "Cannot invalidate an auth token. Account is not known"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getAuthToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v4

    const-string v5, "cloudprint"

    invoke-virtual {v4, v2, v3, v5}, Lorg/chromium/sync/signin/AccountManagerHelper;->getNewAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setAuthToken(Landroid/content/Context;Ljava/lang/String;)V

    if-eqz v2, :cond_2

    move v0, v1

    :cond_2
    if-nez v0, :cond_0

    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNeedsUpdating(Landroid/content/Context;Z)V

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private isAuthTokenAvailable()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getAuthToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private scheduleGetAuthToken(Landroid/content/Intent;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->createAlarm(Landroid/content/Intent;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-string v2, "AuthTokenHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": Failed to get CloudPrint auth token.  Rescheduling in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private setAcquiringAuthToken(Z)V
    .locals 4

    sget-boolean v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->sAuthTokenStateMonitor:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->sAuthTokenState:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    const-string v2, "AuthTokenHelper"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "authTokenState"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private tryToGetAuthToken(Landroid/accounts/Account;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    const-string v1, "cloudprint"

    invoke-virtual {v0, p1, v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromBackground(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setAuthToken(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public acquireAuthToken(Landroid/accounts/Account;Landroid/content/Intent;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->tryToGetAuthToken(Landroid/accounts/Account;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->isAuthTokenAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->resetFailedAttempts()V

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->setAcquiringAuthToken(Z)V

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->setAcquiringAuthToken(Z)V

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->scheduleGetAuthToken(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->increaseFailedAttempts()V

    move v0, v1

    goto :goto_0
.end method

.method public createBackoffScheduler(Ljava/lang/String;Landroid/content/Context;JJ)V
    .locals 7

    new-instance v0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;-><init>(Ljava/lang/String;Landroid/content/Context;JJ)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    return-void
.end method

.method public stopAcquiringAuthToken(Landroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->setAcquiringAuthToken(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->cancelAlarm(Landroid/content/Intent;)Z

    return-void
.end method
