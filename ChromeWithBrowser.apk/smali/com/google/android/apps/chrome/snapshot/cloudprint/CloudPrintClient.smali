.class public Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;
.super Ljava/lang/Object;


# static fields
.field static final DELETEJOB_FAILED:Ljava/lang/String; = "Job delete failed: "

.field static final FAILED_TO_UPDATE_PRINT_JOB_STATUS:Ljava/lang/String; = "Failed to update print job status: "

.field static final FETCH_JOBS_FAILED:Ljava/lang/String; = "Fetch print jobs failed: "

.field static final PRINTER_CHECK_FAILED:Ljava/lang/String; = "Printer data fetching failed: "

.field static final PRINTER_DELETE_FAILED:Ljava/lang/String; = "Printer delete failed: "

.field static final PRINTER_REGISTRATION_CHECK_FAILED:Ljava/lang/String; = "Printer registration check failed: "

.field static final PRINTER_REGISTRATION_FAILED:Ljava/lang/String; = "Printer registration failed: "

.field static final PRINTER_UPDATE_FAILED:Ljava/lang/String; = "Printer update failed: "

.field private static final TAG:Ljava/lang/String; = "CloudPrintClient"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static controlJob(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Z
    .locals 3

    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-static {p2, p3}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getControlJobInterfaceUrl(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->setRequestHeaders(Lorg/apache/http/HttpMessage;Landroid/content/Context;)V

    const/4 v1, 0x0

    :try_start_0
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/json/HttpRequestExecutor;->execute(Lorg/apache/http/client/methods/HttpRequestBase;Landroid/content/SyncResult;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/apache/http/auth/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "CloudPrintClient"

    const-string v1, "Failed to update print job status: request failed."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;

    const-string v2, "Failed to update print job status: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    invoke-static {v0, p3}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->parseControlResponse(Lorg/json/JSONObject;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Z

    move-result v0

    goto :goto_0
.end method

.method static createDownloadRequest(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;
    .locals 4

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getHeaders(Landroid/content/Context;)Ljava/util/List;
    :try_end_0
    .catch Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    const-string v0, "Accept"

    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/snapshot/HeaderField;->create(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/HeaderField;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->setHeaders(Ljava/util/List;)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "CloudPrintClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to create download request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static delete(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p2, :cond_0

    const-string v1, "CloudPrintClient"

    const-string v2, "Printer delete failed: printer id not found"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getDeleteInterfaceUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    invoke-static {v1, p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->setRequestHeaders(Lorg/apache/http/HttpMessage;Landroid/content/Context;)V

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1, v2, p1}, Lcom/google/android/apps/chrome/snapshot/json/HttpRequestExecutor;->execute(Lorg/apache/http/client/methods/HttpRequestBase;Landroid/content/SyncResult;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/apache/http/auth/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "CloudPrintClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Printer delete failed: printer id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;

    const-string v2, "Printer delete failed: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    const-string v0, ""

    const-string v2, "Failed to delete printer: "

    const-string v3, "Printer delete failed: failed to parse Cloud Print response."

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->parseResponseSimple(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method static deleteJob(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Z
    .locals 4

    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getDeleteJobInterfaceUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->setRequestHeaders(Lorg/apache/http/HttpMessage;Landroid/content/Context;)V

    const/4 v1, 0x0

    :try_start_0
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/json/HttpRequestExecutor;->execute(Lorg/apache/http/client/methods/HttpRequestBase;Landroid/content/SyncResult;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/apache/http/auth/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "CloudPrintClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Job delete failed: jobid = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;

    const-string v2, "Job delete failed: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    const-string v1, ""

    const-string v2, "Failed to delete jobid: "

    const-string v3, "Job delete failed: failed to parse Cloud Print response."

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->parseResponseSimple(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method static fetchJobs(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Ljava/util/Set;
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPrinterId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getFetchInterfaceUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-static {v1, p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->setRequestHeaders(Lorg/apache/http/HttpMessage;Landroid/content/Context;)V

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1, v2, p1}, Lcom/google/android/apps/chrome/snapshot/json/HttpRequestExecutor;->execute(Lorg/apache/http/client/methods/HttpRequestBase;Landroid/content/SyncResult;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/apache/http/auth/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "CloudPrintClient"

    const-string v2, "Fetch print jobs failed: request failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;

    const-string v2, "Fetch print jobs failed: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->parseFetchResponse(Lorg/json/JSONObject;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method static find(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-static {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getPrinterInterfaceUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-static {v1, p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->setRequestHeaders(Lorg/apache/http/HttpMessage;Landroid/content/Context;)V

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1, v2, p1}, Lcom/google/android/apps/chrome/snapshot/json/HttpRequestExecutor;->execute(Lorg/apache/http/client/methods/HttpRequestBase;Landroid/content/SyncResult;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/apache/http/auth/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "CloudPrintClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Printer data fetching failed: Unable to find printer with id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;

    const-string v2, "Printer data fetching failed: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    invoke-static {v1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->parsePrinterResponse(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    goto :goto_0
.end method

.method private static getControlJobInterfaceUrl(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Ljava/lang/String;
    .locals 3

    const/16 v2, 0x26

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.google.com/cloudprint/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "control"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "jobid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->getStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "client=chrome-to-mobile-android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDeleteInterfaceUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.google.com/cloudprint/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "delete"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "printerid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "client=chrome-to-mobile-android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDeleteJobInterfaceUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.google.com/cloudprint/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "deletejob"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "jobid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "client=chrome-to-mobile-android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getFetchInterfaceUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.google.com/cloudprint/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "fetch"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "printerid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "client=chrome-to-mobile-android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getHeaders(Landroid/content/Context;)Ljava/util/List;
    .locals 5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getAuthToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;

    const-string v1, "Unable to find Cloud Print auth token."

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GoogleLogin auth="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/snapshot/HeaderField;->create(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/HeaderField;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "X-CloudPrint-Proxy"

    const-string v2, "ChromeMobile"

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/snapshot/HeaderField;->create(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/HeaderField;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static getListInterfaceUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.google.com/cloudprint/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "list"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "proxy"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "client=chrome-to-mobile-android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getPrinterInterfaceUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.google.com/cloudprint/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "printer"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "printerid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "client=chrome-to-mobile-android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getRegisterInterfaceUrl()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.google.com/cloudprint/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "register"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "client=chrome-to-mobile-android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getResponseErrorMessage(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    if-nez p0, :cond_1

    const-string v0, "No reponse returned."

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "success"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "Invalid response, no success value."

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const-string v0, "message"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "No message found."

    goto :goto_0
.end method

.method private static getUpdateInterfaceUrl()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.google.com/cloudprint/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "update"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "client=chrome-to-mobile-android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static list(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Ljava/util/List;
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-static {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getListInterfaceUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-static {v1, p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->setRequestHeaders(Lorg/apache/http/HttpMessage;Landroid/content/Context;)V

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1, v2, p1}, Lcom/google/android/apps/chrome/snapshot/json/HttpRequestExecutor;->execute(Lorg/apache/http/client/methods/HttpRequestBase;Landroid/content/SyncResult;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/apache/http/auth/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "CloudPrintClient"

    const-string v2, "Printer registration check failed: Unable to list printers"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;

    const-string v2, "Printer registration check failed: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    invoke-static {v1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->parseListResponse(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static parseControlResponse(Lorg/json/JSONObject;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Z
    .locals 4

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getResponseErrorMessage(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "CloudPrintClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to mark job "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "CloudPrintClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to mark job "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static parseFetchResponse(Lorg/json/JSONObject;)Ljava/util/Set;
    .locals 6

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getResponseErrorMessage(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "CloudPrintClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fetch print jobs failed: request failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "jobs"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobParser;->parsePrintJob(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;

    move-result-object v3

    if-nez v3, :cond_2

    const-string v3, "CloudPrintClient"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to parse job at index "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "CloudPrintClient"

    const-string v2, "Fetch print jobs failed: failed to parse Cloud Print response."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static parseListResponse(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;
    .locals 8

    const/4 v0, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getResponseErrorMessage(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v1, "CloudPrintClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Printer registration check failed:  request failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    const-string v2, "printers"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v2, v4, :cond_5

    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->parse(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v4

    if-nez p1, :cond_3

    const-string v5, "CloudPrintClient"

    const-string v6, "Printer registration check failed: Unable to verify proxy id. Local proxy id not set"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_2
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_4

    const-string v4, "CloudPrintClient"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Printer id null for printer at index "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getProxy()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "CloudPrintClient"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Printer registration check failed: request and response proxies don\'t match for printer with index "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " != "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getProxy()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    const-string v2, "CloudPrintClient"

    const-string v3, "Printer registration check failed: failed to parse Cloud Print response."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :cond_4
    :try_start_1
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private static parsePrinterResponse(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getResponseErrorMessage(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "CloudPrintClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Printer data fetching failed:  request failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "printers"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    const-string v2, "CloudPrintClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Printer data fetching failed:  too many printers returned: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->parse(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v1, "CloudPrintClient"

    const-string v2, "Printer id null for printer at index 0"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "CloudPrintClient"

    const-string v3, "Printer data fetching failed: failed to parse Cloud Print response."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private static parseRegisterResponse(Lorg/json/JSONObject;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getResponseErrorMessage(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "CloudPrintClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to register printer: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "printers"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    :cond_1
    const-string v1, "CloudPrintClient"

    const-string v2, "Printer registration failed: Unexpected length for printers section."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "CloudPrintClient"

    const-string v3, "Printer registration failed: failed to parse Cloud Print response."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->parse(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->hasId()Z

    move-result v2

    if-nez v2, :cond_3

    const-string v1, "CloudPrintClient"

    const-string v2, "Unable to find printer id in registration response"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v2, "CloudPrintClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Device successfully registered with proxy id=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getProxy()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' and printerId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v1

    goto :goto_0
.end method

.method private static parseResponseSimple(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getResponseErrorMessage(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "CloudPrintClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    const-string v1, "CloudPrintClient"

    invoke-static {v1, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "CloudPrintClient"

    invoke-static {v2, p3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static register(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getRegisterInterfaceUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    invoke-static {v1, p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->setRequestHeaders(Lorg/apache/http/HttpMessage;Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-static {p0, v2, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->toMultipartEntity(Landroid/content/Context;ZLcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/a/a/a;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v1, "CloudPrintClient"

    const-string v2, "Failed to create registration parameters"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1, v2, p1}, Lcom/google/android/apps/chrome/snapshot/json/HttpRequestExecutor;->execute(Lorg/apache/http/client/methods/HttpRequestBase;Landroid/content/SyncResult;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/apache/http/auth/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "CloudPrintClient"

    const-string v2, "Printer registration failed: request failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;

    const-string v2, "Printer registration failed: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    invoke-static {v1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->parseRegisterResponse(Lorg/json/JSONObject;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    goto :goto_0
.end method

.method private static setRequestHeaders(Lorg/apache/http/HttpMessage;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getHeaders(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/HeaderField;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/HeaderField;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/HeaderField;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v2, v0}, Lorg/apache/http/HttpMessage;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static update(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->hasId()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CloudPrintClient"

    const-string v2, "Printer update failed: printer id not found"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->getUpdateInterfaceUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    invoke-static {v1, p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->setRequestHeaders(Lorg/apache/http/HttpMessage;Landroid/content/Context;)V

    const/4 v2, 0x1

    invoke-static {p0, v2, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->toMultipartEntity(Landroid/content/Context;ZLcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/a/a/a;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v1, "CloudPrintClient"

    const-string v2, "Failed to create update parameters"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1, v2, p1}, Lcom/google/android/apps/chrome/snapshot/json/HttpRequestExecutor;->execute(Lorg/apache/http/client/methods/HttpRequestBase;Landroid/content/SyncResult;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/apache/http/auth/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, "CloudPrintClient"

    const-string v2, "Printer update failed: request failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;

    const-string v2, "Printer update failed: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    const-string v0, "Device successfully updated"

    const-string v2, "Failed to update printer: "

    const-string v3, "Printer update failed: failed to parse Cloud Print response."

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->parseResponseSimple(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
