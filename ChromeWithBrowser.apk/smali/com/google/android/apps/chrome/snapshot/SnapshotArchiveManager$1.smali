.class synthetic Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $SwitchMap$com$google$android$apps$chrome$snapshot$PageUrlJobState:[I

.field static final synthetic $SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

.field static final synthetic $SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotState:[I

.field static final synthetic $SwitchMap$com$google$android$apps$chrome$snapshot$cloudprint$PrintJobStatus:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->values()[Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$PageUrlJobState:[I

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$PageUrlJobState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->READY:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_d

    :goto_0
    :try_start_1
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$PageUrlJobState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->DELETION_PENDING:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_c

    :goto_1
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->values()[Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    :try_start_2
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOAD_PENDING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_b

    :goto_2
    :try_start_3
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_a

    :goto_3
    :try_start_4
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_9

    :goto_4
    :try_start_5
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADED_AND_OPENED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_8

    :goto_5
    :try_start_6
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_7

    :goto_6
    :try_start_7
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ERROR:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_6

    :goto_7
    :try_start_8
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->FINISHED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_5

    :goto_8
    :try_start_9
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNKNOWN:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_4

    :goto_9
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->values()[Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotState:[I

    :try_start_a
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->CREATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_3

    :goto_a
    :try_start_b
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotState:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->UPDATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_2

    :goto_b
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->values()[Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$cloudprint$PrintJobStatus:[I

    :try_start_c
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$cloudprint$PrintJobStatus:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->DONE:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1

    :goto_c
    :try_start_d
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$cloudprint$PrintJobStatus:[I

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->ERROR:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_0

    :goto_d
    return-void

    :catch_0
    move-exception v0

    goto :goto_d

    :catch_1
    move-exception v0

    goto :goto_c

    :catch_2
    move-exception v0

    goto :goto_b

    :catch_3
    move-exception v0

    goto :goto_a

    :catch_4
    move-exception v0

    goto :goto_9

    :catch_5
    move-exception v0

    goto :goto_8

    :catch_6
    move-exception v0

    goto :goto_7

    :catch_7
    move-exception v0

    goto :goto_6

    :catch_8
    move-exception v0

    goto :goto_5

    :catch_9
    move-exception v0

    goto/16 :goto_4

    :catch_a
    move-exception v0

    goto/16 :goto_3

    :catch_b
    move-exception v0

    goto/16 :goto_2

    :catch_c
    move-exception v0

    goto/16 :goto_1

    :catch_d
    move-exception v0

    goto/16 :goto_0
.end method
