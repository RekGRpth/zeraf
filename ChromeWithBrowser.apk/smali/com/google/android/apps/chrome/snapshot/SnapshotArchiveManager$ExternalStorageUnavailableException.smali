.class Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageUnavailableException;
.super Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;


# direct methods
.method private constructor <init>(Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unable to use mounted media. ExternalStorageState = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageUnavailableException;-><init>(Ljava/lang/String;I)V

    return-void
.end method
