.class public interface abstract Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;
.super Ljava/lang/Object;


# virtual methods
.method public abstract enqueue(Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;)J
.end method

.method public abstract getUriForDownloadedFile(J)Landroid/net/Uri;
.end method

.method public abstract queryByDownloadId(J)Lcom/google/android/apps/chrome/snapshot/DownloadInfo;
.end method
