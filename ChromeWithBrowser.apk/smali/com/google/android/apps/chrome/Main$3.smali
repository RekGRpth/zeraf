.class Lcom/google/android/apps/chrome/Main$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Main;Landroid/app/Activity;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$3;->this$0:Lcom/google/android/apps/chrome/Main;

    iput-object p2, p0, Lcom/google/android/apps/chrome/Main$3;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/chrome/Main$3;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$3;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1200(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$3;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$3;->val$account:Landroid/accounts/Account;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;-><init>(Landroid/app/Activity;Landroid/accounts/Account;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/GoogleAutoLogin;->start()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$3;->val$activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->showSyncSignInNotification(Landroid/content/Context;)V

    goto :goto_0
.end method
