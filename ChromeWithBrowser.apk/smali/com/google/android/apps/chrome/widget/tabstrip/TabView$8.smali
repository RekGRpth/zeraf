.class Lcom/google/android/apps/chrome/widget/tabstrip/TabView$8;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$8;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$8;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->access$500(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->getHitRect(Landroid/graphics/Rect;)V

    iget v1, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v1, v1, -0x5

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v1, v1, 0x5

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0x7

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget v1, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v1, v1, 0x7

    iput v1, v0, Landroid/graphics/Rect;->right:I

    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$8;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->access$500(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$8;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->access$500(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$8;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->access$500(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    :cond_0
    return-void
.end method
