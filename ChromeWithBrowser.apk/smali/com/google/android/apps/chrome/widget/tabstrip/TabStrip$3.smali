.class Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;
.super Landroid/os/Handler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->invalidate()V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
