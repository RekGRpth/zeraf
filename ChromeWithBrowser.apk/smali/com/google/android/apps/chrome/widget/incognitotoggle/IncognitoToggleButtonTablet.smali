.class public Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;
.super Landroid/widget/ImageButton;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xc
        0x5
        0x2
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$3;-><init>(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)Lcom/google/android/apps/chrome/TabModelSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->updateButtonResource()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->updateButtonVisibility()V

    return-void
.end method

.method private updateButtonResource()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f07020e

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f020030

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setImageResource(I)V

    goto :goto_0

    :cond_2
    const v0, 0x7f07020d

    goto :goto_1

    :cond_3
    const v0, 0x7f020031

    goto :goto_2
.end method

.method private updateButtonVisibility()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$2;-><init>(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-super {p0}, Landroid/widget/ImageButton;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-super {p0}, Landroid/widget/ImageButton;->onDetachedFromWindow()V

    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/ImageButton;->onFinishInflate()V

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setVisibility(I)V

    new-instance v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$1;-><init>(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->updateButtonResource()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->updateButtonVisibility()V

    :cond_0
    return-void
.end method
