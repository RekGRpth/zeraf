.class public Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;
.super Ljava/lang/Object;


# static fields
.field private static final INVALID_TAB_ID:I = -0x1

.field private static NOTIFICATIONS:[I = null

.field private static final RUNNABLE_DELAY_MS:I = 0x12c

.field private static final SEARCH_BUBBLE_BOUNCE_DURATION_MS:I = 0x79e

.field static final STRING_ID:Ljava/lang/String; = "String_Id"

.field private static final TABS_BUBBLE_BOUNCE_DURATION_MS:I = 0x7d0

.field static final VIEW_ID:Ljava/lang/String; = "View_Id"

.field private static sTooltipsInstance:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;


# instance fields
.field private final mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mHandler:Landroid/os/Handler;

.field private mMain:Lcom/google/android/apps/chrome/Main;

.field private final mShowTooltipsRunnable:Ljava/lang/Runnable;

.field private mShowing:Z

.field private mTabPreselected:Z

.field private final mTooltips:Ljava/util/ArrayList;

.field private final mTooltipsInfo:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->NOTIFICATIONS:[I

    return-void

    :array_0
    .array-data 4
        0x36
        0x1a
        0x3
        0x5
        0x0
        0x1
        0x11
        0x15
        0x12
    .end array-data
.end method

.method protected constructor <init>(Lcom/google/android/apps/chrome/Main;)V
    .locals 12

    const v7, 0x7f0c0005

    const v8, 0x7f080084

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltipsInfo:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    iput-boolean v4, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowing:Z

    iput-boolean v4, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTabPreselected:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$1;-><init>(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowTooltipsRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;-><init>(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    iget-object v11, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltipsInfo:Ljava/util/ArrayList;

    const v1, 0x7f0f00c8

    const v2, 0x7f02003b

    const v3, 0x7f02003d

    const v6, 0x7f0701ad

    const v9, 0x7f080088

    const/16 v10, 0x79e

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->addBubbleInfo(IIIZZIIIII)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v11, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltipsInfo:Ljava/util/ArrayList;

    const v1, 0x7f0f006f

    const v2, 0x7f02003c

    const v3, 0x7f02003e

    const v6, 0x7f0701ae

    const v9, 0x7f080089

    const/16 v10, 0x7d0

    move-object v0, p0

    move v4, v5

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->addBubbleInfo(IIIZZIIIII)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->showTooltips()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowing:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Lcom/google/android/apps/chrome/Main;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->dismissAll()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowTooltipsRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTabPreselected:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTabPreselected:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->showTooltipsIfNeeded(I)V

    return-void
.end method

.method private addBubbleInfo(IIIZZIIIII)Landroid/os/Bundle;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "View_Id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "Background_Id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "Tip_Id"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "Center"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "Up_Down"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "String_Id"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "Text_Style_Id"

    invoke-virtual {v0, v1, p7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "Layout_Width_Id"

    invoke-virtual {v0, v1, p8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "Tooltip_Margin"

    invoke-virtual {v0, v1, p9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "Animation_Duration"

    invoke-virtual {v0, v1, p10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method public static destroy()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->sTooltipsInstance:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->NOTIFICATIONS:[I

    sget-object v1, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->sTooltipsInstance:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->sTooltipsInstance:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    :cond_0
    return-void
.end method

.method private dismissAll()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowTooltipsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getAnchorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getAnchorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->dismiss()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowing:Z

    invoke-static {}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->destroy()V

    return-void
.end method

.method public static init(Lcom/google/android/apps/chrome/Main;)V
    .locals 2

    invoke-static {p0}, Lorg/chromium/content/browser/DeviceUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->sTooltipsInstance:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;-><init>(Lcom/google/android/apps/chrome/Main;)V

    sput-object v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->sTooltipsInstance:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    :goto_1
    sget-object v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->NOTIFICATIONS:[I

    sget-object v1, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->sTooltipsInstance:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    sget-object v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->sTooltipsInstance:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->showTooltipsIfNeeded(I)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->sTooltipsInstance:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    iput-object p0, v0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    goto :goto_1
.end method

.method private showTooltips()V
    .locals 9

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    move v1, v2

    move v0, v3

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltipsInfo:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltipsInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v6, "View_Id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    move v4, v3

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v4

    goto :goto_0

    :cond_0
    move v4, v2

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v5, :cond_3

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowing:Z

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltipsInfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltipsInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v3, "String_Id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    const-string v4, "View_Id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/Main;->findViewById(I)Landroid/view/View;

    move-result-object v3

    instance-of v4, v3, Lcom/google/android/apps/chrome/LocationBar;

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    new-instance v6, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    iget-object v7, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v7}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    const/high16 v4, 0x3f800000

    const/4 v6, 0x0

    const/high16 v7, -0x40000000

    const/high16 v8, -0x1000000

    invoke-virtual {v0, v4, v6, v7, v8}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->addShadowLayerToText(FFFI)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v6

    invoke-virtual {v0, v1, v3, v4, v6}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->showTextBubble(Ljava/lang/String;Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->startAnimation()V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTooltips:Ljava/util/ArrayList;

    new-instance v6, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$LocationBarTextBubble;

    iget-object v7, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v7}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, p0, v7, v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$LocationBarTextBubble;-><init>(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowTooltipsRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_4
    return-void
.end method

.method private showTooltipsIfNeeded(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    const/4 v1, -0x1

    if-ne p1, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_1
    if-eqz v0, :cond_4

    const-string v1, "chrome://welcome/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowTooltipsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->showTooltips()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/apps/chrome/TabModelSelector;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowTooltipsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowing:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->dismissAll()V

    goto :goto_0
.end method
