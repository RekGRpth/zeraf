.class Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    const/4 v1, -0x1

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # getter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowing:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$100(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # getter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$200(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Lcom/google/android/apps/chrome/Main;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # getter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$200(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Lcom/google/android/apps/chrome/Main;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # getter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mMain:Lcom/google/android/apps/chrome/Main;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$200(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Lcom/google/android/apps/chrome/Main;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getCurrentTabModel()Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # invokes: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->dismissAll()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$300(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # getter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$500(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # getter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowTooltipsRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$400(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # getter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mShowing:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$100(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # invokes: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->dismissAll()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$300(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTabPreselected:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$602(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->showTooltipsIfNeeded(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$700(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;I)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->showTooltipsIfNeeded(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$700(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;I)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # invokes: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->showTooltipsIfNeeded(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$700(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;I)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # getter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTabPreselected:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$600(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    # invokes: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->showTooltipsIfNeeded(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$700(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips$2;->this$0:Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->mTabPreselected:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;->access$602(Lcom/google/android/apps/chrome/widget/bubble/WelcomePageTooltips;Z)Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_4
        0x3 -> :sswitch_3
        0x5 -> :sswitch_0
        0x11 -> :sswitch_1
        0x12 -> :sswitch_5
        0x15 -> :sswitch_2
        0x1a -> :sswitch_4
        0x36 -> :sswitch_1
    .end sparse-switch
.end method
