.class Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$3;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$3;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$500(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$3;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # setter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$502(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$3;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # invokes: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->hideKeyboardAndStartFinding(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$100(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Z)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$3;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$200(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    goto :goto_1
.end method
