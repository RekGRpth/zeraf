.class public Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
.super Landroid/widget/FrameLayout;


# static fields
.field private static final CLOSE_BUTTON_FADE_DURATION_MS:J = 0xc8L

.field private static final CLOSE_BUTTON_SLOP_X:I = 0x7

.field private static final CLOSE_BUTTON_SLOP_Y:I = 0x5

.field private static final CLOSE_BUTTON_VISIBILITY_FADE_POINT_PERCENTAGE:F = 0.01f

.field private static final CLOSE_DURATION_MS:J = 0x96L

.field private static final LOAD_FINISHED_VISUAL_DELAY_MS:I = 0x64

.field private static final OPEN_DURATION_MS:J = 0x96L

.field private static final RESIZE_DURATION_MS:J = 0x96L

.field private static final SLIDE_DURATION_MS:J = 0x7dL


# instance fields
.field private mCloseButton:Landroid/widget/ImageButton;

.field private mCloseButtonVisible:Z

.field private mCrashed:Z

.field private mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

.field private mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mGesture:Landroid/view/GestureDetector;

.field private mHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

.field private mHeight:F

.field private mLeftSideHiddenAmount:F

.field private mLoadRunnable:Ljava/lang/Runnable;

.field private mLoading:Z

.field private mOffsetX:F

.field private mPageLoadRunnable:Ljava/lang/Runnable;

.field private mPageLoading:Z

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mSelected:Z

.field private mTab:Lcom/google/android/apps/chrome/Tab;

.field private mTabIcon:Landroid/widget/ImageView;

.field private mTabId:I

.field private mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

.field private mTabTitle:Landroid/widget/TextView;

.field private mTabVisibleAmount:F

.field private mWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabId:I

    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->OPENING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCrashed:Z

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoading:Z

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoading:Z

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mSelected:Z

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabVisibleAmount:F

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLeftSideHiddenAmount:F

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mOffsetX:F

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mGesture:Landroid/view/GestureDetector;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButtonVisible:Z

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$4;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoadRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$5;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoadRunnable:Ljava/lang/Runnable;

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHeight:F

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Landroid/view/GestureDetector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mGesture:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoading:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoading:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setTouchDelegateForCloseButton()V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;F)F
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mOffsetX:F

    return p1
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;F)F
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    return p1
.end method

.method private animateClose()V
    .locals 11

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v10, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$15;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getHeight()I

    move-result v1

    int-to-float v4, v1

    const-wide/16 v5, 0x96

    const-wide/16 v7, 0x0

    new-instance v9, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v9}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$15;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {v10, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    return-void
.end method

.method private animateOpen()V
    .locals 11

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$11;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$11;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v10, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$12;

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHeight:F

    const/4 v4, 0x0

    const-wide/16 v5, 0x96

    const-wide/16 v7, 0x0

    new-instance v9, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v9}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$12;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {v10, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    return-void
.end method

.method private animateWidth(FJ)V
    .locals 11

    const-wide/16 v7, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_0
    cmp-long v0, p2, v7

    if-nez v0, :cond_1

    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$13;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$13;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v10, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$14;

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    new-instance v9, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v9}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v1, p0

    move-object v2, p0

    move v4, p1

    move-wide v5, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$14;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {v10, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    goto :goto_0
.end method

.method private setTouchDelegateForCloseButton()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$8;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public animateSlideFrom(F)V
    .locals 11

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$9;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$9;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v10, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$10;

    const/4 v4, 0x0

    const-wide/16 v5, 0x7d

    const-wide/16 v7, 0x0

    new-instance v9, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v9}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v1, p0

    move-object v2, p0

    move v3, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$10;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {v10, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    return-void
.end method

.method public close()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->animateClose()V

    return-void
.end method

.method public crashed()V
    .locals 0

    return-void
.end method

.method public die()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->DYING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mProgressBar:Landroid/widget/ProgressBar;

    if-ne p2, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getTranslationX()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    const/high16 v1, 0x3f800000

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    const/4 v0, 0x1

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v1

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_2
    return v1
.end method

.method public finishAnimation()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_2
    return-void
.end method

.method public getOffsetX()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mOffsetX:F

    return v0
.end method

.method public getTabHeight()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHeight:F

    return v0
.end method

.method public getTabId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabId:I

    return v0
.end method

.method public getTabState()Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    return-object v0
.end method

.method public getTabVisibleAmount()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabVisibleAmount:F

    return v0
.end method

.method public getTabWidth()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    return v0
.end method

.method public handleLayout(FFZ)V
    .locals 0

    iput p2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabVisibleAmount:F

    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLeftSideHiddenAmount:F

    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateVisualsFromLayoutParams(Z)V

    return-void
.end method

.method public hasTabCrashed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCrashed:Z

    return v0
.end method

.method public initializeControl(Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;Lcom/google/android/apps/chrome/Tab;FZ)V
    .locals 4

    const/4 v3, 0x0

    iput p3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHeight:F

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabId:I

    const v0, 0x7f0f00ba

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    const v0, 0x7f0f00bb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mProgressBar:Landroid/widget/ProgressBar;

    const v0, 0x7f0f0002

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    const v0, 0x7f0f00bc

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$1;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mGesture:Landroid/view/GestureDetector;

    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$2;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$3;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    if-eqz p4, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->animateOpen()V

    :goto_0
    invoke-virtual {p0, v3, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->IDLE:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    goto :goto_0
.end method

.method public isTabSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mSelected:Z

    return v0
.end method

.method public loadingFinished()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoadRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoadRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x64

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public loadingStarted()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCrashed:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoading:Z

    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoadRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public pageLoadingFinished()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoadRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoadRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x64

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public pageLoadingStarted()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCrashed:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoading:Z

    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoadRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public selected()V
    .locals 2

    const v0, 0x7f020009

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200b6

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mSelected:Z

    return-void
.end method

.method public setTabWidth(FZ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSED:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->DYING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->OPENING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-eq v0, v1, :cond_2

    const-wide/16 v0, 0x96

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->animateWidth(FJ)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_3
    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateVisualsFromLayoutParams(Z)V

    goto :goto_0
.end method

.method public unselected()V
    .locals 2

    const v0, 0x7f020005

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200b5

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mSelected:Z

    return-void
.end method

.method public update()Z
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->update()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_0
    move v0, v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->update()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_2
    move v0, v1

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->update()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_4
    move v0, v1

    :cond_5
    sget-object v2, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$16;->$SwitchMap$com$google$android$apps$chrome$widget$tabstrip$TabView$TabState:[I

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v1, v0

    :cond_6
    :goto_0
    return v1

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_6

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSED:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->IDLE:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 4

    const/4 v2, 0x4

    const/4 v3, 0x0

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getTitle()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCrashed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070142

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0701f8

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/text/Layout;->getParagraphDirection(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getWidth()I

    move-result v1

    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoading:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoading:Z

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    const v1, 0x7f020075

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1
.end method

.method public updateVisualsFromLayoutParams(Z)V
    .locals 9

    const v3, 0x3c23d70a

    const/4 v8, 0x0

    const-wide/16 v6, 0x64

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLeftSideHiddenAmount:F

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationX(F)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mProgressBar:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLeftSideHiddenAmount:F

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setTranslationX(F)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLeftSideHiddenAmount:F

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationX(F)V

    const/high16 v0, 0x3f800000

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabVisibleAmount:F

    sub-float/2addr v0, v1

    cmpl-float v1, v0, v3

    if-ltz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButtonVisible:Z

    if-eqz v1, :cond_2

    iput-boolean v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButtonVisible:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const-string v2, "alpha"

    new-array v3, v4, [F

    aput v8, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const-string v2, "translationX"

    new-array v3, v4, [F

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getWidth()I

    move-result v4

    int-to-float v4, v4

    aput v4, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$6;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    cmpg-float v0, v0, v3

    if-gez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButtonVisible:Z

    if-nez v0, :cond_1

    iput-boolean v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButtonVisible:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_3
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const-string v1, "alpha"

    new-array v2, v4, [F

    const/high16 v3, 0x3f800000

    aput v3, v2, v5

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const-string v2, "translationX"

    new-array v3, v4, [F

    aput v8, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$7;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    goto :goto_0
.end method
