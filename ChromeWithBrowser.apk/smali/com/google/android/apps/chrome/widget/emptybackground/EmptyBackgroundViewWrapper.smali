.class public Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;
.super Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mBackgroundView:Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

.field private mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x5
        0x2
        0xc
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/TabModelSelector;Landroid/app/Activity;I)V
    .locals 0

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;-><init>(Landroid/app/Activity;I)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/TabModelSelector;Landroid/view/View;I)V
    .locals 0

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;-><init>(Landroid/view/View;I)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    return-void
.end method

.method private checkEmptyContainerState()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->inflateIfNecessary()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mBackgroundView:Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->shouldShowEmptyContainer()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->setEmptyContainerState(Z)V

    :cond_0
    return-void
.end method

.method private shouldShowEmptyContainer()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModel;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected getNotificationsToRegister()[I
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->NOTIFICATIONS:[I

    return-object v0
.end method

.method protected handleNotificationMessage(Landroid/os/Message;)V
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->checkEmptyContainerState()V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->checkEmptyContainerState()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_0
        0xc -> :sswitch_1
    .end sparse-switch
.end method

.method protected shouldInflate()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->shouldShowEmptyContainer()Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic viewInflated(Landroid/view/View;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->viewInflated(Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;)V

    return-void
.end method

.method protected viewInflated(Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mBackgroundView:Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mBackgroundView:Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    return-void
.end method
