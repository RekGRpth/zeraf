.class public Lcom/google/android/apps/chrome/StartUpSnapshotHandler;
.super Ljava/lang/Object;


# static fields
.field private static final LAST_ORIENTATION:Ljava/lang/String; = "Last orientation"

.field private static final LAST_URL:Ljava/lang/String; = "Last url"

.field private static final SNAPSHOT_ALPHA_ANIM_DURATION_MS:J = 0x1f4L

.field private static final SNAPSHOT_COMPRESSION_QUALITY:I = 0x46

.field private static final SNAPSHOT_DIRECTORY:Ljava/lang/String; = "Snapshot"

.field private static final SNAPSHOT_FILE_NAME:Ljava/lang/String; = "snapshot.jpg"

.field private static final SNAPSHOT_SWAP_DELAY_MS:J = 0x5dcL

.field private static final SNAPSHOT_TAKEN:Ljava/lang/String; = "Snapshot taken"

.field private static final TAG:Ljava/lang/String; = "Startup"


# instance fields
.field private final mClearSnapshotRunnable:Ljava/lang/Runnable;

.field private mDisableSnapshot:Z

.field private mLastUrl:Ljava/lang/String;

.field private mMain:Lcom/google/android/apps/chrome/Main;

.field private mOrientationChanged:Z

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mSnapshot:Landroid/widget/ImageView;

.field private mSnapshotAlphaAnimation:Landroid/animation/ObjectAnimator;

.field private mSnapshotDir:Ljava/io/File;

.field private mSnapshotTaken:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/Main;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Lcom/google/android/apps/chrome/StartUpSnapshotHandler$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler$1;-><init>(Lcom/google/android/apps/chrome/StartUpSnapshotHandler;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mClearSnapshotRunnable:Ljava/lang/Runnable;

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mDisableSnapshot:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotTaken:Z

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mOrientationChanged:Z

    iput-object p1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v2

    const-string v3, "disable-snapshot"

    invoke-virtual {v2, v3}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mDisableSnapshot:Z

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mDisableSnapshot:Z

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "Snapshot taken"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotTaken:Z

    iget-object v2, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "Last orientation"

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-eq v2, v3, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mOrientationChanged:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    const-string v2, "Snapshot"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/chrome/Main;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotDir:Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "Last url"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mLastUrl:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/StartUpSnapshotHandler;)Landroid/animation/ObjectAnimator;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotAlphaAnimation:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/StartUpSnapshotHandler;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    return-object v0
.end method

.method private convertToGrayscale(Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 2

    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/BitmapDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    return-object p1
.end method

.method private getSnapshotFilePath()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/snapshot.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addSnapshotIfReady(Landroid/view/ViewGroup;)Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mDisableSnapshot:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getLastUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mLastUrl:Ljava/lang/String;

    return-object v0
.end method

.method public prepareSnapshot()V
    .locals 5

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mDisableSnapshot:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->shouldShowSnapshot()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->getSnapshotFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v0

    :cond_2
    :goto_1
    if-eqz v2, :cond_0

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->convertToGrayscale(Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Startup"

    const-string v3, "I/O exception"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v2, v0

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_3
    const-string v3, "Startup"

    const-string v4, "Snapshot can not be read"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "Startup"

    const-string v3, "I/O exception"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_3
    move-exception v0

    move-object v1, v2

    :goto_3
    :try_start_5
    const-string v3, "Startup"

    const-string v4, "Out of Memory"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v1, :cond_2

    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    :catch_4
    move-exception v0

    const-string v1, "Startup"

    const-string v3, "I/O exception"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_5
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_7
    const-string v3, "Startup"

    const-string v4, "Illegal Argument for decodeStream"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v1, :cond_2

    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_1

    :catch_6
    move-exception v0

    const-string v1, "Startup"

    const-string v3, "I/O exception"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_5
    if-eqz v1, :cond_3

    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    :cond_3
    :goto_6
    throw v0

    :catch_7
    move-exception v1

    const-string v2, "Startup"

    const-string v3, "I/O exception"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    :catchall_1
    move-exception v0

    goto :goto_5

    :catch_8
    move-exception v0

    goto :goto_4

    :catch_9
    move-exception v0

    goto :goto_3

    :catch_a
    move-exception v0

    goto :goto_2
.end method

.method public removeSnapshotIfNeeded(Landroid/view/ViewGroup;Z)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mDisableSnapshot:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotAlphaAnimation:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotAlphaAnimation:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotAlphaAnimation:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotAlphaAnimation:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/StartUpSnapshotHandler$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler$2;-><init>(Lcom/google/android/apps/chrome/StartUpSnapshotHandler;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_2
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mClearSnapshotRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mClearSnapshotRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mClearSnapshotRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method public shouldShowSnapshot()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mDisableSnapshot:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotTaken:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mOrientationChanged:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.action.VIEW"

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotDir:Ljava/io/File;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldTakeSnaphot()Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mDisableSnapshot:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->isCrashed()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->isReady()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->isOnFirstRun()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotDir:Ljava/io/File;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public showLastUrlIfReady()Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mDisableSnapshot:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->getLastUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->getLastUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/LocationBar;->setUrlBarText(Ljava/lang/String;Z)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public takeSnapshot()V
    .locals 6

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mDisableSnapshot:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->shouldTakeSnaphot()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "Snapshot taken"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->getSnapshotFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getTabModelSelector()Lcom/google/android/apps/chrome/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    if-nez v0, :cond_4

    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "Snapshot taken"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    :cond_4
    :try_start_3
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x46

    invoke-virtual {v0, v2, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v2

    :try_start_4
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v1, :cond_5

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-eqz v2, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar;->getUrlBar()Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Last url"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "Last orientation"

    iget-object v4, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_6
    const-string v1, "Snapshot taken"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v1, v2

    move v2, v3

    :goto_4
    :try_start_6
    const-string v3, "Startup"

    const-string v4, "Snapshot was not taken"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    if-eqz v1, :cond_7

    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :cond_7
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-eqz v2, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar;->getUrlBar()Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Last url"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "Last orientation"

    iget-object v4, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_8
    const-string v1, "Snapshot taken"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto/16 :goto_2

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_6
    :try_start_8
    const-string v2, "Startup"

    const-string v4, "Out of Memory"

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-eqz v1, :cond_9

    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    :cond_9
    :goto_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-eqz v3, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Main;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar;->getUrlBar()Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Last url"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "Last orientation"

    iget-object v4, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_a
    const-string v1, "Snapshot taken"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_8
    if-eqz v1, :cond_b

    :try_start_a
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    :cond_b
    :goto_9
    iget-object v1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    if-eqz v3, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/Main;->getLocationBar()Lcom/google/android/apps/chrome/LocationBar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/LocationBar;->getUrlBar()Lcom/google/android/apps/chrome/LocationBar$UrlBar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "Last url"

    invoke-interface {v1, v4, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "Last orientation"

    iget-object v5, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mMain:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_c
    const-string v2, "Snapshot taken"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    throw v0

    :catch_2
    move-exception v1

    const-string v2, "Startup"

    const-string v4, "I/O exception"

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9

    :catch_3
    move-exception v0

    const-string v1, "Startup"

    const-string v3, "I/O exception"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_5

    :catch_4
    move-exception v0

    const-string v1, "Startup"

    const-string v2, "I/O exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_7

    :catch_5
    move-exception v0

    const-string v1, "Startup"

    const-string v2, "I/O exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :catch_6
    move-exception v0

    const-string v1, "Startup"

    const-string v3, "I/O exception"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    goto :goto_8

    :catchall_2
    move-exception v0

    move v3, v2

    goto :goto_8

    :catchall_3
    move-exception v0

    move v3, v2

    goto :goto_8

    :catch_7
    move-exception v0

    goto/16 :goto_6

    :catch_8
    move-exception v0

    move v3, v2

    goto/16 :goto_6

    :catch_9
    move-exception v0

    move v2, v3

    goto/16 :goto_4

    :catch_a
    move-exception v0

    goto/16 :goto_4
.end method
