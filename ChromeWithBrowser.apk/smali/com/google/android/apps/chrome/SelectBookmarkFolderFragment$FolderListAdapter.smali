.class Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;
.super Landroid/widget/ArrayAdapter;


# instance fields
.field private final mDefaultPaddingLeft:I

.field private final mPaddingLeftInc:I

.field final synthetic this$0:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;Landroid/content/Context;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->this$0:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    const v0, 0x7f040030

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->mDefaultPaddingLeft:I

    const v1, 0x7f080054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->mPaddingLeftInc:I

    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;

    iget-object v0, v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;

    iget v2, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->mDefaultPaddingLeft:I

    iget v1, v1, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;->mDepth:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->this$0:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    # getter for: Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mMaximumFolderIndentDepth:I
    invoke-static {v3}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->access$000(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v3, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->mPaddingLeftInc:I

    mul-int/2addr v1, v3

    add-int/2addr v1, v2

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/widget/CheckedTextView;->setPadding(IIII)V

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
