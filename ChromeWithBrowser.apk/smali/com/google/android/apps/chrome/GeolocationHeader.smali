.class Lcom/google/android/apps/chrome/GeolocationHeader;
.super Ljava/lang/Object;


# static fields
.field private static final MAX_LOCATION_AGE:J = 0x927c0L


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getGeoHeader(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x2

    const-wide v4, 0x416312d000000000L

    invoke-static {p1}, Lcom/google/android/apps/chrome/GeolocationHeader;->mayReceiveGeoHeader(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-wide/32 v1, 0x927c0

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/chrome/GeolocationTracker;->getLastKnownLocation(Landroid/content/Context;J)Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v0, v2

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    const/high16 v3, 0x447a0000

    mul-float/2addr v1, v3

    float-to-int v1, v1

    const-string v3, "role:1 producer:12 latlng{latitude_e7:%d longitude_e7:%d} radius:%d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, v6}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "X-Geo: a "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static mayReceiveGeoHeader(Ljava/lang/String;)Z
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/URLUtilities;->nativeIsGoogleSearchUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->setInfo(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/WebsiteSettingsUtils$GeolocationInfo;->getAllowed()Ljava/lang/Boolean;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static primeLocationForGeoHeader(Landroid/content/Context;)V
    .locals 2

    const-wide/32 v0, 0x493e0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/GeolocationTracker;->refreshLastKnownLocation(Landroid/content/Context;J)V

    return-void
.end method
