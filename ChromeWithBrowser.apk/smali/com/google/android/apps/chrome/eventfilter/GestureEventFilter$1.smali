.class Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$100(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->onDown(FF)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$100(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2, p3, p4}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->fling(FFFF)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$100(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->onLongPress(FF)V

    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$100(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    neg-float v3, p3

    neg-float v4, p4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->drag(FFFF)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$100(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$000(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->click(FF)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
