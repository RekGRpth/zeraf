.class Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;
.super Landroid/os/AsyncTask;


# instance fields
.field mEncrypted:Z

.field mId:I

.field mState:Ljava/lang/Object;

.field mStateSaved:Z

.field mTab:Lcom/google/android/apps/chrome/Tab;

.field final synthetic this$0:Lcom/google/android/apps/chrome/TabPersistentStore;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/TabPersistentStore;Lcom/google/android/apps/chrome/Tab;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mStateSaved:Z

    iput-object p2, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mId:I

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/Tab;->isIncognito()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mEncrypted:Z

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mState:Ljava/lang/Object;

    if-nez v0, :cond_0

    :goto_0
    return-object v4

    :cond_0
    :try_start_0
    iget v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mId:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mState:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$300(Lcom/google/android/apps/chrome/TabPersistentStore;)Landroid/app/Activity;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mEncrypted:Z

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/Tab;->saveState(ILjava/lang/Object;Landroid/app/Activity;Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mStateSaved:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "TabPersistentStore"

    const-string v1, "Out of memory error while attempting to save tab state.  Erasing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mId:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$300(Lcom/google/android/apps/chrome/TabPersistentStore;)Landroid/app/Activity;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mEncrypted:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/Tab;->deleteStateFile(ILandroid/app/Activity;Z)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mStateSaved:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->tabStateWasPersisted()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$402(Lcom/google/android/apps/chrome/TabPersistentStore;Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;)Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # invokes: Lcom/google/android/apps/chrome/TabPersistentStore;->saveNextTab()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$500(Lcom/google/android/apps/chrome/TabPersistentStore;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getState()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabPersistentStore$SaveTabTask;->mState:Ljava/lang/Object;

    goto :goto_0
.end method
