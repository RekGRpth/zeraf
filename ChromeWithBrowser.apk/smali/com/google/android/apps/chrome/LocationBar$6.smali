.class Lcom/google/android/apps/chrome/LocationBar$6;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$6;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$6;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$6;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/LocationBar;->updateSuggestionPopupPosition()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$2400(Lcom/google/android/apps/chrome/LocationBar;)V

    :cond_0
    return-void
.end method
