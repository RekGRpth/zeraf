.class public Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;
.super Lcom/google/android/apps/chrome/AsyncTaskFragment;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mActionListener:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;

.field private mAllowFolderAddition:Z

.field private mCancelButton:Landroid/widget/Button;

.field private mEmptyFoldersView:Landroid/widget/TextView;

.field private mFolderIdToSelect:J

.field private mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

.field private mFoldersList:Landroid/widget/ListView;

.field private mIsFolder:Z

.field private mMaximumFolderIndentDepth:I

.field private mNewFolderButton:Landroid/widget/Button;

.field private mOkButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;-><init>()V

    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mMaximumFolderIndentDepth:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mMaximumFolderIndentDepth:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;JZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->handleLoadAllFolders(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;JZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mOkButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;

    return-object v0
.end method

.method private addFolderItem(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;IJ)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    new-instance v1, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;-><init>(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->add(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    cmp-long v0, v0, p3

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_0
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    cmp-long v0, v0, p3

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mIsFolder:Z

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->children()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    add-int/lit8 v2, p2, 0x1

    invoke-direct {p0, v0, v2, p3, p4}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->addFolderItem(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;IJ)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private getRetainedTagId()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Retain"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleLoadAllFolders(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;JZ)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->clear()V

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mEmptyFoldersView:Landroid/widget/TextView;

    const v1, 0x7f070050

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mEmptyFoldersView:Landroid/widget/TextView;

    const v1, 0x7f070055

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->children()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-nez p4, :cond_4

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->type()Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    move-result-object v2

    sget-object v3, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->BOOKMARK_BAR:Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    if-eq v2, v3, :cond_3

    sget-object v3, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->OTHER_NODE:Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    if-eq v2, v3, :cond_3

    :cond_4
    invoke-direct {p0, v0, v4, p2, p3}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->addFolderItem(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;IJ)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0
.end method

.method private loadAllFolders(J)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->isFragmentAsyncTaskRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$LoadAllFoldersTask;-><init>(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f07004d

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->runFragmentAsyncTask(Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static newInstance(ZJZ)Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "allowAdd"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "selectedFolder"

    invoke-virtual {v1, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v2, "isFolder"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public areFoldersLoaded()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;-><init>(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const-wide/16 v0, -0x1

    if-nez p1, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFolderIdToSelect:J

    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->loadAllFolders(J)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mMaximumFolderIndentDepth:I

    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->areFoldersLoaded()Z

    move-result v0

    if-nez v0, :cond_3

    iget-wide v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFolderIdToSelect:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->loadAllFolders(J)V

    goto :goto_0

    :cond_3
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getItemId(I)J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFolderIdToSelect:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mActionListener:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "No OnResultListener specified -- onClick == NoOp"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v3

    if-eq v3, v4, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;

    iget-object v1, v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v1

    iget-object v0, v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v0

    move-wide v5, v1

    move-object v2, v0

    move-wide v0, v5

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mActionListener:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;

    invoke-interface {v3, v0, v1, v2}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;->triggerNewFolderCreation(JLjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mActionListener:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;->onCancel()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mOkButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mActionListener:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;

    iget-object v2, v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v2

    iget-object v0, v0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;->onSelection(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "allowAdd"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mAllowFolderAddition:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selectedFolder"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFolderIdToSelect:J

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "isFolder"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mIsFolder:Z

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const v0, 0x7f04002e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0f000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mOkButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0008

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mCancelButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f00a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mAllowFolderAddition:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    const v0, 0x7f0f00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    const v0, 0x7f0f00a6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mEmptyFoldersView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mEmptyFoldersView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    return-object v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public selectFolder(J)V
    .locals 3

    iput-wide p1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFolderIdToSelect:J

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$FolderListAdapter;->getItemId(I)J

    move-result-wide v1

    cmp-long v1, v1, p1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->loadAllFolders(J)V

    goto :goto_1
.end method

.method public setOnActionListener(Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment;->mActionListener:Lcom/google/android/apps/chrome/SelectBookmarkFolderFragment$OnActionListener;

    return-void
.end method
