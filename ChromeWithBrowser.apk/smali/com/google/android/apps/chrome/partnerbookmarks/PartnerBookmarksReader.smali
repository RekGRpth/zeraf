.class public Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final INVALID_BOOKMARK_ID:J = -0x1L

.field static final ROOT_FOLDER_ID:J = 0x0L

.field private static final TAG:Ljava/lang/String; = "PartnerBookmarksReader"


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mNativePartnerBookmarksReader:I

.field private mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:I

    iput-object p1, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativeInit()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:I

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;Ljava/lang/String;Ljava/lang/String;ZJ[B[B)J
    .locals 2

    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->onBookmarkPush(Ljava/lang/String;Ljava/lang/String;ZJ[B[B)J

    move-result-wide v0

    return-wide v0
.end method

.method private isTaskCancelled()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    # getter for: Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->mRootSync:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->access$100(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->isCancelled()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private native nativeAddPartnerBookmark(ILjava/lang/String;Ljava/lang/String;ZJ[B[B)J
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeInit()I
.end method

.method private native nativePartnerBookmarksCreationComplete(I)V
.end method

.method private native nativeReset(I)V
.end method

.method private onBookmarkPush(Ljava/lang/String;Ljava/lang/String;ZJ[B[B)J
    .locals 9

    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->isTaskCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:I

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-wide v5, p4

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativeAddPartnerBookmark(ILjava/lang/String;Ljava/lang/String;ZJ[B[B)J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public cancelReading()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    # getter for: Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->mRootSync:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->access$100(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->cancel(Z)Z

    iget v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativeReset(I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected getAvailableBookmarks()Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$BookmarkIterator;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->createIfAvailable(Landroid/content/ContentResolver;)Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;

    move-result-object v0

    return-object v0
.end method

.method protected onBookmarksRead()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->isTaskCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativePartnerBookmarksCreationComplete(I)V

    iget v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativeDestroy(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:I

    goto :goto_0
.end method

.method public readBookmarks()V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:I

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "readBookmarks called after nativeDestroy."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->cancelReading()V

    new-instance v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;-><init>(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method
