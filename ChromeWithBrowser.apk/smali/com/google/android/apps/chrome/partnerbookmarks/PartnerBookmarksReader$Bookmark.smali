.class Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;
.super Ljava/lang/Object;


# instance fields
.field entries:Ljava/util/ArrayList;

.field favicon:[B

.field id:J

.field isFolder:Z

.field nativeId:J

.field parent:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;

.field parentId:J

.field title:Ljava/lang/String;

.field touchicon:[B

.field url:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->nativeId:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$Bookmark;->entries:Ljava/util/ArrayList;

    return-void
.end method
