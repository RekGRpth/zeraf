.class public Lcom/google/android/apps/chrome/nfc/BeamController;
.super Ljava/lang/Object;


# static fields
.field private static final NFC_BUGS_ACTIVE:Z

.field private static final REGISTRATION_DELAY_MS:I = 0x1f4

.field private static sCanInitialize:Z

.field private static sHandler:Landroid/os/Handler;

.field private static sRegistrationRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/nfc/BeamController;->NFC_BUGS_ACTIVE:Z

    sput-boolean v1, Lcom/google/android/apps/chrome/nfc/BeamController;->sCanInitialize:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/nfc/BeamController;->sCanInitialize:Z

    return v0
.end method

.method static synthetic access$200()Ljava/lang/Runnable;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$202(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    sput-object p0, Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$500()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/nfc/BeamController;->NFC_BUGS_ACTIVE:Z

    return v0
.end method

.method public static onCreate()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/apps/chrome/nfc/BeamController;->sHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/nfc/BeamController;->sHandler:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    sput-object v2, Lcom/google/android/apps/chrome/nfc/BeamController;->sHandler:Landroid/os/Handler;

    sput-object v2, Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method public static onResume()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/chrome/nfc/BeamController;->sCanInitialize:Z

    invoke-static {}, Lcom/google/android/apps/chrome/nfc/BeamController;->postRegistrationRunnable()V

    return-void
.end method

.method public static pauseRegistration()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/chrome/nfc/BeamController;->sCanInitialize:Z

    return-void
.end method

.method private static postRegistrationRunnable()V
    .locals 4

    sget-object v0, Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/nfc/BeamController;->sHandler:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public static setupAndQueueInit(Lcom/google/android/apps/chrome/Main;)V
    .locals 3

    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    sput-object v1, Lcom/google/android/apps/chrome/nfc/BeamController;->sHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;-><init>(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/nfc/BeamController$1;)V

    new-instance v2, Lcom/google/android/apps/chrome/nfc/BeamController$1;

    invoke-direct {v2, v0, v1, p0}, Lcom/google/android/apps/chrome/nfc/BeamController$1;-><init>(Landroid/nfc/NfcAdapter;Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;Lcom/google/android/apps/chrome/Main;)V

    sput-object v2, Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/android/apps/chrome/nfc/BeamController;->postRegistrationRunnable()V

    goto :goto_0
.end method
