.class final Lcom/google/android/apps/chrome/nfc/BeamController$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic val$beamCallback:Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;

.field final synthetic val$chromeMain:Lcom/google/android/apps/chrome/Main;

.field final synthetic val$nfcAdapter:Landroid/nfc/NfcAdapter;


# direct methods
.method constructor <init>(Landroid/nfc/NfcAdapter;Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/nfc/BeamController$1;->val$nfcAdapter:Landroid/nfc/NfcAdapter;

    iput-object p2, p0, Lcom/google/android/apps/chrome/nfc/BeamController$1;->val$beamCallback:Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;

    iput-object p3, p0, Lcom/google/android/apps/chrome/nfc/BeamController$1;->val$chromeMain:Lcom/google/android/apps/chrome/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    # getter for: Lcom/google/android/apps/chrome/nfc/BeamController;->sCanInitialize:Z
    invoke-static {}, Lcom/google/android/apps/chrome/nfc/BeamController;->access$100()Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;
    invoke-static {}, Lcom/google/android/apps/chrome/nfc/BeamController;->access$200()Ljava/lang/Runnable;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/nfc/BeamController$1;->val$nfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/nfc/BeamController$1;->val$beamCallback:Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;

    iget-object v2, p0, Lcom/google/android/apps/chrome/nfc/BeamController$1;->val$chromeMain:Lcom/google/android/apps/chrome/Main;

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, v2, v3}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/nfc/BeamController$1;->val$nfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/nfc/BeamController$1;->val$beamCallback:Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;

    iget-object v2, p0, Lcom/google/android/apps/chrome/nfc/BeamController$1;->val$chromeMain:Lcom/google/android/apps/chrome/Main;

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, v2, v3}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v0, 0x0

    # setter for: Lcom/google/android/apps/chrome/nfc/BeamController;->sRegistrationRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/nfc/BeamController;->access$202(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "BeamController"

    const-string v1, "NFC registration failure. Can\'t retry, giving up."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
