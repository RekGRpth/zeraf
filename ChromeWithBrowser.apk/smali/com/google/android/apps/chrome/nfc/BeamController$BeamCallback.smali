.class Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
.implements Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;


# static fields
.field private static final TOAST_ERROR_DELAY_MS:I = 0x190


# instance fields
.field private final mChromeMain:Lcom/google/android/apps/chrome/Main;

.field private mErrorRunnableIfBeamSent:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->mChromeMain:Lcom/google/android/apps/chrome/Main;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/nfc/BeamController$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;-><init>(Lcom/google/android/apps/chrome/Main;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;)Lcom/google/android/apps/chrome/Main;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->mChromeMain:Lcom/google/android/apps/chrome/Main;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/TabModel;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->getCurrentURL(Lcom/google/android/apps/chrome/TabModel;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getCurrentURL(Lcom/google/android/apps/chrome/TabModel;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, Lcom/google/android/apps/chrome/TabModel;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    const-string v3, "http|https"

    invoke-virtual {v2, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private onInvalidBeam(I)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->beamInvalidAppState()V

    new-instance v0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$2;-><init>(Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;I)V

    # getter for: Lcom/google/android/apps/chrome/nfc/BeamController;->NFC_BUGS_ACTIVE:Z
    invoke-static {}, Lcom/google/android/apps/chrome/nfc/BeamController;->access$500()Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v0, p0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->mErrorRunnableIfBeamSent:Ljava/lang/Runnable;

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;

    const v0, 0x7f0701db

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;-><init>(Ljava/lang/Integer;)V

    :try_start_0
    new-instance v0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$1;-><init>(Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/FutureTask;

    move-result-object v0

    const-wide/16 v3, 0x7d0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v3, v4, v5}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    iget-object v1, v0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;->errorStrID:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;->errorStrID:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->onInvalidBeam(I)V

    move-object v0, v2

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->beamCallbackSuccess()V

    iput-object v2, p0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->mErrorRunnableIfBeamSent:Ljava/lang/Runnable;

    new-instance v1, Landroid/nfc/NdefMessage;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/nfc/NdefRecord;

    const/4 v3, 0x0

    iget-object v0, v0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback$Status;->result:Ljava/lang/String;

    invoke-static {v0}, Landroid/nfc/NdefRecord;->createUri(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-direct {v1, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public onNdefPushComplete(Landroid/nfc/NfcEvent;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->mErrorRunnableIfBeamSent:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->mErrorRunnableIfBeamSent:Ljava/lang/Runnable;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/nfc/BeamController$BeamCallback;->mErrorRunnableIfBeamSent:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method
