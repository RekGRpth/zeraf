.class Lcom/google/android/apps/chrome/Main$InitializerContinuation;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mBackgroundHandler:Landroid/os/Handler;

.field private mBackgroundHandlerThread:Landroid/os/HandlerThread;

.field private mGLPreLoader:Lcom/google/android/apps/chrome/gl/GLPreLoader;

.field private mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

.field private final mNativeLibraryLoadedRunnable:Ljava/lang/Runnable;

.field private mOnResumePending:Z

.field private mPendingActivityResults:Ljava/util/List;

.field private mPendingNewIntents:Ljava/util/List;

.field private mSavedInstance:Landroid/os/Bundle;

.field private mWaitingForFirstDraw:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/Main;Landroid/os/Bundle;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mOnResumePending:Z

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "BackgroundHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandlerThread:Landroid/os/HandlerThread;

    new-instance v0, Lcom/google/android/apps/chrome/Main$InitializerContinuation$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation$1;-><init>(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mNativeLibraryLoadedRunnable:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mSavedInstance:Landroid/os/Bundle;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/Main;Landroid/os/Bundle;Lcom/google/android/apps/chrome/Main$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;-><init>(Lcom/google/android/apps/chrome/Main;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$1002(Lcom/google/android/apps/chrome/Main$InitializerContinuation;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onNativeLibraryLoaded()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)Lcom/google/android/apps/chrome/gl/GLPreLoader;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mGLPreLoader:Lcom/google/android/apps/chrome/gl/GLPreLoader;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/Main$InitializerContinuation;Lcom/google/android/apps/chrome/gl/GLPreLoader;)Lcom/google/android/apps/chrome/gl/GLPreLoader;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mGLPreLoader:Lcom/google/android/apps/chrome/gl/GLPreLoader;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->loadLibraryNow()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/Main$InitializerContinuation;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onLibraryLoaded(Z)V

    return-void
.end method

.method static synthetic access$6500(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->startBackgroundTasks()V

    return-void
.end method

.method static synthetic access$7600(Lcom/google/android/apps/chrome/Main$InitializerContinuation;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$7700(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onPause()V

    return-void
.end method

.method static synthetic access$7800(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onResume()V

    return-void
.end method

.method static synthetic access$7900(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->onDestroy()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)Landroid/os/HandlerThread;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandlerThread:Landroid/os/HandlerThread;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/Main$InitializerContinuation;Landroid/os/HandlerThread;)Landroid/os/HandlerThread;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandlerThread:Landroid/os/HandlerThread;

    return-object p1
.end method

.method private loadLibraryNow()Z
    .locals 3

    :try_start_0
    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->loadNow()V
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "Main"

    const-string v2, "Failed to load native library."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->shutDown()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->access$1800(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    :cond_1
    return-void
.end method

.method private onLibraryLoaded(Z)V
    .locals 3

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    const v1, 0x7f070061

    # invokes: Lcom/google/android/apps/chrome/Main;->showInvalidStartupErrorDialog(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main;->access$600(Lcom/google/android/apps/chrome/Main;I)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->ensureInitialized()V
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mFirstDrawView:Lcom/google/android/apps/chrome/Main$FirstDrawView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$700(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/Main$FirstDrawView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main$FirstDrawView;->hasDrawn()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->postOnNativeLibraryLoaded()V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Main"

    const-string v2, "error loading library"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->finish()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mWaitingForFirstDraw:Z

    goto :goto_0
.end method

.method private onNativeLibraryLoaded()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1200(Lcom/google/android/apps/chrome/Main;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/Main$MainWithNative;-><init>(Lcom/google/android/apps/chrome/Main;Lcom/google/android/apps/chrome/Main$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mSavedInstance:Landroid/os/Bundle;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative;->onCreate(Landroid/os/Bundle;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/Main$MainWithNative;->access$1400(Lcom/google/android/apps/chrome/Main$MainWithNative;Landroid/os/Bundle;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mSavedInstance:Landroid/os/Bundle;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mOnResumePending:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mOnResumePending:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->resumeNowAndProcessPendingItems()V

    goto :goto_0
.end method

.method private onNewIntent(Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingNewIntents:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingNewIntents:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingNewIntents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative;->onNewIntent(Landroid/content/Intent;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/Main$MainWithNative;->access$1900(Lcom/google/android/apps/chrome/Main$MainWithNative;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/tabs/TabsView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/tabs/TabsView;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/TabsView;->onPause()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mOnResumePending:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative;->onPause()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->access$1600(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    :cond_1
    return-void
.end method

.method private onResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mOnResumePending:Z

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/tabs/TabsView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mTabsView:Lcom/google/android/apps/chrome/tabs/TabsView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1500(Lcom/google/android/apps/chrome/Main;)Lcom/google/android/apps/chrome/tabs/TabsView;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/TabsView;->onResume()V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->resumeNowAndProcessPendingItems()V

    goto :goto_0
.end method

.method private postOnNativeLibraryLoaded()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    # getter for: Lcom/google/android/apps/chrome/Main;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$1100(Lcom/google/android/apps/chrome/Main;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mNativeLibraryLoadedRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private resumeNowAndProcessPendingItems()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingNewIntents:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingNewIntents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative;->onNewIntent(Landroid/content/Intent;)V
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->access$1900(Lcom/google/android/apps/chrome/Main$MainWithNative;Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    iput-object v5, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingNewIntents:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingActivityResults:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingActivityResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/Main$ActivityResult;

    iget-object v2, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    iget v3, v0, Lcom/google/android/apps/chrome/Main$ActivityResult;->requestCode:I

    iget v4, v0, Lcom/google/android/apps/chrome/Main$ActivityResult;->resultCode:I

    iget-object v0, v0, Lcom/google/android/apps/chrome/Main$ActivityResult;->data:Landroid/content/Intent;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative;->onActivityResult(IILandroid/content/Intent;)V
    invoke-static {v2, v3, v4, v0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->access$2000(Lcom/google/android/apps/chrome/Main$MainWithNative;IILandroid/content/Intent;)V

    goto :goto_1

    :cond_2
    iput-object v5, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingActivityResults:Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative;->onResume()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main$MainWithNative;->access$2100(Lcom/google/android/apps/chrome/Main$MainWithNative;)V

    return-void
.end method

.method private startBackgroundTasks()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/Main$InitializerContinuation$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation$2;-><init>(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public firstDrawComplete()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->inflateUIElements()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$800(Lcom/google/android/apps/chrome/Main;)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mWaitingForFirstDraw:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mWaitingForFirstDraw:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->postOnNativeLibraryLoaded()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/Main$InitializerContinuation$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/Main$InitializerContinuation$3;-><init>(Lcom/google/android/apps/chrome/Main$InitializerContinuation;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingActivityResults:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingActivityResults:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mPendingActivityResults:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/chrome/Main$ActivityResult;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/chrome/Main$ActivityResult;-><init>(IILandroid/content/Intent;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$InitializerContinuation;->mMainWithNative:Lcom/google/android/apps/chrome/Main$MainWithNative;

    # invokes: Lcom/google/android/apps/chrome/Main$MainWithNative;->onActivityResult(IILandroid/content/Intent;)V
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/Main$MainWithNative;->access$2000(Lcom/google/android/apps/chrome/Main$MainWithNative;IILandroid/content/Intent;)V

    goto :goto_0
.end method
