.class public Lcom/google/android/apps/chrome/EmbedContentViewActivity;
.super Landroid/app/Activity;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "EmbedContentViewActivity"


# instance fields
.field private mContentView:Lorg/chromium/content/browser/ContentView;

.field private mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

.field private mErrorDialog:Landroid/app/AlertDialog;

.field private mIsForeground:Z

.field private mTitle:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;

.field private mUrlErrorToShowOnResume:Ljava/lang/String;

.field private mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/EmbedContentViewActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->showErrorAndFinish(Ljava/lang/String;)V

    return-void
.end method

.method public static show(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/EmbedContentViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private showErrorAndFinish(Ljava/lang/String;)V
    .locals 5

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mIsForeground:Z

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mUrlErrorToShowOnResume:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701d4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_1
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->finish()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mUrl:Ljava/lang/String;

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mTitle:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v0}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(Z)I

    move-result v0

    new-instance v1, Lorg/chromium/ui/gfx/ActivityNativeWindow;

    invoke-direct {v1, p0}, Lorg/chromium/ui/gfx/ActivityNativeWindow;-><init>(Landroid/app/Activity;)V

    const/4 v2, -0x1

    :try_start_0
    invoke-static {p0, v2}, Lorg/chromium/content/browser/AndroidBrowserProcess;->init(Landroid/content/Context;I)Z
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lorg/chromium/content/browser/ContentView;->newInstance(Landroid/content/Context;ILorg/chromium/ui/gfx/NativeWindow;I)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentView:Lorg/chromium/content/browser/ContentView;

    new-instance v0, Lcom/google/android/apps/chrome/EmbedContentViewActivity$1;

    iget-object v1, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/EmbedContentViewActivity$1;-><init>(Lcom/google/android/apps/chrome/EmbedContentViewActivity;Lorg/chromium/content/browser/ContentViewCore;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    new-instance v0, Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewRenderView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewRenderView;->setCurrentContentView(Lorg/chromium/content/browser/ContentView;)V

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentView:Lorg/chromium/content/browser/ContentView;

    new-instance v1, Lorg/chromium/content/browser/LoadUrlParams;

    iget-object v2, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mUrl:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/chromium/content/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentView;->loadUrl(Lorg/chromium/content/browser/LoadUrlParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mTitle:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "EmbedContentViewActivity"

    const-string v2, "Failed to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->destroy()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentView:Lorg/chromium/content/browser/ContentView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewRenderView;->destroy()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mErrorDialog:Landroid/app/AlertDialog;

    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->finish()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mIsForeground:Z

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mIsForeground:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mUrlErrorToShowOnResume:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mUrlErrorToShowOnResume:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->showErrorAndFinish(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->mUrlErrorToShowOnResume:Ljava/lang/String;

    :cond_0
    return-void
.end method
