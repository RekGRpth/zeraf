.class public Lcom/google/android/apps/chrome/ChromeAnimation;
.super Ljava/lang/Object;


# static fields
.field private static final FIRST_FRAME_OFFSET_MS:I = 0x10

.field private static mAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

.field private static mDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

.field private static mLinearInterpolator:Landroid/view/animation/LinearInterpolator;


# instance fields
.field private final mAnimations:Ljava/util/ArrayList;

.field private mCurrentTime:J

.field private final mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    return-void
.end method

.method private finishInternal()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finish()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method

.method public static getAccelerateInterpolator()Landroid/view/animation/AccelerateInterpolator;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

    return-object v0
.end method

.method public static getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->mDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->mDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->mDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    return-object v0
.end method

.method public static getLinearInterpolator()Landroid/view/animation/LinearInterpolator;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->mLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->mLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->mLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    return-object v0
.end method


# virtual methods
.method public add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V
    .locals 10

    new-instance v0, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;

    move-result-object v9

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-wide v5, p5

    move-wide/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;-><init>(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    return-void
.end method

.method public cancel(Ljava/lang/Object;Ljava/lang/Enum;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->getAnimatedObject()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->checkProperty(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected finish()V
    .locals 0

    return-void
.end method

.method public finished()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v3

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->finished()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method public start()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->start()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public update()Z
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeAnimation;->update(J)Z

    move-result v0

    return v0
.end method

.method public update(J)Z
    .locals 8

    const/4 v0, 0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-wide v3, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-nez v1, :cond_1

    const-wide/16 v3, 0x10

    sub-long v3, p1, v3

    iput-wide v3, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    :cond_1
    iget-wide v3, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    sub-long v4, p1, v3

    iget-wide v6, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    add-long/2addr v6, v4

    iput-wide v6, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    move v1, v2

    move v3, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->update(J)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->finished()Z

    move-result v0

    and-int/2addr v3, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public updateAndFinish()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->updateAndFinish()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finishInternal()V

    return-void
.end method
