.class public interface abstract Lcom/google/android/apps/chrome/OverviewBehavior;
.super Ljava/lang/Object;


# virtual methods
.method public abstract hideOverview(Z)V
.end method

.method public abstract overviewVisible()Z
.end method

.method public abstract setChromeViewHolder(Lcom/google/android/apps/chrome/ContentViewHolder;)V
.end method

.method public abstract setEnableAnimations(Z)V
.end method

.method public abstract setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
.end method

.method public abstract setTiltControlsEnabled(Z)V
.end method

.method public abstract showOverview(Z)V
.end method
