.class public Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;
.super Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected entryRemovedFromMaximumCacheSizeChange(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    return-void
.end method

.method protected bridge synthetic entryRemovedFromMaximumCacheSizeChange(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->entryRemovedFromMaximumCacheSizeChange(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    return-void
.end method

.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->removeEldestEntry(Ljava/util/Map$Entry;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    # invokes: Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->cleanupGLTexture(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->access$700(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;)V

    :cond_0
    return v1
.end method
