.class public Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;
.super Ljava/util/LinkedHashMap;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mMaximumCacheSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 2

    const/high16 v0, 0x3f400000

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->mMaximumCacheSize:I

    sget-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->mMaximumCacheSize:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method protected entryRemovedFromMaximumCacheSizeChange(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public maximumCacheSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->mMaximumCacheSize:I

    return v0
.end method

.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->mMaximumCacheSize:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMaximumCacheSize(I)V
    .locals 2

    iput p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->mMaximumCacheSize:I

    sget-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->mMaximumCacheSize:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->mMaximumCacheSize:I

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringCache;->entryRemovedFromMaximumCacheSizeChange(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method
