.class Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CleanupCPURunnable;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CleanupCPURunnable;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CleanupCPURunnable;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CleanupCPURunnable;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupCPUData(Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$CleanupCPURunnable;->this$0:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->mApproximationCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ExpiringTextureCache;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupCPUData(Z)V

    goto :goto_1

    :cond_1
    return-void
.end method
