.class public Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/glui/thumbnail/GLTexture;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final COMPRESSED_KEY:I = -0x54545455

.field private static final CURRENT_ENCODING_VERSION:I = 0x1

.field private static final DECOMPRESSED_KEY:I = -0x32323233

.field private static final ENABLE_COMPRESSION:Z = true

.field private static final JPEG_COMPRESS_QUALITY:I = 0x5a

.field private static final TAG:Ljava/lang/String;

.field private static final sLeakDetector:Lcom/google/android/apps/chrome/utilities/LeakDetector;


# instance fields
.field private mCanRecycleRawBitmap:Z

.field private mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

.field private mCurrentTextureFormat:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

.field private mDataWritePending:Z

.field private mFromStream:Z

.field private final mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

.field private mId:I

.field private mRawData:Landroid/graphics/Bitmap;

.field private mScale:F

.field private mTextureBytes:J

.field private final mTextureCompressing:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mTextureHeight:I

.field private final mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field private mTextureRebuildPending:Z

.field private mTextureWidth:I

.field private final mTextureWriting:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mTextures:[I

.field private mValidHeight:I

.field private mValidWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/chrome/utilities/LeakDetector;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/utilities/LeakDetector;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->sLeakDetector:Lcom/google/android/apps/chrome/utilities/LeakDetector;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;F)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    iput v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureWidth:I

    iput v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureHeight:I

    iput v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidWidth:I

    iput v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidHeight:I

    iput-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCanRecycleRawBitmap:Z

    iput-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mScale:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mId:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureBytes:J

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;->Decompressed:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCurrentTextureFormat:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureRebuildPending:Z

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mDataWritePending:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureWriting:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureCompressing:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mFromStream:Z

    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    aput v2, v0, v2

    iput p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mId:I

    iput-object p2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iput p3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mScale:F

    return-void
.end method

.method public constructor <init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;IILandroid/opengl/ETC1Util$ETC1Texture;F)V
    .locals 1

    invoke-direct {p0, p1, p2, p6}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;-><init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;F)V

    sget-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p5, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0, p5, p3, p4}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->setCompressedTextureData(Landroid/opengl/ETC1Util$ETC1Texture;II)V

    return-void
.end method

.method public constructor <init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Landroid/graphics/Bitmap;F)V
    .locals 1

    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;-><init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;F)V

    sget-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p3, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->setRawTextureData(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public constructor <init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Landroid/graphics/Bitmap;ZF)V
    .locals 1

    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;-><init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;F)V

    sget-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0, p3, p4}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->setRawTextureData(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public static buildTextureFromStream(ILcom/google/android/apps/chrome/gl/GLResourceProvider;Ljava/io/InputStream;)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;
    .locals 7

    const/4 v0, 0x0

    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    const v3, -0x54545455

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    invoke-static {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->createTexture(Ljava/io/InputStream;)Landroid/opengl/ETC1Util$ETC1Texture;

    move-result-object v5

    invoke-static {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->readVersionDataFromStream(Ljava/io/DataInputStream;)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v5, :cond_2

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->scale:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->access$000(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;)F

    move-result v6

    move v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;-><init>(ILcom/google/android/apps/chrome/gl/GLResourceProvider;IILandroid/opengl/ETC1Util$ETC1Texture;F)V

    :cond_2
    if-eqz v0, :cond_0

    invoke-direct {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->setFromStream()V

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->TAG:Ljava/lang/String;

    const-string v2, "Ignoring read of corrupt compressed texture"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private freeRawData()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->TAG:Ljava/lang/String;

    const-string v1, "Trying to free raw data and it is already recycled."

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCanRecycleRawBitmap:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    return-void
.end method

.method private getTemporaryCompressedReadTexture()Landroid/opengl/ETC1Util$ETC1Texture;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-virtual {v0}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/opengl/ETC1Util$ETC1Texture;

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-virtual {v1}, Landroid/opengl/ETC1Util$ETC1Texture;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-virtual {v2}, Landroid/opengl/ETC1Util$ETC1Texture;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-virtual {v3}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Landroid/opengl/ETC1Util$ETC1Texture;-><init>(IILjava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method private static readVersionDataFromStream(Ljava/io/DataInputStream;)Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;-><init>(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$1;)V

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->version:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->access$202(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;I)I

    invoke-static {}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->defaultScale()F

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->scale:F
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->access$002(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;F)F

    :try_start_0
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->version:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->access$202(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;I)I

    # getter for: Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->version:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->access$200(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->scale:F
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;->access$002(Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$VersionData;F)F
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private setCompressedTextureData(Landroid/opengl/ETC1Util$ETC1Texture;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->recycleTexture(Landroid/opengl/ETC1Util$ETC1Texture;)V

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->freeRawData()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-virtual {v0}, Landroid/opengl/ETC1Util$ETC1Texture;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureWidth:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-virtual {v0}, Landroid/opengl/ETC1Util$ETC1Texture;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureHeight:I

    iput p2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidWidth:I

    iput p3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidHeight:I

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;->Compressed:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCurrentTextureFormat:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureRebuildPending:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mDataWritePending:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
.end method

.method private setFromStream()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mFromStream:Z

    return-void
.end method

.method private setRawTextureData(Landroid/graphics/Bitmap;Z)V
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->freeRawData()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCanRecycleRawBitmap:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->recycleTexture(Landroid/opengl/ETC1Util$ETC1Texture;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureWidth:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureHeight:I

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureWidth:I

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidWidth:I

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureHeight:I

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidHeight:I

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;->Decompressed:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCurrentTextureFormat:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureRebuildPending:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mDataWritePending:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
.end method


# virtual methods
.method public attemptToScheduleRebuildFromData()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureRebuildPending:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method public bind()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->rebuildTexture()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    aget v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    aget v0, v2, v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/gl/GLResourceProvider;->bindTexture(I)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public canFreeSourceData()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public cleanupCPUData(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupRequired()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureWriting:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureCompressing:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->freeRawData()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->recycleTexture(Landroid/opengl/ETC1Util$ETC1Texture;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
.end method

.method public cleanupRequired()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->compressionRequired()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->dataWriteRequired()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureRebuildPending:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->canFreeSourceData()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method public cleanupTexture()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    aget v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    aget v1, v1, v2

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/gl/GLResourceProvider;->deleteTexture(I)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureBytes:J

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    aput v2, v0, v2

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->sLeakDetector:Lcom/google/android/apps/chrome/utilities/LeakDetector;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/utilities/LeakDetector;->removeObject(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public compressTexture()V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->compressionRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    const-string v2, "compressTexture"

    invoke-static {v2}, Lorg/chromium/content/common/TraceEvent;->begin(Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureCompressing:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCurrentTextureFormat:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    sget-object v3, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;->Decompressed:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->TAG:Ljava/lang/String;

    const-string v2, "Trying to compress recycled bitmap data."

    invoke-static {v0, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "compressTexture"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureCompressing:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ETC1;->nativeGetEncodedWidth(Landroid/graphics/Bitmap;)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/apps/chrome/glui/thumbnail/ETC1;->nativeGetEncodedHeight(Landroid/graphics/Bitmap;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-static {v3}, Lcom/google/android/apps/chrome/glui/thumbnail/ETC1;->nativeGetEncodedDataSize(Landroid/graphics/Bitmap;)I

    move-result v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/ETC1;->nativeEncodeImage(Landroid/graphics/Bitmap;Ljava/nio/ByteBuffer;)I

    move-result v3

    if-eqz v3, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Texture compression failed. err="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v0, "compressTexture"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureCompressing:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    goto :goto_0

    :cond_2
    :try_start_2
    new-instance v3, Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-direct {v3, v0, v2, v4}, Landroid/opengl/ETC1Util$ETC1Texture;-><init>(IILjava/nio/ByteBuffer;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    :goto_1
    const-string v4, "compressTexture"

    invoke-static {v4}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureCompressing:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    if-eqz v3, :cond_0

    if-lez v2, :cond_0

    if-lez v0, :cond_0

    invoke-direct {p0, v3, v2, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->setCompressedTextureData(Landroid/opengl/ETC1Util$ETC1Texture;II)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v2, "compressTexture"

    invoke-static {v2}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureCompressing:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0

    :cond_3
    move v2, v1

    move-object v3, v0

    move v0, v1

    goto :goto_1
.end method

.method public compressionRequired()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dataWriteRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mDataWritePending:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mFromStream:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getByteCount()J
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-virtual {v1}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureBytes:J

    add-long/2addr v0, v2

    return-wide v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public getCompressedData()Landroid/opengl/ETC1Util$ETC1Texture;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    return-object v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mId:I

    return v0
.end method

.method public getOriginalHeight()I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mScale:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public getOriginalWidth()I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mScale:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public getRawData()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getRawTextureId()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getScale()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mScale:F

    return v0
.end method

.method public getTextureHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureHeight:I

    return v0
.end method

.method public getTextureSizeGuess()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureBytes:J

    return-wide v0
.end method

.method public getTextureWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureWidth:I

    return v0
.end method

.method public getValidHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidHeight:I

    return v0
.end method

.method public getValidWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidWidth:I

    return v0
.end method

.method public hasGLTexture()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    const/4 v2, 0x0

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mId:I

    return v0
.end method

.method public rebuildTexture()V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureRebuildPending:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupTexture()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCurrentTextureFormat:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    sget-object v1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;->Decompressed:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    if-eq v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCurrentTextureFormat:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    sget-object v1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;->Compressed:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    goto :goto_0

    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->TAG:Ljava/lang/String;

    const-string v1, "Trying to build texture from recycled data."

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    goto :goto_0

    :cond_4
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCurrentTextureFormat:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    sget-object v1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;->Compressed:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getTemporaryCompressedReadTexture()Landroid/opengl/ETC1Util$ETC1Texture;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/chrome/gl/GLResourceProvider;->generateCompressedTexture(Landroid/opengl/ETC1Util$ETC1Texture;)I

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-virtual {v0}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureBytes:J

    :goto_1
    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->sLeakDetector:Lcom/google/android/apps/chrome/utilities/LeakDetector;

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iget-object v4, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/chrome/glui/thumbnail/TextureCleanupTask;-><init>(Lcom/google/android/apps/chrome/gl/GLResourceProvider;I)V

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/utilities/LeakDetector;->addObject(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/chrome/utilities/LeakDetector$CleanupTask;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureRebuildPending:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    invoke-virtual {p0, v6}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupCPUData(Z)V

    goto/16 :goto_0

    :cond_5
    :try_start_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mGLResourceProvider:Lcom/google/android/apps/chrome/gl/GLResourceProvider;

    iget-object v3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-interface {v2, v3}, Lcom/google/android/apps/chrome/gl/GLResourceProvider;->generateTexture(Landroid/graphics/Bitmap;)I

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureBytes:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method public toLongString()Ljava/lang/String;
    .locals 6

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    if-eqz v4, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    invoke-virtual {v2}, Landroid/opengl/ETC1Util$ETC1Texture;->getData()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    int-to-long v2, v2

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "(raw: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes, comp: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes, tex: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureBytes:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextures:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " texture)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeDataToStream(Ljava/io/OutputStream;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->dataWriteRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureWriting:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCurrentTextureFormat:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    sget-object v2, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;->Compressed:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCompressedData:Landroid/opengl/ETC1Util$ETC1Texture;

    if-eqz v1, :cond_2

    const v1, -0x54545455

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidWidth:I

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mValidHeight:I

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->getTemporaryCompressedReadTexture()Landroid/opengl/ETC1Util$ETC1Texture;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/opengl/ETC1Util;->writeTexture(Landroid/opengl/ETC1Util$ETC1Texture;Ljava/io/OutputStream;)V

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mScale:F

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureWriting:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mDataWritePending:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->cleanupCPUData(Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mCurrentTextureFormat:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    sget-object v2, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;->Decompressed:Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture$TextureFormat;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mRawData:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureWriting:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->mTextureLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/GLOptimizedTexture;->TAG:Ljava/lang/String;

    const-string v1, "Unable to write texture to file"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
