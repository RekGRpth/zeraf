.class public Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;
.super Ljava/lang/Object;


# static fields
.field private static final APPROXIMATION_SCALE_FACTOR:F = 4.0f

.field private static final COMPROMISE_SCALE_FACTOR:F = 1.414f

.field private static final DEFAULT_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

.field private static final DEFAULT_SCALE_FACTOR:F = 1.0f

.field private static final SMALL_SCALE_FACTOR:F = 2.0f


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mScale:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->DEFAULT_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;F)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    iput p2, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mScale:F

    return-void
.end method

.method public static createApproximation(Landroid/graphics/Bitmap;F)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;
    .locals 4

    const/4 v1, 0x0

    if-nez p0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/high16 v0, 0x40800000

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v2

    const-string v3, "approximation-scale"

    invoke-virtual {v2, v3}, Lorg/chromium/content/common/CommandLine;->getSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v0

    float-to-int v2, v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v0

    float-to-int v3, v3

    if-lez v2, :cond_2

    if-gtz v3, :cond_3

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    const/4 v1, 0x1

    invoke-static {p0, v2, v3, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v1, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;

    mul-float/2addr v0, p1

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;-><init>(Landroid/graphics/Bitmap;F)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static defaultBitmapConfig()Landroid/graphics/Bitmap$Config;
    .locals 3

    sget-object v0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->DEFAULT_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v1

    const-string v2, "sixteen-bit-thumbnails"

    invoke-virtual {v1, v2}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    :cond_0
    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v1

    const-string v2, "thirtytwo-bit-thumbnails"

    invoke-virtual {v1, v2}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :cond_1
    return-object v0
.end method

.method public static defaultScale()F
    .locals 3

    const/high16 v0, 0x3f800000

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v1

    const-string v2, "full-size-thumbnails"

    invoke-virtual {v1, v2}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v1

    const-string v2, "compromise-size-thumbnails"

    invoke-virtual {v1, v2}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, 0x3fb4fdf4

    goto :goto_0

    :cond_2
    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v1

    const-string v2, "small-size-thumbnails"

    invoke-virtual {v1, v2}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v0, 0x40000000

    goto :goto_0
.end method


# virtual methods
.method public bitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public createApproximation()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mScale:F

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->createApproximation(Landroid/graphics/Bitmap;F)Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;

    move-result-object v0

    return-object v0
.end method

.method public getByteCount()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getHeight()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mScale:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public getScale()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mScale:F

    return v0
.end method

.method public getWidth()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailBitmap;->mScale:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method
