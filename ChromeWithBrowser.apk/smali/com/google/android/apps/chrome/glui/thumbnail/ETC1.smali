.class public Lcom/google/android/apps/chrome/glui/thumbnail/ETC1;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native nativeEncodeImage(Landroid/graphics/Bitmap;Ljava/nio/ByteBuffer;)I
.end method

.method public static native nativeGetEncodedDataSize(Landroid/graphics/Bitmap;)I
.end method

.method public static native nativeGetEncodedHeight(Landroid/graphics/Bitmap;)I
.end method

.method public static native nativeGetEncodedWidth(Landroid/graphics/Bitmap;)I
.end method
