.class Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;
.super Ljava/lang/Object;


# static fields
.field private static final DETECT_LEAKS:Z


# instance fields
.field private mCache:Ljava/util/LinkedList;

.field private mLeakDetector:Lcom/google/android/apps/chrome/utilities/LeakDetector;

.field private mSize:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mCache:Ljava/util/LinkedList;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mSize:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mLeakDetector:Lcom/google/android/apps/chrome/utilities/LeakDetector;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;Ljava/nio/ByteBuffer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->returnBuffer(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;I)Ljava/nio/ByteBuffer;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->getBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->setSize(I)V

    return-void
.end method

.method private declared-synchronized getBuffer(I)Ljava/nio/ByteBuffer;
    .locals 3

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized prune()V
    .locals 2

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mSize:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized returnBuffer(Ljava/nio/ByteBuffer;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->prune()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setSize(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->mSize:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util$ByteBufferCache;->prune()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
