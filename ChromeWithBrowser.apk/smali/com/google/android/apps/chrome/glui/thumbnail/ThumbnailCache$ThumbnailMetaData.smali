.class Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;
.super Ljava/lang/Object;


# instance fields
.field private final mCaptureTime:J

.field private final mUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;->mCaptureTime:J

    if-nez p3, :cond_0

    const-string p3, ""

    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;->mUrl:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache$ThumbnailMetaData;->mCaptureTime:J

    return-wide v0
.end method
