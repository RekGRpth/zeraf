.class Lcom/google/android/apps/chrome/HttpAuthDialog$4;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/HttpAuthDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/HttpAuthDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/HttpAuthDialog$4;->this$0:Lcom/google/android/apps/chrome/HttpAuthDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/HttpAuthDialog$4;->this$0:Lcom/google/android/apps/chrome/HttpAuthDialog;

    # getter for: Lcom/google/android/apps/chrome/HttpAuthDialog;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/HttpAuthDialog;->access$100(Lcom/google/android/apps/chrome/HttpAuthDialog;)Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/HttpAuthDialog$4;->this$0:Lcom/google/android/apps/chrome/HttpAuthDialog;

    # invokes: Lcom/google/android/apps/chrome/HttpAuthDialog;->getUsername()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/HttpAuthDialog;->access$200(Lcom/google/android/apps/chrome/HttpAuthDialog;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/HttpAuthDialog$4;->this$0:Lcom/google/android/apps/chrome/HttpAuthDialog;

    # invokes: Lcom/google/android/apps/chrome/HttpAuthDialog;->getPassword()Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/HttpAuthDialog;->access$300(Lcom/google/android/apps/chrome/HttpAuthDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->proceed(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
