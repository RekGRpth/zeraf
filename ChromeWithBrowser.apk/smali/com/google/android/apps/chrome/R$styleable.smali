.class public final Lcom/google/android/apps/chrome/R$styleable;
.super Ljava/lang/Object;


# static fields
.field public static final ChromeViewHolder:[I

.field public static final ChromeViewHolderPhone:[I

.field public static final ChromeViewHolderPhone_spaceBetweenTabs:I = 0x0

.field public static final ChromeViewHolder_gutterPercentage:I = 0x0

.field public static final GeolocationPermission:[I

.field public static final GeolocationPermission_location_allowed:I = 0x0

.field public static final HyperlinkPreference:[I

.field public static final HyperlinkPreference_url:I = 0x0

.field public static final PopupsPermission:[I

.field public static final PopupsPermission_popups_allowed:I = 0x0

.field public static final SeekBarPreference:[I

.field public static final SeekBarPreference_max:I = 0x1

.field public static final SeekBarPreference_min:I = 0x0

.field public static final SeekBarPreference_step:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x7f010001

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/R$styleable;->ChromeViewHolder:[I

    new-array v0, v3, [I

    const v1, 0x7f010002

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/R$styleable;->GeolocationPermission:[I

    new-array v0, v3, [I

    const v1, 0x7f010004

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/R$styleable;->HyperlinkPreference:[I

    new-array v0, v3, [I

    const v1, 0x7f010003

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/R$styleable;->PopupsPermission:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/R$styleable;->SeekBarPreference:[I

    new-array v0, v3, [I

    const/high16 v1, 0x7f010000

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/R$styleable;->ChromeViewHolderPhone:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f010005
        0x7f010006
        0x7f010007
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
