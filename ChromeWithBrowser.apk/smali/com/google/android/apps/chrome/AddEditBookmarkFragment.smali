.class public Lcom/google/android/apps/chrome/AddEditBookmarkFragment;
.super Lcom/google/android/apps/chrome/AsyncTaskFragment;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

.field private mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

.field private mBookmarkId:Ljava/lang/Long;

.field private mBookmarkNodeLoaded:Z

.field private mCancelButton:Landroid/widget/Button;

.field private mDefaultFolderLoaded:Z

.field private mFolderInput:Landroid/widget/Button;

.field private mInitialName:Ljava/lang/String;

.field private mInitialUrl:Ljava/lang/String;

.field private mLoadedLock:Ljava/lang/Object;

.field private mOkButton:Landroid/widget/Button;

.field private mParentFolderId:J

.field private mParentFolderName:Ljava/lang/String;

.field private mRemoveButton:Landroid/widget/Button;

.field private mTitleInput:Landroid/widget/EditText;

.field private mUrlInput:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/AsyncTaskFragment;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderId:J

    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mLoadedLock:Ljava/lang/Object;

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mBookmarkNodeLoaded:Z

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mDefaultFolderLoaded:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->handleDefaultBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->handleGetBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;

    return-object v0
.end method

.method private handleDefaultBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->setParentFolderInfo(JLjava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mLoadedLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mDefaultFolderLoaded:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setError(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private handleGetBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$2;-><init>(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$1;-><init>(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->isUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->url()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->setParentFolderInfo(JLjava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mLoadedLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mBookmarkNodeLoaded:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private initializeFolderInputContent(Landroid/os/Bundle;)V
    .locals 4

    const-wide/16 v2, -0x1

    if-eqz p1, :cond_1

    const-string v0, "parentId"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    iput-wide v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderId:J

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    const-string v1, "parentName"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderName:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->isFragmentAsyncTaskRunning()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->isDefaultFolderLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->isBookmarkNodeLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    sget-object v2, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    sget-object v2, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    if-ne v0, v2, :cond_4

    :cond_3
    const/4 v0, 0x0

    :goto_1
    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;-><init>(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;Ljava/lang/Long;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f07004d

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->runFragmentAsyncTask(Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;Ljava/lang/String;)Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mBookmarkId:Ljava/lang/Long;

    goto :goto_1
.end method

.method private initializeStateFromIntent(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mInitialName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mInitialName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mInitialUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mInitialUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static newAddNewFolderInstance(JLjava/lang/String;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment;
    .locals 4

    new-instance v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "mode"

    sget-object v3, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v2, "parentId"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v2, "parentName"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static newEditInstance(ZJ)Lcom/google/android/apps/chrome/AddEditBookmarkFragment;
    .locals 4

    new-instance v1, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "mode"

    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->EDIT_FOLDER:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v0, "id"

    invoke-virtual {v2, v0, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->EDIT_BOOKMARK:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    goto :goto_0
.end method

.method public static newInstance(ZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/AddEditBookmarkFragment;
    .locals 7

    new-instance v1, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-nez v0, :cond_4

    :cond_0
    const-string v3, "mode"

    if-eqz p0, :cond_3

    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :goto_1
    if-eqz p2, :cond_1

    const-string v0, "name"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz p3, :cond_2

    const-string v0, "url"

    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1

    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    goto :goto_0

    :cond_4
    const-string v3, "mode"

    if-eqz p0, :cond_5

    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->EDIT_FOLDER:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    :goto_2
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v0, "id"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v0, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->EDIT_BOOKMARK:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    goto :goto_2
.end method


# virtual methods
.method protected getOnActionListener()Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

    return-object v0
.end method

.method protected getParentFolderId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderId:J

    return-wide v0
.end method

.method protected isBookmarkNodeLoaded()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mLoadedLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mBookmarkNodeLoaded:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected isDefaultFolderLoaded()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mLoadedLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mDefaultFolderLoaded:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected isFolder()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->isFolder()Z

    move-result v0

    return v0
.end method

.method public isLoadingBookmarks()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mCurrentTask:Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;

    instance-of v0, v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$LoadBookmarkNodeTask;

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    const/16 v3, 0x8

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$3;->$SwitchMap$com$google$android$apps$chrome$AddEditBookmarkFragment$Mode:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    const v1, 0x7f070042

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_0
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    sget-object v1, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    sget-object v1, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f000f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->initializeFolderInputContent(Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->initializeStateFromIntent(Landroid/os/Bundle;)V

    return-void

    :pswitch_2
    const v1, 0x7f070043

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_3
    const v1, 0x7f070044

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "No OnResultListener specified -- onClick == NoOp"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v0, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    const v4, 0x7f070048

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    move v0, v1

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->isFolder()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    const v5, 0x7f070049

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    move v0, v1

    :cond_3
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->isFragmentAsyncTaskRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->TAG:Ljava/lang/String;

    const-string v1, "Pending asynchronous task when trying to add/update bookmarks."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->isFolder()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "url"

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string v0, "title"

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "parentId"

    iget-wide v2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "isFolder"

    iget-object v2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->isFolder()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    new-instance v2, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    sget-object v3, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    if-eq v0, v3, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    sget-object v3, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    if-ne v0, v3, :cond_7

    :cond_6
    const/4 v0, 0x0

    :goto_1
    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$InsertUpdateBookmarkTask;-><init>(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;Landroid/content/ContentValues;Ljava/lang/Long;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f07004e

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->runFragmentAsyncTask(Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mBookmarkId:Ljava/lang/Long;

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    sget-object v1, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    if-ne v0, v1, :cond_9

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "The remove functionality should be disabled for new bookmarks."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->isFragmentAsyncTaskRunning()Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->TAG:Ljava/lang/String;

    const-string v1, "Pending asynchronous task when trying to delete bookmarks."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_a
    new-instance v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mBookmarkId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$DeleteBookmarkTask;-><init>(Lcom/google/android/apps/chrome/AddEditBookmarkFragment;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f07004f

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->runFragmentAsyncTask(Lcom/google/android/apps/chrome/AsyncTaskFragment$FragmentAsyncTask;Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;->onCancel()V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;->triggerFolderSelection()V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v0, "mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Created new AddEditBookmarkFragment without a mode defined.  Make sure arguments are correctly populated."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$Mode;

    :goto_0
    return-void

    :cond_0
    const-string v0, "id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mBookmarkId:Ljava/lang/Long;

    :cond_1
    const-string v0, "parentId"

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderId:J

    const-string v0, "parentName"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderName:Ljava/lang/String;

    const-string v0, "name"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mInitialName:Ljava/lang/String;

    const-string v0, "url"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mInitialUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040002

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "parentName"

    iget-object v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "parentId"

    iget-wide v1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/AsyncTaskFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f000e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f0011

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f0014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnActionListener(Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/AddEditBookmarkFragment$OnActionListener;

    return-void
.end method

.method protected setParentFolderInfo(JLjava/lang/String;)V
    .locals 1

    iput-object p3, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderName:Ljava/lang/String;

    iput-wide p1, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mParentFolderId:J

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
