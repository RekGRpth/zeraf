.class public Lcom/google/android/apps/chrome/omaha/OmahaClient;
.super Landroid/app/IntentService;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ACTION_INITIALIZE:Ljava/lang/String; = "com.google.android.apps.chrome.omaha.ACTION_INITIALIZE"

.field private static final ACTION_POST_REQUEST:Ljava/lang/String; = "com.google.android.apps.chrome.omaha.ACTION_POST_REQUEST"

.field private static final ACTION_REGISTER_REQUEST:Ljava/lang/String; = "com.google.android.apps.chrome.omaha.ACTION_REGISTER_REQUEST"

.field private static final EXTRA_FORCE_ACTION:Ljava/lang/String; = "forceAction"

.field static final INVALID_REQUEST_ID:Ljava/lang/String; = "invalid"

.field private static final INVALID_TIMESTAMP:J = -0x1L

.field private static final MS_BETWEEN_REQUESTS:J = 0x112a880L

.field private static final MS_CONNECTION_TIMEOUT:I = 0xea60

.field private static final MS_PER_HOUR:J = 0x36ee80L

.field private static final MS_POST_BASE_DELAY:J = 0x36ee80L

.field private static final MS_POST_MAX_DELAY:J = 0x112a880L

.field static final PREF_LATEST_VERSION:Ljava/lang/String; = "latestVersion"

.field static final PREF_MARKET_URL:Ljava/lang/String; = "marketURL"

.field static final PREF_PACKAGE:Ljava/lang/String; = "com.google.android.apps.chrome.omaha"

.field private static final PREF_PERSISTED_REQUEST_ID:Ljava/lang/String; = "persistedRequestID"

.field private static final PREF_SEND_INSTALL_EVENT:Ljava/lang/String; = "sendInstallEvent"

.field private static final PREF_TIMESTAMP_FOR_NEW_REQUEST:Ljava/lang/String; = "timestampForNewRequest"

.field private static final PREF_TIMESTAMP_FOR_NEXT_POST_ATTEMPT:Ljava/lang/String; = "timestampForNextPostAttempt"

.field private static final PREF_TIMESTAMP_OF_INSTALL:Ljava/lang/String; = "timestampOfInstall"

.field private static final PREF_TIMESTAMP_OF_REQUEST:Ljava/lang/String; = "timestampOfRequest"

.field private static final TAG:Ljava/lang/String;

.field static final URL_OMAHA_SERVER:Ljava/lang/String; = "https://tools.google.com/service/update2"

.field private static sEnableCommunication:Z

.field private static sEnableUpdateDetection:Z

.field private static sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

.field private static sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;


# instance fields
.field private mApplicationContext:Landroid/content/Context;

.field private mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

.field private mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

.field private mGenerator:Lcom/google/android/apps/chrome/omaha/RequestGenerator;

.field private mLatestVersion:Ljava/lang/String;

.field private mMarketURL:Ljava/lang/String;

.field protected mSendInstallEvent:Z

.field private mStateHasBeenRestored:Z

.field private mTimestampForNewRequest:J

.field private mTimestampForNextPostAttempt:J

.field private mTimestampOfInstall:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-class v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->TAG:Ljava/lang/String;

    sput-boolean v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableCommunication:Z

    sput-boolean v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableUpdateDetection:Z

    sput-object v2, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    sput-object v2, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->setIntentRedelivery(Z)V

    return-void
.end method

.method private cancelRepeatingAlarm()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRegisterRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const/high16 v2, 0x20000000

    invoke-static {v1, v3, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    invoke-virtual {v1}, Landroid/app/PendingIntent;->cancel()V

    :cond_0
    return-void
.end method

.method private checkServerResponseCode(Ljava/net/HttpURLConnection;)V
    .locals 3

    :try_start_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " code instead of 200 (OK) from the server.  Aborting."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Failed to read response code from server: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    return-void
.end method

.method public static createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.chrome.omaha.ACTION_INITIALIZE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static createPostRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.chrome.omaha.ACTION_POST_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "forceAction"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method public static createRegisterRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.chrome.omaha.ACTION_REGISTER_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "forceAction"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method private createRequestData(JLjava/lang/String;)Lcom/google/android/apps/chrome/omaha/RequestData;
    .locals 2

    if-eqz p3, :cond_0

    const-string v0, "invalid"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->generateRandomUUID()Ljava/lang/String;

    move-result-object p3

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/omaha/RequestData;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/google/android/apps/chrome/omaha/RequestData;-><init>(ZJLjava/lang/String;)V

    return-object v0
.end method

.method private generateAndPostRequest(JLjava/lang/String;)Z
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampOfInstall:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omaha/RequestData;->isSendInstallEvent()Z

    move-result v2

    invoke-static {p1, p2, v0, v1, v2}, Lcom/google/android/apps/chrome/omaha/RequestGenerator;->installAge(JJZ)J

    move-result-wide v3

    invoke-static {}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getVersionNumberGetter()Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;->getCurrentlyUsedVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mGenerator:Lcom/google/android/apps/chrome/omaha/RequestGenerator;

    iget-object v5, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    move-object v1, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/omaha/RequestGenerator;->generateXML(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/chrome/omaha/RequestData;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->postRequest(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->parseServerResponse(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    const-wide/32 v0, 0x36ee80

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->resetFailedAttempts()V
    :try_end_0
    .catch Lcom/google/android/apps/chrome/omaha/RequestFailureException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;->TAG:Ljava/lang/String;

    const-string v2, "Failed to contact server: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createPostRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->createAlarm(Landroid/content/Intent;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->increaseFailedAttempts()V

    move v0, v6

    goto :goto_0
.end method

.method private static getBooleanFromMap(Ljava/util/Map;Ljava/lang/String;Z)Z
    .locals 1

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :cond_0
    return p2
.end method

.method private static getLongFromMap(Ljava/util/Map;Ljava/lang/String;J)J
    .locals 1

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    :cond_0
    return-wide p2
.end method

.method public static getMarketURLGetter()Lcom/google/android/apps/chrome/omaha/MarketURLGetter;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/omaha/MarketURLGetter;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    return-object v0
.end method

.method private static getStringFromMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method static getVersionNumberGetter()Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    return-object v0
.end method

.method private handleInitialize()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->scheduleRepeatingAlarm()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createPostRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method

.method private handlePostRequestIntent(Landroid/content/Intent;)V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "forceAction"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    if-eqz v1, :cond_5

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->generateRandomUUID()Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->generateAndPostRequest(JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v5, :cond_2

    iput-boolean v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->registerNewRequest(J)V

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->generateAndPostRequest(JLjava/lang/String;)Z

    move-result v0

    :cond_2
    if-eqz v1, :cond_3

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->TAG:Ljava/lang/String;

    const-string v1, "Requests successfully sent to Omaha server."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->saveState()V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->TAG:Ljava/lang/String;

    const-string v1, "Requests failed to reach Omaha server."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createPostRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->createAlarm(Landroid/content/Intent;J)J

    goto :goto_1
.end method

.method private handleRegisterRequest(Landroid/content/Intent;)V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "forceAction"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->isChromeBeingUsed()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->cancelRepeatingAlarm()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/chrome/omaha/RequestData;->getAgeInMilliseconds(J)J

    move-result-wide v6

    const-wide/32 v8, 0x112a880

    cmp-long v0, v6, v8

    if-ltz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v6

    if-nez v6, :cond_5

    iget-wide v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    cmp-long v6, v4, v6

    if-ltz v6, :cond_5

    :goto_2
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    if-eqz v3, :cond_3

    :cond_2
    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->registerNewRequest(J)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createPostRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method public static isNewerVersionAvailable(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    sget-boolean v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableUpdateDetection:Z

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const-string v1, ""

    invoke-static {}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getMarketURLGetter()Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/android/apps/chrome/omaha/MarketURLGetter;->getMarketURL(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getVersionNumberGetter()Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;->getCurrentlyUsedVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0}, Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;->getLatestKnownVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Lcom/google/android/apps/chrome/omaha/VersionNumber;->fromString(Ljava/lang/String;)Lcom/google/android/apps/chrome/omaha/VersionNumber;

    move-result-object v2

    invoke-static {v1}, Lcom/google/android/apps/chrome/omaha/VersionNumber;->fromString(Ljava/lang/String;)Lcom/google/android/apps/chrome/omaha/VersionNumber;

    move-result-object v1

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/omaha/VersionNumber;->isSmallerThan(Lcom/google/android/apps/chrome/omaha/VersionNumber;)Z

    move-result v0

    goto :goto_0
.end method

.method private parseServerResponse(Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mGenerator:Lcom/google/android/apps/chrome/omaha/RequestGenerator;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omaha/RequestGenerator;->getAppId()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/ResponseParser;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    invoke-direct {v1, p1, v0, v2}, Lcom/google/android/apps/chrome/omaha/ResponseParser;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v2

    const-wide/32 v4, 0x112a880

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omaha/ResponseParser;->getMarketVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mLatestVersion:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omaha/ResponseParser;->getURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mMarketURL:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->scheduleRepeatingAlarm()V

    return-void
.end method

.method private readResponseFromServer(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->checkServerResponseCode(Ljava/net/HttpURLConnection;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_1
    :try_start_3
    new-instance v2, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v3, "Failed to read response from server: "

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Failed to close stream: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Failed to close stream: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method private saveState()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string v1, "com.google.android.apps.chrome.omaha"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v0, "sendInstallEvent"

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v0, "timestampOfInstall"

    iget-wide v3, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampOfInstall:J

    invoke-interface {v2, v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v0, "timestampForNextPostAttempt"

    iget-wide v3, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    invoke-interface {v2, v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v0, "timestampForNewRequest"

    iget-wide v3, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    invoke-interface {v2, v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v3, "timestampOfRequest"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omaha/RequestData;->getCreationTimestamp()J

    move-result-wide v0

    :goto_0
    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "persistedRequestID"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omaha/RequestData;->getRequestID()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "latestVersion"

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mLatestVersion:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "marketURL"

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mMarketURL:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_3
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_1
    const-string v0, "invalid"

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mLatestVersion:Ljava/lang/String;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mMarketURL:Ljava/lang/String;

    goto :goto_3
.end method

.method private scheduleRepeatingAlarm()V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRegisterRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->setAlarm(Landroid/app/AlarmManager;Landroid/app/PendingIntent;IJ)V

    return-void
.end method

.method private sendRequestToServer(Ljava/net/HttpURLConnection;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-direct {v1, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, p2, v0, v2}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;II)V

    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->checkServerResponseCode(Ljava/net/HttpURLConnection;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Failed to write request to server: "

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static setEnableCommunication(Z)V
    .locals 0

    sput-boolean p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableCommunication:Z

    return-void
.end method

.method public static setEnableUpdateDetection(Z)V
    .locals 0

    sput-boolean p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableUpdateDetection:Z

    return-void
.end method

.method static setMarketURLGetterForTests(Lcom/google/android/apps/chrome/omaha/MarketURLGetter;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    return-void
.end method

.method private setUpPostRequest(JLjava/net/HttpURLConnection;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p3, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual {p4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    invoke-virtual {p3, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getCumulativeFailedAttempts()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/omaha/RequestData;->getAgeInSeconds(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "X-RequestAge"

    invoke-virtual {p3, v1, v0}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Caught an IllegalAccessError:"

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Caught an IllegalArgumentException:"

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Caught an IllegalStateException:"

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static setVersionNumberGetterForTests(Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    return-void
.end method


# virtual methods
.method createBackoffScheduler(Ljava/lang/String;Landroid/content/Context;JJ)Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;
    .locals 7

    new-instance v0, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;-><init>(Ljava/lang/String;Landroid/content/Context;JJ)V

    return-object v0
.end method

.method protected createConnection()Ljava/net/HttpURLConnection;
    .locals 3

    :try_start_0
    new-instance v0, Ljava/net/URL;

    const-string v1, "https://tools.google.com/service/update2"

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    const v1, 0xea60

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const v1, 0xea60

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Caught a malformed URL exception."

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/apps/chrome/omaha/RequestFailureException;

    const-string v2, "Failed to open connection to URL"

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method createRequestGenerator(Landroid/content/Context;)Lcom/google/android/apps/chrome/omaha/RequestGenerator;
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/omaha/RequestGenerator;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/omaha/RequestGenerator;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected generateRandomUUID()Ljava/lang/String;
    .locals 1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getCumulativeFailedAttempts()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getNumFailedAttempts()I

    move-result v0

    return v0
.end method

.method getTimestampForNewRequest()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    return-wide v0
.end method

.method getTimestampForNextPostAttempt()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    return-wide v0
.end method

.method hasRequest()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isChromeBeingUsed()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->isChromeInForeground(Landroid/content/Context;)Z

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 7

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string v1, "com.google.android.apps.chrome.omaha"

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-wide/32 v3, 0x36ee80

    const-wide/32 v5, 0x112a880

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createBackoffScheduler(Ljava/lang/String;Landroid/content/Context;JJ)Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRequestGenerator(Landroid/content/Context;)Lcom/google/android/apps/chrome/omaha/RequestGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mGenerator:Lcom/google/android/apps/chrome/omaha/RequestGenerator;

    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    sget-boolean v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableCommunication:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->TAG:Ljava/lang/String;

    const-string v1, "Disabled.  Ignoring intent."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mStateHasBeenRestored:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->restoreState()V

    :cond_2
    const-string v0, "com.google.android.apps.chrome.omaha.ACTION_INITIALIZE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->handleInitialize()V

    goto :goto_0

    :cond_3
    const-string v0, "com.google.android.apps.chrome.omaha.ACTION_REGISTER_REQUEST"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->handleRegisterRequest(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string v0, "com.google.android.apps.chrome.omaha.ACTION_POST_REQUEST"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->handlePostRequestIntent(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got unknown action from intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method postRequest(JLjava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createConnection()Ljava/net/HttpURLConnection;

    move-result-object v1

    invoke-direct {p0, p1, p2, v1, p3}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->setUpPostRequest(JLjava/net/HttpURLConnection;Ljava/lang/String;)V

    invoke-direct {p0, v1, p3}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sendRequestToServer(Ljava/net/HttpURLConnection;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->readResponseFromServer(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    return-object v0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_1
    throw v0
.end method

.method registerNewRequest(J)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRequestData(JLjava/lang/String;)Lcom/google/android/apps/chrome/omaha/RequestData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->resetFailedAttempts()V

    iput-wide p1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    const-wide/32 v0, 0x112a880

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->scheduleRepeatingAlarm()V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->saveState()V

    return-void
.end method

.method restoreState()V
    .locals 11

    const-wide/16 v9, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string v3, "com.google.android.apps.chrome.omaha"

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v4

    const-string v0, "timestampForNewRequest"

    invoke-static {v3, v0, v4, v5}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getLongFromMap(Ljava/util/Map;Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    const-string v0, "timestampForNextPostAttempt"

    invoke-static {v3, v0, v4, v5}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getLongFromMap(Ljava/util/Map;Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    const-string v0, "timestampOfRequest"

    invoke-static {v3, v0, v9, v10}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getLongFromMap(Ljava/util/Map;Ljava/lang/String;J)J

    move-result-wide v6

    const-string v0, "sendInstallEvent"

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getBooleanFromMap(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    if-eqz v0, :cond_2

    const-string v0, "persistedRequestID"

    const-string v8, "invalid"

    invoke-static {v3, v0, v8}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getStringFromMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    cmp-long v8, v6, v9

    if-nez v8, :cond_3

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    const-string v0, "latestVersion"

    const-string v6, ""

    invoke-static {v3, v0, v6}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getStringFromMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mLatestVersion:Ljava/lang/String;

    const-string v0, "marketURL"

    const-string v6, ""

    invoke-static {v3, v0, v6}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getStringFromMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mMarketURL:Ljava/lang/String;

    const-string v0, "timestampOfInstall"

    invoke-static {v3, v0, v4, v5}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getLongFromMap(Ljava/util/Map;Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampOfInstall:J

    iget-wide v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    sub-long/2addr v6, v4

    const-wide/32 v8, 0x112a880

    cmp-long v0, v6, v8

    if-lez v0, :cond_4

    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Delay to next request ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is longer than expected.  Resetting to now."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    move v0, v1

    :goto_2
    iget-wide v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    sub-long/2addr v2, v4

    iget-object v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getGeneratedDelay()J

    move-result-wide v6

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Delay to next post attempt ("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is greater than expected ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getGeneratedDelay()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ").  Resetting to now."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    move v0, v1

    :cond_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->saveState()V

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mStateHasBeenRestored:Z

    return-void

    :cond_2
    const-string v0, "invalid"

    goto/16 :goto_0

    :cond_3
    invoke-direct {p0, v6, v7, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRequestData(JLjava/lang/String;)Lcom/google/android/apps/chrome/omaha/RequestData;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method protected setAlarm(Landroid/app/AlarmManager;Landroid/app/PendingIntent;IJ)V
    .locals 7

    const/4 v1, 0x1

    const-wide/32 v4, 0x112a880

    move-object v0, p1

    move-wide v2, p4

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    return-void
.end method
