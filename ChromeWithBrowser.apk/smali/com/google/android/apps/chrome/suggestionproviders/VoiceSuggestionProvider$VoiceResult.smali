.class public Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;
.super Ljava/lang/Object;


# instance fields
.field private final mConfidence:F

.field private final mMatch:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;F)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->mMatch:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->mConfidence:F

    return-void
.end method


# virtual methods
.method public getConfidence()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->mConfidence:F

    return v0
.end method

.method public getMatch()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/suggestionproviders/VoiceSuggestionProvider$VoiceResult;->mMatch:Ljava/lang/String;

    return-object v0
.end method
