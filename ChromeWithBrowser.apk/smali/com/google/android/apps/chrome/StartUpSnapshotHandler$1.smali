.class Lcom/google/android/apps/chrome/StartUpSnapshotHandler$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/StartUpSnapshotHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler$1;->this$0:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler$1;->this$0:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    # getter for: Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotAlphaAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->access$000(Lcom/google/android/apps/chrome/StartUpSnapshotHandler;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler$1;->this$0:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    # getter for: Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshot:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->access$100(Lcom/google/android/apps/chrome/StartUpSnapshotHandler;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/StartUpSnapshotHandler$1;->this$0:Lcom/google/android/apps/chrome/StartUpSnapshotHandler;

    # getter for: Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->mSnapshotAlphaAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/StartUpSnapshotHandler;->access$000(Lcom/google/android/apps/chrome/StartUpSnapshotHandler;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    return-void
.end method
