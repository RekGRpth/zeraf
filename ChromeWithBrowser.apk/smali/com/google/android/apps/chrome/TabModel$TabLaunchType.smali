.class public final enum Lcom/google/android/apps/chrome/TabModel$TabLaunchType;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

.field public static final enum FROM_EXTERNAL_APP:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

.field public static final enum FROM_INSTANT:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

.field public static final enum FROM_KEYBOARD:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

.field public static final enum FROM_LINK:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

.field public static final enum FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

.field public static final enum FROM_MENU:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

.field public static final enum FROM_OVERVIEW:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

.field public static final enum FROM_RESTORE:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const-string v1, "FROM_LINK"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LINK:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    new-instance v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const-string v1, "FROM_EXTERNAL_APP"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    new-instance v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const-string v1, "FROM_OVERVIEW"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_OVERVIEW:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    new-instance v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const-string v1, "FROM_MENU"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_MENU:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    new-instance v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const-string v1, "FROM_RESTORE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_RESTORE:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    new-instance v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const-string v1, "FROM_LONGPRESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    new-instance v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const-string v1, "FROM_INSTANT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_INSTANT:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    new-instance v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const-string v1, "FROM_KEYBOARD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_KEYBOARD:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LINK:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_OVERVIEW:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_MENU:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_RESTORE:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_INSTANT:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_KEYBOARD:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->$VALUES:[Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/TabModel$TabLaunchType;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/TabModel$TabLaunchType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->$VALUES:[Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    return-object v0
.end method
