.class Lcom/google/android/apps/chrome/Main$5;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Main;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Main;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Main$5;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/Main$5;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->begin()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$5;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$5;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$5;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Main;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "disable_crash_dump_uploading"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    invoke-static {v1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->tryUploadAllCrashDumps(Landroid/content/Context;)V

    :goto_1
    new-instance v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/crash/CrashFileManager;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->cleanOutAllNonFreshMinidumpFiles()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$5;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->importBookmarksAfterOTAIfNeeded()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$6700(Lcom/google/android/apps/chrome/Main;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$5;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetProvider;->refreshAllWidgets(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$5;->this$0:Lcom/google/android/apps/chrome/Main;

    # invokes: Lcom/google/android/apps/chrome/Main;->checkIfUpdateInfobarNecessary()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/Main;->access$4200(Lcom/google/android/apps/chrome/Main;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$5;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/Main$5;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/Main;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const/16 v0, 0x26

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->end()V

    const/4 v0, 0x0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/Main$5;->this$0:Lcom/google/android/apps/chrome/Main;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->disableCrashUploading()V

    goto :goto_1
.end method
