.class Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static PROGRESS_INCREMENT:I

.field private static PROGRESS_INCREMENT_DELAY_MS:I


# instance fields
.field private final mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

.field private mProgress:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xa

    sput v0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->PROGRESS_INCREMENT:I

    sput v0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->PROGRESS_INCREMENT_DELAY_MS:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mProgress:I

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    return-void
.end method


# virtual methods
.method cancel()V
    .locals 1

    const/16 v0, 0x65

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mProgress:I

    return-void
.end method

.method public run()V
    .locals 3

    const/16 v0, 0x64

    iget v1, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mProgress:I

    if-le v1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mProgress:I

    sget v2, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->PROGRESS_INCREMENT:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mProgress:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    iget v2, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mProgress:I

    if-le v2, v0, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/LocationBar;->setLoadProgress(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mLocationBar:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mUiHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$800(Lcom/google/android/apps/chrome/LocationBar;)Landroid/os/Handler;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->PROGRESS_INCREMENT_DELAY_MS:I

    int-to-long v1, v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mProgress:I

    goto :goto_1
.end method

.method public start()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->mProgress:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBar$LoadProgressSimulator;->run()V

    return-void
.end method
