.class public Lcom/google/android/apps/chrome/ShortcutActivity;
.super Lcom/google/android/apps/chrome/ChromeActivity;


# static fields
.field public static final BOOKMARK_SHORTCUT_URL:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "ShortcutActivity"


# instance fields
.field private mActivityNativeWindow:Lorg/chromium/ui/gfx/ActivityNativeWindow;

.field private mTab:Lcom/google/android/apps/chrome/Tab;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "chrome://newtab/#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;->BOOKMARK_SHORTCUT:Lcom/google/android/apps/chrome/NewTabPageUtil$NTPSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/ShortcutActivity;->BOOKMARK_SHORTCUT_URL:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ShortcutActivity;)Lorg/chromium/ui/gfx/ActivityNativeWindow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mActivityNativeWindow:Lorg/chromium/ui/gfx/ActivityNativeWindow;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ShortcutActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ShortcutActivity;->onShortcutAdded(Landroid/content/Intent;)V

    return-void
.end method

.method private onShortcutAdded(Landroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/ShortcutActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ShortcutActivity;->finish()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, -0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->onCreate(Landroid/os/Bundle;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ShortcutActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Z)V
    :try_end_0
    .catch Lorg/chromium/content/common/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Lorg/chromium/ui/gfx/ActivityNativeWindow;

    invoke-direct {v0, p0}, Lorg/chromium/ui/gfx/ActivityNativeWindow;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mActivityNativeWindow:Lorg/chromium/ui/gfx/ActivityNativeWindow;

    new-instance v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->initialize(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->kickOffReading()V

    new-instance v0, Lcom/google/android/apps/chrome/ShortcutActivity$BookmarkShortcutTab;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/chrome/ShortcutActivity$BookmarkShortcutTab;-><init>(Lcom/google/android/apps/chrome/ShortcutActivity;Lcom/google/android/apps/chrome/ChromeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mTab:Lcom/google/android/apps/chrome/Tab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mTab:Lcom/google/android/apps/chrome/Tab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/Tab;->initialize(I)V

    new-instance v0, Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewRenderView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewRenderView;->setCurrentContentView(Lorg/chromium/content/browser/ContentView;)V

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/chrome/ShortcutActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mTab:Lcom/google/android/apps/chrome/Tab;

    sget-object v1, Lcom/google/android/apps/chrome/ShortcutActivity;->BOOKMARK_SHORTCUT_URL:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/Tab;->loadUrl(Ljava/lang/String;Ljava/lang/String;I)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ShortcutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ShortcutActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ShortcutActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ShortcutActivity"

    const-string v2, "Failed to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ShortcutActivity;->finish()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mTab:Lcom/google/android/apps/chrome/Tab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mTab:Lcom/google/android/apps/chrome/Tab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->destroy()V

    :cond_0
    return-void
.end method
