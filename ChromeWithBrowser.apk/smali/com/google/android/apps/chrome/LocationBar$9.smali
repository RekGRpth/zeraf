.class Lcom/google/android/apps/chrome/LocationBar$9;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/LocationBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/LocationBar$9;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$9;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mForceResizeSuggestionPopupRunnable:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->access$2602(Lcom/google/android/apps/chrome/LocationBar;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$9;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$9;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/LocationBar$9;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v1}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->getWidth()I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$9;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mClearSuggestionsOnDismiss:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->access$2502(Lcom/google/android/apps/chrome/LocationBar;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$9;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$9;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/LocationBar;->mClearSuggestionsOnDismiss:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/LocationBar;->access$2502(Lcom/google/android/apps/chrome/LocationBar;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBar$9;->this$0:Lcom/google/android/apps/chrome/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/LocationBar;->mSuggestionListPopup:Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/LocationBar;->access$900(Lcom/google/android/apps/chrome/LocationBar;)Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/LocationBar$SuggestionsPopupWindow;->show()V

    goto :goto_0
.end method
