.class public Lcom/google/android/apps/chrome/ChromeBrowserProvider;
.super Lorg/chromium/chrome/browser/ChromeBrowserProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;-><init>()V

    return-void
.end method


# virtual methods
.method protected ensureNativeChromeLoadedOnUIThread()Z
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeBrowserProvider;->isNativeSideInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startChromeBrowserProcesses(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-super {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->ensureNativeChromeLoadedOnUIThread()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getBookmarkNode(JZZZZ)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 7

    if-eqz p3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeBrowserProvider;->getMobileBookmarksFolderId()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p3, 0x0

    move v3, p3

    :goto_0
    move-object v0, p0

    move-wide v1, p1

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-super/range {v0 .. v6}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarkNode(JZZZZ)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    return-object v0

    :cond_0
    move v3, p3

    goto :goto_0
.end method

.method public onCreate()Z
    .locals 2

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/data/local/chrome-command-line"

    invoke-static {v0}, Lorg/chromium/content/common/CommandLine;->initFromFile(Ljava/lang/String;)V

    :cond_0
    const/16 v0, 0x25

    new-instance v1, Lcom/google/android/apps/chrome/ChromeBrowserProvider$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeBrowserProvider$1;-><init>(Lcom/google/android/apps/chrome/ChromeBrowserProvider;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    invoke-super {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->onCreate()Z

    move-result v0

    return v0
.end method
