.class Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;
.super Landroid/os/AsyncTask;


# instance fields
.field private final mCurrentFolderId:J

.field private final mEnabled:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;ZJ)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->mEnabled:Z

    iput-wide p3, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->mCurrentFolderId:J

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;

    # getter for: Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->access$100(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->getMobileBookmarksFolderId(Landroid/content/Context;)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->mCurrentFolderId:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->refreshWidget()V

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->mEnabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;

    # getter for: Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->mContext:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v2}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->access$100(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->mCurrentFolderId:J

    invoke-static {v2, v3, v4}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->isBookmarkInMobileBookmarksBranch(Landroid/content/Context;J)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory$SyncEnabledStatusUpdatedTask;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetService$BookmarkFactory;->requestFolderChange(J)V

    goto :goto_0
.end method
