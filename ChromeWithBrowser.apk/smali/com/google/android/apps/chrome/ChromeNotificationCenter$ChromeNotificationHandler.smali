.class public abstract Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.super Ljava/lang/Object;


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;-><init>(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public abstract handleMessage(Landroid/os/Message;)V
.end method
