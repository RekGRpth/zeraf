.class Lcom/google/android/apps/chrome/TabModelSelectorImpl$SnapshotStateBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$SnapshotStateBroadcastReceiver;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$SnapshotStateBroadcastReceiver;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Lcom/google/android/apps/chrome/TabModelSelectorImpl$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$SnapshotStateBroadcastReceiver;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$SnapshotStateBroadcastReceiver;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;

    iget-object v2, p0, Lcom/google/android/apps/chrome/TabModelSelectorImpl$SnapshotStateBroadcastReceiver;->this$0:Lcom/google/android/apps/chrome/TabModelSelectorImpl;

    invoke-direct {v1, v2, p2}, Lcom/google/android/apps/chrome/TabModelSelectorImpl$UpdateSnapshotStateTask;-><init>(Lcom/google/android/apps/chrome/TabModelSelectorImpl;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
