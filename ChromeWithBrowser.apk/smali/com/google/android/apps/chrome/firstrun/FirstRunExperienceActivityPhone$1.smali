.class Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/app/FragmentManager$OnBackStackChangedListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackStackChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;

    const v1, 0x7f0f004f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/AndroidUtils;->hideKeyboard(Landroid/view/View;)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;

    move-result-object v0

    invoke-interface {v0}, Landroid/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;

    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;

    # invokes: Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->updateNotificationBar()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;)V

    :cond_0
    return-void
.end method
