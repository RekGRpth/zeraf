.class public Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;
.super Lcom/google/android/apps/chrome/sync/SyncChooseAccountFragment;


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncChooseAccountFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-boolean v0, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    packed-switch p2, :pswitch_data_0

    iput p2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;->mSelectedAccount:I

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;->mAccounts:[Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunTabletAccountSelectionFragment;->mSelectedAccount:I

    aget-object v2, v0, v2

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/chrome/sync/SyncChooseAccountFragment$Listener;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/SyncChooseAccountFragment$Listener;->onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
