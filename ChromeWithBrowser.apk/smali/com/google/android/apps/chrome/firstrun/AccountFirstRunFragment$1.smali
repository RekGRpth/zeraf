.class Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountSelected(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "AccountName"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Attempted Sign In"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    iget-object v1, v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->setBundle(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    iget-object v1, v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->onProgressFirstRunExperience(Landroid/os/Bundle;)V

    return-void
.end method

.method public onAccountSelectionCanceled()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    # invokes: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->signOutIfNeeded()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->access$000(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    iget-object v0, v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "Attempted Sign In"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->freSignInAttempted()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    iget-object v0, v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;->onQuitFirstRunExperience(Z)V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/UmaRecordAction;->freSkipSignIn()V

    goto :goto_0
.end method

.method public onNewAccount()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/chrome/sync/AccountAdder;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/AccountAdder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/AccountAdder;->addAccount(Landroid/app/Fragment;I)V

    return-void
.end method
