.class Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;
.super Landroid/app/Fragment;


# static fields
.field protected static final ACCOUNT_NAME:Ljava/lang/String; = "AccountName"

.field protected static final SIGN_IN_ATTEMPTED:Ljava/lang/String; = "Attempted Sign In"


# instance fields
.field protected mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

.field private mIsDestroyed:Z


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected isDestroyed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->mIsDestroyed:Z

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->mFirstRunFlowListener:Lcom/google/android/apps/chrome/firstrun/FirstRunFlowListener;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFragment;->mIsDestroyed:Z

    return-void
.end method
