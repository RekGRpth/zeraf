.class public Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;


# instance fields
.field private mNotificationIcon:Landroid/widget/ImageView;

.field private mProgressIndicator:Landroid/widget/LinearLayout;

.field private mProgressUnit:[Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->updateNotificationBar()V

    return-void
.end method

.method private initializeProgressIndicator()V
    .locals 5

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->getNumberOfMainStates()I

    move-result v2

    new-array v0, v2, [Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressUnit:[Landroid/widget/CheckBox;

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressUnit:[Landroid/widget/CheckBox;

    new-instance v4, Landroid/widget/CheckBox;

    invoke-direct {v4, p0}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    aput-object v4, v3, v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressUnit:[Landroid/widget/CheckBox;

    aget-object v3, v3, v0

    const v4, 0x7f02006c

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setButtonDrawable(I)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressUnit:[Landroid/widget/CheckBox;

    aget-object v3, v3, v0

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressIndicator:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressUnit:[Landroid/widget/CheckBox;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateNotificationBar()V
    .locals 4

    const/16 v1, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->isOnMainFlow()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->getNumberOfMainStates()I

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressIndicator:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mNotificationIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressIndicator:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressIndicator:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->getMainFlowProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressIndicator:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->getResourceId()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mNotificationIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mNotificationIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->getResourceId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mNotificationIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method protected loadCurrentFragment(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->loadCurrentFragment(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->updateNotificationBar()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;->TOS_AND_UMA:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mCurrentState:Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity$State;

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->setRequestedOrientation(I)V

    const v0, 0x7f0f004b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mNotificationIcon:Landroid/widget/ImageView;

    const v0, 0x7f0f004d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->mProgressIndicator:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->initializeProgressIndicator()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone$1;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;)V

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunExperienceActivityPhone;->loadCurrentFragment(Landroid/os/Bundle;)V

    return-void
.end method
