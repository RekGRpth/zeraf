.class public Lcom/google/android/apps/chrome/PasswordPrefEdit;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final FRAGMENT_NAME:Ljava/lang/String; = "com.google.android.apps.chrome.PasswordPrefEdit"


# instance fields
.field private mException:Z

.field private mID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/PasswordPrefEdit;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/PasswordPrefEdit;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/PasswordPrefEdit;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/PasswordPrefEdit;->removeItem()V

    return-void
.end method

.method private hookupCancelDeleteButtons(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f0f009b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/chrome/PasswordPrefEdit$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/PasswordPrefEdit$1;-><init>(Lcom/google/android/apps/chrome/PasswordPrefEdit;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f009c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/chrome/PasswordPrefEdit$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/PasswordPrefEdit$2;-><init>(Lcom/google/android/apps/chrome/PasswordPrefEdit;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private removeItem()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/PasswordPrefEdit;->mException:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/PasswordPrefEdit;->mID:I

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->removeSavedPasswordException(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/PasswordPrefEdit;->mID:I

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->removeSavedNamePassword(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    invoke-super {p0, p3}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040029

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0700dd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/PasswordPrefEdit;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    sget-boolean v0, Lcom/google/android/apps/chrome/PasswordPrefEdit;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const-string v0, "id"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/PasswordPrefEdit;->mID:I

    const/4 v0, 0x0

    const-string v1, "name"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "name"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    const v0, 0x7f0f0099

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const-string v0, "url"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0f009a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/PasswordPrefEdit;->hookupCancelDeleteButtons(Landroid/view/View;)V

    return-object v2

    :cond_1
    const v1, 0x7f0700f1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/PasswordPrefEdit;->mException:Z

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method
