.class public Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static sStackBufferHeight:F

.field public static sStackBufferWidth:F

.field public static sStackedTabVisibleSize:F


# instance fields
.field private mAlpha:F

.field private mCacheStackVisibility:F

.field private mCachedIndexDistance:F

.field private mCachedVisibleArea:F

.field private mDiscardAmount:F

.field protected mDying:Z

.field private mIndex:I

.field private mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mOrderSortingValue:I

.field private mRotation:F

.field private mScale:F

.field private mScrollOffset:F

.field private mVisiblitySortingValue:J

.field private mXInStackInfluence:F

.field private mXInStackOffset:F

.field private mXOutOfStack:F

.field private mYInStackInfluence:F

.field private mYInStackOffset:F

.field private mYOutOfStack:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V
    .locals 3

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackInfluence:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackInfluence:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXOutOfStack:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYOutOfStack:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mAlpha:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScale:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mRotation:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardAmount:F

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDying:Z

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedVisibleArea:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedIndexDistance:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mVisiblitySortingValue:J

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    return-void
.end method

.method private static computeOrderSortingValue(FF)I
    .locals 3

    const/high16 v0, 0x3f800000

    add-float/2addr v0, p0

    const v1, 0x3dcccccd

    const v2, 0x3f666666

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private static computeVisibilitySortingValue(FFF)J
    .locals 2

    mul-float v0, p0, p2

    sub-float/2addr v0, p1

    float-to-long v0, v0

    return-wide v0
.end method

.method public static resetDimensionConstants(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    const v1, 0x7f080011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackBufferWidth:F

    const v1, 0x7f080010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackBufferHeight:F

    return-void
.end method

.method public static screenToScroll(FF)F
    .locals 2

    const/4 v0, 0x0

    cmpg-float v1, p0, v0

    if-gtz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    cmpl-float v0, p0, p1

    if-ltz v0, :cond_1

    add-float v0, p0, p1

    goto :goto_0

    :cond_1
    mul-float v0, p0, p1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    const/high16 v1, 0x40000000

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method public static scrollToScreen(FF)F
    .locals 3

    const/high16 v2, 0x40000000

    const/4 v0, 0x0

    cmpg-float v1, p0, v0

    if-gtz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    mul-float v0, p1, v2

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_1

    sub-float v0, p0, p1

    goto :goto_0

    :cond_1
    sub-float v0, p0, p1

    mul-float v1, p1, v2

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    add-float/2addr v0, v1

    mul-float/2addr v0, v0

    mul-float/2addr v0, p1

    goto :goto_0
.end method


# virtual methods
.method public addToDiscardAmount(F)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardAmount:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardAmount:F

    return-void
.end method

.method public getAlpha()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mAlpha:F

    return v0
.end method

.method public getDiscardAmount()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardAmount:F

    return v0
.end method

.method public getId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    return v0
.end method

.method public getIndex()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mIndex:I

    return v0
.end method

.method public getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    return-object v0
.end method

.method public getOrderSortingValue()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    return v0
.end method

.method public getRotation()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mRotation:F

    return v0
.end method

.method public getScale()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScale:F

    return v0
.end method

.method public getScrollOffset()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScrollOffset:F

    return v0
.end method

.method public getSizeInScrollDirection(I)F
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v0

    goto :goto_0
.end method

.method public getVisiblitySortingValue()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mVisiblitySortingValue:J

    return-wide v0
.end method

.method public getXInStackInfluence()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackInfluence:F

    return v0
.end method

.method public getXInStackOffset()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackOffset:F

    return v0
.end method

.method public getXOutOfStack()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXOutOfStack:F

    return v0
.end method

.method public getYInStackInfluence()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackInfluence:F

    return v0
.end method

.method public getYInStackOffset()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackOffset:F

    return v0
.end method

.method public getYOutOfStack()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYOutOfStack:F

    return v0
.end method

.method public isDying()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDying:Z

    return v0
.end method

.method public resetOffset()V
    .locals 2

    const/high16 v1, 0x3f800000

    const/4 v0, 0x0

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackInfluence:F

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackInfluence:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXOutOfStack:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYOutOfStack:F

    return-void
.end method

.method public setAlpha(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mAlpha:F

    return-void
.end method

.method public setDiscardAmount(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardAmount:F

    return-void
.end method

.method public setDying(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDying:Z

    return-void
.end method

.method public setLayoutTab(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    return-void
.end method

.method public setNewIndex(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mIndex:I

    return-void
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;F)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$StackTab$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScale(F)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setAlpha(F)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setRotation(F)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setXInStackInfluence(F)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setXInStackOffset(F)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setXOutOfStack(F)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYInStackInfluence(F)V

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYInStackOffset(F)V

    goto :goto_0

    :pswitch_9
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYOutOfStack(F)V

    goto :goto_0

    :pswitch_a
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardAmount(F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;F)V

    return-void
.end method

.method public setRotation(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mRotation:F

    return-void
.end method

.method public setScale(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScale:F

    return-void
.end method

.method public setScrollOffset(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScrollOffset:F

    return-void
.end method

.method public setXInStackInfluence(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackInfluence:F

    return-void
.end method

.method public setXInStackOffset(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackOffset:F

    return-void
.end method

.method public setXOutOfStack(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXOutOfStack:F

    return-void
.end method

.method public setYInStackInfluence(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackInfluence:F

    return-void
.end method

.method public setYInStackOffset(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackOffset:F

    return-void
.end method

.method public setYOutOfStack(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYOutOfStack:F

    return-void
.end method

.method public updateStackVisiblityValue(F)V
    .locals 3

    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedIndexDistance:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->computeOrderSortingValue(FF)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedVisibleArea:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->computeVisibilitySortingValue(FFF)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mVisiblitySortingValue:J

    return-void
.end method

.method public updateVisiblityValue(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->computeVisibleArea()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedVisibleArea:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mIndex:I

    sub-int/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedIndexDistance:F

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedIndexDistance:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->computeOrderSortingValue(FF)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedVisibleArea:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->computeVisibilitySortingValue(FFF)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mVisiblitySortingValue:J

    return-void
.end method
