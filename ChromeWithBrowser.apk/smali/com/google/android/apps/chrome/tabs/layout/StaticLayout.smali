.class public Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private final mHandlesTabCloseAll:Z

.field private final mHandlesTabClosing:Z

.field private final mHandlesTabCreating:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;ZZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput-boolean p5, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandlesTabCreating:Z

    iput-boolean p6, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandlesTabClosing:Z

    iput-boolean p7, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandlesTabCloseAll:Z

    return-void
.end method

.method private setStaticTab(I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getModelFromTabId(I)Lcom/google/android/apps/chrome/tabs/DeferredTabModel;

    move-result-object v0

    if-eqz v0, :cond_0

    new-array v1, v4, [Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v1, v1

    if-eq v1, v4, :cond_3

    :cond_2
    new-array v1, v4, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModel;->isIncognito()Z

    move-result v0

    invoke-virtual {p0, p1, v0, v3, v3}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    aput-object v0, v1, v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setDrawDecoration(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->requestRender()V

    goto :goto_0
.end method


# virtual methods
.method public cleanupInstanceData(J)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->cleanupInstanceData(J)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    return-void
.end method

.method public getFreezeTextureSource()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public handlesCloseAll()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandlesTabCloseAll:Z

    return v0
.end method

.method public handlesTabClosing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandlesTabClosing:Z

    return v0
.end method

.method public handlesTabCreating()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandlesTabCreating:Z

    return v0
.end method

.method public isTabInteractive()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldDisplayContentOverlay()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public shouldDisplayTabDecoration()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public show(JZ)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentTabId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStaticTab(I)V

    return-void
.end method

.method public supportsFullscreen()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public tabCreated(JIIIZZ)V
    .locals 0

    invoke-super/range {p0 .. p7}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabCreated(JIIIZZ)V

    if-nez p7, :cond_0

    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStaticTab(I)V

    :cond_0
    return-void
.end method

.method public tabModelSwitched(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabModelSwitched(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mDeferredTabModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->getCurrentTabId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStaticTab(I)V

    return-void
.end method

.method public tabSelected(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStaticTab(I)V

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabSelected(I)V

    return-void
.end method

.method public tabSelecting(JI)V
    .locals 0

    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStaticTab(I)V

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabSelecting(JI)V

    return-void
.end method

.method protected updateLayout(JJ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    :cond_0
    return-void
.end method
