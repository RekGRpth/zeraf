.class public Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
.implements Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;
.implements Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DT:J = 0x10L

.field private static final NOTIFICATIONS:[I

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

.field protected mBlackHoleEventFilter:Lcom/google/android/apps/chrome/eventfilter/BlackHoleEventFilter;

.field protected mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

.field protected mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

.field private mFullscreenToken:I

.field protected final mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

.field private mLastChangedHeight:I

.field private mLastChangedWidth:I

.field private mLastContentHeight:I

.field private mLastContentWidth:I

.field private mLastViewport:Landroid/graphics/Rect;

.field private mLastVisibleViewport:Landroid/graphics/Rect;

.field private mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field protected mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

.field protected mSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

.field protected mTabBorder:Lcom/google/android/apps/chrome/tabs/TabBorder;

.field private final mTabBorderHandle:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

.field private final mTabCache:Ljava/util/HashMap;

.field protected mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

.field protected mTabShadow:Lcom/google/android/apps/chrome/tabs/TabShadow;

.field private final mTabShadowHandle:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

.field protected mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

.field protected mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

.field private mUpdateRequested:Z

.field private mVisibilityState:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->TAG:Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->NOTIFICATIONS:[I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :array_0
    .array-data 4
        0x3
        0x2
        0x5
        0xc
        0x4
        0x3f
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->HIDDEN:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mVisibilityState:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabCache:Ljava/util/HashMap;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mFullscreenToken:I

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$1;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$2;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabBorderHandle:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$3;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabShadowHandle:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-direct {v1, v0, p2, p0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    new-instance v1, Lcom/google/android/apps/chrome/eventfilter/BlackHoleEventFilter;

    invoke-direct {v1, p2}, Lcom/google/android/apps/chrome/eventfilter/BlackHoleEventFilter;-><init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mBlackHoleEventFilter:Lcom/google/android/apps/chrome/eventfilter/BlackHoleEventFilter;

    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedWidth:I

    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedHeight:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedWidth:I

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentWidth:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedHeight:I

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentHeight:I

    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentWidth:I

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentHeight:I

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastViewport:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentWidth:I

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentHeight:I

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastVisibleViewport:Landroid/graphics/Rect;

    new-instance v1, Lcom/google/android/apps/chrome/tabs/TabBorder;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/tabs/TabBorder;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabBorder:Lcom/google/android/apps/chrome/tabs/TabBorder;

    new-instance v1, Lcom/google/android/apps/chrome/tabs/TabShadow;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/tabs/TabShadow;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabShadow:Lcom/google/android/apps/chrome/tabs/TabShadow;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->tabModelSwitched(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;IIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->tabMoved(IIZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabCache:Ljava/util/HashMap;

    return-object v0
.end method

.method private emptyCachesExcept(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/tabs/TitleCache;->clearExcept(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->unpinAllExcept(I)V

    :cond_1
    return-void
.end method

.method private getOrientation()I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedWidth:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedHeight:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private registerNotifications()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getNotificationsToRegisterFor()[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->registerForNotifications()V

    return-void
.end method

.method private setVisibilityState(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mVisibilityState:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    return-void
.end method

.method private tabModelSwitched(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabModelSwitched(Z)V

    :cond_0
    return-void
.end method

.method private tabMoved(IIZ)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabMoved(IIZ)V

    :cond_0
    return-void
.end method

.method public static time()J
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private updateLayoutFromViewport()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getViewport()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentWidth:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentHeight:I

    if-eq v0, v2, :cond_1

    :cond_0
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentWidth:I

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentHeight:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    int-to-float v4, v1

    int-to-float v5, v2

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setContentSize(FF)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedWidth:I

    if-ne v1, v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedHeight:I

    if-eq v2, v0, :cond_3

    :cond_2
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedWidth:I

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedHeight:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getOrientation()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->sizeChanged(III)V

    :cond_3
    return-void
.end method


# virtual methods
.method public cleanupLayout()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->cleanupInstanceData(J)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->cleanupInstanceData(J)V

    return-void
.end method

.method public createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 9

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabBorderHandle:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabShadowHandle:Lcom/google/android/apps/chrome/utilities/ObjectHandle;

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentWidth:I

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentHeight:I

    move v1, p1

    move v2, p2

    move v7, p3

    move v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;-><init>(IZLcom/google/android/apps/chrome/utilities/ObjectHandle;Lcom/google/android/apps/chrome/utilities/ObjectHandle;IIZZ)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentWidth:I

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastContentHeight:I

    invoke-virtual {v0, v1, v2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->init(IIZZ)V

    goto :goto_0
.end method

.method public doneHiding()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->propagateAll()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->layoutVisible()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentTab()Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v0

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->emptyCachesExcept(I)V

    :cond_3
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "mNextActiveLayout cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_4
    const/4 v0, -0x1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->finalizeShowing()V

    goto :goto_0
.end method

.method public enableTabSwiping(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->enableTabSwiping(Z)V

    return-void
.end method

.method protected finalizeShowing()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->show()V

    :cond_0
    return-void
.end method

.method public getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    return-object v0
.end method

.method public getEdgeSwipeEventFilter()Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    return-object v0
.end method

.method public getLayoutToRender()Lcom/google/android/apps/chrome/tabs/layout/Layout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    return-object v0
.end method

.method protected getNotificationsToRegisterFor()[I
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getOverviewBehavior()Lcom/google/android/apps/chrome/OverviewBehavior;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTabBorder()Lcom/google/android/apps/chrome/tabs/TabBorder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabBorder:Lcom/google/android/apps/chrome/tabs/TabBorder;

    return-object v0
.end method

.method protected getTabById(I)Lcom/google/android/apps/chrome/Tab;
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v3, v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-nez v0, :cond_2

    :goto_2
    invoke-interface {v3, v1}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModel;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    move-object v0, v3

    goto :goto_0
.end method

.method public getTabShadow()Lcom/google/android/apps/chrome/tabs/TabShadow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabShadow:Lcom/google/android/apps/chrome/tabs/TabShadow;

    return-object v0
.end method

.method public getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    return-object v0
.end method

.method protected getTitleBitmap(Lcom/google/android/apps/chrome/Tab;)Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewport()Landroid/graphics/Rect;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->supportsFullscreen()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastViewport:Landroid/graphics/Rect;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastVisibleViewport:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method protected getVisibilityState()Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mVisibilityState:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    return-object v0
.end method

.method protected handleNotificationMessage(Landroid/os/Message;)V
    .locals 0

    return-void
.end method

.method public initLayoutTabFromHost(I)V
    .locals 4

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getTabById(I)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getBackgroundColor()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getFallbackTextureId()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->initFromHost(II)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isTitleNeeded()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getTitleBitmap(Lcom/google/android/apps/chrome/Tab;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/chrome/tabs/TitleCache;->put(ILandroid/graphics/Bitmap;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->requestRender()V

    :cond_2
    return-void
.end method

.method public layoutVisible()Z
    .locals 2

    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mVisibilityState:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->HIDDEN:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mVisibilityState:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->HIDDEN:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onContextChanged(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TabBorder;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/tabs/TabBorder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabBorder:Lcom/google/android/apps/chrome/tabs/TabBorder;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/TabShadow;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/tabs/TabShadow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabShadow:Lcom/google/android/apps/chrome/tabs/TabShadow;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->contextChanged(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public onEndGesture()V
    .locals 0

    return-void
.end method

.method public onHostFocusChanged(Z)V
    .locals 0

    return-void
.end method

.method public onStartGesture()V
    .locals 0

    return-void
.end method

.method public onUpdate()V
    .locals 4

    const-string v0, "LayoutManager:onUpdate"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->begin(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->time()J

    move-result-wide v0

    const-wide/16 v2, 0x10

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->onUpdate(JJ)Z

    const-string v0, "LayoutManager:onUpdate"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->end(Ljava/lang/String;)V

    return-void
.end method

.method public onUpdate(JJ)Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mUpdateRequested:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mUpdateRequested:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpdate(JJ)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isHiding()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->doneHiding()V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mUpdateRequested:Z

    goto :goto_0
.end method

.method public onViewportSizeChanged(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastViewport:Landroid/graphics/Rect;

    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastVisibleViewport:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->updateLayoutFromViewport()V

    return-void
.end method

.method public pushDebugValues(FFF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->pushDebugValues(FFF)V

    return-void
.end method

.method public releaseTabLayout(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/tabs/TitleCache;->remove(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->unpinTexture(I)V

    :cond_0
    return-void
.end method

.method public requestUpdate()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mUpdateRequested:Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->requestRender()V

    return-void
.end method

.method public resetThumbnailCachePriorities()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;->updateVisibleIds(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method protected setLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->setNextLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedWidth:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedHeight:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->contextChanged(Landroid/content/Context;)V

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedWidth:I

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mLastChangedHeight:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getOrientation()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->sizeChanged(III)V

    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->requestRender()V

    goto :goto_0
.end method

.method protected setNextLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V
    .locals 0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    return-void
.end method

.method public setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {p1}, Lcom/google/android/apps/chrome/TabModelSelector;->getThumbnailCache()Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getTitleCache()Lcom/google/android/apps/chrome/tabs/TitleCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;-><init>(Lcom/google/android/apps/chrome/TabModelSelector;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->setTabModelSelector(Lcom/google/android/apps/chrome/TabModelSelector;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->setTabModelSelector(Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mThumbnailCache:Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->setTabModelSelector(Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;Lcom/google/android/apps/chrome/glui/thumbnail/ThumbnailCache;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->finalizeShowing()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->registerNotifications()V

    return-void
.end method

.method protected shouldTabBeLaunchedInForeground(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/TabModel$TabLaunchType;->FROM_LONGPRESS:Lcom/google/android/apps/chrome/TabModel$TabLaunchType;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public simulateClick(II)V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->time()J

    move-result-wide v1

    int-to-float v3, p1

    int-to-float v4, p2

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->click(JFF)V

    :cond_0
    return-void
.end method

.method public simulateDrag(IIII)V
    .locals 7

    const-wide/16 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    int-to-float v3, p1

    int-to-float v4, p2

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onDown(JFF)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    int-to-float v3, p1

    int-to-float v4, p2

    int-to-float v5, p3

    int-to-float v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->drag(JFFFF)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpOrCancel(J)V

    :cond_0
    return-void
.end method

.method public startHiding()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->PREPARING_TO_HIDE:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mVisibilityState:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->requestUpdate()V

    return-void
.end method

.method protected startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V
    .locals 3

    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "SetTabModelSelector must be called first"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/glui/thumbnail/ChromeETC1Util;->setByteBufferCacheSize(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->layoutVisible()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->loadPersitentTextureDataIfNeeded()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->init()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->setLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->time()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;->FOREGROUND:Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->setVisibilityState(Lcom/google/android/apps/chrome/tabs/layout/LayoutManager$VisibilityState;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getEventFilter()Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->setEventFilter(Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->shouldDisplayContentOverlay()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->setContentOverlayVisibility(Z)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->requestRender()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->shouldDisplayContentOverlay()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x35

    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mFullscreenToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->hideControlsPersistent(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mFullscreenToken:I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->supportsFullscreen()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsPersistent()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mFullscreenToken:I

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->updateLayoutFromViewport()V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->propagateAll()V

    goto :goto_0

    :cond_4
    const/16 v0, 0x34

    goto :goto_1
.end method

.method public swipeFinished()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->swipeFinished()V

    :cond_0
    return-void
.end method

.method public swipeFlingOccurred(FFFF)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->swipeFlingOccurred(FFFF)V

    :cond_0
    return-void
.end method

.method public swipeStarted(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->swipeStarted(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->finalizeShowing()V

    :cond_0
    return-void
.end method

.method public swipeUpdated(FF)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->swipeUpdated(FF)V

    :cond_0
    return-void
.end method

.method protected tabClosed(II)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabClosed(II)V

    :cond_0
    return-void
.end method

.method protected tabCreated(IILcom/google/android/apps/chrome/TabModel$TabLaunchType;ZZ)V
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->layoutVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0, p4}, Lcom/google/android/apps/chrome/TabModelSelector;->getModel(Z)Lcom/google/android/apps/chrome/TabModel;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/TabModel;->getTabIndexById(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->time()J

    move-result-wide v1

    invoke-virtual {p0, p3, p5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->shouldTabBeLaunchedInForeground(Lcom/google/android/apps/chrome/TabModel$TabLaunchType;Z)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v7, 0x1

    :goto_0
    move v3, p1

    move v5, p2

    move v6, p4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabCreated(JIIIZZ)V

    :cond_0
    return-void

    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public tabSelected(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mDeferredModelSelector:Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/DeferredTabModelSelector;->init()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->tabSelected(I)V

    :cond_0
    return-void
.end method

.method public tryLoadBitmap(Lcom/google/android/apps/chrome/tabs/BitmapRequester;I)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->layoutVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/tabs/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->bitmapLoaded(Lcom/google/android/apps/chrome/tabs/BitmapRequester;Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/BitmapRequester;->loadRequestAbandoned()V

    goto :goto_0
.end method

.method public unregisterNotifications()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->getNotificationsToRegisterFor()[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManager;->mEdgeSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->unregisterForNotifications()V

    return-void
.end method
