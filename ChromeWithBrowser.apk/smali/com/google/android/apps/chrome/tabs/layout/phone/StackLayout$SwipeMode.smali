.class final enum Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

.field public static final enum NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

.field public static final enum SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

.field public static final enum SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    const-string v1, "SEND_TO_STACK"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    const-string v1, "SWITCH_STACK"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    return-object v0
.end method
