.class Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;
.super Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mHeight:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$1200(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;->width:F

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mWidth:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$1300(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;->height:F

    return-void
.end method


# virtual methods
.method getHeight()F
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getWidth()F

    move-result v0

    return v0
.end method

.method getInnerMargin()F
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInnerMarginPercent:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$900(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinMaxInnerMargin:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$1000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;->width:F

    const v3, 0x3e2e147b

    mul-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method getStack0Left()F
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0Top()F

    move-result v0

    return v0
.end method

.method getStack0ToStack1TranslationX()F
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0ToStack1TranslationY()F

    move-result v0

    return v0
.end method

.method getStack0ToStack1TranslationY()F
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0ToStack1TranslationX()F

    move-result v0

    return v0
.end method

.method getStack0Top()F
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0Left()F

    move-result v0

    return v0
.end method

.method getStackIndexAt(FF)I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;->getStack0Top()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;->getHeight()F

    move-result v1

    add-float/2addr v0, v1

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method getWidth()F
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getHeight()F

    move-result v0

    return v0
.end method
