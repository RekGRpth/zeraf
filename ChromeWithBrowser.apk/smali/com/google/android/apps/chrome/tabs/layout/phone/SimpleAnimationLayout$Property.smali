.class public final enum Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

.field public static final enum DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    const-string v1, "DISCARD_AMOUNT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    return-object v0
.end method
