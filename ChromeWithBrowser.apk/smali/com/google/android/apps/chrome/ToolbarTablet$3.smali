.class Lcom/google/android/apps/chrome/ToolbarTablet$3;
.super Lcom/google/android/apps/chrome/KeyboardNavigationListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ToolbarTablet;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ToolbarTablet;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/ToolbarTablet$3;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/KeyboardNavigationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getNextFocusBackward()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$3;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$3;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$3;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$3;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$3;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    const v1, 0x7f0f0074

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getNextFocusForward()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ToolbarTablet$3;->this$0:Lcom/google/android/apps/chrome/ToolbarTablet;

    const v1, 0x7f0f006e

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
