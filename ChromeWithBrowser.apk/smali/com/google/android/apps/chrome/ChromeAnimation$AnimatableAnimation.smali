.class public Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;
.super Lcom/google/android/apps/chrome/ChromeAnimation$Animation;


# instance fields
.field private final mProperty:Ljava/lang/Enum;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJLandroid/view/animation/Interpolator;)V
    .locals 9

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move-wide v4, p5

    move-wide/from16 v6, p7

    move-object/from16 v8, p9

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;-><init>(Ljava/lang/Object;FFJJLandroid/view/animation/Interpolator;)V

    iput-object p2, p0, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->mProperty:Ljava/lang/Enum;

    return-void
.end method

.method public static addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V
    .locals 10

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-wide v5, p5

    move-wide/from16 v7, p7

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZ)V

    return-void
.end method

.method public static addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZ)V
    .locals 11

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;

    move-result-object v10

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move/from16 v9, p9

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    return-void
.end method

.method public static addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-gtz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static/range {p1 .. p10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->createAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    goto :goto_0
.end method

.method public static createAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;
    .locals 11

    new-instance v1, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move-wide v6, p4

    move-wide/from16 v8, p6

    move-object/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;-><init>(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJLandroid/view/animation/Interpolator;)V

    move/from16 v0, p8

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->setStartValueAfterStartDelay(Z)V

    return-object v1
.end method


# virtual methods
.method public checkProperty(Ljava/lang/Enum;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->mProperty:Ljava/lang/Enum;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setProperty(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->mAnimatedObject:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->mProperty:Ljava/lang/Enum;

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;->setProperty(Ljava/lang/Enum;F)V

    return-void
.end method
