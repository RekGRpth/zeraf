.class public Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
.super Ljava/lang/Object;


# static fields
.field private static final HIDE_ANIMATION_DELAY_MS:J = 0x7d0L

.field public static final INVALID_TOKEN:I = -0x1

.field private static final MAX_ANIMATION_DURATION_MS:J = 0x1f4L

.field private static final MAX_PER_FRAME_MOVEMENT_DP:I = 0x4

.field private static final MINIMUM_SHOW_DURATION_MS:J = 0xbb8L

.field private static final MIN_SCROLL_DP_TO_HIDE:I

.field private static final MSG_ID_CONTROLS_REQUEST_LAYOUT:I


# instance fields
.field private mBrowserControlOffset:F

.field mCompleteCallback:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$CompleteCallback;

.field private mControlAnimation:Landroid/animation/ObjectAnimator;

.field private final mControlContainer:Landroid/view/ViewGroup;

.field protected final mControlContainerHeight:I

.field private mCurrentAnimationIsShowing:Z

.field private mCurrentShowTime:J

.field private final mEnabled:Z

.field private final mHandler:Landroid/os/Handler;

.field private mInGesture:Z

.field private mIsForcedNonFullscreenViewport:Z

.field private mIsPersistentMode:Z

.field private mListeners:Ljava/util/ArrayList;

.field private final mMaxPerFrameMovementPx:I

.field private final mMinScrollRequiredToHide:I

.field private final mPersistentControlTokens:Ljava/util/HashSet;

.field private mPersistentControlsCurrentToken:I

.field private final mPersistentFullscreenSupported:Z

.field private mPreviousContentOffset:F

.field private mRendererContentOffset:F

.field private mRendererControlOffset:F

.field private mResizeAllowed:Z

.field private mSmoothingPosition:Z

.field private final mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

.field private mTransientShowPending:Z

.field private final mVSyncMonitor:Lorg/chromium/content/browser/VSyncMonitor;

.field protected final mWindow:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/view/Window;Landroid/view/ViewGroup;ZZLcom/google/android/apps/chrome/TabModelSelector;)V
    .locals 3

    const/high16 v1, 0x7fc00000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    iput v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mBrowserControlOffset:F

    iput v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mRendererControlOffset:F

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mListeners:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mWindow:Landroid/view/Window;

    new-instance v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$1;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mHandler:Landroid/os/Handler;

    iput-object p2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mWindow:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/4 v2, 0x0

    mul-float/2addr v2, v1

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mMinScrollRequiredToHide:I

    const/high16 v2, 0x40800000

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mMaxPerFrameMovementPx:I

    const v1, 0x7f08000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mRendererContentOffset:F

    iput-boolean p3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mEnabled:Z

    iput-boolean p4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentFullscreenSupported:Z

    iput-object p5, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    new-instance v0, Lorg/chromium/content/browser/VSyncMonitor;

    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$2;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    invoke-direct {v0, v1, v2}, Lorg/chromium/content/browser/VSyncMonitor;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/VSyncMonitor$Listener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mVSyncMonitor:Lorg/chromium/content/browser/VSyncMonitor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;F)F
    .locals 0

    iput p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mBrowserControlOffset:F

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->updateTopControlsOffset()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mSmoothingPosition:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mCurrentShowTime:J

    return-wide v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mCurrentShowTime:J

    return-wide p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;ZJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->animateIfNecessary(ZJ)V

    return-void
.end method

.method private animateIfNecessary(ZJ)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mCurrentAnimationIsShowing:Z

    if-ne p1, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->rendererControlOffset()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v0

    :goto_1
    if-eqz p1, :cond_3

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_4

    const/high16 v0, 0x7fc00000

    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mBrowserControlOffset:F

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->rendererControlOffset()F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    const/high16 v1, 0x43fa0000

    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v2

    sub-float v2, v0, v2

    iget v3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-long v1, v1

    new-instance v3, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$ControlsOffsetProperty;

    invoke-direct {v3, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$ControlsOffsetProperty;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    const/4 v4, 0x1

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v0, v4, v5

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    new-instance v3, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$3;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;Z)V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p2, p3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mCurrentAnimationIsShowing:Z

    goto :goto_0
.end method

.method private getActiveContentView()Lorg/chromium/content/browser/ContentView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mTabModelSelector:Lcom/google/android/apps/chrome/TabModelSelector;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/TabModelSelector;->getCurrentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    return-object v0
.end method

.method private getLayoutParams(Landroid/view/View;)Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    instance-of v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    goto :goto_0
.end method

.method private rendererContentOffset()F
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsForcedNonFullscreenViewport:Z

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsPersistentMode:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mRendererContentOffset:F

    goto :goto_0
.end method

.method private rendererControlOffset()F
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsForcedNonFullscreenViewport:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsPersistentMode:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mRendererControlOffset:F

    goto :goto_0
.end method

.method private resizeContentView()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getActiveContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getLayoutParams(Landroid/view/View;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->updateContentViewLayoutParams(Landroid/view/ViewGroup$MarginLayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->requestLayout()V

    goto :goto_0
.end method

.method private update()V
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mEnabled:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mTransientShowPending:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mTransientShowPending:Z

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsPersistentMode:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    neg-int v5, v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-nez v4, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    iput-object v6, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v4

    cmpl-float v4, v4, v3

    if-nez v4, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    iput-object v6, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    const/high16 v0, 0x7fc00000

    :goto_2
    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mBrowserControlOffset:F

    goto :goto_0

    :cond_6
    move v0, v3

    goto :goto_2

    :cond_7
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsPersistentMode:Z

    if-nez v3, :cond_8

    if-nez v0, :cond_9

    :cond_8
    const-wide/16 v2, 0x7d0

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->animateIfNecessary(ZJ)V

    goto :goto_0

    :cond_9
    const-wide/16 v0, 0x0

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->animateIfNecessary(ZJ)V

    goto :goto_0
.end method

.method private updateTopControlsOffset()V
    .locals 6

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mEnabled:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mBrowserControlOffset:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->rendererControlOffset()F

    move-result v0

    :cond_2
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v0, 0x0

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v2

    sub-float v3, v0, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mMaxPerFrameMovementPx:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_5

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mSmoothingPosition:Z

    iget v3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mMaxPerFrameMovementPx:I

    int-to-float v3, v3

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mVSyncMonitor:Lorg/chromium/content/browser/VSyncMonitor;

    invoke-virtual {v2}, Lorg/chromium/content/browser/VSyncMonitor;->requestUpdate()V

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setTranslationY(F)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v0, v2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$FullscreenListener;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getVisibleContentOffset()F

    move-result v3

    invoke-interface {v0, v3}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$FullscreenListener;->onVisibleContentOffsetChanged(F)V

    goto :goto_2

    :cond_5
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mSmoothingPosition:Z

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->resizeContentView()Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getActiveContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getLayoutParams(Landroid/view/View;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->rendererContentOffset()F

    move-result v3

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v0, v0

    sub-float/2addr v3, v0

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentView;->getTranslationY()F

    move-result v0

    invoke-static {v0, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v2, v3}, Lorg/chromium/content/browser/ContentView;->setTranslationY(F)V

    :goto_3
    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_8

    invoke-virtual {v2, v1}, Lorg/chromium/content/browser/ContentView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v0, :cond_7

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x50

    const/16 v5, 0x50

    if-ne v0, v5, :cond_7

    neg-float v0, v3

    invoke-virtual {v4, v0}, Landroid/view/View;->setTranslationY(F)V

    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getContentOffset()F

    move-result v1

    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPreviousContentOffset:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$FullscreenListener;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$FullscreenListener;->onContentOffsetChanged(F)V

    goto :goto_4

    :cond_9
    iput v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPreviousContentOffset:F

    goto/16 :goto_0
.end method


# virtual methods
.method public addListener(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$FullscreenListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public getContentOffset()F
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mEnabled:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getActiveContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getLayoutParams(Landroid/view/View;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    iget v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v0, v0

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->getTranslationY()F

    move-result v1

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public getPersistentFullscreenMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsPersistentMode:Z

    return v0
.end method

.method public getVisibleContentOffset()F
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v1

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public hideControlsPersistent(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->update()V

    :cond_0
    return-void
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mInGesture:Z

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mResizeAllowed:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-ne v0, v2, :cond_1

    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mInGesture:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->updateTopControlsOffset()V

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mResizeAllowed:Z

    goto :goto_0
.end method

.method public removeListener(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager$FullscreenListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setForceNonFullscreenViewport(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsForcedNonFullscreenViewport:Z

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsForcedNonFullscreenViewport:Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mEnabled:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->resizeContentView()Z

    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->animateIfNecessary(ZJ)V

    goto :goto_0
.end method

.method public setPersistentFullscreenMode(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentFullscreenSupported:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mEnabled:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsPersistentMode:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsPersistentMode:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->resizeContentView()Z

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->animateIfNecessary(ZJ)V

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mIsPersistentMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getActiveContentView()Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->exitFullscreen()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setPositionsForTab(FF)V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mRendererControlOffset:F

    iput p2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mRendererContentOffset:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mCurrentAnimationIsShowing:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mBrowserControlOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mRendererControlOffset:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->updateTopControlsOffset()V

    :cond_1
    return-void
.end method

.method public setPositionsForTabToNonFullscreen()V
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTab(FF)V

    return-void
.end method

.method public showControlsPersistent()I
    .locals 3

    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlsCurrentToken:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlsCurrentToken:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->update()V

    return v0
.end method

.method public showControlsPersistentAndClearOldToken(I)I
    .locals 2

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsPersistent()I

    move-result v0

    return v0
.end method

.method public showControlsTransient()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mTransientShowPending:Z

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->update()V

    goto :goto_0
.end method

.method public updateContentViewLayoutParams(Landroid/view/ViewGroup$MarginLayoutParams;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mEnabled:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->rendererContentOffset()F

    move-result v0

    iget v3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    iget v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mMinScrollRequiredToHide:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_4

    move v3, v2

    :goto_1
    if-eqz v3, :cond_5

    move v0, v1

    :goto_2
    iget-boolean v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mInGesture:Z

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mResizeAllowed:Z

    if-eqz v4, :cond_0

    :cond_2
    iget-boolean v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mInGesture:Z

    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mResizeAllowed:Z

    :cond_3
    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v3, v0, :cond_0

    iput v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move v1, v2

    goto :goto_0

    :cond_4
    move v3, v1

    goto :goto_1

    :cond_5
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mControlContainerHeight:I

    goto :goto_2
.end method
