.class public Lcom/google/android/apps/chrome/ChromeContextMenuInfo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ContextMenu$ContextMenuInfo;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final MEDIA_TYPE_AUDIO:I = 0x3

.field private static final MEDIA_TYPE_IMAGE:I = 0x1

.field private static final MEDIA_TYPE_NONE:I = 0x0

.field private static final MEDIA_TYPE_PLUGIN:I = 0x4

.field private static final MEDIA_TYPE_VIDEO:I = 0x2


# instance fields
.field public final isAnchor:Z

.field public final isAudio:Z

.field public final isEditable:Z

.field public final isImage:Z

.field public final isPlugin:Z

.field public final isSelectedText:Z

.field public final isTextLink:Z

.field public final isVideo:Z

.field public final linkText:Ljava/lang/String;

.field public final linkUrl:Ljava/lang/String;

.field private mCustomItems:Ljava/util/ArrayList;

.field private final mediaType:I

.field public final selectionEndX:I

.field public final selectionEndY:I

.field public final selectionStartX:I

.field public final selectionStartY:I

.field public final selectionText:Ljava/lang/String;

.field public final srcUrl:Ljava/lang/String;

.field public final unfilteredLinkUrl:Ljava/lang/String;

.field public final x:I

.field public final y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(II)V
    .locals 14

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v0, p0

    move v1, p1

    move/from16 v2, p2

    invoke-direct/range {v0 .. v13}, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;-><init>(IIIIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private constructor <init>(IIIIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->x:I

    iput p2, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->y:I

    iput p3, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->mediaType:I

    iput p4, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->selectionStartX:I

    iput p5, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->selectionStartY:I

    iput p6, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->selectionEndX:I

    iput p7, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->selectionEndY:I

    iput-object p8, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->linkUrl:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->linkText:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->unfilteredLinkUrl:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->srcUrl:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->selectionText:Ljava/lang/String;

    iput-boolean p13, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isEditable:Z

    if-eqz p8, :cond_0

    invoke-virtual {p8}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isAnchor:Z

    if-eqz p12, :cond_1

    invoke-virtual {p12}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isSelectedText:Z

    const/4 v0, 0x1

    if-ne p3, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isImage:Z

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isAnchor:Z

    if-eqz v0, :cond_3

    if-nez p3, :cond_3

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isTextLink:Z

    const/4 v0, 0x2

    if-ne p3, v0, :cond_4

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isVideo:Z

    const/4 v0, 0x3

    if-ne p3, v0, :cond_5

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isAudio:Z

    const/4 v0, 0x4

    if-ne p3, v0, :cond_6

    const/4 v0, 0x1

    :goto_6
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->isPlugin:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    goto :goto_5

    :cond_6
    const/4 v0, 0x0

    goto :goto_6
.end method

.method private addCustomItem(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->mCustomItems:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->mCustomItems:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->mCustomItems:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/apps/chrome/ChromeContextMenuInfo$CustomMenuItem;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/chrome/ChromeContextMenuInfo$CustomMenuItem;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public getCustomItemAt(I)Lcom/google/android/apps/chrome/ChromeContextMenuInfo$CustomMenuItem;
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->mCustomItems:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->mCustomItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo$CustomMenuItem;

    return-object v0
.end method

.method public getCustomItemSize()I
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->mCustomItems:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->mCustomItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public isCustomMenu()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeContextMenuInfo;->mCustomItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
