.class Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;
.super Lcom/google/android/apps/chrome/LocationBar$UrlBar;


# instance fields
.field private mFocused:Z

.field private mKeyboardResizeModeTask:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet$1;-><init>(Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 3

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;->mFocused:Z

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->onFocusChanged(ZILandroid/graphics/Rect;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/16 v2, 0x20

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;->mFocused:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/LocationBarTablet$UrlBarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    if-eq v1, v2, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/LocationBar$UrlBar;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
