.class public Lcom/google/android/apps/chrome/compositor/Invalidator;
.super Ljava/lang/Object;


# static fields
.field private static mHost:Lcom/google/android/apps/chrome/compositor/Invalidator$Host;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static invalidate(Lcom/google/android/apps/chrome/compositor/Invalidator$Client;)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/chrome/compositor/Invalidator;->mHost:Lcom/google/android/apps/chrome/compositor/Invalidator$Host;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/compositor/Invalidator;->mHost:Lcom/google/android/apps/chrome/compositor/Invalidator$Host;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/compositor/Invalidator$Host;->deferInvalidate(Lcom/google/android/apps/chrome/compositor/Invalidator$Client;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p0}, Lcom/google/android/apps/chrome/compositor/Invalidator$Client;->doInvalidate()V

    goto :goto_0
.end method

.method public static set(Lcom/google/android/apps/chrome/compositor/Invalidator$Host;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/chrome/compositor/Invalidator;->mHost:Lcom/google/android/apps/chrome/compositor/Invalidator$Host;

    return-void
.end method
