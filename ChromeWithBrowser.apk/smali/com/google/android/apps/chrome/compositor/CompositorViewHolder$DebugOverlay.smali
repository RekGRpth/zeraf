.class Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;
.super Landroid/view/View;


# instance fields
.field private mFirstPush:Z

.field private final mPaint:Landroid/graphics/Paint;

.field private final mRectangles:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mRectangles:Ljava/util/List;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mPaint:Landroid/graphics/Paint;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mFirstPush:Z

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mRectangles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mRectangles:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mRectangles:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mFirstPush:Z

    return-void
.end method

.method public pushRect(Landroid/graphics/Rect;I)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mFirstPush:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mRectangles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mFirstPush:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->mRectangles:Ljava/util/List;

    new-instance v1, Landroid/util/Pair;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$DebugOverlay;->invalidate()V

    return-void
.end method
