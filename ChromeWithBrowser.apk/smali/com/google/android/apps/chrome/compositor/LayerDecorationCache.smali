.class public Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;
.super Ljava/lang/Object;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mHostHeight:I

.field private mHostWidth:I

.field private mNativeLayerDecorationCache:I

.field private mOrientation:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->$assertionsDisabled:Z

    const-class v0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mHostWidth:I

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mHostHeight:I

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mOrientation:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->nativeInit()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mNativeLayerDecorationCache:I

    return-void
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeInit()I
.end method

.method private native nativeUpdateBackLogoLayer(ILandroid/graphics/Bitmap;II)V
.end method

.method private native nativeUpdateBackgroundLayer(ILandroid/graphics/Bitmap;IFF)V
.end method

.method private native nativeUpdateBorderLayer(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;FFIIIIIIIIFFFFFFFF)V
.end method

.method private native nativeUpdateCloseButtonLayer(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;FFFF)V
.end method

.method private native nativeUpdateShadowLayer(ILandroid/graphics/Bitmap;FFFFF)V
.end method

.method private updateBackLogo(Landroid/content/Context;)V
    .locals 4

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mNativeLayerDecorationCache:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020090

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/tabs/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getBackColor(Landroid/content/Context;)I

    move-result v2

    invoke-static {p1}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getIncognitoBackColor(Landroid/content/Context;)I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->nativeUpdateBackLogoLayer(ILandroid/graphics/Bitmap;II)V

    return-void
.end method

.method private updateBackground(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/Layout;Landroid/view/View;)V
    .locals 7

    const/high16 v6, 0x40000000

    const/4 v5, 0x0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p3, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v2

    const/4 v3, 0x0

    aget v3, v0, v3

    iget v4, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v4, v2, v6

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v2

    const/4 v3, 0x1

    aget v0, v0, v3

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    sub-int v0, v2, v0

    int-to-float v0, v0

    div-float v5, v0, v6

    :goto_1
    iget v1, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mNativeLayerDecorationCache:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020091

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/tabs/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getBackgroundColor()I

    move-result v3

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->nativeUpdateBackgroundLayer(ILandroid/graphics/Bitmap;IFF)V

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1
.end method

.method private updateBorder(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/TabBorder;Lcom/google/android/apps/chrome/tabs/layout/Layout;)V
    .locals 23

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getFrameBitmapA()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getFrameBitmapB()Landroid/graphics/Rect;

    move-result-object v14

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getFrameBitmapMargin()Landroid/graphics/RectF;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getHeaderBitmapPlacement()Landroid/graphics/RectF;

    move-result-object v22

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mNativeLayerDecorationCache:I

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02001c

    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/tabs/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02001a

    invoke-static {v4, v5}, Lcom/google/android/apps/chrome/tabs/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getMinContentSizeToBeVisible()F

    move-result v5

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getWidth()I

    move-result v6

    int-to-float v6, v6

    invoke-static {v6}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getHeaderWidth(F)F

    move-result v6

    iget v7, v1, Landroid/graphics/Rect;->left:I

    iget v8, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v9

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v10

    iget v11, v14, Landroid/graphics/Rect;->left:I

    iget v12, v14, Landroid/graphics/Rect;->top:I

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v13

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v14

    move-object/from16 v0, v18

    iget v15, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v16, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v22, v0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v22}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->nativeUpdateBorderLayer(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;FFIIIIIIIIFFFFFFFF)V

    goto/16 :goto_0
.end method

.method private updateCloseButton(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/TabBorder;)V
    .locals 8

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tabs/TabBorder;->getCloseButtonMargin()Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mNativeLayerDecorationCache:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020049

    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/tabs/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02004a

    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/tabs/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    iget v4, v0, Landroid/graphics/RectF;->left:F

    iget v5, v0, Landroid/graphics/RectF;->top:F

    iget v6, v0, Landroid/graphics/RectF;->right:F

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->nativeUpdateCloseButtonLayer(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;FFFF)V

    goto :goto_0
.end method

.method private updateShadow(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/TabShadow;Lcom/google/android/apps/chrome/tabs/layout/Layout;)V
    .locals 8

    const/4 v0, 0x1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getOrientation()I

    move-result v1

    if-ne v1, v0, :cond_1

    :goto_1
    invoke-virtual {p2, v0}, Lcom/google/android/apps/chrome/tabs/TabShadow;->getMargin(Z)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mNativeLayerDecorationCache:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02001b

    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/tabs/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tabs/TabShadow;->getRadius()F

    move-result v3

    iget v4, v0, Landroid/graphics/RectF;->left:F

    iget v5, v0, Landroid/graphics/RectF;->top:F

    iget v6, v0, Landroid/graphics/RectF;->right:F

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->nativeUpdateShadowLayer(ILandroid/graphics/Bitmap;FFFFF)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public shutDown()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mNativeLayerDecorationCache:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->nativeDestroy(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mNativeLayerDecorationCache:I

    return-void
.end method

.method public update(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Landroid/view/View;)V
    .locals 3

    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "The LayoutProvider must be set"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-interface {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getLayoutToRender()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "The Layout to render must be set"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    invoke-interface {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getLayoutToRender()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getOrientation()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mOrientation:I

    if-eq v1, v0, :cond_2

    invoke-interface {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getTabShadow()Lcom/google/android/apps/chrome/tabs/TabShadow;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getLayoutToRender()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v2

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->updateShadow(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/TabShadow;Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    invoke-interface {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getTabBorder()Lcom/google/android/apps/chrome/tabs/TabBorder;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getLayoutToRender()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v2

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->updateBorder(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/TabBorder;Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    invoke-interface {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getTabBorder()Lcom/google/android/apps/chrome/tabs/TabBorder;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->updateCloseButton(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/TabBorder;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->updateBackLogo(Landroid/content/Context;)V

    :cond_2
    iget v1, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mHostWidth:I

    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v2

    if-ne v1, v2, :cond_3

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mHostHeight:I

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v2

    if-eq v1, v2, :cond_4

    :cond_3
    invoke-interface {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getLayoutToRender()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-direct {p0, p1, v1, p3}, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->updateBackground(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/Layout;Landroid/view/View;)V

    :cond_4
    iput v0, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mOrientation:I

    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mHostWidth:I

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/LayerDecorationCache;->mHostHeight:I

    return-void
.end method
