.class Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/content/browser/VSyncMonitor$Listener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVSync(Lorg/chromium/content/browser/VSyncMonitor;J)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$000(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const-wide/16 v0, 0xc80

    add-long/2addr p2, v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mViewVisible:Lorg/chromium/content/browser/ContentView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$000(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mVSyncMonitor:Lorg/chromium/content/browser/VSyncMonitor;
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$100(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/VSyncMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/VSyncMonitor;->getVSyncPeriodInMicroseconds()J

    move-result-wide v1

    invoke-virtual {v0, p2, p3, v1, v2}, Lorg/chromium/content/browser/ContentViewCore;->updateVSync(JJ)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNeedToRender:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$300(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$400(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x2

    if-gt v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNeedToRender:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$202(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # operator++ for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingRenders:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$408(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # invokes: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->render()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$500(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const-string v0, "CompositorViewHolder::bail"

    invoke-static {v0}, Lorg/chromium/content/common/TraceEvent;->instant(Ljava/lang/String;)V

    goto :goto_0
.end method
