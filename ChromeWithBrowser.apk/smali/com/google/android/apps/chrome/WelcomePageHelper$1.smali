.class Lcom/google/android/apps/chrome/WelcomePageHelper$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/WelcomePageHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/WelcomePageHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/WelcomePageHelper$1;->this$0:Lcom/google/android/apps/chrome/WelcomePageHelper;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/WelcomePageHelper$1;->this$0:Lcom/google/android/apps/chrome/WelcomePageHelper;

    # getter for: Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/WelcomePageHelper;->access$000(Lcom/google/android/apps/chrome/WelcomePageHelper;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper$1;->this$0:Lcom/google/android/apps/chrome/WelcomePageHelper;

    # invokes: Lcom/google/android/apps/chrome/WelcomePageHelper;->maybeShowSettings()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->access$100(Lcom/google/android/apps/chrome/WelcomePageHelper;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/WelcomePageHelper$1;->this$0:Lcom/google/android/apps/chrome/WelcomePageHelper;

    # getter for: Lcom/google/android/apps/chrome/WelcomePageHelper;->mTab:Lcom/google/android/apps/chrome/Tab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/WelcomePageHelper;->access$000(Lcom/google/android/apps/chrome/WelcomePageHelper;)Lcom/google/android/apps/chrome/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/WelcomePageHelper$1;->this$0:Lcom/google/android/apps/chrome/WelcomePageHelper;

    # invokes: Lcom/google/android/apps/chrome/WelcomePageHelper;->sendSyncEnabled()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/WelcomePageHelper;->access$200(Lcom/google/android/apps/chrome/WelcomePageHelper;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x9 -> :sswitch_1
        0x1b -> :sswitch_0
    .end sparse-switch
.end method
