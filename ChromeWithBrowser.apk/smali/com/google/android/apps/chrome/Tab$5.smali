.class Lcom/google/android/apps/chrome/Tab$5;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/Tab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/Tab;Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/Tab$5;->this$0:Lcom/google/android/apps/chrome/Tab;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content/browser/ContentViewCore;)V

    return-void
.end method


# virtual methods
.method public didFailLoad(ZZILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$5;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->notifyPageLoadFailed(I)V
    invoke-static {v0, p3}, Lcom/google/android/apps/chrome/Tab;->access$4000(Lcom/google/android/apps/chrome/Tab;I)V

    :cond_0
    return-void
.end method

.method public didFinishLoad(JLjava/lang/String;Z)V
    .locals 1

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$5;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->didFinishPageLoad(Ljava/lang/String;)V
    invoke-static {v0, p3}, Lcom/google/android/apps/chrome/Tab;->access$3900(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public didNavigateMainFrame(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$5;->this$0:Lcom/google/android/apps/chrome/Tab;

    # setter for: Lcom/google/android/apps/chrome/Tab;->mBaseUrl:Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/Tab;->access$4102(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method public didStartProvisionalLoadForFrame(JJZLjava/lang/String;ZZ)V
    .locals 1

    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/Tab$5;->this$0:Lcom/google/android/apps/chrome/Tab;

    # invokes: Lcom/google/android/apps/chrome/Tab;->didStartPageLoad(Ljava/lang/String;)V
    invoke-static {v0, p6}, Lcom/google/android/apps/chrome/Tab;->access$4200(Lcom/google/android/apps/chrome/Tab;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
