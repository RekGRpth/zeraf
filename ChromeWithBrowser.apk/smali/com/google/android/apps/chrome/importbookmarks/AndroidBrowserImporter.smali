.class public Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;
.super Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;


# static fields
.field private static final ANDROID_BROWSER_AUTHORITIES:Ljava/lang/String; = "com.android.browser;browser"

.field private static final ANDROID_BROWSER_DATABASE_FILE:Ljava/lang/String; = "android_browser.db"

.field private static final BROWSER_PROVIDER_PROXY_PACKAGE:Ljava/lang/String; = "com.android.browser.provider"

.field private static final TAG:Ljava/lang/String; = "AndroidBrowserImporter"

.field private static final TEMPORARY_DATABASE_FILE:Ljava/lang/String; = "android_browser_temp.db"


# instance fields
.field private mDatabaseFile:Ljava/io/File;

.field private mIgnoreAvailableProvidersForTestPurposes:Z

.field private mInputResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    const-string v0, "android_browser.db"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;)Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    return-object v0
.end method

.method private areProvidersValid()Z
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mIgnoreAvailableProvidersForTestPurposes:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.android.browser.provider"

    const/16 v4, 0x8

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, v2, Landroid/content/pm/PackageInfo;->providers:[Landroid/content/pm/ProviderInfo;

    if-nez v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    if-eqz v5, :cond_3

    iget-object v6, v5, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v5, v5, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    const-string v6, "com.android.browser;browser"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static isDatabaseFilePresent(Landroid/content/Context;)Z
    .locals 1

    const-string v0, "android_browser.db"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public areBookmarksAccessible()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->areProvidersValid()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    invoke-static {v2}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->isProviderAvailable(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    invoke-static {v2}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->isProviderAvailable(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected availableBookmarks()[Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$BookmarkIterator;
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    invoke-static {v1}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;->createIfAvailable(Ljava/io/File;)Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "AndroidBrowserImporter"

    const-string v2, "Error accessing Android Browser database."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "AndroidBrowserImporter"

    const-string v2, "Importing bookmarks from Android Browser\'s database."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v4, [Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$BookmarkIterator;

    invoke-static {v1}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->createIfAvailable(Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserDatabaseProvider;)Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->areProvidersValid()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$BookmarkIterator;

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    invoke-static {v1}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;->createIfAvailable(Landroid/content/ContentResolver;)Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPrivateProviderIterator;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    invoke-static {v1}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;->createIfAvailable(Landroid/content/ContentResolver;)Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserPublicProviderIterator;

    move-result-object v1

    aput-object v1, v0, v4

    goto :goto_0
.end method

.method public importFromDatabaseAfterOTA()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mContext:Landroid/content/Context;

    const-string v2, "android_browser_temp.db"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, "AndroidBrowserImporter"

    const-string v2, "Temporary file deletion failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    invoke-virtual {v2, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v1, "AndroidBrowserImporter"

    const-string v2, "Database file renaming failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    new-instance v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter$1;-><init>(Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->importBookmarks(Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$OnBookmarksImportedListener;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method setDatabaseFile(Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    return-void
.end method

.method setIgnoreAvailableProvidersForTestPurposes(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mIgnoreAvailableProvidersForTestPurposes:Z

    return-void
.end method

.method setInputResolver(Landroid/content/ContentResolver;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    return-void
.end method
