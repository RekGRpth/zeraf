.class public Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;

# interfaces
.implements Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$OnBookmarksImportedListener;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mImporter:Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;

.field private mProgressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onBookmarksImported(Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportResults;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    if-eqz p1, :cond_1

    iget v0, p1, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportResults;->numImported:I

    iget v1, p1, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportResults;->newBookmarks:I

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "chrome://newtab/#bookmarks:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$ImportResults;->rootFolderId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    const-class v3, Lcom/google/android/apps/chrome/Main;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksRetryDialog;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksRetryDialog;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksRetryDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    const v2, 0x7f07005a

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    const v3, 0x7f07005b

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v4, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/importbookmarks/AndroidBrowserImporter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mImporter:Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mImporter:Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->importBookmarks(Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter$OnBookmarksImportedListener;)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mImporter:Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/importbookmarks/ImportBookmarksProgressDialog;->mImporter:Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/importbookmarks/BookmarkImporter;->cancel()V

    :cond_0
    return-void
.end method
