.class public final Lcom/google/android/apps/chrome/GoogleLocationSettingsHelper;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "GoogleLocationSettingHelper"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInfoBarAcceptLabel(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/chrome/GoogleLocationSettingsUtil;->isGoogleLocationSettingsAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/GoogleLocationSettingsUtil;->isMasterLocationProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/GoogleLocationSettingsUtil;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070171

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static isGoogleAppsLocationSettingEnabled(Landroid/content/Context;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-static {p0}, Lcom/google/android/apps/chrome/GoogleLocationSettingsUtil;->isGoogleLocationSettingsAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/GoogleLocationSettingsUtil;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMasterLocationSettingEnabled(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/GoogleLocationSettingsUtil;->isGoogleLocationSettingsAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/GoogleLocationSettingsUtil;->isMasterLocationProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAllowLocationEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public static startGoogleLocationSettingsActivity(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/chrome/GoogleLocationSettingsUtil;->getGoogleLocationSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
