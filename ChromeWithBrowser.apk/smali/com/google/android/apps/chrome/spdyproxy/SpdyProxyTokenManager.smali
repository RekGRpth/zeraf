.class public Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;
.super Ljava/lang/Object;


# static fields
.field private static final GOOGLE_ACCOUNT_TYPE:Ljava/lang/String; = "com.google"

.field private static final SID_TOKEN_TYPE:Ljava/lang/String; = "SID"

.field private static final TAG:Ljava/lang/String; = "SpdyProxyTokenManager"

.field static final lock:Ljava/lang/Object;

.field private static sInstance:Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;


# instance fields
.field private final mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

.field private mSid:Ljava/lang/String;

.field private final mSystemAccountManager:Landroid/accounts/AccountManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->lock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSystemAccountManager:Landroid/accounts/AccountManager;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSid:Ljava/lang/String;

    return-object p1
.end method

.method public static get(Landroid/content/Context;)Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;
    .locals 3

    sget-object v1, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->sInstance:Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->sInstance:Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->sInstance:Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getGoogleAccount()Landroid/accounts/Account;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v5, "@google.com"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public foregroundTokenRefresh(Landroid/app/Activity;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V
    .locals 5

    const/16 v4, 0x9

    invoke-direct {p0}, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->getGoogleAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSpdyProxyProperty()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSpdyProxyValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_3

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v4, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v4, :cond_2

    iput-object v1, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSid:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSid:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_4

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    const-string v2, "SID"

    new-instance v3, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager$1;

    invoke-direct {v3, p0, p3}, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager$1;-><init>(Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    invoke-virtual {v1, p1, v0, v2, v3}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromForeground(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/apps/chrome/utilities/Hasher;->getMd5Hash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSid:Ljava/lang/String;

    goto :goto_0

    :cond_3
    if-nez v0, :cond_0

    goto :goto_1

    :cond_4
    const-string v0, "SpdyProxyTokenManager"

    const-string v1, "Skipping SpdyProxy token refresh since we already have a non-empty token."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSid:Ljava/lang/String;

    invoke-interface {p3, v0}, Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;->tokenAvailable(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSid:Ljava/lang/String;

    return-object v0
.end method

.method public invalidateToken()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSid:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "SpdyProxyTokenManager"

    const-string v1, "Attempting to invalidate SpdyProxy token, but no SpdyProxy token present."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v0, "SpdyProxyTokenManager"

    const-string v1, "Invalidating SpdyProxy token."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSystemAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    iget-object v2, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/spdyproxy/SpdyProxyTokenManager;->mSid:Ljava/lang/String;

    goto :goto_0
.end method
