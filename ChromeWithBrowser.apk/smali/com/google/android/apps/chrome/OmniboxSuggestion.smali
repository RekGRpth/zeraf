.class public Lcom/google/android/apps/chrome/OmniboxSuggestion;
.super Ljava/lang/Object;


# instance fields
.field private final mDescription:Ljava/lang/String;

.field private final mDisplayText:Ljava/lang/String;

.field private final mIsStarred:Z

.field private final mRelevance:I

.field private final mTransition:I

.field private final mType:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

.field private final mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->GetTypeFromNativeType(I)Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mType:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    iput p2, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mRelevance:I

    iput p3, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mTransition:I

    iput-object p4, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mDescription:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mUrl:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mIsStarred:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/android/apps/chrome/OmniboxSuggestion;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/apps/chrome/OmniboxSuggestion;

    iget-object v1, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mType:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    iget-object v2, p1, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mType:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mIsStarred:Z

    iget-boolean v2, p1, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mIsStarred:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    return-object v0
.end method

.method public getRelevance()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mRelevance:I

    return v0
.end method

.method public getTransition()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mTransition:I

    return v0
.end method

.method public getType()Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mType:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mType:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    # getter for: Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->mNativeType:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->access$000(Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public isStarred()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mIsStarred:Z

    return v0
.end method

.method public isUrlSuggestion()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mType:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;->isUrl()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mType:Lcom/google/android/apps/chrome/OmniboxSuggestion$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " relevance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mRelevance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/OmniboxSuggestion;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
