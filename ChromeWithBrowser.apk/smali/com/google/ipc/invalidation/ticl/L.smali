.class final Lcom/google/ipc/invalidation/ticl/L;
.super Ljava/lang/Object;


# instance fields
.field final a:Lcom/google/ipc/invalidation/ticl/N;

.field final b:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

.field final c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

.field final d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

.field final e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

.field final f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

.field final g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;


# direct methods
.method constructor <init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;)V
    .locals 5

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v0

    new-instance v2, Lcom/google/ipc/invalidation/ticl/N;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->h()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->i()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v3, v0}, Lcom/google/ipc/invalidation/ticl/N;-><init>(Lcom/google/protobuf/ByteString;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;)V

    iput-object v2, p0, Lcom/google/ipc/invalidation/ticl/L;->a:Lcom/google/ipc/invalidation/ticl/N;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/L;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$TokenControlMessage;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->j()Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/L;->c:Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationMessage;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->l()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    move-result-object v0

    :goto_3
    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/L;->d:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationStatusMessage;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->n()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/L;->e:Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSyncRequestMessage;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->p()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    :cond_0
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->q()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->r()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    move-result-object v0

    :goto_5
    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/L;->f:Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoRequestMessage;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->t()Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    move-result-object v1

    :cond_1
    iput-object v1, p0, Lcom/google/ipc/invalidation/ticl/L;->g:Lcom/google/protos/ipc/invalidation/ClientProtocol$ErrorMessage;

    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_3

    :cond_6
    move-object v0, v1

    goto :goto_4

    :cond_7
    move-object v0, v1

    goto :goto_5
.end method
