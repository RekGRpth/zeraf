.class final Lcom/google/ipc/invalidation/ticl/a/g;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:[B

.field private synthetic b:Lcom/google/ipc/invalidation/ticl/a/d;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/a/d;[B)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/a/g;->b:Lcom/google/ipc/invalidation/ticl/a/d;

    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/a/g;->a:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/g;->b:Lcom/google/ipc/invalidation/ticl/a/d;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/a/d;->a(Lcom/google/ipc/invalidation/ticl/a/d;)Lcom/google/ipc/invalidation/external/client/SystemResources;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/a/g;->b:Lcom/google/ipc/invalidation/ticl/a/d;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/a/g;->a:[B

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/a/d;->b([B)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/ipc/invalidation/ticl/a/d;->e()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "Dropping outbound messages because resources are stopped"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
