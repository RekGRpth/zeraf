.class final Lcom/google/ipc/invalidation/ticl/q;
.super Lcom/google/ipc/invalidation/ticl/O;


# instance fields
.field private final a:Lcom/google/ipc/invalidation/ticl/I;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/I;Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/b/h;I)V
    .locals 8

    const-string v1, "Batching"

    invoke-interface {p2}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v2

    invoke-interface {p2}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v4, p3

    move v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/ipc/invalidation/ticl/O;-><init>(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/b/h;Lcom/google/ipc/invalidation/ticl/ak;II)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/q;->a:Lcom/google/ipc/invalidation/ticl/I;

    return-void
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/ticl/I;Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/b/h;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V
    .locals 7

    const-string v1, "Batching"

    invoke-interface {p2}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v2

    invoke-interface {p2}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v3

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/ticl/O;-><init>(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/b/h;Lcom/google/ipc/invalidation/ticl/ak;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/q;->a:Lcom/google/ipc/invalidation/ticl/I;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/q;->a:Lcom/google/ipc/invalidation/ticl/I;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/I;->a()V

    const/4 v0, 0x0

    return v0
.end method
