.class final Lcom/google/ipc/invalidation/ticl/ab;
.super Lcom/google/ipc/invalidation/b/e;

# interfaces
.implements Lcom/google/ipc/invalidation/ticl/k;


# instance fields
.field private final a:Ljava/util/SortedMap;

.field private final b:Lcom/google/a/b/B;

.field private c:Lcom/google/ipc/invalidation/b/c;


# direct methods
.method constructor <init>(Lcom/google/a/b/B;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/e;-><init>()V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/ab;->a:Ljava/util/SortedMap;

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/ab;->b:Lcom/google/a/b/B;

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/ab;->e()V

    return-void
.end method

.method private e()V
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ab;->a:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/ab;->b:Lcom/google/a/b/B;

    invoke-virtual {v1}, Lcom/google/a/b/B;->c()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/c;->a()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/a/b/B;->a([B)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v1}, Lcom/google/a/b/B;->d()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/b/c;-><init>([B)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/ab;->c:Lcom/google/ipc/invalidation/b/c;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ab;->a:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/ab;->a:Ljava/util/SortedMap;

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/ab;->b:Lcom/google/a/b/B;

    invoke-static {v0, v4}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Lcom/google/a/b/B;)Lcom/google/ipc/invalidation/b/c;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/ab;->e()V

    :cond_2
    return-object v1
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ab;->a:Ljava/util/SortedMap;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/ab;->b:Lcom/google/a/b/B;

    invoke-static {p1, v1}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Lcom/google/a/b/B;)Lcom/google/ipc/invalidation/b/c;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/ab;->a:Ljava/util/SortedMap;

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/ab;->b:Lcom/google/a/b/B;

    invoke-static {v0, v4}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Lcom/google/a/b/B;)Lcom/google/ipc/invalidation/b/c;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/ab;->e()V

    :cond_2
    return-object v1
.end method

.method public final synthetic b(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ab;->a:Ljava/util/SortedMap;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/ab;->b:Lcom/google/a/b/B;

    invoke-static {p1, v1}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Lcom/google/a/b/B;)Lcom/google/ipc/invalidation/b/c;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/ab;->e()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()[B
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ab;->c:Lcom/google/ipc/invalidation/b/c;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/b/c;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/ab;->a:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/Collection;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/ab;->a:Ljava/util/SortedMap;

    invoke-interface {v1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/ab;->a:Ljava/util/SortedMap;

    invoke-interface {v1}, Ljava/util/SortedMap;->clear()V

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/ab;->e()V

    return-object v0
.end method
