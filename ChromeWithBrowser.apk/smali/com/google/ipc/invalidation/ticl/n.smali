.class final Lcom/google/ipc/invalidation/ticl/n;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/types/Callback;


# instance fields
.field private synthetic a:Lcom/google/ipc/invalidation/ticl/l;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/l;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/n;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic accept(Ljava/lang/Object;)V
    .locals 6

    check-cast p1, Lcom/google/ipc/invalidation/external/client/types/SimplePair;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/n;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/types/Status;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/Status;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->getSecond()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    move-object v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/types/Status;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/Status;->isSuccess()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/n;->a:Lcom/google/ipc/invalidation/ticl/l;

    iget-object v0, v0, Lcom/google/ipc/invalidation/ticl/l;->c:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v2, Lcom/google/ipc/invalidation/ticl/ad;->e:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/n;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0}, Lcom/google/ipc/invalidation/ticl/l;->b(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string v3, "Could not read state blob: %s"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/types/Status;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/Status;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/n;->a:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;[B)V

    return-void

    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method
