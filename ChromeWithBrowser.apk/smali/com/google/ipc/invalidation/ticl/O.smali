.class public abstract Lcom/google/ipc/invalidation/ticl/O;
.super Lcom/google/ipc/invalidation/b/e;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field private final c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

.field private final d:I

.field private final e:I

.field private final f:Lcom/google/ipc/invalidation/b/h;

.field private final g:Lcom/google/ipc/invalidation/ticl/ak;

.field private final h:Lcom/google/ipc/invalidation/b/g;

.field private i:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/b/h;Lcom/google/ipc/invalidation/ticl/ak;II)V
    .locals 2

    invoke-direct {p0}, Lcom/google/ipc/invalidation/b/e;-><init>()V

    iput-object p5, p0, Lcom/google/ipc/invalidation/ticl/O;->g:Lcom/google/ipc/invalidation/ticl/ak;

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->a:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-static {p4}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/b/h;

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->f:Lcom/google/ipc/invalidation/b/h;

    iput p6, p0, Lcom/google/ipc/invalidation/ticl/O;->d:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/O;->i:Z

    iput p7, p0, Lcom/google/ipc/invalidation/ticl/O;->e:I

    new-instance v0, Lcom/google/ipc/invalidation/ticl/P;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/O;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/ticl/P;-><init>(Lcom/google/ipc/invalidation/ticl/O;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->h:Lcom/google/ipc/invalidation/b/g;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/b/h;Lcom/google/ipc/invalidation/ticl/ak;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V
    .locals 8

    invoke-virtual {p6}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->f()I

    move-result v6

    invoke-virtual {p6}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->h()I

    move-result v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/ipc/invalidation/ticl/O;-><init>(Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/b/h;Lcom/google/ipc/invalidation/ticl/ak;II)V

    invoke-virtual {p6}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/O;->i:Z

    return-void
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/O;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    return-object v0
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/O;ZLjava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p2}, Lcom/google/ipc/invalidation/ticl/O;->a(ZLjava/lang/String;)V

    return-void
.end method

.method private a(ZLjava/lang/String;)V
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/a/a/a;->b(Z)V

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/O;->i:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->g:Lcom/google/ipc/invalidation/ticl/ak;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/O;->e:I

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/O;->g:Lcom/google/ipc/invalidation/ticl/ak;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/ak;->c()I

    move-result v1

    add-int/2addr v0, v1

    :goto_1
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/O;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "[%s] Scheduling %s with a delay %s, Now = %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/O;->a:Ljava/lang/String;

    aput-object v4, v3, v7

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/O;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/O;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/O;->h:Lcom/google/ipc/invalidation/b/g;

    invoke-interface {v1, v0, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->schedule(ILjava/lang/Runnable;)V

    iput-boolean v7, p0, Lcom/google/ipc/invalidation/ticl/O;->i:Z

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/O;->e:I

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/O;->f:Lcom/google/ipc/invalidation/b/h;

    iget v2, p0, Lcom/google/ipc/invalidation/ticl/O;->d:I

    invoke-virtual {v1, v2}, Lcom/google/ipc/invalidation/b/h;->a(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->f:Lcom/google/ipc/invalidation/b/h;

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/O;->d:I

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/b/h;->a(I)I

    move-result v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/ipc/invalidation/ticl/O;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/ticl/O;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/google/ipc/invalidation/ticl/O;)Lcom/google/ipc/invalidation/ticl/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->g:Lcom/google/ipc/invalidation/ticl/ak;

    return-object v0
.end method

.method static synthetic c(Lcom/google/ipc/invalidation/ticl/O;)I
    .locals 1

    iget v0, p0, Lcom/google/ipc/invalidation/ticl/O;->d:I

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/ipc/invalidation/ticl/O;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public abstract a()Z
.end method

.method final b()Lcom/google/ipc/invalidation/b/h;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->f:Lcom/google/ipc/invalidation/b/h;

    return-object v0
.end method

.method public final c()Lcom/google/ipc/invalidation/b/g;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/O;->h:Lcom/google/ipc/invalidation/b/g;

    return-object v0
.end method

.method public final d()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;
    .locals 2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/O;->d:I

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(I)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/ipc/invalidation/ticl/O;->i:Z

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/O;->e:I

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->b(I)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/O;->g:Lcom/google/ipc/invalidation/ticl/ak;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/O;->g:Lcom/google/ipc/invalidation/ticl/ak;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/ak;->a()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->a(Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;

    :cond_0
    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState$Builder;->c()Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;

    move-result-object v0

    return-object v0
.end method
