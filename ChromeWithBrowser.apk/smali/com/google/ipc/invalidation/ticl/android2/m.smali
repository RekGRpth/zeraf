.class final Lcom/google/ipc/invalidation/ticl/android2/m;
.super Lcom/google/ipc/invalidation/ticl/l;


# static fields
.field static d:Ljava/lang/Class;


# instance fields
.field private final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/m;->d:Ljava/lang/Class;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)V
    .locals 8

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/google/ipc/invalidation/ticl/android2/n;

    invoke-interface {p2}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-direct {v7, p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/ipc/invalidation/ticl/l;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Ljava/lang/String;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)V

    invoke-interface {p2}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/android2/m;->e:J

    invoke-interface {p2}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    const-string v1, "Create new Ticl scheduling id: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/ipc/invalidation/ticl/android2/m;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/android2/m;->l()V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;)V
    .locals 9

    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->j()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;->f()I

    move-result v3

    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->j()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->c()[B

    move-result-object v4

    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->j()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;->l()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->h()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v7

    new-instance v8, Lcom/google/ipc/invalidation/ticl/android2/n;

    invoke-interface {p2}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-direct {v8, p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/n;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/ipc/invalidation/ticl/l;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Ljava/lang/String;Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;Lcom/google/ipc/invalidation/external/client/InvalidationListener;)V

    invoke-virtual {p4}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->j()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/android2/m;->e:J

    invoke-direct {p0}, Lcom/google/ipc/invalidation/ticl/android2/m;->l()V

    return-void
.end method

.method private l()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/android2/m;->e()Lcom/google/ipc/invalidation/external/client/SystemResources;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    instance-of v0, v0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;

    const-string v1, "Scheduler must be an AndroidInternalScheduler, not %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/android2/m;->e()Lcom/google/ipc/invalidation/external/client/SystemResources;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/android2/m;->e()Lcom/google/ipc/invalidation/external/client/SystemResources;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/android2/m;->i()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v2, v1}, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected final c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;
    .locals 1

    invoke-super {p0}, Lcom/google/ipc/invalidation/ticl/l;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    return-object v0
.end method

.method protected final d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;
    .locals 1

    invoke-super {p0}, Lcom/google/ipc/invalidation/ticl/l;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-result-object v0

    return-object v0
.end method

.method final k()J
    .locals 2

    iget-wide v0, p0, Lcom/google/ipc/invalidation/ticl/android2/m;->e:J

    return-wide v0
.end method
