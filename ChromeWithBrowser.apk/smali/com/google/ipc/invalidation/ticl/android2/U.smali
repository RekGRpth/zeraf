.class public final Lcom/google/ipc/invalidation/ticl/android2/U;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/ticl/android2/U;->a:Ljava/util/Random;

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;)Lcom/google/ipc/invalidation/ticl/android2/m;
    .locals 3

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/m;

    sget-object v2, Lcom/google/ipc/invalidation/ticl/android2/U;->a:Ljava/util/Random;

    invoke-direct {v0, p0, p1, v2, v1}, Lcom/google/ipc/invalidation/ticl/android2/m;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;)V

    invoke-static {p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/android2/m;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;
    .locals 10

    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    :try_start_0
    const-string v1, "android_ticl_service_state.bin"

    invoke-virtual {p0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v5

    const-wide/32 v7, 0x19000

    cmp-long v7, v5, v7

    if-lez v7, :cond_1

    const-string v2, "Ignoring too-large Ticl state file with size %s > %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v7, v8

    const/4 v5, 0x1

    const v6, 0x19000

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v7, v5

    invoke-interface {p1, v2, v7}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    long-to-int v5, v5

    :try_start_3
    new-array v5, v5, [B

    invoke-interface {v2, v5}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {v5}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;->a([B)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;

    move-result-object v5

    new-instance v2, Lcom/google/ipc/invalidation/ticl/android2/b;

    invoke-direct {v2, p1}, Lcom/google/ipc/invalidation/ticl/android2/b;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)V

    invoke-virtual {v2, v5}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "Read AndroidTiclStateWithDigest with invalid structure: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-interface {p1, v2, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    :try_start_5
    new-instance v2, Lcom/google/a/b/B;

    invoke-direct {v2}, Lcom/google/a/b/B;-><init>()V

    invoke-virtual {v5}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;->f()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b()[B

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/a/b/B;->a([B)V

    invoke-virtual {v2}, Lcom/google/a/b/B;->d()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v5}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;->h()Lcom/google/protobuf/ByteString;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "Android Ticl state digest mismatch; computed %s for %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v2, 0x1

    aput-object v5, v7, v2

    invoke-interface {p1, v6, v7}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v3

    :goto_2
    if-eqz v2, :cond_4

    invoke-virtual {v5}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;->f()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->h()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    invoke-virtual {v5}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;->f()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_5 .. :try_end_5} :catch_b
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_a
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v0

    if-eqz v1, :cond_0

    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v2, v4

    goto :goto_2

    :cond_4
    :try_start_7
    const-string v2, "Android Ticl state failed digest check: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-interface {p1, v2, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_7 .. :try_end_7} :catch_b
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_a
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_0

    :catch_2
    move-exception v2

    :goto_3
    :try_start_8
    const-string v2, "Ticl state file does not exist: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "android_ticl_service_state.bin"

    aput-object v7, v5, v6

    invoke-interface {p1, v2, v5}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-eqz v1, :cond_0

    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_1

    :catch_3
    move-exception v1

    const-string v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :catch_4
    move-exception v1

    const-string v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :catch_5
    move-exception v1

    move-object v2, v0

    :goto_4
    :try_start_a
    const-string v5, "Could not read Ticl state: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-interface {p1, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    if-eqz v2, :cond_0

    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    goto/16 :goto_1

    :catch_6
    move-exception v1

    const-string v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :catch_7
    move-exception v1

    move-object v2, v0

    :goto_5
    :try_start_c
    const-string v5, "Could not read Ticl state: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-interface {p1, v5, v6}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    if-eqz v2, :cond_0

    :try_start_d
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    goto/16 :goto_1

    :catch_8
    move-exception v1

    const-string v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_6
    if-eqz v1, :cond_5

    :try_start_e
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_9

    :cond_5
    :goto_7
    throw v0

    :catch_9
    move-exception v1

    const-string v2, "Exception closing Ticl state file: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    invoke-interface {p1, v2, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_7

    :catchall_1
    move-exception v0

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_6

    :catch_a
    move-exception v2

    move-object v9, v2

    move-object v2, v1

    move-object v1, v9

    goto :goto_5

    :catch_b
    move-exception v2

    move-object v9, v2

    move-object v2, v1

    move-object v1, v9

    goto :goto_4

    :catch_c
    move-exception v1

    move-object v1, v0

    goto/16 :goto_3
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    const-string v0, "android_ticl_service_state.bin"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/m;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Lcom/google/a/b/B;

    invoke-direct {v0}, Lcom/google/a/b/B;-><init>()V

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/android2/m;->d()Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;

    move-result-object v2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/android2/m;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->h()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->f()I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;->a(I)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/android2/m;->k()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;->a(J)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;

    move-result-object v2

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Metadata;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/ipc/invalidation/ticl/android2/m;->j()Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$InvalidationClientState;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v2

    sget-object v3, Lcom/google/ipc/invalidation/ticl/android2/P;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v2, v3}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;->b()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/a/b/B;->a([B)V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;->newBuilder()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest$Builder;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclState;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/a/b/B;->d()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest$Builder;->c()Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;

    move-result-object v0

    new-instance v2, Lcom/google/ipc/invalidation/ticl/android2/b;

    invoke-direct {v2, p1}, Lcom/google/ipc/invalidation/ticl/android2/b;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;)V

    invoke-virtual {v2, v0}, Lcom/google/ipc/invalidation/ticl/android2/b;->a(Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;)Z

    move-result v2

    const-string v3, "Produced invalid digested state: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    const-string v2, "android_ticl_service_state.bin"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidService$AndroidTiclStateWithDigest;->b()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Exception closing Ticl state file: %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-interface {p1, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v2, "Could not write Ticl state: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-interface {p1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "Exception closing Ticl state file: %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-interface {p1, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_3
    move-exception v0

    :try_start_4
    const-string v2, "Could not write Ticl state: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-interface {p1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v1, :cond_0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    const-string v1, "Exception closing Ticl state file: %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-interface {p1, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_1
    :goto_1
    throw v0

    :catch_5
    move-exception v1

    const-string v2, "Exception closing Ticl state file: %s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-interface {p1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method static a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;Z)V
    .locals 7

    const-string v0, "android_ticl_service_state.bin"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Ticl already exists"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/m;

    sget-object v3, Lcom/google/ipc/invalidation/ticl/android2/U;->a:Ljava/util/Random;

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/ipc/invalidation/ticl/android2/m;-><init>(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources;Ljava/util/Random;I[BLcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)V

    if-nez p5, :cond_0

    invoke-static {p1, v0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/android2/m;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/m;->start()V

    :cond_0
    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/ipc/invalidation/ticl/android2/U;->a(Landroid/content/Context;Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;Lcom/google/ipc/invalidation/ticl/android2/m;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/android2/m;)V
    .locals 3

    invoke-interface {p0}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/android2/m;->k()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/ipc/invalidation/ticl/android2/AndroidInternalScheduler;->a(J)V

    return-void
.end method
