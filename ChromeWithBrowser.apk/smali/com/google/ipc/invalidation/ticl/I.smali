.class final Lcom/google/ipc/invalidation/ticl/I;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

.field private final b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

.field private final c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

.field private final d:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

.field private final e:Lcom/google/ipc/invalidation/ticl/M;

.field private final f:Lcom/google/ipc/invalidation/a/R;

.field private final g:Lcom/google/ipc/invalidation/ticl/K;

.field private h:I

.field private i:J

.field private j:J

.field private final k:Lcom/google/ipc/invalidation/ticl/ac;

.field private final l:I


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/ac;ILjava/lang/String;Lcom/google/ipc/invalidation/ticl/M;Lcom/google/ipc/invalidation/a/R;Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;)V
    .locals 5

    const-wide/16 v0, 0x0

    const/4 v4, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v4, p0, Lcom/google/ipc/invalidation/ticl/I;->h:I

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/I;->i:J

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/I;->j:J

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getLogger()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    iput-object p2, p0, Lcom/google/ipc/invalidation/ticl/I;->k:Lcom/google/ipc/invalidation/ticl/ac;

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getInternalScheduler()Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getNetwork()Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->d:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

    iput-object p5, p0, Lcom/google/ipc/invalidation/ticl/I;->e:Lcom/google/ipc/invalidation/ticl/M;

    iput-object p6, p0, Lcom/google/ipc/invalidation/ticl/I;->f:Lcom/google/ipc/invalidation/a/R;

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getPlatform()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Java"

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;

    move-result-object v2

    sget-object v3, Lcom/google/ipc/invalidation/a/F;->b:Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    invoke-virtual {v2, v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;->b(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;->c(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    iput p3, p0, Lcom/google/ipc/invalidation/ticl/I;->l:I

    if-nez p7, :cond_0

    new-instance v0, Lcom/google/ipc/invalidation/ticl/K;

    invoke-direct {v0, p1, p2}, Lcom/google/ipc/invalidation/ticl/K;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/ac;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->g:Lcom/google/ipc/invalidation/ticl/K;

    :goto_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Created protocol handler for application %s, platform %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p4, v2, v3

    invoke-interface {p1}, Lcom/google/ipc/invalidation/external/client/SystemResources;->getPlatform()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/ticl/K;

    invoke-virtual {p7}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->l()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, Lcom/google/ipc/invalidation/ticl/K;-><init>(Lcom/google/ipc/invalidation/external/client/SystemResources;Lcom/google/ipc/invalidation/ticl/ac;Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->g:Lcom/google/ipc/invalidation/ticl/K;

    invoke-virtual {p7}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->f()I

    move-result v0

    iput v0, p0, Lcom/google/ipc/invalidation/ticl/I;->h:I

    invoke-virtual {p7}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/I;->i:J

    invoke-virtual {p7}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/ipc/invalidation/ticl/I;->j:J

    goto :goto_0
.end method


# virtual methods
.method final a([B)Lcom/google/ipc/invalidation/ticl/L;
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v1

    const-string v2, "Not on internal thread"

    invoke-static {v1, v2}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    :try_start_0
    invoke-static {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->a([B)Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Incoming message: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/I;->f:Lcom/google/ipc/invalidation/a/R;

    invoke-virtual {v2, v1}, Lcom/google/ipc/invalidation/a/R;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/I;->k:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v3, Lcom/google/ipc/invalidation/ticl/ad;->b:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Received invalid message: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Incoming message is unparseable: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/ipc/invalidation/a/G;->a([B)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;->f()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/I;->k:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v3, Lcom/google/ipc/invalidation/ticl/ad;->g:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Dropping message with incompatible version: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->o()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->p()Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;

    move-result-object v1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/I;->k:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v3, Lcom/google/ipc/invalidation/ticl/ag;->g:Lcom/google/ipc/invalidation/ticl/ag;

    invoke-virtual {v2, v3}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ag;)V

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ConfigChangeMessage;->f()J

    move-result-wide v4

    add-long v1, v2, v4

    iput-wide v1, p0, Lcom/google/ipc/invalidation/ticl/I;->j:J

    goto :goto_0

    :cond_3
    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/I;->i:J

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerHeader;->l()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/ipc/invalidation/ticl/I;->i:J

    new-instance v0, Lcom/google/ipc/invalidation/ticl/L;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/ticl/L;-><init>(Lcom/google/protos/ipc/invalidation/ClientProtocol$ServerToClientMessage;)V

    goto :goto_0
.end method

.method final a()V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v3, "Not on internal thread"

    invoke-static {v0, v3}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-wide v3, p0, Lcom/google/ipc/invalidation/ticl/I;->j:J

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "In quiet period: not sending message to server: %s > %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-wide v5, p0, Lcom/google/ipc/invalidation/ticl/I;->j:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-interface {v0, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/I;->g:Lcom/google/ipc/invalidation/ticl/K;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->e:Lcom/google/ipc/invalidation/ticl/M;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/ticl/M;->f()Lcom/google/protobuf/ByteString;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/ipc/invalidation/ticl/K;->a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v3

    const-string v4, "Not on internal thread"

    invoke-static {v3, v4}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v3

    sget-object v4, Lcom/google/ipc/invalidation/a/F;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;

    invoke-virtual {v3, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->a(J)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v3

    iget v4, p0, Lcom/google/ipc/invalidation/ticl/I;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->a(Ljava/lang/String;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/ipc/invalidation/ticl/I;->i:J

    invoke-virtual {v3, v4, v5}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->b(J)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/I;->e:Lcom/google/ipc/invalidation/ticl/M;

    invoke-interface {v4}, Lcom/google/ipc/invalidation/ticl/M;->h()Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSummary;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v3

    iget v4, p0, Lcom/google/ipc/invalidation/ticl/I;->l:I

    invoke-virtual {v3, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/I;->e:Lcom/google/ipc/invalidation/ticl/M;

    invoke-interface {v4}, Lcom/google/ipc/invalidation/ticl/M;->f()Lcom/google/protobuf/ByteString;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v5, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v6, "Sending token on client->server message: %s"

    new-array v7, v1, [Ljava/lang/Object;

    invoke-static {v4}, Lcom/google/ipc/invalidation/a/G;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-interface {v5, v6, v7}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;

    :cond_2
    invoke-virtual {v0, v3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientHeader$Builder;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;

    iget v3, p0, Lcom/google/ipc/invalidation/ticl/I;->h:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/ipc/invalidation/ticl/I;->h:I

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;

    move-result-object v0

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/I;->f:Lcom/google/ipc/invalidation/a/R;

    invoke-virtual {v3, v0}, Lcom/google/ipc/invalidation/a/R;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Tried to send invalid message: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-interface {v3, v4, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->k:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v1, Lcom/google/ipc/invalidation/ticl/ad;->c:Lcom/google/ipc/invalidation/ticl/ad;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ad;)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/I;->k:Lcom/google/ipc/invalidation/ticl/ac;

    sget-object v2, Lcom/google/ipc/invalidation/ticl/ah;->f:Lcom/google/ipc/invalidation/ticl/ah;

    invoke-virtual {v1, v2}, Lcom/google/ipc/invalidation/ticl/ac;->a(Lcom/google/ipc/invalidation/ticl/ah;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/I;->d:Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientToServerMessage;->b()[B

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$NetworkChannel;->sendMessage([B)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->e:Lcom/google/ipc/invalidation/ticl/M;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/ticl/M;->g()V

    goto/16 :goto_0
.end method

.method final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/ticl/q;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;->f()I

    move-result v0

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/I;->l:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Client type in application id does not match constructor-provided type: %s vs %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v4

    iget v3, p0, Lcom/google/ipc/invalidation/ticl/I;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Lcom/google/ipc/invalidation/ticl/I;->l:I

    sget-object v1, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a(I)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ApplicationClientIdP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$DigestSerializationType;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->b(Lcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/I;->g:Lcom/google/ipc/invalidation/ticl/K;

    invoke-virtual {v1, v0}, Lcom/google/ipc/invalidation/ticl/K;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InitializeMessage;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Batching initialize message for client: %s, %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p4, v3, v4

    aput-object v0, v3, v5

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p3, p4}, Lcom/google/ipc/invalidation/ticl/q;->a(Ljava/lang/String;)V

    return-void
.end method

.method final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;Lcom/google/ipc/invalidation/ticl/q;)V
    .locals 4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Sending ack for invalidation %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->g:Lcom/google/ipc/invalidation/ticl/K;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/ticl/K;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;)V

    const-string v0, "Send-Ack"

    invoke-virtual {p2, v0}, Lcom/google/ipc/invalidation/ticl/q;->a(Ljava/lang/String;)V

    return-void
.end method

.method final a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;Lcom/google/ipc/invalidation/ticl/q;)V
    .locals 4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->g:Lcom/google/ipc/invalidation/ticl/K;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/ticl/K;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationSubtree;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->b:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Adding subtree: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "Send-reg-sync"

    invoke-virtual {p2, v0}, Lcom/google/ipc/invalidation/ticl/q;->a(Ljava/lang/String;)V

    return-void
.end method

.method final a(Ljava/util/Collection;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;Lcom/google/ipc/invalidation/ticl/q;)V
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/I;->g:Lcom/google/ipc/invalidation/ticl/K;

    invoke-virtual {v2, v0, p2}, Lcom/google/ipc/invalidation/ticl/K;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;Lcom/google/protos/ipc/invalidation/ClientProtocol$RegistrationP$OpType;)V

    goto :goto_0

    :cond_0
    const-string v0, "Send-registrations"

    invoke-virtual {p3, v0}, Lcom/google/ipc/invalidation/ticl/q;->a(Ljava/lang/String;)V

    return-void
.end method

.method final a(Ljava/util/List;Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;ZLcom/google/ipc/invalidation/ticl/q;)V
    .locals 4

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->c:Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    invoke-interface {v0}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->isRunningOnThread()Z

    move-result v0

    const-string v1, "Not on internal thread"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    invoke-static {}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;->newBuilder()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/I;->a:Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientVersion;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    move-result-object v2

    if-eqz p2, :cond_0

    invoke-virtual {v2, p2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/ipc/invalidation/external/client/types/SimplePair;

    iget-object v0, v1, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Lcom/google/ipc/invalidation/external/client/types/SimplePair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/b/c;->a(Ljava/lang/String;I)Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$PropertyRecord;)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    goto :goto_0

    :cond_1
    invoke-virtual {v2, p3}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->a(Z)Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;

    iget-object v0, p0, Lcom/google/ipc/invalidation/ticl/I;->g:Lcom/google/ipc/invalidation/ticl/K;

    invoke-virtual {v2}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage$Builder;->c()Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/ticl/K;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$InfoMessage;)V

    const-string v0, "Send-info"

    invoke-virtual {p4, v0}, Lcom/google/ipc/invalidation/ticl/q;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final b()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;
    .locals 3

    invoke-static {}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;->newBuilder()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/ipc/invalidation/ticl/I;->i:J

    invoke-virtual {v0, v1, v2}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a(J)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    iget v1, p0, Lcom/google/ipc/invalidation/ticl/I;->h:I

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a(I)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    iget-wide v1, p0, Lcom/google/ipc/invalidation/ticl/I;->j:J

    invoke-virtual {v0, v1, v2}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->b(J)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    iget-object v1, p0, Lcom/google/ipc/invalidation/ticl/I;->g:Lcom/google/ipc/invalidation/ticl/K;

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/K;->a()Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->a(Lcom/google/protos/ipc/invalidation/JavaClient$BatcherState;)Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState$Builder;->c()Lcom/google/protos/ipc/invalidation/JavaClient$ProtocolHandlerState;

    move-result-object v0

    return-object v0
.end method
