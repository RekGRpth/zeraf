.class final Lcom/google/ipc/invalidation/ticl/r;
.super Lcom/google/ipc/invalidation/ticl/w;


# instance fields
.field private a:J

.field private synthetic b:Lcom/google/ipc/invalidation/ticl/l;


# direct methods
.method constructor <init>(Lcom/google/ipc/invalidation/ticl/l;)V
    .locals 6

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/r;->b:Lcom/google/ipc/invalidation/ticl/l;

    const-string v2, "Heartbeat"

    invoke-static {p1}, Lcom/google/ipc/invalidation/ticl/l;->d(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/ipc/invalidation/ticl/w;-><init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;IIZ)V

    return-void
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/ticl/l;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;)V
    .locals 2

    iput-object p1, p0, Lcom/google/ipc/invalidation/ticl/r;->b:Lcom/google/ipc/invalidation/ticl/l;

    const-string v0, "Heartbeat"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/google/ipc/invalidation/ticl/w;-><init>(Lcom/google/ipc/invalidation/ticl/l;Ljava/lang/String;Lcom/google/protos/ipc/invalidation/JavaClient$RecurringTaskState;B)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ipc/invalidation/ticl/r;->b:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v2}, Lcom/google/ipc/invalidation/ticl/l;->b(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    move-result-object v2

    const-string v3, "Sending heartbeat to server: %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p0, v4, v0

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-wide v2, p0, Lcom/google/ipc/invalidation/ticl/r;->a:J

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/r;->b:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v4}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/r;->b:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v3}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Scheduler;->getCurrentTimeMs()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/ticl/r;->b()Lcom/google/ipc/invalidation/b/h;

    move-result-object v5

    iget-object v6, p0, Lcom/google/ipc/invalidation/ticl/r;->b:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v6}, Lcom/google/ipc/invalidation/ticl/l;->d(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->n()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/ipc/invalidation/b/h;->a(I)I

    move-result v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/ipc/invalidation/ticl/r;->a:J

    :cond_0
    iget-object v3, p0, Lcom/google/ipc/invalidation/ticl/r;->b:Lcom/google/ipc/invalidation/ticl/l;

    iget-object v4, p0, Lcom/google/ipc/invalidation/ticl/r;->b:Lcom/google/ipc/invalidation/ticl/l;

    invoke-static {v4}, Lcom/google/ipc/invalidation/ticl/l;->j(Lcom/google/ipc/invalidation/ticl/l;)Lcom/google/ipc/invalidation/ticl/Q;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/ipc/invalidation/ticl/Q;->c()Z

    move-result v4

    if-nez v4, :cond_1

    move v0, v1

    :cond_1
    invoke-static {v3, v2, v0}, Lcom/google/ipc/invalidation/ticl/l;->a(Lcom/google/ipc/invalidation/ticl/l;ZZ)V

    return v1

    :cond_2
    move v2, v0

    goto :goto_0
.end method
