.class Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId$1;

    invoke-direct {v0}, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId$1;-><init>()V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->newInstance(I[B)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    check-cast p1, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;

    iget-object v1, p1, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->getSource()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->getName()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method
