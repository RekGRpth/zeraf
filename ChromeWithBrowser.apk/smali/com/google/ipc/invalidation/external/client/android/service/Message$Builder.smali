.class public abstract Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;
.super Ljava/lang/Object;


# instance fields
.field protected final bundle:Landroid/os/Bundle;

.field private thisInstance:Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;


# direct methods
.method protected constructor <init>(ILandroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->bundle:Landroid/os/Bundle;

    iput-object p0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->thisInstance:Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    const-string v0, "action"

    invoke-virtual {p2, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/google/ipc/invalidation/external/client/android/service/Message;
.end method

.method public setAccount(Landroid/accounts/Account;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->thisInstance:Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    return-object v0
.end method

.method public setAckHandle(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "ackToken"

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->getHandleData()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->thisInstance:Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    return-object v0
.end method

.method public setAuthType(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "authType"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->thisInstance:Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    return-object v0
.end method

.method public setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "client"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->thisInstance:Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    return-object v0
.end method

.method public setError(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "error"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->thisInstance:Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    return-object v0
.end method

.method public setObjectId(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->bundle:Landroid/os/Bundle;

    const-string v1, "objectId"

    new-instance v2, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;

    invoke-direct {v2, p1}, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;-><init>(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;->thisInstance:Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    return-object v0
.end method
