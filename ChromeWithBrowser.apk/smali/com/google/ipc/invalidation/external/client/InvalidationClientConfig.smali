.class public Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;
.super Ljava/lang/Object;


# instance fields
.field public final allowSuppression:Z

.field public final applicationName:Ljava/lang/String;

.field public final clientName:[B

.field public final clientType:I


# direct methods
.method public constructor <init>(I[BLjava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;->clientType:I

    iput-object p2, p0, Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;->clientName:[B

    iput-object p3, p0, Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;->applicationName:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/ipc/invalidation/external/client/InvalidationClientConfig;->allowSuppression:Z

    return-void
.end method
