.class public abstract Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;
.super Ljava/lang/Object;


# static fields
.field private static final logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;


# instance fields
.field private final componentClassName:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private hasCalledBind:Z

.field private final lock:Ljava/lang/Object;

.field private numStartBindForTest:I

.field private final pendingWork:Ljava/util/Queue;

.field private queueHandlingInProgress:Z

.field private final serviceClass:Ljava/lang/Class;

.field private final serviceConnection:Landroid/content/ServiceConnection;

.field private serviceInstance:Ljava/lang/Object;

.field private final serviceIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "InvServiceBinder"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->pendingWork:Ljava/util/Queue;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->lock:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceInstance:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->hasCalledBind:Z

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->queueHandlingInProgress:Z

    iput v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->numStartBindForTest:I

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$1;-><init>(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->context:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceIntent:Landroid/content/Intent;

    invoke-static {p3}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceClass:Ljava/lang/Class;

    iput-object p4, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->componentClassName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceInstance:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceInstance:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->handleQueue()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/lang/Class;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceClass:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Landroid/content/ServiceConnection;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->hasCalledBind:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)Ljava/util/Queue;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->pendingWork:Ljava/util/Queue;

    return-object v0
.end method

.method private handleQueue()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    :goto_0
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->queueHandlingInProgress:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->pendingWork:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->hasCalledBind:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceInstance:Ljava/lang/Object;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "Bind not called but service instance is set: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceClass:Ljava/lang/Class;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->startBind()Z

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceInstance:Ljava/lang/Object;

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->hasCalledBind:Z

    const-string v3, "No service instance and not waiting for bind: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceClass:Ljava/lang/Class;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->pendingWork:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$BoundWork;

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->queueHandlingInProgress:Z

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceInstance:Ljava/lang/Object;

    invoke-interface {v0, v3}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$BoundWork;->run(Ljava/lang/Object;)V

    iput-boolean v2, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->queueHandlingInProgress:Z

    goto :goto_0
.end method

.method private startBind()Z
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->hasCalledBind:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Bind already called for %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceClass:Ljava/lang/Class;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/a/a/a;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->numStartBindForTest:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->numStartBindForTest:I

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v3, v0, v4, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v4, "Unable to bind to service: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-interface {v3, v4, v1}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->severe(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return v2

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->hasCalledBind:Z

    move v2, v1

    goto :goto_1
.end method


# virtual methods
.method protected abstract asInterface(Landroid/os/IBinder;)Ljava/lang/Object;
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->componentClassName:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceIntent:Landroid/content/Intent;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceIntent:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->componentClassName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public getNumStartBindForTest()I
    .locals 1

    iget v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->numStartBindForTest:I

    return v0
.end method

.method public isBoundForTest()Z
    .locals 2

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->hasCalledBind:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public release()V
    .locals 6

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->hasCalledBind:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Release is a no-op since not bound: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceClass:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->fine(Ljava/lang/String;[Ljava/lang/Object;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$2;-><init>(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;)V

    invoke-virtual {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->runWhenBound(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$BoundWork;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public runWhenBound(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$BoundWork;)V
    .locals 2

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->pendingWork:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->handleQueue()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder;->serviceIntent:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
