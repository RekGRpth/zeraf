.class public final Lcom/google/ipc/invalidation/external/client/types/ObjectId;
.super Ljava/lang/Object;


# instance fields
.field private final name:[B

.field private final source:I


# direct methods
.method private constructor <init>(I[B)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "source"

    invoke-static {v0, v1}, Lcom/google/a/a/a;->b(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->source:I

    const-string v0, "name"

    invoke-static {p2, v0}, Lcom/google/a/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->name:[B

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(I[B)Lcom/google/ipc/invalidation/external/client/types/ObjectId;
    .locals 1

    new-instance v0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    invoke-direct {v0, p0, p1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;-><init>(I[B)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    iget v2, p0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->source:I

    iget v3, p1, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->source:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->name:[B

    iget-object v3, p1, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->name:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final getName()[B
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->name:[B

    return-object v0
.end method

.method public final getSource()I
    .locals 1

    iget v0, p0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->source:I

    return v0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->source:I

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->name:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Oid: <"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->source:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->name:[B

    invoke-static {v1}, Lcom/google/ipc/invalidation/external/client/types/BytesFormatter;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
