.class final Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClient;


# static fields
.field private static final logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;


# instance fields
.field private account:Landroid/accounts/Account;

.field private authType:Ljava/lang/String;

.field private final clientKey:Ljava/lang/String;

.field private final clientType:I

.field public final context:Landroid/content/Context;

.field private final listenerClass:Ljava/lang/Class;

.field private refcnt:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final serviceBinder:Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;

.field wasOverReleasedForTest:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "InvClient"

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forTag(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->refcnt:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->wasOverReleasedForTest:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p2, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->context:Landroid/content/Context;

    iput-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->account:Landroid/accounts/Account;

    iput-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->authType:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->listenerClass:Ljava/lang/Class;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientType:I

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;

    invoke-direct {v0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->serviceBinder:Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;ILandroid/accounts/Account;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->refcnt:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->wasOverReleasedForTest:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    iput p3, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientType:I

    iput-object p4, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->account:Landroid/accounts/Account;

    iput-object p5, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->authType:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->listenerClass:Ljava/lang/Class;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;

    invoke-direct {v0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->serviceBinder:Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;

    return-void
.end method

.method static synthetic access$000()Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-object v0
.end method

.method private executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->serviceBinder:Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;

    new-instance v1, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl$1;

    invoke-direct {v1, p0, p1}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl$1;-><init>(Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;->runWhenBound(Lcom/google/ipc/invalidation/external/client/android/service/ServiceBinder$BoundWork;)V

    return-void
.end method


# virtual methods
.method public final acknowledge(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->ACKNOWLEDGE:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setAckHandle(Lcom/google/ipc/invalidation/external/client/types/AckHandle;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    return-void
.end method

.method final addReference()V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->refcnt:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    return-void
.end method

.method public final destroy()V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->DESTROY:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    return-void
.end method

.method public final getClientKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    return-object v0
.end method

.method final getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->context:Landroid/content/Context;

    return-object v0
.end method

.method final getListenerClass()Ljava/lang/Class;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->listenerClass:Ljava/lang/Class;

    return-object v0
.end method

.method final getReferenceCountForTest()I
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->refcnt:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method final hasServiceBindingForTest()Z
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->serviceBinder:Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;->isBoundForTest()Z

    move-result v0

    return v0
.end method

.method final initResumed(Z)V
    .locals 2

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->RESUME:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->addReference()V

    return-void
.end method

.method final initialize()V
    .locals 4

    new-instance v1, Landroid/content/Intent;

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event;->LISTENER_INTENT:Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    new-instance v0, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->listenerClass:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->CREATE:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    iget v2, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientType:I

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientType(I)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->account:Landroid/accounts/Account;

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setAccount(Landroid/accounts/Account;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->authType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setAuthType(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setIntent(Landroid/content/Intent;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->addReference()V

    return-void
.end method

.method public final register(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->REGISTER:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setObjectId(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    return-void
.end method

.method public final register(Ljava/util/Collection;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->REGISTER:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setObjectIds(Ljava/util/Collection;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    return-void
.end method

.method public final release()V
    .locals 5

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->refcnt:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->wasOverReleasedForTest:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v1, "Over-release of client %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidClientFactory;->release(Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->serviceBinder:Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/InvalidationBinder;->release()V

    goto :goto_0
.end method

.method public final start()V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->START:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    return-void
.end method

.method public final stop()V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->STOP:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    return-void
.end method

.method public final unregister(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->UNREGISTER:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setObjectId(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    return-void
.end method

.method public final unregister(Ljava/util/Collection;)V
    .locals 2

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->UNREGISTER:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request;->newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->clientKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setClientKey(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Message$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    invoke-virtual {v0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->setObjectIds(Ljava/util/Collection;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Builder;->build()Lcom/google/ipc/invalidation/external/client/android/service/Request;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/android/AndroidInvalidationClientImpl;->executeServiceRequest(Lcom/google/ipc/invalidation/external/client/android/service/Request;)V

    return-void
.end method
