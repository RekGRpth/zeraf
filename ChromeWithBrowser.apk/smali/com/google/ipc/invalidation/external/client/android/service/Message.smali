.class public abstract Lcom/google/ipc/invalidation/external/client/android/service/Message;
.super Ljava/lang/Object;


# instance fields
.field protected final parameters:Landroid/os/Bundle;


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message;->parameters:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public getAccount()Landroid/accounts/Account;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message;->parameters:Landroid/os/Bundle;

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    return-object v0
.end method

.method public getAckHandle()Lcom/google/ipc/invalidation/external/client/types/AckHandle;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message;->parameters:Landroid/os/Bundle;

    const-string v1, "ackToken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/types/AckHandle;->newInstance([B)Lcom/google/ipc/invalidation/external/client/types/AckHandle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getActionOrdinal()I
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message;->parameters:Landroid/os/Bundle;

    const-string v1, "action"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getAuthType()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message;->parameters:Landroid/os/Bundle;

    const-string v1, "authType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message;->parameters:Landroid/os/Bundle;

    return-object v0
.end method

.method public getClientKey()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message;->parameters:Landroid/os/Bundle;

    const-string v1, "client"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getError()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message;->parameters:Landroid/os/Bundle;

    const-string v1, "error"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Message;->parameters:Landroid/os/Bundle;

    const-string v1, "objectId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableObjectId;->objectId:Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/service/Message;->getActionOrdinal()I

    move-result v0

    invoke-static {}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->values()[Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    invoke-static {}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->values()[Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/service/Message;->getActionOrdinal()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Message ACTION = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " CLIENT = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/service/Message;->getClientKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "invalid"

    goto :goto_0
.end method
