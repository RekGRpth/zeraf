.class public Lcom/google/ipc/invalidation/external/client/android/service/Event;
.super Lcom/google/ipc/invalidation/external/client/android/service/Message;


# static fields
.field public static final LISTENER_INTENT:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.ipc.invalidation.EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Event;->LISTENER_INTENT:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/android/service/Message;-><init>(Landroid/os/Bundle;)V

    return-void
.end method

.method public static newBuilder(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;)Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;
    .locals 2

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Builder;-><init>(Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;Lcom/google/ipc/invalidation/external/client/android/service/Event$1;)V

    return-object v0
.end method


# virtual methods
.method public getAction()Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;
    .locals 2

    invoke-static {}, Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;->values()[Lcom/google/ipc/invalidation/external/client/android/service/Event$Action;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/android/service/Event;->getActionOrdinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getErrorInfo()Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event;->parameters:Landroid/os/Bundle;

    const-string v1, "error"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableErrorInfo;->errorInfo:Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInvalidation()Lcom/google/ipc/invalidation/external/client/types/Invalidation;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event;->parameters:Landroid/os/Bundle;

    const-string v1, "invalidation"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/ipc/invalidation/external/client/android/service/ParcelableInvalidation;->invalidation:Lcom/google/ipc/invalidation/external/client/types/Invalidation;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsTransient()Z
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event;->parameters:Landroid/os/Bundle;

    const-string v1, "isTransient"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPrefix()[B
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event;->parameters:Landroid/os/Bundle;

    const-string v1, "prefix"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getPrefixLength()I
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event;->parameters:Landroid/os/Bundle;

    const-string v1, "prefixLength"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getRegistrationState()Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;
    .locals 3

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/android/service/Event;->parameters:Landroid/os/Bundle;

    const-string v1, "registrationState"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->values()[Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    move-result-object v1

    aget-object v0, v1, v0

    goto :goto_0
.end method
