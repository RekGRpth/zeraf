.class Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;
.super Ljava/lang/Object;


# static fields
.field static final EXTRA_ACK:Ljava/lang/String; = "com.google.ipc.invalidation.android_listener.ACK"

.field static final EXTRA_REGISTRATION:Ljava/lang/String; = "com.google.ipc.invalidation.android_listener.REGISTRATION"

.field static final EXTRA_START:Ljava/lang/String; = "com.google.ipc.invalidation.android_listener.START"

.field static final EXTRA_STOP:Ljava/lang/String; = "com.google.ipc.invalidation.android_listener.STOP"

.field private static final logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->forPrefix(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;

    move-result-object v0

    sput-object v0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static createAckIntent(Landroid/content/Context;[B)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.ipc.invalidation.android_listener.ACK"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static createRegistrationIntent(Landroid/content/Context;[BLjava/lang/Iterable;Z)Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    if-eqz p3, :cond_0

    invoke-static {p1}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newRegisterCommand(Lcom/google/protobuf/ByteString;Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    :goto_0
    const-string v2, "com.google.ipc.invalidation.android_listener.REGISTRATION"

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-static {p0, v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newUnregisterCommand(Lcom/google/protobuf/ByteString;Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    goto :goto_0
.end method

.method static createStartIntent(Landroid/content/Context;I[B)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {p2}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newStartCommand(ILcom/google/protobuf/ByteString;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;

    move-result-object v1

    const-string v2, "com.google.ipc.invalidation.android_listener.START"

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;->b()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static createStopIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.ipc.invalidation.android_listener.STOP"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static findAckHandle(Landroid/content/Intent;)[B
    .locals 1

    const-string v0, "com.google.ipc.invalidation.android_listener.ACK"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method static findRegistrationCommand(Landroid/content/Intent;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    .locals 6

    const/4 v0, 0x0

    const-string v1, "com.google.ipc.invalidation.android_listener.REGISTRATION"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->a([B)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Received invalid proto: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static findStartCommand(Landroid/content/Intent;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;
    .locals 6

    const/4 v0, 0x0

    const-string v1, "com.google.ipc.invalidation.android_listener.START"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;->a([B)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$StartCommand;
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v3, "Received invalid proto: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static isAuthTokenRequest(Landroid/content/Intent;)Z
    .locals 2

    const-string v0, "com.google.ipc.invalidation.AUTH_TOKEN_REQUEST"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static isStopIntent(Landroid/content/Intent;)Z
    .locals 1

    const-string v0, "com.google.ipc.invalidation.android_listener.STOP"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static issueAndroidListenerIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    invoke-static {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method static issueAuthTokenResponse(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.ipc.invalidation.AUTH_TOKEN"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.ipc.invalidation.AUTH_TOKEN_TYPE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, p0, v1, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerIntents;->logger:Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;

    const-string v2, "Canceled auth request: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/SystemResources$Logger;->warning(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static issueDelayedRegistrationIntent(Landroid/content/Context;Lcom/google/ipc/invalidation/ticl/android2/a;Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/external/client/types/ObjectId;ZII)V
    .locals 6

    if-eqz p4, :cond_0

    invoke-static {p2, p3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newDelayedRegisterCommand(Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.ipc.invalidation.android_listener.REGISTRATION"

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;->b()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener$AlarmReceiver;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x40000000

    invoke-static {p0, p6, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/ticl/android2/a;->a()J

    move-result-wide v2

    int-to-long v4, p5

    add-long/2addr v2, v4

    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void

    :cond_0
    invoke-static {p2, p3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newDelayedUnregisterCommand(Lcom/google/protobuf/ByteString;Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$RegistrationCommand;

    move-result-object v0

    goto :goto_0
.end method

.method static issueTiclIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/O;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/android2/O;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/O;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method static setAndroidListenerClass(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 1

    new-instance v0, Lcom/google/ipc/invalidation/ticl/android2/O;

    invoke-direct {v0, p0}, Lcom/google/ipc/invalidation/ticl/android2/O;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/android2/O;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
