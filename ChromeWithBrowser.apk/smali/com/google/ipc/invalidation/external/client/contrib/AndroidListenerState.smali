.class final Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;
.super Ljava/lang/Object;


# instance fields
.field private final clientId:Lcom/google/protobuf/ByteString;

.field private final delayGenerators:Ljava/util/Map;

.field private final desiredRegistrations:Ljava/util/Set;

.field private final initialMaxDelayMs:I

.field private isDirty:Z

.field private final maxDelayFactor:I

.field private final random:Ljava/util/Random;

.field private requestCodeSeqNum:I


# direct methods
.method constructor <init>(II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->random:Ljava/util/Random;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-static {}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->createGloballyUniqueClientId()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/protobuf/ByteString;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    iput p1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->initialMaxDelayMs:I

    iput p2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->maxDelayFactor:I

    return-void
.end method

.method constructor <init>(IILcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->random:Ljava/util/Random;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/b/c;->a(Lcom/google/protos/ipc/invalidation/ClientProtocol$ObjectIdP;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v2

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    new-instance v4, Lcom/google/ipc/invalidation/ticl/ak;

    iget-object v5, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->random:Ljava/util/Random;

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState$RetryRegistrationState;->h()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    invoke-direct {v4, v5, p1, p2, v0}, Lcom/google/ipc/invalidation/ticl/ak;-><init>(Ljava/util/Random;IILcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;)V

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->h()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/protobuf/ByteString;

    invoke-virtual {p3}, Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;->j()I

    move-result v0

    iput v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    iput p1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->initialMaxDelayMs:I

    iput p2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->maxDelayFactor:I

    return-void
.end method

.method private static createGloballyUniqueClientId()Lcom/google/protobuf/ByteString;
    .locals 5

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    const/16 v1, 0x10

    new-array v1, v1, [B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->a([B)Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method private static equals(Ljava/util/Map;Ljava/util/Map;)Z
    .locals 4

    const/4 v2, 0x0

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ipc/invalidation/ticl/ak;

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/ak;

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/ak;->a()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->a()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/ipc/invalidation/ticl/ak;->a()Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protos/ipc/invalidation/Client$ExponentialBackoffState;->a()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private resetDelayGeneratorFor(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    :cond_0
    return-void
.end method


# virtual methods
.method final addDesiredRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final containsDesiredRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;

    iget-boolean v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    iget-boolean v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    iget v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    iget-object v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    iget-object v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/protobuf/ByteString;

    iget-object v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/protobuf/ByteString;

    invoke-virtual {v2, v3}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    invoke-static {v2, v3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->equals(Ljava/util/Map;Ljava/util/Map;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method final getClientId()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method final getIsDirty()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    return v0
.end method

.method final getNextDelay(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)I
    .locals 4

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/ticl/ak;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/ipc/invalidation/ticl/ak;

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->random:Ljava/util/Random;

    iget v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->initialMaxDelayMs:I

    iget v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->maxDelayFactor:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ipc/invalidation/ticl/ak;-><init>(Ljava/util/Random;II)V

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    invoke-virtual {v0}, Lcom/google/ipc/invalidation/ticl/ak;->c()I

    move-result v0

    return v0
.end method

.method final getNextRequestCode()I
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    iget v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    return v0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public final informRegistrationFailure(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->removeDesiredRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Z

    if-nez p2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->resetDelayGeneratorFor(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    :cond_0
    return-void
.end method

.method final informRegistrationSuccess(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->resetDelayGeneratorFor(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)V

    return-void
.end method

.method public final marshal()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;
    .locals 4

    iget-object v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/protobuf/ByteString;

    iget v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    iget-object v2, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-static {v0, v1, v2, v3}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerProtos;->newAndroidListenerState(Lcom/google/protobuf/ByteString;ILjava/util/Map;Ljava/lang/Iterable;)Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic marshal()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->marshal()Lcom/google/protos/ipc/invalidation/AndroidListenerProtocol$AndroidListenerState;

    move-result-object v0

    return-object v0
.end method

.method final removeDesiredRegistration(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final resetIsDirty()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "AndroidListenerState[%s]: isDirty = %b, desiredRegistrations.size() = %d, delayGenerators.size() = %d, requestCodeSeqNum = %d"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->clientId:Lcom/google/protobuf/ByteString;

    invoke-static {v3}, Lcom/google/ipc/invalidation/b/c;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->isDirty:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->desiredRegistrations:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->delayGenerators:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListenerState;->requestCodeSeqNum:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
