.class public final enum Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

.field public static final enum ACKNOWLEDGE:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

.field public static final enum CREATE:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

.field public static final enum DESTROY:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

.field public static final enum REGISTER:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

.field public static final enum RESUME:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

.field public static final enum START:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

.field public static final enum STOP:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

.field public static final enum UNREGISTER:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    const-string v1, "CREATE"

    invoke-direct {v0, v1, v3}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->CREATE:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    const-string v1, "RESUME"

    invoke-direct {v0, v1, v4}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->RESUME:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    const-string v1, "START"

    invoke-direct {v0, v1, v5}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->START:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    const-string v1, "STOP"

    invoke-direct {v0, v1, v6}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->STOP:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    const-string v1, "REGISTER"

    invoke-direct {v0, v1, v7}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->REGISTER:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    const-string v1, "UNREGISTER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->UNREGISTER:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    const-string v1, "ACKNOWLEDGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->ACKNOWLEDGE:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    new-instance v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    const-string v1, "DESTROY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->DESTROY:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->CREATE:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->RESUME:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->START:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->STOP:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->REGISTER:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->UNREGISTER:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->ACKNOWLEDGE:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->DESTROY:Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->$VALUES:[Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;
    .locals 1

    const-class v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    return-object v0
.end method

.method public static values()[Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->$VALUES:[Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    invoke-virtual {v0}, [Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ipc/invalidation/external/client/android/service/Request$Action;

    return-object v0
.end method
