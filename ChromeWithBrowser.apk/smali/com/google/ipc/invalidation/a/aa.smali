.class final Lcom/google/ipc/invalidation/a/aa;
.super Lcom/google/ipc/invalidation/a/Q;


# instance fields
.field private synthetic a:Lcom/google/ipc/invalidation/a/X;


# direct methods
.method varargs constructor <init>(Lcom/google/ipc/invalidation/a/X;Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V
    .locals 0

    iput-object p1, p0, Lcom/google/ipc/invalidation/a/aa;->a:Lcom/google/ipc/invalidation/a/X;

    invoke-direct {p0, p2, p3}, Lcom/google/ipc/invalidation/a/Q;-><init>(Lcom/google/ipc/invalidation/a/M;[Lcom/google/ipc/invalidation/a/O;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/MessageLite;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p1, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->j()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/aa;->a:Lcom/google/ipc/invalidation/a/X;

    iget-object v0, v0, Lcom/google/ipc/invalidation/a/X;->h:Lcom/google/ipc/invalidation/a/R;

    iget-object v0, v0, Lcom/google/ipc/invalidation/a/R;->a:Lcom/google/ipc/invalidation/b/a;

    const-string v3, "Version was negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-interface {v0, v3, v1}, Lcom/google/ipc/invalidation/b/a;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->h()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->k()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$InvalidationP;->l()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_1
    move v3, v1

    :goto_2
    if-eqz v0, :cond_4

    if-nez v3, :cond_4

    iget-object v0, p0, Lcom/google/ipc/invalidation/a/aa;->a:Lcom/google/ipc/invalidation/a/X;

    iget-object v0, v0, Lcom/google/ipc/invalidation/a/X;->h:Lcom/google/ipc/invalidation/a/R;

    iget-object v0, v0, Lcom/google/ipc/invalidation/a/R;->a:Lcom/google/ipc/invalidation/b/a;

    const-string v3, "if is_known_version is false, is_trickle_restart must be true or missing: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-interface {v0, v3, v1}, Lcom/google/ipc/invalidation/b/a;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v3, v2

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_0
.end method
