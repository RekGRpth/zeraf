.class public final Lcom/google/ipc/invalidation/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ipc/invalidation/a/M;


# static fields
.field public static final a:Lcom/google/ipc/invalidation/a/N;

.field public static final b:Lcom/google/ipc/invalidation/a/N;

.field public static final c:Lcom/google/ipc/invalidation/a/N;

.field public static final d:Lcom/google/ipc/invalidation/a/N;

.field public static final e:Lcom/google/ipc/invalidation/a/N;

.field public static final f:Lcom/google/ipc/invalidation/a/N;

.field public static final g:Lcom/google/ipc/invalidation/a/N;

.field public static final h:Lcom/google/ipc/invalidation/a/N;

.field public static final i:Lcom/google/ipc/invalidation/a/N;

.field public static final j:Lcom/google/ipc/invalidation/a/N;

.field public static final k:Lcom/google/ipc/invalidation/a/N;

.field public static final l:Lcom/google/ipc/invalidation/a/N;

.field public static final m:Lcom/google/ipc/invalidation/a/N;

.field private static final n:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "version"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "network_timeout_delay_ms"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "write_retry_delay_ms"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "heartbeat_interval_ms"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "perf_counter_delay_ms"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "max_exponential_backoff_factor"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "smear_percent"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "is_transient"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "initial_persistent_heartbeat_delay_ms"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "protocol_handler_config"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "channel_supports_offline_delivery"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "offline_heartbeat_threshold_ms"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "allow_suppression"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->n:Ljava/util/Set;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "version"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->a:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "network_timeout_delay_ms"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->b:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "write_retry_delay_ms"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->c:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "heartbeat_interval_ms"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->d:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "perf_counter_delay_ms"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->e:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "max_exponential_backoff_factor"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->f:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "smear_percent"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->g:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "is_transient"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->h:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "initial_persistent_heartbeat_delay_ms"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->i:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "protocol_handler_config"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->j:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "channel_supports_offline_delivery"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->k:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "offline_heartbeat_threshold_ms"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->l:Lcom/google/ipc/invalidation/a/N;

    new-instance v0, Lcom/google/ipc/invalidation/a/N;

    const-string v1, "allow_suppression"

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/a/N;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/ipc/invalidation/a/c;->m:Lcom/google/ipc/invalidation/a/N;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/util/Collection;
    .locals 1

    sget-object v0, Lcom/google/ipc/invalidation/a/c;->n:Ljava/util/Set;

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/N;)Z
    .locals 3

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    sget-object v0, Lcom/google/ipc/invalidation/a/c;->a:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->e()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->b:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->g()Z

    move-result v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->c:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->i()Z

    move-result v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->d:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->k()Z

    move-result v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->e:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->m()Z

    move-result v0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->f:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->o()Z

    move-result v0

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->g:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->q()Z

    move-result v0

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->h:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->s()Z

    move-result v0

    goto :goto_0

    :cond_7
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->i:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_8

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->u()Z

    move-result v0

    goto :goto_0

    :cond_8
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->j:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->w()Z

    move-result v0

    goto :goto_0

    :cond_9
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->k:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->y()Z

    move-result v0

    goto :goto_0

    :cond_a
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->l:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_b

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->A()Z

    move-result v0

    goto :goto_0

    :cond_b
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->m:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_c

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->C()Z

    move-result v0

    goto :goto_0

    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad descriptor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lcom/google/protobuf/MessageLite;Lcom/google/ipc/invalidation/a/N;)Ljava/lang/Object;
    .locals 3

    invoke-static {p1}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/a/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;

    sget-object v0, Lcom/google/ipc/invalidation/a/c;->a:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->f()Lcom/google/protos/ipc/invalidation/ClientProtocol$Version;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->b:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->c:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->d:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->e:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->f:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->p()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->g:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->r()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->h:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->t()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_7
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->i:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_8

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->v()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_8
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->j:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->x()Lcom/google/protos/ipc/invalidation/ClientProtocol$ProtocolHandlerConfigP;

    move-result-object v0

    goto :goto_0

    :cond_9
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->k:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->z()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_a
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->l:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_b

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->B()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    sget-object v0, Lcom/google/ipc/invalidation/a/c;->m:Lcom/google/ipc/invalidation/a/N;

    if-ne p2, v0, :cond_c

    invoke-virtual {p1}, Lcom/google/protos/ipc/invalidation/ClientProtocol$ClientConfigP;->D()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad descriptor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
