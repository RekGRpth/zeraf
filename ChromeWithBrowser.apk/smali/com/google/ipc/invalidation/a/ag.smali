.class public final enum Lcom/google/ipc/invalidation/a/ag;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/ipc/invalidation/a/ag;

.field private static enum b:Lcom/google/ipc/invalidation/a/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/ipc/invalidation/a/ag;

    const-string v1, "RESTART"

    invoke-direct {v0, v1, v2}, Lcom/google/ipc/invalidation/a/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/a/ag;->a:Lcom/google/ipc/invalidation/a/ag;

    new-instance v0, Lcom/google/ipc/invalidation/a/ag;

    const-string v1, "CONTINUE"

    invoke-direct {v0, v1, v3}, Lcom/google/ipc/invalidation/a/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ipc/invalidation/a/ag;->b:Lcom/google/ipc/invalidation/a/ag;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/ipc/invalidation/a/ag;

    sget-object v1, Lcom/google/ipc/invalidation/a/ag;->a:Lcom/google/ipc/invalidation/a/ag;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ipc/invalidation/a/ag;->b:Lcom/google/ipc/invalidation/a/ag;

    aput-object v1, v0, v3

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Z)Lcom/google/ipc/invalidation/a/ag;
    .locals 1

    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/ipc/invalidation/a/ag;->a:Lcom/google/ipc/invalidation/a/ag;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/ipc/invalidation/a/ag;->b:Lcom/google/ipc/invalidation/a/ag;

    goto :goto_0
.end method
