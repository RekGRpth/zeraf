.class public final Lcom/google/ipc/invalidation/b/i;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/StringBuilder;

.field private final b:Lcom/google/ipc/invalidation/b/j;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/ipc/invalidation/b/i;->a:Ljava/lang/StringBuilder;

    new-instance v0, Lcom/google/ipc/invalidation/b/j;

    iget-object v1, p0, Lcom/google/ipc/invalidation/b/i;->a:Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Lcom/google/ipc/invalidation/b/j;-><init>(Ljava/lang/StringBuilder;)V

    iput-object v0, p0, Lcom/google/ipc/invalidation/b/i;->b:Lcom/google/ipc/invalidation/b/j;

    return-void
.end method


# virtual methods
.method public final a(C)Lcom/google/ipc/invalidation/b/i;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/i;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    return-object p0
.end method

.method public final a(I)Lcom/google/ipc/invalidation/b/i;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/i;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    return-object p0
.end method

.method public final a(J)Lcom/google/ipc/invalidation/b/i;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/i;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    return-object p0
.end method

.method public final a(Lcom/google/protobuf/ByteString;)Lcom/google/ipc/invalidation/b/i;
    .locals 2

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/i;->a:Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/google/ipc/invalidation/b/c;->a(Lcom/google/protobuf/ByteString;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p0
.end method

.method public final a(Ljava/lang/Object;)Lcom/google/ipc/invalidation/b/i;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/i;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/ipc/invalidation/b/i;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/i;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p0
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/google/ipc/invalidation/b/i;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/i;->b:Lcom/google/ipc/invalidation/b/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/ipc/invalidation/b/j;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p0
.end method

.method public final a(Z)Lcom/google/ipc/invalidation/b/i;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/i;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ipc/invalidation/b/i;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
