.class Lcom/google/a/b/p;
.super Lcom/google/a/b/n;

# interfaces
.implements Ljava/util/List;


# instance fields
.field final synthetic e:Lcom/google/a/b/e;


# direct methods
.method constructor <init>(Lcom/google/a/b/e;Ljava/lang/Object;Ljava/util/List;Lcom/google/a/b/n;)V
    .locals 0

    iput-object p1, p0, Lcom/google/a/b/p;->e:Lcom/google/a/b/e;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/a/b/n;-><init>(Lcom/google/a/b/e;Ljava/lang/Object;Ljava/util/Collection;Lcom/google/a/b/n;)V

    return-void
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/a/b/p;->a()V

    iget-object v0, p0, Lcom/google/a/b/n;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/a/b/p;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/a/b/p;->e:Lcom/google/a/b/e;

    invoke-static {v1}, Lcom/google/a/b/e;->c(Lcom/google/a/b/e;)I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/a/b/p;->c()V

    :cond_0
    return-void
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 4

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/a/b/p;->size()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/a/b/p;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/a/b/n;->b:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    iget-object v3, p0, Lcom/google/a/b/p;->e:Lcom/google/a/b/e;

    sub-int/2addr v2, v1

    invoke-static {v3, v2}, Lcom/google/a/b/e;->a(Lcom/google/a/b/e;I)I

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/a/b/p;->c()V

    goto :goto_0
.end method

.method final d()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/a/b/n;->b:Ljava/util/Collection;

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/a/b/p;->a()V

    invoke-virtual {p0}, Lcom/google/a/b/p;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/a/b/p;->a()V

    invoke-virtual {p0}, Lcom/google/a/b/p;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/a/b/p;->a()V

    invoke-virtual {p0}, Lcom/google/a/b/p;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/a/b/p;->a()V

    new-instance v0, Lcom/google/a/b/q;

    invoke-direct {v0, p0}, Lcom/google/a/b/q;-><init>(Lcom/google/a/b/p;)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/a/b/p;->a()V

    new-instance v0, Lcom/google/a/b/q;

    invoke-direct {v0, p0, p1}, Lcom/google/a/b/q;-><init>(Lcom/google/a/b/p;I)V

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lcom/google/a/b/p;->a()V

    invoke-virtual {p0}, Lcom/google/a/b/p;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/a/b/p;->e:Lcom/google/a/b/e;

    invoke-static {v1}, Lcom/google/a/b/e;->b(Lcom/google/a/b/e;)I

    invoke-virtual {p0}, Lcom/google/a/b/p;->b()V

    return-object v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/a/b/p;->a()V

    invoke-virtual {p0}, Lcom/google/a/b/p;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 4

    invoke-virtual {p0}, Lcom/google/a/b/p;->a()V

    iget-object v0, p0, Lcom/google/a/b/p;->e:Lcom/google/a/b/e;

    iget-object v1, p0, Lcom/google/a/b/n;->a:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/a/b/p;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/a/b/n;->c:Lcom/google/a/b/n;

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0, v1, v2, p0}, Lcom/google/a/b/e;->a(Lcom/google/a/b/e;Ljava/lang/Object;Ljava/util/List;Lcom/google/a/b/n;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object p0, p0, Lcom/google/a/b/n;->c:Lcom/google/a/b/n;

    goto :goto_0
.end method
