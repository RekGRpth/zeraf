.class public Lcom/a/a/a/a/d;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Landroid/content/Intent;

.field private static final c:[B


# instance fields
.field private final d:Landroid/os/Binder;

.field private final e:Landroid/content/Context;

.field private final f:Z

.field private final g:Lcom/a/a/a/a/f;

.field private h:Z

.field private volatile i:Lcom/a/a/a/a/e;

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/a/a/a/a/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/a/d;->a:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.googlecode.eyesfree.braille.service.ACTION_SELF_BRAILLE_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.googlecode.eyesfree.brailleback"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/a/d;->b:Landroid/content/Intent;

    const/16 v0, 0x14

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/a/a/a/a/d;->c:[B

    return-void

    nop

    :array_0
    .array-data 1
        -0x65t
        0x42t
        0x4ct
        0x2dt
        0x27t
        -0x53t
        0x51t
        -0x5ct
        0x2at
        0x33t
        0x7et
        0xbt
        -0x4at
        -0x67t
        0x1ct
        0x76t
        -0x14t
        -0x5ct
        0x44t
        0x61t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a/d;->d:Landroid/os/Binder;

    new-instance v0, Lcom/a/a/a/a/f;

    invoke-direct {v0, p0, v1}, Lcom/a/a/a/a/f;-><init>(Lcom/a/a/a/a/d;B)V

    iput-object v0, p0, Lcom/a/a/a/a/d;->g:Lcom/a/a/a/a/f;

    iput-boolean v1, p0, Lcom/a/a/a/a/d;->h:Z

    iput v1, p0, Lcom/a/a/a/a/d;->j:I

    iput-object p1, p0, Lcom/a/a/a/a/d;->e:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/a/a/a/a/d;->f:Z

    invoke-direct {p0}, Lcom/a/a/a/a/d;->c()V

    return-void
.end method

.method static synthetic a(Lcom/a/a/a/a/d;I)I
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/a/a/a/a/d;->j:I

    return v0
.end method

.method static synthetic a(Lcom/a/a/a/a/d;)Z
    .locals 1

    invoke-direct {p0}, Lcom/a/a/a/a/d;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/a/a/a/a/d;)Lcom/a/a/a/a/f;
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/d;->g:Lcom/a/a/a/a/f;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/a/a/a/a/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/a/a/a/a/d;)I
    .locals 1

    iget v0, p0, Lcom/a/a/a/a/d;->j:I

    return v0
.end method

.method private c()V
    .locals 4

    new-instance v0, Lcom/a/a/a/a/e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/a/a/a/e;-><init>(Lcom/a/a/a/a/d;B)V

    iget-object v1, p0, Lcom/a/a/a/a/d;->e:Landroid/content/Context;

    sget-object v2, Lcom/a/a/a/a/d;->b:Landroid/content/Intent;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v0, Lcom/a/a/a/a/d;->a:Ljava/lang/String;

    const-string v1, "Failed to bind to service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/a/a/a/a/d;->g:Lcom/a/a/a/a/f;

    invoke-virtual {v0}, Lcom/a/a/a/a/f;->a()V

    :goto_0
    return-void

    :cond_0
    iput-object v0, p0, Lcom/a/a/a/a/d;->i:Lcom/a/a/a/a/e;

    sget-object v0, Lcom/a/a/a/a/d;->a:Ljava/lang/String;

    const-string v1, "Bound to self braille service"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic d(Lcom/a/a/a/a/d;)I
    .locals 1

    iget v0, p0, Lcom/a/a/a/a/d;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/a/a/a/d;->j:I

    return v0
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lcom/a/a/a/a/d;->i:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/a/a/a/a/d;->e()Lcom/a/a/a/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/a/a/a/a/d;->d:Landroid/os/Binder;

    invoke-interface {v0, v1}, Lcom/a/a/a/a/a;->a(Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/a/a/a/a/d;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/a/a/a/a/d;->i:Lcom/a/a/a/a/e;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/a/d;->i:Lcom/a/a/a/a/e;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private e()Lcom/a/a/a/a/a;
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/d;->i:Lcom/a/a/a/a/e;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/a/a/a/a/e;->a(Lcom/a/a/a/a/e;)Lcom/a/a/a/a/a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/a/a/a/a/d;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/a/a/a/a/d;->h:Z

    return v0
.end method

.method static synthetic f(Lcom/a/a/a/a/d;)Lcom/a/a/a/a/e;
    .locals 1

    iget-object v0, p0, Lcom/a/a/a/a/d;->i:Lcom/a/a/a/a/e;

    return-object v0
.end method

.method private f()Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/a/a/a/d;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    :try_start_0
    const-string v3, "com.googlecode.eyesfree.brailleback"

    const/16 v4, 0x40

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :try_start_1
    const-string v3, "SHA-1"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    iget-object v4, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    invoke-virtual {v6}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/security/MessageDigest;->update([B)V

    sget-object v6, Lcom/a/a/a/a/d;->c:[B

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v7

    invoke-static {v6, v7}, Ljava/security/MessageDigest;->isEqual([B[B)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_1
    return v0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/a/a/a/a/d;->a:Ljava/lang/String;

    const-string v3, "Can\'t verify package com.googlecode.eyesfree.brailleback"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v2, Lcom/a/a/a/a/d;->a:Ljava/lang/String;

    const-string v3, "SHA-1 not supported"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_1

    :cond_0
    invoke-virtual {v3}, Ljava/security/MessageDigest;->reset()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-boolean v2, p0, Lcom/a/a/a/a/d;->f:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/a/a/a/a/d;->a:Ljava/lang/String;

    const-string v3, "*** %s connected to BrailleBack with invalid (debug?) signature ***"

    new-array v4, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/a/a/a/a/d;->e:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static synthetic g(Lcom/a/a/a/a/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/a/a/a/a/d;->d()V

    return-void
.end method

.method static synthetic h(Lcom/a/a/a/a/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/a/a/a/a/d;->c()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/a/a/a/d;->h:Z

    invoke-direct {p0}, Lcom/a/a/a/a/d;->d()V

    return-void
.end method

.method public final a(Lcom/a/a/a/a/g;)V
    .locals 3

    invoke-virtual {p1}, Lcom/a/a/a/a/g;->a()V

    invoke-direct {p0}, Lcom/a/a/a/a/d;->e()Lcom/a/a/a/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/a/a/a/a/d;->d:Landroid/os/Binder;

    invoke-interface {v0, v1, p1}, Lcom/a/a/a/a/a;->a(Landroid/os/IBinder;Lcom/a/a/a/a/g;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/a/a/a/a/d;->a:Ljava/lang/String;

    const-string v2, "Self braille write failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
