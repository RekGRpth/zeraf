.class public Lorg/chromium/sync/notifier/InvalidationService;
.super Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;


# static fields
.field static final CLIENT_TYPE:I = 0x3fa

.field private static final RANDOM:Ljava/util/Random;

.field private static final TAG:Ljava/lang/String;

.field private static sClientId:[B

.field private static sIsClientStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/sync/notifier/InvalidationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/chromium/sync/notifier/InvalidationService;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lorg/chromium/sync/notifier/InvalidationService;->RANDOM:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;-><init>()V

    return-void
.end method

.method static computeRegistrationOps(Ljava/util/Set;Ljava/util/Set;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 1

    invoke-static {p1, p0}, Lcom/google/a/b/B;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/a/b/K;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    invoke-static {p0, p1}, Lcom/google/a/b/B;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/a/b/K;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private ensureAccount(Landroid/accounts/Account;)V
    .locals 1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->stopClient()V

    :cond_2
    invoke-direct {p0, p1}, Lorg/chromium/sync/notifier/InvalidationService;->setAccount(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method private ensureClientStartState()V
    .locals 2

    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->shouldClientBeRunning()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v1, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->stopClient()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v0, :cond_0

    sget-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->startClient()V

    goto :goto_0
.end method

.method static getClientIdForTest()[B
    .locals 1

    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    return-object v0
.end method

.method private static getClientName()[B
    .locals 2

    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->RANDOM:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method static getIsClientStartedForTest()Z
    .locals 1

    sget-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    return v0
.end method

.method private requestSync(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 4

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-eqz p3, :cond_3

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "objectId"

    new-instance v1, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->getName()[B

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v3, "version"

    if-nez p2, :cond_4

    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "payload"

    if-nez p3, :cond_2

    const-string p3, ""

    :cond_2
    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-static {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/InvalidationController;->getContractAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v0, v1}, Lorg/chromium/sync/notifier/InvalidationService;->requestSyncFromContentResolver(Landroid/os/Bundle;Landroid/accounts/Account;Ljava/lang/String;)V

    return-void

    :cond_4
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method private setAccount(Landroid/accounts/Account;)V
    .locals 2

    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->edit()Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->setAccount(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;Landroid/accounts/Account;)V

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->commit(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Z

    return-void
.end method

.method private static setClientId([B)V
    .locals 0

    sput-object p0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    return-void
.end method

.method private static setIsClientStarted(Z)V
    .locals 0

    sput-boolean p0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    return-void
.end method

.method private setRegisteredTypes(Ljava/util/Set;)V
    .locals 4

    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    new-instance v1, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v1, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->edit()Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->setSyncTypes(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;Ljava/util/Collection;)V

    invoke-virtual {v1, v2}, Lorg/chromium/sync/notifier/InvalidationPreferences;->commit(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Z

    sget-object v1, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    if-nez v1, :cond_1

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->syncTypesToModelTypes(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v1}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->modelTypesToObjectIds(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1, v3, v2}, Lorg/chromium/sync/notifier/InvalidationService;->computeRegistrationOps(Ljava/util/Set;Ljava/util/Set;Ljava/util/Collection;Ljava/util/Collection;)V

    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    invoke-virtual {p0, v0, v2}, Lorg/chromium/sync/notifier/InvalidationService;->unregister([BLjava/lang/Iterable;)V

    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    invoke-virtual {p0, v0, v3}, Lorg/chromium/sync/notifier/InvalidationService;->register([BLjava/lang/Iterable;)V

    goto :goto_1
.end method

.method private startClient()V
    .locals 2

    const/16 v0, 0x3fa

    invoke-static {}, Lorg/chromium/sync/notifier/InvalidationService;->getClientName()[B

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->createStartIntent(Landroid/content/Context;I[B)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const/4 v0, 0x1

    sput-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    return-void
.end method

.method private stopClient()V
    .locals 1

    invoke-static {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->createStopIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const/4 v0, 0x0

    sput-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    return-void
.end method


# virtual methods
.method public informError(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V
    .locals 3

    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalidation client error:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->isTransient()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->stopClient()V

    :cond_0
    return-void
.end method

.method public informRegistrationFailure([BLcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V
    .locals 3

    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Registration failure on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ; transient = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-static {v0}, Lcom/google/a/b/B;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->register([BLjava/lang/Iterable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->unregister([BLjava/lang/Iterable;)V

    goto :goto_0
.end method

.method public informRegistrationStatus([BLcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V
    .locals 3

    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Registration status for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-static {v0}, Lcom/google/a/b/B;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    sget-object v2, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->REGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    if-ne p3, v2, :cond_1

    if-nez v1, :cond_0

    sget-object v1, Lorg/chromium/sync/notifier/InvalidationService;->TAG:Ljava/lang/String;

    const-string v2, "Unregistering for object we\'re no longer interested in"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->unregister([BLjava/lang/Iterable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v1, :cond_0

    sget-object v1, Lorg/chromium/sync/notifier/InvalidationService;->TAG:Ljava/lang/String;

    const-string v2, "Registering for an object"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->register([BLjava/lang/Iterable;)V

    goto :goto_0
.end method

.method public invalidate(Lcom/google/ipc/invalidation/external/client/types/Invalidation;[B)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getPayload()[B

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getVersion()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, Lorg/chromium/sync/notifier/InvalidationService;->requestSync(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Ljava/lang/Long;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lorg/chromium/sync/notifier/InvalidationService;->acknowledge([B)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public invalidateAll([B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->requestSync(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Ljava/lang/Long;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lorg/chromium/sync/notifier/InvalidationService;->acknowledge([B)V

    return-void
.end method

.method public invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/types/ObjectId;[B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->requestSync(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Ljava/lang/Long;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lorg/chromium/sync/notifier/InvalidationService;->acknowledge([B)V

    return-void
.end method

.method isChromeInForeground()Z
    .locals 1

    invoke-static {}, Lorg/chromium/base/ActivityStatus;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method isSyncEnabled()Z
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    return v0
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 1

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    :goto_0
    invoke-direct {p0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->ensureAccount(Landroid/accounts/Account;)V

    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->ensureClientStartState()V

    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationController$IntentProtocol;->isStop(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->stopClient()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationController$IntentProtocol;->isRegisteredTypesChange(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "registered_types"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/a/b/B;->a(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->setRegisteredTypes(Ljava/util/Set;)V

    goto :goto_1

    :cond_2
    invoke-super {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->onHandleIntent(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method readRegistrationsFromPrefs()Ljava/util/Set;
    .locals 3

    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedTypes()Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->syncTypesToModelTypes(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/a/b/B;->a(I)Ljava/util/HashSet;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-virtual {v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->toObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public readState()[B
    .locals 1

    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getInternalNotificationClientState()[B

    move-result-object v0

    return-object v0
.end method

.method public ready([B)V
    .locals 0

    sput-object p1, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    invoke-virtual {p0, p1}, Lorg/chromium/sync/notifier/InvalidationService;->reissueRegistrations([B)V

    return-void
.end method

.method public reissueRegistrations([B)V
    .locals 2

    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->register([BLjava/lang/Iterable;)V

    :cond_0
    return-void
.end method

.method public requestAuthToken(Landroid/app/PendingIntent;Ljava/lang/String;)V
    .locals 3

    invoke-static {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->TAG:Ljava/lang/String;

    const-string v1, "No signed-in user; cannot send message to data center"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    const-string v2, "chromiumsync"

    invoke-virtual {v1, v0, p2, v2}, Lorg/chromium/sync/signin/AccountManagerHelper;->getNewAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "chromiumsync"

    invoke-static {p0, p1, v0, v1}, Lorg/chromium/sync/notifier/InvalidationService;->setAuthToken(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method requestSyncFromContentResolver(Landroid/os/Bundle;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Request sync: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p2, p3, p1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method shouldClientBeRunning()Z
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->isSyncEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->isChromeInForeground()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeState([B)V
    .locals 2

    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->edit()Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->setInternalNotificationClientState(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;[B)V

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->commit(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Z

    return-void
.end method
