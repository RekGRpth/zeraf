.class public Lorg/chromium/sync/notifier/SyncStatusHelper;
.super Ljava/lang/Object;


# static fields
.field public static final AUTH_TOKEN_TYPE_SYNC:Ljava/lang/String; = "chromiumsync"

.field public static final SIGNED_IN_ACCOUNT_KEY:Ljava/lang/String; = "google.services.username"

.field public static final TAG:Ljava/lang/String; = "SyncStatusHelper"

.field private static final lock:Ljava/lang/Object;

.field private static sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private mListeners:Ljava/util/ArrayList;

.field private final mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/sync/notifier/SyncStatusHelper;->lock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    iput-object p2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mListeners:Ljava/util/ArrayList;

    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;
    .locals 4

    sget-object v1, Lorg/chromium/sync/notifier/SyncStatusHelper;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/chromium/sync/notifier/SyncStatusHelper;->sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Lorg/chromium/sync/notifier/SyncStatusHelper;

    new-instance v3, Lorg/chromium/sync/notifier/SystemSyncContentResolverDelegate;

    invoke-direct {v3}, Lorg/chromium/sync/notifier/SystemSyncContentResolverDelegate;-><init>()V

    invoke-direct {v2, v0, v3}, Lorg/chromium/sync/notifier/SyncStatusHelper;-><init>(Landroid/content/Context;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;)V

    sput-object v2, Lorg/chromium/sync/notifier/SyncStatusHelper;->sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lorg/chromium/sync/notifier/SyncStatusHelper;->sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getPreferences()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private getSignedInAccountName()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "google.services.username"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private makeSyncable(Landroid/accounts/Account;)V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationController;->getContractAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->hasFinishedFirstSync(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    const/4 v3, 0x1

    invoke-interface {v0, p1, v2, v3}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    invoke-virtual {v5, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v6, v5, v2}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_1

    iget-object v6, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v6, v5, v2, v1}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static overrideSyncStatusHelperForTests(Landroid/content/Context;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;)V
    .locals 3

    sget-object v1, Lorg/chromium/sync/notifier/SyncStatusHelper;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/chromium/sync/notifier/SyncStatusHelper;->sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "SyncStatusHelper already exists"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-direct {v0, p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;-><init>(Landroid/content/Context;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;)V

    sput-object v0, Lorg/chromium/sync/notifier/SyncStatusHelper;->sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private static temporarilyAllowDiskWritesAndDiskReads()Landroid/os/StrictMode$ThreadPolicy;
    .locals 2

    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    new-instance v1, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v1, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>(Landroid/os/StrictMode$ThreadPolicy;)V

    invoke-virtual {v1}, Landroid/os/StrictMode$ThreadPolicy$Builder;->permitDiskReads()Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-virtual {v1}, Landroid/os/StrictMode$ThreadPolicy$Builder;->permitDiskWrites()Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-virtual {v1}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    return-object v0
.end method


# virtual methods
.method public addListener(Lorg/chromium/sync/notifier/SyncStatusHelper$Listener;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearSignedInUser()V
    .locals 2

    const-string v0, "SyncStatusHelper"

    const-string v1, "Clearing user signed in to Chrome"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->setSignedInAccountName(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/notifier/SyncStatusHelper$Listener;

    invoke-interface {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper$Listener;->onClearSignedInUser()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public disableAndroidSync(Landroid/accounts/Account;)V
    .locals 4

    invoke-static {}, Lorg/chromium/sync/notifier/SyncStatusHelper;->temporarilyAllowDiskWritesAndDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/InvalidationController;->getContractAuthority()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v2, p1, v1}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    const/4 v3, 0x0

    invoke-interface {v2, p1, v1, v3}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    :cond_0
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    return-void
.end method

.method public enableAndroidSync(Landroid/accounts/Account;)V
    .locals 4

    invoke-static {}, Lorg/chromium/sync/notifier/SyncStatusHelper;->temporarilyAllowDiskWritesAndDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-direct {p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->makeSyncable(Landroid/accounts/Account;)V

    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/InvalidationController;->getContractAuthority()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v2, p1, v1}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    const/4 v3, 0x1

    invoke-interface {v2, p1, v1, v3}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    :cond_0
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    return-void
.end method

.method public getSignedInUser()Landroid/accounts/Account;
    .locals 1

    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0
.end method

.method hasFinishedFirstSync(Landroid/accounts/Account;)Z
    .locals 2

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationController;->getContractAuthority()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v1, p1, v0}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMasterSyncAutomaticallyEnabled()Z
    .locals 2

    invoke-static {}, Lorg/chromium/sync/notifier/SyncStatusHelper;->temporarilyAllowDiskWritesAndDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v1}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getMasterSyncAutomatically()Z

    move-result v1

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    return v1
.end method

.method public isSignedIn()Z
    .locals 1

    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSyncEnabled()Z
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method public isSyncEnabled(Landroid/accounts/Account;)Z
    .locals 3

    invoke-static {}, Lorg/chromium/sync/notifier/SyncStatusHelper;->temporarilyAllowDiskWritesAndDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationController;->getContractAuthority()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v2}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getMasterSyncAutomatically()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v2, p1, v0}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSyncEnabledForChrome(Landroid/accounts/Account;)Z
    .locals 3

    invoke-static {}, Lorg/chromium/sync/notifier/SyncStatusHelper;->temporarilyAllowDiskWritesAndDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/InvalidationController;->newInstance(Landroid/content/Context;)Lorg/chromium/sync/notifier/InvalidationController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationController;->getContractAuthority()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v2, p1, v0}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerContentResolverObserver(Landroid/content/SyncStatusObserver;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    const/4 v1, 0x1

    invoke-interface {v0, v1, p1}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public removeListener(Lorg/chromium/sync/notifier/SyncStatusHelper$Listener;)Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setSignedInAccountName(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "google.services.username"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public unregisterContentResolverObserver(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverWrapper:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v0, p1}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->removeStatusChangeListener(Ljava/lang/Object;)V

    return-void
.end method
