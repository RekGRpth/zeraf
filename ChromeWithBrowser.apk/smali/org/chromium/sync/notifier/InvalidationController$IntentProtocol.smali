.class public Lorg/chromium/sync/notifier/InvalidationController$IntentProtocol;
.super Ljava/lang/Object;


# static fields
.field public static final ACTION_REGISTER:Ljava/lang/String; = "org.chromium.sync.notifier.ACTION_REGISTER_TYPES"

.field public static final EXTRA_ACCOUNT:Ljava/lang/String; = "account"

.field public static final EXTRA_REGISTERED_TYPES:Ljava/lang/String; = "registered_types"

.field public static final EXTRA_STOP:Ljava/lang/String; = "stop"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createRegisterIntent(Landroid/accounts/Account;ZLjava/util/Set;)Landroid/content/Intent;
    .locals 6

    const/4 v0, 0x0

    new-instance v4, Landroid/content/Intent;

    const-string v1, "org.chromium.sync.notifier.ACTION_REGISTER_TYPES"

    invoke-direct {v4, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "ALL_TYPES"

    aput-object v2, v1, v0

    move-object v0, v1

    :goto_0
    const-string v1, "registered_types"

    invoke-static {v0}, Lcom/google/a/b/B;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "account"

    invoke-virtual {v4, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v4

    :cond_0
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v1

    new-array v3, v1, [Ljava/lang/String;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->name()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    move v1, v2

    goto :goto_1

    :cond_1
    move-object v0, v3

    goto :goto_0
.end method

.method public static isRegisteredTypesChange(Landroid/content/Intent;)Z
    .locals 1

    const-string v0, "registered_types"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isStop(Landroid/content/Intent;)Z
    .locals 2

    const-string v0, "stop"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
