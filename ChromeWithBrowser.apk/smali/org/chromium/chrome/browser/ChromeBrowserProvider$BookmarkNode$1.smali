.class final Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# instance fields
.field private mNodeMap:Ljava/util/HashMap;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getNode(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 4

    const/4 v0, 0x0

    const-wide/16 v1, -0x1

    cmp-long v1, p1, v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, "ChromeBrowserProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid BookmarkNode hierarchy. Unknown id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    goto :goto_0
.end method

.method private readNodeContents(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 11

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v7

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v8

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v9

    if-ltz v3, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->values()[Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    move-result-object v0

    array-length v0, v0

    if-lt v3, v0, :cond_1

    :cond_0
    const-string v0, "ChromeBrowserProvider"

    const-string v1, "Invalid node type ordinal value."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-static {}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->values()[Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    move-result-object v6

    aget-object v3, v6, v3

    invoke-direct {p0, v9, v10}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->getNode(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;-><init>(JLorg/chromium/chrome/browser/ChromeBrowserProvider$Type;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    invoke-virtual {v0, v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->setFavicon([B)V

    invoke-virtual {v0, v8}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->setThumbnail([B)V

    goto :goto_0
.end method

.method private readNodeContentsRecursive(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->readNodeContents(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v2, "ChromeBrowserProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid BookmarkNode hierarchy. Duplicate id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->readNodeContentsRecursive(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->addChild(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->createFromParcel(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    return-object v0
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->readNodeContentsRecursive(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->getNode(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->newArray(I)[Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 1

    new-array v0, p1, [Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    return-object v0
.end method
