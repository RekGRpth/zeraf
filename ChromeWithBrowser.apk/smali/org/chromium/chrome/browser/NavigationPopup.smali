.class public Lorg/chromium/chrome/browser/NavigationPopup;
.super Landroid/widget/ListPopupWindow;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final FAVICON_SIZE_DP:I = 0x10

.field private static final LIST_ITEM_HEIGHT_DP:I = 0x30

.field private static final MAXIMUM_HISTORY_ITEMS:I = 0x8

.field private static final PADDING_DP:I = 0x8

.field private static final TEXT_SIZE_SP:I = 0x12


# instance fields
.field private final mAdapter:Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;

.field private final mContext:Landroid/content/Context;

.field private final mDelegate:Lorg/chromium/chrome/browser/NavigationPopup$NavigationPopupDelegate;

.field private final mFaviconSize:I

.field private final mHistory:Lorg/chromium/content/browser/NavigationHistory;

.field private final mListItemHeight:I

.field private mNativeNavigationPopup:I

.field private final mNavigationClient:Lorg/chromium/content/browser/NavigationClient;

.field private final mPadding:I

.field private final mShowHistoryView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/NavigationPopup;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/NavigationPopup$NavigationPopupDelegate;Lorg/chromium/content/browser/NavigationClient;Z)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const v0, 0x1010300

    invoke-direct {p0, p1, v4, v0}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mContext:Landroid/content/Context;

    sget-boolean v0, Lorg/chromium/chrome/browser/NavigationPopup;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "NavigationPopup requires non-null delegate."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mDelegate:Lorg/chromium/chrome/browser/NavigationPopup$NavigationPopupDelegate;

    iput-object p3, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNavigationClient:Lorg/chromium/content/browser/NavigationClient;

    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNavigationClient:Lorg/chromium/content/browser/NavigationClient;

    const/16 v1, 0x8

    invoke-interface {v0, p4, v1}, Lorg/chromium/content/browser/NavigationClient;->getDirectedNavigationHistory(ZI)Lorg/chromium/content/browser/NavigationHistory;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content/browser/NavigationHistory;

    new-instance v0, Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;

    invoke-direct {v0, p0, v4}, Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;-><init>(Lorg/chromium/chrome/browser/NavigationPopup;Lorg/chromium/chrome/browser/NavigationPopup$1;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mAdapter:Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;

    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42400000

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mListItemHeight:I

    const/high16 v1, 0x41800000

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mFaviconSize:I

    const/high16 v1, 0x41000000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mPadding:I

    invoke-virtual {p0, v3}, Lorg/chromium/chrome/browser/NavigationPopup;->setModal(Z)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->setInputMethodMode(I)V

    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->setHeight(I)V

    invoke-virtual {p0, p0}, Lorg/chromium/chrome/browser/NavigationPopup;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Landroid/widget/ListView$FixedViewInfo;

    new-instance v1, Landroid/widget/ListView;

    invoke-direct {v1, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v1}, Landroid/widget/ListView$FixedViewInfo;-><init>(Landroid/widget/ListView;)V

    invoke-direct {p0}, Lorg/chromium/chrome/browser/NavigationPopup;->createListItem()Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mShowHistoryView:Landroid/widget/TextView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mShowHistoryView:Landroid/widget/TextView;

    sget v2, Lorg/chromium/chrome/R$string;->show_history_label:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iput-boolean v3, v0, Landroid/widget/ListView$FixedViewInfo;->isSelectable:Z

    iget-object v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mShowHistoryView:Landroid/widget/TextView;

    iput-object v1, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/widget/HeaderViewListAdapter;

    iget-object v2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mAdapter:Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;

    invoke-direct {v0, v4, v1, v2}, Landroid/widget/HeaderViewListAdapter;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/NavigationPopup;)Lorg/chromium/content/browser/NavigationHistory;
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content/browser/NavigationHistory;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/NavigationPopup;)Landroid/widget/TextView;
    .locals 1

    invoke-direct {p0}, Lorg/chromium/chrome/browser/NavigationPopup;->createListItem()Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/NavigationPopup;Landroid/widget/TextView;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/NavigationPopup;->updateBitmapForTextView(Landroid/widget/TextView;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private createListItem()Landroid/widget/TextView;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    const/high16 v1, 0x41900000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mListItemHeight:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinimumHeight(I)V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mPadding:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    iget v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mPadding:I

    iget v2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mPadding:I

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    return-object v0
.end method

.method public static getHistoryUrl()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeGetHistoryUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initializeNative()V
    .locals 4

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    invoke-direct {p0}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeInit()I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:I

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content/browser/NavigationHistory;

    invoke-virtual {v2}, Lorg/chromium/content/browser/NavigationHistory;->getEntryCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content/browser/NavigationHistory;

    invoke-virtual {v2, v0}, Lorg/chromium/content/browser/NavigationHistory;->getEntryAtIndex(I)Lorg/chromium/content/browser/NavigationEntry;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/content/browser/NavigationEntry;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lorg/chromium/content/browser/NavigationEntry;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:I

    invoke-direct {p0, v3, v2}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeFetchFaviconForUrl(ILjava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:I

    invoke-static {}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeGetHistoryUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeFetchFaviconForUrl(ILjava/lang/String;)V

    return-void
.end method

.method private native nativeDestroy(I)V
.end method

.method private native nativeFetchFaviconForUrl(ILjava/lang/String;)V
.end method

.method private static native nativeGetHistoryUrl()Ljava/lang/String;
.end method

.method private native nativeInit()I
.end method

.method private onFaviconUpdated(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content/browser/NavigationHistory;

    invoke-virtual {v0}, Lorg/chromium/content/browser/NavigationHistory;->getEntryCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content/browser/NavigationHistory;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/NavigationHistory;->getEntryAtIndex(I)Lorg/chromium/content/browser/NavigationEntry;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/content/browser/NavigationEntry;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v0}, Lorg/chromium/content/browser/NavigationEntry;->updateFavicon(Landroid/graphics/Bitmap;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeGetHistoryUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mShowHistoryView:Landroid/widget/TextView;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, p2}, Lorg/chromium/chrome/browser/NavigationPopup;->updateBitmapForTextView(Landroid/widget/TextView;Landroid/graphics/Bitmap;)V

    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mAdapter:Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method private updateBitmapForTextView(Landroid/widget/TextView;Landroid/graphics/Bitmap;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v1, v0, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object v0, v1

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    const/16 v2, 0x77

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    :goto_0
    iget v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mFaviconSize:I

    iget v2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mFaviconSize:I

    invoke-virtual {v1, v3, v3, v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p1, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    iget v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:I

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeDestroy(I)V

    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:I

    :cond_0
    invoke-super {p0}, Landroid/widget/ListPopupWindow;->dismiss()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content/browser/NavigationHistory;

    invoke-virtual {v0}, Lorg/chromium/content/browser/NavigationHistory;->getEntryCount()I

    move-result v0

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mDelegate:Lorg/chromium/chrome/browser/NavigationPopup$NavigationPopupDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/NavigationPopup$NavigationPopupDelegate;->openHistory()V

    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/NavigationPopup;->dismiss()V

    return-void

    :cond_0
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/browser/NavigationEntry;

    iget-object v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNavigationClient:Lorg/chromium/content/browser/NavigationClient;

    invoke-virtual {v0}, Lorg/chromium/content/browser/NavigationEntry;->getIndex()I

    move-result v0

    invoke-interface {v1, v0}, Lorg/chromium/content/browser/NavigationClient;->goToNavigationIndex(I)V

    goto :goto_0
.end method

.method public shouldBeShown()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content/browser/NavigationHistory;

    invoke-virtual {v0}, Lorg/chromium/content/browser/NavigationHistory;->getEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public show()V
    .locals 1

    iget v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/chrome/browser/NavigationPopup;->initializeNative()V

    :cond_0
    invoke-super {p0}, Landroid/widget/ListPopupWindow;->show()V

    return-void
.end method
