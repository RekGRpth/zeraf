.class public Lorg/chromium/chrome/browser/TabBase;
.super Ljava/lang/Object;


# instance fields
.field private mCleanupReference:Lorg/chromium/content/common/CleanupReference;

.field private mContentView:Lorg/chromium/content/browser/ContentView;

.field private mIsLoading:Z

.field private mNativeTabBaseAndroidImpl:I

.field private mObservers:Ljava/util/List;

.field private mWebContentsDelegate:Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

.field private mWindow:Lorg/chromium/ui/gfx/NativeWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILorg/chromium/ui/gfx/NativeWindow;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mObservers:Ljava/util/List;

    iput-boolean v1, p0, Lorg/chromium/chrome/browser/TabBase;->mIsLoading:Z

    iput-object p3, p0, Lorg/chromium/chrome/browser/TabBase;->mWindow:Lorg/chromium/ui/gfx/NativeWindow;

    if-nez p2, :cond_0

    invoke-static {v1}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(Z)I

    move-result p2

    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mWindow:Lorg/chromium/ui/gfx/NativeWindow;

    const/4 v1, 0x1

    invoke-static {p1, p2, v0, v1}, Lorg/chromium/content/browser/ContentView;->newInstance(Landroid/content/Context;ILorg/chromium/ui/gfx/NativeWindow;I)Lorg/chromium/content/browser/ContentView;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {p3}, Lorg/chromium/ui/gfx/NativeWindow;->getNativePointer()I

    move-result v0

    invoke-direct {p0, p2, v0}, Lorg/chromium/chrome/browser/TabBase;->nativeInit(II)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/TabBase;->mNativeTabBaseAndroidImpl:I

    new-instance v0, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;

    invoke-direct {v0, p0, v3}, Lorg/chromium/chrome/browser/TabBase$TabBaseChromeWebContentsDelegateAndroid;-><init>(Lorg/chromium/chrome/browser/TabBase;Lorg/chromium/chrome/browser/TabBase$1;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mWebContentsDelegate:Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

    iget v0, p0, Lorg/chromium/chrome/browser/TabBase;->mNativeTabBaseAndroidImpl:I

    iget-object v1, p0, Lorg/chromium/chrome/browser/TabBase;->mWebContentsDelegate:Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/TabBase;->nativeInitWebContentsDelegate(ILorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;)V

    new-instance v0, Lorg/chromium/content/common/CleanupReference;

    new-instance v1, Lorg/chromium/chrome/browser/TabBase$DestroyRunnable;

    iget v2, p0, Lorg/chromium/chrome/browser/TabBase;->mNativeTabBaseAndroidImpl:I

    invoke-direct {v1, v2, v3}, Lorg/chromium/chrome/browser/TabBase$DestroyRunnable;-><init>(ILorg/chromium/chrome/browser/TabBase$1;)V

    invoke-direct {v0, p0, v1}, Lorg/chromium/content/common/CleanupReference;-><init>(Ljava/lang/Object;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mCleanupReference:Lorg/chromium/content/common/CleanupReference;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lorg/chromium/ui/gfx/NativeWindow;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Lorg/chromium/chrome/browser/TabBase;-><init>(Landroid/content/Context;ILorg/chromium/ui/gfx/NativeWindow;)V

    invoke-virtual {p0, p2}, Lorg/chromium/chrome/browser/TabBase;->loadUrlWithSanitization(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(I)V
    .locals 0

    invoke-static {p0}, Lorg/chromium/chrome/browser/TabBase;->nativeDestroy(I)V

    return-void
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/TabBase;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mObservers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lorg/chromium/chrome/browser/TabBase;Z)Z
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/chrome/browser/TabBase;->mIsLoading:Z

    return p1
.end method

.method private destroyContentView()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mContentView:Lorg/chromium/content/browser/ContentView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mContentView:Lorg/chromium/content/browser/ContentView;

    goto :goto_0
.end method

.method private static native nativeDestroy(I)V
.end method

.method private native nativeFixupUrl(ILjava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeInit(II)I
.end method

.method private native nativeInitWebContentsDelegate(ILorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;)V
.end method


# virtual methods
.method public addObserver(Lorg/chromium/chrome/browser/TabObserver;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public destroy()V
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mObservers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mObservers:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/TabObserver;->onCloseTab(Lorg/chromium/chrome/browser/TabBase;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lorg/chromium/chrome/browser/TabBase;->destroyContentView()V

    iget v0, p0, Lorg/chromium/chrome/browser/TabBase;->mNativeTabBaseAndroidImpl:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mCleanupReference:Lorg/chromium/content/common/CleanupReference;

    invoke-virtual {v0}, Lorg/chromium/content/common/CleanupReference;->cleanupNow()V

    iput v2, p0, Lorg/chromium/chrome/browser/TabBase;->mNativeTabBaseAndroidImpl:I

    :cond_1
    return-void
.end method

.method public getContentView()Lorg/chromium/content/browser/ContentView;
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mContentView:Lorg/chromium/content/browser/ContentView;

    return-object v0
.end method

.method public getNativeTab()I
    .locals 1

    iget v0, p0, Lorg/chromium/chrome/browser/TabBase;->mNativeTabBaseAndroidImpl:I

    return v0
.end method

.method public isLoading()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/TabBase;->mIsLoading:Z

    return v0
.end method

.method public loadUrlWithSanitization(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lorg/chromium/chrome/browser/TabBase;->mNativeTabBaseAndroidImpl:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/chrome/browser/TabBase;->nativeFixupUrl(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/chromium/chrome/browser/TabBase;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentView;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mContentView:Lorg/chromium/content/browser/ContentView;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentView;->reload()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lorg/chromium/chrome/browser/TabBase;->mContentView:Lorg/chromium/content/browser/ContentView;

    new-instance v2, Lorg/chromium/content/browser/LoadUrlParams;

    invoke-direct {v2, v0}, Lorg/chromium/content/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lorg/chromium/content/browser/ContentView;->loadUrl(Lorg/chromium/content/browser/LoadUrlParams;)V

    goto :goto_0
.end method

.method public removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/chrome/browser/TabBase;->mObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method
