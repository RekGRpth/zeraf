.class public Lorg/chromium/chrome/browser/ProcessUtils;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native nativeToggleWebKitSharedTimers(Z)V
.end method

.method public static toggleWebKitSharedTimers(Z)V
    .locals 0

    invoke-static {p0}, Lorg/chromium/chrome/browser/ProcessUtils;->nativeToggleWebKitSharedTimers(Z)V

    return-void
.end method
