.class public Lorg/chromium/content/app/LibraryLoader;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAG:Ljava/lang/String; = "LibraryLoader"

.field private static sCheckThreadLock:Ljava/lang/Object;

.field private static sInitialized:Z

.field private static sLibrary:Ljava/lang/String;

.field private static sLoaded:Ljava/lang/Boolean;

.field private static sLoadedLock:Ljava/lang/Object;

.field private static sMyThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lorg/chromium/content/app/LibraryLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/app/LibraryLoader;->$assertionsDisabled:Z

    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/content/app/LibraryLoader;->sLibrary:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/content/app/LibraryLoader;->sLoadedLock:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lorg/chromium/content/app/LibraryLoader;->sLoaded:Ljava/lang/Boolean;

    sput-boolean v1, Lorg/chromium/content/app/LibraryLoader;->sInitialized:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/content/app/LibraryLoader;->sCheckThreadLock:Ljava/lang/Object;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkThreadUsage()V
    .locals 6

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lorg/chromium/content/app/LibraryLoader;->sCheckThreadLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lorg/chromium/content/app/LibraryLoader;->sMyThread:Ljava/lang/Thread;

    if-nez v2, :cond_1

    sput-object v0, Lorg/chromium/content/app/LibraryLoader;->sMyThread:Ljava/lang/Thread;

    :cond_0
    monitor-exit v1

    return-void

    :cond_1
    sget-object v2, Lorg/chromium/content/app/LibraryLoader;->sMyThread:Ljava/lang/Thread;

    if-eq v2, v0, :cond_0

    const-string v2, "LibraryLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Threading violation detected. My thread="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lorg/chromium/content/app/LibraryLoader;->sMyThread:Ljava/lang/Thread;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lorg/chromium/content/app/LibraryLoader;->sMyThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " but I\'m being accessed from thread="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lorg/chromium/content/app/LibraryLoader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static ensureInitialized()V
    .locals 1

    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->checkThreadUsage()V

    sget-boolean v0, Lorg/chromium/content/app/LibraryLoader;->sInitialized:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->loadNow()V

    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->initializeOnMainThread()V

    goto :goto_0
.end method

.method public static getLibraryToLoad()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/chromium/content/app/LibraryLoader;->sLibrary:Ljava/lang/String;

    return-object v0
.end method

.method private static initializeOnMainThread()V
    .locals 1

    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->checkThreadUsage()V

    sget-boolean v0, Lorg/chromium/content/app/LibraryLoader;->sInitialized:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getJavaSwitchesOrNull()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/app/LibraryLoader;->initializeOnMainThread([Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static initializeOnMainThread([Ljava/lang/String;)V
    .locals 3

    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->checkThreadUsage()V

    sget-boolean v0, Lorg/chromium/content/app/LibraryLoader;->sInitialized:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lorg/chromium/content/app/LibraryLoader;->nativeLibraryLoadedOnMainThread([Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    const-string v1, "LibraryLoader"

    const-string v2, "error calling nativeLibraryLoadedOnMainThread"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lorg/chromium/content/common/ProcessInitException;

    invoke-direct {v1, v0}, Lorg/chromium/content/common/ProcessInitException;-><init>(I)V

    throw v1

    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lorg/chromium/content/app/LibraryLoader;->sInitialized:Z

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->enableNativeProxy()V

    invoke-static {}, Lorg/chromium/content/common/TraceEvent;->setEnabledToMatchNative()V

    goto :goto_0
.end method

.method public static loadNow()V
    .locals 4

    sget-object v0, Lorg/chromium/content/app/LibraryLoader;->sLibrary:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-boolean v0, Lorg/chromium/content/app/LibraryLoader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No library specified to load.  Call setLibraryToLoad before first."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    :try_start_0
    sget-object v1, Lorg/chromium/content/app/LibraryLoader;->sLoadedLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget-object v0, Lorg/chromium/content/app/LibraryLoader;->sLoaded:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lorg/chromium/content/app/LibraryLoader;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lorg/chromium/content/app/LibraryLoader;->sInitialized:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/chromium/content/common/ProcessInitException;

    const/4 v2, 0x5

    invoke-direct {v1, v2, v0}, Lorg/chromium/content/common/ProcessInitException;-><init>(ILjava/lang/Throwable;)V

    throw v1

    :cond_1
    :try_start_3
    const-string v0, "LibraryLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "loading: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lorg/chromium/content/app/LibraryLoader;->sLibrary:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lorg/chromium/content/app/LibraryLoader;->sLibrary:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    const-string v0, "LibraryLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "loaded: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lorg/chromium/content/app/LibraryLoader;->sLibrary:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lorg/chromium/content/app/LibraryLoader;->sLoaded:Ljava/lang/Boolean;

    :cond_2
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method private static native nativeLibraryLoadedOnMainThread([Ljava/lang/String;)I
.end method

.method public static setLibraryToLoad(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lorg/chromium/content/app/LibraryLoader;->sLibrary:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lorg/chromium/content/app/LibraryLoader;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lorg/chromium/content/app/LibraryLoader;->sLoaded:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Setting the library must happen before load is called."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    sput-object p0, Lorg/chromium/content/app/LibraryLoader;->sLibrary:Ljava/lang/String;

    goto :goto_0
.end method
