.class public Lorg/chromium/content/browser/SandboxedProcessLauncher;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final MAX_REGISTERED_SERVICES:I = 0x6

.field private static final NULL_PROCESS_HANDLE:I

.field private static TAG:Ljava/lang/String;

.field private static final mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

.field private static final mFreeConnectionIndices:Ljava/util/ArrayList;

.field private static mServiceMap:Ljava/util/Map;

.field static mSpareConnection:Lorg/chromium/content/browser/SandboxedProcessConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x6

    const-class v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->$assertionsDisabled:Z

    const-string v0, "SandboxedProcessLauncher"

    sput-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    new-array v0, v3, [Lorg/chromium/content/browser/SandboxedProcessConnection;

    sput-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mFreeConnectionIndices:Ljava/util/ArrayList;

    :goto_1
    if-ge v1, v3, :cond_1

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mFreeConnectionIndices:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mServiceMap:Ljava/util/Map;

    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mSpareConnection:Lorg/chromium/content/browser/SandboxedProcessConnection;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mServiceMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/SandboxedProcessConnection;)V
    .locals 0

    invoke-static {p0}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->freeConnection(Lorg/chromium/content/browser/SandboxedProcessConnection;)V

    return-void
.end method

.method static synthetic access$300(II)V
    .locals 0

    invoke-static {p0, p1}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->nativeOnSandboxedProcessStarted(II)V

    return-void
.end method

.method private static allocateBoundConnection(Landroid/content/Context;[Ljava/lang/String;)Lorg/chromium/content/browser/SandboxedProcessConnection;
    .locals 3

    invoke-static {p0}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->allocateConnection(Landroid/content/Context;)Lorg/chromium/content/browser/SandboxedProcessConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->getLibraryToLoad()Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lorg/chromium/content/browser/SandboxedProcessLauncher;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Attempting to launch a sandbox process without first calling LibraryLoader.setLibraryToLoad"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    invoke-virtual {v0, v1, p1}, Lorg/chromium/content/browser/SandboxedProcessConnection;->bind(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_1
    return-object v0
.end method

.method private static allocateConnection(Landroid/content/Context;)Lorg/chromium/content/browser/SandboxedProcessConnection;
    .locals 5

    new-instance v1, Lorg/chromium/content/browser/SandboxedProcessLauncher$1;

    invoke-direct {v1}, Lorg/chromium/content/browser/SandboxedProcessLauncher$1;-><init>()V

    sget-object v2, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mFreeConnectionIndices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    const-string v1, "Ran out of sandboxed services."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    monitor-exit v2

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mFreeConnectionIndices:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-boolean v3, Lorg/chromium/content/browser/SandboxedProcessLauncher;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    sget-object v3, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    sget-object v3, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    new-instance v4, Lorg/chromium/content/browser/SandboxedProcessConnection;

    invoke-direct {v4, p0, v0, v1}, Lorg/chromium/content/browser/SandboxedProcessConnection;-><init>(Landroid/content/Context;ILorg/chromium/content/browser/SandboxedProcessConnection$DeathCallback;)V

    aput-object v4, v3, v0

    sget-object v1, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    aget-object v0, v1, v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method static bindAsHighPriority(I)V
    .locals 3

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mServiceMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/browser/SandboxedProcessConnection;

    if-nez v0, :cond_0

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tried to bind a non-existent connection to pid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->bindHighPriority()V

    goto :goto_0
.end method

.method private static createCallback()Lorg/chromium/content/common/ISandboxedProcessCallback;
    .locals 1

    new-instance v0, Lorg/chromium/content/browser/SandboxedProcessLauncher$3;

    invoke-direct {v0}, Lorg/chromium/content/browser/SandboxedProcessLauncher$3;-><init>()V

    return-object v0
.end method

.method static establishSurfacePeer(IILandroid/view/Surface;II)V
    .locals 4

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "establishSurfaceTexturePeer: pid = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", primaryID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", secondaryID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->getSandboxedService(I)Lorg/chromium/content/common/ISandboxedProcessService;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    const-string v1, "Unable to get SandboxedProcessService from pid."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-interface {v0, p1, p2, p3, p4}, Lorg/chromium/content/common/ISandboxedProcessService;->setSurface(ILandroid/view/Surface;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to call setSurface: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static freeConnection(Lorg/chromium/content/browser/SandboxedProcessConnection;)V
    .locals 6

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->getServiceNumber()I

    move-result v1

    sget-object v2, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    aget-object v0, v0, v1

    if-eq v0, p0, :cond_2

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_1
    sget-object v3, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to find connection to free in slot: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " already occupied by service: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->getServiceNumber()I

    move-result v0

    goto :goto_1

    :cond_2
    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    const/4 v3, 0x0

    aput-object v3, v0, v1

    sget-boolean v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mFreeConnectionIndices:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mFreeConnectionIndices:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static getNumberOfConnections()I
    .locals 2

    sget-object v1, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mConnections:[Lorg/chromium/content/browser/SandboxedProcessConnection;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mFreeConnectionIndices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getSandboxedService(I)Lorg/chromium/content/common/ISandboxedProcessService;
    .locals 2

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mServiceMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/browser/SandboxedProcessConnection;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->getService()Lorg/chromium/content/common/ISandboxedProcessService;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native nativeOnSandboxedProcessStarted(II)V
.end method

.method static start(Landroid/content/Context;[Ljava/lang/String;[I[I[ZI)V
    .locals 7

    const/4 v1, 0x0

    sget-boolean v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    array-length v0, p2

    array-length v2, p3

    if-ne v0, v2, :cond_0

    array-length v0, p3

    array-length v2, p4

    if-eq v0, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    array-length v0, p3

    new-array v2, v0, [Lorg/chromium/content/browser/FileDescriptorInfo;

    move v0, v1

    :goto_0
    array-length v3, p3

    if-ge v0, v3, :cond_2

    new-instance v3, Lorg/chromium/content/browser/FileDescriptorInfo;

    aget v4, p2, v0

    aget v5, p3, v0

    aget-boolean v6, p4, v0

    invoke-direct {v3, v4, v5, v6}, Lorg/chromium/content/browser/FileDescriptorInfo;-><init>(IIZ)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    sget-boolean v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p5, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    const-class v3, Lorg/chromium/content/browser/SandboxedProcessLauncher;

    monitor-enter v3

    :try_start_0
    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mSpareConnection:Lorg/chromium/content/browser/SandboxedProcessConnection;

    const/4 v4, 0x0

    sput-object v4, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mSpareConnection:Lorg/chromium/content/browser/SandboxedProcessConnection;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_4

    invoke-static {p0, p1}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->allocateBoundConnection(Landroid/content/Context;[Ljava/lang/String;)Lorg/chromium/content/browser/SandboxedProcessConnection;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {p5, v1}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->nativeOnSandboxedProcessStarted(II)V

    :goto_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_4
    sget-object v1, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting up connection to process: slot="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->getServiceNumber()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lorg/chromium/content/browser/SandboxedProcessLauncher$2;

    invoke-direct {v1, v0, p5}, Lorg/chromium/content/browser/SandboxedProcessLauncher$2;-><init>(Lorg/chromium/content/browser/SandboxedProcessConnection;I)V

    invoke-static {}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->createCallback()Lorg/chromium/content/common/ISandboxedProcessCallback;

    move-result-object v3

    invoke-virtual {v0, p1, v2, v3, v1}, Lorg/chromium/content/browser/SandboxedProcessConnection;->setupConnection([Ljava/lang/String;[Lorg/chromium/content/browser/FileDescriptorInfo;Lorg/chromium/content/common/ISandboxedProcessCallback;Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method static stop(I)V
    .locals 3

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stopping sandboxed connection: pid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mServiceMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/browser/SandboxedProcessConnection;

    if-nez v0, :cond_0

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tried to stop non-existent connection to pid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->unbind()V

    invoke-static {v0}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->freeConnection(Lorg/chromium/content/browser/SandboxedProcessConnection;)V

    goto :goto_0
.end method

.method static unbindAsHighPriority(I)V
    .locals 3

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mServiceMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/content/browser/SandboxedProcessConnection;

    if-nez v0, :cond_0

    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tried to unbind non-existent connection to pid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/SandboxedProcessConnection;->unbindHighPriority(Z)V

    goto :goto_0
.end method

.method public static declared-synchronized warmUp(Landroid/content/Context;)V
    .locals 2

    const-class v1, Lorg/chromium/content/browser/SandboxedProcessLauncher;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    sget-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mSpareConnection:Lorg/chromium/content/browser/SandboxedProcessConnection;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/chromium/content/browser/SandboxedProcessLauncher;->allocateBoundConnection(Landroid/content/Context;[Ljava/lang/String;)Lorg/chromium/content/browser/SandboxedProcessConnection;

    move-result-object v0

    sput-object v0, Lorg/chromium/content/browser/SandboxedProcessLauncher;->mSpareConnection:Lorg/chromium/content/browser/SandboxedProcessConnection;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit v1

    return-void
.end method
