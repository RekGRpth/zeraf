.class public Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/chromium/content/browser/ContentVideoViewContextDelegate;


# instance fields
.field private mActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;->mActivity:Landroid/app/Activity;

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public getErrorButton()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;->mActivity:Landroid/app/Activity;

    sget v1, Lorg/chromium/content/R$string;->media_player_error_button:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getErrorTitle()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;->mActivity:Landroid/app/Activity;

    sget v1, Lorg/chromium/content/R$string;->media_player_error_title:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayBackErrorText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;->mActivity:Landroid/app/Activity;

    sget v1, Lorg/chromium/content/R$string;->media_player_error_text_invalid_progressive_playback:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnknownErrorText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;->mActivity:Landroid/app/Activity;

    sget v1, Lorg/chromium/content/R$string;->media_player_error_text_unknown:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoLoadingText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;->mActivity:Landroid/app/Activity;

    sget v1, Lorg/chromium/content/R$string;->media_player_loading_video:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyContentVideoView()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method public onShowCustomView(Landroid/view/View;)V
    .locals 4

    const/16 v1, 0x400

    const/4 v3, -0x1

    iget-object v0, p0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    iget-object v0, p0, Lorg/chromium/content/browser/ActivityContentVideoViewDelegate;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x11

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, p1, v1}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
