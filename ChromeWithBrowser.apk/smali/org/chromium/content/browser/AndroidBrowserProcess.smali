.class public Lorg/chromium/content/browser/AndroidBrowserProcess;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# static fields
.field public static final MAX_RENDERERS_AUTOMATIC:I = -0x1

.field public static final MAX_RENDERERS_LIMIT:I = 0x3

.field public static final MAX_RENDERERS_SINGLE_PROCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "BrowserProcessMain"

.field private static sInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lorg/chromium/content/browser/AndroidBrowserProcess;->sInitialized:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Landroid/content/Context;I)Z
    .locals 7

    const/4 v0, 0x1

    sget-boolean v1, Lorg/chromium/content/browser/AndroidBrowserProcess;->sInitialized:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    return v0

    :cond_1
    sput-boolean v0, Lorg/chromium/content/browser/AndroidBrowserProcess;->sInitialized:Z

    invoke-static {p0}, Lorg/chromium/content/browser/ResourceExtractor;->get(Landroid/content/Context;)Lorg/chromium/content/browser/ResourceExtractor;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ResourceExtractor;->startExtractingResources()V

    invoke-static {}, Lorg/chromium/content/app/LibraryLoader;->ensureInitialized()V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, Lorg/chromium/content/browser/AndroidBrowserProcess;->normalizeMaxRendererProcesses(Landroid/content/Context;I)I

    move-result v3

    const-string v4, "BrowserProcessMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Initializing chromium process, renderers="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lorg/chromium/content/browser/ResourceExtractor;->waitForCompletion()V

    invoke-static {v3}, Lorg/chromium/content/browser/AndroidBrowserProcess;->nativeSetCommandLineFlags(I)V

    invoke-static {v2}, Lorg/chromium/content/app/ContentMain;->initApplicationContext(Landroid/content/Context;)V

    invoke-static {}, Lorg/chromium/content/app/ContentMain;->start()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v0, Lorg/chromium/content/common/ProcessInitException;

    invoke-direct {v0, v1}, Lorg/chromium/content/common/ProcessInitException;-><init>(I)V

    throw v0
.end method

.method public static initChromiumBrowserProcessForTests(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lorg/chromium/content/browser/ResourceExtractor;->get(Landroid/content/Context;)Lorg/chromium/content/browser/ResourceExtractor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ResourceExtractor;->startExtractingResources()V

    invoke-virtual {v0}, Lorg/chromium/content/browser/ResourceExtractor;->waitForCompletion()V

    const/4 v0, 0x1

    invoke-static {v0}, Lorg/chromium/content/browser/AndroidBrowserProcess;->nativeSetCommandLineFlags(I)V

    return-void
.end method

.method private static native nativeIsOfficialBuild()Z
.end method

.method private static native nativeSetCommandLineFlags(I)V
.end method

.method private static normalizeMaxRendererProcesses(Landroid/content/Context;I)I
    .locals 4

    const/4 v1, 0x3

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    add-int/lit8 v0, v0, -0x8

    div-int/lit8 v0, v0, 0x8

    const/4 v2, 0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result p1

    :cond_0
    if-le p1, v1, :cond_1

    const-string v0, "BrowserProcessMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Excessive maxRendererProcesses value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method
