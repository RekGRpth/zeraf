.class Lorg/chromium/content/browser/InputDialogContainer$DateListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field private final mDialogType:I

.field final synthetic this$0:Lorg/chromium/content/browser/InputDialogContainer;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/InputDialogContainer;I)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/InputDialogContainer$DateListener;->this$0:Lorg/chromium/content/browser/InputDialogContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lorg/chromium/content/browser/InputDialogContainer$DateListener;->mDialogType:I

    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 8

    const/4 v5, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/InputDialogContainer$DateListener;->this$0:Lorg/chromium/content/browser/InputDialogContainer;

    # getter for: Lorg/chromium/content/browser/InputDialogContainer;->mDialogAlreadyDismissed:Z
    invoke-static {v0}, Lorg/chromium/content/browser/InputDialogContainer;->access$000(Lorg/chromium/content/browser/InputDialogContainer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/InputDialogContainer$DateListener;->this$0:Lorg/chromium/content/browser/InputDialogContainer;

    iget v1, p0, Lorg/chromium/content/browser/InputDialogContainer$DateListener;->mDialogType:I

    const-string v7, "%Y-%m-%d"

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, v5

    # invokes: Lorg/chromium/content/browser/InputDialogContainer;->setFieldDateTimeValue(IIIIIILjava/lang/String;)V
    invoke-static/range {v0 .. v7}, Lorg/chromium/content/browser/InputDialogContainer;->access$200(Lorg/chromium/content/browser/InputDialogContainer;IIIIIILjava/lang/String;)V

    :cond_0
    return-void
.end method
