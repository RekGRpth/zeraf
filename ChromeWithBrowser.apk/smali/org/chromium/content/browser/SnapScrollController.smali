.class Lorg/chromium/content/browser/SnapScrollController;
.super Ljava/lang/Object;


# static fields
.field private static CHANNEL_DISTANCE:F = 0.0f

.field private static final SNAP_BOUND:I = 0x10

.field private static final SNAP_HORIZ:I = 0x1

.field private static final SNAP_NONE:I = 0x0

.field private static final SNAP_VERT:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDistanceX:F

.field private mDistanceY:F

.field private mFirstTouchX:I

.field private mFirstTouchY:I

.field private mSnapScrollMode:I

.field private mZoomManager:Lorg/chromium/content/browser/ZoomManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/chromium/content/browser/SnapScrollController;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/chromium/content/browser/SnapScrollController;->TAG:Ljava/lang/String;

    const/high16 v0, 0x41800000

    sput v0, Lorg/chromium/content/browser/SnapScrollController;->CHANNEL_DISTANCE:F

    return-void
.end method

.method constructor <init>(Lorg/chromium/content/browser/ZoomManager;)V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    iput v2, p0, Lorg/chromium/content/browser/SnapScrollController;->mFirstTouchX:I

    iput v2, p0, Lorg/chromium/content/browser/SnapScrollController;->mFirstTouchY:I

    iput v1, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceX:F

    iput v1, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceY:F

    iput-object p1, p0, Lorg/chromium/content/browser/SnapScrollController;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    return-void
.end method


# virtual methods
.method isSnapHorizontal()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isSnapVertical()Z
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isSnappingScrolls()Z
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method resetSnapScrollMode()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    return-void
.end method

.method setSnapScrollingMode(Landroid/view/MotionEvent;)V
    .locals 4

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/16 v3, 0x10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lorg/chromium/content/browser/SnapScrollController;->TAG:Ljava/lang/String;

    const-string v1, "setSnapScrollingMode case-default no-op"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mFirstTouchX:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mFirstTouchY:I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->isScaleGestureDetectionInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lorg/chromium/content/browser/SnapScrollController;->mFirstTouchX:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lorg/chromium/content/browser/SnapScrollController;->mFirstTouchY:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-int v1, v1

    if-le v0, v3, :cond_1

    if-ge v1, v3, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    goto :goto_0

    :cond_1
    if-ge v0, v3, :cond_0

    if-le v1, v3, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    goto :goto_0

    :pswitch_2
    iput v2, p0, Lorg/chromium/content/browser/SnapScrollController;->mFirstTouchX:I

    iput v2, p0, Lorg/chromium/content/browser/SnapScrollController;->mFirstTouchY:I

    iput v1, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceX:F

    iput v1, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceY:F

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method updateSnapScrollMode(FF)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceX:F

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceX:F

    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceY:F

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceY:F

    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    if-ne v0, v4, :cond_3

    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceY:F

    sget v1, Lorg/chromium/content/browser/SnapScrollController;->CHANNEL_DISTANCE:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iput v3, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceX:F

    sget v1, Lorg/chromium/content/browser/SnapScrollController;->CHANNEL_DISTANCE:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iput v2, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceX:F

    iput v2, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceY:F

    goto :goto_0

    :cond_3
    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceX:F

    sget v1, Lorg/chromium/content/browser/SnapScrollController;->CHANNEL_DISTANCE:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    iput v3, p0, Lorg/chromium/content/browser/SnapScrollController;->mSnapScrollMode:I

    goto :goto_0

    :cond_4
    iget v0, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceY:F

    sget v1, Lorg/chromium/content/browser/SnapScrollController;->CHANNEL_DISTANCE:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iput v2, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceX:F

    iput v2, p0, Lorg/chromium/content/browser/SnapScrollController;->mDistanceY:F

    goto :goto_0
.end method
