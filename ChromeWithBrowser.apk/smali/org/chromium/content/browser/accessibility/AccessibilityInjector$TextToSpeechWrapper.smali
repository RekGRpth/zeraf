.class Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;
.super Ljava/lang/Object;


# instance fields
.field private mSelfBrailleClient:Lcom/a/a/a/a/d;

.field private mTextToSpeech:Landroid/speech/tts/TextToSpeech;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mView:Landroid/view/View;

    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-direct {v0, p2, v1, v1}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    new-instance v0, Lcom/a/a/a/a/d;

    invoke-static {}, Lorg/chromium/content/common/CommandLine;->getInstance()Lorg/chromium/content/common/CommandLine;

    move-result-object v1

    const-string v2, "debug-braille-service"

    invoke-virtual {v1, v2}, Lorg/chromium/content/common/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {v0, p2, v1}, Lcom/a/a/a/a/d;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mSelfBrailleClient:Lcom/a/a/a/a/d;

    return-void
.end method


# virtual methods
.method public braille(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lorg/chromium/content/browser/JavascriptInterface;
    .end annotation

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mView:Landroid/view/View;

    invoke-static {v1}, Lcom/a/a/a/a/g;->a(Landroid/view/View;)Lcom/a/a/a/a/g;

    move-result-object v1

    const-string v2, "text"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/a/a/a/a/g;->a(Ljava/lang/CharSequence;)Lcom/a/a/a/a/g;

    const-string v2, "startIndex"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/a/a/a/a/g;->a(I)Lcom/a/a/a/a/g;

    const-string v2, "endIndex"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/a/a/a/a/g;->b(I)Lcom/a/a/a/a/g;

    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mSelfBrailleClient:Lcom/a/a/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/a/a/d;->a(Lcom/a/a/a/a/g;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    # getter for: Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->TAG:Ljava/lang/String;
    invoke-static {}, Lorg/chromium/content/browser/accessibility/AccessibilityInjector;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error parsing JS JSON object"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isSpeaking()Z
    .locals 1
    .annotation runtime Lorg/chromium/content/browser/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    return v0
.end method

.method protected shutdownInternal()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mSelfBrailleClient:Lcom/a/a/a/a/d;

    invoke-virtual {v0}, Lcom/a/a/a/a/d;->a()V

    return-void
.end method

.method public speak(Ljava/lang/String;ILjava/util/HashMap;)I
    .locals 1
    .annotation runtime Lorg/chromium/content/browser/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, p1, p2, p3}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    move-result v0

    return v0
.end method

.method public stop()I
    .locals 1
    .annotation runtime Lorg/chromium/content/browser/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    move-result v0

    return v0
.end method
