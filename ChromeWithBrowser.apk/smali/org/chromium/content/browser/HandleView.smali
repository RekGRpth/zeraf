.class Lorg/chromium/content/browser/HandleView;
.super Landroid/view/View;


# static fields
.field static final CENTER:I = 0x1

.field private static final FADE_DURATION:F = 200.0f

.field static final LEFT:I = 0x0

.field private static final LINE_OFFSET_Y_DIP:F = 5.0f

.field static final RIGHT:I = 0x2

.field private static final TEXT_VIEW_HANDLE_ATTRS:[I


# instance fields
.field private mAlpha:F

.field private final mContainer:Landroid/widget/PopupWindow;

.field private mContainerPositionX:I

.field private mContainerPositionY:I

.field private final mController:Lorg/chromium/content/browser/CursorController;

.field private mDownPositionX:F

.field private mDownPositionY:F

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mFadeStartTime:J

.field private mHotspotX:F

.field private mHotspotY:F

.field private mIsDragging:Z

.field private mIsInsertionHandle:Z

.field private mLastParentX:I

.field private mLastParentY:I

.field private mLineOffsetY:I

.field private mParent:Landroid/view/View;

.field private mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

.field private mPositionX:I

.field private mPositionY:I

.field private mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

.field private mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

.field private mSelectHandleRight:Landroid/graphics/drawable/Drawable;

.field private final mTempCoords:[I

.field private final mTempRect:Landroid/graphics/Rect;

.field private final mTextSelectHandleLeftRes:I

.field private final mTextSelectHandleRes:I

.field private final mTextSelectHandleRightRes:I

.field private mTouchTimer:J

.field private mTouchToWindowOffsetX:F

.field private mTouchToWindowOffsetY:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/chromium/content/browser/HandleView;->TEXT_VIEW_HANDLE_ATTRS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x10102c5
        0x10102c7
        0x10102c6
    .end array-data
.end method

.method constructor <init>(Lorg/chromium/content/browser/CursorController;ILandroid/view/View;)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-boolean v4, p0, Lorg/chromium/content/browser/HandleView;->mIsInsertionHandle:Z

    new-array v0, v6, [I

    iput-object v0, p0, Lorg/chromium/content/browser/HandleView;->mTempCoords:[I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/HandleView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object p3, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    iput-object p1, p0, Lorg/chromium/content/browser/HandleView;->mController:Lorg/chromium/content/browser/CursorController;

    new-instance v1, Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    const v3, 0x10102c8

    invoke-direct {v1, v0, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lorg/chromium/content/browser/HandleView;->mContainer:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setSplitTouchEnabled(Z)V

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    sget-object v1, Lorg/chromium/content/browser/HandleView;->TEXT_VIEW_HANDLE_ATTRS:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lorg/chromium/content/browser/HandleView;->mTextSelectHandleLeftRes:I

    invoke-virtual {v1, v5}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lorg/chromium/content/browser/HandleView;->mTextSelectHandleRes:I

    invoke-virtual {v1, v6}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lorg/chromium/content/browser/HandleView;->mTextSelectHandleRightRes:I

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, p2}, Lorg/chromium/content/browser/HandleView;->setOrientation(I)V

    const/high16 v1, 0x40a00000

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v5, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mLineOffsetY:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mAlpha:F

    return-void
.end method

.method private isPositionVisible()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lorg/chromium/content/browser/HandleView;->mIsDragging:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lorg/chromium/content/browser/HandleView;->mTempRect:Landroid/graphics/Rect;

    iput v1, v2, Landroid/graphics/Rect;->left:I

    iput v1, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v4, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v2, v5}, Landroid/view/ViewParent;->getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lorg/chromium/content/browser/HandleView;->mTempCoords:[I

    iget-object v4, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->getLocationInWindow([I)V

    aget v4, v3, v1

    iget v5, p0, Lorg/chromium/content/browser/HandleView;->mPositionX:I

    add-int/2addr v4, v5

    iget v5, p0, Lorg/chromium/content/browser/HandleView;->mHotspotX:F

    float-to-int v5, v5

    add-int/2addr v4, v5

    aget v3, v3, v0

    iget v5, p0, Lorg/chromium/content/browser/HandleView;->mPositionY:I

    add-int/2addr v3, v5

    iget v5, p0, Lorg/chromium/content/browser/HandleView;->mHotspotY:F

    float-to-int v5, v5

    add-int/2addr v3, v5

    iget v5, v2, Landroid/graphics/Rect;->left:I

    if-lt v4, v5, :cond_4

    iget v5, v2, Landroid/graphics/Rect;->right:I

    if-gt v4, v5, :cond_4

    iget v4, v2, Landroid/graphics/Rect;->top:I

    if-lt v3, v4, :cond_4

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-le v3, v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private updateAlpha()V
    .locals 5

    const/high16 v4, 0x3f800000

    iget v0, p0, Lorg/chromium/content/browser/HandleView;->mAlpha:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lorg/chromium/content/browser/HandleView;->mFadeStartTime:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    const/high16 v1, 0x43480000

    div-float/2addr v0, v1

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mAlpha:F

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000

    iget v2, p0, Lorg/chromium/content/browser/HandleView;->mAlpha:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->invalidate()V

    goto :goto_0
.end method

.method private updatePosition(FF)V
    .locals 3

    iget v0, p0, Lorg/chromium/content/browser/HandleView;->mTouchToWindowOffsetX:F

    sub-float v0, p1, v0

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mHotspotX:F

    add-float/2addr v0, v1

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mTouchToWindowOffsetY:F

    sub-float v1, p2, v1

    iget v2, p0, Lorg/chromium/content/browser/HandleView;->mHotspotY:F

    add-float/2addr v1, v2

    iget v2, p0, Lorg/chromium/content/browser/HandleView;->mLineOffsetY:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lorg/chromium/content/browser/HandleView;->mController:Lorg/chromium/content/browser/CursorController;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-interface {v2, p0, v0, v1}, Lorg/chromium/content/browser/CursorController;->updatePosition(Lorg/chromium/content/browser/HandleView;II)V

    return-void
.end method


# virtual methods
.method beginFadeIn()V
    .locals 2

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mAlpha:F

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/content/browser/HandleView;->mFadeStartTime:J

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/HandleView;->setVisibility(I)V

    goto :goto_0
.end method

.method getAdjustedPositionX()I
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/HandleView;->mPositionX:I

    int-to-float v0, v0

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mHotspotX:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method getAdjustedPositionY()I
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/HandleView;->mPositionY:I

    int-to-float v0, v0

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mHotspotY:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method getLineAdjustedPositionY()I
    .locals 2

    iget v0, p0, Lorg/chromium/content/browser/HandleView;->mPositionY:I

    int-to-float v0, v0

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mHotspotY:F

    add-float/2addr v0, v1

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mLineOffsetY:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method getPositionX()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/HandleView;->mPositionX:I

    return v0
.end method

.method getPositionY()I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/HandleView;->mPositionY:I

    return v0
.end method

.method hide()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/HandleView;->mIsDragging:Z

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->hide()V

    :cond_0
    return-void
.end method

.method isDragging()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/HandleView;->mIsDragging:Z

    return v0
.end method

.method isShowing()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method moveTo(II)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    iput p1, p0, Lorg/chromium/content/browser/HandleView;->mPositionX:I

    iput p2, p0, Lorg/chromium/content/browser/HandleView;->mPositionY:I

    invoke-direct {p0}, Lorg/chromium/content/browser/HandleView;->isPositionVisible()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mTempCoords:[I

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    aget v1, v0, v7

    iget v2, p0, Lorg/chromium/content/browser/HandleView;->mPositionX:I

    add-int/2addr v1, v2

    aget v2, v0, v8

    iget v3, p0, Lorg/chromium/content/browser/HandleView;->mPositionY:I

    add-int/2addr v2, v3

    iget v3, p0, Lorg/chromium/content/browser/HandleView;->mContainerPositionX:I

    if-ne v1, v3, :cond_0

    iget v3, p0, Lorg/chromium/content/browser/HandleView;->mContainerPositionY:I

    if-eq v2, v3, :cond_1

    :cond_0
    iput v1, p0, Lorg/chromium/content/browser/HandleView;->mContainerPositionX:I

    iput v2, p0, Lorg/chromium/content/browser/HandleView;->mContainerPositionY:I

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mContainer:Landroid/widget/PopupWindow;

    iget v2, p0, Lorg/chromium/content/browser/HandleView;->mContainerPositionX:I

    iget v3, p0, Lorg/chromium/content/browser/HandleView;->mContainerPositionY:I

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getRight()I

    move-result v4

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getLeft()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getBottom()I

    move-result v5

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getTop()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/PopupWindow;->update(IIII)V

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v1}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->hide()V

    :cond_1
    :goto_0
    iget-boolean v1, p0, Lorg/chromium/content/browser/HandleView;->mIsDragging:Z

    if-eqz v1, :cond_5

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mTempCoords:[I

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    :cond_2
    aget v1, v0, v7

    iget v2, p0, Lorg/chromium/content/browser/HandleView;->mLastParentX:I

    if-ne v1, v2, :cond_3

    aget v1, v0, v8

    iget v2, p0, Lorg/chromium/content/browser/HandleView;->mLastParentY:I

    if-eq v1, v2, :cond_4

    :cond_3
    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mTouchToWindowOffsetX:F

    aget v2, v0, v7

    iget v3, p0, Lorg/chromium/content/browser/HandleView;->mLastParentX:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p0, Lorg/chromium/content/browser/HandleView;->mTouchToWindowOffsetX:F

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mTouchToWindowOffsetY:F

    aget v2, v0, v8

    iget v3, p0, Lorg/chromium/content/browser/HandleView;->mLastParentY:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p0, Lorg/chromium/content/browser/HandleView;->mTouchToWindowOffsetY:F

    aget v1, v0, v7

    iput v1, p0, Lorg/chromium/content/browser/HandleView;->mLastParentX:I

    aget v0, v0, v8

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mLastParentY:I

    :cond_4
    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->hide()V

    :cond_5
    :goto_1
    return-void

    :cond_6
    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->show()V

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->hide()V

    goto :goto_1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Lorg/chromium/content/browser/HandleView;->updateAlpha()V

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getBottom()I

    move-result v2

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lorg/chromium/content/browser/HandleView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v5

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mDownPositionX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mDownPositionY:F

    iget v0, p0, Lorg/chromium/content/browser/HandleView;->mDownPositionX:F

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mPositionX:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mTouchToWindowOffsetX:F

    iget v0, p0, Lorg/chromium/content/browser/HandleView;->mDownPositionY:F

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mPositionY:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mTouchToWindowOffsetY:F

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mTempCoords:[I

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    aget v1, v0, v4

    iput v1, p0, Lorg/chromium/content/browser/HandleView;->mLastParentX:I

    aget v0, v0, v5

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mLastParentY:I

    iput-boolean v5, p0, Lorg/chromium/content/browser/HandleView;->mIsDragging:Z

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mController:Lorg/chromium/content/browser/CursorController;

    invoke-interface {v0, p0}, Lorg/chromium/content/browser/CursorController;->beforeStartUpdatingPosition(Lorg/chromium/content/browser/HandleView;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/content/browser/HandleView;->mTouchTimer:J

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/HandleView;->updatePosition(FF)V

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lorg/chromium/content/browser/HandleView;->mIsInsertionHandle:Z

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lorg/chromium/content/browser/HandleView;->mTouchTimer:J

    sub-long/2addr v0, v2

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->hide()V

    :cond_0
    :goto_1
    iput-boolean v4, p0, Lorg/chromium/content/browser/HandleView;->mIsDragging:Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->showPastePopupWindow()V

    goto :goto_1

    :pswitch_3
    iput-boolean v4, p0, Lorg/chromium/content/browser/HandleView;->mIsDragging:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method positionAt(II)V
    .locals 3

    int-to-float v0, p1

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mHotspotX:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v1, p2

    iget v2, p0, Lorg/chromium/content/browser/HandleView;->mHotspotY:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lorg/chromium/content/browser/HandleView;->moveTo(II)V

    return-void
.end method

.method setOrientation(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mTextSelectHandleRes:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/HandleView;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mHotspotX:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/HandleView;->mIsInsertionHandle:Z

    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mHotspotY:F

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->invalidate()V

    return-void

    :pswitch_1
    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mTextSelectHandleLeftRes:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/HandleView;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    int-to-float v0, v0

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mHotspotX:F

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mTextSelectHandleRightRes:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/HandleView;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    :cond_2
    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    int-to-float v0, v0

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mHotspotX:F

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method show()V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Lorg/chromium/content/browser/HandleView;->isPositionVisible()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/chromium/content/browser/HandleView;->hide()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p0}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mTempCoords:[I

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    aget v1, v0, v4

    iget v2, p0, Lorg/chromium/content/browser/HandleView;->mPositionX:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/chromium/content/browser/HandleView;->mContainerPositionX:I

    const/4 v1, 0x1

    aget v0, v0, v1

    iget v1, p0, Lorg/chromium/content/browser/HandleView;->mPositionY:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/chromium/content/browser/HandleView;->mContainerPositionY:I

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mContainer:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mParent:Landroid/view/View;

    iget v2, p0, Lorg/chromium/content/browser/HandleView;->mContainerPositionX:I

    iget v3, p0, Lorg/chromium/content/browser/HandleView;->mContainerPositionY:I

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->hide()V

    goto :goto_0
.end method

.method showPastePopupWindow()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mController:Lorg/chromium/content/browser/CursorController;

    check-cast v0, Lorg/chromium/content/browser/InsertionHandleController;

    iget-boolean v1, p0, Lorg/chromium/content/browser/HandleView;->mIsInsertionHandle:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->canPaste()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    if-nez v1, :cond_0

    new-instance v1, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v0}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;-><init>(Lorg/chromium/content/browser/InsertionHandleController;)V

    iput-object v1, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/HandleView;->mPastePopupWindow:Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->show()V

    :cond_1
    return-void
.end method
