.class public Lorg/chromium/content/browser/ContentSettings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final MAXIMUM_FONT_SIZE:I = 0x48

.field private static final MINIMUM_FONT_SIZE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ContentSettings"

.field private static sAppCachePathIsSet:Z

.field private static final sGlobalContentSettingsLock:Ljava/lang/Object;


# instance fields
.field private mAllowFileAccessFromFileURLs:Z

.field private mAllowUniversalAccessFromFileURLs:Z

.field private mAppCacheEnabled:Z

.field private mBuiltInZoomControls:Z

.field private mCanModifySettings:Z

.field private final mContentSettingsLock:Ljava/lang/Object;

.field private mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mCursiveFontFamily:Ljava/lang/String;

.field private mDefaultFixedFontSize:I

.field private mDefaultFontSize:I

.field private mDefaultTextEncoding:Ljava/lang/String;

.field private mDisplayZoomControls:Z

.field private mDomStorageEnabled:Z

.field private final mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

.field private mFantasyFontFamily:Ljava/lang/String;

.field private mFixedFontFamily:Ljava/lang/String;

.field private mImagesEnabled:Z

.field private mIsSyncMessagePending:Z

.field private mJavaScriptCanOpenWindowsAutomatically:Z

.field private mJavaScriptEnabled:Z

.field private mLayoutAlgorithm:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

.field private mLoadsImagesAutomatically:Z

.field private mMinimumFontSize:I

.field private mMinimumLogicalFontSize:I

.field private mNativeContentSettings:I

.field private mPluginState:Landroid/webkit/WebSettings$PluginState;

.field private mSansSerifFontFamily:Ljava/lang/String;

.field private mSerifFontFamily:Ljava/lang/String;

.field private mStandardFontFamily:Ljava/lang/String;

.field private mSupportMultipleWindows:Z

.field private mSupportZoom:Z

.field private mTextSizePercent:I

.field private mUseWideViewport:Z

.field private mUserAgent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lorg/chromium/content/browser/ContentSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/content/browser/ContentSettings;->sGlobalContentSettingsLock:Ljava/lang/Object;

    sput-boolean v1, Lorg/chromium/content/browser/ContentSettings;->sAppCachePathIsSet:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method constructor <init>(Lorg/chromium/content/browser/ContentViewCore;IZ)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lorg/chromium/content/browser/ContentSettings;->mNativeContentSettings:I

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mIsSyncMessagePending:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    sget-object v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->NARROW_COLUMNS:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mLayoutAlgorithm:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    const/16 v0, 0x64

    iput v0, p0, Lorg/chromium/content/browser/ContentSettings;->mTextSizePercent:I

    const-string v0, "sans-serif"

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mStandardFontFamily:Ljava/lang/String;

    const-string v0, "monospace"

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mFixedFontFamily:Ljava/lang/String;

    const-string v0, "sans-serif"

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mSansSerifFontFamily:Ljava/lang/String;

    const-string v0, "serif"

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mSerifFontFamily:Ljava/lang/String;

    const-string v0, "cursive"

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCursiveFontFamily:Ljava/lang/String;

    const-string v0, "fantasy"

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mFantasyFontFamily:Ljava/lang/String;

    const-string v0, "Latin-1"

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultTextEncoding:Ljava/lang/String;

    iput v3, p0, Lorg/chromium/content/browser/ContentSettings;->mMinimumFontSize:I

    iput v3, p0, Lorg/chromium/content/browser/ContentSettings;->mMinimumLogicalFontSize:I

    const/16 v0, 0x10

    iput v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultFontSize:I

    const/16 v0, 0xd

    iput v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultFixedFontSize:I

    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentSettings;->mLoadsImagesAutomatically:Z

    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentSettings;->mImagesEnabled:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mJavaScriptEnabled:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mAllowUniversalAccessFromFileURLs:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mAllowFileAccessFromFileURLs:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mJavaScriptCanOpenWindowsAutomatically:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mSupportMultipleWindows:Z

    sget-object v0, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mAppCacheEnabled:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mDomStorageEnabled:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mUseWideViewport:Z

    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentSettings;->mSupportZoom:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mBuiltInZoomControls:Z

    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentSettings;->mDisplayZoomControls:Z

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isPersonalityView()Z

    move-result v0

    iput-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    invoke-direct {p0, p2, v0}, Lorg/chromium/content/browser/ContentSettings;->nativeInit(IZ)I

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/ContentSettings;->mNativeContentSettings:I

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mNativeContentSettings:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    if-eqz p3, :cond_1

    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentSettings;->mAllowUniversalAccessFromFileURLs:Z

    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentSettings;->mAllowFileAccessFromFileURLs:Z

    :cond_1
    new-instance v0, Lorg/chromium/content/browser/ContentSettings$EventHandler;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;-><init>(Lorg/chromium/content/browser/ContentSettings;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-eqz v0, :cond_2

    # getter for: Lorg/chromium/content/browser/ContentSettings$LazyDefaultUserAgent;->sInstance:Ljava/lang/String;
    invoke-static {}, Lorg/chromium/content/browser/ContentSettings$LazyDefaultUserAgent;->access$500()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mUserAgent:Ljava/lang/String;

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentSettings;->syncToNativeOnUiThread()V

    :goto_0
    return-void

    :cond_2
    iput-boolean v2, p0, Lorg/chromium/content/browser/ContentSettings;->mBuiltInZoomControls:Z

    iput-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mDisplayZoomControls:Z

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentSettings;->syncFromNativeOnUiThread()V

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lorg/chromium/content/browser/ContentSettings;->nativeGetDefaultUserAgent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/content/browser/ContentSettings;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/ContentSettings;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lorg/chromium/content/browser/ContentSettings;)Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mIsSyncMessagePending:Z

    return v0
.end method

.method static synthetic access$302(Lorg/chromium/content/browser/ContentSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mIsSyncMessagePending:Z

    return p1
.end method

.method static synthetic access$400(Lorg/chromium/content/browser/ContentSettings;)I
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mNativeContentSettings:I

    return v0
.end method

.method private clipFontSize(I)I
    .locals 1

    const/16 v0, 0x48

    if-gtz p1, :cond_1

    const/4 p1, 0x1

    :cond_0
    :goto_0
    return p1

    :cond_1
    if-le p1, v0, :cond_0

    move p1, v0

    goto :goto_0
.end method

.method private getAppCacheEnabled()Z
    .locals 2

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mAppCacheEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lorg/chromium/content/browser/ContentSettings;->sGlobalContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->sAppCachePathIsSet:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getDefaultUserAgent()Ljava/lang/String;
    .locals 1

    # getter for: Lorg/chromium/content/browser/ContentSettings$LazyDefaultUserAgent;->sInstance:Ljava/lang/String;
    invoke-static {}, Lorg/chromium/content/browser/ContentSettings$LazyDefaultUserAgent;->access$500()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPluginsDisabled()Z
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    sget-object v1, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTextAutosizingEnabled()Z
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mLayoutAlgorithm:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    sget-object v1, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native nativeGetDefaultUserAgent()Ljava/lang/String;
.end method

.method private native nativeInit(IZ)I
.end method

.method private native nativeSyncFromNative(I)V
.end method

.method private native nativeSyncToNative(I)V
.end method

.method private onNativeContentSettingsDestroyed(I)V
    .locals 1

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mNativeContentSettings:I

    if-eq v0, p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/content/browser/ContentSettings;->mNativeContentSettings:I

    return-void
.end method

.method private setPluginsDisabled(Z)V
    .locals 1

    if-eqz p1, :cond_0

    sget-object v0, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    :goto_0
    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    return-void

    :cond_0
    sget-object v0, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    goto :goto_0
.end method

.method private setTextAutosizingEnabled(Z)V
    .locals 1

    if-eqz p1, :cond_0

    sget-object v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    :goto_0
    iput-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mLayoutAlgorithm:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    return-void

    :cond_0
    sget-object v0, Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;->NARROW_COLUMNS:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    goto :goto_0
.end method


# virtual methods
.method public getAllowFileAccessFromFileURLs()Z
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mAllowFileAccessFromFileURLs:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getAllowUniversalAccessFromFileURLs()Z
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mAllowUniversalAccessFromFileURLs:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getBuiltInZoomControls()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mBuiltInZoomControls:Z

    return v0
.end method

.method public getCursiveFontFamily()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCursiveFontFamily:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getDefaultFixedFontSize()I
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultFixedFontSize:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getDefaultFontSize()I
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultFontSize:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getDefaultTextEncodingName()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultTextEncoding:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getDisplayZoomControls()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDisplayZoomControls:Z

    return v0
.end method

.method public getDomStorageEnabled()Z
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDomStorageEnabled:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getFantasyFontFamily()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mFantasyFontFamily:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getFixedFontFamily()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mFixedFontFamily:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getImagesEnabled()Z
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mImagesEnabled:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getJavaScriptCanOpenWindowsAutomatically()Z
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mJavaScriptCanOpenWindowsAutomatically:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getJavaScriptEnabled()Z
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mJavaScriptEnabled:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getLayoutAlgorithm()Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mLayoutAlgorithm:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getLoadsImagesAutomatically()Z
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mLoadsImagesAutomatically:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getMinimumFontSize()I
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mMinimumFontSize:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getMinimumLogicalFontSize()I
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mMinimumLogicalFontSize:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getPluginState()Landroid/webkit/WebSettings$PluginState;
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getPluginsEnabled()Z
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    sget-object v2, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getSansSerifFontFamily()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mSansSerifFontFamily:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getSerifFontFamily()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mSerifFontFamily:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getStandardFontFamily()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mStandardFontFamily:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getTextZoom()I
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mTextSizePercent:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getUseWideViewPort()Z
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mUseWideViewport:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getUserAgentString()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mUserAgent:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public initFrom(Lorg/chromium/content/browser/ContentSettings;)V
    .locals 1

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getLayoutAlgorithm()Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setLayoutAlgorithm(Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getTextZoom()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setTextZoom(I)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getStandardFontFamily()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setStandardFontFamily(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getFixedFontFamily()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setFixedFontFamily(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getSansSerifFontFamily()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setSansSerifFontFamily(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getSerifFontFamily()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setSerifFontFamily(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getCursiveFontFamily()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setCursiveFontFamily(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getFantasyFontFamily()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setFantasyFontFamily(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getDefaultTextEncodingName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setUserAgentString(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getMinimumFontSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setMinimumFontSize(I)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getMinimumLogicalFontSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setMinimumLogicalFontSize(I)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getDefaultFontSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setDefaultFontSize(I)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getDefaultFixedFontSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setDefaultFixedFontSize(I)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getLoadsImagesAutomatically()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setLoadsImagesAutomatically(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getImagesEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setImagesEnabled(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getJavaScriptEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getAllowUniversalAccessFromFileURLs()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getAllowFileAccessFromFileURLs()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setAllowFileAccessFromFileURLs(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getJavaScriptCanOpenWindowsAutomatically()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->supportMultipleWindows()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setSupportMultipleWindows(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getPluginState()Landroid/webkit/WebSettings$PluginState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    iget-boolean v0, p1, Lorg/chromium/content/browser/ContentSettings;->mAppCacheEnabled:Z

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setAppCacheEnabled(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getDomStorageEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setDomStorageEnabled(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->supportZoom()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setSupportZoom(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getBuiltInZoomControls()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setBuiltInZoomControls(Z)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentSettings;->getDisplayZoomControls()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setDisplayZoomControls(Z)V

    return-void
.end method

.method public setAllowFileAccessFromFileURLs(Z)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mAllowFileAccessFromFileURLs:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mAllowFileAccessFromFileURLs:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setAllowUniversalAccessFromFileURLs(Z)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mAllowUniversalAccessFromFileURLs:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mAllowUniversalAccessFromFileURLs:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setAppCacheEnabled(Z)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mAppCacheEnabled:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mAppCacheEnabled:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setAppCachePath(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x1

    sget-boolean v1, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v1, 0x0

    sget-object v2, Lorg/chromium/content/browser/ContentSettings;->sGlobalContentSettingsLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-boolean v3, Lorg/chromium/content/browser/ContentSettings;->sAppCachePathIsSet:Z

    if-nez v3, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v1, 0x1

    sput-boolean v1, Lorg/chromium/content/browser/ContentSettings;->sAppCachePathIsSet:Z

    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public setBuiltInZoomControls(Z)V
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mBuiltInZoomControls:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->sendUpdateMultiTouchMessageLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$700(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setCursiveFontFamily(Ljava/lang/String;)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCursiveFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mCursiveFontFamily:Ljava/lang/String;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setDefaultFixedFontSize(I)V
    .locals 3

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentSettings;->clipFontSize(I)I

    move-result v0

    iget v2, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultFixedFontSize:I

    if-eq v2, v0, :cond_1

    iput v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultFixedFontSize:I

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setDefaultFontSize(I)V
    .locals 3

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentSettings;->clipFontSize(I)I

    move-result v0

    iget v2, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultFontSize:I

    if-eq v2, v0, :cond_1

    iput v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultFontSize:I

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setDefaultTextEncodingName(Ljava/lang/String;)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultTextEncoding:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mDefaultTextEncoding:Ljava/lang/String;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setDisplayZoomControls(Z)V
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mDisplayZoomControls:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->sendUpdateMultiTouchMessageLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$700(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setDomStorageEnabled(Z)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDomStorageEnabled:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mDomStorageEnabled:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setFantasyFontFamily(Ljava/lang/String;)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mFantasyFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mFantasyFontFamily:Ljava/lang/String;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setFixedFontFamily(Ljava/lang/String;)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mFixedFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mFixedFontFamily:Ljava/lang/String;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setImagesEnabled(Z)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mImagesEnabled:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mImagesEnabled:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setJavaScriptCanOpenWindowsAutomatically(Z)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mJavaScriptCanOpenWindowsAutomatically:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mJavaScriptCanOpenWindowsAutomatically:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setJavaScriptEnabled(Z)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mJavaScriptEnabled:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mJavaScriptEnabled:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setLayoutAlgorithm(Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mLayoutAlgorithm:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    if-eq v0, p1, :cond_1

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mLayoutAlgorithm:Lorg/chromium/content/browser/ContentSettings$LayoutAlgorithm;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setLoadsImagesAutomatically(Z)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mLoadsImagesAutomatically:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mLoadsImagesAutomatically:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setMinimumFontSize(I)V
    .locals 3

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentSettings;->clipFontSize(I)I

    move-result v0

    iget v2, p0, Lorg/chromium/content/browser/ContentSettings;->mMinimumFontSize:I

    if-eq v2, v0, :cond_1

    iput v0, p0, Lorg/chromium/content/browser/ContentSettings;->mMinimumFontSize:I

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setMinimumLogicalFontSize(I)V
    .locals 3

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lorg/chromium/content/browser/ContentSettings;->clipFontSize(I)I

    move-result v0

    iget v2, p0, Lorg/chromium/content/browser/ContentSettings;->mMinimumLogicalFontSize:I

    if-eq v2, v0, :cond_1

    iput v0, p0, Lorg/chromium/content/browser/ContentSettings;->mMinimumLogicalFontSize:I

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setPluginState(Landroid/webkit/WebSettings$PluginState;)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    if-eq v0, p1, :cond_1

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setPluginsEnabled(Z)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    :goto_0
    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    return-void

    :cond_1
    sget-object v0, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    goto :goto_0
.end method

.method public setSansSerifFontFamily(Ljava/lang/String;)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mSansSerifFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mSansSerifFontFamily:Ljava/lang/String;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setSerifFontFamily(Ljava/lang/String;)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mSerifFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mSerifFontFamily:Ljava/lang/String;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setStandardFontFamily(Ljava/lang/String;)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mStandardFontFamily:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mStandardFontFamily:Ljava/lang/String;

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setSupportMultipleWindows(Z)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mSupportMultipleWindows:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mSupportMultipleWindows:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setSupportZoom(Z)V
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mSupportZoom:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->sendUpdateMultiTouchMessageLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$700(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setTextZoom(I)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mTextSizePercent:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lorg/chromium/content/browser/ContentSettings;->mTextSizePercent:I

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setUseWideViewPort(Z)V
    .locals 2

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mUseWideViewport:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lorg/chromium/content/browser/ContentSettings;->mUseWideViewport:Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setUserAgentString(Ljava/lang/String;)V
    .locals 3

    sget-boolean v0, Lorg/chromium/content/browser/ContentSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mCanModifySettings:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mUserAgent:Ljava/lang/String;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    # getter for: Lorg/chromium/content/browser/ContentSettings$LazyDefaultUserAgent;->sInstance:Ljava/lang/String;
    invoke-static {}, Lorg/chromium/content/browser/ContentSettings$LazyDefaultUserAgent;->access$500()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/chromium/content/browser/ContentSettings;->mUserAgent:Ljava/lang/String;

    :goto_0
    iget-object v2, p0, Lorg/chromium/content/browser/ContentSettings;->mUserAgent:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->sendUpdateUaMessageLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$600(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    :cond_2
    monitor-exit v1

    return-void

    :cond_3
    iput-object p1, p0, Lorg/chromium/content/browser/ContentSettings;->mUserAgent:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method shouldDisplayZoomControls()Z
    .locals 1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentSettings;->supportsMultiTouchZoom()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mDisplayZoomControls:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public supportMultipleWindows()Z
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mSupportMultipleWindows:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public supportZoom()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mSupportZoom:Z

    return v0
.end method

.method supportsMultiTouchZoom()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mSupportZoom:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/content/browser/ContentSettings;->mBuiltInZoomControls:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method syncFromNativeOnUiThread()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mNativeContentSettings:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mNativeContentSettings:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->nativeSyncFromNative(I)V

    :cond_0
    return-void
.end method

.method syncSettings()V
    .locals 2

    iget-object v1, p0, Lorg/chromium/content/browser/ContentSettings;->mContentSettingsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentSettings;->mEventHandler:Lorg/chromium/content/browser/ContentSettings$EventHandler;

    # invokes: Lorg/chromium/content/browser/ContentSettings$EventHandler;->syncSettingsLocked()V
    invoke-static {v0}, Lorg/chromium/content/browser/ContentSettings$EventHandler;->access$800(Lorg/chromium/content/browser/ContentSettings$EventHandler;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method syncToNativeOnUiThread()V
    .locals 1

    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mNativeContentSettings:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/content/browser/ContentSettings;->mNativeContentSettings:I

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/ContentSettings;->nativeSyncToNative(I)V

    :cond_0
    return-void
.end method
