.class public Lorg/chromium/content/browser/third_party/GestureDetector;
.super Ljava/lang/Object;


# static fields
.field private static final DOUBLE_TAP_TIMEOUT:I

.field private static final LONGPRESS_TIMEOUT:I

.field private static final LONG_PRESS:I = 0x2

.field private static final SHOW_PRESS:I = 0x1

.field private static final TAP:I = 0x3

.field private static final TAP_TIMEOUT:I


# instance fields
.field private mAlwaysInBiggerTapRegion:Z

.field private mAlwaysInTapRegion:Z

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mDoubleTapListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;

.field private mDoubleTapSlopSquare:I

.field private mDoubleTapTouchSlopSquare:I

.field private mDownFocusX:F

.field private mDownFocusY:F

.field private final mHandler:Landroid/os/Handler;

.field private mInLongPress:Z

.field private mIsDoubleTapping:Z

.field private mIsLongpressEnabled:Z

.field private mLastFocusX:F

.field private mLastFocusY:F

.field private final mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

.field private mMaximumFlingVelocity:I

.field private mMinimumFlingVelocity:I

.field private mPreviousUpEvent:Landroid/view/MotionEvent;

.field private mStillDown:Z

.field private mTouchSlopSquare:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Lorg/chromium/content/browser/third_party/GestureDetector;->LONGPRESS_TIMEOUT:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lorg/chromium/content/browser/third_party/GestureDetector;->TAP_TIMEOUT:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lorg/chromium/content/browser/third_party/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/chromium/content/browser/third_party/GestureDetector;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p3, :cond_1

    new-instance v0, Lorg/chromium/content/browser/third_party/GestureDetector$GestureHandler;

    invoke-direct {v0, p0, p3}, Lorg/chromium/content/browser/third_party/GestureDetector$GestureHandler;-><init>(Lorg/chromium/content/browser/third_party/GestureDetector;Landroid/os/Handler;)V

    iput-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    :goto_0
    iput-object p2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    instance-of v0, p2, Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_0

    check-cast p2, Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;

    invoke-virtual {p0, p2}, Lorg/chromium/content/browser/third_party/GestureDetector;->setOnDoubleTapListener(Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;)V

    :cond_0
    invoke-direct {p0, p1}, Lorg/chromium/content/browser/third_party/GestureDetector;->init(Landroid/content/Context;)V

    return-void

    :cond_1
    new-instance v0, Lorg/chromium/content/browser/third_party/GestureDetector$GestureHandler;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/third_party/GestureDetector$GestureHandler;-><init>(Lorg/chromium/content/browser/third_party/GestureDetector;)V

    iput-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/content/browser/third_party/GestureDetector;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    return-void
.end method

.method public constructor <init>(Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, v0}, Lorg/chromium/content/browser/third_party/GestureDetector;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    return-void
.end method

.method public constructor <init>(Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lorg/chromium/content/browser/third_party/GestureDetector;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/third_party/GestureDetector;)Landroid/view/MotionEvent;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/content/browser/third_party/GestureDetector;)Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/third_party/GestureDetector;)V
    .locals 0

    invoke-direct {p0}, Lorg/chromium/content/browser/third_party/GestureDetector;->dispatchLongPress()V

    return-void
.end method

.method static synthetic access$300(Lorg/chromium/content/browser/third_party/GestureDetector;)Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;

    return-object v0
.end method

.method static synthetic access$400(Lorg/chromium/content/browser/third_party/GestureDetector;)Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mStillDown:Z

    return v0
.end method

.method private cancel()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    iput-boolean v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mIsDoubleTapping:Z

    iput-boolean v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mStillDown:Z

    iput-boolean v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInTapRegion:Z

    iput-boolean v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    iget-boolean v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mInLongPress:Z

    :cond_0
    return-void
.end method

.method private cancelTaps()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iput-boolean v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mIsDoubleTapping:Z

    iput-boolean v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInTapRegion:Z

    iput-boolean v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    iget-boolean v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mInLongPress:Z

    :cond_0
    return-void
.end method

.method private dispatchLongPress()V
    .locals 2

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mInLongPress:Z

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "OnGestureListener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mIsLongpressEnabled:Z

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mMinimumFlingVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mMaximumFlingVelocity:I

    mul-int v0, v1, v1

    iput v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mTouchSlopSquare:I

    mul-int v0, v1, v1

    iput v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapTouchSlopSquare:I

    mul-int v0, v2, v2

    iput v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapSlopSquare:I

    return-void
.end method

.method private isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x0

    iget-boolean v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    sget v3, Lorg/chromium/content/browser/third_party/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v3, v3

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    mul-int/2addr v1, v1

    mul-int/2addr v2, v2

    add-int/2addr v1, v2

    iget v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapSlopSquare:I

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public isLongpressEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mIsLongpressEnabled:Z

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    const/4 v6, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x3

    const/4 v8, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    and-int/lit16 v0, v9, 0xff

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    move v7, v8

    :goto_0
    if-eqz v7, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    move v5, v3

    move v1, v6

    move v2, v6

    :goto_2
    if-ge v5, v4, :cond_4

    if-eq v0, v5, :cond_1

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    add-float/2addr v2, v10

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    add-float/2addr v1, v10

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_2
    move v7, v3

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_1

    :cond_4
    if-eqz v7, :cond_6

    add-int/lit8 v0, v4, -0x1

    :goto_3
    int-to-float v5, v0

    div-float/2addr v2, v5

    int-to-float v0, v0

    div-float/2addr v1, v0

    and-int/lit16 v0, v9, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_5
    :goto_4
    :pswitch_0
    return v3

    :cond_6
    move v0, v4

    goto :goto_3

    :pswitch_1
    iput v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusX:F

    iput v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDownFocusX:F

    iput v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusY:F

    iput v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDownFocusY:F

    invoke-direct {p0}, Lorg/chromium/content/browser/third_party/GestureDetector;->cancelTaps()V

    goto :goto_4

    :pswitch_2
    iput v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusX:F

    iput v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDownFocusX:F

    iput v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusY:F

    iput v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDownFocusY:F

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mMaximumFlingVelocity:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iget-object v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v2

    iget-object v5, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5, v0}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    move v0, v3

    :goto_5
    if-ge v0, v4, :cond_5

    if-eq v0, v1, :cond_7

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    iget-object v8, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v8, v7}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v8

    mul-float/2addr v8, v2

    iget-object v9, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v9, v7}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v7

    mul-float/2addr v7, v5

    add-float/2addr v7, v8

    cmpg-float v7, v7, v6

    if-gez v7, :cond_7

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_4

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :pswitch_3
    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v4, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v11}, Landroid/os/Handler;->removeMessages(I)V

    :cond_8
    iget-object v4, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    if-eqz v4, :cond_b

    if-eqz v0, :cond_b

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    iget-object v4, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    invoke-direct {p0, v0, v4, p1}, Lorg/chromium/content/browser/third_party/GestureDetector;->isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_b

    iput-boolean v8, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mIsDoubleTapping:Z

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;

    iget-object v4, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v0, v4}, Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    iget-object v4, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;

    invoke-interface {v4, p1}, Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    or-int/2addr v0, v4

    :goto_6
    iput v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusX:F

    iput v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDownFocusX:F

    iput v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusY:F

    iput v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDownFocusY:F

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    :cond_9
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    iput-boolean v8, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInTapRegion:Z

    iput-boolean v8, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    iput-boolean v8, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mStillDown:Z

    iput-boolean v3, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mInLongPress:Z

    iget-boolean v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mIsLongpressEnabled:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v4, Lorg/chromium/content/browser/third_party/GestureDetector;->TAP_TIMEOUT:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    sget v4, Lorg/chromium/content/browser/third_party/GestureDetector;->LONGPRESS_TIMEOUT:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v12, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    :cond_a
    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v4, Lorg/chromium/content/browser/third_party/GestureDetector;->TAP_TIMEOUT:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v8, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    invoke-interface {v1, p1}, Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int v3, v0, v1

    goto/16 :goto_4

    :cond_b
    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    sget v4, Lorg/chromium/content/browser/third_party/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v4, v4

    invoke-virtual {v0, v11, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_c
    move v0, v3

    goto :goto_6

    :pswitch_4
    iget-boolean v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mInLongPress:Z

    if-nez v0, :cond_5

    iget v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusX:F

    sub-float/2addr v0, v2

    iget v4, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusY:F

    sub-float/2addr v4, v1

    iget-boolean v5, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mIsDoubleTapping:Z

    if-eqz v5, :cond_d

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v3, v0, 0x0

    goto/16 :goto_4

    :cond_d
    iget-boolean v5, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInTapRegion:Z

    if-eqz v5, :cond_f

    iget v5, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDownFocusX:F

    sub-float v5, v2, v5

    float-to-int v5, v5

    iget v6, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDownFocusY:F

    sub-float v6, v1, v6

    float-to-int v6, v6

    mul-int/2addr v5, v5

    mul-int/2addr v6, v6

    add-int/2addr v5, v6

    iget v6, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mTouchSlopSquare:I

    if-le v5, v6, :cond_18

    iget-object v6, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    iget-object v7, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v6, v7, p1, v0, v4}, Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    iput v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusX:F

    iput v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusY:F

    iput-boolean v3, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInTapRegion:Z

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    :goto_7
    iget v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapTouchSlopSquare:I

    if-le v5, v1, :cond_e

    iput-boolean v3, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    :cond_e
    move v3, v0

    goto/16 :goto_4

    :cond_f
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f800000

    cmpl-float v5, v5, v6

    if-gez v5, :cond_10

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f800000

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_5

    :cond_10
    iget-object v3, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    iget-object v5, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v3, v5, p1, v0, v4}, Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v3

    iput v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusX:F

    iput v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mLastFocusY:F

    goto/16 :goto_4

    :pswitch_5
    iput-boolean v3, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mStillDown:Z

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iget-boolean v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mIsDoubleTapping:Z

    if-eqz v0, :cond_13

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    :goto_8
    iget-object v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    :cond_11
    iput-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_12
    iput-boolean v3, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mIsDoubleTapping:Z

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    move v3, v0

    goto/16 :goto_4

    :cond_13
    iget-boolean v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_14

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    iput-boolean v3, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mInLongPress:Z

    move v0, v3

    goto :goto_8

    :cond_14
    iget-boolean v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mAlwaysInTapRegion:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_8

    :cond_15
    iget-object v0, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    const/16 v4, 0x3e8

    iget v5, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mMaximumFlingVelocity:I

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v4

    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v5, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mMinimumFlingVelocity:I

    int-to-float v5, v5

    cmpl-float v2, v2, v5

    if-gtz v2, :cond_16

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v5, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mMinimumFlingVelocity:I

    int-to-float v5, v5

    cmpl-float v2, v2, v5

    if-lez v2, :cond_17

    :cond_16
    iget-object v2, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;

    iget-object v5, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v2, v5, p1, v0, v4}, Lorg/chromium/content/browser/third_party/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_8

    :pswitch_6
    invoke-direct {p0}, Lorg/chromium/content/browser/third_party/GestureDetector;->cancel()V

    goto/16 :goto_4

    :cond_17
    move v0, v3

    goto :goto_8

    :cond_18
    move v0, v3

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setIsLongpressEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mIsLongpressEnabled:Z

    return-void
.end method

.method public setOnDoubleTapListener(Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/third_party/GestureDetector;->mDoubleTapListener:Lorg/chromium/content/browser/third_party/GestureDetector$OnDoubleTapListener;

    return-void
.end method
