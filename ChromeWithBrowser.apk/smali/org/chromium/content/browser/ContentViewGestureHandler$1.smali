.class Lorg/chromium/content/browser/ContentViewGestureHandler$1;
.super Lorg/chromium/content/browser/third_party/GestureDetector$SimpleOnGestureListener;


# instance fields
.field final synthetic this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/ContentViewGestureHandler;)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-direct {p0}, Lorg/chromium/content/browser/third_party/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method private isDistanceBetweenDownAndUpTooLong(FF)Z
    .locals 4

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawX:F
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$500(Lorg/chromium/content/browser/ContentViewGestureHandler;)F

    move-result v0

    sub-float/2addr v0, p1

    float-to-double v0, v0

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawY:F
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$600(Lorg/chromium/content/browser/ContentViewGestureHandler;)F

    move-result v2

    sub-float/2addr v2, p2

    float-to-double v2, v2

    mul-double/2addr v0, v0

    mul-double/2addr v2, v2

    add-double/2addr v0, v2

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mScaledTouchSlopSquare:I
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1500(Lorg/chromium/content/browser/ContentViewGestureHandler;)I

    move-result v2

    int-to-double v2, v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v1, 0x1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->sendShowPressCancelIfNecessary(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    return v1
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mShowPressIsCalled:Z
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$002(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mIgnoreSingleTap:Z
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$102(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mNativeScrolling:Z
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$202(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mSeenFirstScrollEvent:Z
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$302(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mSnapScrollController:Lorg/chromium/content/browser/SnapScrollController;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$400(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/SnapScrollController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/SnapScrollController;->resetSnapScrollMode()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawX:F
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$502(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawY:F
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$602(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorX:F
    invoke-static {v0, v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$702(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorY:F
    invoke-static {v0, v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$802(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F

    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mSnapScrollController:Lorg/chromium/content/browser/SnapScrollController;
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$400(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/SnapScrollController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/SnapScrollController;->isSnappingScrolls()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mSnapScrollController:Lorg/chromium/content/browser/SnapScrollController;
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$400(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/SnapScrollController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/SnapScrollController;->isSnapHorizontal()Z

    move-result v1

    if-eqz v1, :cond_1

    move p4, v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    float-to-int v4, v4

    float-to-int v5, p3

    float-to-int v6, p4

    invoke-virtual/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler;->fling(JIIII)V

    const/4 v0, 0x1

    return v0

    :cond_1
    move p3, v0

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 7

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mZoomManager:Lorg/chromium/content/browser/ZoomManager;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1400(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ZoomManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ZoomManager;->isScaleGestureDetectionInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->sendShowPressCancelIfNecessary(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    const/4 v0, 0x0

    const/4 v7, 0x1

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mSnapScrollController:Lorg/chromium/content/browser/SnapScrollController;
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$400(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/SnapScrollController;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Lorg/chromium/content/browser/SnapScrollController;->updateSnapScrollMode(FF)V

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mSnapScrollController:Lorg/chromium/content/browser/SnapScrollController;
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$400(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/SnapScrollController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/SnapScrollController;->isSnappingScrolls()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mSnapScrollController:Lorg/chromium/content/browser/SnapScrollController;
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$400(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/SnapScrollController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/SnapScrollController;->isSnapHorizontal()Z

    move-result v1

    if-eqz v1, :cond_2

    move p4, v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mSeenFirstScrollEvent:Z
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$300(Lorg/chromium/content/browser/ContentViewGestureHandler;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mSeenFirstScrollEvent:Z
    invoke-static {v0, v7}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$302(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z

    :cond_1
    :goto_1
    return v7

    :cond_2
    move p3, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawX:F
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$500(Lorg/chromium/content/browser/ContentViewGestureHandler;)F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget-object v3, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawY:F
    invoke-static {v3}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$600(Lorg/chromium/content/browser/ContentViewGestureHandler;)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-interface {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->didUIStealScroll(FF)Z

    move-result v0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawX:F
    invoke-static {v1, v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$502(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mLastRawY:F
    invoke-static {v1, v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$602(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mNativeScrolling:Z
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$200(Lorg/chromium/content/browser/ContentViewGestureHandler;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->sendShowPressCancelIfNecessary(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mNativeScrolling:Z
    invoke-static {v0, v7}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$202(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z

    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorX:F
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$700(Lorg/chromium/content/browser/ContentViewGestureHandler;)F

    move-result v0

    add-float/2addr v0, p3

    float-to-int v0, v0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorY:F
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$800(Lorg/chromium/content/browser/ContentViewGestureHandler;)F

    move-result v1

    add-float/2addr v1, p4

    float-to-int v1, v1

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    iget-object v3, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorX:F
    invoke-static {v3}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$700(Lorg/chromium/content/browser/ContentViewGestureHandler;)F

    move-result v3

    add-float/2addr v3, p3

    int-to-float v6, v0

    sub-float/2addr v3, v6

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorX:F
    invoke-static {v2, v3}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$702(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    iget-object v3, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorY:F
    invoke-static {v3}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$800(Lorg/chromium/content/browser/ContentViewGestureHandler;)F

    move-result v3

    add-float/2addr v3, p4

    int-to-float v6, v1

    sub-float/2addr v3, v6

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mAccumulatedScrollErrorY:F
    invoke-static {v2, v3}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$802(Lorg/chromium/content/browser/ContentViewGestureHandler;F)F

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->clear()V

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "Distance X"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "Distance Y"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    or-int/2addr v0, v1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iget-object v6, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;
    invoke-static {v6}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    :cond_5
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->invokeZoomPicker()V

    goto/16 :goto_1
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 7

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    const/4 v1, 0x1

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mShowPressIsCalled:Z
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$002(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1100(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/LongPressDetector;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/LongPressDetector;->isInLongPress()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mIgnoreSingleTap:Z
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$100(Lorg/chromium/content/browser/ContentViewGestureHandler;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ShowPress"

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mShowPressIsCalled:Z
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iget-object v6, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;
    invoke-static {v6}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # invokes: Lorg/chromium/content/browser/ContentViewGestureHandler;->setClickXAndY(II)V
    invoke-static {v0, v4, v5}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1300(Lorg/chromium/content/browser/ContentViewGestureHandler;II)V

    goto :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 10

    const/4 v7, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->isDistanceBetweenDownAndUpTooLong(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mIgnoreSingleTap:Z
    invoke-static {v0, v7}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$102(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z

    move v0, v7

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mIgnoreSingleTap:Z
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$100(Lorg/chromium/content/browser/ContentViewGestureHandler;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mLongPressDetector:Lorg/chromium/content/browser/LongPressDetector;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1100(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/LongPressDetector;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/LongPressDetector;->isInLongPress()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->DOUBLE_TAP_TIMEOUT:I
    invoke-static {}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1200()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    float-to-int v4, v8

    float-to-int v5, v9

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mIgnoreSingleTap:Z
    invoke-static {v0, v7}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$102(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z

    :cond_1
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    float-to-int v1, v8

    float-to-int v2, v9

    # invokes: Lorg/chromium/content/browser/ContentViewGestureHandler;->setClickXAndY(II)V
    invoke-static {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1300(Lorg/chromium/content/browser/ContentViewGestureHandler;II)V

    move v0, v7

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->hasFixedPageScale()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ShowPress"

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mShowPressIsCalled:Z
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mMotionEventDelegate:Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;
    invoke-static {v0}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$900(Lorg/chromium/content/browser/ContentViewGestureHandler;)Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    float-to-int v4, v8

    float-to-int v5, v9

    iget-object v6, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # getter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mExtraParamBundle:Landroid/os/Bundle;
    invoke-static {v6}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1000(Lorg/chromium/content/browser/ContentViewGestureHandler;)Landroid/os/Bundle;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, Lorg/chromium/content/browser/ContentViewGestureHandler$MotionEventDelegate;->sendGesture(IJIILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    # setter for: Lorg/chromium/content/browser/ContentViewGestureHandler;->mIgnoreSingleTap:Z
    invoke-static {v0, v7}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$102(Lorg/chromium/content/browser/ContentViewGestureHandler;Z)Z

    :cond_3
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    float-to-int v1, v8

    float-to-int v2, v9

    # invokes: Lorg/chromium/content/browser/ContentViewGestureHandler;->setClickXAndY(II)V
    invoke-static {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewGestureHandler;->access$1300(Lorg/chromium/content/browser/ContentViewGestureHandler;II)V

    :cond_4
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewGestureHandler$1;->this$0:Lorg/chromium/content/browser/ContentViewGestureHandler;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewGestureHandler;->triggerLongTapIfNeeded(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_0
.end method
