.class Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final mContainer:Landroid/widget/PopupWindow;

.field private mPasteViewLayouts:[I

.field private mPasteViews:[Landroid/view/View;

.field private mPositionX:I

.field private mPositionY:I

.field final synthetic this$0:Lorg/chromium/content/browser/InsertionHandleController;


# direct methods
.method public constructor <init>(Lorg/chromium/content/browser/InsertionHandleController;)V
    .locals 6

    const/4 v5, -0x2

    const/4 v1, 0x0

    iput-object p1, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/widget/PopupWindow;

    # getter for: Lorg/chromium/content/browser/InsertionHandleController;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lorg/chromium/content/browser/InsertionHandleController;->access$000(Lorg/chromium/content/browser/InsertionHandleController;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const v4, 0x10102c8

    invoke-direct {v0, v2, v3, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setSplitTouchEnabled(Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    array-length v2, v0

    new-array v2, v2, [Landroid/view/View;

    iput-object v2, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mPasteViews:[Landroid/view/View;

    array-length v2, v0

    new-array v2, v2, [I

    iput-object v2, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mPasteViewLayouts:[I

    # getter for: Lorg/chromium/content/browser/InsertionHandleController;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lorg/chromium/content/browser/InsertionHandleController;->access$000(Lorg/chromium/content/browser/InsertionHandleController;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    move v0, v1

    :goto_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mPasteViewLayouts:[I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    nop

    :array_0
    .array-data 4
        0x1010314
        0x1010315
        0x101035e
        0x101035f
    .end array-data
.end method

.method private updateContent(Z)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, -0x2

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->viewIndex(Z)I

    move-result v2

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mPasteViews:[Landroid/view/View;

    aget-object v1, v0, v2

    if-nez v1, :cond_2

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mPasteViewLayouts:[I

    aget v3, v0, v2

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    # getter for: Lorg/chromium/content/browser/InsertionHandleController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/content/browser/InsertionHandleController;->access$000(Lorg/chromium/content/browser/InsertionHandleController;)Landroid/content/Context;

    move-result-object v0

    const-string v4, "layout_inflater"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to inflate TextEdit paste window"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1, v1}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mPasteViews:[Landroid/view/View;

    aput-object v0, v1, v2

    :goto_1
    iget-object v1, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private viewIndex(Z)I
    .locals 3

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v2}, Lorg/chromium/content/browser/InsertionHandleController;->canPaste()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method hide()V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    return-void
.end method

.method isShowing()Z
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->canPaste()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v0}, Lorg/chromium/content/browser/InsertionHandleController;->paste()V

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->hide()V

    return-void
.end method

.method positionAtCursor()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v2, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    invoke-virtual {v2}, Lorg/chromium/content/browser/InsertionHandleController;->getLineHeight()I

    move-result v2

    iget-object v3, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    # getter for: Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;
    invoke-static {v3}, Lorg/chromium/content/browser/InsertionHandleController;->access$100(Lorg/chromium/content/browser/InsertionHandleController;)Lorg/chromium/content/browser/HandleView;

    move-result-object v3

    invoke-virtual {v3}, Lorg/chromium/content/browser/HandleView;->getAdjustedPositionX()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v1

    const/high16 v5, 0x40000000

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mPositionX:I

    iget-object v3, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    # getter for: Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;
    invoke-static {v3}, Lorg/chromium/content/browser/InsertionHandleController;->access$100(Lorg/chromium/content/browser/InsertionHandleController;)Lorg/chromium/content/browser/HandleView;

    move-result-object v3

    invoke-virtual {v3}, Lorg/chromium/content/browser/HandleView;->getAdjustedPositionY()I

    move-result v3

    sub-int v0, v3, v0

    sub-int/2addr v0, v2

    iput v0, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mPositionY:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v3, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    # getter for: Lorg/chromium/content/browser/InsertionHandleController;->mParent:Landroid/view/View;
    invoke-static {v3}, Lorg/chromium/content/browser/InsertionHandleController;->access$200(Lorg/chromium/content/browser/InsertionHandleController;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->getLocationInWindow([I)V

    aget v3, v0, v6

    iget v4, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mPositionX:I

    add-int/2addr v3, v4

    aput v3, v0, v6

    aget v3, v0, v7

    iget v4, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mPositionY:I

    add-int/2addr v3, v4

    aput v3, v0, v7

    iget-object v3, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    # getter for: Lorg/chromium/content/browser/InsertionHandleController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lorg/chromium/content/browser/InsertionHandleController;->access$000(Lorg/chromium/content/browser/InsertionHandleController;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    aget v4, v0, v7

    if-gez v4, :cond_1

    invoke-direct {p0, v6}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->updateContent(Z)V

    iget-object v1, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    aget v5, v0, v7

    add-int/2addr v1, v5

    aput v1, v0, v7

    aget v1, v0, v7

    add-int/2addr v1, v2

    aput v1, v0, v7

    iget-object v1, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    # getter for: Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;
    invoke-static {v1}, Lorg/chromium/content/browser/InsertionHandleController;->access$100(Lorg/chromium/content/browser/InsertionHandleController;)Lorg/chromium/content/browser/HandleView;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/HandleView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    # getter for: Lorg/chromium/content/browser/InsertionHandleController;->mHandle:Lorg/chromium/content/browser/HandleView;
    invoke-static {v2}, Lorg/chromium/content/browser/InsertionHandleController;->access$100(Lorg/chromium/content/browser/InsertionHandleController;)Lorg/chromium/content/browser/HandleView;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/content/browser/HandleView;->getAdjustedPositionX()I

    move-result v2

    add-int/2addr v2, v4

    if-ge v2, v3, :cond_0

    aget v2, v0, v6

    div-int/lit8 v3, v4, 0x2

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    aput v1, v0, v6

    :goto_0
    iget-object v1, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->mContainer:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->this$0:Lorg/chromium/content/browser/InsertionHandleController;

    # getter for: Lorg/chromium/content/browser/InsertionHandleController;->mParent:Landroid/view/View;
    invoke-static {v2}, Lorg/chromium/content/browser/InsertionHandleController;->access$200(Lorg/chromium/content/browser/InsertionHandleController;)Landroid/view/View;

    move-result-object v2

    aget v3, v0, v6

    aget v0, v0, v7

    invoke-virtual {v1, v2, v6, v3, v0}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    return-void

    :cond_0
    aget v2, v0, v6

    div-int/lit8 v3, v4, 0x2

    add-int/2addr v1, v3

    sub-int v1, v2, v1

    aput v1, v0, v6

    goto :goto_0

    :cond_1
    aget v2, v0, v6

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    aput v2, v0, v6

    sub-int v1, v3, v1

    aget v2, v0, v6

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    aput v1, v0, v6

    goto :goto_0
.end method

.method show()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->updateContent(Z)V

    invoke-virtual {p0}, Lorg/chromium/content/browser/InsertionHandleController$PastePopupMenu;->positionAtCursor()V

    return-void
.end method
