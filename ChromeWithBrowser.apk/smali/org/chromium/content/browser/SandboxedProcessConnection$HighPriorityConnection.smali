.class Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private mHBound:Z

.field final synthetic this$0:Lorg/chromium/content/browser/SandboxedProcessConnection;


# direct methods
.method private constructor <init>(Lorg/chromium/content/browser/SandboxedProcessConnection;)V
    .locals 1

    iput-object p1, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;->this$0:Lorg/chromium/content/browser/SandboxedProcessConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;->mHBound:Z

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/content/browser/SandboxedProcessConnection;Lorg/chromium/content/browser/SandboxedProcessConnection$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;-><init>(Lorg/chromium/content/browser/SandboxedProcessConnection;)V

    return-void
.end method


# virtual methods
.method bind()V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;->this$0:Lorg/chromium/content/browser/SandboxedProcessConnection;

    # invokes: Lorg/chromium/content/browser/SandboxedProcessConnection;->createServiceBindIntent()Landroid/content/Intent;
    invoke-static {v0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->access$100(Lorg/chromium/content/browser/SandboxedProcessConnection;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;->this$0:Lorg/chromium/content/browser/SandboxedProcessConnection;

    # getter for: Lorg/chromium/content/browser/SandboxedProcessConnection;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lorg/chromium/content/browser/SandboxedProcessConnection;->access$200(Lorg/chromium/content/browser/SandboxedProcessConnection;)Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x41

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;->mHBound:Z

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    return-void
.end method

.method unbind()V
    .locals 1

    iget-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;->mHBound:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;->this$0:Lorg/chromium/content/browser/SandboxedProcessConnection;

    # getter for: Lorg/chromium/content/browser/SandboxedProcessConnection;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/content/browser/SandboxedProcessConnection;->access$200(Lorg/chromium/content/browser/SandboxedProcessConnection;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/SandboxedProcessConnection$HighPriorityConnection;->mHBound:Z

    :cond_0
    return-void
.end method
