.class public interface abstract Lorg/chromium/content/browser/ContentVideoViewContextDelegate;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getErrorButton()Ljava/lang/String;
.end method

.method public abstract getErrorTitle()Ljava/lang/String;
.end method

.method public abstract getPlayBackErrorText()Ljava/lang/String;
.end method

.method public abstract getUnknownErrorText()Ljava/lang/String;
.end method

.method public abstract getVideoLoadingText()Ljava/lang/String;
.end method

.method public abstract onDestroyContentVideoView()V
.end method

.method public abstract onShowCustomView(Landroid/view/View;)V
.end method
