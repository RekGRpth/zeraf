.class Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mNativeImeAdapter:I

.field final synthetic this$0:Lorg/chromium/content/browser/ImeAdapter;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/ImeAdapter;I)V
    .locals 0

    iput-object p1, p0, Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;->this$0:Lorg/chromium/content/browser/ImeAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;->mNativeImeAdapter:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;->this$0:Lorg/chromium/content/browser/ImeAdapter;

    iget v1, p0, Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;->mNativeImeAdapter:I

    sget v2, Lorg/chromium/content/browser/ImeAdapter;->sTextInputTypeNone:I

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ImeAdapter;->attach(II)V

    iget-object v0, p0, Lorg/chromium/content/browser/ImeAdapter$DelayedDismissInput;->this$0:Lorg/chromium/content/browser/ImeAdapter;

    const/4 v1, 0x1

    # invokes: Lorg/chromium/content/browser/ImeAdapter;->dismissInput(Z)V
    invoke-static {v0, v1}, Lorg/chromium/content/browser/ImeAdapter;->access$000(Lorg/chromium/content/browser/ImeAdapter;Z)V

    return-void
.end method
