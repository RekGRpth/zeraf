.class public Lorg/chromium/content/common/SurfaceCallback;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
    value = "content"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native nativeSetSurface(ILandroid/view/Surface;II)V
.end method

.method public static setSurface(ILandroid/view/Surface;II)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lorg/chromium/content/common/SurfaceCallback;->nativeSetSurface(ILandroid/view/Surface;II)V

    return-void
.end method
