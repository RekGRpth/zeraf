.class Lcom/android/magicsmoke/MagicSmokeRS$Preset;
.super Ljava/lang/Object;
.source "MagicSmokeRS.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/magicsmoke/MagicSmokeRS;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Preset"
.end annotation


# instance fields
.field public mAlphaMul:F

.field public mBackColor:I

.field public mHighColor:I

.field public mLowColor:I

.field public mPreMul:Z

.field public mProcessTextureMode:I

.field public mRotate:Z

.field public mTextureMask:I

.field public mTextureSwap:Z


# direct methods
.method constructor <init>(IIIIFIZZZ)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # F
    .param p6    # I
    .param p7    # Z
    .param p8    # Z
    .param p9    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mProcessTextureMode:I

    iput p2, p0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mBackColor:I

    iput p3, p0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mLowColor:I

    iput p4, p0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mHighColor:I

    iput p5, p0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mAlphaMul:F

    iput p6, p0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mTextureMask:I

    iput-boolean p7, p0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mRotate:Z

    iput-boolean p8, p0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mTextureSwap:Z

    iput-boolean p9, p0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mPreMul:Z

    return-void
.end method
