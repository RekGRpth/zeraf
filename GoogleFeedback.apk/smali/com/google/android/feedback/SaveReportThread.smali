.class public Lcom/google/android/feedback/SaveReportThread;
.super Ljava/lang/Thread;
.source "SaveReportThread.java"


# static fields
.field private static final mCompareByDate:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContext:Lcom/google/android/feedback/FeedbackActivity;

.field private final mReport:Lcom/google/android/feedback/ExtendedErrorReport;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/feedback/SaveReportThread$1;

    invoke-direct {v0}, Lcom/google/android/feedback/SaveReportThread$1;-><init>()V

    sput-object v0, Lcom/google/android/feedback/SaveReportThread;->mCompareByDate:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/feedback/FeedbackActivity;Lcom/google/android/feedback/ExtendedErrorReport;)V
    .locals 0
    .param p1    # Lcom/google/android/feedback/FeedbackActivity;
    .param p2    # Lcom/google/android/feedback/ExtendedErrorReport;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p1, p0, Lcom/google/android/feedback/SaveReportThread;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    iput-object p2, p0, Lcom/google/android/feedback/SaveReportThread;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    return-void
.end method

.method private static createAndroidBugReportProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 3
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->ANDROID_BUG_REPORT:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createCommonDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    const/4 v1, 0x2

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createAndroidDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->type:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    return-object v0
.end method

.method private static createAndroidDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 4
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->ANDROID_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x1

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createSystemDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    const/4 v2, 0x2

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createPackageDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    const/4 v2, 0x3

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createBuildDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createCrashDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    :goto_0
    iget v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->type:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_0

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createUserInitiatedFeedbackDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_0

    const/16 v2, 0x9

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createUserInitiatedFeedbackDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    if-eqz v2, :cond_2

    const/4 v2, 0x5

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createAnrDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    if-eqz v2, :cond_3

    const/4 v2, 0x6

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createBatteryDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    if-eqz v2, :cond_4

    const/4 v2, 0x7

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createRunningServiceDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_4
    const-string v2, "SaveReportThread"

    const-string v3, "unknown error report type"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static createAnrDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 3
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->ANR_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    iget-object v1, p0, Lcom/google/android/feedback/ExtendedErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$AnrInfo;->activity:Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$AnrInfo;->activity:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_0
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$AnrInfo;->cause:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$AnrInfo;->info:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/feedback/ExtendedErrorReport;->anrStackTraces:Ljava/lang/String;

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->anrStackTraces:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_1
    return-object v0
.end method

.method private static createBatteryDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 4
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->BATTERY_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget v2, v2, Landroid/app/ApplicationErrorReport$BatteryInfo;->usagePercent:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-wide v2, v2, Landroid/app/ApplicationErrorReport$BatteryInfo;->durationMicros:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$BatteryInfo;->usageDetails:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$BatteryInfo;->checkinDetails:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    return-object v0
.end method

.method private static createBuildDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 3
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->BUILD_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->device:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->buildId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->buildType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->model:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->product:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->sdk_int:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->release:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->incremental:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->codename:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->board:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->brand:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->buildFingerprint:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    return-object v0
.end method

.method private static createCommonDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 3
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->COMMON_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->description:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/feedback/ExtendedErrorReport;->account:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method private static createCrashDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 4
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    const/4 v3, 0x3

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->CRASH_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :goto_0
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$CrashInfo;->throwClassName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget v2, v2, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    return-object v0

    :cond_0
    const-string v1, "unknown"

    invoke-virtual {v0, v3, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private static createPackageDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 3
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->PACKAGE_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->installerPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->packageVersion:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->packageVersionName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->systemApp:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    return-object v0
.end method

.method private static createRunningServiceDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 4
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->RUNNING_SERVICE_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-wide v2, v2, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->durationMillis:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-object v2, v2, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->serviceDetails:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    return-object v0
.end method

.method private static createSystemDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 7
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    new-instance v3, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->SYSTEM_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    iget-object v4, p0, Lcom/google/android/feedback/ExtendedErrorReport;->eventLog:Ljava/lang/String;

    if-eqz v4, :cond_0

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/feedback/ExtendedErrorReport;->eventLog:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/feedback/ExtendedErrorReport;->systemLog:Ljava/lang/String;

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/feedback/ExtendedErrorReport;->systemLog:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_1
    const/4 v4, 0x1

    iget-wide v5, p0, Lcom/google/android/feedback/ExtendedErrorReport;->time:J

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    iget-object v4, p0, Lcom/google/android/feedback/ExtendedErrorReport;->installedPackages:Ljava/util/List;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/feedback/ExtendedErrorReport;->installedPackages:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v4, 0x4

    invoke-virtual {v3, v4, v2}, Lcom/google/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/feedback/ExtendedErrorReport;->runningApplications:Ljava/util/List;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/feedback/ExtendedErrorReport;->runningApplications:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v4, 0x5

    invoke-virtual {v3, v4, v0}, Lcom/google/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    goto :goto_1

    :cond_3
    const/4 v4, 0x6

    invoke-static {p0}, Lcom/google/android/feedback/SaveReportThread;->createTelephonyDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    return-object v3
.end method

.method private static createTelephonyDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 3
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->TELEPHONY_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->phoneType:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->networkType:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/feedback/ExtendedErrorReport;->networkName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    return-object v0
.end method

.method private static createUserInitiatedFeedbackDataProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 6
    .param p0    # Lcom/google/android/feedback/ExtendedErrorReport;

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/google/android/feedback/ExtendedErrorReport;->screenshot:Ljava/lang/String;

    if-eqz v3, :cond_0

    new-instance v2, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->USER_INITIATED_FEEDBACK_DATA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->IMAGE:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const-string v3, "image/jpeg"

    invoke-virtual {v0, v4, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/feedback/ExtendedErrorReport;->screenshot:Ljava/lang/String;

    invoke-virtual {v0, v5, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    new-instance v1, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/feedback/proto/AndroidClientMessageTypes;->DIMENSIONS:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v3}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    iget v3, p0, Lcom/google/android/feedback/ExtendedErrorReport;->screenshotHeight:I

    int-to-float v3, v3

    invoke-virtual {v1, v5, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setFloat(IF)V

    iget v3, p0, Lcom/google/android/feedback/ExtendedErrorReport;->screenshotWidth:I

    int-to-float v3, v3

    invoke-virtual {v1, v4, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setFloat(IF)V

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v0}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized deleteOldest(Ljava/io/File;I)V
    .locals 5
    .param p0    # Ljava/io/File;
    .param p1    # I

    const-class v4, Lcom/google/android/feedback/SaveReportThread;

    monitor-enter v4

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v3, v1

    sub-int v0, v3, p1

    if-lez v0, :cond_0

    sget-object v3, Lcom/google/android/feedback/SaveReportThread;->mCompareByDate:Ljava/util/Comparator;

    invoke-static {v1, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v4

    return-void

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method private saveReport(Lcom/google/common/io/protocol/ProtoBuf;)Ljava/io/File;
    .locals 9
    .param p1    # Lcom/google/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/feedback/SaveReportThread;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v6}, Lcom/google/android/feedback/FeedbackActivity;->getFilesDir()Ljava/io/File;

    move-result-object v6

    const-string v7, "reports"

    invoke-direct {v0, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/io/IOException;

    const-string v7, "failed to create reports directory"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    const/4 v6, 0x3

    invoke-static {v0, v6}, Lcom/google/android/feedback/SaveReportThread;->deleteOldest(Ljava/io/File;I)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".tmp"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v0, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".proto.gz"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v0, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v5, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v5, v2}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p1, v5}, Lcom/google/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    invoke-virtual {v5}, Ljava/util/zip/GZIPOutputStream;->close()V

    invoke-virtual {v4, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v6, Ljava/io/IOException;

    const-string v7, "failed to rename temporary file"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v6

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    throw v6

    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    return-object v3
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v2, p0, Lcom/google/android/feedback/SaveReportThread;->mReport:Lcom/google/android/feedback/ExtendedErrorReport;

    invoke-static {v2}, Lcom/google/android/feedback/SaveReportThread;->createAndroidBugReportProto(Lcom/google/android/feedback/ExtendedErrorReport;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/feedback/SaveReportThread;->saveReport(Lcom/google/common/io/protocol/ProtoBuf;)Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/feedback/SaveReportThread;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    invoke-virtual {v2}, Lcom/google/android/feedback/FeedbackActivity;->runSendService()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/feedback/SaveReportThread;->mContext:Lcom/google/android/feedback/FeedbackActivity;

    const v3, 0x7f060013

    invoke-virtual {v2, v3}, Lcom/google/android/feedback/FeedbackActivity;->showToast(I)V

    const-string v2, "SaveReportThread"

    const-string v3, "failed to write bug report"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "SaveReportThread"

    const-string v3, "invalid report"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
