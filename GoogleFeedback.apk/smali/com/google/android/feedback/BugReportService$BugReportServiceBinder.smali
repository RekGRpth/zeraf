.class public Lcom/google/android/feedback/BugReportService$BugReportServiceBinder;
.super Landroid/os/Binder;
.source "BugReportService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/feedback/BugReportService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BugReportServiceBinder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/feedback/BugReportService;


# direct methods
.method public constructor <init>(Lcom/google/android/feedback/BugReportService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/feedback/BugReportService$BugReportServiceBinder;->this$0:Lcom/google/android/feedback/BugReportService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method

.method private createFeedbackIntent(Landroid/os/Parcel;Landroid/app/ApplicationErrorReport;)Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/feedback/BugReportService$BugReportServiceBinder;->getService()Lcom/google/android/feedback/BugReportService;

    move-result-object v0

    const-class v2, Lcom/google/android/feedback/FeedbackActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.extra.BUG_REPORT"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/os/Parcel;->dataSize()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Lcom/google/android/feedback/FeedbackSession$Screenshot;->from(Landroid/graphics/Bitmap;)Lcom/google/android/feedback/FeedbackSession$Screenshot;

    move-result-object v0

    const-string v2, "com.android.feedback.SCREENSHOT_EXTRA"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    return-object v1
.end method


# virtual methods
.method getService()Lcom/google/android/feedback/BugReportService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/feedback/BugReportService$BugReportServiceBinder;->this$0:Lcom/google/android/feedback/BugReportService;

    return-object v0
.end method

.method protected onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v5, 0x0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    iget-object v6, p0, Lcom/google/android/feedback/BugReportService$BugReportServiceBinder;->this$0:Lcom/google/android/feedback/BugReportService;

    invoke-virtual {v6}, Lcom/google/android/feedback/BugReportService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    array-length v6, v1

    if-nez v6, :cond_0

    :goto_0
    return v5

    :cond_0
    new-instance v3, Landroid/app/ApplicationErrorReport;

    invoke-direct {v3}, Landroid/app/ApplicationErrorReport;-><init>()V

    aget-object v5, v1, v5

    iput-object v5, v3, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    const/16 v5, 0xb

    iput v5, v3, Landroid/app/ApplicationErrorReport;->type:I

    iget-object v5, v3, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Landroid/app/ApplicationErrorReport;->installerPackageName:Ljava/lang/String;

    invoke-direct {p0, p2, v3}, Lcom/google/android/feedback/BugReportService$BugReportServiceBinder;->createFeedbackIntent(Landroid/os/Parcel;Landroid/app/ApplicationErrorReport;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/feedback/BugReportService$BugReportServiceBinder;->getService()Lcom/google/android/feedback/BugReportService;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/feedback/BugReportService;->startActivity(Landroid/content/Intent;)V

    const/4 v5, 0x1

    goto :goto_0
.end method
