.class Lcom/android/facelock/FaceLockService$1;
.super Ljava/lang/Thread;
.source "FaceLockService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/facelock/FaceLockService;->initializeIfNecessary()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/facelock/FaceLockService;


# direct methods
.method constructor <init>(Lcom/android/facelock/FaceLockService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/facelock/FaceLockService$1;->this$0:Lcom/android/facelock/FaceLockService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v0, -0x2

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    # getter for: Lcom/android/facelock/FaceLockService;->sInitializeLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/facelock/FaceLockService;->access$000()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    # getter for: Lcom/android/facelock/FaceLockService;->sInitialized:Z
    invoke-static {}, Lcom/android/facelock/FaceLockService;->access$100()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/facelock/FaceLockService$1;->this$0:Lcom/android/facelock/FaceLockService;

    invoke-virtual {v2}, Lcom/android/facelock/FaceLockService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/facelock/FaceLockService$1;->this$0:Lcom/android/facelock/FaceLockService;

    # invokes: Lcom/android/facelock/FaceLockService;->getFilesPath()Ljava/lang/String;
    invoke-static {v3}, Lcom/android/facelock/FaceLockService;->access$200(Lcom/android/facelock/FaceLockService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/android/facelock/FaceLockUtil;->initialize(ZLandroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    # setter for: Lcom/android/facelock/FaceLockService;->sInitialized:Z
    invoke-static {v0}, Lcom/android/facelock/FaceLockService;->access$102(Z)Z

    :cond_0
    # getter for: Lcom/android/facelock/FaceLockService;->sInitialized:Z
    invoke-static {}, Lcom/android/facelock/FaceLockService;->access$100()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "FULFaceLockService"

    const-string v2, "JNI library Initialization failed"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$1;->this$0:Lcom/android/facelock/FaceLockService;

    const v2, 0x7f060020

    # setter for: Lcom/android/facelock/FaceLockService;->mInitializeErrorResource:I
    invoke-static {v0, v2}, Lcom/android/facelock/FaceLockService;->access$302(Lcom/android/facelock/FaceLockService;I)I

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$1;->this$0:Lcom/android/facelock/FaceLockService;

    sget-object v2, Lcom/android/facelock/FaceLockService$InitializeState;->INITIALIZE_FAILED:Lcom/android/facelock/FaceLockService$InitializeState;

    # invokes: Lcom/android/facelock/FaceLockService;->displayIfInitializationError(Lcom/android/facelock/FaceLockService$InitializeState;)V
    invoke-static {v0, v2}, Lcom/android/facelock/FaceLockService;->access$400(Lcom/android/facelock/FaceLockService;Lcom/android/facelock/FaceLockService$InitializeState;)V

    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/facelock/FaceLockService$1;->this$0:Lcom/android/facelock/FaceLockService;

    # invokes: Lcom/android/facelock/FaceLockService;->loadGallery()V
    invoke-static {v0}, Lcom/android/facelock/FaceLockService;->access$500(Lcom/android/facelock/FaceLockService;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
