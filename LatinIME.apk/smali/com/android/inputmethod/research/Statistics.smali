.class public Lcom/android/inputmethod/research/Statistics;
.super Ljava/lang/Object;
.source "Statistics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;
    }
.end annotation


# static fields
.field public static final MIN_DELETION_INTERMISSION:I = 0x2710

.field public static final MIN_TYPING_INTERMISSION:I = 0x7d0

.field private static final sInstance:Lcom/android/inputmethod/research/Statistics;


# instance fields
.field final mAfterDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

.field final mBeforeDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

.field mCharCount:I

.field mDeleteKeyCount:I

.field final mDuringRepeatedDeleteKeysCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

.field mIsEmptinessStateKnown:Z

.field mIsEmptyUponStarting:Z

.field mIsLastKeyDeleteKey:Z

.field final mKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

.field private mLastTapTime:J

.field mLetterCount:I

.field mNumberCount:I

.field mSpaceCount:I

.field mWordCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/inputmethod/research/Statistics;

    invoke-direct {v0}, Lcom/android/inputmethod/research/Statistics;-><init>()V

    sput-object v0, Lcom/android/inputmethod/research/Statistics;->sInstance:Lcom/android/inputmethod/research/Statistics;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-direct {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    new-instance v0, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-direct {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mBeforeDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    new-instance v0, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-direct {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mDuringRepeatedDeleteKeysCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    new-instance v0, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-direct {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mAfterDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {p0}, Lcom/android/inputmethod/research/Statistics;->reset()V

    return-void
.end method

.method public static getInstance()Lcom/android/inputmethod/research/Statistics;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/research/Statistics;->sInstance:Lcom/android/inputmethod/research/Statistics;

    return-object v0
.end method


# virtual methods
.method public recordChar(IJ)V
    .locals 6
    .param p1    # I
    .param p2    # J

    const-wide/16 v4, 0x2710

    iget-wide v2, p0, Lcom/android/inputmethod/research/Statistics;->mLastTapTime:J

    sub-long v0, p2, v2

    const/4 v2, -0x4

    if-ne p1, v2, :cond_2

    iget v2, p0, Lcom/android/inputmethod/research/Statistics;->mDeleteKeyCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mDeleteKeyCount:I

    cmp-long v2, v0, v4

    if-gez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/inputmethod/research/Statistics;->mIsLastKeyDeleteKey:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/research/Statistics;->mDuringRepeatedDeleteKeysCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v2, v0, v1}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->add(J)V

    :cond_0
    :goto_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/inputmethod/research/Statistics;->mIsLastKeyDeleteKey:Z

    :goto_1
    iput-wide p2, p0, Lcom/android/inputmethod/research/Statistics;->mLastTapTime:J

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/research/Statistics;->mBeforeDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v2, v0, v1}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->add(J)V

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/android/inputmethod/research/Statistics;->mCharCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mCharCount:I

    invoke-static {p1}, Ljava/lang/Character;->isDigit(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/android/inputmethod/research/Statistics;->mNumberCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mNumberCount:I

    :cond_3
    invoke-static {p1}, Ljava/lang/Character;->isLetter(I)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/android/inputmethod/research/Statistics;->mLetterCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mLetterCount:I

    :cond_4
    invoke-static {p1}, Ljava/lang/Character;->isSpaceChar(I)Z

    move-result v2

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/android/inputmethod/research/Statistics;->mSpaceCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mSpaceCount:I

    :cond_5
    iget-boolean v2, p0, Lcom/android/inputmethod/research/Statistics;->mIsLastKeyDeleteKey:Z

    if-eqz v2, :cond_7

    cmp-long v2, v0, v4

    if-gez v2, :cond_7

    iget-object v2, p0, Lcom/android/inputmethod/research/Statistics;->mAfterDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v2, v0, v1}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->add(J)V

    :cond_6
    :goto_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/inputmethod/research/Statistics;->mIsLastKeyDeleteKey:Z

    goto :goto_1

    :cond_7
    iget-boolean v2, p0, Lcom/android/inputmethod/research/Statistics;->mIsLastKeyDeleteKey:Z

    if-nez v2, :cond_6

    const-wide/16 v2, 0x7d0

    cmp-long v2, v0, v2

    if-gez v2, :cond_6

    iget-object v2, p0, Lcom/android/inputmethod/research/Statistics;->mKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v2, v0, v1}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->add(J)V

    goto :goto_2
.end method

.method public recordWordEntered()V
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/research/Statistics;->mWordCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/inputmethod/research/Statistics;->mWordCount:I

    return-void
.end method

.method public reset()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mCharCount:I

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mLetterCount:I

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mNumberCount:I

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mSpaceCount:I

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mDeleteKeyCount:I

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mWordCount:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/research/Statistics;->mIsEmptyUponStarting:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/research/Statistics;->mIsEmptinessStateKnown:Z

    iget-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->reset()V

    iget-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mBeforeDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->reset()V

    iget-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mDuringRepeatedDeleteKeysCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->reset()V

    iget-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mAfterDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->reset()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/inputmethod/research/Statistics;->mLastTapTime:J

    iput-boolean v2, p0, Lcom/android/inputmethod/research/Statistics;->mIsLastKeyDeleteKey:Z

    return-void
.end method

.method public setIsEmptyUponStarting(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/research/Statistics;->mIsEmptyUponStarting:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/research/Statistics;->mIsEmptinessStateKnown:Z

    return-void
.end method
