.class Lcom/android/inputmethod/research/ResearchLog$1;
.super Ljava/lang/Object;
.source "ResearchLog.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/inputmethod/research/ResearchLog;->close(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/research/ResearchLog;

.field final synthetic val$onClosed:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/research/ResearchLog;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    iput-object p2, p0, Lcom/android/inputmethod/research/ResearchLog$1;->val$onClosed:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v3, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    invoke-static {v1}, Lcom/android/inputmethod/research/ResearchLog;->access$100(Lcom/android/inputmethod/research/ResearchLog;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    invoke-static {v1}, Lcom/android/inputmethod/research/ResearchLog;->access$200(Lcom/android/inputmethod/research/ResearchLog;)Landroid/util/JsonWriter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    invoke-static {v1}, Lcom/android/inputmethod/research/ResearchLog;->access$200(Lcom/android/inputmethod/research/ResearchLog;)Landroid/util/JsonWriter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/JsonWriter;->flush()V

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    invoke-static {v1}, Lcom/android/inputmethod/research/ResearchLog;->access$200(Lcom/android/inputmethod/research/ResearchLog;)Landroid/util/JsonWriter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/JsonWriter;->close()V

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/inputmethod/research/ResearchLog;->access$102(Lcom/android/inputmethod/research/ResearchLog;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    iget-object v1, v1, Lcom/android/inputmethod/research/ResearchLog;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    iget-object v1, v1, Lcom/android/inputmethod/research/ResearchLog;->mFile:Ljava/io/File;

    invoke-virtual {v1, v3, v3}, Ljava/io/File;->setWritable(ZZ)Z

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->val$onClosed:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->val$onClosed:Ljava/lang/Runnable;

    :goto_0
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    :cond_2
    const/4 v1, 0x0

    return-object v1

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {}, Lcom/android/inputmethod/research/ResearchLog;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "error when closing ResearchLog:"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    iget-object v1, v1, Lcom/android/inputmethod/research/ResearchLog;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    iget-object v1, v1, Lcom/android/inputmethod/research/ResearchLog;->mFile:Ljava/io/File;

    invoke-virtual {v1, v3, v3}, Ljava/io/File;->setWritable(ZZ)Z

    :cond_3
    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->val$onClosed:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/inputmethod/research/ResearchLog$1;->val$onClosed:Ljava/lang/Runnable;

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    iget-object v2, v2, Lcom/android/inputmethod/research/ResearchLog;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLog$1;->this$0:Lcom/android/inputmethod/research/ResearchLog;

    iget-object v2, v2, Lcom/android/inputmethod/research/ResearchLog;->mFile:Ljava/io/File;

    invoke-virtual {v2, v3, v3}, Ljava/io/File;->setWritable(ZZ)Z

    :cond_4
    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLog$1;->val$onClosed:Ljava/lang/Runnable;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/inputmethod/research/ResearchLog$1;->val$onClosed:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    :cond_5
    throw v1
.end method
