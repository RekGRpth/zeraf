.class public final Lcom/android/inputmethod/latin/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final auto_correction_threshold_mode_indexes:I = 0x7f0e0003

.field public static final auto_correction_threshold_modes:I = 0x7f0e0004

.field public static final auto_correction_threshold_values:I = 0x7f0e0000

.field public static final keyboard_heights:I = 0x7f0e000f

.field public static final keyboard_layout_modes:I = 0x7f0e0008

.field public static final keyboard_layout_modes_values:I = 0x7f0e0009

.field public static final keypress_vibration_durations:I = 0x7f0e0010

.field public static final keypress_volumes:I = 0x7f0e0011

.field public static final locale_and_extra_value_to_keyboard_layout_set_map:I = 0x7f0e000e

.field public static final phantom_sudden_move_event_device_list:I = 0x7f0e0012

.field public static final predefined_layout_display_names:I = 0x7f0e000d

.field public static final predefined_layouts:I = 0x7f0e000c

.field public static final predefined_subtypes:I = 0x7f0e0013

.field public static final prefs_suggestion_visibilities:I = 0x7f0e0002

.field public static final prefs_suggestion_visibility_values:I = 0x7f0e0001

.field public static final subtype_locale_exception_keys:I = 0x7f0e000a

.field public static final subtype_locale_exception_values:I = 0x7f0e000b

.field public static final sudden_jumping_touch_event_device_list:I = 0x7f0e0014

.field public static final touch_position_correction_data_default:I = 0x7f0e0015

.field public static final touch_position_correction_data_gingerbread:I = 0x7f0e0016

.field public static final touch_position_correction_data_ice_cream_sandwich:I = 0x7f0e0017

.field public static final voice_input_modes:I = 0x7f0e0006

.field public static final voice_input_modes_summary:I = 0x7f0e0007

.field public static final voice_input_modes_values:I = 0x7f0e0005


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
