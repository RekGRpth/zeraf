.class public final Lcom/android/inputmethod/latin/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final additionalMoreKeys:I = 0x7f010075

.field public static final alphaAutoCorrect:I = 0x7f01004f

.field public static final alphaObsoleted:I = 0x7f010051

.field public static final alphaSuggested:I = 0x7f010050

.field public static final alphaTypedWord:I = 0x7f01004e

.field public static final alphaValidTypedWord:I = 0x7f01004d

.field public static final altCode:I = 0x7f010073

.field public static final altCodeKeyWhileTypingFadeinAnimator:I = 0x7f010031

.field public static final altCodeKeyWhileTypingFadeoutAnimator:I = 0x7f010030

.field public static final autoCorrectionSpacebarLedEnabled:I = 0x7f010029

.field public static final autoCorrectionSpacebarLedIcon:I = 0x7f01002a

.field public static final backgroundDimAlpha:I = 0x7f01001a

.field public static final backgroundType:I = 0x7f010077

.field public static final centerSuggestionPercentile:I = 0x7f010053

.field public static final clobberSettingsKey:I = 0x7f01009c

.field public static final code:I = 0x7f010072

.field public static final colorAutoCorrect:I = 0x7f01004b

.field public static final colorSuggested:I = 0x7f01004c

.field public static final colorTypedWord:I = 0x7f01004a

.field public static final colorValidTypedWord:I = 0x7f010049

.field public static final countryCode:I = 0x7f0100a4

.field public static final elementKeyboard:I = 0x7f0100a8

.field public static final elementName:I = 0x7f0100a7

.field public static final enableProximityCharsCorrection:I = 0x7f0100a9

.field public static final gestureDetectFastMoveSpeedThreshold:I = 0x7f01003e

.field public static final gestureDynamicDistanceThresholdFrom:I = 0x7f010042

.field public static final gestureDynamicDistanceThresholdTo:I = 0x7f010043

.field public static final gestureDynamicThresholdDecayDuration:I = 0x7f01003f

.field public static final gestureDynamicTimeThresholdFrom:I = 0x7f010040

.field public static final gestureDynamicTimeThresholdTo:I = 0x7f010041

.field public static final gestureFloatingPreviewColor:I = 0x7f01001e

.field public static final gestureFloatingPreviewHorizontalPadding:I = 0x7f01001f

.field public static final gestureFloatingPreviewRoundRadius:I = 0x7f010021

.field public static final gestureFloatingPreviewTextColor:I = 0x7f01001c

.field public static final gestureFloatingPreviewTextLingerTimeout:I = 0x7f010022

.field public static final gestureFloatingPreviewTextOffset:I = 0x7f01001d

.field public static final gestureFloatingPreviewTextSize:I = 0x7f01001b

.field public static final gestureFloatingPreviewVerticalPadding:I = 0x7f010020

.field public static final gesturePreviewTrailColor:I = 0x7f010026

.field public static final gesturePreviewTrailEndWidth:I = 0x7f010028

.field public static final gesturePreviewTrailFadeoutDuration:I = 0x7f010024

.field public static final gesturePreviewTrailFadeoutStartDelay:I = 0x7f010023

.field public static final gesturePreviewTrailStartWidth:I = 0x7f010027

.field public static final gesturePreviewTrailUpdateInterval:I = 0x7f010025

.field public static final gestureRecognitionMinimumTime:I = 0x7f010045

.field public static final gestureRecognitionSpeedThreshold:I = 0x7f010046

.field public static final gestureSamplingMinimumDistance:I = 0x7f010044

.field public static final gestureStaticTimeThresholdAfterFastTyping:I = 0x7f01003d

.field public static final hasShortcutKey:I = 0x7f01009e

.field public static final horizontalGap:I = 0x7f01005f

.field public static final iconDeleteKey:I = 0x7f010063

.field public static final iconEnterKey:I = 0x7f010066

.field public static final iconLanguageSwitchKey:I = 0x7f01006f

.field public static final iconSearchKey:I = 0x7f010067

.field public static final iconSettingsKey:I = 0x7f010064

.field public static final iconShiftKey:I = 0x7f010062

.field public static final iconShiftKeyShifted:I = 0x7f01006c

.field public static final iconShortcutForLabel:I = 0x7f01006a

.field public static final iconShortcutKey:I = 0x7f010069

.field public static final iconShortcutKeyDisabled:I = 0x7f01006d

.field public static final iconSpaceKey:I = 0x7f010065

.field public static final iconSpaceKeyForNumberLayout:I = 0x7f01006b

.field public static final iconTabKey:I = 0x7f010068

.field public static final iconTabKeyPreview:I = 0x7f01006e

.field public static final iconZwjKey:I = 0x7f010071

.field public static final iconZwnjKey:I = 0x7f010070

.field public static final ignoreAltCodeKeyTimeout:I = 0x7f01003b

.field public static final imeAction:I = 0x7f0100a1

.field public static final isMultiLine:I = 0x7f0100a0

.field public static final keyActionFlags:I = 0x7f010078

.field public static final keyBackground:I = 0x7f01000b

.field public static final keyHintLabel:I = 0x7f01007b

.field public static final keyHintLabelColor:I = 0x7f010091

.field public static final keyHintLabelRatio:I = 0x7f01008b

.field public static final keyHintLetterColor:I = 0x7f010090

.field public static final keyHintLetterPadding:I = 0x7f01000d

.field public static final keyHintLetterRatio:I = 0x7f01008a

.field public static final keyHysteresisDistance:I = 0x7f010032

.field public static final keyHysteresisDistanceForSlidingModifier:I = 0x7f010033

.field public static final keyIcon:I = 0x7f01007d

.field public static final keyIconDisabled:I = 0x7f01007e

.field public static final keyIconPreview:I = 0x7f01007f

.field public static final keyLabel:I = 0x7f01007a

.field public static final keyLabelFlags:I = 0x7f01007c

.field public static final keyLabelHorizontalPadding:I = 0x7f01000c

.field public static final keyLabelSize:I = 0x7f010087

.field public static final keyLargeLabelRatio:I = 0x7f010089

.field public static final keyLargeLetterRatio:I = 0x7f010088

.field public static final keyLetterSize:I = 0x7f010086

.field public static final keyOutputText:I = 0x7f010079

.field public static final keyPopupHintLetterPadding:I = 0x7f01000e

.field public static final keyPreviewHeight:I = 0x7f010016

.field public static final keyPreviewLayout:I = 0x7f010011

.field public static final keyPreviewLingerTimeout:I = 0x7f010017

.field public static final keyPreviewOffset:I = 0x7f010015

.field public static final keyPreviewTextColor:I = 0x7f010094

.field public static final keyPreviewTextRatio:I = 0x7f010095

.field public static final keyRepeatInterval:I = 0x7f010038

.field public static final keyRepeatStartTimeout:I = 0x7f010037

.field public static final keyShiftedLetterHintActivatedColor:I = 0x7f010093

.field public static final keyShiftedLetterHintInactivatedColor:I = 0x7f010092

.field public static final keyShiftedLetterHintPadding:I = 0x7f01000f

.field public static final keyShiftedLetterHintRatio:I = 0x7f01008c

.field public static final keyStyle:I = 0x7f010080

.field public static final keyTextColor:I = 0x7f01008d

.field public static final keyTextInactivatedColor:I = 0x7f01008f

.field public static final keyTextShadowColor:I = 0x7f01008e

.field public static final keyTextShadowRadius:I = 0x7f010010

.field public static final keyTypeface:I = 0x7f010085

.field public static final keyWidth:I = 0x7f010083

.field public static final keyXPos:I = 0x7f010084

.field public static final keyboardBottomPadding:I = 0x7f01005c

.field public static final keyboardHeight:I = 0x7f010058

.field public static final keyboardHorizontalEdgesPadding:I = 0x7f01005d

.field public static final keyboardLayout:I = 0x7f010096

.field public static final keyboardLayoutSetElement:I = 0x7f010097

.field public static final keyboardStyle:I = 0x7f010000

.field public static final keyboardTopPadding:I = 0x7f01005b

.field public static final keyboardViewStyle:I = 0x7f010001

.field public static final languageCode:I = 0x7f0100a3

.field public static final languageOnSpacebarFadeoutAnimator:I = 0x7f01002f

.field public static final languageOnSpacebarFinalAlpha:I = 0x7f01002e

.field public static final languageSwitchKeyEnabled:I = 0x7f01009f

.field public static final localeCode:I = 0x7f0100a2

.field public static final longPressKeyTimeout:I = 0x7f010039

.field public static final longPressShiftKeyTimeout:I = 0x7f01003a

.field public static final mainKeyboardViewStyle:I = 0x7f010002

.field public static final maxKeyboardHeight:I = 0x7f010059

.field public static final maxMoreKeysColumn:I = 0x7f010076

.field public static final maxMoreSuggestionsRow:I = 0x7f010054

.field public static final minKeyboardHeight:I = 0x7f01005a

.field public static final minMoreSuggestionsWidth:I = 0x7f010055

.field public static final mode:I = 0x7f010098

.field public static final moreKeys:I = 0x7f010074

.field public static final moreKeysKeyboardPanelStyle:I = 0x7f010005

.field public static final moreKeysKeyboardStyle:I = 0x7f010003

.field public static final moreKeysKeyboardViewStyle:I = 0x7f010004

.field public static final moreKeysLayout:I = 0x7f010019

.field public static final moreKeysTemplate:I = 0x7f010061

.field public static final moreSuggestionsViewStyle:I = 0x7f010008

.field public static final navigateNext:I = 0x7f010099

.field public static final navigatePrevious:I = 0x7f01009a

.field public static final parentStyle:I = 0x7f0100a6

.field public static final passwordInput:I = 0x7f01009b

.field public static final rowHeight:I = 0x7f01005e

.field public static final shortcutKeyEnabled:I = 0x7f01009d

.field public static final showMoreKeysKeyboardAtTouchedPoint:I = 0x7f01003c

.field public static final slidingKeyInputEnable:I = 0x7f010036

.field public static final spacebarTextColor:I = 0x7f01002c

.field public static final spacebarTextRatio:I = 0x7f01002b

.field public static final spacebarTextShadowColor:I = 0x7f01002d

.field public static final state_has_morekeys:I = 0x7f010014

.field public static final state_left_edge:I = 0x7f010012

.field public static final state_right_edge:I = 0x7f010013

.field public static final styleName:I = 0x7f0100a5

.field public static final suggestionBackgroundStyle:I = 0x7f010009

.field public static final suggestionPreviewBackgroundStyle:I = 0x7f01000a

.field public static final suggestionStripOption:I = 0x7f010048

.field public static final suggestionStripViewStyle:I = 0x7f010007

.field public static final suggestionsCountInStrip:I = 0x7f010052

.field public static final suggestionsStripBackgroundStyle:I = 0x7f010006

.field public static final suppressKeyPreviewAfterBatchInputDuration:I = 0x7f010047

.field public static final themeId:I = 0x7f010056

.field public static final touchNoiseThresholdDistance:I = 0x7f010035

.field public static final touchNoiseThresholdTime:I = 0x7f010034

.field public static final touchPositionCorrectionData:I = 0x7f010057

.field public static final verticalCorrection:I = 0x7f010018

.field public static final verticalGap:I = 0x7f010060

.field public static final visualInsetsLeft:I = 0x7f010081

.field public static final visualInsetsRight:I = 0x7f010082


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
