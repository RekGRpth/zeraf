.class public final Lcom/android/inputmethod/latin/R$fraction;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "fraction"
.end annotation


# static fields
.field public static final center_suggestion_percentile:I = 0x7f0d0024

.field public static final config_gesture_detect_fast_move_speed_threshold:I = 0x7f0d0000

.field public static final config_gesture_dynamic_distance_threshold_from:I = 0x7f0d0001

.field public static final config_gesture_dynamic_distance_threshold_to:I = 0x7f0d0002

.field public static final config_gesture_recognition_speed_threshold:I = 0x7f0d0004

.field public static final config_gesture_sampling_minimum_distance:I = 0x7f0d0003

.field public static final key_bottom_gap:I = 0x7f0d000a

.field public static final key_bottom_gap_5row:I = 0x7f0d001f

.field public static final key_bottom_gap_gb:I = 0x7f0d0010

.field public static final key_bottom_gap_ics:I = 0x7f0d0014

.field public static final key_bottom_gap_stone:I = 0x7f0d000e

.field public static final key_hint_label_ratio:I = 0x7f0d001b

.field public static final key_hint_letter_ratio:I = 0x7f0d001a

.field public static final key_horizontal_gap:I = 0x7f0d000b

.field public static final key_horizontal_gap_gb:I = 0x7f0d0011

.field public static final key_horizontal_gap_ics:I = 0x7f0d0015

.field public static final key_horizontal_gap_stone:I = 0x7f0d000f

.field public static final key_label_ratio:I = 0x7f0d0018

.field public static final key_large_label_ratio:I = 0x7f0d0019

.field public static final key_large_letter_ratio:I = 0x7f0d0017

.field public static final key_letter_ratio:I = 0x7f0d0016

.field public static final key_letter_ratio_5row:I = 0x7f0d0020

.field public static final key_preview_text_ratio:I = 0x7f0d001d

.field public static final key_uppercase_letter_ratio:I = 0x7f0d001c

.field public static final key_uppercase_letter_ratio_5row:I = 0x7f0d0021

.field public static final keyboard_bottom_padding:I = 0x7f0d0008

.field public static final keyboard_bottom_padding_ics:I = 0x7f0d0013

.field public static final keyboard_bottom_padding_stone:I = 0x7f0d000d

.field public static final keyboard_horizontal_edges_padding:I = 0x7f0d0009

.field public static final keyboard_top_padding:I = 0x7f0d0007

.field public static final keyboard_top_padding_ics:I = 0x7f0d0012

.field public static final keyboard_top_padding_stone:I = 0x7f0d000c

.field public static final maxKeyboardHeight:I = 0x7f0d0005

.field public static final minKeyboardHeight:I = 0x7f0d0006

.field public static final min_more_suggestions_width:I = 0x7f0d0022

.field public static final more_suggestions_info_ratio:I = 0x7f0d0023

.field public static final spacebar_text_ratio:I = 0x7f0d001e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
