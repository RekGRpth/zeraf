.class public final Lcom/android/inputmethod/latin/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final accessibility_edge_slop:I = 0x7f0c0024

.field public static final config_key_hysteresis_distance:I = 0x7f0c0000

.field public static final config_key_hysteresis_distance_for_sliding_modifier:I = 0x7f0c0001

.field public static final config_touch_noise_threshold_distance:I = 0x7f0c0002

.field public static final gesture_floating_preview_horizontal_padding:I = 0x7f0c0021

.field public static final gesture_floating_preview_round_radius:I = 0x7f0c0023

.field public static final gesture_floating_preview_text_offset:I = 0x7f0c0020

.field public static final gesture_floating_preview_text_size:I = 0x7f0c001f

.field public static final gesture_floating_preview_vertical_padding:I = 0x7f0c0022

.field public static final gesture_preview_trail_end_width:I = 0x7f0c001e

.field public static final gesture_preview_trail_start_width:I = 0x7f0c001d

.field public static final gesture_preview_trail_width:I = 0x7f0c0025

.field public static final key_hint_letter_padding:I = 0x7f0c000d

.field public static final key_label_horizontal_padding:I = 0x7f0c000c

.field public static final key_popup_hint_letter_padding:I = 0x7f0c000e

.field public static final key_preview_height:I = 0x7f0c000a

.field public static final key_preview_offset:I = 0x7f0c000b

.field public static final key_preview_offset_ics:I = 0x7f0c0010

.field public static final key_uppercase_letter_padding:I = 0x7f0c000f

.field public static final keyboardHeight:I = 0x7f0c0003

.field public static final keyboard_vertical_correction:I = 0x7f0c0009

.field public static final more_keys_keyboard_horizontal_edges_padding_ics:I = 0x7f0c0006

.field public static final more_keys_keyboard_key_horizontal_padding:I = 0x7f0c0005

.field public static final more_keys_keyboard_slide_allowance:I = 0x7f0c0007

.field public static final more_keys_keyboard_vertical_correction:I = 0x7f0c0008

.field public static final more_keys_keyboard_vertical_correction_ics:I = 0x7f0c0011

.field public static final more_suggestions_bottom_gap:I = 0x7f0c0015

.field public static final more_suggestions_hint_text_size:I = 0x7f0c001c

.field public static final more_suggestions_key_horizontal_padding:I = 0x7f0c0013

.field public static final more_suggestions_modal_tolerance:I = 0x7f0c0016

.field public static final more_suggestions_row_height:I = 0x7f0c0014

.field public static final more_suggestions_slide_allowance:I = 0x7f0c0017

.field public static final popup_key_height:I = 0x7f0c0004

.field public static final suggestion_min_width:I = 0x7f0c0019

.field public static final suggestion_padding:I = 0x7f0c001a

.field public static final suggestion_text_size:I = 0x7f0c001b

.field public static final suggestions_strip_height:I = 0x7f0c0012

.field public static final suggestions_strip_padding:I = 0x7f0c0018


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
