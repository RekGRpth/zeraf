.class public final Lcom/android/inputmethod/latin/ResizableIntArray;
.super Ljava/lang/Object;
.source "ResizableIntArray.java"


# instance fields
.field private mArray:[I

.field private mLength:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/latin/ResizableIntArray;->reset(I)V

    return-void
.end method

.method private calculateCapacity(I)I
    .locals 3
    .param p1    # I

    iget-object v2, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    array-length v0, v2

    if-ge v0, p1, :cond_1

    mul-int/lit8 v1, v0, 0x2

    if-le p1, v1, :cond_0

    :goto_0
    return p1

    :cond_0
    move p1, v1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private ensureCapacity(I)V
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/ResizableIntArray;->calculateCapacity(I)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    :cond_0
    return-void
.end method


# virtual methods
.method public add(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->ensureCapacity(I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    aput p1, v1, v0

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    return-void
.end method

.method public add(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    aput p2, v0, p1

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    invoke-virtual {p0, p2}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    goto :goto_0
.end method

.method public append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/latin/ResizableIntArray;
    .param p2    # I
    .param p3    # I

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    add-int v1, v0, p3

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->ensureCapacity(I)V

    iget-object v2, p1, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    iget-object v3, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    invoke-static {v2, p2, v3, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    goto :goto_0
.end method

.method public copy(Lcom/android/inputmethod/latin/ResizableIntArray;)V
    .locals 5
    .param p1    # Lcom/android/inputmethod/latin/ResizableIntArray;

    const/4 v4, 0x0

    iget v1, p1, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->calculateCapacity(I)I

    move-result v0

    if-lez v0, :cond_0

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    :cond_0
    iget-object v1, p1, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    iget-object v2, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    iget v3, p1, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p1, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    iput v1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    return-void
.end method

.method public fill(III)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    if-ltz p2, :cond_0

    if-gez p3, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startPos="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; length="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    add-int v0, p2, p3

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->ensureCapacity(I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    invoke-static {v1, p2, v0, p1}, Ljava/util/Arrays;->fill([IIII)V

    iget v1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    if-ge v1, v0, :cond_2

    iput v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    :cond_2
    return-void
.end method

.method public get(I)I
    .locals 3
    .param p1    # I

    iget v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    aget v0, v0, p1

    return v0

    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; index="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getLength()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    return v0
.end method

.method public getPrimitiveArray()[I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    return-object v0
.end method

.method public reset(I)V
    .locals 1
    .param p1    # I

    new-array v0, p1, [I

    iput-object v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    return-void
.end method

.method public set(Lcom/android/inputmethod/latin/ResizableIntArray;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v0, p1, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    iput-object v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    iget v0, p1, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    iput v0, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    return-void
.end method

.method public setLength(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/ResizableIntArray;->ensureCapacity(I)V

    iput p1, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mLength:I

    if-ge v0, v2, :cond_1

    if-eqz v0, :cond_0

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/ResizableIntArray;->mArray:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
