.class public final Lcom/android/inputmethod/latin/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/Constants$Dictionary;,
        Lcom/android/inputmethod/latin/Constants$TextUtils;,
        Lcom/android/inputmethod/latin/Constants$Subtype;,
        Lcom/android/inputmethod/latin/Constants$ImeOption;,
        Lcom/android/inputmethod/latin/Constants$Color;
    }
.end annotation


# static fields
.field public static final NOT_A_CODE:I = -0x1

.field public static final NOT_A_COORDINATE:I = -0x1

.field public static final SPELL_CHECKER_COORDINATE:I = -0x3

.field public static final SUGGESTION_STRIP_COORDINATE:I = -0x2


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
