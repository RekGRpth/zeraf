.class public final Lcom/android/inputmethod/latin/LatinIME;
.super Landroid/inputmethodservice/InputMethodService;
.source "LatinIME.java"

# interfaces
.implements Lcom/android/inputmethod/keyboard/KeyboardActionListener;
.implements Lcom/android/inputmethod/latin/Suggest$SuggestInitializationListener;
.implements Lcom/android/inputmethod/latin/TargetApplicationGetter$OnTargetApplicationKnownListener;
.implements Lcom/android/inputmethod/latin/suggestions/SuggestionStripView$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/latin/LatinIME$BatchInputUpdater;,
        Lcom/android/inputmethod/latin/LatinIME$SubtypeState;,
        Lcom/android/inputmethod/latin/LatinIME$UIHandler;
    }
.end annotation


# static fields
.field public static final CODE_SHOW_INPUT_METHOD_PICKER:I = 0x1

.field private static DEBUG:Z = false

.field private static final DELETE_ACCELERATE_AT:I = 0x14

.field private static final EXTENDED_TOUCHABLE_REGION_HEIGHT:I = 0x64

.field private static final NOT_A_CURSOR_POSITION:I = -0x1

.field private static final PENDING_IMS_CALLBACK_DURATION:I = 0x320

.field private static final QUICK_PRESS:I = 0xc8

.field private static final SCHEME_PACKAGE:Ljava/lang/String; = "package"

.field private static final SPACE_STATE_DOUBLE:I = 0x1

.field private static final SPACE_STATE_NONE:I = 0x0

.field private static final SPACE_STATE_PHANTOM:I = 0x4

.field private static final SPACE_STATE_SWAP_PUNCTUATION:I = 0x2

.field private static final SPACE_STATE_WEAK:I = 0x3

.field private static final TAG:Ljava/lang/String;

.field private static final TRACE:Z = false

.field private static final TRIM_ON_LOW_MEMORY:Z = true


# instance fields
.field private mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

.field private mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

.field private mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

.field private mDeleteCount:I

.field private mDictionaryPackInstallReceiver:Landroid/content/BroadcastReceiver;

.field private mDisplayOrientation:I

.field private mEnteredText:Ljava/lang/CharSequence;

.field private mExpectingUpdateSelection:Z

.field private mExtractArea:Landroid/view/View;

.field private mFeedbackManager:Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;

.field public final mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

.field private mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

.field private mIsAutoCorrectionIndicatorOn:Z

.field private mIsConfigurationChanged:Z

.field private final mIsHardwareAcceleratedDrawingEnabled:Z

.field private mIsMainDictionaryAvailable:Z

.field private mIsUserDictionaryAvailable:Z

.field private mKeyPreviewBackingView:Landroid/view/View;

.field final mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

.field private mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

.field private mLastKeyTime:J

.field private mLastSelectionEnd:I

.field private mLastSelectionStart:I

.field private mOptionsDialog:Landroid/app/AlertDialog;

.field private mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mResources:Landroid/content/res/Resources;

.field private mSpaceState:I

.field private final mSubtypeState:Lcom/android/inputmethod/latin/LatinIME$SubtypeState;

.field private final mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

.field mSuggest:Lcom/android/inputmethod/latin/Suggest;

.field private mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

.field private mSuggestionsContainer:Landroid/view/View;

.field private mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

.field private mUserDictionary:Lcom/android/inputmethod/latin/UserBinaryDictionary;

.field private mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

.field private final mWordComposer:Lcom/android/inputmethod/latin/WordComposer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/LatinIME;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v1, -0x1

    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;-><init>()V

    new-instance v0, Lcom/android/inputmethod/latin/LatinIME$SubtypeState;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/LatinIME$SubtypeState;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeState:Lcom/android/inputmethod/latin/LatinIME$SubtypeState;

    sget-object v0, Lcom/android/inputmethod/latin/LastComposedWord;->NOT_A_COMPOSED_WORD:Lcom/android/inputmethod/latin/LastComposedWord;

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    new-instance v0, Lcom/android/inputmethod/latin/WordComposer;

    invoke-direct {v0}, Lcom/android/inputmethod/latin/WordComposer;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    new-instance v0, Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/RichInputConnection;-><init>(Landroid/inputmethodservice/InputMethodService;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    new-instance v0, Lcom/android/inputmethod/latin/DictionaryPackInstallBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/DictionaryPackInstallBroadcastReceiver;-><init>(Lcom/android/inputmethod/latin/LatinIME;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mDictionaryPackInstallReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;-><init>(Lcom/android/inputmethod/latin/LatinIME;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsConfigurationChanged:Z

    new-instance v0, Lcom/android/inputmethod/latin/LatinIME$2;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/latin/LatinIME$2;-><init>(Lcom/android/inputmethod/latin/LatinIME;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getInstance()Lcom/android/inputmethod/latin/SubtypeSwitcher;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-static {}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getInstance()Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-static {p0}, Lcom/android/inputmethod/compat/InputMethodServiceCompatUtils;->enableHardwareAcceleration(Landroid/inputmethodservice/InputMethodService;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsHardwareAcceleratedDrawingEnabled:Z

    sget-object v0, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Hardware accelerated drawing: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsHardwareAcceleratedDrawingEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateSuggestionStrip()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/inputmethod/latin/LatinIME;Lcom/android/inputmethod/latin/SuggestedWords;Z)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME;->showGesturePreviewAndSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/inputmethod/latin/LatinIME;)Lcom/android/inputmethod/latin/SubtypeSwitcher;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/inputmethod/latin/LatinIME;)Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mFeedbackManager:Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->launchSettings()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/inputmethod/latin/LatinIME;Z)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->onFinishInputViewInternal(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/inputmethod/latin/LatinIME;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->onFinishInputInternal()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME;->onStartInputInternal(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/inputmethod/latin/LatinIME;Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME;->onStartInputViewInternal(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/inputmethod/latin/LatinIME;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/inputmethod/latin/LatinIME;)Lcom/android/inputmethod/latin/WordComposer;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/inputmethod/latin/LatinIME;I)Lcom/android/inputmethod/latin/SuggestedWords;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/LatinIME;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->getSuggestedWords(I)Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v0

    return-object v0
.end method

.method private addToUserHistoryDictionary(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 8
    .param p1    # Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v1, v4

    :goto_0
    return-object v1

    :cond_0
    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-nez v5, :cond_1

    move-object v1, v4

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v5, v5, Lcom/android/inputmethod/latin/SettingsValues;->mCorrectionEnabled:Z

    if-nez v5, :cond_2

    move-object v1, v4

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    if-eqz v3, :cond_7

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v6, v6, Lcom/android/inputmethod/latin/SettingsValues;->mWordSeparators:Ljava/lang/String;

    const/4 v7, 0x2

    invoke-virtual {v5, v6, v7}, Lcom/android/inputmethod/latin/RichInputConnection;->getNthPreviousWord(Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v5}, Lcom/android/inputmethod/latin/WordComposer;->wasAutoCapitalized()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v5}, Lcom/android/inputmethod/latin/WordComposer;->isMostlyCaps()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v5}, Lcom/android/inputmethod/latin/Suggest;->getUnigramDictionaries()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/android/inputmethod/latin/AutoCorrection;->getMaxFrequency(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/CharSequence;)I

    move-result v0

    if-nez v0, :cond_4

    move-object v1, v4

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    if-nez v1, :cond_5

    move-object v5, v4

    :goto_2
    if-lez v0, :cond_6

    const/4 v4, 0x1

    :goto_3
    invoke-virtual {v3, v5, v2, v4}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->addToUserHistory(Ljava/lang/String;Ljava/lang/String;Z)I

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    goto :goto_3

    :cond_7
    move-object v1, v4

    goto :goto_0
.end method

.method private static canBeFollowedByPeriod(I)Z
    .locals 1
    .param p0    # I

    invoke-static {p0}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x27

    if-eq p0, v0, :cond_0

    const/16 v0, 0x22

    if-eq p0, v0, :cond_0

    const/16 v0, 0x29

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3e

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private clearSuggestionStrip()V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/android/inputmethod/latin/SuggestedWords;->EMPTY:Lcom/android/inputmethod/latin/SuggestedWords;

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Z)V

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setAutoCorrectionIndicator(Z)V

    return-void
.end method

.method private commitChosenWord(Ljava/lang/CharSequence;ILjava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->getSuggestions()Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget-boolean v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsMainDictionaryAvailable:Z

    invoke-static {p0, p1, v1, v3}, Lcom/android/inputmethod/compat/SuggestionSpanUtils;->getTextWithSuggestionSpan(Landroid/content/Context;Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/SuggestedWords;Z)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/android/inputmethod/latin/RichInputConnection;->commitText(Ljava/lang/CharSequence;I)V

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->addToUserHistoryDictionary(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3, p3, v0}, Lcom/android/inputmethod/latin/WordComposer;->commitWord(ILjava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/android/inputmethod/latin/LastComposedWord;

    move-result-object v2

    iput-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    return-void
.end method

.method private commitCurrentAutoCorrection(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->hasPendingUpdateSuggestions()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateSuggestionStrip()V

    :cond_0
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/WordComposer;->getAutoCorrectionOrNull()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_1

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "We have an auto-correction but the typed word is empty? Impossible! I must commit suicide."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    move-object v0, v2

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    const/4 v3, 0x2

    invoke-direct {p0, v0, v3, p1}, Lcom/android/inputmethod/latin/LatinIME;->commitChosenWord(Ljava/lang/CharSequence;ILjava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    new-instance v4, Landroid/view/inputmethod/CorrectionInfo;

    iget v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-direct {v4, v5, v2, v0}, Landroid/view/inputmethod/CorrectionInfo;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/latin/RichInputConnection;->commitCorrection(Landroid/view/inputmethod/CorrectionInfo;)V

    :cond_3
    return-void
.end method

.method private commitTyped(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/android/inputmethod/latin/LatinIME;->commitChosenWord(Ljava/lang/CharSequence;ILjava/lang/String;)V

    goto :goto_0
.end method

.method private static getActionId(Lcom/android/inputmethod/keyboard/Keyboard;)I
    .locals 1
    .param p0    # Lcom/android/inputmethod/keyboard/Keyboard;

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardId;->imeActionId()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getActualCapsMode()I
    .locals 4

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboardShiftMode()I

    move-result v1

    if-eq v1, v2, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->getCurrentAutoCapsState()I

    move-result v0

    and-int/lit16 v3, v0, 0x1000

    if-eqz v3, :cond_1

    const/4 v1, 0x7

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getAdjustedBackingViewHeight()I
    .locals 10

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v9}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v8

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v1, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {v9, v6}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    iget v4, v6, Landroid/graphics/Rect;->top:I

    sub-int v9, v1, v4

    sub-int/2addr v9, v8

    sub-int v7, v9, v2

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v9, v7}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->setMoreSuggestionsHeight(I)I

    move-result v9

    iput v9, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {v9, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v0, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method private getSuggestedWords(I)Lcom/android/inputmethod/latin/SuggestedWords;
    .locals 9
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/android/inputmethod/latin/SuggestedWords;->EMPTY:Lcom/android/inputmethod/latin/SuggestedWords;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v8

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v3, v0, Lcom/android/inputmethod/latin/SettingsValues;->mWordSeparators:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_1
    invoke-virtual {v1, v3, v0}, Lcom/android/inputmethod/latin/RichInputConnection;->getNthPreviousWord(Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/Keyboard;->getProximityInfo()Lcom/android/inputmethod/keyboard/ProximityInfo;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v4, v4, Lcom/android/inputmethod/latin/SettingsValues;->mCorrectionEnabled:Z

    move v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/inputmethod/latin/Suggest;->getSuggestedWords(Lcom/android/inputmethod/latin/WordComposer;Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/ProximityInfo;ZI)Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v7

    invoke-direct {p0, v8, v7}, Lcom/android/inputmethod/latin/LatinIME;->maybeRetrieveOlderSuggestions(Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/SuggestedWords;)Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private getTextWithUnderline(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsAutoCorrectionIndicatorOn:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/android/inputmethod/compat/SuggestionSpanUtils;->getTextWithAutoCorrectionIndicatorUnderline(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private handleBackspace(I)V
    .locals 6
    .param p1    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateShiftState()V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v2, 0x4

    if-ne v2, p1, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->reset()V

    :goto_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/inputmethod/latin/LatinIME;->getTextWithUnderline(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3, v4}, Lcom/android/inputmethod/latin/RichInputConnection;->setComposingText(Ljava/lang/CharSequence;I)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestionStrip()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->deleteLast()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v2, v4, v5}, Lcom/android/inputmethod/latin/RichInputConnection;->deleteSurroundingText(II)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LastComposedWord;->canRevertCommit()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->revertCommit()V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/latin/RichInputConnection;->sameAsTextBeforeCursor(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v2, v0, v5}, Lcom/android/inputmethod/latin/RichInputConnection;->deleteSurroundingText(II)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_5
    if-ne v4, p1, :cond_8

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelDoubleSpacesTimer()V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/RichInputConnection;->revertDoubleSpace()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_6
    iget v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    if-eq v2, v3, :cond_9

    iget v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    sub-int v1, v2, v3

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    iget v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    invoke-virtual {v2, v3, v4}, Lcom/android/inputmethod/latin/RichInputConnection;->setSelection(II)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v2, v1, v5}, Lcom/android/inputmethod/latin/RichInputConnection;->deleteSurroundingText(II)V

    :cond_7
    :goto_2
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/latin/SettingsValues;->isSuggestionsRequested(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->restartSuggestionsOnWordBeforeCursorIfAtEndOfWord()V

    goto :goto_1

    :cond_8
    const/4 v2, 0x2

    if-ne v2, p1, :cond_6

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/RichInputConnection;->revertSwapPunctuation()Z

    move-result v2

    if-eqz v2, :cond_6

    goto :goto_1

    :cond_9
    const/4 v2, -0x1

    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    if-ne v2, v3, :cond_a

    sget-object v2, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v3, "Backspace when we don\'t know the selection position"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_b

    const/16 v2, 0x43

    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->sendDownUpKeyEventForBackwardCompatibility(I)V

    :goto_3
    iget v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mDeleteCount:I

    const/16 v3, 0x14

    if-le v2, v3, :cond_7

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v2, v4, v5}, Lcom/android/inputmethod/latin/RichInputConnection;->deleteSurroundingText(II)V

    goto :goto_2

    :cond_b
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v2, v4, v5}, Lcom/android/inputmethod/latin/RichInputConnection;->deleteSurroundingText(II)V

    goto :goto_3
.end method

.method private handleCharacter(IIII)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v0

    const/4 v7, 0x4

    if-ne v7, p4, :cond_1

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v7, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isSymbolExcludedFromWordSeparators(I)Z

    move-result v7

    if-nez v7, :cond_1

    if-eqz v0, :cond_0

    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Should not be composing here"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->promotePhantomSpace()V

    :cond_1
    if-nez v0, :cond_3

    invoke-static {p1}, Lcom/android/inputmethod/latin/LatinIME;->isAlphabet(I)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v7, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isSymbolExcludedFromWordSeparators(I)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    invoke-virtual {v7, v8}, Lcom/android/inputmethod/latin/SettingsValues;->isSuggestionsRequested(I)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v7, v8}, Lcom/android/inputmethod/latin/RichInputConnection;->isCursorTouchingWord(Lcom/android/inputmethod/latin/SettingsValues;)Z

    move-result v7

    if-nez v7, :cond_3

    const/16 v7, 0x27

    if-eq v7, p1, :cond_7

    move v0, v5

    :goto_0
    invoke-direct {p0, v6}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    :cond_3
    if-eqz v0, :cond_9

    invoke-static {p2}, Lcom/android/inputmethod/keyboard/KeyboardActionListener$Adapter;->isInvalidCoordinate(I)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-static {p3}, Lcom/android/inputmethod/keyboard/KeyboardActionListener$Adapter;->isInvalidCoordinate(I)Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_4
    move v2, p2

    move v3, p3

    :goto_1
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v6, p1, v2, v3}, Lcom/android/inputmethod/latin/WordComposer;->add(III)V

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v6

    if-ne v6, v5, :cond_5

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->getActualCapsMode()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/inputmethod/latin/WordComposer;->setCapitalizedModeAtStartComposingTime(I)V

    :cond_5
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/inputmethod/latin/LatinIME;->getTextWithUnderline(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Lcom/android/inputmethod/latin/RichInputConnection;->setComposingText(Ljava/lang/CharSequence;I)V

    :cond_6
    :goto_2
    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v5}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestionStrip()V

    return-void

    :cond_7
    move v0, v6

    goto :goto_0

    :cond_8
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->getKeyDetector()Lcom/android/inputmethod/keyboard/KeyDetector;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/android/inputmethod/keyboard/KeyDetector;->getTouchX(I)I

    move-result v2

    invoke-virtual {v1, p3}, Lcom/android/inputmethod/keyboard/KeyDetector;->getTouchY(I)I

    move-result v3

    goto :goto_1

    :cond_9
    const/4 v7, -0x2

    if-ne v7, p2, :cond_b

    :goto_3
    invoke-direct {p0, p1, p4, v5}, Lcom/android/inputmethod/latin/LatinIME;->maybeStripSpace(IIZ)Z

    move-result v4

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->sendKeyCodePoint(I)V

    if-eqz v4, :cond_a

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->swapSwapperAndSpace()V

    const/4 v5, 0x3

    iput v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    :cond_a
    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v5}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->dismissAddToDictionaryHint()Z

    goto :goto_2

    :cond_b
    move v5, v6

    goto :goto_3
.end method

.method private handleClose()V
    .locals 2

    const-string v1, ""

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->commitTyped(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->requestHideSelf(I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->closing()V

    :cond_0
    return-void
.end method

.method private handleLanguageSwitchKey()V
    .locals 3

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getWindow()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget-object v0, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v1, v1, Lcom/android/inputmethod/latin/SettingsValues;->mIncludesOtherImesInLanguageSwitchList:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->switchToNextInputMethod(Landroid/os/IBinder;Z)Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeState:Lcom/android/inputmethod/latin/LatinIME$SubtypeState;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    invoke-virtual {v1, v0, v2, p0}, Lcom/android/inputmethod/latin/LatinIME$SubtypeState;->switchSubtype(Landroid/os/IBinder;Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private handleSeparator(IIII)Z
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v6, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v4}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v4, v4, Lcom/android/inputmethod/latin/SettingsValues;->mCorrectionEnabled:Z

    if-eqz v4, :cond_5

    new-instance v4, Ljava/lang/String;

    new-array v5, v3, [I

    aput p1, v5, v2

    invoke-direct {v4, v5, v2, v3}, Ljava/lang/String;-><init>([III)V

    invoke-direct {p0, v4}, Lcom/android/inputmethod/latin/LatinIME;->commitCurrentAutoCorrection(Ljava/lang/String;)V

    const/4 v0, 0x1

    :cond_0
    :goto_0
    const/4 v4, -0x2

    if-ne v4, p2, :cond_1

    move v2, v3

    :cond_1
    invoke-direct {p0, p1, p4, v2}, Lcom/android/inputmethod/latin/LatinIME;->maybeStripSpace(IIZ)Z

    move-result v1

    if-ne v6, p4, :cond_2

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v2, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isPhantomSpacePromotingSymbol(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->promotePhantomSpace()V

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->sendKeyCodePoint(I)V

    const/16 v2, 0x20

    if-ne v2, p1, :cond_7

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    invoke-virtual {v2, v4}, Lcom/android/inputmethod/latin/SettingsValues;->isSuggestionsRequested(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->maybeDoubleSpace()Z

    move-result v2

    if-eqz v2, :cond_6

    iput v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->startDoubleSpacesTimer()V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/latin/RichInputConnection;->isCursorTouchingWord(Lcom/android/inputmethod/latin/SettingsValues;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestionStrip()V

    :cond_4
    :goto_2
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    return v0

    :cond_5
    new-instance v4, Ljava/lang/String;

    new-array v5, v3, [I

    aput p1, v5, v2

    invoke-direct {v4, v5, v2, v3}, Ljava/lang/String;-><init>([III)V

    invoke-direct {p0, v4}, Lcom/android/inputmethod/latin/LatinIME;->commitTyped(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingPunctuationList()Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x3

    iput v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    goto :goto_1

    :cond_7
    if-eqz v1, :cond_9

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->swapSwapperAndSpace()V

    const/4 v2, 0x2

    iput v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    :cond_8
    :goto_3
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->setPunctuationSuggestions()V

    goto :goto_2

    :cond_9
    if-ne v6, p4, :cond_8

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v2, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceStripper(I)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v2, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isPhantomSpacePromotingSymbol(I)Z

    move-result v2

    if-nez v2, :cond_8

    iput v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    goto :goto_3
.end method

.method private initSuggest()V
    .locals 5

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/Suggest;->getContactsDictionary()Lcom/android/inputmethod/latin/ContactsBinaryDictionary;

    move-result-object v1

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/Suggest;->close()V

    :goto_0
    new-instance v3, Lcom/android/inputmethod/latin/Suggest;

    invoke-direct {v3, p0, v2, p0}, Lcom/android/inputmethod/latin/Suggest;-><init>(Landroid/content/Context;Ljava/util/Locale;Lcom/android/inputmethod/latin/Suggest$SuggestInitializationListener;)V

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/SettingsValues;->mCorrectionEnabled:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v4, v4, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectionThreshold:F

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/latin/Suggest;->setAutoCorrectionThreshold(F)V

    :cond_0
    invoke-static {p0, v2}, Lcom/android/inputmethod/latin/DictionaryFactory;->isDictionaryAvailable(Landroid/content/Context;Ljava/util/Locale;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsMainDictionaryAvailable:Z

    new-instance v3, Lcom/android/inputmethod/latin/UserBinaryDictionary;

    invoke-direct {v3, p0, v0}, Lcom/android/inputmethod/latin/UserBinaryDictionary;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserDictionary:Lcom/android/inputmethod/latin/UserBinaryDictionary;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserDictionary:Lcom/android/inputmethod/latin/UserBinaryDictionary;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/UserBinaryDictionary;->isEnabled()Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsUserDictionaryAvailable:Z

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserDictionary:Lcom/android/inputmethod/latin/UserBinaryDictionary;

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/latin/Suggest;->setUserDictionary(Lcom/android/inputmethod/latin/UserBinaryDictionary;)V

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->resetContactsDictionary(Lcom/android/inputmethod/latin/ContactsBinaryDictionary;)V

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    :cond_1
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    invoke-static {p0, v0, v3}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->getInstance(Landroid/content/Context;Ljava/lang/String;Landroid/content/SharedPreferences;)Lcom/android/inputmethod/latin/UserHistoryDictionary;

    move-result-object v3

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    invoke-virtual {v3, v4}, Lcom/android/inputmethod/latin/Suggest;->setUserHistoryDictionary(Lcom/android/inputmethod/latin/UserHistoryDictionary;)V

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isAlphabet(I)Z
    .locals 1
    .param p0    # I

    invoke-static {p0}, Ljava/lang/Character;->isLetter(I)Z

    move-result v0

    return v0
.end method

.method private isShowingOptionDialog()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSuggestionsStripVisible()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->isShowingAddToDictionaryHint()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/latin/SettingsValues;->isSuggestionStripVisibleInOrientation(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/SettingsValues;->isApplicationSpecifiedCompletionsOn()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/SettingsValues;->isSuggestionsRequested(I)Z

    move-result v0

    goto :goto_0
.end method

.method private launchSettings()V
    .locals 1

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->handleClose()V

    const-class v0, Lcom/android/inputmethod/latin/SettingsActivity;

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->launchSubActivity(Ljava/lang/Class;)V

    return-void
.end method

.method private launchSubActivity(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private maybeDoubleSpace()Z
    .locals 7

    const/16 v6, 0x20

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/SettingsValues;->mCorrectionEnabled:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->isAcceptingDoubleSpaces()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v3, v5, v1}, Lcom/android/inputmethod/latin/RichInputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ne v3, v5, :cond_0

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/android/inputmethod/latin/LatinIME;->canBeFollowedByPeriod(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-ne v3, v6, :cond_0

    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-ne v3, v6, :cond_0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelDoubleSpacesTimer()V

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v3, v4, v1}, Lcom/android/inputmethod/latin/RichInputConnection;->deleteSurroundingText(II)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    const-string v3, ". "

    invoke-virtual {v1, v3, v2}, Lcom/android/inputmethod/latin/RichInputConnection;->commitText(Ljava/lang/CharSequence;I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    move v1, v2

    goto :goto_0
.end method

.method private maybeRetrieveOlderSuggestions(Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/SuggestedWords;)Lcom/android/inputmethod/latin/SuggestedWords;
    .locals 8
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Lcom/android/inputmethod/latin/SuggestedWords;

    const/4 v5, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v0

    if-gt v0, v5, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v5, :cond_0

    iget-boolean v0, p2, Lcom/android/inputmethod/latin/SuggestedWords;->mTypedWordValid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->isShowingAddToDictionaryHint()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p2

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->getSuggestions()Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v7

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v0, v0, Lcom/android/inputmethod/latin/SettingsValues;->mSuggestPuncList:Lcom/android/inputmethod/latin/SuggestedWords;

    if-ne v7, v0, :cond_2

    sget-object v7, Lcom/android/inputmethod/latin/SuggestedWords;->EMPTY:Lcom/android/inputmethod/latin/SuggestedWords;

    :cond_2
    invoke-static {p1, v7}, Lcom/android/inputmethod/latin/SuggestedWords;->getTypedWordAndPreviousSuggestions(Ljava/lang/CharSequence;Lcom/android/inputmethod/latin/SuggestedWords;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v0, Lcom/android/inputmethod/latin/SuggestedWords;

    move v3, v2

    move v4, v2

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/android/inputmethod/latin/SuggestedWords;-><init>(Ljava/util/ArrayList;ZZZZZ)V

    goto :goto_0
.end method

.method private maybeStripSpace(IIZ)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    const/4 v2, 0x2

    const/4 v0, 0x0

    const/16 v1, 0xa

    if-ne v1, p1, :cond_1

    if-ne v2, p2, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/RichInputConnection;->removeTrailingSpace()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x3

    if-eq v1, p2, :cond_2

    if-ne v2, p2, :cond_0

    :cond_2
    if-eqz p3, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v1, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceSwapper(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v1, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceStripper(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/RichInputConnection;->removeTrailingSpace()V

    goto :goto_0
.end method

.method private onFinishInputInternal()V
    .locals 2

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onFinishInput()V

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    :cond_0
    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->commit()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->closing()V

    :cond_1
    return-void
.end method

.method private onFinishInputViewInternal(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->onFinishInputView(Z)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onFinishInputView()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->cancelAllMessages()V

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelUpdateSuggestionStrip()V

    return-void
.end method

.method private onSettingsKeyPressed()V
    .locals 1

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingOptionDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->showSubtypeSelectorAndSettings()V

    goto :goto_0
.end method

.method private onStartInputInternal(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 0
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method private onStartInputViewInternal(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 13
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v4

    if-nez p1, :cond_0

    sget-object v7, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v8, "Null EditorInfo in onStartInputView()"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v7, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v7, :cond_5

    new-instance v7, Ljava/lang/NullPointerException;

    const-string v8, "Null EditorInfo in onStartInputView()"

    invoke-direct {v7, v8}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    sget-boolean v7, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    if-eqz v7, :cond_1

    sget-object v7, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onStartInputView: editorInfo:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "inputType=0x%08x imeOptions=0x%08x"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v8, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "All caps = "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v7, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    and-int/lit16 v7, v7, 0x1000

    if-eqz v7, :cond_6

    const/4 v7, 0x1

    :goto_0
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", sentence caps = "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v7, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    and-int/lit16 v7, v7, 0x4000

    if-eqz v7, :cond_7

    const/4 v7, 0x1

    :goto_1
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", word caps = "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v7, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    and-int/lit16 v7, v7, 0x2000

    if-eqz v7, :cond_8

    const/4 v7, 0x1

    :goto_2
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v7, 0x0

    const-string v8, "nm"

    invoke-static {v7, v8, p1}, Lcom/android/inputmethod/latin/InputAttributes;->inPrivateImeOptions(Ljava/lang/String;Ljava/lang/String;Landroid/view/inputmethod/EditorInfo;)Z

    move-result v7

    if-eqz v7, :cond_2

    sget-object v7, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Deprecated private IME option specified: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v7, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Use "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "noMicrophoneKey"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " instead"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "forceAscii"

    invoke-static {v7, v8, p1}, Lcom/android/inputmethod/latin/InputAttributes;->inPrivateImeOptions(Ljava/lang/String;Ljava/lang/String;Landroid/view/inputmethod/EditorInfo;)Z

    move-result v7

    if-eqz v7, :cond_3

    sget-object v7, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Deprecated private IME option specified: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v7, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v8, "Use EditorInfo.IME_FLAG_FORCE_ASCII flag instead"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v7, p1, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    invoke-static {v7}, Lcom/android/inputmethod/latin/TargetApplicationGetter;->getCachedApplicationInfo(Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iput-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    if-nez v7, :cond_4

    new-instance v7, Lcom/android/inputmethod/latin/TargetApplicationGetter;

    invoke-direct {v7, p0, p0}, Lcom/android/inputmethod/latin/TargetApplicationGetter;-><init>(Landroid/content/Context;Lcom/android/inputmethod/latin/TargetApplicationGetter$OnTargetApplicationKnownListener;)V

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p1, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v7, v8}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_4
    invoke-static {p1}, Lcom/android/inputmethod/latin/LatinImeLogger;->onStartInputView(Landroid/view/inputmethod/EditorInfo;)V

    if-nez v4, :cond_9

    :cond_5
    :goto_3
    return-void

    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_1

    :cond_8
    const/4 v7, 0x0

    goto/16 :goto_2

    :cond_9
    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->getInstance()Lcom/android/inputmethod/accessibility/AccessibilityUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->isTouchExplorationEnabled()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-virtual {v0, v4, p1, p2}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->onStartInputViewInternal(Landroid/view/View;Landroid/view/inputmethod/EditorInfo;Z)V

    :cond_a
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v7, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isSameInputType(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v7

    if-nez v7, :cond_10

    const/4 v2, 0x1

    :goto_4
    if-eqz p2, :cond_b

    if-eqz v2, :cond_11

    :cond_b
    const/4 v3, 0x1

    :goto_5
    if-eqz v3, :cond_c

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateParametersOnStartInputViewAndReturnIfCurrentSubtypeEnabled()Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getNoLanguageSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/inputmethod/latin/ImfUtils;->getCurrentInputMethodSubtype(Landroid/content/Context;Landroid/view/inputmethod/InputMethodSubtype;)Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v5

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v7, v5}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateSubtype(Landroid/view/inputmethod/InputMethodSubtype;)V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->loadKeyboard()V

    :cond_c
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->updateFullscreenMode()V

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    const/4 v7, 0x1

    invoke-direct {p0, v7}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    const/4 v7, 0x0

    iput v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mDeleteCount:I

    const/4 v7, 0x0

    iput v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    if-eqz v7, :cond_d

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->setPunctuationSuggestions()V

    :cond_d
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget v8, p1, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    invoke-virtual {v7, v8}, Lcom/android/inputmethod/latin/RichInputConnection;->resetCachesUponCursorMove(I)V

    if-eqz v3, :cond_12

    invoke-virtual {v4}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->closing()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->loadSettings()V

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v7, :cond_e

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v7, v7, Lcom/android/inputmethod/latin/SettingsValues;->mCorrectionEnabled:Z

    if-eqz v7, :cond_e

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v8, v8, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCorrectionThreshold:F

    invoke-virtual {v7, v8}, Lcom/android/inputmethod/latin/Suggest;->setAutoCorrectionThreshold(F)V

    :cond_e
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v6, p1, v7}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->loadKeyboard(Landroid/view/inputmethod/EditorInfo;Lcom/android/inputmethod/latin/SettingsValues;)V

    :cond_f
    :goto_6
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsStripVisible()Z

    move-result v7

    const/4 v8, 0x0

    invoke-direct {p0, v7, v8}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStripShownInternal(ZZ)V

    iget-boolean v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsConfigurationChanged:Z

    if-nez v7, :cond_13

    iget v7, p1, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    iput v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    iget v7, p1, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    iput v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    :goto_7
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelUpdateSuggestionStrip()V

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelDoubleSpacesTimer()V

    iget-boolean v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsMainDictionaryAvailable:Z

    invoke-virtual {v4, v7}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->setMainDictionaryAvailability(Z)V

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v7, v7, Lcom/android/inputmethod/latin/SettingsValues;->mKeyPreviewPopupOn:Z

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v8, v8, Lcom/android/inputmethod/latin/SettingsValues;->mKeyPreviewPopupDismissDelay:I

    invoke-virtual {v4, v7, v8}, Lcom/android/inputmethod/keyboard/KeyboardView;->setKeyPreviewPopupEnabled(ZI)V

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v7, v7, Lcom/android/inputmethod/latin/SettingsValues;->mGestureInputEnabled:Z

    invoke-virtual {v4, v7}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->setGestureHandlingEnabledByUser(Z)V

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v7, v7, Lcom/android/inputmethod/latin/SettingsValues;->mGesturePreviewTrailEnabled:Z

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v8, v8, Lcom/android/inputmethod/latin/SettingsValues;->mGestureFloatingPreviewTextEnabled:Z

    invoke-virtual {v4, v7, v8}, Lcom/android/inputmethod/keyboard/KeyboardView;->setGesturePreviewMode(ZZ)V

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    invoke-virtual {v7, v8, p1, v9}, Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;->tryReplaceWithActualWord(Lcom/android/inputmethod/latin/RichInputConnection;Landroid/view/inputmethod/EditorInfo;I)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    goto/16 :goto_3

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_12
    if-eqz p2, :cond_f

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->resetKeyboardStateToAlphabet()V

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    goto :goto_6

    :cond_13
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsConfigurationChanged:Z

    goto :goto_7
.end method

.method private performEditorAction(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/RichInputConnection;->performEditorAction(I)V

    return-void
.end method

.method private resetComposingState(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->reset()V

    if-eqz p1, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LastComposedWord;->NOT_A_COMPOSED_WORD:Lcom/android/inputmethod/latin/LastComposedWord;

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    :cond_0
    return-void
.end method

.method private resetContactsDictionary(Lcom/android/inputmethod/latin/ContactsBinaryDictionary;)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/latin/ContactsBinaryDictionary;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/SettingsValues;->mUseContactsDict:Z

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;->close()V

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v3, v0}, Lcom/android/inputmethod/latin/Suggest;->setContactsDictionary(Lcom/android/inputmethod/latin/ContactsBinaryDictionary;)V

    :cond_1
    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v1

    if-eqz p1, :cond_5

    iget-object v3, p1, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;->mLocale:Ljava/util/Locale;

    invoke-virtual {v3, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;->close()V

    new-instance v0, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;

    invoke-direct {v0, p0, v1}, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1, p0}, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;->reopen(Landroid/content/Context;)V

    move-object v0, p1

    goto :goto_1

    :cond_5
    new-instance v0, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;

    invoke-direct {v0, p0, v1}, Lcom/android/inputmethod/latin/ContactsBinaryDictionary;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    goto :goto_1
.end method

.method private resetEntireInputState(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v0, v0, Lcom/android/inputmethod/latin/SettingsValues;->mBigramPredictionEnabled:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->clearSuggestionStrip()V

    :goto_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/RichInputConnection;->resetCachesUponCursorMove(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v0, v0, Lcom/android/inputmethod/latin/SettingsValues;->mSuggestPuncList:Lcom/android/inputmethod/latin/SuggestedWords;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Z)V

    goto :goto_0
.end method

.method private restartSuggestionsOnWordBeforeCursor(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/android/inputmethod/latin/WordComposer;->setComposingWord(Ljava/lang/CharSequence;Lcom/android/inputmethod/keyboard/Keyboard;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/android/inputmethod/latin/RichInputConnection;->deleteSurroundingText(II)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Lcom/android/inputmethod/latin/RichInputConnection;->setComposingText(Ljava/lang/CharSequence;I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestionStrip()V

    return-void
.end method

.method private restartSuggestionsOnWordBeforeCursorIfAtEndOfWord()V
    .locals 3

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/latin/RichInputConnection;->getWordBeforeCursorIfAtEndOfWord(Lcom/android/inputmethod/latin/SettingsValues;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->restartSuggestionsOnWordBeforeCursor(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private revertCommit()V
    .locals 10

    const/4 v8, 0x0

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v4, v7, Lcom/android/inputmethod/latin/LastComposedWord;->mPrevWord:Ljava/lang/CharSequence;

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v3, v7, Lcom/android/inputmethod/latin/LastComposedWord;->mTypedWord:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v1, v7, Lcom/android/inputmethod/latin/LastComposedWord;->mCommittedWord:Ljava/lang/String;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v7, v7, Lcom/android/inputmethod/latin/LastComposedWord;->mSeparatorString:Ljava/lang/String;

    invoke-static {v7}, Lcom/android/inputmethod/latin/LastComposedWord;->getSeparatorLength(Ljava/lang/String;)I

    move-result v5

    add-int v2, v0, v5

    sget-boolean v7, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v7

    if-eqz v7, :cond_0

    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "revertCommit, but we are composing a word"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v7, v2, v8}, Lcom/android/inputmethod/latin/RichInputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7, v8, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "revertCommit check failed: we thought we were reverting \""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\", but before the cursor we found \""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_1
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v7, v2, v8}, Lcom/android/inputmethod/latin/RichInputConnection;->deleteSurroundingText(II)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserHistoryDictionary:Lcom/android/inputmethod/latin/UserHistoryDictionary;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/inputmethod/latin/UserHistoryDictionary;->cancelAddingUserHistory(Ljava/lang/String;Ljava/lang/String;)Z

    :cond_2
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v9, v9, Lcom/android/inputmethod/latin/LastComposedWord;->mSeparatorString:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Lcom/android/inputmethod/latin/RichInputConnection;->commitText(Ljava/lang/CharSequence;I)V

    sget-object v7, Lcom/android/inputmethod/latin/LastComposedWord;->NOT_A_COMPOSED_WORD:Lcom/android/inputmethod/latin/LastComposedWord;

    iput-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestionStrip()V

    return-void
.end method

.method private sendDownUpKeyEventForBackwardCompatibility(I)V
    .locals 15
    .param p1    # I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-object v12, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    new-instance v0, Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x6

    move-wide v3, v1

    move/from16 v6, p1

    invoke-direct/range {v0 .. v11}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    invoke-virtual {v12, v0}, Lcom/android/inputmethod/latin/RichInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    new-instance v3, Landroid/view/KeyEvent;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, -0x1

    const/4 v13, 0x0

    const/4 v14, 0x6

    move-wide v6, v1

    move/from16 v9, p1

    invoke-direct/range {v3 .. v14}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    invoke-virtual {v0, v3}, Lcom/android/inputmethod/latin/RichInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)V

    return-void
.end method

.method private sendKeyCodePoint(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v1, 0x30

    if-lt p1, v1, :cond_0

    const/16 v1, 0x39

    if-gt p1, v1, :cond_0

    add-int/lit8 v1, p1, -0x30

    add-int/lit8 v1, v1, 0x7

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->sendDownUpKeyEventForBackwardCompatibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0xa

    if-ne v1, p1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    const/16 v1, 0x42

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->sendDownUpKeyEventForBackwardCompatibility(I)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    new-array v1, v4, [I

    aput p1, v1, v3

    invoke-direct {v0, v1, v3, v4}, Ljava/lang/String;-><init>([III)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/android/inputmethod/latin/RichInputConnection;->commitText(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method private setAutoCorrectionIndicator(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsAutoCorrectionIndicatorOn:Z

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean p1, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsAutoCorrectionIndicatorOn:Z

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->getTextWithUnderline(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/android/inputmethod/latin/RichInputConnection;->setComposingText(Ljava/lang/CharSequence;I)V

    :cond_0
    return-void
.end method

.method private setPunctuationSuggestions()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v0, v0, Lcom/android/inputmethod/latin/SettingsValues;->mBigramPredictionEnabled:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->clearSuggestionStrip()V

    :goto_0
    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setAutoCorrectionIndicator(Z)V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsStripVisible()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStripShown(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v0, v0, Lcom/android/inputmethod/latin/SettingsValues;->mSuggestPuncList:Lcom/android/inputmethod/latin/SuggestedWords;

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Z)V

    goto :goto_0
.end method

.method private setSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Z)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->setSuggestions(Lcom/android/inputmethod/latin/SuggestedWords;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0, p2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onAutoCorrectionStateChanged(Z)V

    :cond_0
    return-void
.end method

.method private setSuggestionStripShown(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStripShownInternal(ZZ)V

    return-void
.end method

.method private setSuggestionStripShownInternal(ZZ)V
    .locals 5
    .param p1    # Z
    .param p2    # Z

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onEvaluateInputViewShown()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v4}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v0

    :goto_0
    if-eqz p1, :cond_3

    if-eqz p2, :cond_0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v2, 0x1

    :goto_1
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isFullscreenMode()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    if-eqz v2, :cond_4

    :goto_2
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_3
    return-void

    :cond_2
    move v0, v3

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :cond_4
    const/16 v3, 0x8

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    if-eqz v2, :cond_6

    :goto_4
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_6
    const/4 v3, 0x4

    goto :goto_4
.end method

.method private showGesturePreviewAndSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Z)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # Z

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->showGestureFloatingPreviewText(Ljava/lang/String;)V

    invoke-direct {p0, p1, v2}, Lcom/android/inputmethod/latin/LatinIME;->showSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->dismissGestureFloatingPreviewText()V

    :cond_0
    return-void

    :cond_1
    move-object v0, v2

    goto :goto_0
.end method

.method private showSubtypeSelectorAndSettings()V
    .locals 7

    const v5, 0x7f0b002f

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v2, v5, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    const v6, 0x7f0b008c

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    const/4 v5, 0x1

    const v6, 0x7f0b002c

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    move-object v1, p0

    new-instance v3, Lcom/android/inputmethod/latin/LatinIME$3;

    invoke-direct {v3, p0, v1}, Lcom/android/inputmethod/latin/LatinIME$3;-><init>(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/Context;)V

    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v2, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/android/inputmethod/latin/LatinIME;->showOptionDialog(Landroid/app/AlertDialog;)V

    return-void
.end method

.method private showSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Lcom/android/inputmethod/latin/SuggestedWords;
    .param p2    # Ljava/lang/CharSequence;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v2

    if-gtz v2, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->clearSuggestionStrip()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v2

    if-lez v2, :cond_3

    iget-boolean v2, p1, Lcom/android/inputmethod/latin/SuggestedWords;->mWillAutoCorrect:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/WordComposer;->setAutoCorrection(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->willAutoCorrect()Z

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Z)V

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->setAutoCorrectionIndicator(Z)V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->isSuggestionsStripVisible()Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStripShown(Z)V

    goto :goto_0

    :cond_2
    move-object v0, p2

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private specificTldProcessingOnTextInput(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Ljava/lang/CharSequence;

    const/16 v4, 0x2e

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    iput v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1, v2, v3}, Lcom/android/inputmethod/latin/RichInputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-interface {p1, v2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0
.end method

.method private swapSwapperAndSpace()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1, v4, v3}, Lcom/android/inputmethod/latin/RichInputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1, v4, v3}, Lcom/android/inputmethod/latin/RichInputConnection;->deleteSurroundingText(II)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Lcom/android/inputmethod/latin/RichInputConnection;->commitText(Ljava/lang/CharSequence;I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    :cond_0
    return-void
.end method

.method private updateSuggestionStrip()V
    .locals 4

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelUpdateSuggestionStrip()V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/latin/SettingsValues;->isSuggestionsRequested(I)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v3, "Called updateSuggestionsOrPredictions but suggestions were not requested!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/latin/WordComposer;->setAutoCorrection(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v2, v2, Lcom/android/inputmethod/latin/SettingsValues;->mBigramPredictionEnabled:Z

    if-nez v2, :cond_3

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->setPunctuationSuggestions()V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->getSuggestedWords(I)Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/inputmethod/latin/LatinIME;->showSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public addWordToUserDictionary(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;-><init>(Ljava/lang/String;ILandroid/view/inputmethod/EditorInfo;)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mUserDictionary:Lcom/android/inputmethod/latin/UserBinaryDictionary;

    const/16 v1, 0x80

    invoke-virtual {v0, p1, v1}, Lcom/android/inputmethod/latin/UserBinaryDictionary;->addWordToUserDictionary(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public debugDumpStateAndCrashWithException(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Target application : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nPackage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nTarget app sdk version : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nAttributes : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/SettingsValues;->getInputAttributesDebugString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nContext : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/io/FileDescriptor;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # [Ljava/lang/String;

    invoke-super {p0, p1, p2, p3}, Landroid/inputmethodservice/InputMethodService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    new-instance v2, Landroid/util/PrintWriterPrinter;

    invoke-direct {v2, p2}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    const-string v3, "LatinIME state :"

    invoke-interface {v2, v3}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget v1, v3, Lcom/android/inputmethod/keyboard/KeyboardId;->mMode:I

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Keyboard mode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  mIsSuggestionsSuggestionsRequested = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    invoke-virtual {v4, v5}, Lcom/android/inputmethod/latin/SettingsValues;->isSuggestionsRequested(I)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  mCorrectionEnabled="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v4, v4, Lcom/android/inputmethod/latin/SettingsValues;->mCorrectionEnabled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  isComposingWord="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v4}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  mSoundOn="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v4, v4, Lcom/android/inputmethod/latin/SettingsValues;->mSoundOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  mVibrateOn="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v4, v4, Lcom/android/inputmethod/latin/SettingsValues;->mVibrateOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  mKeyPreviewPopupOn="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v4, v4, Lcom/android/inputmethod/latin/SettingsValues;->mKeyPreviewPopupOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  inputAttributes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v4}, Lcom/android/inputmethod/latin/SettingsValues;->getInputAttributesDebugString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v1, -0x1

    goto/16 :goto_0
.end method

.method public getCurrentAutoCapsState()I
    .locals 7

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-boolean v3, v3, Lcom/android/inputmethod/latin/SettingsValues;->mAutoCap:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, v0, Landroid/view/inputmethod/EditorInfo;->inputType:I

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    iget-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v4}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v4

    const/4 v5, 0x4

    iget v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    if-ne v5, v6, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-virtual {v3, v1, v4, v2}, Lcom/android/inputmethod/latin/RichInputConnection;->getCursorCapsMode(ILjava/util/Locale;Z)I

    move-result v2

    goto :goto_0
.end method

.method public hapticAndAudioFeedback(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mFeedbackManager:Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;->hapticAndAudioFeedback(ILandroid/view/View;)V

    return-void
.end method

.method public hideWindow()V
    .locals 1

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->commit()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onHideWindow()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    :cond_0
    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->hideWindow()V

    return-void
.end method

.method isShowingPunctuationList()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v1, v1, Lcom/android/inputmethod/latin/SettingsValues;->mSuggestPuncList:Lcom/android/inputmethod/latin/SuggestedWords;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->getSuggestions()Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isWordSeparator(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isWordSeparator(I)Z

    move-result v0

    return v0
.end method

.method public launchDebugSettings()V
    .locals 1

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->handleClose()V

    const-class v0, Lcom/android/inputmethod/latin/DebugSettingsActivity;

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->launchSubActivity(Ljava/lang/Class;)V

    return-void
.end method

.method public launchKeyboardedDialogActivity(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->commitTyped(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->launchSubActivity(Ljava/lang/Class;)V

    return-void
.end method

.method loadKeyboard()V
    .locals 3

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->initSuggest()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->loadSettings()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v0, v1, v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->loadKeyboard(Landroid/view/inputmethod/EditorInfo;Lcom/android/inputmethod/latin/SettingsValues;)V

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestionStrip()V

    return-void
.end method

.method loadSettings()V
    .locals 4

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v2, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    :cond_0
    new-instance v0, Lcom/android/inputmethod/latin/InputAttributes;

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v2

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isFullscreenMode()Z

    move-result v3

    invoke-direct {v0, v2, v3}, Lcom/android/inputmethod/latin/InputAttributes;-><init>(Landroid/view/inputmethod/EditorInfo;Z)V

    new-instance v1, Lcom/android/inputmethod/latin/LatinIME$1;

    invoke-direct {v1, p0, v0}, Lcom/android/inputmethod/latin/LatinIME$1;-><init>(Lcom/android/inputmethod/latin/LatinIME;Lcom/android/inputmethod/latin/InputAttributes;)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/inputmethod/latin/LocaleUtils$RunInLocale;->runInLocale(Landroid/content/res/Resources;Ljava/util/Locale;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/inputmethod/latin/SettingsValues;

    iput-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    new-instance v2, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-direct {v2, p0, v3}, Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;-><init>(Lcom/android/inputmethod/latin/LatinIME;Lcom/android/inputmethod/latin/SettingsValues;)V

    iput-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mFeedbackManager:Lcom/android/inputmethod/latin/AudioAndHapticFeedbackManager;

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_0
    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->resetContactsDictionary(Lcom/android/inputmethod/latin/ContactsBinaryDictionary;)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/Suggest;->getContactsDictionary()Lcom/android/inputmethod/latin/ContactsBinaryDictionary;

    move-result-object v2

    goto :goto_0
.end method

.method public onCancelInput()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onCancelInput()V

    return-void
.end method

.method public onCodeInput(III)V
    .locals 12
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    const/4 v8, -0x4

    if-ne p1, v8, :cond_0

    iget-wide v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastKeyTime:J

    const-wide/16 v10, 0xc8

    add-long/2addr v8, v10

    cmp-long v8, v6, v8

    if-lez v8, :cond_1

    :cond_0
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mDeleteCount:I

    :cond_1
    iput-wide v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastKeyTime:J

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v8}, Lcom/android/inputmethod/latin/RichInputConnection;->beginBatchEdit()V

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    iget v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v8}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v8

    if-nez v8, :cond_2

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsAutoCorrectionIndicatorOn:Z

    :cond_2
    const/16 v8, 0x20

    if-eq p1, v8, :cond_3

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v8}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->cancelDoubleSpacesTimer()V

    :cond_3
    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v8, p1}, Lcom/android/inputmethod/latin/SettingsValues;->isWordSeparator(I)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-direct {p0, p1, p2, p3, v4}, Lcom/android/inputmethod/latin/LatinIME;->handleSeparator(IIII)Z

    move-result v0

    :goto_0
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    :goto_1
    :pswitch_1
    invoke-virtual {v5, p1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onCodeInput(I)V

    if-nez v0, :cond_4

    const/4 v8, -0x1

    if-eq p1, v8, :cond_4

    const/4 v8, -0x2

    if-eq p1, v8, :cond_4

    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    invoke-virtual {v8}, Lcom/android/inputmethod/latin/LastComposedWord;->deactivate()V

    :cond_4
    const/4 v8, -0x4

    if-eq v8, p1, :cond_5

    const/4 v8, 0x0

    iput-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    :cond_5
    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v8}, Lcom/android/inputmethod/latin/RichInputConnection;->endBatchEdit()V

    return-void

    :pswitch_2
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    invoke-direct {p0, v4}, Lcom/android/inputmethod/latin/LatinIME;->handleBackspace(I)V

    iget v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mDeleteCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mDeleteCount:I

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    invoke-static {p2, p3}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnDelete(II)V

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->onSettingsKeyPressed()V

    goto :goto_1

    :pswitch_4
    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v8, p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->switchToShortcutIME(Landroid/inputmethodservice/InputMethodService;)V

    goto :goto_1

    :pswitch_5
    invoke-virtual {v5}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v8

    invoke-static {v8}, Lcom/android/inputmethod/latin/LatinIME;->getActionId(Lcom/android/inputmethod/keyboard/Keyboard;)I

    move-result v8

    invoke-direct {p0, v8}, Lcom/android/inputmethod/latin/LatinIME;->performEditorAction(I)V

    goto :goto_1

    :pswitch_6
    const/4 v8, 0x5

    invoke-direct {p0, v8}, Lcom/android/inputmethod/latin/LatinIME;->performEditorAction(I)V

    goto :goto_1

    :pswitch_7
    const/4 v8, 0x7

    invoke-direct {p0, v8}, Lcom/android/inputmethod/latin/LatinIME;->performEditorAction(I)V

    goto :goto_1

    :pswitch_8
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->handleLanguageSwitchKey()V

    goto :goto_1

    :cond_6
    const/4 v8, 0x4

    if-ne v8, v4, :cond_7

    const-string v8, ""

    invoke-direct {p0, v8}, Lcom/android/inputmethod/latin/LatinIME;->commitTyped(Ljava/lang/String;)V

    :cond_7
    iget-object v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v8}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {v3, p1}, Lcom/android/inputmethod/keyboard/Keyboard;->hasProximityCharsCorrection(I)Z

    move-result v8

    if-eqz v8, :cond_8

    move v1, p2

    move v2, p3

    :goto_2
    invoke-direct {p0, p1, v1, v2, v4}, Lcom/android/inputmethod/latin/LatinIME;->handleCharacter(IIII)V

    goto :goto_0

    :cond_8
    const/4 v1, -0x1

    const/4 v2, -0x1

    goto :goto_2

    :pswitch_data_0
    .packed-switch -0xb
        :pswitch_1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onComputeInsets(Landroid/inputmethodservice/InputMethodService$Insets;)V
    .locals 13
    .param p1    # Landroid/inputmethodservice/InputMethodService$Insets;

    const/16 v12, 0x8

    const/4 v10, 0x0

    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->onComputeInsets(Landroid/inputmethodservice/InputMethodService$Insets;)V

    iget-object v11, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v11}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v11, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    if-nez v11, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->getAdjustedBackingViewHeight()I

    move-result v0

    iget-object v11, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v11

    if-ne v11, v12, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_5

    move v2, v10

    :goto_2
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isFullscreenMode()Z

    move-result v11

    if-eqz v11, :cond_6

    iget-object v11, p0, Lcom/android/inputmethod/latin/LatinIME;->mExtractArea:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v4

    :goto_3
    iget-object v11, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v11

    if-ne v11, v12, :cond_7

    move v6, v10

    :goto_4
    add-int v11, v4, v2

    add-int v3, v11, v6

    move v9, v3

    invoke-virtual {v5}, Landroid/view/View;->isShown()Z

    move-result v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v11

    if-nez v11, :cond_2

    sub-int/2addr v9, v6

    :cond_2
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v11

    add-int/2addr v11, v3

    add-int/lit8 v7, v11, 0x64

    const/4 v11, 0x3

    iput v11, p1, Landroid/inputmethodservice/InputMethodService$Insets;->touchableInsets:I

    iget-object v11, p1, Landroid/inputmethodservice/InputMethodService$Insets;->touchableRegion:Landroid/graphics/Region;

    invoke-virtual {v11, v10, v9, v8, v7}, Landroid/graphics/Region;->set(IIII)Z

    :cond_3
    iput v9, p1, Landroid/inputmethodservice/InputMethodService$Insets;->contentTopInsets:I

    iput v9, p1, Landroid/inputmethodservice/InputMethodService$Insets;->visibleTopInsets:I

    goto :goto_0

    :cond_4
    move v1, v10

    goto :goto_1

    :cond_5
    move v2, v0

    goto :goto_2

    :cond_6
    move v4, v10

    goto :goto_3

    :cond_7
    iget-object v11, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v6

    goto :goto_4
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v0, p1, p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->onConfigurationChanged(Landroid/content/res/Configuration;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->loadKeyboard()V

    :cond_0
    iget v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_2

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->startOrientationChanging()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/RichInputConnection;->beginBatchEdit()V

    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->commitTyped(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/RichInputConnection;->finishComposingText()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/RichInputConnection;->endBatchEdit()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingOptionDialog()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsConfigurationChanged:Z

    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_2
    return-void
.end method

.method public onCreate()V
    .locals 6

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mPrefs:Landroid/content/SharedPreferences;

    invoke-static {p0, v3}, Lcom/android/inputmethod/latin/LatinImeLogger;->init(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V

    invoke-static {p0}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->init(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->init(Landroid/content/Context;)V

    invoke-static {p0, v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->init(Lcom/android/inputmethod/latin/LatinIME;Landroid/content/SharedPreferences;)V

    invoke-static {p0}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->init(Landroid/inputmethodservice/InputMethodService;)V

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onCreate()V

    invoke-static {}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->getInstance()Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    move-result-object v5

    iput-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v5}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->onCreate()V

    sget-boolean v5, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v5, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iput-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mResources:Landroid/content/res/Resources;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->loadSettings()V

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v5}, Lcom/android/inputmethod/latin/SettingsValues;->getAdditionalSubtypes()[Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/android/inputmethod/latin/ImfUtils;->setAdditionalInputMethodSubtypes(Landroid/content/Context;[Landroid/view/inputmethod/InputMethodSubtype;)V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->initSuggest()V

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    iput v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v5, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v5, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v5, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v5, "package"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mDictionaryPackInstallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v2}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v5, "com.android.inputmethod.latin.dictionarypack.newdict"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mDictionaryPackInstallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onCreateInputView()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    iget-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsHardwareAcceleratedDrawingEnabled:Z

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onCreateInputView(Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCurrentInputMethodSubtypeChanged(Landroid/view/inputmethod/InputMethodSubtype;)V
    .locals 1
    .param p1    # Landroid/view/inputmethod/InputMethodSubtype;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->updateSubtype(Landroid/view/inputmethod/InputMethodSubtype;)V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->loadKeyboard()V

    return-void
.end method

.method public onCustomRequest(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingOptionDialog()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {p0, v1}, Lcom/android/inputmethod/latin/ImfUtils;->hasMultipleEnabledIMEsOrSubtypes(Landroid/content/Context;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mImm:Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;

    invoke-virtual {v0}, Lcom/android/inputmethod/compat/InputMethodManagerCompatWrapper;->showInputMethodPicker()V

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/Suggest;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mDictionaryPackInstallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->commit()V

    invoke-static {}, Lcom/android/inputmethod/latin/LatinImeLogger;->onDestroy()V

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onDestroy()V

    return-void
.end method

.method public onDisplayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    .locals 9
    .param p1    # [Landroid/view/inputmethod/CompletionInfo;

    const/4 v2, 0x0

    sget-boolean v3, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v4, "Received completions:"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    const/4 v7, 0x0

    :goto_0
    array-length v3, p1

    if-ge v7, v3, :cond_0

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p1, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/SettingsValues;->isApplicationSpecifiedCompletionsOn()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    return-void

    :cond_1
    iput-object p1, p0, Lcom/android/inputmethod/latin/LatinIME;->mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

    if-nez p1, :cond_2

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->clearSuggestionStrip()V

    goto :goto_1

    :cond_2
    invoke-static {p1}, Lcom/android/inputmethod/latin/SuggestedWords;->getFromApplicationSpecifiedCompletions([Landroid/view/inputmethod/CompletionInfo;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v0, Lcom/android/inputmethod/latin/SuggestedWords;

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/android/inputmethod/latin/SuggestedWords;-><init>(Ljava/util/ArrayList;ZZZZZ)V

    const/4 v8, 0x0

    invoke-direct {p0, v0, v2}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStrip(Lcom/android/inputmethod/latin/SuggestedWords;Z)V

    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->setAutoCorrectionIndicator(Z)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/inputmethod/latin/WordComposer;->setAutoCorrection(Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/inputmethod/latin/LatinIME;->setSuggestionStripShown(Z)V

    goto :goto_1
.end method

.method public onEndBatchInput(Lcom/android/inputmethod/latin/InputPointers;)V
    .locals 5
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;

    const/4 v4, 0x4

    const/4 v3, 0x1

    invoke-static {}, Lcom/android/inputmethod/latin/LatinIME$BatchInputUpdater;->getInstance()Lcom/android/inputmethod/latin/LatinIME$BatchInputUpdater;

    move-result-object v2

    invoke-virtual {v2, p1, p0}, Lcom/android/inputmethod/latin/LatinIME$BatchInputUpdater;->onEndBatchInput(Lcom/android/inputmethod/latin/InputPointers;Lcom/android/inputmethod/latin/LatinIME;)Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/SuggestedWords;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/latin/SuggestedWords;->getWord(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v2, v0}, Lcom/android/inputmethod/latin/WordComposer;->setBatchInputWord(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/RichInputConnection;->beginBatchEdit()V

    iget v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    if-ne v4, v2, :cond_2

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->promotePhantomSpace()V

    :cond_2
    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v2, v0, v3}, Lcom/android/inputmethod/latin/RichInputConnection;->setComposingText(Ljava/lang/CharSequence;I)V

    iput-boolean v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/RichInputConnection;->endBatchEdit()V

    iput v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    goto :goto_1
.end method

.method public onEvaluateFullscreenMode()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/android/inputmethod/latin/SettingsValues;->isFullscreenModeAllowed(Landroid/content/res/Resources;)Z

    move-result v1

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onEvaluateFullscreenMode()Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v3, v0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v4, 0x10000000

    and-int/2addr v3, v4

    if-nez v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method public onExtractedCursorMovement(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/SettingsValues;->isSuggestionsRequested(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onExtractedCursorMovement(II)V

    goto :goto_0
.end method

.method public onExtractedTextClicked()V
    .locals 2

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mDisplayOrientation:I

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/SettingsValues;->isSuggestionsRequested(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onExtractedTextClicked()V

    goto :goto_0
.end method

.method public onFinishInput()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->onFinishInput()V

    return-void
.end method

.method public onFinishInputView(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->onFinishInputView(Z)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->dismissMoreSuggestions()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v1, "MoreSuggestionWindow is showing, just dismiss it"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 3

    sget-object v0, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "on Low memory! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isInputViewShown()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/Suggest;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    :cond_0
    return-void
.end method

.method public onPressKey(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onPressKey(I)V

    return-void
.end method

.method public onReleaseKey(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1, p1, p2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onReleaseKey(IZ)V

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->getInstance()Lcom/android/inputmethod/accessibility/AccessibilityUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v1, -0x4

    if-ne v1, p1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1, v3, v2}, Lcom/android/inputmethod/latin/RichInputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1, v3, v2}, Lcom/android/inputmethod/latin/RichInputConnection;->deleteSurroundingText(II)V

    :cond_1
    return-void

    :pswitch_0
    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->notifyShiftState()V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->notifySymbolsState()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onStartBatchInput()V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x1

    invoke-static {}, Lcom/android/inputmethod/latin/LatinIME$BatchInputUpdater;->getInstance()Lcom/android/inputmethod/latin/LatinIME$BatchInputUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME$BatchInputUpdater;->onStartBatchInput()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/RichInputConnection;->beginBatchEdit()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/WordComposer;->size()I

    move-result v1

    if-gt v1, v2, :cond_1

    const-string v1, ""

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->commitCurrentAutoCorrection(Ljava/lang/String;)V

    :goto_0
    iput-boolean v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    iput v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/RichInputConnection;->endBatchEdit()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->getActualCapsMode()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/latin/WordComposer;->setCapitalizedModeAtStartComposingTime(I)V

    return-void

    :cond_1
    const-string v1, ""

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->commitTyped(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/RichInputConnection;->getCodePointBeforeCursor()I

    move-result v0

    const/4 v1, -0x1

    if-eq v1, v0, :cond_0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v1, v0}, Lcom/android/inputmethod/latin/SettingsValues;->isPhantomSpacePromotingSymbol(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v1, v0}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceStripper(I)Z

    move-result v1

    if-nez v1, :cond_0

    iput v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    goto :goto_1
.end method

.method public onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 1
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method public onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 1
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V

    return-void
.end method

.method public onTargetApplicationKnown(Landroid/content/pm/ApplicationInfo;)V
    .locals 0
    .param p1    # Landroid/content/pm/ApplicationInfo;

    iput-object p1, p0, Lcom/android/inputmethod/latin/LatinIME;->mTargetApplicationInfo:Landroid/content/pm/ApplicationInfo;

    return-void
.end method

.method public onTextInput(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/RichInputConnection;->beginBatchEdit()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/inputmethod/latin/LatinIME;->commitCurrentAutoCorrection(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestionStrip()V

    invoke-direct {p0, p1}, Lcom/android/inputmethod/latin/LatinIME;->specificTldProcessingOnTextInput(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x4

    iget v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->promotePhantomSpace()V

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1, v0, v3}, Lcom/android/inputmethod/latin/RichInputConnection;->commitText(Ljava/lang/CharSequence;I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/RichInputConnection;->endBatchEdit()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    const/4 v2, -0x3

    invoke-virtual {v1, v2}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->onCodeInput(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mEnteredText:Ljava/lang/CharSequence;

    return-void

    :cond_1
    invoke-direct {p0, v3}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    goto :goto_0
.end method

.method public onUpdateBatchInput(Lcom/android/inputmethod/latin/InputPointers;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;

    invoke-static {}, Lcom/android/inputmethod/latin/LatinIME$BatchInputUpdater;->getInstance()Lcom/android/inputmethod/latin/LatinIME$BatchInputUpdater;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/android/inputmethod/latin/LatinIME$BatchInputUpdater;->onUpdateBatchInput(Lcom/android/inputmethod/latin/InputPointers;Lcom/android/inputmethod/latin/LatinIME;)V

    return-void
.end method

.method public onUpdateMainDictionaryAvailability(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsMainDictionaryAvailable:Z

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->setMainDictionaryAvailability(Z)V

    :cond_0
    return-void
.end method

.method public onUpdateSelection(IIIIII)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v0, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x0

    invoke-super/range {p0 .. p6}, Landroid/inputmethodservice/InputMethodService;->onUpdateSelection(IIIIII)V

    sget-boolean v3, Lcom/android/inputmethod/latin/LatinIME;->DEBUG:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onUpdateSelection: oss="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ose="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", lss="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", lse="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", nss="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", nse="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", cs="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ce="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-ne p3, p6, :cond_1

    if-eq p4, p6, :cond_5

    :cond_1
    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    if-eq v3, p3, :cond_5

    move v1, v0

    :goto_0
    if-ne p5, v6, :cond_6

    if-ne p6, v6, :cond_6

    :goto_1
    iget-boolean v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v3, p1, p3}, Lcom/android/inputmethod/latin/RichInputConnection;->isBelatedExpectedUpdate(II)Z

    move-result v3

    if-nez v3, :cond_4

    iput v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v1, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0, p3}, Lcom/android/inputmethod/latin/LatinIME;->resetEntireInputState(I)V

    :cond_3
    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    :cond_4
    iput-boolean v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    iput p3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionStart:I

    iput p4, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    iget-object v2, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeState:Lcom/android/inputmethod/latin/LatinIME$SubtypeState;

    invoke-virtual {v2}, Lcom/android/inputmethod/latin/LatinIME$SubtypeState;->currentSubtypeUsed()V

    return-void

    :cond_5
    move v1, v2

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method public onWindowHidden()V
    .locals 2

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onWindowHidden()V

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->closing()V

    :cond_0
    return-void
.end method

.method public onWordAddedToUserDictionary(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/WordComposer;->isComposingWord()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;->setActualWordBeingAdded(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v2

    iget v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastSelectionEnd:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;->tryReplaceWithActualWord(Lcom/android/inputmethod/latin/RichInputConnection;Landroid/view/inputmethod/EditorInfo;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mPositionalInfoForUserDictPendingAddition:Lcom/android/inputmethod/latin/PositionalInfoForUserDictPendingAddition;

    goto :goto_0
.end method

.method public pickSuggestionManually(ILjava/lang/CharSequence;)V
    .locals 10
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;

    const/4 v8, 0x4

    const/4 v9, -0x2

    const/4 v6, 0x0

    const/4 v4, 0x1

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->getSuggestions()Lcom/android/inputmethod/latin/SuggestedWords;

    move-result-object v5

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-ne v7, v4, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->isShowingPunctuationList()Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, ""

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, p1, v5}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnManualSuggestion(Ljava/lang/String;Ljava/lang/String;ILcom/android/inputmethod/latin/SuggestedWords;)V

    invoke-interface {p2, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2, v9, v9}, Lcom/android/inputmethod/latin/LatinIME;->onCodeInput(III)V

    :goto_0
    return-void

    :cond_0
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/RichInputConnection;->beginBatchEdit()V

    iget v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    if-ne v8, v7, :cond_1

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-lez v7, :cond_1

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/WordComposer;->isBatchMode()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {p2, v6}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v1

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v7, v1}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceStripper(I)Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v7, v1}, Lcom/android/inputmethod/latin/SettingsValues;->isWeakSpaceSwapper(I)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/LatinIME;->promotePhantomSpace()V

    :cond_1
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/SettingsValues;->isApplicationSpecifiedCompletionsOn()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

    if-eqz v7, :cond_3

    if-ltz p1, :cond_3

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

    array-length v7, v7

    if-ge p1, v7, :cond_3

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->clear()V

    :cond_2
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v6}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    invoke-direct {p0, v4}, Lcom/android/inputmethod/latin/LatinIME;->resetComposingState(Z)V

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mApplicationSpecifiedCompletions:[Landroid/view/inputmethod/CompletionInfo;

    aget-object v0, v6, p1

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v6, v0}, Lcom/android/inputmethod/latin/RichInputConnection;->commitCompletion(Landroid/view/inputmethod/CompletionInfo;)V

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/RichInputConnection;->endBatchEdit()V

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mWordComposer:Lcom/android/inputmethod/latin/WordComposer;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/WordComposer;->getTypedWord()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7, p1, v5}, Lcom/android/inputmethod/latin/LatinImeLogger;->logOnManualSuggestion(Ljava/lang/String;Ljava/lang/String;ILcom/android/inputmethod/latin/SuggestedWords;)V

    iput-boolean v4, p0, Lcom/android/inputmethod/latin/LatinIME;->mExpectingUpdateSelection:Z

    const-string v7, ""

    invoke-direct {p0, p2, v4, v7}, Lcom/android/inputmethod/latin/LatinIME;->commitChosenWord(Ljava/lang/CharSequence;ILjava/lang/String;)V

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mConnection:Lcom/android/inputmethod/latin/RichInputConnection;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/RichInputConnection;->endBatchEdit()V

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mLastComposedWord:Lcom/android/inputmethod/latin/LastComposedWord;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/LastComposedWord;->deactivate()V

    iput v8, p0, Lcom/android/inputmethod/latin/LatinIME;->mSpaceState:I

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->updateShiftState()V

    if-nez p1, :cond_4

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v7}, Lcom/android/inputmethod/latin/Suggest;->getUnigramDictionaries()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v7

    invoke-static {v7, p2, v4}, Lcom/android/inputmethod/latin/AutoCorrection;->isValidWord(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/CharSequence;Z)Z

    move-result v7

    if-nez v7, :cond_4

    :goto_1
    if-eqz v4, :cond_5

    iget-boolean v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsUserDictionaryAvailable:Z

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    iget-object v7, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    iget-object v7, v7, Lcom/android/inputmethod/latin/SettingsValues;->mHintToSaveText:Ljava/lang/CharSequence;

    invoke-virtual {v6, p2, v7}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->showAddToDictionaryHint(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    move v4, v6

    goto :goto_1

    :cond_5
    iget-object v6, p0, Lcom/android/inputmethod/latin/LatinIME;->mHandler:Lcom/android/inputmethod/latin/LatinIME$UIHandler;

    invoke-virtual {v6}, Lcom/android/inputmethod/latin/LatinIME$UIHandler;->postUpdateSuggestionStrip()V

    goto/16 :goto_0
.end method

.method public promotePhantomSpace()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mCurrentSettings:Lcom/android/inputmethod/latin/SettingsValues;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/SettingsValues;->shouldInsertSpacesAutomatically()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/android/inputmethod/latin/LatinIME;->sendKeyCodePoint(I)V

    :cond_0
    return-void
.end method

.method resetSuggestMainDict()V
    .locals 2

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSubtypeSwitcher:Lcom/android/inputmethod/latin/SubtypeSwitcher;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getCurrentSubtypeLocale()Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    invoke-virtual {v1, p0, v0, p0}, Lcom/android/inputmethod/latin/Suggest;->resetMainDict(Landroid/content/Context;Ljava/util/Locale;Lcom/android/inputmethod/latin/Suggest$SuggestInitializationListener;)V

    invoke-static {p0, v0}, Lcom/android/inputmethod/latin/DictionaryFactory;->isDictionaryAvailable(Landroid/content/Context;Ljava/util/Locale;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mIsMainDictionaryAvailable:Z

    goto :goto_0
.end method

.method public setCandidatesView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public setInputView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->setInputView(Landroid/view/View;)V

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getWindow()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x102001c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mExtractArea:Landroid/view/View;

    const v0, 0x7f08003e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    const v0, 0x7f08003f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionsContainer:Landroid/view/View;

    const v0, 0x7f080040

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    iput-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggestionStripView:Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;

    invoke-virtual {v0, p0, p1}, Lcom/android/inputmethod/latin/suggestions/SuggestionStripView;->setListener(Lcom/android/inputmethod/latin/suggestions/SuggestionStripView$Listener;Landroid/view/View;)V

    :cond_0
    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sVISUALDEBUG:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    const/high16 v1, 0x10ff0000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_1
    return-void
.end method

.method public showOptionDialog(Landroid/app/AlertDialog;)V
    .locals 5
    .param p1    # Landroid/app/AlertDialog;

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyboardSwitcher:Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getMainKeyboardView()Lcom/android/inputmethod/keyboard/MainKeyboardView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {p1, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput-object v2, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    const/16 v3, 0x3eb

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const/high16 v3, 0x20000

    invoke-virtual {v1, v3}, Landroid/view/Window;->addFlags(I)V

    iput-object p1, p0, Lcom/android/inputmethod/latin/LatinIME;->mOptionsDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public showWindow(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mSuggest:Lcom/android/inputmethod/latin/Suggest;

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/latin/LatinIME;->TAG:Ljava/lang/String;

    const-string v1, "Load suggest in showWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/inputmethod/latin/LatinIME;->initSuggest()V

    :cond_0
    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->showWindow(Z)V

    return-void
.end method

.method public updateFullscreenMode()V
    .locals 2

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->updateFullscreenMode()V

    iget-object v0, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/LatinIME;->mKeyPreviewBackingView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isFullscreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
