.class public final Lcom/android/inputmethod/latin/R$xml;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "xml"
.end annotation


# static fields
.field public static final additional_subtype_settings:I = 0x7f060000

.field public static final kbd_10_10_7_symbols:I = 0x7f060001

.field public static final kbd_10_10_7_symbols_shift:I = 0x7f060002

.field public static final kbd_arabic:I = 0x7f060003

.field public static final kbd_azerty:I = 0x7f060004

.field public static final kbd_bengali:I = 0x7f060005

.field public static final kbd_bulgarian:I = 0x7f060006

.field public static final kbd_bulgarian_bds:I = 0x7f060007

.field public static final kbd_burmese:I = 0x7f060008

.field public static final kbd_colemak:I = 0x7f060009

.field public static final kbd_dvorak:I = 0x7f06000a

.field public static final kbd_east_slavic:I = 0x7f06000b

.field public static final kbd_farsi:I = 0x7f06000c

.field public static final kbd_georgian:I = 0x7f06000d

.field public static final kbd_greek:I = 0x7f06000e

.field public static final kbd_hebrew:I = 0x7f06000f

.field public static final kbd_hindi:I = 0x7f060010

.field public static final kbd_khmer:I = 0x7f060011

.field public static final kbd_more_keys_keyboard_template:I = 0x7f060012

.field public static final kbd_nordic:I = 0x7f060013

.field public static final kbd_number:I = 0x7f060014

.field public static final kbd_pcqwerty:I = 0x7f060015

.field public static final kbd_pcqwerty_symbols:I = 0x7f060016

.field public static final kbd_phone:I = 0x7f060017

.field public static final kbd_phone_symbols:I = 0x7f060018

.field public static final kbd_qwerty:I = 0x7f060019

.field public static final kbd_qwertz:I = 0x7f06001a

.field public static final kbd_south_slavic:I = 0x7f06001b

.field public static final kbd_spanish:I = 0x7f06001c

.field public static final kbd_suggestions_pane_template:I = 0x7f06001d

.field public static final kbd_symbols:I = 0x7f06001e

.field public static final kbd_symbols_shift:I = 0x7f06001f

.field public static final kbd_thai:I = 0x7f060020

.field public static final kbd_thai_symbols:I = 0x7f060021

.field public static final kbd_thai_symbols_shift:I = 0x7f060022

.field public static final kbd_urdu:I = 0x7f060023

.field public static final key_apostrophe:I = 0x7f060024

.field public static final key_azerty_quote:I = 0x7f060025

.field public static final key_colemak_colon:I = 0x7f060026

.field public static final key_dash:I = 0x7f060027

.field public static final key_f1:I = 0x7f060028

.field public static final key_f2:I = 0x7f060029

.field public static final key_greek_semicolon:I = 0x7f06002a

.field public static final key_question_exclamation:I = 0x7f06002b

.field public static final key_settings:I = 0x7f06002c

.field public static final key_shortcut:I = 0x7f06002d

.field public static final key_space:I = 0x7f06002e

.field public static final key_styles_common:I = 0x7f06002f

.field public static final key_styles_currency:I = 0x7f060030

.field public static final key_styles_currency_dollar:I = 0x7f060031

.field public static final key_styles_currency_euro:I = 0x7f060032

.field public static final key_styles_enter:I = 0x7f060033

.field public static final key_styles_f1:I = 0x7f060034

.field public static final key_styles_number:I = 0x7f060035

.field public static final key_thai_kho_khuat:I = 0x7f060036

.field public static final keyboard_layout_set_arabic:I = 0x7f060037

.field public static final keyboard_layout_set_azerty:I = 0x7f060038

.field public static final keyboard_layout_set_bengali:I = 0x7f060039

.field public static final keyboard_layout_set_bulgarian:I = 0x7f06003a

.field public static final keyboard_layout_set_bulgarian_bds:I = 0x7f06003b

.field public static final keyboard_layout_set_burmese:I = 0x7f06003c

.field public static final keyboard_layout_set_colemak:I = 0x7f06003d

.field public static final keyboard_layout_set_dvorak:I = 0x7f06003e

.field public static final keyboard_layout_set_east_slavic:I = 0x7f06003f

.field public static final keyboard_layout_set_farsi:I = 0x7f060040

.field public static final keyboard_layout_set_georgian:I = 0x7f060041

.field public static final keyboard_layout_set_greek:I = 0x7f060042

.field public static final keyboard_layout_set_hebrew:I = 0x7f060043

.field public static final keyboard_layout_set_hindi:I = 0x7f060044

.field public static final keyboard_layout_set_khmer:I = 0x7f060045

.field public static final keyboard_layout_set_nordic:I = 0x7f060046

.field public static final keyboard_layout_set_pcqwerty:I = 0x7f060047

.field public static final keyboard_layout_set_qwerty:I = 0x7f060048

.field public static final keyboard_layout_set_qwertz:I = 0x7f060049

.field public static final keyboard_layout_set_south_slavic:I = 0x7f06004a

.field public static final keyboard_layout_set_spanish:I = 0x7f06004b

.field public static final keyboard_layout_set_thai:I = 0x7f06004c

.field public static final keyboard_layout_set_urdu:I = 0x7f06004d

.field public static final keys_comma_period:I = 0x7f06004e

.field public static final keys_curly_brackets:I = 0x7f06004f

.field public static final keys_dvorak_123:I = 0x7f060050

.field public static final keys_less_greater:I = 0x7f060051

.field public static final keys_parentheses:I = 0x7f060052

.field public static final keys_pcqwerty2_right3:I = 0x7f060053

.field public static final keys_pcqwerty3_right2:I = 0x7f060054

.field public static final keys_pcqwerty4_right3:I = 0x7f060055

.field public static final keys_pcqwerty_symbols1:I = 0x7f060056

.field public static final keys_pcqwerty_symbols2:I = 0x7f060057

.field public static final keys_pcqwerty_symbols3:I = 0x7f060058

.field public static final keys_pcqwerty_symbols4:I = 0x7f060059

.field public static final keys_square_brackets:I = 0x7f06005a

.field public static final method:I = 0x7f06005b

.field public static final prefs:I = 0x7f06005c

.field public static final prefs_for_debug:I = 0x7f06005d

.field public static final row_dvorak4:I = 0x7f06005e

.field public static final row_hebrew4:I = 0x7f06005f

.field public static final row_pcqwerty5:I = 0x7f060060

.field public static final row_qwerty4:I = 0x7f060061

.field public static final row_symbols4:I = 0x7f060062

.field public static final row_symbols_shift4:I = 0x7f060063

.field public static final rowkeys_arabic1:I = 0x7f060064

.field public static final rowkeys_arabic2:I = 0x7f060065

.field public static final rowkeys_arabic3:I = 0x7f060066

.field public static final rowkeys_azerty1:I = 0x7f060067

.field public static final rowkeys_azerty2:I = 0x7f060068

.field public static final rowkeys_azerty3:I = 0x7f060069

.field public static final rowkeys_bengali1:I = 0x7f06006a

.field public static final rowkeys_bengali2:I = 0x7f06006b

.field public static final rowkeys_bengali3:I = 0x7f06006c

.field public static final rowkeys_bulgarian1:I = 0x7f06006d

.field public static final rowkeys_bulgarian2:I = 0x7f06006e

.field public static final rowkeys_bulgarian3:I = 0x7f06006f

.field public static final rowkeys_bulgarian_bds1:I = 0x7f060070

.field public static final rowkeys_bulgarian_bds2:I = 0x7f060071

.field public static final rowkeys_bulgarian_bds3:I = 0x7f060072

.field public static final rowkeys_burmese1:I = 0x7f060073

.field public static final rowkeys_burmese2:I = 0x7f060074

.field public static final rowkeys_burmese3:I = 0x7f060075

.field public static final rowkeys_colemak1:I = 0x7f060076

.field public static final rowkeys_colemak2:I = 0x7f060077

.field public static final rowkeys_colemak3:I = 0x7f060078

.field public static final rowkeys_dvorak1:I = 0x7f060079

.field public static final rowkeys_dvorak2:I = 0x7f06007a

.field public static final rowkeys_dvorak3:I = 0x7f06007b

.field public static final rowkeys_east_slavic1:I = 0x7f06007c

.field public static final rowkeys_east_slavic2:I = 0x7f06007d

.field public static final rowkeys_east_slavic3:I = 0x7f06007e

.field public static final rowkeys_farsi1:I = 0x7f06007f

.field public static final rowkeys_farsi2:I = 0x7f060080

.field public static final rowkeys_farsi3:I = 0x7f060081

.field public static final rowkeys_georgian1:I = 0x7f060082

.field public static final rowkeys_georgian2:I = 0x7f060083

.field public static final rowkeys_georgian3:I = 0x7f060084

.field public static final rowkeys_greek1:I = 0x7f060085

.field public static final rowkeys_greek2:I = 0x7f060086

.field public static final rowkeys_greek3:I = 0x7f060087

.field public static final rowkeys_hebrew1:I = 0x7f060088

.field public static final rowkeys_hebrew2:I = 0x7f060089

.field public static final rowkeys_hebrew3:I = 0x7f06008a

.field public static final rowkeys_hindi1:I = 0x7f06008b

.field public static final rowkeys_hindi2:I = 0x7f06008c

.field public static final rowkeys_hindi3:I = 0x7f06008d

.field public static final rowkeys_khmer1:I = 0x7f06008e

.field public static final rowkeys_khmer2:I = 0x7f06008f

.field public static final rowkeys_khmer3:I = 0x7f060090

.field public static final rowkeys_nordic1:I = 0x7f060091

.field public static final rowkeys_nordic2:I = 0x7f060092

.field public static final rowkeys_pcqwerty1:I = 0x7f060093

.field public static final rowkeys_pcqwerty2:I = 0x7f060094

.field public static final rowkeys_pcqwerty3:I = 0x7f060095

.field public static final rowkeys_pcqwerty4:I = 0x7f060096

.field public static final rowkeys_qwerty1:I = 0x7f060097

.field public static final rowkeys_qwerty2:I = 0x7f060098

.field public static final rowkeys_qwerty3:I = 0x7f060099

.field public static final rowkeys_qwertz1:I = 0x7f06009a

.field public static final rowkeys_qwertz3:I = 0x7f06009b

.field public static final rowkeys_south_slavic1:I = 0x7f06009c

.field public static final rowkeys_south_slavic2:I = 0x7f06009d

.field public static final rowkeys_south_slavic3:I = 0x7f06009e

.field public static final rowkeys_spanish2:I = 0x7f06009f

.field public static final rowkeys_symbols1:I = 0x7f0600a0

.field public static final rowkeys_symbols2:I = 0x7f0600a1

.field public static final rowkeys_symbols3:I = 0x7f0600a2

.field public static final rowkeys_symbols_shift1:I = 0x7f0600a3

.field public static final rowkeys_symbols_shift2:I = 0x7f0600a4

.field public static final rowkeys_symbols_shift3:I = 0x7f0600a5

.field public static final rowkeys_thai1:I = 0x7f0600a6

.field public static final rowkeys_thai2:I = 0x7f0600a7

.field public static final rowkeys_thai3:I = 0x7f0600a8

.field public static final rowkeys_thai4:I = 0x7f0600a9

.field public static final rowkeys_thai_digits:I = 0x7f0600aa

.field public static final rowkeys_urdu1:I = 0x7f0600ab

.field public static final rowkeys_urdu2:I = 0x7f0600ac

.field public static final rowkeys_urdu3:I = 0x7f0600ad

.field public static final rows_10_10_7_symbols:I = 0x7f0600ae

.field public static final rows_10_10_7_symbols_shift:I = 0x7f0600af

.field public static final rows_arabic:I = 0x7f0600b0

.field public static final rows_azerty:I = 0x7f0600b1

.field public static final rows_bengali:I = 0x7f0600b2

.field public static final rows_bulgarian:I = 0x7f0600b3

.field public static final rows_bulgarian_bds:I = 0x7f0600b4

.field public static final rows_burmese:I = 0x7f0600b5

.field public static final rows_colemak:I = 0x7f0600b6

.field public static final rows_dvorak:I = 0x7f0600b7

.field public static final rows_east_slavic:I = 0x7f0600b8

.field public static final rows_farsi:I = 0x7f0600b9

.field public static final rows_georgian:I = 0x7f0600ba

.field public static final rows_greek:I = 0x7f0600bb

.field public static final rows_hebrew:I = 0x7f0600bc

.field public static final rows_hindi:I = 0x7f0600bd

.field public static final rows_khmer:I = 0x7f0600be

.field public static final rows_nordic:I = 0x7f0600bf

.field public static final rows_number:I = 0x7f0600c0

.field public static final rows_number_normal:I = 0x7f0600c1

.field public static final rows_number_password:I = 0x7f0600c2

.field public static final rows_pcqwerty:I = 0x7f0600c3

.field public static final rows_pcqwerty_symbols:I = 0x7f0600c4

.field public static final rows_phone:I = 0x7f0600c5

.field public static final rows_phone_symbols:I = 0x7f0600c6

.field public static final rows_qwerty:I = 0x7f0600c7

.field public static final rows_qwertz:I = 0x7f0600c8

.field public static final rows_south_slavic:I = 0x7f0600c9

.field public static final rows_spanish:I = 0x7f0600ca

.field public static final rows_symbols:I = 0x7f0600cb

.field public static final rows_symbols_shift:I = 0x7f0600cc

.field public static final rows_thai:I = 0x7f0600cd

.field public static final rows_thai_symbols:I = 0x7f0600ce

.field public static final rows_thai_symbols_shift:I = 0x7f0600cf

.field public static final rows_urdu:I = 0x7f0600d0

.field public static final spell_checker_settings:I = 0x7f0600d1

.field public static final spellchecker:I = 0x7f0600d2


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
