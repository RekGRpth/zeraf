.class public final Lcom/android/inputmethod/latin/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action:I = 0x7f080005

.field public static final actionCustomLabel:I = 0x7f08003b

.field public static final actionDone:I = 0x7f080039

.field public static final actionGo:I = 0x7f080035

.field public static final actionNext:I = 0x7f080038

.field public static final actionNone:I = 0x7f080034

.field public static final actionPrevious:I = 0x7f08003a

.field public static final actionSearch:I = 0x7f080036

.field public static final actionSend:I = 0x7f080037

.field public static final actionUnspecified:I = 0x7f080033

.field public static final alignLeft:I = 0x7f08000c

.field public static final alignLeftOfCenter:I = 0x7f08000e

.field public static final alignRight:I = 0x7f08000d

.field public static final alphabet:I = 0x7f080025

.field public static final alphabetAutomaticShifted:I = 0x7f080027

.field public static final alphabetManualShifted:I = 0x7f080026

.field public static final alphabetShiftLockShifted:I = 0x7f080029

.field public static final alphabetShiftLocked:I = 0x7f080028

.field public static final altCodeWhileTyping:I = 0x7f08000a

.field public static final autoCorrectBold:I = 0x7f080000

.field public static final autoCorrectUnderline:I = 0x7f080001

.field public static final autoXScale:I = 0x7f08001b

.field public static final bold:I = 0x7f080022

.field public static final boldItalic:I = 0x7f080024

.field public static final disableAdditionalMoreKeys:I = 0x7f080020

.field public static final disableKeyHintLabel:I = 0x7f08001f

.field public static final email:I = 0x7f080031

.field public static final enableLongPress:I = 0x7f08000b

.field public static final fillRight:I = 0x7f080021

.field public static final followKeyHintLabelRatio:I = 0x7f080015

.field public static final followKeyLabelRatio:I = 0x7f080013

.field public static final followKeyLargeLabelRatio:I = 0x7f080014

.field public static final followKeyLargeLetterRatio:I = 0x7f080011

.field public static final followKeyLetterRatio:I = 0x7f080012

.field public static final fontMonoSpace:I = 0x7f080010

.field public static final fontNormal:I = 0x7f08000f

.field public static final fromCustomActionLabel:I = 0x7f08001e

.field public static final functional:I = 0x7f080004

.field public static final hasHintLabel:I = 0x7f080018

.field public static final hasPopupHint:I = 0x7f080016

.field public static final hasShiftedLetterHint:I = 0x7f080017

.field public static final im:I = 0x7f080032

.field public static final isRepeatable:I = 0x7f080008

.field public static final italic:I = 0x7f080023

.field public static final key_preview_backing:I = 0x7f08003e

.field public static final keyboard_layout_set_spinner:I = 0x7f08003d

.field public static final keyboard_view:I = 0x7f080041

.field public static final more_keys_keyboard_view:I = 0x7f080042

.field public static final more_suggestions_view:I = 0x7f080043

.field public static final noKeyPreview:I = 0x7f080009

.field public static final normal:I = 0x7f080003

.field public static final number:I = 0x7f08002e

.field public static final phone:I = 0x7f08002c

.field public static final phoneSymbols:I = 0x7f08002d

.field public static final preserveCase:I = 0x7f08001c

.field public static final research_feedback_cancel_button:I = 0x7f080048

.field public static final research_feedback_contents:I = 0x7f080046

.field public static final research_feedback_fragment:I = 0x7f080045

.field public static final research_feedback_include_history:I = 0x7f080047

.field public static final research_feedback_layout:I = 0x7f080044

.field public static final research_feedback_send_button:I = 0x7f080049

.field public static final shiftedLetterActivated:I = 0x7f08001d

.field public static final sound_effect_volume_bar:I = 0x7f08004b

.field public static final sound_effect_volume_value:I = 0x7f08004a

.field public static final stickyOff:I = 0x7f080006

.field public static final stickyOn:I = 0x7f080007

.field public static final subtype_locale_spinner:I = 0x7f08003c

.field public static final suggestion_strip_view:I = 0x7f080040

.field public static final suggestions_container:I = 0x7f08003f

.field public static final suggestions_strip:I = 0x7f08004c

.field public static final symbols:I = 0x7f08002a

.field public static final symbolsShifted:I = 0x7f08002b

.field public static final text:I = 0x7f08002f

.field public static final url:I = 0x7f080030

.field public static final validTypedWordBold:I = 0x7f080002

.field public static final vibration_settings:I = 0x7f08004e

.field public static final vibration_value:I = 0x7f08004d

.field public static final withIconLeft:I = 0x7f080019

.field public static final withIconRight:I = 0x7f08001a


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
