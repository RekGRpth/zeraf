.class public final Lcom/android/inputmethod/latin/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final config_delay_before_fadeout_language_on_spacebar:I = 0x7f0a001d

.field public static final config_delay_update_old_suggestions:I = 0x7f0a0002

.field public static final config_delay_update_shift_state:I = 0x7f0a0003

.field public static final config_delay_update_suggestions:I = 0x7f0a0001

.field public static final config_device_form_factor:I = 0x7f0a0000

.field public static final config_double_spaces_turn_into_period_timeout:I = 0x7f0a0009

.field public static final config_gesture_dynamic_threshold_decay_duration:I = 0x7f0a0017

.field public static final config_gesture_dynamic_time_threshold_from:I = 0x7f0a0018

.field public static final config_gesture_dynamic_time_threshold_to:I = 0x7f0a0019

.field public static final config_gesture_floating_preview_text_linger_timeout:I = 0x7f0a000c

.field public static final config_gesture_preview_trail_fadeout_duration:I = 0x7f0a000e

.field public static final config_gesture_preview_trail_fadeout_start_delay:I = 0x7f0a000d

.field public static final config_gesture_preview_trail_update_interval:I = 0x7f0a000f

.field public static final config_gesture_recognition_minimum_time:I = 0x7f0a001a

.field public static final config_gesture_static_time_threshold_after_fast_typing:I = 0x7f0a0016

.field public static final config_ignore_alt_code_key_timeout:I = 0x7f0a0015

.field public static final config_key_preview_linger_timeout:I = 0x7f0a000b

.field public static final config_key_repeat_interval:I = 0x7f0a0012

.field public static final config_key_repeat_start_timeout:I = 0x7f0a0011

.field public static final config_keyboard_grid_height:I = 0x7f0a0008

.field public static final config_keyboard_grid_width:I = 0x7f0a0007

.field public static final config_language_on_spacebar_final_alpha:I = 0x7f0a0004

.field public static final config_long_press_key_timeout:I = 0x7f0a0013

.field public static final config_long_press_shift_key_timeout:I = 0x7f0a0014

.field public static final config_max_more_keys_column:I = 0x7f0a000a

.field public static final config_more_keys_keyboard_fadein_anim_time:I = 0x7f0a0005

.field public static final config_more_keys_keyboard_fadeout_anim_time:I = 0x7f0a0006

.field public static final config_suppress_key_preview_after_batch_input_duration:I = 0x7f0a001b

.field public static final config_touch_noise_threshold_time:I = 0x7f0a0010

.field public static final log_screen_metrics:I = 0x7f0a001c

.field public static final max_more_suggestions_row:I = 0x7f0a001e

.field public static final suggestions_count_in_strip:I = 0x7f0a001f


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
