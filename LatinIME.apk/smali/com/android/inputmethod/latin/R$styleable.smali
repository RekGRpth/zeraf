.class public final Lcom/android/inputmethod/latin/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final Keyboard:[I

.field public static final KeyboardLayoutSet_Element:[I

.field public static final KeyboardLayoutSet_Element_elementKeyboard:I = 0x1

.field public static final KeyboardLayoutSet_Element_elementName:I = 0x0

.field public static final KeyboardLayoutSet_Element_enableProximityCharsCorrection:I = 0x2

.field public static final KeyboardTheme:[I

.field public static final KeyboardTheme_keyboardStyle:I = 0x0

.field public static final KeyboardTheme_keyboardViewStyle:I = 0x1

.field public static final KeyboardTheme_mainKeyboardViewStyle:I = 0x2

.field public static final KeyboardTheme_moreKeysKeyboardPanelStyle:I = 0x5

.field public static final KeyboardTheme_moreKeysKeyboardStyle:I = 0x3

.field public static final KeyboardTheme_moreKeysKeyboardViewStyle:I = 0x4

.field public static final KeyboardTheme_moreSuggestionsViewStyle:I = 0x8

.field public static final KeyboardTheme_suggestionBackgroundStyle:I = 0x9

.field public static final KeyboardTheme_suggestionPreviewBackgroundStyle:I = 0xa

.field public static final KeyboardTheme_suggestionStripViewStyle:I = 0x7

.field public static final KeyboardTheme_suggestionsStripBackgroundStyle:I = 0x6

.field public static final KeyboardView:[I

.field public static final KeyboardView_backgroundDimAlpha:I = 0xf

.field public static final KeyboardView_gestureFloatingPreviewColor:I = 0x13

.field public static final KeyboardView_gestureFloatingPreviewHorizontalPadding:I = 0x14

.field public static final KeyboardView_gestureFloatingPreviewRoundRadius:I = 0x16

.field public static final KeyboardView_gestureFloatingPreviewTextColor:I = 0x11

.field public static final KeyboardView_gestureFloatingPreviewTextLingerTimeout:I = 0x17

.field public static final KeyboardView_gestureFloatingPreviewTextOffset:I = 0x12

.field public static final KeyboardView_gestureFloatingPreviewTextSize:I = 0x10

.field public static final KeyboardView_gestureFloatingPreviewVerticalPadding:I = 0x15

.field public static final KeyboardView_gesturePreviewTrailColor:I = 0x1b

.field public static final KeyboardView_gesturePreviewTrailEndWidth:I = 0x1d

.field public static final KeyboardView_gesturePreviewTrailFadeoutDuration:I = 0x19

.field public static final KeyboardView_gesturePreviewTrailFadeoutStartDelay:I = 0x18

.field public static final KeyboardView_gesturePreviewTrailStartWidth:I = 0x1c

.field public static final KeyboardView_gesturePreviewTrailUpdateInterval:I = 0x1a

.field public static final KeyboardView_keyBackground:I = 0x0

.field public static final KeyboardView_keyHintLetterPadding:I = 0x2

.field public static final KeyboardView_keyLabelHorizontalPadding:I = 0x1

.field public static final KeyboardView_keyPopupHintLetterPadding:I = 0x3

.field public static final KeyboardView_keyPreviewHeight:I = 0xb

.field public static final KeyboardView_keyPreviewLayout:I = 0x6

.field public static final KeyboardView_keyPreviewLingerTimeout:I = 0xc

.field public static final KeyboardView_keyPreviewOffset:I = 0xa

.field public static final KeyboardView_keyShiftedLetterHintPadding:I = 0x4

.field public static final KeyboardView_keyTextShadowRadius:I = 0x5

.field public static final KeyboardView_moreKeysLayout:I = 0xe

.field public static final KeyboardView_state_has_morekeys:I = 0x9

.field public static final KeyboardView_state_left_edge:I = 0x7

.field public static final KeyboardView_state_right_edge:I = 0x8

.field public static final KeyboardView_verticalCorrection:I = 0xd

.field public static final Keyboard_Case:[I

.field public static final Keyboard_Case_clobberSettingsKey:I = 0x5

.field public static final Keyboard_Case_countryCode:I = 0xd

.field public static final Keyboard_Case_hasShortcutKey:I = 0x7

.field public static final Keyboard_Case_imeAction:I = 0xa

.field public static final Keyboard_Case_isMultiLine:I = 0x9

.field public static final Keyboard_Case_keyboardLayoutSetElement:I = 0x0

.field public static final Keyboard_Case_languageCode:I = 0xc

.field public static final Keyboard_Case_languageSwitchKeyEnabled:I = 0x8

.field public static final Keyboard_Case_localeCode:I = 0xb

.field public static final Keyboard_Case_mode:I = 0x1

.field public static final Keyboard_Case_navigateNext:I = 0x2

.field public static final Keyboard_Case_navigatePrevious:I = 0x3

.field public static final Keyboard_Case_passwordInput:I = 0x4

.field public static final Keyboard_Case_shortcutKeyEnabled:I = 0x6

.field public static final Keyboard_Include:[I

.field public static final Keyboard_Include_keyboardLayout:I = 0x0

.field public static final Keyboard_Key:[I

.field public static final Keyboard_KeyStyle:[I

.field public static final Keyboard_KeyStyle_parentStyle:I = 0x1

.field public static final Keyboard_KeyStyle_styleName:I = 0x0

.field public static final Keyboard_Key_additionalMoreKeys:I = 0x3

.field public static final Keyboard_Key_altCode:I = 0x1

.field public static final Keyboard_Key_backgroundType:I = 0x5

.field public static final Keyboard_Key_code:I = 0x0

.field public static final Keyboard_Key_keyActionFlags:I = 0x6

.field public static final Keyboard_Key_keyHintLabel:I = 0x9

.field public static final Keyboard_Key_keyHintLabelColor:I = 0x1f

.field public static final Keyboard_Key_keyHintLabelRatio:I = 0x19

.field public static final Keyboard_Key_keyHintLetterColor:I = 0x1e

.field public static final Keyboard_Key_keyHintLetterRatio:I = 0x18

.field public static final Keyboard_Key_keyIcon:I = 0xb

.field public static final Keyboard_Key_keyIconDisabled:I = 0xc

.field public static final Keyboard_Key_keyIconPreview:I = 0xd

.field public static final Keyboard_Key_keyLabel:I = 0x8

.field public static final Keyboard_Key_keyLabelFlags:I = 0xa

.field public static final Keyboard_Key_keyLabelSize:I = 0x15

.field public static final Keyboard_Key_keyLargeLabelRatio:I = 0x17

.field public static final Keyboard_Key_keyLargeLetterRatio:I = 0x16

.field public static final Keyboard_Key_keyLetterSize:I = 0x14

.field public static final Keyboard_Key_keyOutputText:I = 0x7

.field public static final Keyboard_Key_keyPreviewTextColor:I = 0x22

.field public static final Keyboard_Key_keyPreviewTextRatio:I = 0x23

.field public static final Keyboard_Key_keyShiftedLetterHintActivatedColor:I = 0x21

.field public static final Keyboard_Key_keyShiftedLetterHintInactivatedColor:I = 0x20

.field public static final Keyboard_Key_keyShiftedLetterHintRatio:I = 0x1a

.field public static final Keyboard_Key_keyStyle:I = 0xe

.field public static final Keyboard_Key_keyTextColor:I = 0x1b

.field public static final Keyboard_Key_keyTextInactivatedColor:I = 0x1d

.field public static final Keyboard_Key_keyTextShadowColor:I = 0x1c

.field public static final Keyboard_Key_keyTypeface:I = 0x13

.field public static final Keyboard_Key_keyWidth:I = 0x11

.field public static final Keyboard_Key_keyXPos:I = 0x12

.field public static final Keyboard_Key_maxMoreKeysColumn:I = 0x4

.field public static final Keyboard_Key_moreKeys:I = 0x2

.field public static final Keyboard_Key_visualInsetsLeft:I = 0xf

.field public static final Keyboard_Key_visualInsetsRight:I = 0x10

.field public static final Keyboard_horizontalGap:I = 0x9

.field public static final Keyboard_iconDeleteKey:I = 0xd

.field public static final Keyboard_iconEnterKey:I = 0x10

.field public static final Keyboard_iconLanguageSwitchKey:I = 0x19

.field public static final Keyboard_iconSearchKey:I = 0x11

.field public static final Keyboard_iconSettingsKey:I = 0xe

.field public static final Keyboard_iconShiftKey:I = 0xc

.field public static final Keyboard_iconShiftKeyShifted:I = 0x16

.field public static final Keyboard_iconShortcutForLabel:I = 0x14

.field public static final Keyboard_iconShortcutKey:I = 0x13

.field public static final Keyboard_iconShortcutKeyDisabled:I = 0x17

.field public static final Keyboard_iconSpaceKey:I = 0xf

.field public static final Keyboard_iconSpaceKeyForNumberLayout:I = 0x15

.field public static final Keyboard_iconTabKey:I = 0x12

.field public static final Keyboard_iconTabKeyPreview:I = 0x18

.field public static final Keyboard_iconZwjKey:I = 0x1b

.field public static final Keyboard_iconZwnjKey:I = 0x1a

.field public static final Keyboard_keyboardBottomPadding:I = 0x6

.field public static final Keyboard_keyboardHeight:I = 0x2

.field public static final Keyboard_keyboardHorizontalEdgesPadding:I = 0x7

.field public static final Keyboard_keyboardTopPadding:I = 0x5

.field public static final Keyboard_maxKeyboardHeight:I = 0x3

.field public static final Keyboard_minKeyboardHeight:I = 0x4

.field public static final Keyboard_moreKeysTemplate:I = 0xb

.field public static final Keyboard_rowHeight:I = 0x8

.field public static final Keyboard_themeId:I = 0x0

.field public static final Keyboard_touchPositionCorrectionData:I = 0x1

.field public static final Keyboard_verticalGap:I = 0xa

.field public static final MainKeyboardView:[I

.field public static final MainKeyboardView_altCodeKeyWhileTypingFadeinAnimator:I = 0x8

.field public static final MainKeyboardView_altCodeKeyWhileTypingFadeoutAnimator:I = 0x7

.field public static final MainKeyboardView_autoCorrectionSpacebarLedEnabled:I = 0x0

.field public static final MainKeyboardView_autoCorrectionSpacebarLedIcon:I = 0x1

.field public static final MainKeyboardView_gestureDetectFastMoveSpeedThreshold:I = 0x15

.field public static final MainKeyboardView_gestureDynamicDistanceThresholdFrom:I = 0x19

.field public static final MainKeyboardView_gestureDynamicDistanceThresholdTo:I = 0x1a

.field public static final MainKeyboardView_gestureDynamicThresholdDecayDuration:I = 0x16

.field public static final MainKeyboardView_gestureDynamicTimeThresholdFrom:I = 0x17

.field public static final MainKeyboardView_gestureDynamicTimeThresholdTo:I = 0x18

.field public static final MainKeyboardView_gestureRecognitionMinimumTime:I = 0x1c

.field public static final MainKeyboardView_gestureRecognitionSpeedThreshold:I = 0x1d

.field public static final MainKeyboardView_gestureSamplingMinimumDistance:I = 0x1b

.field public static final MainKeyboardView_gestureStaticTimeThresholdAfterFastTyping:I = 0x14

.field public static final MainKeyboardView_ignoreAltCodeKeyTimeout:I = 0x12

.field public static final MainKeyboardView_keyHysteresisDistance:I = 0x9

.field public static final MainKeyboardView_keyHysteresisDistanceForSlidingModifier:I = 0xa

.field public static final MainKeyboardView_keyRepeatInterval:I = 0xf

.field public static final MainKeyboardView_keyRepeatStartTimeout:I = 0xe

.field public static final MainKeyboardView_languageOnSpacebarFadeoutAnimator:I = 0x6

.field public static final MainKeyboardView_languageOnSpacebarFinalAlpha:I = 0x5

.field public static final MainKeyboardView_longPressKeyTimeout:I = 0x10

.field public static final MainKeyboardView_longPressShiftKeyTimeout:I = 0x11

.field public static final MainKeyboardView_showMoreKeysKeyboardAtTouchedPoint:I = 0x13

.field public static final MainKeyboardView_slidingKeyInputEnable:I = 0xd

.field public static final MainKeyboardView_spacebarTextColor:I = 0x3

.field public static final MainKeyboardView_spacebarTextRatio:I = 0x2

.field public static final MainKeyboardView_spacebarTextShadowColor:I = 0x4

.field public static final MainKeyboardView_suppressKeyPreviewAfterBatchInputDuration:I = 0x1e

.field public static final MainKeyboardView_touchNoiseThresholdDistance:I = 0xc

.field public static final MainKeyboardView_touchNoiseThresholdTime:I = 0xb

.field public static final SuggestionStripView:[I

.field public static final SuggestionStripView_alphaAutoCorrect:I = 0x7

.field public static final SuggestionStripView_alphaObsoleted:I = 0x9

.field public static final SuggestionStripView_alphaSuggested:I = 0x8

.field public static final SuggestionStripView_alphaTypedWord:I = 0x6

.field public static final SuggestionStripView_alphaValidTypedWord:I = 0x5

.field public static final SuggestionStripView_centerSuggestionPercentile:I = 0xb

.field public static final SuggestionStripView_colorAutoCorrect:I = 0x3

.field public static final SuggestionStripView_colorSuggested:I = 0x4

.field public static final SuggestionStripView_colorTypedWord:I = 0x2

.field public static final SuggestionStripView_colorValidTypedWord:I = 0x1

.field public static final SuggestionStripView_maxMoreSuggestionsRow:I = 0xc

.field public static final SuggestionStripView_minMoreSuggestionsWidth:I = 0xd

.field public static final SuggestionStripView_suggestionStripOption:I = 0x0

.field public static final SuggestionStripView_suggestionsCountInStrip:I = 0xa


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0xe

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->KeyboardLayoutSet_Element:[I

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->KeyboardTheme:[I

    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->KeyboardView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Case:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010096

    aput v2, v0, v1

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Include:[I

    const/16 v0, 0x24

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Key:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_KeyStyle:[I

    const/16 v0, 0x1f

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->MainKeyboardView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->SuggestionStripView:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f010056
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f01005a
        0x7f01005b
        0x7f01005c
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
        0x7f010061
        0x7f010062
        0x7f010063
        0x7f010064
        0x7f010065
        0x7f010066
        0x7f010067
        0x7f010068
        0x7f010069
        0x7f01006a
        0x7f01006b
        0x7f01006c
        0x7f01006d
        0x7f01006e
        0x7f01006f
        0x7f010070
        0x7f010071
    .end array-data

    :array_1
    .array-data 4
        0x7f0100a7
        0x7f0100a8
        0x7f0100a9
    .end array-data

    :array_2
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
    .end array-data

    :array_3
    .array-data 4
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
    .end array-data

    :array_4
    .array-data 4
        0x7f010097
        0x7f010098
        0x7f010099
        0x7f01009a
        0x7f01009b
        0x7f01009c
        0x7f01009d
        0x7f01009e
        0x7f01009f
        0x7f0100a0
        0x7f0100a1
        0x7f0100a2
        0x7f0100a3
        0x7f0100a4
    .end array-data

    :array_5
    .array-data 4
        0x7f010072
        0x7f010073
        0x7f010074
        0x7f010075
        0x7f010076
        0x7f010077
        0x7f010078
        0x7f010079
        0x7f01007a
        0x7f01007b
        0x7f01007c
        0x7f01007d
        0x7f01007e
        0x7f01007f
        0x7f010080
        0x7f010081
        0x7f010082
        0x7f010083
        0x7f010084
        0x7f010085
        0x7f010086
        0x7f010087
        0x7f010088
        0x7f010089
        0x7f01008a
        0x7f01008b
        0x7f01008c
        0x7f01008d
        0x7f01008e
        0x7f01008f
        0x7f010090
        0x7f010091
        0x7f010092
        0x7f010093
        0x7f010094
        0x7f010095
    .end array-data

    :array_6
    .array-data 4
        0x7f0100a5
        0x7f0100a6
    .end array-data

    :array_7
    .array-data 4
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f01003e
        0x7f01003f
        0x7f010040
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
    .end array-data

    :array_8
    .array-data 4
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
        0x7f010055
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
