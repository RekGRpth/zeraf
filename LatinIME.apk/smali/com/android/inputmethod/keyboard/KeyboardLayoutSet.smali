.class public final Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;
.super Ljava/lang/Object;
.source "KeyboardLayoutSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Builder;,
        Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;,
        Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$ElementParams;,
        Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException;
    }
.end annotation


# static fields
.field private static final DEBUG_CACHE:Z

.field private static final KEYBOARD_LAYOUT_SET_RESOURCE_PREFIX:Ljava/lang/String; = "keyboard_layout_set_"

.field private static final TAG:Ljava/lang/String;

.field private static final TAG_ELEMENT:Ljava/lang/String; = "Element"

.field private static final TAG_KEYBOARD_SET:Ljava/lang/String; = "KeyboardLayoutSet"

.field private static final sKeyboardCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/android/inputmethod/keyboard/KeyboardId;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/android/inputmethod/keyboard/Keyboard;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sKeysCache:Lcom/android/inputmethod/keyboard/internal/KeysCache;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->TAG:Ljava/lang/String;

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    sput-boolean v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->DEBUG_CACHE:Z

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeysCache;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/KeysCache;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeysCache:Lcom/android/inputmethod/keyboard/internal/KeysCache;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    return-void
.end method

.method public static clearKeyboardCache()V
    .locals 1

    sget-object v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    sget-object v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeysCache:Lcom/android/inputmethod/keyboard/internal/KeysCache;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/KeysCache;->clear()V

    return-void
.end method

.method private getKeyboard(Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$ElementParams;Lcom/android/inputmethod/keyboard/KeyboardId;)Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 7
    .param p1    # Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$ElementParams;
    .param p2    # Lcom/android/inputmethod/keyboard/KeyboardId;

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    invoke-virtual {v4, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/SoftReference;

    if-nez v3, :cond_3

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_5

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mContext:Landroid/content/Context;

    new-instance v5, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;

    invoke-direct {v5}, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;-><init>()V

    invoke-direct {v0, v4, v5}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;-><init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/internal/KeyboardParams;)V

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/KeyboardId;->isAlphabetKeyboard()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeysCache:Lcom/android/inputmethod/keyboard/internal/KeysCache;

    invoke-virtual {v0, v4}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->setAutoGenerate(Lcom/android/inputmethod/keyboard/internal/KeysCache;)V

    :cond_0
    iget v2, p1, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$ElementParams;->mKeyboardXmlId:I

    invoke-virtual {v0, v2, p2}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->load(ILcom/android/inputmethod/keyboard/KeyboardId;)Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    iget-boolean v4, v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mDisableTouchPositionCorrectionDataForTest:Z

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->disableTouchPositionCorrectionDataForTest()V

    :cond_1
    iget-boolean v4, p1, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$ElementParams;->mProximityCharsCorrectionEnabled:Z

    invoke-virtual {v0, v4}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->setProximityCharsCorrectionEnabled(Z)V

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardBuilder;->build()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v1

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    new-instance v5, Ljava/lang/ref/SoftReference;

    invoke-direct {v5, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v4, p2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->DEBUG_CACHE:Z

    if-eqz v4, :cond_2

    sget-object v5, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "keyboard cache size="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v6, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v3, :cond_4

    const-string v4, "LOAD"

    :goto_1
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " id="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_2
    return-object v1

    :cond_3
    invoke-virtual {v3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/inputmethod/keyboard/Keyboard;

    move-object v1, v4

    goto :goto_0

    :cond_4
    const-string v4, "GCed"

    goto :goto_1

    :cond_5
    sget-boolean v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->DEBUG_CACHE:Z

    if-eqz v4, :cond_2

    sget-object v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "keyboard cache size="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->sKeyboardCache:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": HIT  id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private getKeyboardId(I)Lcom/android/inputmethod/keyboard/KeyboardId;
    .locals 16
    .param p1    # I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    const/4 v1, 0x5

    move/from16 v0, p1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x6

    move/from16 v0, p1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v13, 0x1

    :goto_0
    iget-object v1, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    invoke-static {v1}, Lcom/android/inputmethod/latin/SubtypeLocale;->isNoLanguage(Landroid/view/inputmethod/InputMethodSubtype;)Z

    move-result v14

    iget-boolean v1, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mVoiceKeyEnabled:Z

    if-eqz v1, :cond_2

    if-nez v14, :cond_2

    const/4 v10, 0x1

    :goto_1
    if-eqz v10, :cond_3

    iget-boolean v1, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mVoiceKeyOnMain:Z

    if-eq v13, v1, :cond_3

    const/4 v11, 0x1

    :goto_2
    new-instance v1, Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v3, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    iget v4, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mDeviceFormFactor:I

    iget v5, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mOrientation:I

    iget v6, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mWidth:I

    iget v7, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mMode:I

    iget-object v8, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mEditorInfo:Landroid/view/inputmethod/EditorInfo;

    iget-boolean v9, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mNoSettingsKey:Z

    iget-boolean v12, v15, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mLanguageSwitchKeyEnabled:Z

    move/from16 v2, p1

    invoke-direct/range {v1 .. v12}, Lcom/android/inputmethod/keyboard/KeyboardId;-><init>(ILandroid/view/inputmethod/InputMethodSubtype;IIIILandroid/view/inputmethod/EditorInfo;ZZZZ)V

    return-object v1

    :cond_1
    const/4 v13, 0x0

    goto :goto_0

    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    :cond_3
    const/4 v11, 0x0

    goto :goto_2
.end method


# virtual methods
.method public getKeyboard(I)Lcom/android/inputmethod/keyboard/Keyboard;
    .locals 6
    .param p1    # I

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    iget v4, v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mMode:I

    packed-switch v4, :pswitch_data_0

    move v3, p1

    :goto_0
    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    iget-object v4, v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mKeyboardLayoutSetElementIdToParamsMap:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$ElementParams;

    if-nez v1, :cond_0

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->mParams:Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;

    iget-object v4, v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$Params;->mKeyboardLayoutSetElementIdToParamsMap:Landroid/util/SparseArray;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$ElementParams;

    :cond_0
    invoke-direct {p0, v3}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboardId(I)Lcom/android/inputmethod/keyboard/KeyboardId;

    move-result-object v2

    :try_start_0
    invoke-direct {p0, v1, v2}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet;->getKeyboard(Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$ElementParams;Lcom/android/inputmethod/keyboard/KeyboardId;)Lcom/android/inputmethod/keyboard/Keyboard;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    :pswitch_0
    const/4 v4, 0x5

    if-ne p1, v4, :cond_1

    const/16 v3, 0x8

    goto :goto_0

    :cond_1
    const/4 v3, 0x7

    goto :goto_0

    :pswitch_1
    const/16 v3, 0x9

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v4, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException;

    invoke-direct {v4, v0, v2}, Lcom/android/inputmethod/keyboard/KeyboardLayoutSet$KeyboardLayoutSetException;-><init>(Ljava/lang/Throwable;Lcom/android/inputmethod/keyboard/KeyboardId;)V

    throw v4

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
