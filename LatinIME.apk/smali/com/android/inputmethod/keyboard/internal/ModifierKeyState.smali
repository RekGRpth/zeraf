.class Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;
.super Ljava/lang/Object;
.source "ModifierKeyState.java"


# static fields
.field protected static final CHORDING:I = 0x2

.field protected static final DEBUG:Z = false

.field protected static final PRESSING:I = 0x1

.field protected static final RELEASING:I

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field protected final mName:Ljava/lang/String;

.field protected mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public isChording()Z
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPressing()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReleasing()Z
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOtherKeyPressed()V
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    :cond_0
    return-void
.end method

.method public onPress()V
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    return-void
.end method

.method public onRelease()V
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->mState:I

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/internal/ModifierKeyState;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected toString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "RELEASING"

    goto :goto_0

    :pswitch_1
    const-string v0, "PRESSING"

    goto :goto_0

    :pswitch_2
    const-string v0, "CHORDING"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
