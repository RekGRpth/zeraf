.class public final Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;
.super Ljava/lang/Object;
.source "KeyVisualAttributes.java"


# static fields
.field private static final ATTR_DEFINED:I = 0x1

.field private static final ATTR_NOT_FOUND:I

.field private static final VISUAL_ATTRIBUTE_IDS:[I

.field private static final sVisualAttributeIds:Landroid/util/SparseIntArray;


# instance fields
.field public final mHintLabelColor:I

.field public final mHintLabelRatio:F

.field public final mHintLetterColor:I

.field public final mHintLetterRatio:F

.field public final mLabelRatio:F

.field public final mLabelSize:I

.field public final mLargeLabelRatio:F

.field public final mLargeLetterRatio:F

.field public final mLetterRatio:F

.field public final mLetterSize:I

.field public final mPreviewTextColor:I

.field public final mPreviewTextRatio:F

.field public final mShiftedLetterHintActivatedColor:I

.field public final mShiftedLetterHintInactivatedColor:I

.field public final mShiftedLetterHintRatio:F

.field public final mTextColor:I

.field public final mTextInactivatedColor:I

.field public final mTextShadowColor:I

.field public final mTypeface:Landroid/graphics/Typeface;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v4, 0x11

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    sput-object v4, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->VISUAL_ATTRIBUTE_IDS:[I

    new-instance v4, Landroid/util/SparseIntArray;

    invoke-direct {v4}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v4, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->sVisualAttributeIds:Landroid/util/SparseIntArray;

    sget-object v0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->VISUAL_ATTRIBUTE_IDS:[I

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget v1, v0, v2

    sget-object v4, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->sVisualAttributeIds:Landroid/util/SparseIntArray;

    const/4 v5, 0x1

    invoke-virtual {v4, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x13
        0x14
        0x15
        0x16
        0x17
        0x18
        0x1a
        0x19
        0x23
        0x1b
        0x1d
        0x1c
        0x1e
        0x1f
        0x20
        0x21
        0x22
    .end array-data
.end method

.method private constructor <init>(Landroid/content/res/TypedArray;)V
    .locals 5
    .param p1    # Landroid/content/res/TypedArray;

    const/16 v4, 0x15

    const/16 v3, 0x14

    const/16 v2, 0x13

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mTypeface:Landroid/graphics/Typeface;

    :goto_0
    invoke-static {p1, v3}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLetterRatio:F

    invoke-static {p1, v3}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionPixelSize(Landroid/content/res/TypedArray;I)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLetterSize:I

    invoke-static {p1, v4}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLabelRatio:F

    invoke-static {p1, v4}, Lcom/android/inputmethod/latin/ResourceUtils;->getDimensionPixelSize(Landroid/content/res/TypedArray;I)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLabelSize:I

    const/16 v0, 0x16

    invoke-static {p1, v0}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLargeLetterRatio:F

    const/16 v0, 0x17

    invoke-static {p1, v0}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLargeLabelRatio:F

    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mHintLetterRatio:F

    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mShiftedLetterHintRatio:F

    const/16 v0, 0x19

    invoke-static {p1, v0}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mHintLabelRatio:F

    const/16 v0, 0x23

    invoke-static {p1, v0}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;I)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mPreviewTextRatio:F

    const/16 v0, 0x1b

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mTextColor:I

    const/16 v0, 0x1d

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mTextInactivatedColor:I

    const/16 v0, 0x1c

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mTextShadowColor:I

    const/16 v0, 0x1e

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mHintLetterColor:I

    const/16 v0, 0x1f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mHintLabelColor:I

    const/16 v0, 0x20

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mShiftedLetterHintInactivatedColor:I

    const/16 v0, 0x21

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mShiftedLetterHintActivatedColor:I

    const/16 v0, 0x22

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mPreviewTextColor:I

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mTypeface:Landroid/graphics/Typeface;

    goto/16 :goto_0
.end method

.method public static newInstance(Landroid/content/res/TypedArray;)Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;
    .locals 5
    .param p0    # Landroid/content/res/TypedArray;

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v0

    sget-object v3, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->sVisualAttributeIds:Landroid/util/SparseIntArray;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    invoke-direct {v3, p0}, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;-><init>(Landroid/content/res/TypedArray;)V

    :goto_1
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method
