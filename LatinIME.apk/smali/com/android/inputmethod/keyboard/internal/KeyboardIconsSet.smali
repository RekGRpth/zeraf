.class public final Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;
.super Ljava/lang/Object;
.source "KeyboardIconsSet.java"


# static fields
.field private static final ATTR_ID_TO_ICON_ID:Landroid/util/SparseIntArray;

.field private static final ATTR_UNDEFINED:I

.field private static final ICON_NAMES:[Ljava/lang/String;

.field public static final ICON_UNDEFINED:I

.field private static final NAMES_AND_ATTR_IDS:[Ljava/lang/Object;

.field private static NUM_ICONS:I

.field private static final TAG:Ljava/lang/String;

.field private static final sNameToIdsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mIcons:[Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/16 v10, 0xf

    const/16 v9, 0xe

    const/16 v8, 0xd

    const/16 v7, 0xc

    const/4 v6, 0x0

    const-class v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->TAG:Ljava/lang/String;

    new-instance v4, Landroid/util/SparseIntArray;

    invoke-direct {v4}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->ATTR_ID_TO_ICON_ID:Landroid/util/SparseIntArray;

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newHashMap()Ljava/util/HashMap;

    move-result-object v4

    sput-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->sNameToIdsMap:Ljava/util/HashMap;

    const/16 v4, 0x22

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "undefined"

    aput-object v5, v4, v6

    const/4 v5, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "shift_key"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "delete_key"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "settings_key"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "space_key"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "enter_key"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    const/16 v6, 0x10

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const-string v5, "search_key"

    aput-object v5, v4, v7

    const/16 v5, 0x11

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    const-string v5, "tab_key"

    aput-object v5, v4, v9

    const/16 v5, 0x12

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v10

    const/16 v5, 0x10

    const-string v6, "shortcut_key"

    aput-object v6, v4, v5

    const/16 v5, 0x11

    const/16 v6, 0x13

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x12

    const-string v6, "shortcut_for_label"

    aput-object v6, v4, v5

    const/16 v5, 0x13

    const/16 v6, 0x14

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x14

    const-string v6, "space_key_for_number_layout"

    aput-object v6, v4, v5

    const/16 v5, 0x15

    const/16 v6, 0x15

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x16

    const-string v6, "shift_key_shifted"

    aput-object v6, v4, v5

    const/16 v5, 0x17

    const/16 v6, 0x16

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x18

    const-string v6, "shortcut_key_disabled"

    aput-object v6, v4, v5

    const/16 v5, 0x19

    const/16 v6, 0x17

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x1a

    const-string v6, "tab_key_preview"

    aput-object v6, v4, v5

    const/16 v5, 0x1b

    const/16 v6, 0x18

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x1c

    const-string v6, "language_switch_key"

    aput-object v6, v4, v5

    const/16 v5, 0x1d

    const/16 v6, 0x19

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x1e

    const-string v6, "zwnj_key"

    aput-object v6, v4, v5

    const/16 v5, 0x1f

    const/16 v6, 0x1a

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x20

    const-string v6, "zwj_key"

    aput-object v6, v4, v5

    const/16 v5, 0x21

    const/16 v6, 0x1b

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    sput-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->NAMES_AND_ATTR_IDS:[Ljava/lang/Object;

    sget-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->NAMES_AND_ATTR_IDS:[Ljava/lang/Object;

    array-length v4, v4

    div-int/lit8 v4, v4, 0x2

    sput v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->NUM_ICONS:I

    sget v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->NUM_ICONS:I

    new-array v4, v4, [Ljava/lang/String;

    sput-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->ICON_NAMES:[Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    sget-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->NAMES_AND_ATTR_IDS:[Ljava/lang/Object;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    sget-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->NAMES_AND_ATTR_IDS:[Ljava/lang/Object;

    aget-object v3, v4, v1

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->NAMES_AND_ATTR_IDS:[Ljava/lang/Object;

    add-int/lit8 v5, v1, 0x1

    aget-object v0, v4, v5

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->ATTR_ID_TO_ICON_ID:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5, v2}, Landroid/util/SparseIntArray;->put(II)V

    :cond_0
    sget-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->sNameToIdsMap:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->ICON_NAMES:[Ljava/lang/String;

    aput-object v3, v4, v2

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_1
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->NUM_ICONS:I

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->mIcons:[Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method static getIconId(Ljava/lang/String;)I
    .locals 4
    .param p0    # Ljava/lang/String;

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->sNameToIdsMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown icon name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static getIconName(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    invoke-static {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->isValidIconId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->ICON_NAMES:[Ljava/lang/String;

    aget-object v0, v0, p0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unknown<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static isValidIconId(I)Z
    .locals 1
    .param p0    # I

    if-ltz p0, :cond_0

    sget-object v0, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->ICON_NAMES:[Ljava/lang/String;

    array-length v0, v0

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setDefaultBounds(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p0    # Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p0, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getIconDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1    # I

    invoke-static {p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->isValidIconId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->mIcons:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, p1

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown icon id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->getIconName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public loadIcons(Landroid/content/res/TypedArray;)V
    .locals 9
    .param p1    # Landroid/content/res/TypedArray;

    sget-object v6, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->ATTR_ID_TO_ICON_ID:Landroid/util/SparseIntArray;

    invoke-virtual {v6}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    sget-object v6, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->ATTR_ID_TO_ICON_ID:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v4}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    :try_start_0
    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->setDefaultBounds(Landroid/graphics/drawable/Drawable;)V

    sget-object v6, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->ATTR_ID_TO_ICON_ID:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->mIcons:[Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput-object v2, v6, v7
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v6, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Drawable resource for icon #"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " not found"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_0
    return-void
.end method
