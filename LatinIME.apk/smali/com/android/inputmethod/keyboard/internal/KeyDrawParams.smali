.class public final Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;
.super Ljava/lang/Object;
.source "KeyDrawParams.java"


# instance fields
.field public mAnimAlpha:I

.field public mHintLabelColor:I

.field public mHintLabelSize:I

.field public mHintLetterColor:I

.field public mHintLetterSize:I

.field public mLabelSize:I

.field public mLargeLabelSize:I

.field public mLargeLetterSize:I

.field public mLetterSize:I

.field public mPreviewTextColor:I

.field public mPreviewTextSize:I

.field public mShiftedLetterHintActivatedColor:I

.field public mShiftedLetterHintInactivatedColor:I

.field public mShiftedLetterHintSize:I

.field public mTextColor:I

.field public mTextInactivatedColor:I

.field public mTextShadowColor:I

.field public mTypeface:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V
    .locals 1
    .param p1    # Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTypeface:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTypeface:Landroid/graphics/Typeface;

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLetterSize:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLetterSize:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLabelSize:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLabelSize:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLargeLetterSize:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLargeLetterSize:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLargeLabelSize:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLargeLabelSize:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterSize:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterSize:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintSize:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintSize:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelSize:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelSize:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mPreviewTextSize:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mPreviewTextSize:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextColor:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextColor:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextInactivatedColor:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextInactivatedColor:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextShadowColor:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextShadowColor:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterColor:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterColor:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelColor:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelColor:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintInactivatedColor:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintInactivatedColor:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintActivatedColor:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintActivatedColor:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mPreviewTextColor:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mPreviewTextColor:I

    iget v0, p1, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mAnimAlpha:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mAnimAlpha:I

    return-void
.end method

.method private static final selectColor(II)I
    .locals 0
    .param p0    # I
    .param p1    # I

    if-eqz p0, :cond_0

    :goto_0
    return p0

    :cond_0
    move p0, p1

    goto :goto_0
.end method

.method private static final selectTextSize(IFI)I
    .locals 1
    .param p0    # I
    .param p1    # F
    .param p2    # I

    invoke-static {p1}, Lcom/android/inputmethod/latin/ResourceUtils;->isValidFraction(F)Z

    move-result v0

    if-eqz v0, :cond_0

    int-to-float v0, p0

    mul-float/2addr v0, p1

    float-to-int p2, v0

    :cond_0
    return p2
.end method

.method private static final selectTextSizeFromDimensionOrRatio(IIFI)I
    .locals 1
    .param p0    # I
    .param p1    # I
    .param p2    # F
    .param p3    # I

    invoke-static {p1}, Lcom/android/inputmethod/latin/ResourceUtils;->isValidDimensionPixelSize(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return p1

    :cond_0
    invoke-static {p2}, Lcom/android/inputmethod/latin/ResourceUtils;->isValidFraction(F)Z

    move-result v0

    if-eqz v0, :cond_1

    int-to-float v0, p0

    mul-float/2addr v0, p2

    float-to-int p1, v0

    goto :goto_0

    :cond_1
    move p1, p3

    goto :goto_0
.end method


# virtual methods
.method public mayCloneAndUpdateParams(ILcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;)Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    if-nez p2, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    invoke-direct {v0, p0}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->updateParams(ILcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public updateParams(ILcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mTypeface:Landroid/graphics/Typeface;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mTypeface:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTypeface:Landroid/graphics/Typeface;

    :cond_1
    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLetterSize:I

    iget v1, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLetterRatio:F

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLetterSize:I

    invoke-static {p1, v0, v1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectTextSizeFromDimensionOrRatio(IIFI)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLetterSize:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLabelSize:I

    iget v1, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLabelRatio:F

    iget v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLabelSize:I

    invoke-static {p1, v0, v1, v2}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectTextSizeFromDimensionOrRatio(IIFI)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLabelSize:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLargeLabelRatio:F

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLargeLabelSize:I

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectTextSize(IFI)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLargeLabelSize:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mLargeLetterRatio:F

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLargeLetterSize:I

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectTextSize(IFI)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mLargeLetterSize:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mHintLetterRatio:F

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterSize:I

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectTextSize(IFI)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterSize:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mShiftedLetterHintRatio:F

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintSize:I

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectTextSize(IFI)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintSize:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mHintLabelRatio:F

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelSize:I

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectTextSize(IFI)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelSize:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mPreviewTextRatio:F

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mPreviewTextSize:I

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectTextSize(IFI)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mPreviewTextSize:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mTextColor:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextColor:I

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextColor:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mTextInactivatedColor:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextInactivatedColor:I

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextInactivatedColor:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mTextShadowColor:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextShadowColor:I

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mTextShadowColor:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mHintLetterColor:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterColor:I

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLetterColor:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mHintLabelColor:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelColor:I

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mHintLabelColor:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mShiftedLetterHintInactivatedColor:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintInactivatedColor:I

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintInactivatedColor:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mShiftedLetterHintActivatedColor:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintActivatedColor:I

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mShiftedLetterHintActivatedColor:I

    iget v0, p2, Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;->mPreviewTextColor:I

    iget v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mPreviewTextColor:I

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->selectColor(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mPreviewTextColor:I

    goto/16 :goto_0
.end method
