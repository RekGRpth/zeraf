.class public final Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;
.super Ljava/lang/Object;
.source "TouchPositionCorrection.java"


# static fields
.field private static final TOUCH_POSITION_CORRECTION_RECORD_SIZE:I = 0x3


# instance fields
.field private mEnabled:Z

.field private mRadii:[F

.field private mXs:[F

.field private mYs:[F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRadius(I)F
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mRadii:[F

    aget v0, v0, p1

    return v0
.end method

.method public getRows()I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mRadii:[F

    array-length v0, v0

    return v0
.end method

.method public getX(I)F
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public getY(I)F
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mYs:[F

    aget v0, v0, p1

    return v0
.end method

.method public isValid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mEnabled:Z

    return v0
.end method

.method public load([Ljava/lang/String;)V
    .locals 11
    .param p1    # [Ljava/lang/String;

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v10, 0x0

    array-length v0, p1

    rem-int/lit8 v9, v0, 0x3

    if-eqz v9, :cond_0

    sget-boolean v7, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v7, :cond_4

    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "the size of touch position correction data is invalid"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    div-int/lit8 v4, v0, 0x3

    new-array v9, v4, [F

    iput-object v9, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mXs:[F

    new-array v9, v4, [F

    iput-object v9, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mYs:[F

    new-array v9, v4, [F

    iput-object v9, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mRadii:[F

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_3

    :try_start_0
    rem-int/lit8 v5, v2, 0x3

    div-int/lit8 v3, v2, 0x3

    aget-object v9, p1, v2

    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    if-nez v5, :cond_1

    iget-object v9, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mXs:[F

    aput v6, v9, v3

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-ne v5, v7, :cond_2

    iget-object v9, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mYs:[F

    aput v6, v9, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    sget-boolean v7, Lcom/android/inputmethod/latin/LatinImeLogger;->sDBG:Z

    if-eqz v7, :cond_6

    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "the number format for touch position correction data is invalid"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_2
    :try_start_1
    iget-object v9, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mRadii:[F

    aput v6, v9, v3

    goto :goto_1

    :cond_3
    if-lez v0, :cond_5

    :goto_2
    iput-boolean v7, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mEnabled:Z
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_4
    :goto_3
    return-void

    :cond_5
    move v7, v8

    goto :goto_2

    :cond_6
    iput-boolean v8, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mEnabled:Z

    iput-object v10, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mXs:[F

    iput-object v10, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mYs:[F

    iput-object v10, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mRadii:[F

    goto :goto_3
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;->mEnabled:Z

    return-void
.end method
