.class public final Lcom/android/inputmethod/keyboard/MainKeyboardView;
.super Lcom/android/inputmethod/keyboard/KeyboardView;
.source "MainKeyboardView.java"

# interfaces
.implements Lcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;
.implements Lcom/android/inputmethod/keyboard/internal/SuddenJumpingTouchEventHandler$ProcessMotionEvent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;
    }
.end annotation


# static fields
.field private static final ENABLE_USABILITY_STUDY_LOG:Z

.field private static final MINIMUM_XSCALE_OF_LANGUAGE_NAME:F = 0.8f

.field private static final SPACE_LED_LENGTH_PERCENT:I = 0x50

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAltCodeKeyWhileTypingAnimAlpha:I

.field private mAltCodeKeyWhileTypingFadeinAnimator:Landroid/animation/ObjectAnimator;

.field private mAltCodeKeyWhileTypingFadeoutAnimator:Landroid/animation/ObjectAnimator;

.field private final mAutoCorrectionSpacebarLedEnabled:Z

.field private final mAutoCorrectionSpacebarLedIcon:Landroid/graphics/drawable/Drawable;

.field private mAutoCorrectionSpacebarLedOn:Z

.field private final mConfigShowMoreKeysKeyboardAtTouchedPoint:Z

.field private mHasDistinctMultitouch:Z

.field private mHasMultipleEnabledIMEsOrSubtypes:Z

.field protected mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

.field private final mKeyTimerHandler:Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;

.field private mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

.field private mLanguageOnSpacebarAnimAlpha:I

.field private mLanguageOnSpacebarFadeoutAnimator:Landroid/animation/ObjectAnimator;

.field private final mLanguageOnSpacebarFinalAlpha:I

.field private mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

.field private final mMoreKeysPanelCache:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/android/inputmethod/keyboard/Key;",
            "Lcom/android/inputmethod/keyboard/MoreKeysPanel;",
            ">;"
        }
    .end annotation
.end field

.field private mMoreKeysPanelPointerTrackerId:I

.field private mMoreKeysWindow:Landroid/widget/PopupWindow;

.field private mNeedsToDisplayLanguage:Z

.field private mOldKey:Lcom/android/inputmethod/keyboard/Key;

.field private mOldPointerCount:I

.field private mSpaceIcon:Landroid/graphics/drawable/Drawable;

.field private mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

.field private final mSpacebarTextColor:I

.field private final mSpacebarTextRatio:F

.field private final mSpacebarTextShadowColor:I

.field private mSpacebarTextSize:F

.field private final mTouchScreenRegulator:Lcom/android/inputmethod/keyboard/internal/SuddenJumpingTouchEventHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->TAG:Ljava/lang/String;

    sget-boolean v0, Lcom/android/inputmethod/latin/LatinImeLogger;->sUsabilityStudy:Z

    sput-boolean v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->ENABLE_USABILITY_STUDY_LOG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v0, 0x7f010002

    invoke-direct {p0, p1, p2, v0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/KeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v8, 0xff

    iput v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    const/16 v8, 0xff

    iput v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAltCodeKeyWhileTypingAnimAlpha:I

    new-instance v8, Ljava/util/WeakHashMap;

    invoke-direct {v8}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanelCache:Ljava/util/WeakHashMap;

    const/4 v8, 0x1

    iput v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mOldPointerCount:I

    new-instance v8, Lcom/android/inputmethod/keyboard/internal/SuddenJumpingTouchEventHandler;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9, p0}, Lcom/android/inputmethod/keyboard/internal/SuddenJumpingTouchEventHandler;-><init>(Landroid/content/Context;Lcom/android/inputmethod/keyboard/internal/SuddenJumpingTouchEventHandler$ProcessMotionEvent;)V

    iput-object v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mTouchScreenRegulator:Lcom/android/inputmethod/keyboard/internal/SuddenJumpingTouchEventHandler;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const-string v9, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v8, v9}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mHasDistinctMultitouch:Z

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e0012

    const-string v9, "false"

    invoke-static {v7, v8, v9}, Lcom/android/inputmethod/latin/ResourceUtils;->getDeviceOverrideValue(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    iget-boolean v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mHasDistinctMultitouch:Z

    invoke-static {v8, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->init(ZZ)V

    sget-object v8, Lcom/android/inputmethod/latin/R$styleable;->MainKeyboardView:[I

    const v9, 0x7f0f0005

    invoke-virtual {p1, p2, v8, p3, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAutoCorrectionSpacebarLedEnabled:Z

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAutoCorrectionSpacebarLedIcon:Landroid/graphics/drawable/Drawable;

    const/4 v8, 0x2

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/high16 v11, 0x3f800000

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v8

    iput v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpacebarTextRatio:F

    const/4 v8, 0x3

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v8

    iput v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpacebarTextColor:I

    const/4 v8, 0x4

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v8

    iput v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpacebarTextShadowColor:I

    const/4 v8, 0x5

    const/16 v9, 0xff

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    iput v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mLanguageOnSpacebarFinalAlpha:I

    const/4 v8, 0x6

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    const/4 v8, 0x7

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    const/16 v8, 0x8

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    const/16 v8, 0x9

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    const/16 v8, 0xa

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    new-instance v8, Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-direct {v8, v3, v4}, Lcom/android/inputmethod/keyboard/KeyDetector;-><init>(FF)V

    iput-object v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    new-instance v8, Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;

    invoke-direct {v8, p0, v0}, Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;-><init>(Lcom/android/inputmethod/keyboard/MainKeyboardView;Landroid/content/res/TypedArray;)V

    iput-object v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;

    const/16 v8, 0x13

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mConfigShowMoreKeysKeyboardAtTouchedPoint:Z

    invoke-static {v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->setParameters(Landroid/content/res/TypedArray;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-direct {p0, v5, p0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->loadObjectAnimator(ILjava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v8

    iput-object v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mLanguageOnSpacebarFadeoutAnimator:Landroid/animation/ObjectAnimator;

    invoke-direct {p0, v2, p0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->loadObjectAnimator(ILjava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v8

    iput-object v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAltCodeKeyWhileTypingFadeoutAnimator:Landroid/animation/ObjectAnimator;

    invoke-direct {p0, v1, p0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->loadObjectAnimator(ILjava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v8

    iput-object v8, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAltCodeKeyWhileTypingFadeinAnimator:Landroid/animation/ObjectAnimator;

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethod/keyboard/MainKeyboardView;Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z
    .locals 1
    .param p0    # Lcom/android/inputmethod/keyboard/MainKeyboardView;
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Lcom/android/inputmethod/keyboard/PointerTracker;

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->openMoreKeysKeyboardIfRequired(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/android/inputmethod/keyboard/MainKeyboardView;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0    # Lcom/android/inputmethod/keyboard/MainKeyboardView;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAltCodeKeyWhileTypingFadeoutAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/inputmethod/keyboard/MainKeyboardView;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0    # Lcom/android/inputmethod/keyboard/MainKeyboardView;

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAltCodeKeyWhileTypingFadeinAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private drawSpacebar(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 16
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;

    move-object/from16 v0, p1

    iget v14, v0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    move-object/from16 v0, p1

    iget v10, v0, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mNeedsToDisplayLanguage:Z

    if-eqz v2, :cond_0

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpacebarTextSize:F

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v2

    iget-object v2, v2, Lcom/android/inputmethod/keyboard/Keyboard;->mId:Lcom/android/inputmethod/keyboard/KeyboardId;

    iget-object v12, v2, Lcom/android/inputmethod/keyboard/KeyboardId;->mSubtype:Landroid/view/inputmethod/InputMethodSubtype;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v12, v14}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->layoutLanguageOnSpacebar(Landroid/graphics/Paint;Landroid/view/inputmethod/InputMethodSubtype;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->descent()F

    move-result v9

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    neg-float v2, v2

    add-float v13, v2, v9

    div-int/lit8 v2, v10, 0x2

    int-to-float v2, v2

    const/high16 v3, 0x40000000

    div-float v3, v13, v3

    add-float v8, v2, v3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpacebarTextShadowColor:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    div-int/lit8 v2, v14, 0x2

    int-to-float v2, v2

    sub-float v3, v8, v9

    const/high16 v15, 0x3f800000

    sub-float/2addr v3, v15

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v11, v2, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpacebarTextColor:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    div-int/lit8 v2, v14, 0x2

    int-to-float v2, v2

    sub-float v3, v8, v9

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v11, v2, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAutoCorrectionSpacebarLedOn:Z

    if-eqz v2, :cond_2

    mul-int/lit8 v2, v14, 0x50

    div-int/lit8 v6, v2, 0x64

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAutoCorrectionSpacebarLedIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    sub-int v2, v14, v6

    div-int/lit8 v4, v2, 0x2

    sub-int v5, v10, v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAutoCorrectionSpacebarLedIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v2, p2

    invoke-static/range {v2 .. v7}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    sub-int v2, v14, v6

    div-int/lit8 v4, v2, 0x2

    sub-int v5, v10, v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v2, p2

    invoke-static/range {v2 .. v7}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->drawIcon(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    goto :goto_0
.end method

.method private fitsTextIntoWidth(ILjava/lang/String;Landroid/graphics/Paint;)Z
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Paint;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000

    invoke-virtual {p3, v4}, Landroid/graphics/Paint;->setTextScaleX(F)V

    invoke-virtual {p0, p2, p3}, Lcom/android/inputmethod/keyboard/KeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v1

    int-to-float v4, p1

    cmpg-float v4, v1, v4

    if-gez v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    int-to-float v4, p1

    div-float v0, v4, v1

    const v4, 0x3f4ccccd

    cmpg-float v4, v0, v4

    if-gez v4, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setTextScaleX(F)V

    invoke-virtual {p0, p2, p3}, Lcom/android/inputmethod/keyboard/KeyboardView;->getLabelWidth(Ljava/lang/String;Landroid/graphics/Paint;)F

    move-result v4

    int-to-float v5, p1

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method static getFullDisplayName(Landroid/view/inputmethod/InputMethodSubtype;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/view/inputmethod/InputMethodSubtype;
    .param p1    # Landroid/content/res/Resources;

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->isNoLanguage(Landroid/view/inputmethod/InputMethodSubtype;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->getKeyboardLayoutSetDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/android/inputmethod/latin/SubtypeLocale;->getSubtypeDisplayName(Landroid/view/inputmethod/InputMethodSubtype;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static getMiddleDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/view/inputmethod/InputMethodSubtype;

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->isNoLanguage(Landroid/view/inputmethod/InputMethodSubtype;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->getKeyboardLayoutSetDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->getSubtypeLocale(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0, v0}, Ljava/util/Locale;->getDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/inputmethod/latin/StringUtils;->toTitleCase(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method static getShortDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/view/inputmethod/InputMethodSubtype;

    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->isNoLanguage(Landroid/view/inputmethod/InputMethodSubtype;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Lcom/android/inputmethod/latin/SubtypeLocale;->getSubtypeLocale(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/inputmethod/latin/StringUtils;->toTitleCase(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private invokeCodeInput(I)V
    .locals 2
    .param p1    # I

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v0, p1, v1, v1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onCodeInput(III)V

    return-void
.end method

.method private invokeCustomRequest(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v0, p1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onCustomRequest(I)Z

    move-result v0

    return v0
.end method

.method private invokeReleaseKey(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onReleaseKey(IZ)V

    return-void
.end method

.method private layoutLanguageOnSpacebar(Landroid/graphics/Paint;Landroid/view/inputmethod/InputMethodSubtype;I)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/graphics/Paint;
    .param p2    # Landroid/view/inputmethod/InputMethodSubtype;
    .param p3    # I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->getFullDisplayName(Landroid/view/inputmethod/InputMethodSubtype;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p3, v0, p1}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->fitsTextIntoWidth(ILjava/lang/String;Landroid/graphics/Paint;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p2}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->getMiddleDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p3, v0, p1}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->fitsTextIntoWidth(ILjava/lang/String;Landroid/graphics/Paint;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v0

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->getShortDisplayName(Landroid/view/inputmethod/InputMethodSubtype;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p3, v0, p1}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->fitsTextIntoWidth(ILjava/lang/String;Landroid/graphics/Paint;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v0

    goto :goto_0

    :cond_2
    const-string v1, ""

    goto :goto_0
.end method

.method private loadObjectAnimator(ILjava/lang/Object;)Landroid/animation/ObjectAnimator;
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private openMoreKeysKeyboardIfRequired(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Lcom/android/inputmethod/keyboard/PointerTracker;

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mMoreKeysLayout:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->onLongPress(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z

    move-result v0

    goto :goto_0
.end method

.method private openMoreKeysPanel(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z
    .locals 11
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Lcom/android/inputmethod/keyboard/PointerTracker;

    const/4 v7, 0x0

    const/4 v10, 0x1

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanelCache:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->onCreateMoreKeysPanel(Lcom/android/inputmethod/keyboard/Key;)Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return v7

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanelCache:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    if-nez v1, :cond_2

    new-instance v1, Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    const v2, 0x7f0f0029

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    :cond_2
    iput-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    iget v1, p2, Lcom/android/inputmethod/keyboard/PointerTracker;->mPointerId:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanelPointerTrackerId:I

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->isKeyPreviewPopupEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->noKeyPreview()Z

    move-result v1

    if-nez v1, :cond_3

    move v7, v10

    :cond_3
    iget-boolean v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mConfigShowMoreKeysKeyboardAtTouchedPoint:Z

    if-eqz v1, :cond_4

    if-nez v7, :cond_4

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getLastX()I

    move-result v3

    :goto_1
    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mY:I

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mKeyPreviewDrawParams:Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;

    iget v2, v2, Lcom/android/inputmethod/keyboard/internal/KeyPreviewDrawParams;->mPreviewVisibleOffset:I

    add-int v4, v1, v2

    iget-object v5, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    move-object v1, p0

    move-object v2, p0

    invoke-interface/range {v0 .. v6}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->showMoreKeysPanel(Landroid/view/View;Lcom/android/inputmethod/keyboard/MoreKeysPanel$Controller;IILandroid/widget/PopupWindow;Lcom/android/inputmethod/keyboard/KeyboardActionListener;)V

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getLastX()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateX(I)I

    move-result v8

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getLastY()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateY(I)I

    move-result v9

    invoke-virtual {p2, v8, v9, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->onShowMoreKeysPanel(IILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    invoke-virtual {p0, v10}, Lcom/android/inputmethod/keyboard/KeyboardView;->dimEntireKeyboard(Z)V

    move v7, v10

    goto :goto_0

    :cond_4
    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mX:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    div-int/lit8 v2, v2, 0x2

    add-int v3, v1, v2

    goto :goto_1
.end method

.method private updateAltCodeKeyWhileTyping()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, v3, Lcom/android/inputmethod/keyboard/Keyboard;->mAltCodeKeysWhileTyping:[Lcom/android/inputmethod/keyboard/Key;

    array-length v4, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v2, v0, v1

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public cancelAllMessages()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;

    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;->cancelAllMessages()V

    invoke-super {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->cancelAllMessages()V

    return-void
.end method

.method public closing()V
    .locals 1

    invoke-super {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->closing()V

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->dismissMoreKeysPanel()Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanelCache:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->clear()V

    return-void
.end method

.method public dismissMoreKeysPanel()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanelPointerTrackerId:I

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->dimEntireKeyboard(Z)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->getInstance()Lcom/android/inputmethod/accessibility/AccessibilityUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->isTouchExplorationEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1, p0}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v0

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->dispatchHoverEvent(Landroid/view/MotionEvent;Lcom/android/inputmethod/keyboard/PointerTracker;)Z

    move-result v1

    :cond_0
    return v1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->getInstance()Lcom/android/inputmethod/accessibility/AccessibilityUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/inputmethod/accessibility/AccessibilityUtils;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getAltCodeKeyWhileTypingAnimAlpha()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAltCodeKeyWhileTypingAnimAlpha:I

    return v0
.end method

.method public getDrawingProxy()Lcom/android/inputmethod/keyboard/PointerTracker$DrawingProxy;
    .locals 0

    return-object p0
.end method

.method public getKeyDetector()Lcom/android/inputmethod/keyboard/KeyDetector;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    return-object v0
.end method

.method public getKeyboardActionListener()Lcom/android/inputmethod/keyboard/KeyboardActionListener;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    return-object v0
.end method

.method public getLanguageOnSpacebarAnimAlpha()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    return v0
.end method

.method public getPointerCount()I
    .locals 1

    iget v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mOldPointerCount:I

    return v0
.end method

.method public getTimerProxy()Lcom/android/inputmethod/keyboard/PointerTracker$TimerProxy;
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;

    return-object v0
.end method

.method public hasDistinctMultitouch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mHasDistinctMultitouch:Z

    return v0
.end method

.method public isInSlidingKeyInput()Z
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/android/inputmethod/keyboard/PointerTracker;->isAnyInSlidingKeyInput()Z

    move-result v0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    return-void
.end method

.method protected onCreateMoreKeysPanel(Lcom/android/inputmethod/keyboard/Key;)Lcom/android/inputmethod/keyboard/MoreKeysPanel;
    .locals 6
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    const/4 v2, 0x0

    const/4 v5, -0x2

    iget-object v3, p1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    if-nez v3, :cond_0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget v4, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mMoreKeysLayout:I

    invoke-virtual {v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    :cond_1
    const v3, 0x7f080042

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;

    new-instance v3, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;

    invoke-direct {v3, v0, p1, p0}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;-><init>(Landroid/view/View;Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/KeyboardView;)V

    invoke-virtual {v3}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboard$Builder;->build()Lcom/android/inputmethod/keyboard/MoreKeysKeyboard;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/android/inputmethod/keyboard/MoreKeysKeyboardView;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    invoke-virtual {v0, v5, v5}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onDrawKeyTopVisuals(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # Landroid/graphics/Paint;
    .param p4    # Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->altCodeWhileTyping()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAltCodeKeyWhileTypingAnimAlpha:I

    iput v0, p4, Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;->mAnimAlpha:I

    :cond_0
    iget v0, p1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    invoke-direct {p0, p1, p2, p3}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->drawSpacebar(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->isLongPressEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mHasMultipleEnabledIMEsOrSubtypes:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawKeyPopupHint(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/16 v1, -0xa

    if-ne v0, v1, :cond_3

    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKeyTopVisuals(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/KeyboardView;->drawKeyPopupHint(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/inputmethod/keyboard/KeyboardView;->onDrawKeyTopVisuals(Lcom/android/inputmethod/keyboard/Key;Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/android/inputmethod/keyboard/internal/KeyDrawParams;)V

    goto :goto_0
.end method

.method protected onLongPress(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z
    .locals 5
    .param p1    # Lcom/android/inputmethod/keyboard/Key;
    .param p2    # Lcom/android/inputmethod/keyboard/PointerTracker;

    const/4 v2, 0x1

    iget v1, p1, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    invoke-virtual {p1}, Lcom/android/inputmethod/keyboard/Key;->hasEmbeddedMoreKey()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/android/inputmethod/keyboard/Key;->mMoreKeys:[Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget v0, v3, Lcom/android/inputmethod/keyboard/internal/MoreKeySpec;->mCode:I

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onLongPressed()V

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->invokeCodeInput(I)V

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->invokeReleaseKey(I)V

    invoke-static {}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->getInstance()Lcom/android/inputmethod/keyboard/KeyboardSwitcher;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/android/inputmethod/keyboard/KeyboardSwitcher;->hapticAndAudioFeedback(I)V

    :goto_0
    return v2

    :cond_0
    const/16 v3, 0x20

    if-eq v1, v3, :cond_1

    const/16 v3, -0xa

    if-ne v1, v3, :cond_2

    :cond_1
    invoke-direct {p0, v2}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->invokeCustomRequest(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p2}, Lcom/android/inputmethod/keyboard/PointerTracker;->onLongPressed()V

    invoke-direct {p0, v1}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->invokeReleaseKey(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->openMoreKeysPanel(Lcom/android/inputmethod/keyboard/Key;Lcom/android/inputmethod/keyboard/PointerTracker;)Z

    move-result v2

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mTouchScreenRegulator:Lcom/android/inputmethod/keyboard/internal/SuddenJumpingTouchEventHandler;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/keyboard/internal/SuddenJumpingTouchEventHandler;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public processMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 37
    .param p1    # Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mHasDistinctMultitouch:Z

    if-nez v7, :cond_0

    const/16 v29, 0x1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v31

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mOldPointerCount:I

    move/from16 v30, v0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mOldPointerCount:I

    if-eqz v29, :cond_1

    const/4 v7, 0x1

    move/from16 v0, v31

    if-le v0, v7, :cond_1

    const/4 v7, 0x1

    move/from16 v0, v30

    if-le v0, v7, :cond_1

    const/4 v7, 0x1

    :goto_1
    return v7

    :cond_0
    const/16 v29, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v25

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v24

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    if-eqz v7, :cond_5

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanelPointerTrackerId:I

    move/from16 v0, v24

    if-ne v0, v7, :cond_5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    float-to-int v9, v9

    invoke-interface {v7, v9}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateX(I)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    float-to-int v9, v9

    invoke-interface {v7, v9}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateY(I)I

    move-result v4

    :goto_2
    sget-boolean v7, Lcom/android/inputmethod/keyboard/MainKeyboardView;->ENABLE_USABILITY_STUDY_LOG:Z

    if-eqz v7, :cond_2

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[Action"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "]"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    :goto_3
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getSize(I)F

    move-result v36

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v35

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ","

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ","

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ","

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ","

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v36

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ","

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v35

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->write(Ljava/lang/String;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;->isInKeyRepeat()Z

    move-result v7

    if-eqz v7, :cond_3

    move/from16 v0, v24

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v2

    const/4 v7, 0x1

    move/from16 v0, v31

    if-le v0, v7, :cond_3

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->isModifier()Z

    move-result v7

    if-nez v7, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;

    invoke-virtual {v7}, Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;->cancelKeyRepeatTimer()V

    :cond_3
    if-eqz v29, :cond_9

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-static {v7, v0}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v2

    const/4 v7, 0x1

    move/from16 v0, v31

    if-ne v0, v7, :cond_6

    const/4 v7, 0x2

    move/from16 v0, v30

    if-ne v0, v7, :cond_6

    invoke-virtual {v2, v3, v4}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKeyOn(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mOldKey:Lcom/android/inputmethod/keyboard/Key;

    move-object/from16 v0, v28

    if-eq v7, v0, :cond_4

    move-object/from16 v7, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/inputmethod/keyboard/PointerTracker;->onDownEvent(IIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    const/4 v7, 0x1

    if-ne v8, v7, :cond_4

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->onUpEvent(IIJ)V

    :cond_4
    :goto_4
    const/4 v7, 0x1

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    float-to-int v3, v7

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    float-to-int v4, v7

    goto/16 :goto_2

    :pswitch_1
    const-string v22, "[Up]"

    goto/16 :goto_3

    :pswitch_2
    const-string v22, "[Down]"

    goto/16 :goto_3

    :pswitch_3
    const-string v22, "[PointerUp]"

    goto/16 :goto_3

    :pswitch_4
    const-string v22, "[PointerDown]"

    goto/16 :goto_3

    :pswitch_5
    const-string v22, ""

    goto/16 :goto_3

    :cond_6
    const/4 v7, 0x2

    move/from16 v0, v31

    if-ne v0, v7, :cond_7

    const/4 v7, 0x1

    move/from16 v0, v30

    if-ne v0, v7, :cond_7

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getLastX()I

    move-result v26

    invoke-virtual {v2}, Lcom/android/inputmethod/keyboard/PointerTracker;->getLastY()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v2, v0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getKeyOn(II)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v7

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mOldKey:Lcom/android/inputmethod/keyboard/Key;

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v2, v0, v1, v5, v6}, Lcom/android/inputmethod/keyboard/PointerTracker;->onUpEvent(IIJ)V

    goto :goto_4

    :cond_7
    const/4 v7, 0x1

    move/from16 v0, v31

    if-ne v0, v7, :cond_8

    const/4 v7, 0x1

    move/from16 v0, v30

    if-ne v0, v7, :cond_8

    move-object v7, v2

    move v9, v3

    move v10, v4

    move-wide v11, v5

    move-object/from16 v13, p0

    invoke-virtual/range {v7 .. v13}, Lcom/android/inputmethod/keyboard/PointerTracker;->processMotionEvent(IIIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    goto :goto_4

    :cond_8
    sget-object v7, Lcom/android/inputmethod/keyboard/MainKeyboardView;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unknown touch panel behavior: pointer count is "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v31

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, " (old "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v30

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ")"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_9
    const/4 v7, 0x2

    if-ne v8, v7, :cond_c

    const/16 v23, 0x0

    :goto_5
    move/from16 v0, v23

    move/from16 v1, v31

    if-ge v0, v1, :cond_d

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v32

    move/from16 v0, v32

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    if-eqz v7, :cond_b

    iget v7, v2, Lcom/android/inputmethod/keyboard/PointerTracker;->mPointerId:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanelPointerTrackerId:I

    if-ne v7, v9, :cond_b

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    float-to-int v9, v9

    invoke-interface {v7, v9}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateX(I)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanel:Lcom/android/inputmethod/keyboard/MoreKeysPanel;

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    float-to-int v9, v9

    invoke-interface {v7, v9}, Lcom/android/inputmethod/keyboard/MoreKeysPanel;->translateY(I)I

    move-result v11

    const/4 v14, 0x0

    :goto_6
    move-object v9, v2

    move-wide v12, v5

    invoke-virtual/range {v9 .. v14}, Lcom/android/inputmethod/keyboard/PointerTracker;->onMoveEvent(IIJLandroid/view/MotionEvent;)V

    sget-boolean v7, Lcom/android/inputmethod/keyboard/MainKeyboardView;->ENABLE_USABILITY_STUDY_LOG:Z

    if-eqz v7, :cond_a

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getSize(I)F

    move-result v34

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v33

    invoke-static {}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->getInstance()Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[Move]"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ","

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v32

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ","

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ","

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ","

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v34

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ","

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v33

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/android/inputmethod/latin/Utils$UsabilityStudyLogUtils;->write(Ljava/lang/String;)V

    :cond_a
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_5

    :cond_b
    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    float-to-int v10, v7

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    float-to-int v11, v7

    move-object/from16 v14, p1

    goto/16 :goto_6

    :cond_c
    move/from16 v0, v24

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->getPointerTracker(ILcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)Lcom/android/inputmethod/keyboard/PointerTracker;

    move-result-object v2

    move-object v15, v2

    move/from16 v16, v8

    move/from16 v17, v3

    move/from16 v18, v4

    move-wide/from16 v19, v5

    move-object/from16 v21, p0

    invoke-virtual/range {v15 .. v21}, Lcom/android/inputmethod/keyboard/PointerTracker;->processMotionEvent(IIIJLcom/android/inputmethod/keyboard/PointerTracker$KeyEventHandler;)V

    :cond_d
    const/4 v7, 0x1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public setAltCodeKeyWhileTypingAnimAlpha(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAltCodeKeyWhileTypingAnimAlpha:I

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->updateAltCodeKeyWhileTyping()V

    return-void
.end method

.method public setDistinctMultitouch(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mHasDistinctMultitouch:Z

    return-void
.end method

.method public setGestureHandlingEnabledByUser(Z)V
    .locals 0
    .param p1    # Z

    invoke-static {p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->setGestureHandlingEnabledByUser(Z)V

    return-void
.end method

.method public setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V
    .locals 5
    .param p1    # Lcom/android/inputmethod/keyboard/Keyboard;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyTimerHandler:Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;

    invoke-virtual {v1}, Lcom/android/inputmethod/keyboard/MainKeyboardView$KeyTimerHandler;->cancelLongPressTimer()V

    invoke-super {p0, p1}, Lcom/android/inputmethod/keyboard/KeyboardView;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    iget v4, p0, Lcom/android/inputmethod/keyboard/KeyboardView;->mVerticalCorrection:F

    add-float/2addr v3, v4

    invoke-virtual {v1, p1, v2, v3}, Lcom/android/inputmethod/keyboard/KeyDetector;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;FF)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyDetector:Lcom/android/inputmethod/keyboard/KeyDetector;

    invoke-static {v1}, Lcom/android/inputmethod/keyboard/PointerTracker;->setKeyDetector(Lcom/android/inputmethod/keyboard/KeyDetector;)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mTouchScreenRegulator:Lcom/android/inputmethod/keyboard/internal/SuddenJumpingTouchEventHandler;

    invoke-virtual {v1, p1}, Lcom/android/inputmethod/keyboard/internal/SuddenJumpingTouchEventHandler;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mMoreKeysPanelCache:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->clear()V

    const/16 v1, 0x20

    invoke-virtual {p1, v1}, Lcom/android/inputmethod/keyboard/Keyboard;->getKey(I)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v1

    iput-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    iget-object v2, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    const/16 v3, 0xff

    invoke-virtual {v1, v2, v3}, Lcom/android/inputmethod/keyboard/Key;->getIcon(Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceIcon:Landroid/graphics/drawable/Drawable;

    iget v1, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mMostCommonKeyHeight:I

    iget v2, p1, Lcom/android/inputmethod/keyboard/Keyboard;->mVerticalGap:I

    sub-int v0, v1, v2

    int-to-float v1, v0

    iget v2, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpacebarTextRatio:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpacebarTextSize:F

    invoke-static {}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->getInstance()Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/inputmethod/accessibility/AccessibleKeyboardViewProxy;->setKeyboard(Lcom/android/inputmethod/keyboard/Keyboard;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setKeyboardActionListener(Lcom/android/inputmethod/keyboard/KeyboardActionListener;)V
    .locals 0
    .param p1    # Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mKeyboardActionListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-static {p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->setKeyboardActionListener(Lcom/android/inputmethod/keyboard/KeyboardActionListener;)V

    return-void
.end method

.method public setLanguageOnSpacebarAnimAlpha(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    return-void
.end method

.method public setMainDictionaryAvailability(Z)V
    .locals 0
    .param p1    # Z

    invoke-static {p1}, Lcom/android/inputmethod/keyboard/PointerTracker;->setMainDictionaryAvailability(Z)V

    return-void
.end method

.method public startDisplayLanguageOnSpacebar(ZZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z

    iput-boolean p2, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mNeedsToDisplayLanguage:Z

    iput-boolean p3, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mHasMultipleEnabledIMEsOrSubtypes:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mLanguageOnSpacebarFadeoutAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mNeedsToDisplayLanguage:Z

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    return-void

    :cond_1
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    const/16 v1, 0xff

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/keyboard/MainKeyboardView;->setLanguageOnSpacebarAnimAlpha(I)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_2
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mLanguageOnSpacebarFinalAlpha:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mLanguageOnSpacebarAnimAlpha:I

    goto :goto_0
.end method

.method public updateAutoCorrectionState(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAutoCorrectionSpacebarLedEnabled:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mAutoCorrectionSpacebarLedOn:Z

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/MainKeyboardView;->mSpaceKey:Lcom/android/inputmethod/keyboard/Key;

    invoke-virtual {p0, v0}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    goto :goto_0
.end method

.method public updateShortcutKey(Z)V
    .locals 3
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/inputmethod/keyboard/KeyboardView;->getKeyboard()Lcom/android/inputmethod/keyboard/Keyboard;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, -0x6

    invoke-virtual {v0, v2}, Lcom/android/inputmethod/keyboard/Keyboard;->getKey(I)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lcom/android/inputmethod/keyboard/Key;->setEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/android/inputmethod/keyboard/KeyboardView;->invalidateKey(Lcom/android/inputmethod/keyboard/Key;)V

    goto :goto_0
.end method
