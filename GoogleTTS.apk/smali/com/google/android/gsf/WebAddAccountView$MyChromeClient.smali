.class Lcom/google/android/gsf/WebAddAccountView$MyChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "WebAddAccountView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/WebAddAccountView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyChromeClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/WebAddAccountView;


# virtual methods
.method public onCreateWindow(Landroid/webkit/WebView;ZZLandroid/os/Message;)Z
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Landroid/os/Message;

    const-string v0, "WebAddAccountView"

    const-string v1, "onCreateWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gsf/WebAddAccountView$MyChromeClient;->this$0:Lcom/google/android/gsf/WebAddAccountView;

    # getter for: Lcom/google/android/gsf/WebAddAccountView;->mWebView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/google/android/gsf/WebAddAccountView;->access$200(Lcom/google/android/gsf/WebAddAccountView;)Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p4}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x1

    return v0
.end method

.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/gsf/WebAddAccountView$MyChromeClient;->this$0:Lcom/google/android/gsf/WebAddAccountView;

    # getter for: Lcom/google/android/gsf/WebAddAccountView;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/google/android/gsf/WebAddAccountView;->access$300(Lcom/google/android/gsf/WebAddAccountView;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method
