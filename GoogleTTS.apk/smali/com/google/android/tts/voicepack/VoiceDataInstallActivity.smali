.class public Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;
.super Landroid/app/Activity;
.source "VoiceDataInstallActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;,
        Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;
    }
.end annotation


# static fields
.field static final ENGINE_NAME:Ljava/lang/String; = "com.google.android.tts"
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

.field private final mDownloadSetObserver:Landroid/database/DataSetObserver;

.field private mLangCountryHelper:Lcom/google/android/tts/local/LangCountryHelper;

.field private mMetadataManager:Lcom/google/android/tts/voicepack/MetadataManager;

.field private mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;

.field private mVoicesActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;",
            ">;"
        }
    .end annotation
.end field

.field private mVoicesListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesActions:Ljava/util/List;

    new-instance v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$1;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)V

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mDownloadSetObserver:Landroid/database/DataSetObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    invoke-direct {p0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->onActiveDownloadSetChanged()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesActions:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/content/Context;

    invoke-static {p0, p1}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->getPreferedLocaleFromSettings(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;
    .param p1    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    invoke-direct {p0, p1}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->formatText1(Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Lcom/google/android/tts/voicepack/VoiceDataManager;
    .locals 1
    .param p0    # Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Lcom/google/android/tts/voicepack/VoiceDownloadHelper;
    .locals 1
    .param p0    # Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;
    .param p1    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    invoke-direct {p0, p1}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->formatText2(Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->handleDeleteVoice(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Lcom/google/android/tts/local/LangCountryHelper;
    .locals 1
    .param p0    # Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mLangCountryHelper:Lcom/google/android/tts/local/LangCountryHelper;

    return-object v0
.end method

.method private buildVoicePacksActions()V
    .locals 13

    const/4 v12, 0x0

    const/4 v11, 0x1

    iget-object v9, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mMetadataManager:Lcom/google/android/tts/voicepack/MetadataManager;

    invoke-virtual {v9}, Lcom/google/android/tts/voicepack/MetadataManager;->getVoiceMetadataMap()Ljava/util/Map;

    move-result-object v3

    iget-object v9, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;

    invoke-virtual {v9}, Lcom/google/android/tts/voicepack/VoiceDataManager;->getInstalledVoices()Ljava/util/Map;

    move-result-object v6

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    invoke-virtual {v7}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;

    new-instance v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;

    invoke-direct {v0, v12}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$1;)V

    iput-object v7, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;->metadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    if-eqz v5, :cond_2

    iget-object v9, v5, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;->mMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    invoke-virtual {v9}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getRevision()I

    move-result v2

    const/4 v9, -0x1

    if-ne v2, v9, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-virtual {v7}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getRevision()I

    move-result v9

    if-ge v2, v9, :cond_1

    iput-boolean v11, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;->canUpgrade:Z

    :cond_1
    :goto_1
    invoke-virtual {v7}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iput-boolean v11, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;->canInstall:Z

    goto :goto_1

    :cond_3
    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;

    iget-object v9, v8, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;->mLocation:Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    sget-object v10, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;->DATA:Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    if-ne v9, v10, :cond_4

    iget-object v9, v8, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;->mMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    invoke-virtual {v9}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;

    invoke-direct {v0, v12}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$1;)V

    iget-object v9, v8, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;->mMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    iput-object v9, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;->metadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    :cond_5
    iput-boolean v11, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;->canDelete:Z

    iget-object v9, v8, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;->mMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    invoke-virtual {v9}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_6
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-static {v9}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesActions:Ljava/util/List;

    return-void
.end method

.method private formatText1(Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/tts/voicepack/MetadataManager;->getDisplayString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private formatText2(Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;
    .locals 5
    .param p1    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    const v0, 0x7f06000a

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getSizeKb()I

    move-result v3

    mul-int/lit16 v3, v3, 0x400

    int-to-long v3, v3

    invoke-static {p0, v3, v4}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getPreferedLocaleFromSettings(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "tts_default_locale"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->parsePreferedLocaleFromSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleDeleteVoice(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    new-instance v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$2;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private onActiveDownloadSetChanged()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->buildVoicePacksActions()V

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;

    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesActions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesListView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    const v0, 0x7f070002

    invoke-virtual {p0, v0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method static parsePreferedLocaleFromSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/4 v6, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    const-string v7, ","

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    const/16 v7, 0x3a

    invoke-virtual {v5, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-lez v1, :cond_2

    const/4 v7, 0x0

    invoke-virtual {v5, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v1, 0x7f020000

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->setContentView(I)V

    invoke-static {p0}, Lcom/google/android/tts/GoogleTtsApplication;->get(Landroid/content/Context;)Lcom/google/android/tts/GoogleTtsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDataManager()Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;

    invoke-virtual {v0}, Lcom/google/android/tts/GoogleTtsApplication;->getMetadataManager()Lcom/google/android/tts/voicepack/MetadataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mMetadataManager:Lcom/google/android/tts/voicepack/MetadataManager;

    invoke-virtual {v0}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDownloadManager()Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    invoke-virtual {v0}, Lcom/google/android/tts/GoogleTtsApplication;->getLangCountryHelper()Lcom/google/android/tts/local/LangCountryHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mLangCountryHelper:Lcom/google/android/tts/local/LangCountryHelper;

    invoke-direct {p0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->buildVoicePacksActions()V

    const v1, 0x7f070001

    invoke-virtual {p0, v1}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesListView:Landroid/widget/ListView;

    new-instance v2, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;

    invoke-direct {v2, p0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    iget-object v1, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mDownloadSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/tts/voicepack/VoiceDownloadHelper;->unregisterDownloadSetObserver(Landroid/database/DataSetObserver;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->onActiveDownloadSetChanged()V

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    iget-object v1, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mDownloadSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/tts/voicepack/VoiceDownloadHelper;->registerDownloadSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method
