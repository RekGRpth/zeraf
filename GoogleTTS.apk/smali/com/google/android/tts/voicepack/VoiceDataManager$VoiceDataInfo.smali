.class public Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;
.super Ljava/lang/Object;
.source "VoiceDataManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/voicepack/VoiceDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VoiceDataInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;
    }
.end annotation


# instance fields
.field public final mLocation:Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

.field public final mMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;


# direct methods
.method constructor <init>(Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)V
    .locals 0
    .param p1    # Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;
    .param p2    # Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;->mLocation:Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo$Location;

    iput-object p2, p0, Lcom/google/android/tts/voicepack/VoiceDataManager$VoiceDataInfo;->mMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    return-void
.end method
