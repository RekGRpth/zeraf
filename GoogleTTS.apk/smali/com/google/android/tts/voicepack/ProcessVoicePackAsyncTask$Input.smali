.class Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;
.super Ljava/lang/Object;
.source "ProcessVoicePackAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Input"
.end annotation


# instance fields
.field public app:Lcom/google/android/tts/GoogleTtsApplication;

.field public downloadId:J

.field public result:Landroid/content/BroadcastReceiver$PendingResult;


# direct methods
.method public constructor <init>(Lcom/google/android/tts/GoogleTtsApplication;JLandroid/content/BroadcastReceiver$PendingResult;)V
    .locals 0
    .param p1    # Lcom/google/android/tts/GoogleTtsApplication;
    .param p2    # J
    .param p4    # Landroid/content/BroadcastReceiver$PendingResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->app:Lcom/google/android/tts/GoogleTtsApplication;

    iput-wide p2, p0, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->downloadId:J

    iput-object p4, p0, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->result:Landroid/content/BroadcastReceiver$PendingResult;

    return-void
.end method
