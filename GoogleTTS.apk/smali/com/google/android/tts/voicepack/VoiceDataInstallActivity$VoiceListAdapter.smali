.class final Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;
.super Landroid/widget/BaseAdapter;
.source "VoiceDataInstallActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "VoiceListAdapter"
.end annotation


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;


# direct methods
.method constructor <init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesActions:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$200(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesActions:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$200(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-eqz p2, :cond_1

    move-object/from16 v7, p2

    :goto_0
    const v10, 0x7f070003

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v10, 0x7f070004

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const v10, 0x7f070005

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v10, 0x7f070006

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoicesActions:Ljava/util/List;
    invoke-static {v10}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$200(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Ljava/util/List;

    move-result-object v10

    move/from16 v0, p1

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;

    iget-object v8, v9, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;->metadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    const-string v10, "com.google.android.tts"

    invoke-virtual/range {p3 .. p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v11

    # invokes: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->getPreferedLocaleFromSettings(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    invoke-static {v10, v11}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$300(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # invokes: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->formatText1(Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;
    invoke-static {v10, v8}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$400(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;
    invoke-static {v10}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$500(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/tts/voicepack/VoiceDataManager;->getAvailablePattsLocales()Ljava/util/Set;

    move-result-object v3

    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setEnabled(Z)V

    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setEnabled(Z)V

    const/16 v10, 0x8

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setVisibility(I)V

    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mDownloadManager:Lcom/google/android/tts/voicepack/VoiceDownloadHelper;
    invoke-static {v10}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$600(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    move-result-object v10

    invoke-virtual {v8}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/tts/voicepack/VoiceDownloadHelper;->isActiveDownload(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    const v10, 0x7f06000b

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(I)V

    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setVisibility(I)V

    const v10, 0x7f06000d

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setText(I)V

    new-instance v10, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v8}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$1;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)V

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_1
    return-object v7

    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v11, 0x7f020001

    const/4 v12, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v10, v11, v0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    goto/16 :goto_0

    :cond_2
    iget-boolean v10, v9, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;->canInstall:Z

    if-eqz v10, :cond_4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # invokes: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->formatText2(Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;
    invoke-static {v10, v8}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$700(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setVisibility(I)V

    const v10, 0x7f060010

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setText(I)V

    new-instance v10, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$2;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v8}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$2;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)V

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    :goto_2
    iget-boolean v10, v9, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;->canDelete:Z

    if-eqz v10, :cond_0

    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setVisibility(I)V

    const v10, 0x7f06000e

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setText(I)V

    invoke-virtual {v8}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    const v11, 0x7f060011

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;->this$0:Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    # getter for: Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->mVoiceDataManager:Lcom/google/android/tts/voicepack/VoiceDataManager;
    invoke-static {v15}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->access$500(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;)Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v15

    invoke-virtual {v8}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/tts/voicepack/VoiceDataManager;->getInstalledSize(Ljava/lang/String;)J

    move-result-wide v15

    invoke-static/range {v14 .. v16}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v10, v9, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceDataActions;->canUpgrade:Z

    if-eqz v10, :cond_3

    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setVisibility(I)V

    const v10, 0x7f06000f

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setText(I)V

    new-instance v10, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$3;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v8}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$3;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)V

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_5
    new-instance v10, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$4;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v8}, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter$4;-><init>(Lcom/google/android/tts/voicepack/VoiceDataInstallActivity$VoiceListAdapter;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;)V

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method
