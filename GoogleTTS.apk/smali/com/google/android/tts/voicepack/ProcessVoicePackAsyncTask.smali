.class public Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;
.super Landroid/os/AsyncTask;
.source "ProcessVoicePackAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;,
        Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;",
        "Ljava/lang/Void;",
        "Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private buildNotification(Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;)Landroid/app/Notification;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v0, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-direct {v1, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    iget-object v0, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->downloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v0, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/tts/voicepack/MetadataManager;->getDisplayString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v2, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->success:Z

    if-eqz v2, :cond_0

    const v2, 0x1080082

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    iget-object v2, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    const v3, 0x7f060014

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/tts/GoogleTtsApplication;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object v0, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    const v2, 0x7f060015

    invoke-virtual {v0, v2}, Lcom/google/android/tts/GoogleTtsApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.settings.TTS_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :goto_0
    iget-object v2, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-static {v2, v5, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    :cond_0
    const v2, 0x1080078

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    iget-object v2, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    const v3, 0x7f060016

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/tts/GoogleTtsApplication;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    iget-object v0, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    const v2, 0x7f060017

    invoke-virtual {v0, v2}, Lcom/google/android/tts/GoogleTtsApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    const-class v3, Lcom/google/android/tts/voicepack/VoiceDataInstallActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private createVoiceMetadataFile(Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;Lcom/google/android/tts/GoogleTtsApplication;)V
    .locals 4
    .param p1    # Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;
    .param p2    # Lcom/google/android/tts/GoogleTtsApplication;

    const/4 v0, 0x0

    iget-object v1, p1, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mMisc:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$MiscDownloadInfo;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mMisc:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$MiscDownloadInfo;

    iget-object v0, v1, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$MiscDownloadInfo;->mMetadata:Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    :cond_0
    if-nez v0, :cond_1

    const-string v1, "TTS.ProcessVoicePackAsyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No metadata for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " download. Using one from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "metadata list"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Lcom/google/android/tts/GoogleTtsApplication;->getMetadataManager()Lcom/google/android/tts/voicepack/MetadataManager;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/tts/voicepack/MetadataManager;->getMetadataForLocale(Ljava/lang/String;)Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDataManager()Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;->getLocale()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/tts/voicepack/VoiceDataManager;->updateVoiceMetadataFile(Ljava/lang/String;Lcom/google/android/tts/voicepack/VoiceMetadataProto$Metadata;Z)Z

    :goto_0
    return-void

    :cond_2
    const-string v1, "TTS.ProcessVoicePackAsyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t find metadata to put inside voice pack of"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private deleteDownloadedFile(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "TTS.ProcessVoicePackAsyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to delete downloaded file:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private handleCompletedDownload(Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;)Z
    .locals 7
    .param p1    # Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;
    .param p2    # Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;

    const/4 v2, 0x0

    iget-wide v3, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->downloadId:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    const-string v3, "TTS.ProcessVoicePackAsyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received intent without download ID :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->downloadId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-virtual {v3}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDownloadManager()Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    move-result-object v3

    iget-wide v4, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->downloadId:J

    invoke-virtual {v3, v4, v5}, Lcom/google/android/tts/voicepack/VoiceDownloadHelper;->getDownloadInfo(J)Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    move-result-object v3

    iput-object v3, p2, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->downloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v3, p2, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->downloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    if-eqz v3, :cond_0

    iget-object v0, p2, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->downloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    :try_start_0
    iget v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mStatus:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-virtual {v3}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDataManager()Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/tts/voicepack/VoiceDataManager;->deleteVoiceDataFiles(Ljava/lang/String;Z)Z

    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-direct {p0, v3, v4}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->processDownloadedFile(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-direct {p0, v0, v3}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->createVoiceMetadataFile(Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;Lcom/google/android/tts/GoogleTtsApplication;)V

    iget-object v3, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-virtual {v3}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDataManager()Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/tts/voicepack/VoiceDataManager;->updateAvailableLangs()V

    iget-object v3, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-virtual {v3}, Lcom/google/android/tts/GoogleTtsApplication;->getLangCountryHelper()Lcom/google/android/tts/local/LangCountryHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/tts/local/LangCountryHelper;->updateLocaleList()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    const/4 v2, 0x1

    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->deleteDownloadedFile(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->deleteDownloadedFile(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_1
    const-string v3, "TTS.ProcessVoicePackAsyncTask"

    const-string v4, "Failed to process downloaded voice pack"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-virtual {v3}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDataManager()Lcom/google/android/tts/voicepack/VoiceDataManager;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/tts/voicepack/VoiceDataManager;->deleteVoiceDataFiles(Ljava/lang/String;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->deleteDownloadedFile(Ljava/lang/String;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->deleteDownloadedFile(Ljava/lang/String;)V

    :cond_5
    throw v2
.end method

.method private processDownloadedFile(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 20
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Context;

    :try_start_0
    new-instance v9, Ljava/util/zip/ZipFile;

    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v9, v0, v1}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;I)V

    invoke-virtual {v9}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/zip/ZipEntry;

    const-string v17, "patts"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    new-instance v14, Ljava/io/File;

    invoke-direct {v14, v5, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-virtual {v14}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v10

    const-string v17, "TTS.ProcessVoicePackAsyncTask"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception processing downloaded file :"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v17, 0x0

    :goto_1
    return v17

    :cond_0
    const/4 v11, 0x0

    const/4 v12, 0x0

    :try_start_1
    invoke-virtual {v9, v7}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v11

    new-instance v13, Ljava/io/FileOutputStream;

    invoke-direct {v13, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v4, 0x0

    const/16 v16, 0x0

    const/high16 v17, 0x10000

    :try_start_2
    move/from16 v0, v17

    new-array v3, v0, [B

    :goto_2
    invoke-virtual {v11, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_1

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v13, v3, v0, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    add-int v16, v16, v4

    goto :goto_2

    :cond_1
    if-eqz v11, :cond_2

    :try_start_3
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    :cond_2
    if-eqz v13, :cond_3

    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V

    :cond_3
    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_0

    :catchall_0
    move-exception v17

    :goto_3
    if-eqz v11, :cond_4

    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    :cond_4
    if-eqz v12, :cond_5

    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    :cond_5
    throw v17

    :cond_6
    invoke-virtual {v9}, Ljava/util/zip/ZipFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    const/16 v17, 0x1

    goto :goto_1

    :catchall_1
    move-exception v17

    move-object v12, v13

    goto :goto_3
.end method

.method private showDownloadCompleteNotification(Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;)V
    .locals 4
    .param p1    # Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;

    iget-object v2, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->downloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->downloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v2, v2, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mMisc:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$MiscDownloadInfo;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->downloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v2, v2, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mMisc:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$MiscDownloadInfo;

    iget-boolean v2, v2, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$MiscDownloadInfo;->mUserInitiated:Z

    if-eqz v2, :cond_0

    :cond_2
    iget-object v2, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    const-string v3, "notification"

    invoke-virtual {v2, v3}, Lcom/google/android/tts/GoogleTtsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {p0, p1}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->buildNotification(Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;)Landroid/app/Notification;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;)Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;
    .locals 3
    .param p1    # [Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;

    const/4 v2, 0x0

    aget-object v0, p1, v2

    new-instance v1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;

    invoke-direct {v1}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->handleCompletedDownload(Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;)Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->success:Z

    iget-object v2, v0, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->result:Landroid/content/BroadcastReceiver$PendingResult;

    iput-object v2, v1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->result:Landroid/content/BroadcastReceiver$PendingResult;

    iget-object v2, v0, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;->app:Lcom/google/android/tts/GoogleTtsApplication;

    iput-object v2, v1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;

    invoke-virtual {p0, p1}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->doInBackground([Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Input;)Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;)V
    .locals 3
    .param p1    # Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;

    iget-object v1, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->downloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    if-nez v1, :cond_0

    iget-boolean v1, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->success:Z

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->result:Landroid/content/BroadcastReceiver$PendingResult;

    invoke-virtual {v1}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->downloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    iget-object v1, v1, Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;->mDownloadLocale:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-virtual {v1}, Lcom/google/android/tts/GoogleTtsApplication;->getVoiceDownloadManager()Lcom/google/android/tts/voicepack/VoiceDownloadHelper;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->downloadInfo:Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;

    invoke-virtual {v1, v2}, Lcom/google/android/tts/voicepack/VoiceDownloadHelper;->markCompleted(Lcom/google/android/tts/voicepack/VoiceDownloadHelper$DownloadInfo;)V

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.tts.engine.TTS_DATA_INSTALLED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "dataInstalled"

    iget-boolean v1, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->success:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->app:Lcom/google/android/tts/GoogleTtsApplication;

    invoke-virtual {v1, v0}, Lcom/google/android/tts/GoogleTtsApplication;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->showDownloadCompleteNotification(Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;)V

    iget-object v1, p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;->result:Landroid/content/BroadcastReceiver$PendingResult;

    invoke-virtual {v1}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;

    invoke-virtual {p0, p1}, Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask;->onPostExecute(Lcom/google/android/tts/voicepack/ProcessVoicePackAsyncTask$Output;)V

    return-void
.end method
