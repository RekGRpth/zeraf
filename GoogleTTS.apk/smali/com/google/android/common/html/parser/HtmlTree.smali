.class public Lcom/google/android/common/html/parser/HtmlTree;
.super Ljava/lang/Object;
.source "HtmlTree.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/common/html/parser/HtmlTree$2;,
        Lcom/google/android/common/html/parser/HtmlTree$DefaultPlainTextConverter;,
        Lcom/google/android/common/html/parser/HtmlTree$PlainTextPrinter;,
        Lcom/google/android/common/html/parser/HtmlTree$Block;,
        Lcom/google/android/common/html/parser/HtmlTree$PlainTextConverter;,
        Lcom/google/android/common/html/parser/HtmlTree$PlainTextConverterFactory;
    }
.end annotation


# static fields
.field private static final DEFAULT_CONVERTER_FACTORY:Lcom/google/android/common/html/parser/HtmlTree$PlainTextConverterFactory;

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private final begins:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private converterFactory:Lcom/google/android/common/html/parser/HtmlTree$PlainTextConverterFactory;

.field private final ends:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final nodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/common/html/parser/HtmlDocument$Node;",
            ">;"
        }
    .end annotation
.end field

.field private parent:I

.field private stack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/common/html/parser/HtmlTree$1;

    invoke-direct {v0}, Lcom/google/android/common/html/parser/HtmlTree$1;-><init>()V

    sput-object v0, Lcom/google/android/common/html/parser/HtmlTree;->DEFAULT_CONVERTER_FACTORY:Lcom/google/android/common/html/parser/HtmlTree$PlainTextConverterFactory;

    const-class v0, Lcom/google/android/common/html/parser/HtmlTree;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/common/html/parser/HtmlTree;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->nodes:Ljava/util/List;

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->begins:Ljava/util/Stack;

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->ends:Ljava/util/Stack;

    sget-object v0, Lcom/google/android/common/html/parser/HtmlTree;->DEFAULT_CONVERTER_FACTORY:Lcom/google/android/common/html/parser/HtmlTree$PlainTextConverterFactory;

    iput-object v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->converterFactory:Lcom/google/android/common/html/parser/HtmlTree$PlainTextConverterFactory;

    return-void
.end method

.method private addNode(Lcom/google/android/common/html/parser/HtmlDocument$Node;II)V
    .locals 2
    .param p1    # Lcom/google/android/common/html/parser/HtmlDocument$Node;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->nodes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->begins:Ljava/util/Stack;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->ends:Ljava/util/Stack;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method addEndTag(Lcom/google/android/common/html/parser/HtmlDocument$EndTag;)V
    .locals 4
    .param p1    # Lcom/google/android/common/html/parser/HtmlDocument$EndTag;

    iget-object v1, p0, Lcom/google/android/common/html/parser/HtmlTree;->nodes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/common/html/parser/HtmlTree;->parent:I

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/common/html/parser/HtmlTree;->addNode(Lcom/google/android/common/html/parser/HtmlDocument$Node;II)V

    iget v1, p0, Lcom/google/android/common/html/parser/HtmlTree;->parent:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/common/html/parser/HtmlTree;->ends:Ljava/util/Stack;

    iget v2, p0, Lcom/google/android/common/html/parser/HtmlTree;->parent:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Stack;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lcom/google/android/common/html/parser/HtmlTree;->stack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/android/common/html/parser/HtmlTree;->parent:I

    return-void
.end method

.method addSingularTag(Lcom/google/android/common/html/parser/HtmlDocument$Tag;)V
    .locals 2
    .param p1    # Lcom/google/android/common/html/parser/HtmlDocument$Tag;

    iget-object v1, p0, Lcom/google/android/common/html/parser/HtmlTree;->nodes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/common/html/parser/HtmlTree;->addNode(Lcom/google/android/common/html/parser/HtmlDocument$Node;II)V

    return-void
.end method

.method addStartTag(Lcom/google/android/common/html/parser/HtmlDocument$Tag;)V
    .locals 3
    .param p1    # Lcom/google/android/common/html/parser/HtmlDocument$Tag;

    iget-object v1, p0, Lcom/google/android/common/html/parser/HtmlTree;->nodes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/common/html/parser/HtmlTree;->addNode(Lcom/google/android/common/html/parser/HtmlDocument$Node;II)V

    iget-object v1, p0, Lcom/google/android/common/html/parser/HtmlTree;->stack:Ljava/util/Stack;

    iget v2, p0, Lcom/google/android/common/html/parser/HtmlTree;->parent:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    iput v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->parent:I

    return-void
.end method

.method addText(Lcom/google/android/common/html/parser/HtmlDocument$Text;)V
    .locals 2
    .param p1    # Lcom/google/android/common/html/parser/HtmlDocument$Text;

    iget-object v1, p0, Lcom/google/android/common/html/parser/HtmlTree;->nodes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/common/html/parser/HtmlTree;->addNode(Lcom/google/android/common/html/parser/HtmlDocument$Node;II)V

    return-void
.end method

.method finish()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/common/base/X;->assertTrue(Z)V

    iget v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->parent:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/common/base/X;->assertTrue(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method start()V
    .locals 1

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->stack:Ljava/util/Stack;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/common/html/parser/HtmlTree;->parent:I

    return-void
.end method
