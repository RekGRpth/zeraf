.class public final Lcom/google/speech/patts/engine/api/VoiceType;
.super Ljava/lang/Object;
.source "VoiceType.java"


# instance fields
.field private final cur_val_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/speech/patts/engine/api/VoiceType;->cur_val_:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/speech/patts/engine/api/VoiceType;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/speech/patts/engine/api/VoiceType;->cur_val_:I

    check-cast p1, Lcom/google/speech/patts/engine/api/VoiceType;

    invoke-virtual {p1}, Lcom/google/speech/patts/engine/api/VoiceType;->value()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public value()I
    .locals 1

    iget v0, p0, Lcom/google/speech/patts/engine/api/VoiceType;->cur_val_:I

    return v0
.end method
