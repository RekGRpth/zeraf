.class public Lcom/android/apps/tag/record/TextRecord;
.super Lcom/android/apps/tag/record/ParsedNdefRecord;
.source "TextRecord.java"


# instance fields
.field private final mLanguageCode:Ljava/lang/String;

.field private final mText:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/apps/tag/record/ParsedNdefRecord;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/android/apps/tag/record/TextRecord;->mLanguageCode:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/android/apps/tag/record/TextRecord;->mText:Ljava/lang/String;

    return-void
.end method

.method public static isText(Landroid/nfc/NdefRecord;)Z
    .locals 2
    .param p0    # Landroid/nfc/NdefRecord;

    :try_start_0
    invoke-static {p0}, Lcom/android/apps/tag/record/TextRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/TextRecord;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newTextRecord(Ljava/lang/String;Ljava/util/Locale;)Landroid/nfc/NdefRecord;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/util/Locale;

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/android/apps/tag/record/TextRecord;->newTextRecord(Ljava/lang/String;Ljava/util/Locale;Z)Landroid/nfc/NdefRecord;

    move-result-object v0

    return-object v0
.end method

.method public static newTextRecord(Ljava/lang/String;Ljava/util/Locale;Z)Landroid/nfc/NdefRecord;
    .locals 11
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/util/Locale;
    .param p2    # Z

    const/4 v10, 0x1

    const/4 v6, 0x0

    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    const-string v8, "US-ASCII"

    invoke-static {v8}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    if-eqz p2, :cond_0

    const-string v7, "UTF-8"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v5

    :goto_0
    invoke-virtual {p0, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    if-eqz p2, :cond_1

    move v4, v6

    :goto_1
    array-length v7, v1

    add-int/2addr v7, v4

    int-to-char v2, v7

    const/4 v7, 0x3

    new-array v7, v7, [[B

    new-array v8, v10, [B

    int-to-byte v9, v2

    aput-byte v9, v8, v6

    aput-object v8, v7, v6

    aput-object v1, v7, v10

    const/4 v8, 0x2

    aput-object v3, v7, v8

    invoke-static {v7}, Lcom/google/common/primitives/Bytes;->concat([[B)[B

    move-result-object v0

    new-instance v7, Landroid/nfc/NdefRecord;

    sget-object v8, Landroid/nfc/NdefRecord;->RTD_TEXT:[B

    new-array v6, v6, [B

    invoke-direct {v7, v10, v8, v6, v0}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    return-object v7

    :cond_0
    const-string v7, "UTF-16"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v5

    goto :goto_0

    :cond_1
    const/16 v4, 0x80

    goto :goto_1
.end method

.method public static parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/TextRecord;
    .locals 10
    .param p0    # Landroid/nfc/NdefRecord;

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v6

    if-ne v6, v7, :cond_0

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v6

    sget-object v9, Landroid/nfc/NdefRecord;->RTD_TEXT:[B

    invoke-static {v6, v9}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v6

    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    :try_start_0
    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v3

    array-length v6, v3

    if-lez v6, :cond_1

    move v6, v7

    :goto_1
    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0x80

    if-nez v6, :cond_2

    const-string v5, "UTF-8"

    :goto_2
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit8 v2, v6, 0x3f

    array-length v6, v3

    sub-int/2addr v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ltz v6, :cond_3

    :goto_3
    invoke-static {v7}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    new-instance v1, Ljava/lang/String;

    const/4 v6, 0x1

    const-string v7, "US-ASCII"

    invoke-direct {v1, v3, v6, v2, v7}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    new-instance v4, Ljava/lang/String;

    add-int/lit8 v6, v2, 0x1

    array-length v7, v3

    sub-int/2addr v7, v2

    add-int/lit8 v7, v7, -0x1

    invoke-direct {v4, v3, v6, v7, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    new-instance v6, Lcom/android/apps/tag/record/TextRecord;

    invoke-direct {v6, v1, v4}, Lcom/android/apps/tag/record/TextRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v6

    :cond_0
    move v6, v8

    goto :goto_0

    :cond_1
    move v6, v8

    goto :goto_1

    :cond_2
    const-string v5, "UTF-16"
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :cond_3
    move v7, v8

    goto :goto_3

    :catch_0
    move-exception v0

    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v6
.end method


# virtual methods
.method public getLanguageCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/apps/tag/record/TextRecord;->mLanguageCode:Ljava/lang/String;

    return-object v0
.end method

.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # I

    const v1, 0x7f030002

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/apps/tag/record/TextRecord;->mText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method
