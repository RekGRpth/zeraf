.class public Lcom/android/apps/tag/message/ParsedNdefMessage;
.super Ljava/lang/Object;
.source "ParsedNdefMessage.java"


# instance fields
.field private mRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/apps/tag/record/ParsedNdefRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/apps/tag/record/ParsedNdefRecord;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/apps/tag/message/ParsedNdefMessage;->mRecords:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/apps/tag/record/ParsedNdefRecord;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/apps/tag/message/ParsedNdefMessage;->mRecords:Ljava/util/List;

    return-object v0
.end method
