.class public Lmaps/av/e;
.super Lmaps/av/c;


# static fields
.field private static final e:Lmaps/av/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/av/j;

    const/16 v1, 0x64

    const-string v2, "ByteChunkArrayManager"

    invoke-direct {v0, v1, v2}, Lmaps/av/j;-><init>(ILjava/lang/String;)V

    sput-object v0, Lmaps/av/e;->e:Lmaps/av/d;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    const/16 v0, 0xc

    sget-object v1, Lmaps/av/e;->e:Lmaps/av/d;

    invoke-direct {p0, p1, v0, v1}, Lmaps/av/c;-><init>(IILmaps/av/d;)V

    return-void
.end method


# virtual methods
.method public a(Lmaps/av/k;)V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lmaps/av/e;->b:I

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lmaps/av/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    const/16 v2, 0x1000

    invoke-interface {p1, v0, v2}, Lmaps/av/k;->a([BI)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget v0, p0, Lmaps/av/e;->b:I

    iget-object v1, p0, Lmaps/av/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lmaps/av/e;->c:Ljava/lang/Object;

    check-cast v0, [B

    iget v1, p0, Lmaps/av/e;->d:I

    invoke-interface {p1, v0, v1}, Lmaps/av/k;->a([BI)V

    :cond_1
    return-void
.end method
