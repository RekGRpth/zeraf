.class abstract Lmaps/f/fi;
.super Ljava/lang/Object;


# instance fields
.field b:I

.field c:I

.field d:Lmaps/f/bm;

.field e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field f:Lmaps/f/fm;

.field g:Lmaps/f/i;

.field h:Lmaps/f/i;

.field final synthetic i:Lmaps/f/ea;


# direct methods
.method constructor <init>(Lmaps/f/ea;)V
    .locals 1

    iput-object p1, p0, Lmaps/f/fi;->i:Lmaps/f/ea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lmaps/f/ea;->c:[Lmaps/f/bm;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/f/fi;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lmaps/f/fi;->c:I

    invoke-virtual {p0}, Lmaps/f/fi;->b()V

    return-void
.end method


# virtual methods
.method a(Lmaps/f/fm;)Z
    .locals 4

    :try_start_0
    invoke-interface {p1}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lmaps/f/fi;->i:Lmaps/f/ea;

    invoke-virtual {v1, p1}, Lmaps/f/ea;->b(Lmaps/f/fm;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lmaps/f/i;

    iget-object v3, p0, Lmaps/f/fi;->i:Lmaps/f/ea;

    invoke-direct {v2, v3, v0, v1}, Lmaps/f/i;-><init>(Lmaps/f/ea;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v2, p0, Lmaps/f/fi;->g:Lmaps/f/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/f/fi;->d:Lmaps/f/bm;

    invoke-virtual {v1}, Lmaps/f/bm;->n()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/f/fi;->d:Lmaps/f/bm;

    invoke-virtual {v1}, Lmaps/f/bm;->n()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/f/fi;->d:Lmaps/f/bm;

    invoke-virtual {v1}, Lmaps/f/bm;->n()V

    throw v0
.end method

.method final b()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/f/fi;->g:Lmaps/f/i;

    invoke-virtual {p0}, Lmaps/f/fi;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lmaps/f/fi;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget v0, p0, Lmaps/f/fi;->b:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lmaps/f/fi;->i:Lmaps/f/ea;

    iget-object v0, v0, Lmaps/f/ea;->c:[Lmaps/f/bm;

    iget v1, p0, Lmaps/f/fi;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lmaps/f/fi;->b:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lmaps/f/fi;->d:Lmaps/f/bm;

    iget-object v0, p0, Lmaps/f/fi;->d:Lmaps/f/bm;

    iget v0, v0, Lmaps/f/bm;->b:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/f/fi;->d:Lmaps/f/bm;

    iget-object v0, v0, Lmaps/f/bm;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lmaps/f/fi;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget-object v0, p0, Lmaps/f/fi;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/f/fi;->c:I

    invoke-virtual {p0}, Lmaps/f/fi;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method c()Z
    .locals 1

    iget-object v0, p0, Lmaps/f/fi;->f:Lmaps/f/fm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/f/fi;->f:Lmaps/f/fm;

    invoke-interface {v0}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/fi;->f:Lmaps/f/fm;

    :goto_0
    iget-object v0, p0, Lmaps/f/fi;->f:Lmaps/f/fm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/f/fi;->f:Lmaps/f/fm;

    invoke-virtual {p0, v0}, Lmaps/f/fi;->a(Lmaps/f/fm;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/f/fi;->f:Lmaps/f/fm;

    invoke-interface {v0}, Lmaps/f/fm;->b()Lmaps/f/fm;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/fi;->f:Lmaps/f/fm;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method d()Z
    .locals 3

    :cond_0
    iget v0, p0, Lmaps/f/fi;->c:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lmaps/f/fi;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lmaps/f/fi;->c:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lmaps/f/fi;->c:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/fm;

    iput-object v0, p0, Lmaps/f/fi;->f:Lmaps/f/fm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/f/fi;->f:Lmaps/f/fm;

    invoke-virtual {p0, v0}, Lmaps/f/fi;->a(Lmaps/f/fm;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lmaps/f/fi;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Lmaps/f/i;
    .locals 1

    iget-object v0, p0, Lmaps/f/fi;->g:Lmaps/f/i;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/f/fi;->g:Lmaps/f/i;

    iput-object v0, p0, Lmaps/f/fi;->h:Lmaps/f/i;

    invoke-virtual {p0}, Lmaps/f/fi;->b()V

    iget-object v0, p0, Lmaps/f/fi;->h:Lmaps/f/i;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lmaps/f/fi;->g:Lmaps/f/i;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lmaps/f/fi;->h:Lmaps/f/i;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    iget-object v0, p0, Lmaps/f/fi;->i:Lmaps/f/ea;

    iget-object v1, p0, Lmaps/f/fi;->h:Lmaps/f/i;

    invoke-virtual {v1}, Lmaps/f/i;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/f/ea;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/f/fi;->h:Lmaps/f/i;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
