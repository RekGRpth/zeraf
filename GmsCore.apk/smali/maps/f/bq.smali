.class public final Lmaps/f/bq;
.super Lmaps/f/x;


# instance fields
.field b:Z

.field c:I

.field d:I

.field e:I

.field f:Lmaps/f/cx;

.field g:Lmaps/f/cx;

.field h:J

.field i:J

.field j:Lmaps/f/an;

.field k:Lmaps/ap/a;

.field l:Lmaps/ap/a;

.field m:Lmaps/ap/c;


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, -0x1

    const/4 v0, -0x1

    invoke-direct {p0}, Lmaps/f/x;-><init>()V

    iput v0, p0, Lmaps/f/bq;->c:I

    iput v0, p0, Lmaps/f/bq;->d:I

    iput v0, p0, Lmaps/f/bq;->e:I

    iput-wide v1, p0, Lmaps/f/bq;->h:J

    iput-wide v1, p0, Lmaps/f/bq;->i:J

    return-void
.end method


# virtual methods
.method b()Lmaps/ap/a;
    .locals 2

    iget-object v0, p0, Lmaps/f/bq;->k:Lmaps/ap/a;

    invoke-virtual {p0}, Lmaps/f/bq;->f()Lmaps/f/cx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/f/cx;->a()Lmaps/ap/a;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ap/a;

    return-object v0
.end method

.method c()Lmaps/ap/a;
    .locals 2

    iget-object v0, p0, Lmaps/f/bq;->l:Lmaps/ap/a;

    invoke-virtual {p0}, Lmaps/f/bq;->g()Lmaps/f/cx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/f/cx;->a()Lmaps/ap/a;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ap/a;

    return-object v0
.end method

.method d()I
    .locals 2

    iget v0, p0, Lmaps/f/bq;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/f/bq;->c:I

    goto :goto_0
.end method

.method e()I
    .locals 2

    iget v0, p0, Lmaps/f/bq;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/f/bq;->d:I

    goto :goto_0
.end method

.method f()Lmaps/f/cx;
    .locals 2

    iget-object v0, p0, Lmaps/f/bq;->f:Lmaps/f/cx;

    sget-object v1, Lmaps/f/cx;->a:Lmaps/f/cx;

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/cx;

    return-object v0
.end method

.method g()Lmaps/f/cx;
    .locals 2

    iget-object v0, p0, Lmaps/f/bq;->g:Lmaps/f/cx;

    sget-object v1, Lmaps/f/cx;->a:Lmaps/f/cx;

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/cx;

    return-object v0
.end method

.method h()J
    .locals 4

    iget-wide v0, p0, Lmaps/f/bq;->h:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lmaps/f/bq;->h:J

    goto :goto_0
.end method

.method i()J
    .locals 4

    iget-wide v0, p0, Lmaps/f/bq;->i:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lmaps/f/bq;->i:J

    goto :goto_0
.end method

.method j()Lmaps/ap/c;
    .locals 2

    iget-object v0, p0, Lmaps/f/bq;->m:Lmaps/ap/c;

    invoke-static {}, Lmaps/ap/c;->b()Lmaps/ap/c;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ap/c;

    return-object v0
.end method

.method public k()Ljava/util/concurrent/ConcurrentMap;
    .locals 4

    iget-boolean v0, p0, Lmaps/f/bq;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Lmaps/f/bq;->d()I

    move-result v1

    const/high16 v2, 0x3f400000

    invoke-virtual {p0}, Lmaps/f/bq;->e()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/f/bq;->j:Lmaps/f/an;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/f/ea;

    invoke-direct {v0, p0}, Lmaps/f/ea;-><init>(Lmaps/f/bq;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lmaps/f/d;

    invoke-direct {v0, p0}, Lmaps/f/d;-><init>(Lmaps/f/bq;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    const-wide/16 v5, -0x1

    const/4 v3, -0x1

    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    iget v1, p0, Lmaps/f/bq;->c:I

    if-eq v1, v3, :cond_0

    const-string v1, "initialCapacity"

    iget v2, p0, Lmaps/f/bq;->c:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    :cond_0
    iget v1, p0, Lmaps/f/bq;->d:I

    if-eq v1, v3, :cond_1

    const-string v1, "concurrencyLevel"

    iget v2, p0, Lmaps/f/bq;->d:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    :cond_1
    iget v1, p0, Lmaps/f/bq;->e:I

    if-eq v1, v3, :cond_2

    const-string v1, "maximumSize"

    iget v2, p0, Lmaps/f/bq;->e:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    :cond_2
    iget-wide v1, p0, Lmaps/f/bq;->h:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_3

    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lmaps/f/bq;->h:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    :cond_3
    iget-wide v1, p0, Lmaps/f/bq;->i:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_4

    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lmaps/f/bq;->i:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    :cond_4
    iget-object v1, p0, Lmaps/f/bq;->f:Lmaps/f/cx;

    if-eqz v1, :cond_5

    const-string v1, "keyStrength"

    iget-object v2, p0, Lmaps/f/bq;->f:Lmaps/f/cx;

    invoke-virtual {v2}, Lmaps/f/cx;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/ap/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    :cond_5
    iget-object v1, p0, Lmaps/f/bq;->g:Lmaps/f/cx;

    if-eqz v1, :cond_6

    const-string v1, "valueStrength"

    iget-object v2, p0, Lmaps/f/bq;->g:Lmaps/f/cx;

    invoke-virtual {v2}, Lmaps/f/cx;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/ap/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    :cond_6
    iget-object v1, p0, Lmaps/f/bq;->k:Lmaps/ap/a;

    if-eqz v1, :cond_7

    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, Lmaps/ap/n;->a(Ljava/lang/Object;)Lmaps/ap/n;

    :cond_7
    iget-object v1, p0, Lmaps/f/bq;->l:Lmaps/ap/a;

    if-eqz v1, :cond_8

    const-string v1, "valueEquivalence"

    invoke-virtual {v0, v1}, Lmaps/ap/n;->a(Ljava/lang/Object;)Lmaps/ap/n;

    :cond_8
    iget-object v1, p0, Lmaps/f/bq;->a:Lmaps/f/o;

    if-eqz v1, :cond_9

    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, Lmaps/ap/n;->a(Ljava/lang/Object;)Lmaps/ap/n;

    :cond_9
    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
