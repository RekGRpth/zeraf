.class Lmaps/f/ca;
.super Lmaps/f/am;


# instance fields
.field final transient a:Lmaps/f/ad;


# direct methods
.method constructor <init>(Lmaps/f/ad;)V
    .locals 1

    invoke-static {p1}, Lmaps/f/ad;->a(Lmaps/f/ad;)[Lmaps/f/fw;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/f/am;-><init>([Ljava/lang/Object;)V

    iput-object p1, p0, Lmaps/f/ca;->a:Lmaps/f/ad;

    return-void
.end method


# virtual methods
.method public contains(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_0

    check-cast p1, Ljava/util/Map$Entry;

    iget-object v1, p0, Lmaps/f/ca;->a:Lmaps/f/ad;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/f/ad;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
