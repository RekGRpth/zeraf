.class final Lmaps/f/ej;
.super Ljava/util/AbstractSet;


# instance fields
.field final synthetic a:Lmaps/f/ea;


# direct methods
.method constructor <init>(Lmaps/f/ea;)V
    .locals 0

    iput-object p1, p0, Lmaps/f/ej;->a:Lmaps/f/ea;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lmaps/f/ej;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/f/ej;->a:Lmaps/f/ea;

    invoke-virtual {v0, p1}, Lmaps/f/ea;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lmaps/f/ej;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lmaps/f/by;

    iget-object v1, p0, Lmaps/f/ej;->a:Lmaps/f/ea;

    invoke-direct {v0, v1}, Lmaps/f/by;-><init>(Lmaps/f/ea;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/f/ej;->a:Lmaps/f/ea;

    invoke-virtual {v0, p1}, Lmaps/f/ea;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lmaps/f/ej;->a:Lmaps/f/ea;

    invoke-virtual {v0}, Lmaps/f/ea;->size()I

    move-result v0

    return v0
.end method
