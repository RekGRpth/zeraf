.class Lmaps/f/cr;
.super Lmaps/f/cq;

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field final synthetic c:Lmaps/f/cd;


# direct methods
.method constructor <init>(Lmaps/f/cd;Ljava/util/SortedMap;)V
    .locals 0

    iput-object p1, p0, Lmaps/f/cr;->c:Lmaps/f/cd;

    invoke-direct {p0, p1, p2}, Lmaps/f/cq;-><init>(Lmaps/f/cd;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method b()Ljava/util/SortedMap;
    .locals 1

    iget-object v0, p0, Lmaps/f/cr;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/cr;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/cr;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    new-instance v0, Lmaps/f/cr;

    iget-object v1, p0, Lmaps/f/cr;->c:Lmaps/f/cd;

    invoke-virtual {p0}, Lmaps/f/cr;->b()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/f/cr;-><init>(Lmaps/f/cd;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/cr;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    new-instance v0, Lmaps/f/cr;

    iget-object v1, p0, Lmaps/f/cr;->c:Lmaps/f/cd;

    invoke-virtual {p0}, Lmaps/f/cr;->b()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/f/cr;-><init>(Lmaps/f/cd;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    new-instance v0, Lmaps/f/cr;

    iget-object v1, p0, Lmaps/f/cr;->c:Lmaps/f/cd;

    invoke-virtual {p0}, Lmaps/f/cr;->b()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/f/cr;-><init>(Lmaps/f/cd;Ljava/util/SortedMap;)V

    return-object v0
.end method
