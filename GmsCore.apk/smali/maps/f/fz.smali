.class Lmaps/f/fz;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field a:Z

.field b:Z

.field final synthetic c:Ljava/util/ListIterator;

.field final synthetic d:Lmaps/f/g;


# direct methods
.method constructor <init>(Lmaps/f/g;Ljava/util/ListIterator;)V
    .locals 0

    iput-object p1, p0, Lmaps/f/fz;->d:Lmaps/f/g;

    iput-object p2, p0, Lmaps/f/fz;->c:Ljava/util/ListIterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lmaps/f/fz;->c:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/f/fz;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/f/fz;->a:Z

    iput-boolean v0, p0, Lmaps/f/fz;->b:Z

    return-void
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lmaps/f/fz;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public hasPrevious()Z
    .locals 1

    iget-object v0, p0, Lmaps/f/fz;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/fz;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/f/fz;->a:Z

    iput-boolean v0, p0, Lmaps/f/fz;->b:Z

    iget-object v0, p0, Lmaps/f/fz;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public nextIndex()I
    .locals 2

    iget-object v0, p0, Lmaps/f/fz;->d:Lmaps/f/g;

    iget-object v1, p0, Lmaps/f/fz;->c:Ljava/util/ListIterator;

    invoke-interface {v1}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    invoke-static {v0, v1}, Lmaps/f/g;->a(Lmaps/f/g;I)I

    move-result v0

    return v0
.end method

.method public previous()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/fz;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/f/fz;->a:Z

    iput-boolean v0, p0, Lmaps/f/fz;->b:Z

    iget-object v0, p0, Lmaps/f/fz;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    invoke-virtual {p0}, Lmaps/f/fz;->nextIndex()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public remove()V
    .locals 1

    iget-boolean v0, p0, Lmaps/f/fz;->a:Z

    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    iget-object v0, p0, Lmaps/f/fz;->c:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/f/fz;->b:Z

    iput-boolean v0, p0, Lmaps/f/fz;->a:Z

    return-void
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    iget-boolean v0, p0, Lmaps/f/fz;->b:Z

    invoke-static {v0}, Lmaps/ap/q;->b(Z)V

    iget-object v0, p0, Lmaps/f/fz;->c:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    return-void
.end method
