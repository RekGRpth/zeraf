.class abstract enum Lmaps/f/dd;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/f/dd;

.field public static final enum b:Lmaps/f/dd;

.field public static final enum c:Lmaps/f/dd;

.field public static final enum d:Lmaps/f/dd;

.field public static final enum e:Lmaps/f/dd;

.field public static final enum f:Lmaps/f/dd;

.field public static final enum g:Lmaps/f/dd;

.field public static final enum h:Lmaps/f/dd;

.field public static final enum i:Lmaps/f/dd;

.field public static final enum j:Lmaps/f/dd;

.field public static final enum k:Lmaps/f/dd;

.field public static final enum l:Lmaps/f/dd;

.field static final m:[[Lmaps/f/dd;

.field private static final synthetic n:[Lmaps/f/dd;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lmaps/f/eq;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, Lmaps/f/eq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->a:Lmaps/f/dd;

    new-instance v0, Lmaps/f/es;

    const-string v1, "STRONG_EXPIRABLE"

    invoke-direct {v0, v1, v4}, Lmaps/f/es;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->b:Lmaps/f/dd;

    new-instance v0, Lmaps/f/er;

    const-string v1, "STRONG_EVICTABLE"

    invoke-direct {v0, v1, v5}, Lmaps/f/er;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->c:Lmaps/f/dd;

    new-instance v0, Lmaps/f/ev;

    const-string v1, "STRONG_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1, v6}, Lmaps/f/ev;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->d:Lmaps/f/dd;

    new-instance v0, Lmaps/f/eu;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v7}, Lmaps/f/eu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->e:Lmaps/f/dd;

    new-instance v0, Lmaps/f/el;

    const-string v1, "SOFT_EXPIRABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lmaps/f/el;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->f:Lmaps/f/dd;

    new-instance v0, Lmaps/f/ek;

    const-string v1, "SOFT_EVICTABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lmaps/f/ek;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->g:Lmaps/f/dd;

    new-instance v0, Lmaps/f/eo;

    const-string v1, "SOFT_EXPIRABLE_EVICTABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lmaps/f/eo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->h:Lmaps/f/dd;

    new-instance v0, Lmaps/f/em;

    const-string v1, "WEAK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lmaps/f/em;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->i:Lmaps/f/dd;

    new-instance v0, Lmaps/f/bp;

    const-string v1, "WEAK_EXPIRABLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lmaps/f/bp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->j:Lmaps/f/dd;

    new-instance v0, Lmaps/f/bt;

    const-string v1, "WEAK_EVICTABLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lmaps/f/bt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->k:Lmaps/f/dd;

    new-instance v0, Lmaps/f/bs;

    const-string v1, "WEAK_EXPIRABLE_EVICTABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lmaps/f/bs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/f/dd;->l:Lmaps/f/dd;

    const/16 v0, 0xc

    new-array v0, v0, [Lmaps/f/dd;

    sget-object v1, Lmaps/f/dd;->a:Lmaps/f/dd;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/f/dd;->b:Lmaps/f/dd;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/f/dd;->c:Lmaps/f/dd;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/f/dd;->d:Lmaps/f/dd;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/f/dd;->e:Lmaps/f/dd;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/f/dd;->f:Lmaps/f/dd;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/f/dd;->g:Lmaps/f/dd;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/f/dd;->h:Lmaps/f/dd;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lmaps/f/dd;->i:Lmaps/f/dd;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lmaps/f/dd;->j:Lmaps/f/dd;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lmaps/f/dd;->k:Lmaps/f/dd;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lmaps/f/dd;->l:Lmaps/f/dd;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/f/dd;->n:[Lmaps/f/dd;

    new-array v0, v6, [[Lmaps/f/dd;

    new-array v1, v7, [Lmaps/f/dd;

    sget-object v2, Lmaps/f/dd;->a:Lmaps/f/dd;

    aput-object v2, v1, v3

    sget-object v2, Lmaps/f/dd;->b:Lmaps/f/dd;

    aput-object v2, v1, v4

    sget-object v2, Lmaps/f/dd;->c:Lmaps/f/dd;

    aput-object v2, v1, v5

    sget-object v2, Lmaps/f/dd;->d:Lmaps/f/dd;

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v7, [Lmaps/f/dd;

    sget-object v2, Lmaps/f/dd;->e:Lmaps/f/dd;

    aput-object v2, v1, v3

    sget-object v2, Lmaps/f/dd;->f:Lmaps/f/dd;

    aput-object v2, v1, v4

    sget-object v2, Lmaps/f/dd;->g:Lmaps/f/dd;

    aput-object v2, v1, v5

    sget-object v2, Lmaps/f/dd;->h:Lmaps/f/dd;

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Lmaps/f/dd;

    sget-object v2, Lmaps/f/dd;->i:Lmaps/f/dd;

    aput-object v2, v1, v3

    sget-object v2, Lmaps/f/dd;->j:Lmaps/f/dd;

    aput-object v2, v1, v4

    sget-object v2, Lmaps/f/dd;->k:Lmaps/f/dd;

    aput-object v2, v1, v5

    sget-object v2, Lmaps/f/dd;->l:Lmaps/f/dd;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    sput-object v0, Lmaps/f/dd;->m:[[Lmaps/f/dd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILmaps/f/ep;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/f/dd;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lmaps/f/cx;ZZ)Lmaps/f/dd;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    sget-object v1, Lmaps/f/dd;->m:[[Lmaps/f/dd;

    invoke-virtual {p0}, Lmaps/f/cx;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/f/dd;
    .locals 1

    const-class v0, Lmaps/f/dd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/f/dd;

    return-object v0
.end method

.method public static values()[Lmaps/f/dd;
    .locals 1

    sget-object v0, Lmaps/f/dd;->n:[Lmaps/f/dd;

    invoke-virtual {v0}, [Lmaps/f/dd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/f/dd;

    return-object v0
.end method


# virtual methods
.method abstract a(Lmaps/f/bm;Ljava/lang/Object;ILmaps/f/fm;)Lmaps/f/fm;
.end method

.method a(Lmaps/f/bm;Lmaps/f/fm;Lmaps/f/fm;)Lmaps/f/fm;
    .locals 2

    invoke-interface {p2}, Lmaps/f/fm;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lmaps/f/fm;->c()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lmaps/f/dd;->a(Lmaps/f/bm;Ljava/lang/Object;ILmaps/f/fm;)Lmaps/f/fm;

    move-result-object v0

    return-object v0
.end method

.method a(Lmaps/f/fm;Lmaps/f/fm;)V
    .locals 2

    invoke-interface {p1}, Lmaps/f/fm;->e()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lmaps/f/fm;->a(J)V

    invoke-interface {p1}, Lmaps/f/fm;->g()Lmaps/f/fm;

    move-result-object v0

    invoke-static {v0, p2}, Lmaps/f/ea;->a(Lmaps/f/fm;Lmaps/f/fm;)V

    invoke-interface {p1}, Lmaps/f/fm;->f()Lmaps/f/fm;

    move-result-object v0

    invoke-static {p2, v0}, Lmaps/f/ea;->a(Lmaps/f/fm;Lmaps/f/fm;)V

    invoke-static {p1}, Lmaps/f/ea;->d(Lmaps/f/fm;)V

    return-void
.end method

.method b(Lmaps/f/fm;Lmaps/f/fm;)V
    .locals 1

    invoke-interface {p1}, Lmaps/f/fm;->i()Lmaps/f/fm;

    move-result-object v0

    invoke-static {v0, p2}, Lmaps/f/ea;->b(Lmaps/f/fm;Lmaps/f/fm;)V

    invoke-interface {p1}, Lmaps/f/fm;->h()Lmaps/f/fm;

    move-result-object v0

    invoke-static {p2, v0}, Lmaps/f/ea;->b(Lmaps/f/fm;Lmaps/f/fm;)V

    invoke-static {p1}, Lmaps/f/ea;->e(Lmaps/f/fm;)V

    return-void
.end method
