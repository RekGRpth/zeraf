.class final Lmaps/f/ad;
.super Lmaps/f/fs;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final transient a:[Lmaps/f/fw;

.field private final transient b:[Lmaps/f/fw;

.field private final transient c:I

.field private final transient d:I

.field private transient e:Lmaps/f/bd;

.field private transient f:Lmaps/f/bd;

.field private transient g:Lmaps/f/ak;


# direct methods
.method varargs constructor <init>([Ljava/util/Map$Entry;)V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/f/fs;-><init>()V

    array-length v6, p1

    invoke-direct {p0, v6}, Lmaps/f/ad;->b(I)[Lmaps/f/fw;

    move-result-object v0

    iput-object v0, p0, Lmaps/f/ad;->a:[Lmaps/f/fw;

    invoke-static {v6}, Lmaps/f/ad;->a(I)I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/f/ad;->b(I)[Lmaps/f/fw;

    move-result-object v3

    iput-object v3, p0, Lmaps/f/ad;->b:[Lmaps/f/fw;

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/f/ad;->c:I

    move v4, v2

    move v0, v2

    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v3, p1, v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->hashCode()I

    move-result v8

    add-int v5, v0, v8

    invoke-static {v8}, Lmaps/f/dk;->a(I)I

    move-result v0

    iget v8, p0, Lmaps/f/ad;->c:I

    and-int/2addr v8, v0

    iget-object v0, p0, Lmaps/f/ad;->b:[Lmaps/f/fw;

    aget-object v0, v0, v8

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v7, v3, v0}, Lmaps/f/ad;->a(Ljava/lang/Object;Ljava/lang/Object;Lmaps/f/fw;)Lmaps/f/fw;

    move-result-object v3

    iget-object v9, p0, Lmaps/f/ad;->b:[Lmaps/f/fw;

    aput-object v3, v9, v8

    iget-object v8, p0, Lmaps/f/ad;->a:[Lmaps/f/fw;

    aput-object v3, v8, v4

    move-object v3, v0

    :goto_1
    if-eqz v3, :cond_1

    invoke-interface {v3}, Lmaps/f/fw;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_2
    const-string v8, "duplicate key: %s"

    new-array v9, v1, [Ljava/lang/Object;

    aput-object v7, v9, v2

    invoke-static {v0, v8, v9}, Lmaps/ap/q;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v3}, Lmaps/f/fw;->a()Lmaps/f/fw;

    move-result-object v0

    move-object v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v0, v5

    goto :goto_0

    :cond_2
    iput v0, p0, Lmaps/f/ad;->d:I

    return-void
.end method

.method private static a(I)I
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v3, v0, 0x1

    if-lez v3, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "table too large: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v0, v4, v1}, Lmaps/ap/q;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    return v3

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Lmaps/f/fw;)Lmaps/f/fw;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Lmaps/f/ds;

    invoke-direct {v0, p0, p1}, Lmaps/f/ds;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/f/ch;

    invoke-direct {v0, p0, p1, p2}, Lmaps/f/ch;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lmaps/f/fw;)V

    goto :goto_0
.end method

.method static synthetic a(Lmaps/f/ad;)[Lmaps/f/fw;
    .locals 1

    iget-object v0, p0, Lmaps/f/ad;->a:[Lmaps/f/fw;

    return-object v0
.end method

.method static synthetic b(Lmaps/f/ad;)I
    .locals 1

    iget v0, p0, Lmaps/f/ad;->d:I

    return v0
.end method

.method private b(I)[Lmaps/f/fw;
    .locals 1

    new-array v0, p1, [Lmaps/f/fw;

    return-object v0
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lmaps/f/bd;
    .locals 1

    iget-object v0, p0, Lmaps/f/ad;->e:Lmaps/f/bd;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/f/ca;

    invoke-direct {v0, p0}, Lmaps/f/ca;-><init>(Lmaps/f/ad;)V

    iput-object v0, p0, Lmaps/f/ad;->e:Lmaps/f/bd;

    :cond_0
    return-object v0
.end method

.method public c()Lmaps/f/bd;
    .locals 1

    iget-object v0, p0, Lmaps/f/ad;->f:Lmaps/f/bd;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/f/ao;

    invoke-direct {v0, p0}, Lmaps/f/ao;-><init>(Lmaps/f/ad;)V

    iput-object v0, p0, Lmaps/f/ad;->f:Lmaps/f/bd;

    :cond_0
    return-object v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lmaps/f/ad;->a:[Lmaps/f/fw;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public d()Lmaps/f/ak;
    .locals 1

    iget-object v0, p0, Lmaps/f/ad;->g:Lmaps/f/ak;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/f/ey;

    invoke-direct {v0, p0}, Lmaps/f/ey;-><init>(Lmaps/f/ad;)V

    iput-object v0, p0, Lmaps/f/ad;->g:Lmaps/f/ak;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic entrySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ad;->b()Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Lmaps/f/dk;->a(I)I

    move-result v1

    iget v2, p0, Lmaps/f/ad;->c:I

    and-int/2addr v1, v2

    iget-object v2, p0, Lmaps/f/ad;->b:[Lmaps/f/fw;

    aget-object v1, v2, v1

    :goto_1
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lmaps/f/fw;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Lmaps/f/fw;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Lmaps/f/fw;->a()Lmaps/f/fw;

    move-result-object v1

    goto :goto_1
.end method

.method public isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ad;->c()Lmaps/f/bd;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lmaps/f/ad;->a:[Lmaps/f/fw;

    array-length v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lmaps/f/ad;->size()I

    move-result v0

    invoke-static {v0}, Lmaps/f/ct;->a(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lmaps/f/ct;->a:Lmaps/ap/j;

    iget-object v2, p0, Lmaps/f/ad;->a:[Lmaps/f/fw;

    invoke-virtual {v1, v0, v2}, Lmaps/ap/j;->a(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic values()Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ad;->d()Lmaps/f/ak;

    move-result-object v0

    return-object v0
.end method
