.class final Lmaps/f/aq;
.super Lmaps/f/en;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final a:Lmaps/ap/i;

.field final b:Lmaps/f/en;


# direct methods
.method constructor <init>(Lmaps/ap/i;Lmaps/f/en;)V
    .locals 1

    invoke-direct {p0}, Lmaps/f/en;-><init>()V

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ap/i;

    iput-object v0, p0, Lmaps/f/aq;->a:Lmaps/ap/i;

    invoke-static {p2}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/f/en;

    iput-object v0, p0, Lmaps/f/aq;->b:Lmaps/f/en;

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Lmaps/f/aq;->b:Lmaps/f/en;

    iget-object v1, p0, Lmaps/f/aq;->a:Lmaps/ap/i;

    invoke-interface {v1, p1}, Lmaps/ap/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lmaps/f/aq;->a:Lmaps/ap/i;

    invoke-interface {v2, p2}, Lmaps/ap/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/f/en;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/f/aq;

    if-eqz v2, :cond_3

    check-cast p1, Lmaps/f/aq;

    iget-object v2, p0, Lmaps/f/aq;->a:Lmaps/ap/i;

    iget-object v3, p1, Lmaps/f/aq;->a:Lmaps/ap/i;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmaps/f/aq;->b:Lmaps/f/en;

    iget-object v3, p1, Lmaps/f/aq;->b:Lmaps/f/en;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/f/aq;->a:Lmaps/ap/i;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/f/aq;->b:Lmaps/f/en;

    aput-object v2, v0, v1

    invoke-static {v0}, Lmaps/ap/e;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmaps/f/aq;->b:Lmaps/f/en;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onResultOf("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/f/aq;->a:Lmaps/ap/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
