.class Lmaps/f/c;
.super Lmaps/f/r;


# instance fields
.field final synthetic a:Lmaps/f/bx;


# direct methods
.method constructor <init>(Lmaps/f/bx;)V
    .locals 0

    iput-object p1, p0, Lmaps/f/c;->a:Lmaps/f/bx;

    invoke-direct {p0}, Lmaps/f/r;-><init>()V

    return-void
.end method


# virtual methods
.method a()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/f/c;->a:Lmaps/f/bx;

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/f/c;->a:Lmaps/f/bx;

    iget-object v0, v0, Lmaps/f/bx;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/f/ct;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lmaps/f/ay;

    iget-object v1, p0, Lmaps/f/c;->a:Lmaps/f/bx;

    invoke-direct {v0, v1}, Lmaps/f/ay;-><init>(Lmaps/f/bx;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p0, p1}, Lmaps/f/c;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    iget-object v0, p0, Lmaps/f/c;->a:Lmaps/f/bx;

    iget-object v0, v0, Lmaps/f/bx;->c:Lmaps/f/cd;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/f/cd;->a(Lmaps/f/cd;Ljava/lang/Object;)I

    const/4 v0, 0x1

    goto :goto_0
.end method
