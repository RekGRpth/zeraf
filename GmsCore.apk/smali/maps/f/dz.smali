.class public abstract Lmaps/f/dz;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Lmaps/f/at;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final transient b:Lmaps/f/fs;

.field final transient c:I


# direct methods
.method constructor <init>(Lmaps/f/fs;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/f/dz;->b:Lmaps/f/fs;

    iput p2, p0, Lmaps/f/dz;->c:I

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/f/dz;->c(Ljava/lang/Object;)Lmaps/f/ak;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmaps/f/dz;->c:I

    return v0
.end method

.method public abstract c(Ljava/lang/Object;)Lmaps/f/ak;
.end method

.method public d()Z
    .locals 1

    iget v0, p0, Lmaps/f/dz;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic e()Ljava/util/Map;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/dz;->g()Lmaps/f/fs;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lmaps/f/at;

    if-eqz v0, :cond_0

    check-cast p1, Lmaps/f/at;

    iget-object v0, p0, Lmaps/f/dz;->b:Lmaps/f/fs;

    invoke-interface {p1}, Lmaps/f/at;->e()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/f/fs;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f()Z
    .locals 1

    iget-object v0, p0, Lmaps/f/dz;->b:Lmaps/f/fs;

    invoke-virtual {v0}, Lmaps/f/fs;->a()Z

    move-result v0

    return v0
.end method

.method public g()Lmaps/f/fs;
    .locals 1

    iget-object v0, p0, Lmaps/f/dz;->b:Lmaps/f/fs;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/f/dz;->b:Lmaps/f/fs;

    invoke-virtual {v0}, Lmaps/f/fs;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/f/dz;->b:Lmaps/f/fs;

    invoke-virtual {v0}, Lmaps/f/fs;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
