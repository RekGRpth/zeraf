.class final Lmaps/f/co;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Comparator;[Ljava/lang/Object;)Ljava/util/Collection;
    .locals 4

    const/4 v0, 0x1

    array-length v1, p1

    if-nez v1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    move v1, v0

    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    aget-object v2, p1, v0

    add-int/lit8 v3, v1, -0x1

    aget-object v3, p1, v3

    invoke-interface {p0, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-eqz v2, :cond_1

    add-int/lit8 v2, v1, 0x1

    aget-object v3, p1, v0

    aput-object v3, p1, v1

    move v1, v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    array-length v0, p1

    if-ge v1, v0, :cond_3

    invoke-static {p1, v1}, Lmaps/f/bb;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    :cond_3
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z
    .locals 1

    invoke-static {p0}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v0, p1, Ljava/util/SortedSet;

    if-eqz v0, :cond_1

    check-cast p1, Ljava/util/SortedSet;

    invoke-interface {p1}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/f/en;->b()Lmaps/f/en;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_1
    instance-of v0, p1, Lmaps/f/fk;

    if-eqz v0, :cond_2

    check-cast p1, Lmaps/f/fk;

    invoke-interface {p1}, Lmaps/f/fk;->comparator()Ljava/util/Comparator;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/util/Comparator;Ljava/lang/Iterable;)Ljava/util/Collection;
    .locals 2

    instance-of v0, p1, Lmaps/f/ga;

    if-eqz v0, :cond_3

    check-cast p1, Lmaps/f/ga;

    invoke-interface {p1}, Lmaps/f/ga;->a()Ljava/util/Set;

    move-result-object v0

    :goto_0
    instance-of v1, v0, Ljava/util/Set;

    if-eqz v1, :cond_1

    invoke-static {p0, v0}, Lmaps/f/co;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, Ljava/util/Set;

    :goto_1
    return-object v0

    :cond_0
    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1

    :cond_1
    invoke-static {v0}, Lmaps/f/fa;->c(Ljava/lang/Iterable;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    invoke-static {p0, v0}, Lmaps/f/co;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v1, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    :cond_2
    invoke-static {p0, v1}, Lmaps/f/co;->a(Ljava/util/Comparator;[Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, p1

    goto :goto_0
.end method
