.class final Lmaps/f/ec;
.super Lmaps/f/bi;


# instance fields
.field private final transient c:Lmaps/f/ef;


# direct methods
.method constructor <init>(Lmaps/f/ef;Ljava/util/Comparator;)V
    .locals 1

    invoke-direct {p0, p2}, Lmaps/f/bi;-><init>(Ljava/util/Comparator;)V

    iput-object p1, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-virtual {p1}, Lmaps/f/ef;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/ap/q;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(II)Lmaps/f/bi;
    .locals 3

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lmaps/f/ec;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    if-ge p1, p2, :cond_1

    new-instance v0, Lmaps/f/ec;

    iget-object v1, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-virtual {v1, p1, p2}, Lmaps/f/ef;->a(II)Lmaps/f/ef;

    move-result-object v1

    iget-object v2, p0, Lmaps/f/ec;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lmaps/f/ec;-><init>(Lmaps/f/ef;Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/f/ec;->a:Ljava/util/Comparator;

    invoke-static {v0}, Lmaps/f/ec;->a(Ljava/util/Comparator;)Lmaps/f/bi;

    move-result-object p0

    goto :goto_0
.end method

.method private e(Ljava/lang/Object;)I
    .locals 2

    iget-object v0, p0, Lmaps/f/ec;->a:Ljava/util/Comparator;

    iget-object v1, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-static {v1, p1, v0}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    return v0
.end method


# virtual methods
.method b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lmaps/f/bi;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lmaps/f/ec;->d(Ljava/lang/Object;Z)Lmaps/f/bi;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lmaps/f/bi;->c(Ljava/lang/Object;Z)Lmaps/f/bi;

    move-result-object v0

    return-object v0
.end method

.method c(Ljava/lang/Object;Z)Lmaps/f/bi;
    .locals 5

    if-eqz p2, :cond_0

    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/f/ec;->comparator()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lmaps/f/as;->d:Lmaps/f/as;

    sget-object v4, Lmaps/f/s;->b:Lmaps/f/s;

    invoke-static {v0, v1, v2, v3, v4}, Lmaps/f/bw;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lmaps/f/as;Lmaps/f/s;)I

    move-result v0

    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lmaps/f/ec;->a(II)Lmaps/f/bi;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/f/ec;->comparator()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lmaps/f/as;->c:Lmaps/f/as;

    sget-object v4, Lmaps/f/s;->b:Lmaps/f/s;

    invoke-static {v0, v1, v2, v3, v4}, Lmaps/f/bw;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lmaps/f/as;Lmaps/f/s;)I

    move-result v0

    goto :goto_0
.end method

.method c()Z
    .locals 1

    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-virtual {v0}, Lmaps/f/ef;->c()Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lmaps/f/ec;->e(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lmaps/f/ec;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/f/co;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-gt v0, v1, :cond_1

    :cond_0
    invoke-super {p0, p1}, Lmaps/f/bi;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lmaps/f/ec;->i_()Lmaps/f/fr;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    :cond_2
    :goto_1
    :try_start_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v5, v0}, Lmaps/f/ec;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_1

    :cond_4
    if-lez v5, :cond_2

    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_0

    :catch_1
    move-exception v0

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method d(Ljava/lang/Object;)I
    .locals 5

    const/4 v1, -0x1

    if-nez p1, :cond_0

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-virtual {p0}, Lmaps/f/ec;->comparator()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lmaps/f/as;->a:Lmaps/f/as;

    sget-object v4, Lmaps/f/s;->c:Lmaps/f/s;

    invoke-static {v0, p1, v2, v3, v4}, Lmaps/f/bw;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lmaps/f/as;Lmaps/f/s;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-ltz v0, :cond_1

    iget-object v2, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-virtual {v2, v0}, Lmaps/f/ef;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method d(Ljava/lang/Object;Z)Lmaps/f/bi;
    .locals 5

    if-eqz p2, :cond_0

    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/f/ec;->comparator()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lmaps/f/as;->c:Lmaps/f/as;

    sget-object v4, Lmaps/f/s;->b:Lmaps/f/s;

    invoke-static {v0, v1, v2, v3, v4}, Lmaps/f/bw;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lmaps/f/as;Lmaps/f/s;)I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lmaps/f/ec;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lmaps/f/ec;->a(II)Lmaps/f/bi;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/f/ec;->comparator()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lmaps/f/as;->d:Lmaps/f/as;

    sget-object v4, Lmaps/f/s;->b:Lmaps/f/s;

    invoke-static {v0, v1, v2, v3, v4}, Lmaps/f/bw;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lmaps/f/as;Lmaps/f/s;)I

    move-result v0

    goto :goto_0
.end method

.method e()Lmaps/f/ef;
    .locals 2

    new-instance v0, Lmaps/f/ai;

    iget-object v1, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-direct {v0, p0, v1}, Lmaps/f/ai;-><init>(Lmaps/f/bi;Lmaps/f/ef;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljava/util/Set;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0}, Lmaps/f/ec;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lmaps/f/ec;->a:Ljava/util/Comparator;

    invoke-static {v2, p1}, Lmaps/f/co;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :try_start_0
    invoke-virtual {p0}, Lmaps/f/ec;->i_()Lmaps/f/fr;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {p0, v4, v5}, Lmaps/f/ec;->a(Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-eqz v4, :cond_4

    :cond_5
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {p0, p1}, Lmaps/f/ec;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method

.method public first()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/f/ef;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public i_()Lmaps/f/fr;
    .locals 1

    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-virtual {v0}, Lmaps/f/ef;->i_()Lmaps/f/fr;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/f/ec;->i_()Lmaps/f/fr;

    move-result-object v0

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-virtual {p0}, Lmaps/f/ec;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lmaps/f/ef;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-virtual {v0}, Lmaps/f/ef;->size()I

    move-result v0

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-virtual {v0}, Lmaps/f/ef;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/f/ec;->c:Lmaps/f/ef;

    invoke-virtual {v0, p1}, Lmaps/f/ef;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
