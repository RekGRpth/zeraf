.class public Lmaps/e/r;
.super Ljava/lang/Object;


# instance fields
.field private a:F

.field private b:D

.field private c:F

.field private d:Landroid/os/Bundle;

.field private e:D

.field private f:D

.field private g:Ljava/lang/String;

.field private h:F

.field private i:J

.field private j:J

.field private k:Lmaps/t/bb;

.field private l:Lmaps/bl/a;

.field private m:Lmaps/e/w;

.field private n:Lmaps/e/o;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/e/r;->o:Z

    iput-boolean v0, p0, Lmaps/e/r;->p:Z

    iput-boolean v0, p0, Lmaps/e/r;->q:Z

    iput-boolean v0, p0, Lmaps/e/r;->r:Z

    iput-boolean v0, p0, Lmaps/e/r;->s:Z

    iput-boolean v0, p0, Lmaps/e/r;->t:Z

    return-void
.end method

.method static synthetic a(Lmaps/e/r;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/r;->g:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/location/Location;)V
    .locals 4

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/e/r;->a(F)Lmaps/e/r;

    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lmaps/e/r;->a(D)Lmaps/e/r;

    :cond_1
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/e/r;->b(F)Lmaps/e/r;

    :cond_2
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lmaps/e/r;->a(DD)Lmaps/e/r;

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/e/r;->a(Ljava/lang/String;)Lmaps/e/r;

    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/e/r;->c(F)Lmaps/e/r;

    :cond_3
    return-void
.end method

.method static synthetic b(Lmaps/e/r;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/r;->o:Z

    return v0
.end method

.method static synthetic c(Lmaps/e/r;)F
    .locals 1

    iget v0, p0, Lmaps/e/r;->a:F

    return v0
.end method

.method static synthetic d(Lmaps/e/r;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/r;->p:Z

    return v0
.end method

.method static synthetic e(Lmaps/e/r;)D
    .locals 2

    iget-wide v0, p0, Lmaps/e/r;->b:D

    return-wide v0
.end method

.method static synthetic f(Lmaps/e/r;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/r;->q:Z

    return v0
.end method

.method static synthetic g(Lmaps/e/r;)F
    .locals 1

    iget v0, p0, Lmaps/e/r;->c:F

    return v0
.end method

.method static synthetic h(Lmaps/e/r;)D
    .locals 2

    iget-wide v0, p0, Lmaps/e/r;->e:D

    return-wide v0
.end method

.method static synthetic i(Lmaps/e/r;)D
    .locals 2

    iget-wide v0, p0, Lmaps/e/r;->f:D

    return-wide v0
.end method

.method static synthetic j(Lmaps/e/r;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/r;->r:Z

    return v0
.end method

.method static synthetic k(Lmaps/e/r;)F
    .locals 1

    iget v0, p0, Lmaps/e/r;->h:F

    return v0
.end method

.method static synthetic l(Lmaps/e/r;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/r;->s:Z

    return v0
.end method

.method static synthetic m(Lmaps/e/r;)J
    .locals 2

    iget-wide v0, p0, Lmaps/e/r;->i:J

    return-wide v0
.end method

.method static synthetic n(Lmaps/e/r;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/r;->t:Z

    return v0
.end method

.method static synthetic o(Lmaps/e/r;)J
    .locals 2

    iget-wide v0, p0, Lmaps/e/r;->j:J

    return-wide v0
.end method

.method static synthetic p(Lmaps/e/r;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lmaps/e/r;->d:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic q(Lmaps/e/r;)Lmaps/bl/a;
    .locals 1

    iget-object v0, p0, Lmaps/e/r;->l:Lmaps/bl/a;

    return-object v0
.end method

.method static synthetic r(Lmaps/e/r;)Lmaps/t/bb;
    .locals 1

    iget-object v0, p0, Lmaps/e/r;->k:Lmaps/t/bb;

    return-object v0
.end method

.method static synthetic s(Lmaps/e/r;)Lmaps/e/w;
    .locals 1

    iget-object v0, p0, Lmaps/e/r;->m:Lmaps/e/w;

    return-object v0
.end method

.method static synthetic t(Lmaps/e/r;)Lmaps/e/o;
    .locals 1

    iget-object v0, p0, Lmaps/e/r;->n:Lmaps/e/o;

    return-object v0
.end method


# virtual methods
.method public a()Lmaps/e/r;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/e/r;->o:Z

    return-object p0
.end method

.method public a(D)Lmaps/e/r;
    .locals 1

    iput-wide p1, p0, Lmaps/e/r;->b:D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/r;->p:Z

    return-object p0
.end method

.method public a(DD)Lmaps/e/r;
    .locals 5

    const-wide v3, 0x412e848000000000L

    iput-wide p1, p0, Lmaps/e/r;->e:D

    iput-wide p3, p0, Lmaps/e/r;->f:D

    new-instance v0, Lmaps/bl/a;

    mul-double v1, p1, v3

    double-to-int v1, v1

    mul-double v2, p3, v3

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lmaps/bl/a;-><init>(II)V

    iput-object v0, p0, Lmaps/e/r;->l:Lmaps/bl/a;

    return-object p0
.end method

.method public a(F)Lmaps/e/r;
    .locals 1

    iput p1, p0, Lmaps/e/r;->a:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/r;->o:Z

    return-object p0
.end method

.method public a(J)Lmaps/e/r;
    .locals 1

    iput-wide p1, p0, Lmaps/e/r;->i:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/r;->s:Z

    return-object p0
.end method

.method public a(Landroid/location/Location;)Lmaps/e/r;
    .locals 2

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-direct {p0, p1}, Lmaps/e/r;->b(Landroid/location/Location;)V

    invoke-virtual {p1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/e/r;->a(Landroid/os/Bundle;)Lmaps/e/r;

    invoke-static {p1}, Lmaps/e/b;->a(Landroid/location/Location;)Lmaps/t/bb;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/e/r;->a(Lmaps/t/bb;)Lmaps/e/r;

    instance-of v0, p1, Lmaps/e/a;

    if-eqz v0, :cond_4

    check-cast p1, Lmaps/e/a;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/r;->t:Z

    invoke-static {p1}, Lmaps/e/a;->b(Lmaps/e/a;)J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/e/r;->j:J

    invoke-virtual {p1}, Lmaps/e/a;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/e/a;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lmaps/e/r;->a(J)Lmaps/e/r;

    :cond_2
    invoke-static {p1}, Lmaps/e/a;->c(Lmaps/e/a;)Lmaps/e/w;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v0, Lmaps/e/w;

    invoke-static {p1}, Lmaps/e/a;->c(Lmaps/e/a;)Lmaps/e/w;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/e/w;-><init>(Lmaps/e/w;)V

    iput-object v0, p0, Lmaps/e/r;->m:Lmaps/e/w;

    :cond_3
    invoke-static {p1}, Lmaps/e/a;->d(Lmaps/e/a;)Lmaps/e/o;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/e/o;

    invoke-static {p1}, Lmaps/e/a;->d(Lmaps/e/a;)Lmaps/e/o;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/e/o;-><init>(Lmaps/e/o;)V

    iput-object v0, p0, Lmaps/e/r;->n:Lmaps/e/o;

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lmaps/e/r;->a(J)Lmaps/e/r;

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)Lmaps/e/r;
    .locals 0

    iput-object p1, p0, Lmaps/e/r;->d:Landroid/os/Bundle;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lmaps/e/r;
    .locals 0

    iput-object p1, p0, Lmaps/e/r;->g:Ljava/lang/String;

    return-object p0
.end method

.method public a(Lmaps/bl/a;)Lmaps/e/r;
    .locals 4

    const-wide v2, 0x412e848000000000L

    iput-object p1, p0, Lmaps/e/r;->l:Lmaps/bl/a;

    invoke-virtual {p1}, Lmaps/bl/a;->a()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lmaps/e/r;->e:D

    invoke-virtual {p1}, Lmaps/bl/a;->b()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lmaps/e/r;->f:D

    return-object p0
.end method

.method public a(Lmaps/t/bb;)Lmaps/e/r;
    .locals 1

    iput-object p1, p0, Lmaps/e/r;->k:Lmaps/t/bb;

    if-eqz p1, :cond_0

    invoke-static {p1}, Lmaps/h/i;->a(Lmaps/t/bb;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/e/r;->a(Landroid/os/Bundle;)Lmaps/e/r;

    :cond_0
    return-object p0
.end method

.method public b()Lmaps/e/r;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/e/r;->s:Z

    return-object p0
.end method

.method public b(F)Lmaps/e/r;
    .locals 1

    iput p1, p0, Lmaps/e/r;->c:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/r;->q:Z

    return-object p0
.end method

.method public c()Lmaps/e/a;
    .locals 2

    iget-object v0, p0, Lmaps/e/r;->l:Lmaps/bl/a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lmaps/e/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/e/a;-><init>(Lmaps/e/r;Lmaps/e/q;)V

    return-object v0
.end method

.method public c(F)Lmaps/e/r;
    .locals 1

    iput p1, p0, Lmaps/e/r;->h:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/r;->r:Z

    return-object p0
.end method
