.class public Lmaps/e/m;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/e/ad;


# instance fields
.field private a:Lmaps/ae/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/e/m;-><init>(Lmaps/ae/c;)V

    return-void
.end method

.method constructor <init>(Lmaps/ae/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/e/m;->a:Lmaps/ae/c;

    return-void
.end method

.method private static c(Lmaps/e/aa;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LastLocation_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lmaps/e/aa;)V
    .locals 5

    instance-of v0, p1, Lmaps/e/l;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lmaps/e/aa;->m()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lmaps/e/l;

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p1}, Lmaps/e/l;->n()Lmaps/e/a;

    move-result-object v2

    :try_start_0
    invoke-virtual {v2}, Lmaps/e/a;->getTime()J

    move-result-wide v3

    invoke-interface {v1, v3, v4}, Ljava/io/DataOutput;->writeLong(J)V

    invoke-virtual {v2}, Lmaps/e/a;->getAccuracy()F

    move-result v3

    float-to-int v3, v3

    invoke-interface {v1, v3}, Ljava/io/DataOutput;->writeInt(I)V

    invoke-virtual {v2}, Lmaps/e/a;->a()Lmaps/bl/a;

    move-result-object v2

    invoke-static {v2, v1}, Lmaps/bl/a;->a(Lmaps/bl/a;Ljava/io/DataOutput;)V

    iget-object v1, p0, Lmaps/e/m;->a:Lmaps/ae/c;

    invoke-virtual {v1}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v1

    invoke-static {p1}, Lmaps/e/m;->c(Lmaps/e/aa;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lmaps/bj/b;->a(Ljava/lang/String;[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->h:Z

    if-eqz v1, :cond_0

    const-class v1, Lmaps/e/m;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "I/O issue of location saving"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public b(Lmaps/e/aa;)V
    .locals 7

    instance-of v0, p1, Lmaps/e/l;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lmaps/e/aa;->m()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lmaps/e/l;

    iget-object v0, p0, Lmaps/e/m;->a:Lmaps/ae/c;

    invoke-virtual {v0}, Lmaps/ae/c;->k()Lmaps/ae/f;

    move-result-object v0

    invoke-static {p1}, Lmaps/e/m;->c(Lmaps/e/aa;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ae/f;->b(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v1

    iget-object v3, p0, Lmaps/e/m;->a:Lmaps/ae/c;

    invoke-virtual {v3}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v3

    invoke-interface {v3}, Lmaps/ae/d;->a()J

    move-result-wide v3

    sub-long/2addr v3, v1

    const-wide/32 v5, 0xafc80

    cmp-long v3, v3, v5

    if-gez v3, :cond_2

    invoke-interface {v0}, Ljava/io/DataInput;->readInt()I

    move-result v3

    invoke-static {v0}, Lmaps/bl/a;->a(Ljava/io/DataInput;)Lmaps/bl/a;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v4, Lmaps/e/r;

    invoke-direct {v4}, Lmaps/e/r;-><init>()V

    invoke-virtual {p1}, Lmaps/e/l;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmaps/e/r;->a(Ljava/lang/String;)Lmaps/e/r;

    move-result-object v4

    invoke-virtual {v4, v0}, Lmaps/e/r;->a(Lmaps/bl/a;)Lmaps/e/r;

    move-result-object v0

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Lmaps/e/r;->a(F)Lmaps/e/r;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lmaps/e/r;->a(J)Lmaps/e/r;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/e/r;->c()Lmaps/e/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmaps/e/l;->a(Lmaps/e/a;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    iget-object v0, p0, Lmaps/e/m;->a:Lmaps/ae/c;

    invoke-virtual {v0}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v0

    invoke-static {p1}, Lmaps/e/m;->c(Lmaps/e/aa;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lmaps/bj/b;->a(Ljava/lang/String;[B)Z

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->h:Z

    if-eqz v1, :cond_2

    const-class v1, Lmaps/e/f;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "I/O issue of location reading"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
