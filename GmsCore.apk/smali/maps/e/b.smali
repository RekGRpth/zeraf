.class public abstract Lmaps/e/b;
.super Lmaps/e/l;


# instance fields
.field protected final a:Landroid/location/LocationManager;

.field private final b:Lmaps/e/p;


# direct methods
.method protected constructor <init>(ZLmaps/ae/d;Landroid/location/LocationManager;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lmaps/e/l;-><init>(ZLmaps/ae/d;)V

    iput-object p3, p0, Lmaps/e/b;->a:Landroid/location/LocationManager;

    sget-boolean v0, Lmaps/bm/b;->i:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/e/p;

    invoke-direct {v0, p3}, Lmaps/e/p;-><init>(Landroid/location/LocationManager;)V

    :goto_0
    iput-object v0, p0, Lmaps/e/b;->b:Lmaps/e/p;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static a(Landroid/location/Location;)Lmaps/t/bb;
    .locals 5

    const/high16 v4, -0x80000000

    const/4 v0, 0x0

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    invoke-static {}, Lmaps/k/b;->k()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "levelId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "levelNumberE3"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v2, :cond_0

    invoke-static {v2}, Lmaps/t/bg;->b(Ljava/lang/String;)Lmaps/t/bg;

    move-result-object v3

    if-eqz v3, :cond_4

    if-ne v1, v4, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing level number for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-boolean v2, Lmaps/ae/h;->i:Z

    if-eqz v2, :cond_2

    const-string v2, "LOCATION"

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v2, "LOCATION"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v4}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    new-instance v0, Lmaps/t/bb;

    invoke-direct {v0, v3, v1}, Lmaps/t/bb;-><init>(Lmaps/t/bg;I)V

    goto :goto_0

    :cond_4
    sget-boolean v1, Lmaps/ae/h;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "LOCATION"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid feature id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lmaps/e/b;->b:Lmaps/e/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/b;->b:Lmaps/e/p;

    invoke-virtual {v0, p1}, Lmaps/e/p;->a(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/e/b;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/e/b;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lmaps/e/l;->a()V

    iget-object v0, p0, Lmaps/e/b;->b:Lmaps/e/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/b;->b:Lmaps/e/p;

    invoke-virtual {v0}, Lmaps/e/p;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/b;->b:Lmaps/e/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/b;->b:Lmaps/e/p;

    invoke-virtual {v0}, Lmaps/e/p;->b()V

    :cond_0
    invoke-super {p0}, Lmaps/e/l;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .locals 1

    invoke-virtual {p0}, Lmaps/e/b;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/e/b;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    :try_start_0
    const-string v0, "network"

    invoke-direct {p0, v0}, Lmaps/e/b;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->i:Z

    if-eqz v1, :cond_0

    const-string v1, "LOCATION"

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    :try_start_0
    const-string v0, "gps"

    invoke-direct {p0, v0}, Lmaps/e/b;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    sget-boolean v1, Lmaps/ae/h;->i:Z

    if-eqz v1, :cond_0

    const-string v1, "LOCATION"

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
