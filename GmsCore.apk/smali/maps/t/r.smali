.class public Lmaps/t/r;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/ca;


# instance fields
.field private a:I

.field private b:Lmaps/t/aa;


# direct methods
.method public constructor <init>(ILmaps/t/aa;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/t/r;->a:I

    iput-object p2, p0, Lmaps/t/r;->b:Lmaps/t/aa;

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/r;
    .locals 3

    invoke-virtual {p1}, Lmaps/t/cd;->a()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/t/r;

    invoke-virtual {p2}, Lmaps/t/bn;->a()I

    move-result v1

    invoke-static {}, Lmaps/t/aa;->a()Lmaps/t/aa;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/t/r;-><init>(ILmaps/t/aa;)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()Lmaps/t/v;
    .locals 1

    sget-object v0, Lmaps/t/v;->a:Lmaps/t/v;

    return-object v0
.end method

.method public h()Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/r;->b:Lmaps/t/aa;

    return-object v0
.end method

.method public i()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public j()[I
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0
.end method

.method public k()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
