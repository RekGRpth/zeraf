.class public Lmaps/t/n;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/ca;


# instance fields
.field private final a:I

.field private final b:Lmaps/t/ah;

.field private final c:Lmaps/t/bx;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lmaps/t/v;

.field private final g:Lmaps/t/aa;

.field private final h:I

.field private final i:[I

.field private final j:I

.field private final k:I

.field private final l:I

.field private m:F

.field private n:F

.field private final o:[Lmaps/t/y;

.field private final p:Lmaps/t/bh;

.field private final q:Lmaps/t/bh;

.field private final r:[Lmaps/t/g;

.field private final s:I

.field private final t:Ljava/lang/String;

.field private final u:Ljava/lang/String;

.field private final v:Lmaps/t/g;


# direct methods
.method public constructor <init>(ILmaps/t/ah;Lmaps/t/bx;Lmaps/t/v;[Lmaps/t/y;Lmaps/t/bh;Lmaps/t/bh;[Lmaps/t/g;Ljava/lang/String;Lmaps/t/aa;ILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Lmaps/t/g;[I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v1, -0x40800000

    iput v1, p0, Lmaps/t/n;->m:F

    const/high16 v1, -0x40800000

    iput v1, p0, Lmaps/t/n;->n:F

    iput p1, p0, Lmaps/t/n;->a:I

    iput-object p4, p0, Lmaps/t/n;->f:Lmaps/t/v;

    iput-object p5, p0, Lmaps/t/n;->o:[Lmaps/t/y;

    iput-object p6, p0, Lmaps/t/n;->p:Lmaps/t/bh;

    iput-object p7, p0, Lmaps/t/n;->q:Lmaps/t/bh;

    iput-object p8, p0, Lmaps/t/n;->r:[Lmaps/t/g;

    iput-object p9, p0, Lmaps/t/n;->e:Ljava/lang/String;

    iput-object p10, p0, Lmaps/t/n;->g:Lmaps/t/aa;

    iput p11, p0, Lmaps/t/n;->s:I

    iput-object p12, p0, Lmaps/t/n;->t:Ljava/lang/String;

    iput p13, p0, Lmaps/t/n;->h:I

    move/from16 v0, p14

    iput v0, p0, Lmaps/t/n;->j:I

    const/4 v1, -0x1

    move/from16 v0, p15

    if-ne v0, v1, :cond_0

    const/16 p15, 0x1e

    :cond_0
    move/from16 v0, p15

    iput v0, p0, Lmaps/t/n;->k:I

    move/from16 v0, p16

    iput v0, p0, Lmaps/t/n;->l:I

    move-object/from16 v0, p17

    iput-object v0, p0, Lmaps/t/n;->u:Ljava/lang/String;

    move-object/from16 v0, p18

    iput-object v0, p0, Lmaps/t/n;->d:Ljava/lang/String;

    move-object/from16 v0, p19

    iput-object v0, p0, Lmaps/t/n;->v:Lmaps/t/g;

    move-object/from16 v0, p20

    iput-object v0, p0, Lmaps/t/n;->i:[I

    iput-object p2, p0, Lmaps/t/n;->b:Lmaps/t/ah;

    iput-object p3, p0, Lmaps/t/n;->c:Lmaps/t/bx;

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/n;
    .locals 23

    invoke-static/range {p0 .. p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v7, v3, [Lmaps/t/y;

    invoke-virtual/range {p1 .. p1}, Lmaps/t/cd;->b()Lmaps/t/ah;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lmaps/t/cd;->a()I

    move-result v12

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    move-object/from16 v0, p0

    invoke-static {v0, v4, v12}, Lmaps/t/y;->a(Ljava/io/DataInput;Lmaps/t/ah;I)Lmaps/t/y;

    move-result-object v5

    aput-object v5, v7, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    aget-object v2, v7, v2

    invoke-virtual {v2}, Lmaps/t/y;->b()Lmaps/t/bx;

    move-result-object v5

    invoke-static/range {p0 .. p1}, Lmaps/t/aq;->a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/aq;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v14}, Lmaps/t/bh;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/aq;)Lmaps/t/bh;

    move-result-object v8

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v14}, Lmaps/t/bh;->a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/aq;)Lmaps/t/bh;

    move-result-object v9

    invoke-static/range {p0 .. p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v10, v3, [Lmaps/t/g;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lmaps/t/g;->a(Ljava/io/DataInput;I)Lmaps/t/g;

    move-result-object v6

    aput-object v6, v10, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v15

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v16

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v17

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readInt()I

    move-result v18

    const/4 v6, 0x0

    const/4 v2, 0x1

    move/from16 v0, v18

    invoke-static {v2, v0}, Lmaps/t/ab;->a(II)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static/range {p0 .. p0}, Lmaps/t/v;->a(Ljava/io/DataInput;)Lmaps/t/bs;

    move-result-object v6

    :cond_2
    :goto_2
    const/4 v11, 0x0

    invoke-static/range {v18 .. v18}, Lmaps/t/n;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/av/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v11

    :cond_3
    const/16 v19, 0x0

    invoke-static/range {v18 .. v18}, Lmaps/t/n;->c(I)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v19

    :cond_4
    invoke-static/range {v18 .. v18}, Lmaps/t/n;->b(I)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v20

    :goto_3
    invoke-virtual/range {p1 .. p1}, Lmaps/t/cd;->a()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_9

    invoke-static/range {v18 .. v18}, Lmaps/t/n;->d(I)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lmaps/t/g;->a(Ljava/io/DataInput;I)Lmaps/t/g;

    move-result-object v21

    :goto_4
    invoke-static/range {p0 .. p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v0, v3, [I

    move-object/from16 v22, v0

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v3, :cond_a

    invoke-static/range {p0 .. p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v12

    aput v12, v22, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_5
    const/4 v2, 0x2

    move/from16 v0, v18

    invoke-static {v2, v0}, Lmaps/t/ab;->a(II)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static/range {p0 .. p0}, Lmaps/t/v;->b(Ljava/io/DataInput;)Lmaps/t/bt;

    move-result-object v6

    goto :goto_2

    :cond_6
    invoke-virtual {v8}, Lmaps/t/bh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Lmaps/t/bh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_7

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v13, 0xa

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_7
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    goto :goto_3

    :cond_8
    sget-object v21, Lmaps/t/g;->a:Lmaps/t/g;

    goto :goto_4

    :cond_9
    sget-object v21, Lmaps/t/g;->a:Lmaps/t/g;

    goto :goto_4

    :cond_a
    new-instance v2, Lmaps/t/n;

    invoke-virtual/range {p2 .. p2}, Lmaps/t/bn;->a()I

    move-result v3

    invoke-virtual {v14}, Lmaps/t/aq;->a()Lmaps/t/aa;

    move-result-object v12

    invoke-virtual {v14}, Lmaps/t/aq;->c()I

    move-result v13

    invoke-virtual {v14}, Lmaps/t/aq;->b()Ljava/lang/String;

    move-result-object v14

    invoke-direct/range {v2 .. v22}, Lmaps/t/n;-><init>(ILmaps/t/ah;Lmaps/t/bx;Lmaps/t/v;[Lmaps/t/y;Lmaps/t/bh;Lmaps/t/bh;[Lmaps/t/g;Ljava/lang/String;Lmaps/t/aa;ILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Lmaps/t/g;[I)V

    return-object v2
.end method

.method public static a(Lmaps/t/ah;Lmaps/t/bx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lmaps/t/v;Ljava/lang/String;Lmaps/t/aa;II[I)Lmaps/t/n;
    .locals 21

    new-instance v0, Lmaps/t/y;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v1, p1

    invoke-direct/range {v0 .. v7}, Lmaps/t/y;-><init>(Lmaps/t/bx;IFLmaps/t/bx;FFF)V

    const/4 v1, 0x1

    new-array v13, v1, [Lmaps/t/y;

    const/4 v1, 0x0

    aput-object v0, v13, v1

    const/4 v10, 0x0

    const/4 v9, 0x0

    if-eqz p3, :cond_0

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lmaps/t/a;

    const/4 v1, 0x1

    const/4 v3, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p3

    invoke-direct/range {v0 .. v8}, Lmaps/t/a;-><init>(ILjava/lang/String;ILjava/lang/String;Lmaps/t/aa;ILjava/lang/String;F)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v10, Lmaps/t/bh;

    sget-object v0, Lmaps/t/au;->c:Lmaps/t/au;

    invoke-direct {v10, v11, v0}, Lmaps/t/bh;-><init>(Ljava/util/List;Lmaps/t/au;)V

    :cond_0
    if-eqz p2, :cond_4

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lmaps/t/a;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {}, Lmaps/t/aa;->a()Lmaps/t/aa;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "styleid"

    const/4 v8, 0x0

    move-object/from16 v4, p2

    invoke-direct/range {v0 .. v8}, Lmaps/t/a;-><init>(ILjava/lang/String;ILjava/lang/String;Lmaps/t/aa;ILjava/lang/String;F)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v10, :cond_2

    new-instance v6, Lmaps/t/bh;

    sget-object v0, Lmaps/t/au;->c:Lmaps/t/au;

    invoke-direct {v6, v11, v0}, Lmaps/t/bh;-><init>(Ljava/util/List;Lmaps/t/au;)V

    move-object v7, v9

    :goto_0
    if-nez v7, :cond_1

    new-instance v7, Lmaps/t/bh;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lmaps/t/au;->c:Lmaps/t/au;

    invoke-direct {v7, v0, v1}, Lmaps/t/bh;-><init>(Ljava/util/List;Lmaps/t/au;)V

    :cond_1
    new-instance v0, Lmaps/t/n;

    const/4 v1, -0x1

    const/4 v2, 0x0

    new-array v8, v2, [Lmaps/t/g;

    const/4 v11, 0x0

    const-string v12, "styleid"

    const/4 v14, 0x0

    const/16 v15, 0x14

    const/16 v17, 0x0

    if-nez p2, :cond_3

    const-string v18, ""

    :goto_1
    sget-object v19, Lmaps/t/g;->a:Lmaps/t/g;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p5

    move-object v5, v13

    move-object/from16 v9, p4

    move-object/from16 v10, p7

    move/from16 v13, p8

    move/from16 v16, p9

    move-object/from16 v20, p10

    invoke-direct/range {v0 .. v20}, Lmaps/t/n;-><init>(ILmaps/t/ah;Lmaps/t/bx;Lmaps/t/v;[Lmaps/t/y;Lmaps/t/bh;Lmaps/t/bh;[Lmaps/t/g;Ljava/lang/String;Lmaps/t/aa;ILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;Lmaps/t/g;[I)V

    return-object v0

    :cond_2
    new-instance v7, Lmaps/t/bh;

    sget-object v0, Lmaps/t/au;->c:Lmaps/t/au;

    invoke-direct {v7, v11, v0}, Lmaps/t/bh;-><init>(Ljava/util/List;Lmaps/t/au;)V

    move-object v6, v10

    goto :goto_0

    :cond_3
    move-object/from16 v18, p2

    goto :goto_1

    :cond_4
    move-object v7, v9

    move-object v6, v10

    goto :goto_0
.end method

.method private static a(I)Z
    .locals 1

    const/16 v0, 0x20

    invoke-static {v0, p0}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method private static b(I)Z
    .locals 1

    const/16 v0, 0x80

    invoke-static {v0, p0}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method private static c(I)Z
    .locals 1

    const/16 v0, 0x40

    invoke-static {v0, p0}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method private static d(I)Z
    .locals 1

    const/16 v0, 0x200

    invoke-static {v0, p0}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lmaps/t/n;->m:F

    return-void
.end method

.method public b()Lmaps/t/v;
    .locals 1

    iget-object v0, p0, Lmaps/t/n;->f:Lmaps/t/v;

    return-object v0
.end method

.method public b(F)V
    .locals 0

    iput p1, p0, Lmaps/t/n;->n:F

    return-void
.end method

.method public c()Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/t/n;->b:Lmaps/t/ah;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/n;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lmaps/t/n;->j:I

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lmaps/t/n;->k:I

    return v0
.end method

.method public g()F
    .locals 1

    iget v0, p0, Lmaps/t/n;->m:F

    return v0
.end method

.method public h()Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/n;->g:Lmaps/t/aa;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/t/n;->h:I

    return v0
.end method

.method public j()[I
    .locals 1

    iget-object v0, p0, Lmaps/t/n;->i:[I

    return-object v0
.end method

.method public k()I
    .locals 7

    const/4 v2, 0x0

    const/16 v0, 0x78

    iget-object v1, p0, Lmaps/t/n;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/t/n;->d:Ljava/lang/String;

    invoke-static {v1}, Lmaps/t/ab;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lmaps/t/n;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/t/n;->e:Ljava/lang/String;

    invoke-static {v1}, Lmaps/t/ab;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lmaps/t/n;->o:[Lmaps/t/y;

    if-eqz v1, :cond_2

    iget-object v5, p0, Lmaps/t/n;->o:[Lmaps/t/y;

    array-length v6, v5

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v6, :cond_3

    aget-object v4, v5, v3

    invoke-virtual {v4}, Lmaps/t/y;->d()I

    move-result v4

    add-int/2addr v4, v1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v4

    goto :goto_0

    :cond_2
    move v1, v2

    :cond_3
    iget-object v3, p0, Lmaps/t/n;->r:[Lmaps/t/g;

    if-eqz v3, :cond_4

    iget-object v5, p0, Lmaps/t/n;->r:[Lmaps/t/g;

    array-length v6, v5

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_4

    aget-object v4, v5, v3

    invoke-virtual {v4}, Lmaps/t/g;->d()I

    move-result v4

    add-int/2addr v4, v2

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_1

    :cond_4
    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/t/n;->p:Lmaps/t/bh;

    invoke-static {v2}, Lmaps/t/ab;->a(Lmaps/t/bh;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/t/n;->q:Lmaps/t/bh;

    invoke-static {v2}, Lmaps/t/ab;->a(Lmaps/t/bh;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/t/n;->f:Lmaps/t/v;

    invoke-static {v2}, Lmaps/t/ab;->a(Lmaps/t/v;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/t/n;->g:Lmaps/t/aa;

    invoke-static {v2}, Lmaps/t/ab;->a(Lmaps/t/aa;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/t/n;->t:Ljava/lang/String;

    invoke-static {v2}, Lmaps/t/ab;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/t/n;->u:Ljava/lang/String;

    invoke-static {v2}, Lmaps/t/ab;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public l()F
    .locals 1

    iget v0, p0, Lmaps/t/n;->n:F

    return v0
.end method

.method public m()[Lmaps/t/y;
    .locals 1

    iget-object v0, p0, Lmaps/t/n;->o:[Lmaps/t/y;

    return-object v0
.end method

.method public n()Lmaps/t/bh;
    .locals 1

    iget-object v0, p0, Lmaps/t/n;->p:Lmaps/t/bh;

    return-object v0
.end method

.method public o()Lmaps/t/bh;
    .locals 1

    iget-object v0, p0, Lmaps/t/n;->q:Lmaps/t/bh;

    return-object v0
.end method

.method public p()[Lmaps/t/g;
    .locals 1

    iget-object v0, p0, Lmaps/t/n;->r:[Lmaps/t/g;

    return-object v0
.end method

.method public q()Z
    .locals 2

    const/16 v0, 0x10

    iget v1, p0, Lmaps/t/n;->l:I

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method
