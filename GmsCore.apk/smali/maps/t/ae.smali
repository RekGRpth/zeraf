.class public Lmaps/t/ae;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/ca;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:[B

.field private final d:I

.field private final e:Lmaps/t/aa;

.field private final f:[I


# direct methods
.method public constructor <init>(II[BILmaps/t/aa;[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/t/ae;->a:I

    iput p2, p0, Lmaps/t/ae;->b:I

    iput-object p3, p0, Lmaps/t/ae;->c:[B

    iput p4, p0, Lmaps/t/ae;->d:I

    iput-object p5, p0, Lmaps/t/ae;->e:Lmaps/t/aa;

    iput-object p6, p0, Lmaps/t/ae;->f:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;Lmaps/t/bn;)Lmaps/t/ae;
    .locals 7

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    new-array v3, v0, [B

    invoke-interface {p0, v3}, Ljava/io/DataInput;->readFully([B)V

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v4

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    new-array v6, v1, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/t/ae;

    invoke-virtual {p2}, Lmaps/t/bn;->a()I

    move-result v1

    invoke-static {}, Lmaps/t/aa;->a()Lmaps/t/aa;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, Lmaps/t/ae;-><init>(II[BILmaps/t/aa;[I)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public b()Lmaps/t/v;
    .locals 1

    sget-object v0, Lmaps/t/v;->a:Lmaps/t/v;

    return-object v0
.end method

.method public c()[B
    .locals 1

    iget-object v0, p0, Lmaps/t/ae;->c:[B

    return-object v0
.end method

.method public h()Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/ae;->e:Lmaps/t/aa;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/t/ae;->d:I

    return v0
.end method

.method public j()[I
    .locals 1

    iget-object v0, p0, Lmaps/t/ae;->f:[I

    return-object v0
.end method

.method public k()I
    .locals 1

    iget-object v0, p0, Lmaps/t/ae;->c:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x24

    return v0
.end method
