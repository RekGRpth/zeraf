.class public Lmaps/t/aq;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/t/aa;

.field private final b:Ljava/lang/String;

.field private final c:I


# direct methods
.method public constructor <init>(Lmaps/t/aa;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/t/aq;->a:Lmaps/t/aa;

    iput-object p2, p0, Lmaps/t/aq;->b:Ljava/lang/String;

    iput p3, p0, Lmaps/t/aq;->c:I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/aq;
    .locals 5

    invoke-virtual {p1}, Lmaps/t/cd;->a()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    invoke-virtual {p1, v2}, Lmaps/t/cd;->b(I)Lmaps/t/br;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/t/br;->b()Lmaps/t/aa;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/t/br;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    move v4, v2

    move-object v2, v1

    move v1, v4

    :goto_1
    new-instance v3, Lmaps/t/aq;

    invoke-direct {v3, v2, v0, v1}, Lmaps/t/aq;-><init>(Lmaps/t/aa;Ljava/lang/String;I)V

    return-object v3

    :cond_0
    invoke-static {}, Lmaps/t/aa;->a()Lmaps/t/aa;

    move-result-object v1

    const-string v0, ""

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-virtual {p1, v0}, Lmaps/t/cd;->a(I)Lmaps/t/aa;

    move-result-object v2

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    invoke-virtual {p1, v1}, Lmaps/t/cd;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a()Lmaps/t/aa;
    .locals 1

    iget-object v0, p0, Lmaps/t/aq;->a:Lmaps/t/aa;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/t/aq;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmaps/t/aq;->c:I

    return v0
.end method
