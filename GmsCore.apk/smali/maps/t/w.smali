.class public Lmaps/t/w;
.super Lmaps/t/v;


# instance fields
.field protected final b:Lmaps/t/af;

.field protected final c:I


# direct methods
.method constructor <init>(Lmaps/t/af;I)V
    .locals 0

    invoke-direct {p0}, Lmaps/t/v;-><init>()V

    iput-object p1, p0, Lmaps/t/w;->b:Lmaps/t/af;

    iput p2, p0, Lmaps/t/w;->c:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmaps/t/w;->b:Lmaps/t/af;

    invoke-virtual {v1}, Lmaps/t/af;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/w;->b:Lmaps/t/af;

    invoke-virtual {v1}, Lmaps/t/af;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/t/w;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/t/w;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lmaps/t/w;

    if-eqz v1, :cond_0

    check-cast p1, Lmaps/t/w;

    iget-object v1, p1, Lmaps/t/w;->b:Lmaps/t/af;

    iget-object v2, p0, Lmaps/t/w;->b:Lmaps/t/af;

    invoke-virtual {v1, v2}, Lmaps/t/af;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Lmaps/t/w;->c:I

    iget v2, p0, Lmaps/t/w;->c:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lmaps/t/w;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
