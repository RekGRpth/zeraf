.class public Lmaps/t/d;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/av;


# static fields
.field static final a:Lmaps/t/ax;


# instance fields
.field private b:Ljava/util/List;

.field private c:Lmaps/t/ax;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lmaps/t/d;->b()Lmaps/t/ax;

    move-result-object v0

    sput-object v0, Lmaps/t/d;->a:Lmaps/t/ax;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/t/d;->b:Ljava/util/List;

    sget-object v0, Lmaps/t/d;->a:Lmaps/t/ax;

    iput-object v0, p0, Lmaps/t/d;->c:Lmaps/t/ax;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/f/fd;->c(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/t/d;->b:Ljava/util/List;

    sget-object v0, Lmaps/t/d;->a:Lmaps/t/ax;

    iput-object v0, p0, Lmaps/t/d;->c:Lmaps/t/ax;

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 2

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/t/d;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/av;

    invoke-virtual {p0, v0}, Lmaps/t/d;->a(Lmaps/t/av;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public varargs constructor <init>([Lmaps/t/av;)V
    .locals 1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/t/d;-><init>(Ljava/util/Collection;)V

    return-void
.end method

.method private static b()Lmaps/t/ax;
    .locals 2

    const/high16 v1, -0x80000000

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0, v1, v1}, Lmaps/t/bx;-><init>(II)V

    new-instance v1, Lmaps/t/ax;

    invoke-direct {v1, v0, v0}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    return-object v1
.end method


# virtual methods
.method public a()Lmaps/t/ax;
    .locals 1

    iget-object v0, p0, Lmaps/t/d;->c:Lmaps/t/ax;

    return-object v0
.end method

.method public a(Lmaps/t/av;)V
    .locals 3

    invoke-interface {p1}, Lmaps/t/av;->a()Lmaps/t/ax;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/d;->c:Lmaps/t/ax;

    sget-object v2, Lmaps/t/d;->a:Lmaps/t/ax;

    if-ne v1, v2, :cond_0

    new-instance v1, Lmaps/t/ax;

    iget-object v2, v0, Lmaps/t/ax;->a:Lmaps/t/bx;

    invoke-static {v2}, Lmaps/t/bx;->a(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v2

    iget-object v0, v0, Lmaps/t/ax;->b:Lmaps/t/bx;

    invoke-static {v0}, Lmaps/t/bx;->a(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    iput-object v1, p0, Lmaps/t/d;->c:Lmaps/t/ax;

    :goto_0
    iget-object v0, p0, Lmaps/t/d;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-object v1, p0, Lmaps/t/d;->c:Lmaps/t/ax;

    invoke-virtual {v1, v0}, Lmaps/t/ax;->a(Lmaps/t/ax;)V

    goto :goto_0
.end method

.method public a(Lmaps/t/an;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/t/an;->a()Lmaps/t/ax;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/d;->c:Lmaps/t/ax;

    invoke-virtual {v1, v0}, Lmaps/t/ax;->a(Lmaps/t/an;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    :goto_1
    iget-object v0, p0, Lmaps/t/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lmaps/t/d;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/av;

    invoke-interface {v0, p1}, Lmaps/t/av;->a(Lmaps/t/an;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(Lmaps/t/bx;)Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/t/d;->c:Lmaps/t/ax;

    invoke-virtual {v0, p1}, Lmaps/t/ax;->a(Lmaps/t/bx;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    :goto_1
    iget-object v0, p0, Lmaps/t/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lmaps/t/d;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/av;

    invoke-interface {v0, p1}, Lmaps/t/av;->a(Lmaps/t/bx;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
