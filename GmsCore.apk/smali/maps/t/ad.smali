.class public final Lmaps/t/ad;
.super Ljava/lang/Object;


# instance fields
.field private final a:[I

.field private final b:[I


# direct methods
.method private constructor <init>([I[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/t/ad;->a:[I

    iput-object p2, p0, Lmaps/t/ad;->b:[I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/ah;)Lmaps/t/ad;
    .locals 4

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    rem-int/lit8 v0, v1, 0x3

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Malformed TriangleList, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vertices"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    mul-int/lit8 v0, v1, 0x3

    new-array v2, v0, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-static {p0, p1, v2, v0}, Lmaps/t/bx;->a(Ljava/io/DataInput;Lmaps/t/ah;[II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Lmaps/t/ad;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1}, Lmaps/t/ad;-><init>([I[I)V

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;Lmaps/t/cd;)Lmaps/t/ad;
    .locals 5

    const/4 v0, 0x0

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    rem-int/lit8 v1, v2, 0x3

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Malformed TriangleList, "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vertices"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    mul-int/lit8 v1, v2, 0x3

    new-array v3, v1, [I

    invoke-virtual {p1}, Lmaps/t/cd;->b()Lmaps/t/ah;

    move-result-object v4

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-static {p0, v4, v3, v1}, Lmaps/t/bx;->b(Ljava/io/DataInput;Lmaps/t/ah;[II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    new-array v2, v1, [I

    :goto_1
    if-ge v0, v1, :cond_2

    invoke-static {p0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v4

    aput v4, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    new-instance v0, Lmaps/t/ad;

    invoke-direct {v0, v3, v2}, Lmaps/t/ad;-><init>([I[I)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x9

    return v0
.end method

.method public a(ILmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V
    .locals 3

    mul-int/lit8 v0, p1, 0x9

    iget-object v1, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v2, v0, 0x1

    aget v0, v1, v0

    iput v0, p2, Lmaps/t/bx;->a:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v1, v2, 0x1

    aget v0, v0, v2

    iput v0, p2, Lmaps/t/bx;->b:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v2, v1, 0x1

    aget v0, v0, v1

    iput v0, p2, Lmaps/t/bx;->c:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v1, v2, 0x1

    aget v0, v0, v2

    iput v0, p3, Lmaps/t/bx;->a:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v2, v1, 0x1

    aget v0, v0, v1

    iput v0, p3, Lmaps/t/bx;->b:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v1, v2, 0x1

    aget v0, v0, v2

    iput v0, p3, Lmaps/t/bx;->c:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v2, v1, 0x1

    aget v0, v0, v1

    iput v0, p4, Lmaps/t/bx;->a:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v1, v2, 0x1

    aget v0, v0, v2

    iput v0, p4, Lmaps/t/bx;->b:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    aget v0, v0, v1

    iput v0, p4, Lmaps/t/bx;->c:I

    return-void
.end method

.method public a(ILmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V
    .locals 3

    mul-int/lit8 v0, p1, 0x9

    iget-object v1, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v2, v0, 0x1

    aget v0, v1, v0

    iget v1, p2, Lmaps/t/bx;->a:I

    sub-int/2addr v0, v1

    iput v0, p3, Lmaps/t/bx;->a:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v1, v2, 0x1

    aget v0, v0, v2

    iget v2, p2, Lmaps/t/bx;->b:I

    sub-int/2addr v0, v2

    iput v0, p3, Lmaps/t/bx;->b:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v2, v1, 0x1

    aget v0, v0, v1

    iget v1, p2, Lmaps/t/bx;->c:I

    sub-int/2addr v0, v1

    iput v0, p3, Lmaps/t/bx;->c:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v1, v2, 0x1

    aget v0, v0, v2

    iget v2, p2, Lmaps/t/bx;->a:I

    sub-int/2addr v0, v2

    iput v0, p4, Lmaps/t/bx;->a:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v2, v1, 0x1

    aget v0, v0, v1

    iget v1, p2, Lmaps/t/bx;->b:I

    sub-int/2addr v0, v1

    iput v0, p4, Lmaps/t/bx;->b:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v1, v2, 0x1

    aget v0, v0, v2

    iget v2, p2, Lmaps/t/bx;->c:I

    sub-int/2addr v0, v2

    iput v0, p4, Lmaps/t/bx;->c:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v2, v1, 0x1

    aget v0, v0, v1

    iget v1, p2, Lmaps/t/bx;->a:I

    sub-int/2addr v0, v1

    iput v0, p5, Lmaps/t/bx;->a:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    add-int/lit8 v1, v2, 0x1

    aget v0, v0, v2

    iget v2, p2, Lmaps/t/bx;->b:I

    sub-int/2addr v0, v2

    iput v0, p5, Lmaps/t/bx;->b:I

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    aget v0, v0, v1

    iget v1, p2, Lmaps/t/bx;->c:I

    sub-int/2addr v0, v1

    iput v0, p5, Lmaps/t/bx;->c:I

    return-void
.end method

.method public a(Lmaps/t/d;)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lmaps/t/ad;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    const/4 v2, 0x3

    new-array v2, v2, [Lmaps/t/bx;

    new-instance v3, Lmaps/t/bx;

    invoke-direct {v3}, Lmaps/t/bx;-><init>()V

    aput-object v3, v2, v1

    new-instance v3, Lmaps/t/bx;

    invoke-direct {v3}, Lmaps/t/bx;-><init>()V

    aput-object v3, v2, v6

    new-instance v3, Lmaps/t/bx;

    invoke-direct {v3}, Lmaps/t/bx;-><init>()V

    aput-object v3, v2, v7

    aget-object v3, v2, v1

    aget-object v4, v2, v6

    aget-object v5, v2, v7

    invoke-virtual {p0, v0, v3, v4, v5}, Lmaps/t/ad;->a(ILmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    new-instance v3, Lmaps/t/j;

    invoke-direct {v3, v2}, Lmaps/t/j;-><init>([Lmaps/t/bx;)V

    invoke-virtual {p1, v3}, Lmaps/t/d;->a(Lmaps/t/av;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b()I
    .locals 2

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    array-length v1, v0

    iget-object v0, p0, Lmaps/t/ad;->b:[I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x1c

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/t/ad;->b:[I

    array-length v0, v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lmaps/t/ad;

    if-eqz v1, :cond_0

    check-cast p1, Lmaps/t/ad;

    iget-object v1, p0, Lmaps/t/ad;->a:[I

    iget-object v2, p1, Lmaps/t/ad;->a:[I

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/t/ad;->b:[I

    iget-object v2, p1, Lmaps/t/ad;->b:[I

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lmaps/t/ad;->a:[I

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    iget-object v1, p0, Lmaps/t/ad;->b:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
