.class public Lmaps/t/f;
.super Lmaps/t/ar;


# instance fields
.field private final a:Ljava/util/List;

.field private b:Ljava/util/List;

.field private c:Ljava/util/Set;

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;

.field private f:J


# direct methods
.method public constructor <init>(Lmaps/t/ar;)V
    .locals 18

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->g()Lmaps/t/ah;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->i()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->l()B

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->j()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->o()I

    move-result v10

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->h()Lmaps/o/c;

    move-result-object v12

    const/4 v13, 0x0

    const-wide/16 v14, -0x1

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->r()J

    move-result-wide v16

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v17}, Lmaps/t/ar;-><init>(Lmaps/t/cl;[Ljava/lang/String;Lmaps/t/ah;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lmaps/t/ca;Lmaps/o/c;[Lmaps/t/cr;JJ)V

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/t/f;->c:Ljava/util/Set;

    const-wide/16 v1, -0x1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lmaps/t/f;->f:J

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->p()[Lmaps/t/ca;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/t/f;->a:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->d()Lmaps/t/h;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Lmaps/t/h;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-interface {v1}, Lmaps/t/h;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/t/f;->d:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->b()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/t/f;->d:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/t/f;->e:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->a()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/t/f;->e:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->a()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lmaps/t/ar;->f()J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lmaps/t/f;->f:J

    return-void
.end method

.method static synthetic a(Lmaps/t/f;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/t/f;->b:Ljava/util/List;

    return-object v0
.end method

.method public static a(Lmaps/t/ar;Lmaps/t/ar;)Lmaps/t/ar;
    .locals 6

    const-wide/16 v4, 0x0

    invoke-virtual {p0}, Lmaps/t/ar;->f()J

    move-result-wide v0

    cmp-long v2, v0, v4

    if-ltz v2, :cond_0

    invoke-virtual {p1}, Lmaps/t/ar;->f()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    invoke-virtual {p1}, Lmaps/t/ar;->f()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_4

    :cond_0
    invoke-virtual {p1}, Lmaps/t/ar;->f()J

    move-result-wide v0

    move-wide v1, v0

    :goto_0
    invoke-virtual {p1}, Lmaps/t/ar;->q()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lmaps/t/ar;->f()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    :goto_1
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lmaps/t/ar;->q()I

    move-result v0

    if-lez v0, :cond_2

    invoke-static {p0}, Lmaps/t/f;->c(Lmaps/t/ar;)Lmaps/t/f;

    move-result-object p0

    invoke-virtual {p0, p1}, Lmaps/t/f;->a(Lmaps/t/ar;)V

    invoke-direct {p0, v1, v2}, Lmaps/t/f;->a(J)V

    goto :goto_1

    :cond_2
    instance-of v0, p0, Lmaps/t/f;

    if-eqz v0, :cond_3

    move-object v0, p0

    check-cast v0, Lmaps/t/f;

    invoke-direct {v0, v1, v2}, Lmaps/t/f;->a(J)V

    goto :goto_1

    :cond_3
    new-instance v0, Lmaps/t/cj;

    invoke-direct {v0}, Lmaps/t/cj;-><init>()V

    invoke-virtual {p0}, Lmaps/t/ar;->g()Lmaps/t/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/t/cj;->a(Lmaps/t/ah;)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/ar;->i()I

    move-result v3

    invoke-virtual {v0, v3}, Lmaps/t/cj;->b(I)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/ar;->j()I

    move-result v3

    invoke-virtual {v0, v3}, Lmaps/t/cj;->a(I)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/ar;->a()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/t/cj;->a([Ljava/lang/String;)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/ar;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/t/cj;->b([Ljava/lang/String;)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/ar;->o()I

    move-result v3

    invoke-virtual {v0, v3}, Lmaps/t/cj;->c(I)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/ar;->p()[Lmaps/t/ca;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/t/cj;->a([Lmaps/t/ca;)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/ar;->h()Lmaps/o/c;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/t/cj;->a(Lmaps/o/c;)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lmaps/t/cj;->a(J)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/t/ar;->r()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmaps/t/cj;->b(J)Lmaps/t/cj;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cj;->a()Lmaps/t/ar;

    move-result-object p0

    goto :goto_1

    :cond_4
    move-wide v1, v0

    goto/16 :goto_0
.end method

.method private a(J)V
    .locals 0

    iput-wide p1, p0, Lmaps/t/f;->f:J

    return-void
.end method

.method private a(Lmaps/t/ca;)V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ca;

    invoke-interface {v0}, Lmaps/t/ca;->a()I

    move-result v0

    const/4 v2, 0x6

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-boolean v0, Lmaps/ae/h;->i:Z

    if-eqz v0, :cond_2

    const-string v0, "MutableVectorTile"

    const-string v1, "No raster to replace in the base tile. Adding the new raster to the feature collection"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static b(Lmaps/t/ar;Lmaps/t/ar;)Lmaps/t/ar;
    .locals 7

    invoke-static {p0}, Lmaps/t/f;->c(Lmaps/t/ar;)Lmaps/t/f;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/t/ar;->p()[Lmaps/t/ca;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-interface {v4}, Lmaps/t/ca;->a()I

    move-result v5

    const/4 v6, 0x6

    if-ne v5, v6, :cond_0

    invoke-direct {v1, v4}, Lmaps/t/f;->a(Lmaps/t/ca;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, v1, Lmaps/t/f;->b:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-static {v1, p1}, Lmaps/t/f;->a(Lmaps/t/ar;Lmaps/t/ar;)Lmaps/t/ar;

    move-result-object v0

    return-object v0
.end method

.method private static c(Lmaps/t/ar;)Lmaps/t/f;
    .locals 1

    instance-of v0, p0, Lmaps/t/f;

    if-eqz v0, :cond_0

    check-cast p0, Lmaps/t/f;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lmaps/t/f;

    invoke-direct {v0, p0}, Lmaps/t/f;-><init>(Lmaps/t/ar;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lmaps/t/ca;
    .locals 1

    iget-object v0, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ca;

    return-object v0
.end method

.method public a(Lmaps/t/ar;)V
    .locals 9

    const/4 v2, 0x0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/t/f;->c:Ljava/util/Set;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-virtual {p1}, Lmaps/t/ar;->q()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {p1, v1}, Lmaps/t/ar;->b(I)Lmaps/t/cr;

    move-result-object v0

    instance-of v6, v0, Lmaps/t/q;

    if-eqz v6, :cond_0

    check-cast v0, Lmaps/t/q;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    instance-of v6, v0, Lmaps/t/ch;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lmaps/t/f;->c:Ljava/util/Set;

    check-cast v0, Lmaps/t/ch;

    invoke-virtual {v0}, Lmaps/t/ch;->a()Lmaps/t/v;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    instance-of v6, v0, Lmaps/t/x;

    if-eqz v6, :cond_2

    check-cast v0, Lmaps/t/x;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    instance-of v6, v0, Lmaps/t/ba;

    if-eqz v6, :cond_3

    check-cast v0, Lmaps/t/ba;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong modifier: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    iget-object v0, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ca;

    iget-object v6, p0, Lmaps/t/f;->c:Ljava/util/Set;

    invoke-interface {v0}, Lmaps/t/ca;->b()Lmaps/t/v;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/q;

    move v1, v2

    :goto_4
    invoke-virtual {v0}, Lmaps/t/q;->a()Lmaps/t/ca;

    move-result-object v6

    invoke-interface {v6}, Lmaps/t/ca;->j()[I

    move-result-object v6

    array-length v6, v6

    if-ge v1, v6, :cond_7

    invoke-virtual {v0}, Lmaps/t/q;->a()Lmaps/t/ca;

    move-result-object v6

    invoke-interface {v6}, Lmaps/t/ca;->j()[I

    move-result-object v6

    aget v7, v6, v1

    iget-object v8, p0, Lmaps/t/f;->d:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v7, v8

    aput v7, v6, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_7
    invoke-virtual {v0}, Lmaps/t/q;->b()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Lmaps/t/q;->c()I

    move-result v1

    iget-object v6, p0, Lmaps/t/f;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_a

    iget-object v1, p0, Lmaps/t/f;->a:Ljava/util/List;

    invoke-virtual {v0}, Lmaps/t/q;->c()I

    move-result v6

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/ca;

    iget-object v6, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_9

    invoke-virtual {v0}, Lmaps/t/q;->d()Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-virtual {v0}, Lmaps/t/q;->a()Lmaps/t/ca;

    move-result-object v0

    invoke-interface {v6, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_3

    :cond_8
    iget-object v6, p0, Lmaps/t/f;->b:Ljava/util/List;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Lmaps/t/q;->a()Lmaps/t/ca;

    move-result-object v0

    invoke-interface {v6, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_3

    :cond_9
    iget-object v1, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-virtual {v0}, Lmaps/t/q;->a()Lmaps/t/ca;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_a
    invoke-virtual {v0}, Lmaps/t/q;->c()I

    move-result v1

    iget-object v6, p0, Lmaps/t/f;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v1, v6, :cond_b

    const-string v1, "MutableVectorTile"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid plane index on tile "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lmaps/t/ar;->h()Lmaps/o/c;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " at "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lmaps/t/ar;->g()Lmaps/t/ah;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    iget-object v1, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-virtual {v0}, Lmaps/t/q;->a()Lmaps/t/ca;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_c
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ba;

    goto :goto_5

    :cond_d
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/x;

    iget-object v3, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-virtual {v0}, Lmaps/t/x;->a()Lmaps/t/ca;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_6

    :cond_e
    invoke-virtual {p1}, Lmaps/t/ar;->a()[Ljava/lang/String;

    move-result-object v0

    :goto_7
    array-length v1, v0

    if-ge v2, v1, :cond_10

    iget-object v1, p0, Lmaps/t/f;->e:Ljava/util/List;

    aget-object v3, v0, v2

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    iget-object v1, p0, Lmaps/t/f;->e:Ljava/util/List;

    aget-object v3, v0, v2

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_10
    iget-object v0, p0, Lmaps/t/f;->d:Ljava/util/List;

    invoke-virtual {p1}, Lmaps/t/ar;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public a(Lmaps/ae/d;)Z
    .locals 4

    iget-wide v0, p0, Lmaps/t/f;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lmaps/ae/d;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/t/f;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lmaps/t/f;->e:Ljava/util/List;

    iget-object v1, p0, Lmaps/t/f;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public b()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lmaps/t/f;->d:Ljava/util/List;

    iget-object v1, p0, Lmaps/t/f;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lmaps/t/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public d()Lmaps/t/h;
    .locals 2

    new-instance v0, Lmaps/t/cp;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/t/cp;-><init>(Lmaps/t/f;Lmaps/t/ag;)V

    return-object v0
.end method

.method public e()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/t/f;->c:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lmaps/t/f;->f:J

    return-wide v0
.end method
