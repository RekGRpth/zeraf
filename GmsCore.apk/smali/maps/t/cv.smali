.class public Lmaps/t/cv;
.super Ljava/lang/Object;


# static fields
.field private static g:Lmaps/t/cv;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:F

.field private final e:F

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v4, 0x0

    const/4 v1, 0x0

    new-instance v0, Lmaps/t/cv;

    const/16 v3, 0xc

    move v2, v1

    move v5, v4

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lmaps/t/cv;-><init>(IIIFFI)V

    sput-object v0, Lmaps/t/cv;->g:Lmaps/t/cv;

    return-void
.end method

.method public constructor <init>(IIIFFI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmaps/t/cv;->a:I

    iput p2, p0, Lmaps/t/cv;->b:I

    iput p3, p0, Lmaps/t/cv;->c:I

    iput p4, p0, Lmaps/t/cv;->d:F

    iput p5, p0, Lmaps/t/cv;->e:F

    iput p6, p0, Lmaps/t/cv;->f:I

    return-void
.end method

.method public static a()Lmaps/t/cv;
    .locals 1

    sget-object v0, Lmaps/t/cv;->g:Lmaps/t/cv;

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;I)Lmaps/t/cv;
    .locals 7

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v0

    invoke-static {v0}, Lmaps/t/ab;->b(I)F

    move-result v4

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v0

    invoke-static {v0}, Lmaps/t/ab;->b(I)F

    move-result v5

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v6

    new-instance v0, Lmaps/t/cv;

    invoke-direct/range {v0 .. v6}, Lmaps/t/cv;-><init>(IIIFFI)V

    return-object v0
.end method


# virtual methods
.method public b()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lmaps/t/cv;->f:I

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 2

    const/4 v0, 0x2

    iget v1, p0, Lmaps/t/cv;->f:I

    invoke-static {v0, v1}, Lmaps/t/ab;->a(II)Z

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lmaps/t/cv;->a:I

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lmaps/t/cv;->b:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    move v1, v0

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    check-cast p1, Lmaps/t/cv;

    iget v2, p0, Lmaps/t/cv;->f:I

    iget v3, p1, Lmaps/t/cv;->f:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lmaps/t/cv;->a:I

    iget v3, p1, Lmaps/t/cv;->a:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lmaps/t/cv;->d:F

    iget v3, p1, Lmaps/t/cv;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Lmaps/t/cv;->b:I

    iget v3, p1, Lmaps/t/cv;->b:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lmaps/t/cv;->c:I

    iget v3, p1, Lmaps/t/cv;->c:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lmaps/t/cv;->e:F

    iget v3, p1, Lmaps/t/cv;->e:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lmaps/t/cv;->c:I

    return v0
.end method

.method public g()F
    .locals 1

    iget v0, p0, Lmaps/t/cv;->d:F

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lmaps/t/cv;->f:I

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/t/cv;->a:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/t/cv;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/t/cv;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/t/cv;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/t/cv;->e:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TextStyle{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "color="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/t/cv;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", outlineColor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/t/cv;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/t/cv;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", leadingRatio="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/t/cv;->d:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", trackingRatio="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/t/cv;->e:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", attributes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/t/cv;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
