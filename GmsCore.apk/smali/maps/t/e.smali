.class public Lmaps/t/e;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/t/bg;

.field private final b:Ljava/util/List;

.field private final c:I

.field private final d:Lmaps/t/bx;

.field private final e:J

.field private f:Z


# direct methods
.method protected constructor <init>(Lmaps/t/bg;Ljava/util/List;IZLmaps/t/bx;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/t/e;->a:Lmaps/t/bg;

    iput-object p2, p0, Lmaps/t/e;->b:Ljava/util/List;

    iput p3, p0, Lmaps/t/e;->c:I

    iput-boolean p4, p0, Lmaps/t/e;->f:Z

    iput-object p5, p0, Lmaps/t/e;->d:Lmaps/t/bx;

    iput-wide p6, p0, Lmaps/t/e;->e:J

    return-void
.end method

.method public static a(Lmaps/bb/c;J)Lmaps/t/e;
    .locals 9

    const/4 v5, 0x0

    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, v3}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lmaps/t/bg;->b(Ljava/lang/String;)Lmaps/t/bg;

    move-result-object v1

    if-nez v1, :cond_1

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "INDOOR"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "malformed building id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v3}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    invoke-virtual {p0, v7}, Lmaps/bb/c;->j(I)I

    move-result v6

    invoke-static {v6}, Lmaps/f/fd;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    move v3, v0

    :goto_1
    if-ge v3, v6, :cond_3

    invoke-virtual {p0, v7, v3}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v4

    invoke-static {v4}, Lmaps/t/bi;->a(Lmaps/bb/c;)Lmaps/t/bi;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x4

    invoke-virtual {p0, v3}, Lmaps/bb/c;->b(I)Z

    move-result v4

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lmaps/bb/c;->d(I)I

    move-result v3

    if-ltz v3, :cond_4

    if-lt v3, v6, :cond_5

    :cond_4
    move v3, v0

    :cond_5
    if-nez v4, :cond_6

    if-nez v6, :cond_7

    :cond_6
    const/4 v3, -0x1

    :cond_7
    invoke-virtual {p0, v8}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0, v8}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v0

    invoke-static {v0}, Lmaps/t/bx;->a(Lmaps/bb/c;)Lmaps/t/bx;

    move-result-object v5

    :cond_8
    new-instance v0, Lmaps/t/e;

    move-wide v6, p1

    invoke-direct/range {v0 .. v7}, Lmaps/t/e;-><init>(Lmaps/t/bg;Ljava/util/List;IZLmaps/t/bx;J)V

    move-object v5, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/t/bi;)I
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/t/e;->a(Lmaps/t/bg;)Lmaps/t/bi;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/e;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public a()Lmaps/t/bg;
    .locals 1

    iget-object v0, p0, Lmaps/t/e;->a:Lmaps/t/bg;

    return-object v0
.end method

.method public a(I)Lmaps/t/bi;
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lmaps/t/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lmaps/t/e;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bi;

    goto :goto_0
.end method

.method public a(Lmaps/t/bb;)Lmaps/t/bi;
    .locals 1

    invoke-virtual {p1}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/t/e;->a(Lmaps/t/bg;)Lmaps/t/bi;

    move-result-object v0

    return-object v0
.end method

.method public a(Lmaps/t/bg;)Lmaps/t/bi;
    .locals 3

    iget-object v0, p0, Lmaps/t/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/bi;

    invoke-virtual {v0}, Lmaps/t/bi;->b()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {p1, v2}, Lmaps/t/bg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/ae/d;)Z
    .locals 4

    iget-wide v0, p0, Lmaps/t/e;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lmaps/ae/d;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/t/e;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lmaps/t/bb;)I
    .locals 1

    invoke-virtual {p1}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/t/e;->a(Lmaps/t/bg;)Lmaps/t/bi;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/t/e;->a(Lmaps/t/bi;)I

    move-result v0

    return v0
.end method

.method public b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/t/e;->b:Ljava/util/List;

    return-object v0
.end method

.method public c()Lmaps/t/bi;
    .locals 1

    iget v0, p0, Lmaps/t/e;->c:I

    invoke-virtual {p0, v0}, Lmaps/t/e;->a(I)Lmaps/t/bi;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/t/e;->f:Z

    return v0
.end method

.method public e()Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/t/e;->d:Lmaps/t/bx;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Building: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/t/e;->a:Lmaps/t/bg;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
