.class public abstract enum Lmaps/t/cm;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/t/cm;

.field public static final enum b:Lmaps/t/cm;

.field public static final enum c:Lmaps/t/cm;

.field public static final enum d:Lmaps/t/cm;

.field public static final enum e:Lmaps/t/cm;

.field public static final enum f:Lmaps/t/cm;

.field private static final synthetic g:[Lmaps/t/cm;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lmaps/t/bo;

    const-string v1, "SPOTLIGHT"

    invoke-direct {v0, v1, v3}, Lmaps/t/bo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/t/cm;->a:Lmaps/t/cm;

    new-instance v0, Lmaps/t/bl;

    const-string v1, "SPOTLIGHT_DIFFTILE"

    invoke-direct {v0, v1, v4}, Lmaps/t/bl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/t/cm;->b:Lmaps/t/cm;

    new-instance v0, Lmaps/t/bm;

    const-string v1, "HIGHLIGHT"

    invoke-direct {v0, v1, v5}, Lmaps/t/bm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/t/cm;->c:Lmaps/t/cm;

    new-instance v0, Lmaps/t/bj;

    const-string v1, "INDOOR"

    invoke-direct {v0, v1, v6}, Lmaps/t/bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/t/cm;->d:Lmaps/t/cm;

    new-instance v0, Lmaps/t/bk;

    const-string v1, "TRANSIT"

    invoke-direct {v0, v1, v7}, Lmaps/t/bk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/t/cm;->e:Lmaps/t/cm;

    new-instance v0, Lmaps/t/bp;

    const-string v1, "ALTERNATE_PAINTFE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lmaps/t/bp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/t/cm;->f:Lmaps/t/cm;

    const/4 v0, 0x6

    new-array v0, v0, [Lmaps/t/cm;

    sget-object v1, Lmaps/t/cm;->a:Lmaps/t/cm;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/t/cm;->b:Lmaps/t/cm;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/t/cm;->c:Lmaps/t/cm;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/t/cm;->d:Lmaps/t/cm;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/t/cm;->e:Lmaps/t/cm;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/t/cm;->f:Lmaps/t/cm;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/t/cm;->g:[Lmaps/t/cm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILmaps/t/ct;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/t/cm;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/t/cm;
    .locals 1

    const-class v0, Lmaps/t/cm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/t/cm;

    return-object v0
.end method

.method public static values()[Lmaps/t/cm;
    .locals 1

    sget-object v0, Lmaps/t/cm;->g:[Lmaps/t/cm;

    invoke-virtual {v0}, [Lmaps/t/cm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/t/cm;

    return-object v0
.end method


# virtual methods
.method public a()Lmaps/o/c;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Lmaps/bb/c;)Lmaps/t/ao;
.end method
