.class Lmaps/be/k;
.super Lmaps/ak/e;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/Long;

.field private final c:Ljava/lang/Long;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lmaps/ak/e;-><init>()V

    iput-object p1, p0, Lmaps/be/k;->a:Landroid/content/Context;

    iput-object p2, p0, Lmaps/be/k;->b:Ljava/lang/Long;

    iput-object p3, p0, Lmaps/be/k;->d:Ljava/lang/String;

    iput-object p4, p0, Lmaps/be/k;->c:Ljava/lang/Long;

    iput-object p5, p0, Lmaps/be/k;->e:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lmaps/be/m;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lmaps/be/k;-><init>(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x4b

    return v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/c/ft;->d:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    invoke-virtual {v0, v4, v4}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    iget-object v1, p0, Lmaps/be/k;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/be/k;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v5, v1, v2}, Lmaps/bb/c;->b(IJ)Lmaps/bb/c;

    :cond_0
    new-instance v1, Lmaps/bb/c;

    sget-object v2, Lmaps/c/ft;->a:Lmaps/bb/d;

    invoke-direct {v1, v2}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    invoke-virtual {v1, v4, v0}, Lmaps/bb/c;->a(ILmaps/bb/c;)V

    new-instance v0, Lmaps/bb/c;

    sget-object v2, Lmaps/c/ft;->d:Lmaps/bb/d;

    invoke-direct {v0, v2}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const/4 v2, 0x5

    invoke-virtual {v0, v4, v2}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    iget-object v2, p0, Lmaps/be/k;->c:Ljava/lang/Long;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lmaps/be/k;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v5, v2, v3}, Lmaps/bb/c;->b(IJ)Lmaps/bb/c;

    :cond_1
    invoke-virtual {v1, v4, v0}, Lmaps/bb/c;->a(ILmaps/bb/c;)V

    invoke-static {v1}, Lmaps/az/c;->a(Lmaps/bb/c;)V

    invoke-virtual {v1}, Lmaps/bb/c;->d()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lmaps/bb/c;->b(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 13

    const/4 v12, 0x7

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    sget-object v0, Lmaps/c/ft;->c:Lmaps/bb/d;

    invoke-static {v0, p1}, Lmaps/bb/b;->a(Lmaps/bb/d;Ljava/io/DataInput;)Lmaps/bb/c;

    move-result-object v1

    invoke-virtual {v1, v9}, Lmaps/bb/c;->j(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    invoke-virtual {v1, v9, v0}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v3

    invoke-virtual {v3, v9}, Lmaps/bb/c;->d(I)I

    move-result v4

    if-ne v4, v9, :cond_2

    invoke-virtual {v3, v10}, Lmaps/bb/c;->i(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v11}, Lmaps/bb/c;->i(I)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lmaps/be/k;->a:Landroid/content/Context;

    iget-object v5, p0, Lmaps/be/k;->d:Ljava/lang/String;

    invoke-static {v4, v3, v5}, Lmaps/bt/a;->a(Landroid/content/Context;Lmaps/bb/c;Ljava/lang/String;)Z

    invoke-virtual {v3, v11}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v4

    sget-boolean v5, Lmaps/ae/h;->j:Z

    if-eqz v5, :cond_0

    const-string v5, "ParameterManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Updating nav parameters. Hash: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3, v10}, Lmaps/bb/c;->e(I)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v3, Lmaps/be/o;

    invoke-direct {v3, v4}, Lmaps/be/o;-><init>(Lmaps/bb/c;)V

    invoke-static {v3}, Lmaps/be/b;->a(Lmaps/be/o;)Lmaps/be/o;

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v9}, Lmaps/bb/c;->d(I)I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    invoke-virtual {v3, v10}, Lmaps/bb/c;->i(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v12}, Lmaps/bb/c;->i(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lmaps/be/k;->a:Landroid/content/Context;

    iget-object v5, p0, Lmaps/be/k;->e:Ljava/lang/String;

    invoke-static {v4, v3, v5}, Lmaps/bt/a;->a(Landroid/content/Context;Lmaps/bb/c;Ljava/lang/String;)Z

    invoke-virtual {v3, v12}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v4

    sget-boolean v5, Lmaps/ae/h;->j:Z

    if-eqz v5, :cond_3

    const-string v5, "ParameterManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Updating tile zoom progression. Hash: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3, v10}, Lmaps/bb/c;->e(I)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-static {v4}, Lmaps/p/k;->a(Lmaps/bb/c;)Lmaps/p/k;

    move-result-object v3

    invoke-static {v3}, Lmaps/be/b;->a(Lmaps/p/k;)Lmaps/p/k;

    goto :goto_1

    :cond_4
    invoke-static {v9}, Lmaps/be/b;->b(Z)Z

    const-class v1, Lmaps/be/b;

    monitor-enter v1

    :try_start_0
    const-class v0, Lmaps/be/b;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return v9

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public l_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
