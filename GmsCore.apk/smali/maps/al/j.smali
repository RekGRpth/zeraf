.class public Lmaps/al/j;
.super Lmaps/al/i;


# instance fields
.field private final h:[I

.field private volatile i:J


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0, p1}, Lmaps/al/i;-><init>(I)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/al/j;->h:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/al/j;->i:J

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lmaps/al/i;-><init>(IZ)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/al/j;->h:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/al/j;->i:J

    return-void
.end method


# virtual methods
.method public a(Lmaps/cr/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lmaps/al/i;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/al/j;->h:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/j;->h:[I

    aput v1, v0, v1

    :cond_0
    return-void
.end method

.method public a(Lmaps/cr/c;I)V
    .locals 6

    const v5, 0x8893

    const/4 v4, 0x0

    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/al/j;->i:J

    invoke-virtual {p1}, Lmaps/cr/c;->L()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1, p2}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, Lmaps/al/j;->h:[I

    aget v1, v1, v4

    if-nez v1, :cond_3

    iget-object v1, p0, Lmaps/al/j;->d:Ljava/nio/ShortBuffer;

    if-nez v1, :cond_2

    invoke-virtual {p0, p1}, Lmaps/al/j;->d(Lmaps/cr/c;)V

    :cond_2
    iget-object v1, p0, Lmaps/al/j;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->limit()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/al/j;->h:[I

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    iget-object v1, p0, Lmaps/al/j;->h:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    iget-object v1, p0, Lmaps/al/j;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->limit()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Lmaps/al/j;->f:I

    iget v1, p0, Lmaps/al/j;->f:I

    iget-object v2, p0, Lmaps/al/j;->d:Ljava/nio/ShortBuffer;

    const v3, 0x88e4

    invoke-interface {v0, v5, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/al/j;->d:Ljava/nio/ShortBuffer;

    :cond_3
    iget-object v1, p0, Lmaps/al/j;->h:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    iget v1, p0, Lmaps/al/j;->c:I

    const/16 v2, 0x1403

    invoke-interface {v0, p2, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glDrawElements(IIII)V

    invoke-interface {v0, v5, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_0
.end method

.method public b(Lmaps/cr/c;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/al/j;->h:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/al/j;->h:[I

    aget v0, v0, v3

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lmaps/al/j;->i:J

    invoke-static {v0, v1}, Lmaps/cr/c;->b(J)Lmaps/cr/c;

    move-result-object v0

    if-ne v0, p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/al/j;->h:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    :cond_0
    iget-object v0, p0, Lmaps/al/j;->h:[I

    aput v3, v0, v3

    iput v3, p0, Lmaps/al/j;->f:I

    :cond_1
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/al/j;->i:J

    return-void
.end method

.method public d()I
    .locals 2

    const/16 v0, 0x38

    iget-object v1, p0, Lmaps/al/j;->g:Lmaps/av/a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/al/j;->g:Lmaps/av/a;

    invoke-virtual {v1}, Lmaps/av/a;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/al/j;->a:[S

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/al/j;->a:[S

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method protected d(Lmaps/cr/c;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->L()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lmaps/cr/c;->P()Lmaps/al/m;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/al/m;->b()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/al/j;->d:Ljava/nio/ShortBuffer;

    iget-object v0, p0, Lmaps/al/j;->g:Lmaps/av/a;

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/al/j;->d:Ljava/nio/ShortBuffer;

    iget-object v1, p0, Lmaps/al/j;->a:[S

    iget v2, p0, Lmaps/al/j;->c:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/nio/ShortBuffer;->put([SII)Ljava/nio/ShortBuffer;

    :goto_0
    iget-object v0, p0, Lmaps/al/j;->d:Ljava/nio/ShortBuffer;

    iget v1, p0, Lmaps/al/j;->c:I

    invoke-virtual {v0, v1}, Ljava/nio/ShortBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lmaps/al/j;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    sget-boolean v0, Lmaps/bm/b;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/al/j;->g:Lmaps/av/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/j;->g:Lmaps/av/a;

    invoke-virtual {v0}, Lmaps/av/a;->c()V

    iput-object v4, p0, Lmaps/al/j;->g:Lmaps/av/a;

    :cond_0
    iput-object v4, p0, Lmaps/al/j;->a:[S

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lmaps/al/j;->b()V

    iget-object v0, p0, Lmaps/al/j;->g:Lmaps/av/a;

    iget-object v1, p0, Lmaps/al/j;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v1}, Lmaps/av/a;->a(Ljava/nio/ShortBuffer;)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Lmaps/al/i;->d(Lmaps/cr/c;)V

    goto :goto_1
.end method
