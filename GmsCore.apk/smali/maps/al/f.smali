.class public Lmaps/al/f;
.super Lmaps/al/a;


# instance fields
.field private final g:[I

.field private volatile h:J


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0, p1}, Lmaps/al/a;-><init>(I)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/al/f;->g:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/al/f;->h:J

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lmaps/al/a;-><init>(IZ)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/al/f;->g:[I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/al/f;->h:J

    return-void
.end method


# virtual methods
.method public a(Lmaps/cr/c;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/al/f;->g:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/al/f;->g:[I

    aget v0, v0, v3

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lmaps/al/f;->h:J

    invoke-static {v0, v1}, Lmaps/cr/c;->b(J)Lmaps/cr/c;

    move-result-object v0

    if-ne v0, p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/al/f;->g:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    :cond_0
    iget-object v0, p0, Lmaps/al/f;->g:[I

    aput v3, v0, v3

    iput v3, p0, Lmaps/al/f;->e:I

    :cond_1
    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/al/f;->h:J

    return-void
.end method

.method public c()I
    .locals 2

    const/16 v0, 0x38

    iget-object v1, p0, Lmaps/al/f;->f:Lmaps/av/e;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/al/f;->f:Lmaps/av/e;

    invoke-virtual {v1}, Lmaps/av/e;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/al/f;->a:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/al/f;->a:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 9

    const/4 v2, 0x4

    const/4 v1, 0x2

    const/4 v4, 0x1

    const v8, 0x8892

    const/4 v5, 0x0

    invoke-static {p1}, Lmaps/cr/c;->a(Lmaps/cr/c;)J

    move-result-wide v6

    iput-wide v6, p0, Lmaps/al/f;->h:J

    invoke-virtual {p1}, Lmaps/cr/c;->L()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Lmaps/al/a;->c(Lmaps/cr/c;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljavax/microedition/khronos/opengles/GL11;

    iget-object v0, p0, Lmaps/al/f;->g:[I

    aget v0, v0, v5

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/al/f;->d:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, Lmaps/al/f;->d(Lmaps/cr/c;)V

    :cond_2
    iget-object v0, p0, Lmaps/al/f;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/f;->g:[I

    invoke-interface {v7, v4, v0, v5}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    iget-object v0, p0, Lmaps/al/f;->g:[I

    aget v0, v0, v5

    invoke-interface {v7, v8, v0}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    iget-object v0, p0, Lmaps/al/f;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    iput v0, p0, Lmaps/al/f;->e:I

    iget v0, p0, Lmaps/al/f;->e:I

    iget-object v3, p0, Lmaps/al/f;->d:Ljava/nio/ByteBuffer;

    const v6, 0x88e4

    invoke-interface {v7, v8, v0, v3, v6}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/al/f;->d:Ljava/nio/ByteBuffer;

    :cond_3
    iget-object v0, p0, Lmaps/al/f;->g:[I

    aget v0, v0, v5

    invoke-interface {v7, v8, v0}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    sget-boolean v0, Lmaps/ae/h;->t:Z

    if-eqz v0, :cond_4

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    invoke-virtual {v0, v1}, Lmaps/s/a;->a(I)V

    move-object v0, v7

    check-cast v0, Lmaps/s/a;

    const/16 v3, 0x1401

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lmaps/s/a;->a(IIIZII)V

    :goto_1
    invoke-interface {v7, v8, v5}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x1401

    invoke-interface {v7, v2, v0, v5, v5}, Ljavax/microedition/khronos/opengles/GL11;->glColorPointer(IIII)V

    goto :goto_1
.end method

.method protected d(Lmaps/cr/c;)V
    .locals 2

    invoke-virtual {p1}, Lmaps/cr/c;->L()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/al/f;->c:I

    mul-int/lit8 v1, v0, 0x4

    invoke-virtual {p1}, Lmaps/cr/c;->P()Lmaps/al/m;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/al/m;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lmaps/al/f;->d:Ljava/nio/ByteBuffer;

    sget-boolean v0, Lmaps/bm/b;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lmaps/al/f;->a(IZ)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lmaps/al/a;->d(Lmaps/cr/c;)V

    goto :goto_1
.end method
