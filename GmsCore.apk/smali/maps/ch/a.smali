.class public final Lmaps/ch/a;
.super Lcom/google/android/gms/maps/internal/ILocationSourceDelegate$Stub;

# interfaces
.implements Ljava/lang/Runnable;
.implements Lmaps/ch/b;
.implements Lmaps/e/z;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:I


# instance fields
.field private final c:Lmaps/e/v;

.field private final d:Landroid/os/Handler;

.field private e:Z

.field private f:Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;

.field private g:Lmaps/e/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/ch/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/ch/a;->a:Ljava/lang/String;

    invoke-static {}, Lmaps/k/b;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1388

    :goto_0
    sput v0, Lmaps/ch/a;->b:I

    return-void

    :cond_0
    const/16 v0, 0x1770

    goto :goto_0
.end method

.method constructor <init>(Lmaps/e/v;Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/ILocationSourceDelegate$Stub;-><init>()V

    iput-object p1, p0, Lmaps/ch/a;->c:Lmaps/e/v;

    iput-object p2, p0, Lmaps/ch/a;->d:Landroid/os/Handler;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lmaps/ch/a;
    .locals 10

    const/4 v1, 0x1

    const/4 v9, 0x0

    invoke-static {}, Lmaps/e/f;->a()Lmaps/e/v;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->v()Landroid/location/LocationManager;

    move-result-object v5

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v3

    new-instance v6, Lmaps/ar/a;

    invoke-direct {v6, v3, v5, v9}, Lmaps/ar/a;-><init>(Lmaps/ae/d;Landroid/location/LocationManager;Z)V

    invoke-static {v4}, Lmaps/ch/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lmaps/ch/a;->b:I

    int-to-long v7, v0

    invoke-static {v4, v7, v8}, Lmaps/e/s;->a(Landroid/content/Context;J)Lmaps/e/s;

    move-result-object v0

    :goto_0
    invoke-static {}, Lmaps/k/b;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Lmaps/h/d;

    invoke-direct {v2, v3, v5, v0, v4}, Lmaps/h/d;-><init>(Lmaps/ae/d;Landroid/location/LocationManager;Lmaps/e/ab;Landroid/content/Context;)V

    :goto_1
    new-instance v0, Lmaps/e/f;

    const/4 v5, 0x2

    new-array v5, v5, [Lmaps/e/aa;

    aput-object v6, v5, v9

    aput-object v2, v5, v1

    invoke-static {v5}, Lmaps/f/fd;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v5, Lmaps/e/m;

    invoke-direct {v5}, Lmaps/e/m;-><init>()V

    invoke-direct/range {v0 .. v5}, Lmaps/e/f;-><init>(ZLjava/util/List;Lmaps/ae/d;Landroid/content/Context;Lmaps/e/ad;)V

    invoke-static {v0}, Lmaps/e/f;->a(Lmaps/e/f;)V

    :cond_0
    new-instance v0, Lmaps/ch/a;

    invoke-static {}, Lmaps/e/f;->a()Lmaps/e/v;

    move-result-object v1

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1, v2}, Lmaps/ch/a;-><init>(Lmaps/e/v;Landroid/os/Handler;)V

    return-object v0

    :cond_1
    sget v0, Lmaps/ch/a;->b:I

    int-to-long v7, v0

    invoke-static {v4, v7, v8}, Lmaps/ch/c;->a(Landroid/content/Context;J)Lmaps/ch/c;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v2, Lmaps/e/u;

    invoke-direct {v2, v3, v5, v0}, Lmaps/e/u;-><init>(Lmaps/ae/d;Landroid/location/LocationManager;Lmaps/e/ab;)V

    goto :goto_1
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method private d()V
    .locals 1

    iget-object v0, p0, Lmaps/ch/a;->c:Lmaps/e/v;

    invoke-interface {v0, p0}, Lmaps/e/v;->a(Lmaps/e/z;)V

    iget-object v0, p0, Lmaps/ch/a;->c:Lmaps/e/v;

    invoke-interface {v0}, Lmaps/e/v;->d()V

    return-void
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lmaps/ch/a;->c:Lmaps/e/v;

    invoke-interface {v0}, Lmaps/e/v;->e()V

    iget-object v0, p0, Lmaps/ch/a;->c:Lmaps/e/v;

    invoke-interface {v0, p0}, Lmaps/e/v;->b(Lmaps/e/z;)V

    iget-object v0, p0, Lmaps/ch/a;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ch/a;->g:Lmaps/e/a;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ch/a;->a:Ljava/lang/String;

    const-string v1, "Resuming location source"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ch/a;->e:Z

    iget-object v0, p0, Lmaps/ch/a;->f:Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/ch/a;->d()V

    :cond_1
    return-void
.end method

.method public a(ILmaps/e/v;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/bl/a;Lmaps/e/v;)V
    .locals 1

    iget-boolean v0, p0, Lmaps/ch/a;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ch/a;->f:Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p2}, Lmaps/e/v;->g()Lmaps/e/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/ch/a;->g:Lmaps/e/a;

    iget-object v0, p0, Lmaps/ch/a;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public activate(Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/ch/a;->f:Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "already activated"

    invoke-static {v0, v3}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    if-eqz p1, :cond_3

    :goto_1
    const-string v0, "listener cannot be null"

    invoke-static {v1, v0}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ch/a;->a:Ljava/lang/String;

    const-string v1, "Activating location source"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Lmaps/ch/a;->f:Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;

    iget-boolean v0, p0, Lmaps/ch/a;->e:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/ch/a;->d()V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public b()V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ch/a;->a:Ljava/lang/String;

    const-string v1, "Pausing location source"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lmaps/ch/a;->f:Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/ch/a;->e()V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ch/a;->e:Z

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lmaps/ch/a;->c:Lmaps/e/v;

    invoke-interface {v0}, Lmaps/e/v;->c()Z

    move-result v0

    return v0
.end method

.method public deactivate()V
    .locals 2

    iget-object v0, p0, Lmaps/ch/a;->f:Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "already activated"

    invoke-static {v0, v1}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ch/a;->a:Ljava/lang/String;

    const-string v1, "Deactivating location source"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ch/a;->f:Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;

    iget-boolean v0, p0, Lmaps/ch/a;->e:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/ch/a;->e()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/ch/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Broadcasting location update: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/ch/a;->g:Lmaps/e/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v0, p0, Lmaps/ch/a;->f:Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;

    iget-object v1, p0, Lmaps/ch/a;->g:Lmaps/e/a;

    invoke-static {v1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IOnLocationChangeListener;->onLocationChanged(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
