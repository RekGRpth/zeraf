.class public Lmaps/c/i;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lmaps/bb/d;

.field public static final b:Lmaps/bb/d;

.field public static final c:Lmaps/bb/d;

.field public static final d:Lmaps/bb/d;

.field public static final e:Lmaps/bb/d;

.field public static final f:Lmaps/bb/d;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v6, 0x215

    const/4 v1, 0x0

    new-instance v0, Lmaps/bb/d;

    invoke-direct {v0}, Lmaps/bb/d;-><init>()V

    sput-object v0, Lmaps/c/i;->a:Lmaps/bb/d;

    new-instance v0, Lmaps/bb/d;

    invoke-direct {v0}, Lmaps/bb/d;-><init>()V

    sput-object v0, Lmaps/c/i;->b:Lmaps/bb/d;

    new-instance v0, Lmaps/bb/d;

    invoke-direct {v0}, Lmaps/bb/d;-><init>()V

    sput-object v0, Lmaps/c/i;->c:Lmaps/bb/d;

    new-instance v0, Lmaps/bb/d;

    invoke-direct {v0}, Lmaps/bb/d;-><init>()V

    sput-object v0, Lmaps/c/i;->d:Lmaps/bb/d;

    new-instance v0, Lmaps/bb/d;

    invoke-direct {v0}, Lmaps/bb/d;-><init>()V

    sput-object v0, Lmaps/c/i;->e:Lmaps/bb/d;

    new-instance v0, Lmaps/bb/d;

    invoke-direct {v0}, Lmaps/bb/d;-><init>()V

    sput-object v0, Lmaps/c/i;->f:Lmaps/bb/d;

    sget-object v2, Lmaps/c/i;->a:Lmaps/bb/d;

    const/16 v3, 0x41b

    move-object v0, v1

    check-cast v0, Lmaps/c/fy;

    sget-object v0, Lmaps/c/i;->b:Lmaps/bb/d;

    invoke-virtual {v2, v3, v7, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x41e

    move-object v0, v1

    check-cast v0, Lmaps/c/fy;

    invoke-virtual {v2, v3, v8, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x21e

    move-object v0, v1

    check-cast v0, Lmaps/c/fy;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v9, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    sget-object v2, Lmaps/c/i;->b:Lmaps/bb/d;

    move-object v0, v1

    check-cast v0, Lmaps/c/bj;

    invoke-virtual {v2, v6, v7, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/c/bj;

    const-wide/16 v3, 0x1

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v6, v8, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x224

    move-object v0, v1

    check-cast v0, Lmaps/c/bj;

    invoke-virtual {v2, v3, v9, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/c/bj;

    const/4 v0, 0x4

    invoke-virtual {v2, v6, v0, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x21b

    move-object v0, v1

    check-cast v0, Lmaps/c/bj;

    const/4 v0, 0x5

    sget-object v4, Lmaps/c/il;->g:Lmaps/bb/d;

    invoke-virtual {v2, v3, v0, v4}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x21b

    move-object v0, v1

    check-cast v0, Lmaps/c/bj;

    const/4 v0, 0x6

    sget-object v4, Lmaps/c/i;->c:Lmaps/bb/d;

    invoke-virtual {v2, v3, v0, v4}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x21b

    move-object v0, v1

    check-cast v0, Lmaps/c/bj;

    const/4 v0, 0x7

    sget-object v4, Lmaps/c/i;->e:Lmaps/bb/d;

    invoke-virtual {v2, v3, v0, v4}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x218

    move-object v0, v1

    check-cast v0, Lmaps/c/bj;

    const/16 v0, 0x8

    sget-object v4, Lmaps/bb/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v3, v0, v4}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    sget-object v2, Lmaps/c/i;->c:Lmaps/bb/d;

    move-object v0, v1

    check-cast v0, Lmaps/c/cx;

    invoke-virtual {v2, v6, v7, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x41b

    move-object v0, v1

    check-cast v0, Lmaps/c/cx;

    sget-object v0, Lmaps/c/i;->d:Lmaps/bb/d;

    invoke-virtual {v2, v3, v8, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    sget-object v2, Lmaps/c/i;->d:Lmaps/bb/d;

    const/16 v3, 0x21e

    move-object v0, v1

    check-cast v0, Lmaps/c/q;

    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v7, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/c/q;

    const-wide/16 v3, 0x1

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v6, v8, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/c/q;

    const-wide/16 v3, 0x2

    invoke-static {v3, v4}, Lmaps/ah/c;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v6, v9, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/c/q;

    const/4 v0, 0x4

    invoke-virtual {v2, v6, v0, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/c/q;

    const/4 v0, 0x5

    invoke-virtual {v2, v6, v0, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/c/q;

    const/4 v0, 0x6

    invoke-virtual {v2, v6, v0, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    sget-object v2, Lmaps/c/i;->e:Lmaps/bb/d;

    move-object v0, v1

    check-cast v0, Lmaps/c/ly;

    invoke-virtual {v2, v6, v7, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/c/ly;

    invoke-virtual {v2, v6, v8, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/c/ly;

    invoke-virtual {v2, v6, v9, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x21b

    move-object v0, v1

    check-cast v0, Lmaps/c/ly;

    const/4 v0, 0x4

    sget-object v4, Lmaps/c/il;->g:Lmaps/bb/d;

    invoke-virtual {v2, v3, v0, v4}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x21e

    move-object v0, v1

    check-cast v0, Lmaps/c/ly;

    const/4 v0, 0x5

    invoke-virtual {v2, v3, v0, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x41b

    move-object v0, v1

    check-cast v0, Lmaps/c/ly;

    const/4 v0, 0x6

    sget-object v4, Lmaps/c/i;->f:Lmaps/bb/d;

    invoke-virtual {v2, v3, v0, v4}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    sget-object v2, Lmaps/c/i;->f:Lmaps/bb/d;

    const/16 v3, 0x224

    move-object v0, v1

    check-cast v0, Lmaps/c/li;

    invoke-virtual {v2, v3, v7, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x218

    move-object v0, v1

    check-cast v0, Lmaps/c/li;

    sget-object v0, Lmaps/bb/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v3, v8, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x218

    move-object v0, v1

    check-cast v0, Lmaps/c/li;

    sget-object v0, Lmaps/bb/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v3, v9, v0}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    const/16 v3, 0x224

    move-object v0, v1

    check-cast v0, Lmaps/c/li;

    const/4 v0, 0x4

    invoke-virtual {v2, v3, v0, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lmaps/c/li;

    const/4 v0, 0x5

    invoke-virtual {v2, v6, v0, v1}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    move-result-object v0

    const/16 v2, 0x218

    check-cast v1, Lmaps/c/li;

    const/4 v1, 0x6

    sget-object v3, Lmaps/bb/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v0, v2, v1, v3}, Lmaps/bb/d;->a(IILjava/lang/Object;)Lmaps/bb/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
