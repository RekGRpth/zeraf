.class public Lmaps/q/ba;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/be;


# instance fields
.field private final a:[[Lmaps/q/z;

.field private final b:[Lmaps/q/r;

.field private c:Ljava/lang/String;

.field private d:B

.field volatile e:Lmaps/q/k;

.field protected f:Z

.field protected g:[Lmaps/q/k;

.field protected h:[I

.field i:[[F

.field protected j:Z

.field private k:B

.field private l:Lmaps/q/ad;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/q/ba;->f:Z

    iput-boolean v1, p0, Lmaps/q/ba;->j:Z

    iput-byte v1, p0, Lmaps/q/ba;->d:B

    const/4 v0, -0x1

    iput-byte v0, p0, Lmaps/q/ba;->k:B

    new-array v0, v2, [Lmaps/q/r;

    iput-object v0, p0, Lmaps/q/ba;->b:[Lmaps/q/r;

    sget v0, Lmaps/q/z;->a:I

    filled-new-array {v2, v0}, [I

    move-result-object v0

    const-class v1, Lmaps/q/z;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lmaps/q/z;

    iput-object v0, p0, Lmaps/q/ba;->a:[[Lmaps/q/z;

    new-instance v0, Lmaps/q/k;

    invoke-direct {v0}, Lmaps/q/k;-><init>()V

    iput-object v0, p0, Lmaps/q/ba;->e:Lmaps/q/k;

    new-array v0, v2, [Lmaps/q/k;

    iput-object v0, p0, Lmaps/q/ba;->g:[Lmaps/q/k;

    new-array v0, v2, [[F

    iput-object v0, p0, Lmaps/q/ba;->i:[[F

    new-array v0, v2, [I

    iput-object v0, p0, Lmaps/q/ba;->h:[I

    return-void
.end method


# virtual methods
.method a()Lmaps/q/aw;
    .locals 2

    iget-object v0, p0, Lmaps/q/ba;->a:[[Lmaps/q/z;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sget-object v1, Lmaps/q/ap;->a:Lmaps/q/ap;

    invoke-virtual {v1}, Lmaps/q/ap;->a()I

    move-result v1

    aget-object v0, v0, v1

    check-cast v0, Lmaps/q/aw;

    return-object v0
.end method

.method public a(Lmaps/q/ap;I)Lmaps/q/z;
    .locals 4

    const/4 v0, 0x1

    sget-object v1, Lmaps/q/ap;->a:Lmaps/q/ap;

    if-ne p1, v1, :cond_0

    move p2, v0

    :cond_0
    const/4 v2, -0x1

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x5

    if-ge v1, v3, :cond_4

    shl-int v3, v0, v1

    and-int/2addr v3, p2

    if-eqz v3, :cond_2

    :goto_1
    if-ltz v1, :cond_1

    shl-int/2addr v0, v1

    if-eq p2, v0, :cond_3

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid passMask: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/q/ba;->a:[[Lmaps/q/z;

    aget-object v0, v0, v1

    invoke-virtual {p1}, Lmaps/q/ap;->a()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lmaps/q/ba;->c:Ljava/lang/String;

    return-void
.end method

.method a(Lmaps/q/ad;Lmaps/q/al;)V
    .locals 4

    iget v0, p2, Lmaps/q/al;->b:I

    iget-boolean v1, p0, Lmaps/q/ba;->f:Z

    if-nez v1, :cond_0

    iget-object v1, p2, Lmaps/q/al;->a:Lmaps/q/h;

    iget v1, v1, Lmaps/q/h;->b:I

    iget-object v2, p0, Lmaps/q/ba;->h:[I

    aget v2, v2, v0

    if-eq v1, v2, :cond_1

    :cond_0
    iget-object v1, p2, Lmaps/q/al;->a:Lmaps/q/h;

    iget-object v2, p0, Lmaps/q/ba;->g:[Lmaps/q/k;

    aget-object v2, v2, v0

    iget-object v2, v2, Lmaps/q/k;->a:[F

    iget-object v3, p0, Lmaps/q/ba;->e:Lmaps/q/k;

    iget-object v3, v3, Lmaps/q/k;->a:[F

    invoke-virtual {v1, v2, v3}, Lmaps/q/h;->a([F[F)[F

    iget-object v1, p2, Lmaps/q/al;->a:Lmaps/q/h;

    iget-object v2, p0, Lmaps/q/ba;->i:[[F

    aget-object v2, v2, v0

    iget-object v3, p0, Lmaps/q/ba;->e:Lmaps/q/k;

    iget-object v3, v3, Lmaps/q/k;->a:[F

    invoke-virtual {v1, v2, v3}, Lmaps/q/h;->b([F[F)[F

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/q/ba;->f:Z

    iget-object v1, p0, Lmaps/q/ba;->h:[I

    iget v2, p2, Lmaps/q/al;->b:I

    iget-object v3, p2, Lmaps/q/al;->a:Lmaps/q/h;

    iget v3, v3, Lmaps/q/h;->b:I

    aput v3, v1, v2

    :cond_1
    iget-object v1, p0, Lmaps/q/ba;->a:[[Lmaps/q/z;

    aget-object v1, v1, v0

    invoke-virtual {p1, v1, p0, p2}, Lmaps/q/ad;->a([Lmaps/q/z;Lmaps/q/ba;Lmaps/q/al;)V

    iget-object v1, p0, Lmaps/q/ba;->b:[Lmaps/q/r;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Lmaps/q/r;->a(Lmaps/q/ad;)V

    return-void
.end method

.method public a(Lmaps/q/k;)V
    .locals 1

    iget-boolean v0, p0, Lmaps/q/ba;->j:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/q/ba;->e:Lmaps/q/k;

    invoke-virtual {v0, p1}, Lmaps/q/k;->a(Lmaps/q/k;)Lmaps/q/k;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/q/ba;->f:Z

    return-void
.end method

.method public a(Lmaps/q/r;I)V
    .locals 4

    iget-boolean v0, p0, Lmaps/q/ba;->j:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    iget-byte v0, p0, Lmaps/q/ba;->d:B

    or-int/2addr v0, p2

    int-to-byte v0, v0

    iput-byte v0, p0, Lmaps/q/ba;->d:B

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_5

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/2addr v1, p2

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/q/ba;->b:[Lmaps/q/r;

    aget-object v1, v1, v0

    iget-object v2, p0, Lmaps/q/ba;->b:[Lmaps/q/r;

    aput-object p1, v2, v0

    iget-object v2, p0, Lmaps/q/ba;->g:[Lmaps/q/k;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    iget-object v2, p0, Lmaps/q/ba;->g:[Lmaps/q/k;

    new-instance v3, Lmaps/q/k;

    invoke-direct {v3}, Lmaps/q/k;-><init>()V

    aput-object v3, v2, v0

    :cond_1
    iget-object v2, p0, Lmaps/q/ba;->i:[[F

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    iget-object v2, p0, Lmaps/q/ba;->i:[[F

    const/16 v3, 0x10

    new-array v3, v3, [F

    aput-object v3, v2, v0

    :cond_2
    iget-boolean v2, p0, Lmaps/q/ba;->j:Z

    if-eqz v2, :cond_4

    if-eqz v1, :cond_3

    invoke-virtual {v1, p0}, Lmaps/q/r;->a(Lmaps/q/ba;)V

    iget-object v2, p0, Lmaps/q/ba;->l:Lmaps/q/ad;

    sget-object v3, Lmaps/q/f;->a:Lmaps/q/f;

    invoke-virtual {v1, v2, v3}, Lmaps/q/r;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1, p0}, Lmaps/q/r;->b(Lmaps/q/ba;)V

    iget-object v1, p0, Lmaps/q/ba;->l:Lmaps/q/ad;

    sget-object v2, Lmaps/q/f;->c:Lmaps/q/f;

    invoke-virtual {p1, v1, v2}, Lmaps/q/r;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method public a(Lmaps/q/z;I)V
    .locals 5

    const/4 v0, 0x1

    iget-boolean v1, p0, Lmaps/q/ba;->j:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    iget-object v1, p1, Lmaps/q/z;->f:Lmaps/q/ap;

    sget-object v2, Lmaps/q/ap;->a:Lmaps/q/ap;

    if-ne v1, v2, :cond_1

    move p2, v0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_4

    shl-int v2, v0, v1

    and-int/2addr v2, p2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmaps/q/ba;->a:[[Lmaps/q/z;

    aget-object v2, v2, v1

    iget-object v3, p1, Lmaps/q/z;->f:Lmaps/q/ap;

    invoke-virtual {v3}, Lmaps/q/ap;->a()I

    move-result v3

    aget-object v2, v2, v3

    iget-object v3, p0, Lmaps/q/ba;->a:[[Lmaps/q/z;

    aget-object v3, v3, v1

    iget-object v4, p1, Lmaps/q/z;->f:Lmaps/q/ap;

    invoke-virtual {v4}, Lmaps/q/ap;->a()I

    move-result v4

    aput-object p1, v3, v4

    iget-boolean v3, p0, Lmaps/q/ba;->j:Z

    if-eqz v3, :cond_3

    if-eqz v2, :cond_2

    invoke-virtual {v2, p0}, Lmaps/q/z;->b(Lmaps/q/ba;)V

    iget-object v3, p0, Lmaps/q/ba;->l:Lmaps/q/ad;

    sget-object v4, Lmaps/q/f;->a:Lmaps/q/f;

    invoke-virtual {v2, v3, v4}, Lmaps/q/z;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1, p0}, Lmaps/q/z;->a(Lmaps/q/ba;)V

    iget-object v2, p0, Lmaps/q/ba;->l:Lmaps/q/ad;

    sget-object v3, Lmaps/q/f;->c:Lmaps/q/f;

    invoke-virtual {p1, v2, v3}, Lmaps/q/z;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method a(I)Z
    .locals 2

    iget-byte v0, p0, Lmaps/q/ba;->d:B

    iget-byte v1, p0, Lmaps/q/ba;->k:B

    and-int/2addr v0, v1

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lmaps/q/ad;Lmaps/q/f;)Z
    .locals 9

    const/4 v0, 0x0

    iget-boolean v1, p2, Lmaps/q/f;->e:Z

    iget-boolean v2, p0, Lmaps/q/ba;->j:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p2, Lmaps/q/f;->f:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lmaps/q/ba;->l:Lmaps/q/ad;

    iget-object v2, p0, Lmaps/q/ba;->b:[Lmaps/q/r;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    if-eqz v4, :cond_2

    iget-boolean v5, p2, Lmaps/q/f;->f:Z

    if-nez v5, :cond_1

    iget-boolean v5, p2, Lmaps/q/f;->e:Z

    if-eqz v5, :cond_3

    invoke-virtual {v4, p0}, Lmaps/q/r;->b(Lmaps/q/ba;)V

    :cond_1
    :goto_2
    invoke-virtual {v4, p1, p2}, Lmaps/q/r;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v4, p0}, Lmaps/q/r;->a(Lmaps/q/ba;)V

    goto :goto_2

    :cond_4
    const-string v1, "Entity"

    const-string v2, "vertex data setLive"

    invoke-static {v1, v2}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lmaps/q/ba;->a:[[Lmaps/q/z;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_9

    aget-object v5, v3, v2

    array-length v6, v5

    move v1, v0

    :goto_4
    if-ge v1, v6, :cond_8

    aget-object v7, v5, v1

    if-eqz v7, :cond_6

    iget-boolean v8, p2, Lmaps/q/f;->f:Z

    if-nez v8, :cond_5

    iget-boolean v8, p2, Lmaps/q/f;->e:Z

    if-eqz v8, :cond_7

    invoke-virtual {v7, p0}, Lmaps/q/z;->a(Lmaps/q/ba;)V

    :cond_5
    :goto_5
    invoke-virtual {v7, p1, p2}, Lmaps/q/z;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_7
    invoke-virtual {v7, p0}, Lmaps/q/z;->b(Lmaps/q/ba;)V

    goto :goto_5

    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_9
    const-string v0, "Entity"

    const-string v1, "entity state setLive"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p2, Lmaps/q/f;->e:Z

    iput-boolean v0, p0, Lmaps/q/ba;->j:Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()B
    .locals 1

    iget-byte v0, p0, Lmaps/q/ba;->d:B

    return v0
.end method

.method b(Lmaps/q/ap;I)Lmaps/q/z;
    .locals 3

    sget-object v0, Lmaps/q/ap;->a:Lmaps/q/ap;

    if-ne p1, v0, :cond_0

    const/4 p2, 0x0

    :cond_0
    if-ltz p2, :cond_1

    const/4 v0, 0x5

    if-lt p2, v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid passIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lmaps/q/ba;->a:[[Lmaps/q/z;

    aget-object v0, v0, p2

    invoke-virtual {p1}, Lmaps/q/ap;->a()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public b(I)V
    .locals 1

    iget-boolean v0, p0, Lmaps/q/ba;->j:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    int-to-byte v0, p1

    iput-byte v0, p0, Lmaps/q/ba;->k:B

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/q/ba;->c:Ljava/lang/String;

    return-object v0
.end method

.method c(I)Lmaps/q/r;
    .locals 3

    if-ltz p1, :cond_0

    iget-object v0, p0, Lmaps/q/ba;->b:[Lmaps/q/r;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot get the vertex data for pass: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/q/ba;->b:[Lmaps/q/r;

    aget-object v0, v0, p1

    return-object v0
.end method
