.class public abstract Lmaps/q/al;
.super Ljava/lang/Object;


# instance fields
.field protected a:Lmaps/q/h;

.field protected b:I

.field protected c:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/q/al;->a:Lmaps/q/h;

    iput p1, p0, Lmaps/q/al;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/q/al;->b:I

    :goto_0
    and-int/lit8 v0, p1, 0x1

    if-eq v0, v1, :cond_0

    ushr-int/lit8 p1, p1, 0x1

    iget v0, p0, Lmaps/q/al;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/q/al;->b:I

    goto :goto_0

    :cond_0
    if-eq p1, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only a single pass can be included in the mask"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method a()Lmaps/q/h;
    .locals 1

    iget-object v0, p0, Lmaps/q/al;->a:Lmaps/q/h;

    return-object v0
.end method

.method abstract a(Lmaps/q/ad;)V
.end method

.method abstract a(Lmaps/q/ba;)V
.end method

.method a(Lmaps/q/h;)V
    .locals 2

    iget-object v0, p0, Lmaps/q/al;->a:Lmaps/q/h;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "setCamera can only be called once"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/q/al;->a:Lmaps/q/h;

    return-void
.end method

.method abstract b(Lmaps/q/ad;)V
.end method

.method abstract b(Lmaps/q/ba;)V
.end method
