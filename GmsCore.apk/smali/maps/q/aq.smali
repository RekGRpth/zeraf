.class public Lmaps/q/aq;
.super Lmaps/q/ba;


# instance fields
.field private final a:I

.field final d:I


# direct methods
.method public constructor <init>(Lmaps/y/am;)V
    .locals 2

    invoke-direct {p0}, Lmaps/q/ba;-><init>()V

    invoke-virtual {p1}, Lmaps/y/am;->a()I

    move-result v0

    iput v0, p0, Lmaps/q/aq;->d:I

    new-instance v0, Lmaps/q/k;

    invoke-direct {v0}, Lmaps/q/k;-><init>()V

    iput-object v0, p0, Lmaps/q/aq;->e:Lmaps/q/k;

    sget-object v0, Lmaps/q/s;->a:[I

    invoke-virtual {p1}, Lmaps/y/am;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x10

    iput v0, p0, Lmaps/q/aq;->a:I

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x4

    iput v0, p0, Lmaps/q/aq;->a:I

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x8

    iput v0, p0, Lmaps/q/aq;->a:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(Lmaps/q/ap;)Lmaps/q/z;
    .locals 1

    iget v0, p0, Lmaps/q/aq;->a:I

    invoke-super {p0, p1, v0}, Lmaps/q/ba;->a(Lmaps/q/ap;I)Lmaps/q/z;

    move-result-object v0

    return-object v0
.end method

.method public a(Lmaps/q/r;)V
    .locals 1

    iget v0, p0, Lmaps/q/aq;->a:I

    invoke-super {p0, p1, v0}, Lmaps/q/ba;->a(Lmaps/q/r;I)V

    return-void
.end method

.method public a(Lmaps/q/r;I)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lmaps/q/z;)V
    .locals 1

    iget v0, p0, Lmaps/q/aq;->a:I

    invoke-super {p0, p1, v0}, Lmaps/q/ba;->a(Lmaps/q/z;I)V

    return-void
.end method

.method public a(Lmaps/q/z;I)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
