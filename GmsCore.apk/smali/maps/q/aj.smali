.class public abstract Lmaps/q/aj;
.super Lmaps/q/z;


# instance fields
.field protected g:Lmaps/q/bh;

.field protected final h:Ljava/lang/Class;


# direct methods
.method protected constructor <init>(Ljava/lang/Class;)V
    .locals 1

    sget-object v0, Lmaps/q/ap;->d:Lmaps/q/ap;

    invoke-direct {p0, v0}, Lmaps/q/z;-><init>(Lmaps/q/ap;)V

    iput-object p1, p0, Lmaps/q/aj;->h:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public a(Lmaps/q/ad;Lmaps/q/z;)V
    .locals 2

    if-eqz p2, :cond_0

    check-cast p2, Lmaps/q/aj;

    iget-object v0, p2, Lmaps/q/aj;->g:Lmaps/q/bh;

    iget v0, v0, Lmaps/q/bh;->d:I

    iget-object v1, p0, Lmaps/q/aj;->g:Lmaps/q/bh;

    iget v1, v1, Lmaps/q/bh;->d:I

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lmaps/q/aj;->g:Lmaps/q/bh;

    iget v0, v0, Lmaps/q/bh;->d:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    const-string v0, "ShaderState"

    const-string v1, "glUseProgram"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected a(Lmaps/q/ba;Lmaps/q/h;Lmaps/q/k;I)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/q/aj;->g:Lmaps/q/bh;

    iget v0, v0, Lmaps/q/bh;->e:I

    const/4 v1, 0x1

    iget-object v2, p1, Lmaps/q/ba;->g:[Lmaps/q/k;

    aget-object v2, v2, p4

    iget-object v2, v2, Lmaps/q/k;->a:[F

    invoke-static {v0, v1, v3, v2, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    const-string v0, "ShaderState"

    const-string v1, "glUniform"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lmaps/q/ad;Lmaps/q/f;)Z
    .locals 3

    invoke-super {p0, p1, p2}, Lmaps/q/z;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    move-result v0

    iget-object v1, p0, Lmaps/q/aj;->g:Lmaps/q/bh;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lmaps/q/ad;->c()Lmaps/q/j;

    move-result-object v1

    iget-object v2, p0, Lmaps/q/aj;->h:Ljava/lang/Class;

    invoke-interface {v1, v2}, Lmaps/q/j;->a(Ljava/lang/Class;)Lmaps/q/bh;

    move-result-object v1

    iput-object v1, p0, Lmaps/q/aj;->g:Lmaps/q/bh;

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lmaps/q/aj;->g:Lmaps/q/bh;

    invoke-virtual {v1, p1, p2}, Lmaps/q/bh;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    :cond_1
    return v0
.end method

.method public b(Lmaps/q/ad;Lmaps/q/z;)V
    .locals 2

    if-nez p2, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    const-string v0, "ShaderState"

    const-string v1, "glUseProgram"

    invoke-static {v0, v1}, Lmaps/q/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
