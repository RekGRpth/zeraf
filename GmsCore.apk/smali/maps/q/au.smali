.class public Lmaps/q/au;
.super Lmaps/q/z;


# instance fields
.field g:[I

.field h:Lmaps/q/az;

.field private i:Lmaps/q/ab;

.field private volatile j:I

.field private volatile k:I

.field private volatile l:Z

.field private volatile m:I

.field private volatile n:I

.field private volatile o:Z

.field private final p:I

.field private q:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmaps/q/au;-><init>(Lmaps/q/ab;)V

    return-void
.end method

.method public constructor <init>(Lmaps/q/ab;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/q/au;-><init>(Lmaps/q/ab;I)V

    return-void
.end method

.method public constructor <init>(Lmaps/q/ab;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmaps/q/au;-><init>(Lmaps/q/ab;IZ)V

    return-void
.end method

.method public constructor <init>(Lmaps/q/ab;IZ)V
    .locals 3

    const/16 v2, 0x2901

    const/4 v1, 0x0

    invoke-static {p2}, Lmaps/q/au;->a(I)Lmaps/q/ap;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/q/z;-><init>(Lmaps/q/ap;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/q/au;->i:Lmaps/q/ab;

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/q/au;->g:[I

    const/16 v0, 0x2600

    iput v0, p0, Lmaps/q/au;->j:I

    const/16 v0, 0x2601

    iput v0, p0, Lmaps/q/au;->k:I

    iput-boolean v1, p0, Lmaps/q/au;->l:Z

    iput v2, p0, Lmaps/q/au;->m:I

    iput v2, p0, Lmaps/q/au;->n:I

    iput-boolean v1, p0, Lmaps/q/au;->o:Z

    iput-boolean v1, p0, Lmaps/q/au;->q:Z

    new-instance v0, Lmaps/q/az;

    invoke-direct {v0}, Lmaps/q/az;-><init>()V

    iput-object v0, p0, Lmaps/q/au;->h:Lmaps/q/az;

    iput-object p1, p0, Lmaps/q/au;->i:Lmaps/q/ab;

    iput-boolean p3, p0, Lmaps/q/au;->q:Z

    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported texture unit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x84c0

    iput v0, p0, Lmaps/q/au;->p:I

    :goto_0
    return-void

    :pswitch_1
    const v0, 0x84c1

    iput v0, p0, Lmaps/q/au;->p:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(I)Lmaps/q/ap;
    .locals 3

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported texture unit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v0, Lmaps/q/ap;->b:Lmaps/q/ap;

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lmaps/q/ap;->c:Lmaps/q/ap;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected a(Lmaps/q/ab;Z)V
    .locals 2

    iget-boolean v0, p0, Lmaps/q/au;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must be called BEFORE set live"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean p2, p0, Lmaps/q/au;->q:Z

    iput-object p1, p0, Lmaps/q/au;->i:Lmaps/q/ab;

    return-void
.end method

.method a(Lmaps/q/ad;Lmaps/q/z;)V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0xde1

    iget-object v0, p0, Lmaps/q/au;->i:Lmaps/q/ab;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lmaps/q/au;->p:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    iget-object v0, p0, Lmaps/q/au;->g:[I

    aget v0, v0, v3

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    iget-boolean v0, p0, Lmaps/q/au;->o:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x2802

    iget v1, p0, Lmaps/q/au;->m:I

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v0, 0x2803

    iget v1, p0, Lmaps/q/au;->n:I

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    iput-boolean v3, p0, Lmaps/q/au;->o:Z

    :cond_2
    iget-boolean v0, p0, Lmaps/q/au;->l:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x2801

    iget v1, p0, Lmaps/q/au;->j:I

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v0, 0x2800

    iget v1, p0, Lmaps/q/au;->k:I

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    iput-boolean v3, p0, Lmaps/q/au;->l:Z

    goto :goto_0
.end method

.method a(Lmaps/q/ad;Lmaps/q/f;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/16 v3, 0xde1

    invoke-super {p0, p1, p2}, Lmaps/q/z;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v1, p2, Lmaps/q/f;->e:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/q/au;->g:[I

    invoke-static {v4, v1, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    iget-object v1, p0, Lmaps/q/au;->g:[I

    aget v1, v1, v2

    invoke-static {v3, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    iget-object v1, p0, Lmaps/q/au;->i:Lmaps/q/ab;

    invoke-virtual {v1, p1, p2}, Lmaps/q/ab;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    const/16 v1, 0x2801

    iget v2, p0, Lmaps/q/au;->j:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2800

    iget v2, p0, Lmaps/q/au;->k:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2802

    iget v2, p0, Lmaps/q/au;->m:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const/16 v1, 0x2803

    iget v2, p0, Lmaps/q/au;->n:I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    iget-boolean v1, p0, Lmaps/q/au;->q:Z

    if-eqz v1, :cond_0

    invoke-static {v3}, Landroid/opengl/GLES20;->glGenerateMipmap(I)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/q/au;->i:Lmaps/q/ab;

    invoke-virtual {v1, p1, p2}, Lmaps/q/ab;->a(Lmaps/q/ad;Lmaps/q/f;)Z

    iget-object v1, p0, Lmaps/q/au;->g:[I

    invoke-static {v4, v1, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    goto :goto_0
.end method

.method public b(II)V
    .locals 4

    const v3, 0x8370

    const v2, 0x812f

    const/16 v1, 0x2901

    iget-boolean v0, p0, Lmaps/q/au;->c:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    if-eq p1, v1, :cond_1

    if-eq p1, v2, :cond_1

    if-ne p1, v3, :cond_2

    :cond_1
    if-eq p2, v1, :cond_3

    if-eq p2, v2, :cond_3

    if-eq p2, v3, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal Wrap Mode: wrapS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " wrapT = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput p1, p0, Lmaps/q/au;->m:I

    iput p2, p0, Lmaps/q/au;->n:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/q/au;->o:Z

    return-void
.end method

.method b(Lmaps/q/ad;Lmaps/q/z;)V
    .locals 2

    iget-object v0, p0, Lmaps/q/au;->i:Lmaps/q/ab;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_0

    iget v0, p0, Lmaps/q/au;->p:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    const/16 v0, 0xde1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_0
.end method

.method public c(II)V
    .locals 3

    const/16 v2, 0x2601

    const/16 v1, 0x2600

    iget-boolean v0, p0, Lmaps/q/au;->c:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    if-eq p1, v2, :cond_1

    if-eq p1, v1, :cond_1

    const/16 v0, 0x2703

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2701

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2702

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2700

    if-ne p1, v0, :cond_2

    :cond_1
    if-eq p2, v2, :cond_3

    if-eq p2, v1, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal Filter Mode: min = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput p1, p0, Lmaps/q/au;->j:I

    iput p2, p0, Lmaps/q/au;->k:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/q/au;->l:Z

    return-void
.end method
