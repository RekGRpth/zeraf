.class public Lmaps/q/af;
.super Lmaps/q/av;

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# instance fields
.field protected final a:Lmaps/q/ad;


# direct methods
.method public constructor <init>(Lmaps/q/ad;)V
    .locals 0

    invoke-direct {p0}, Lmaps/q/av;-><init>()V

    iput-object p1, p0, Lmaps/q/af;->a:Lmaps/q/ad;

    return-void
.end method


# virtual methods
.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 1

    iget-object v0, p0, Lmaps/q/af;->a:Lmaps/q/ad;

    invoke-virtual {v0}, Lmaps/q/ad;->a()V

    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 0

    invoke-virtual {p0, p2, p3}, Lmaps/q/af;->a(II)V

    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 1

    iget-object v0, p0, Lmaps/q/af;->a:Lmaps/q/ad;

    invoke-virtual {v0, p1, p0}, Lmaps/q/ad;->a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/q/af;)V

    return-void
.end method
