.class public final enum Lmaps/q/f;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/q/f;

.field public static final enum b:Lmaps/q/f;

.field public static final enum c:Lmaps/q/f;

.field public static final enum d:Lmaps/q/f;

.field private static final synthetic g:[Lmaps/q/f;


# instance fields
.field public final e:Z

.field public final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/q/f;

    const-string v1, "NOT_LIVE"

    invoke-direct {v0, v1, v2, v2, v2}, Lmaps/q/f;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lmaps/q/f;->a:Lmaps/q/f;

    new-instance v0, Lmaps/q/f;

    const-string v1, "NOT_LIVE_WITH_NEW_CONTEXT"

    invoke-direct {v0, v1, v3, v2, v3}, Lmaps/q/f;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lmaps/q/f;->b:Lmaps/q/f;

    new-instance v0, Lmaps/q/f;

    const-string v1, "LIVE"

    invoke-direct {v0, v1, v4, v3, v2}, Lmaps/q/f;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lmaps/q/f;->c:Lmaps/q/f;

    new-instance v0, Lmaps/q/f;

    const-string v1, "LIVE_WITH_NEW_CONTEXT"

    invoke-direct {v0, v1, v5, v3, v3}, Lmaps/q/f;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lmaps/q/f;->d:Lmaps/q/f;

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/q/f;

    sget-object v1, Lmaps/q/f;->a:Lmaps/q/f;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/q/f;->b:Lmaps/q/f;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/q/f;->c:Lmaps/q/f;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/q/f;->d:Lmaps/q/f;

    aput-object v1, v0, v5

    sput-object v0, Lmaps/q/f;->g:[Lmaps/q/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, Lmaps/q/f;->e:Z

    iput-boolean p4, p0, Lmaps/q/f;->f:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/q/f;
    .locals 1

    const-class v0, Lmaps/q/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/q/f;

    return-object v0
.end method

.method public static values()[Lmaps/q/f;
    .locals 1

    sget-object v0, Lmaps/q/f;->g:[Lmaps/q/f;

    invoke-virtual {v0}, [Lmaps/q/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/q/f;

    return-object v0
.end method
