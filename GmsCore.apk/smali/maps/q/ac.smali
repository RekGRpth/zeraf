.class public Lmaps/q/ac;
.super Lmaps/q/aq;


# static fields
.field private static final l:Lmaps/q/bf;

.field private static final m:Lmaps/q/bf;


# instance fields
.field final a:I

.field final b:I

.field final c:Lmaps/t/bx;

.field private k:[F

.field private n:F

.field private o:[F

.field private p:[F

.field private q:Z

.field private r:F

.field private final s:Z

.field private t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/high16 v2, 0x3f800000

    const/4 v1, 0x0

    new-instance v0, Lmaps/q/bf;

    invoke-direct {v0, v2, v1, v1}, Lmaps/q/bf;-><init>(FFF)V

    sput-object v0, Lmaps/q/ac;->l:Lmaps/q/bf;

    new-instance v0, Lmaps/q/bf;

    invoke-direct {v0, v1, v1, v2}, Lmaps/q/bf;-><init>(FFF)V

    sput-object v0, Lmaps/q/ac;->m:Lmaps/q/bf;

    return-void
.end method

.method public constructor <init>(Lmaps/y/am;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmaps/q/ac;-><init>(Lmaps/y/am;Z)V

    return-void
.end method

.method public constructor <init>(Lmaps/y/am;Z)V
    .locals 3

    const/4 v2, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lmaps/q/aq;-><init>(Lmaps/y/am;)V

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/q/ac;->k:[F

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/q/ac;->c:Lmaps/t/bx;

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/q/ac;->o:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/q/ac;->p:[F

    iput-boolean v1, p0, Lmaps/q/ac;->t:Z

    iput v1, p0, Lmaps/q/ac;->b:I

    iput v1, p0, Lmaps/q/ac;->a:I

    iput-boolean p2, p0, Lmaps/q/ac;->s:Z

    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 1

    iget-boolean v0, p0, Lmaps/q/ac;->j:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    iput p1, p0, Lmaps/q/ac;->r:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/q/ac;->f:Z

    return-void
.end method

.method a(Lmaps/q/ad;Lmaps/q/al;)V
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-boolean v0, p0, Lmaps/q/ac;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p2, Lmaps/q/al;->a:Lmaps/q/h;

    iget v0, v0, Lmaps/q/h;->b:I

    iget-object v1, p0, Lmaps/q/ac;->h:[I

    iget v2, p2, Lmaps/q/al;->b:I

    aget v1, v1, v2

    if-eq v0, v1, :cond_4

    :cond_0
    iget-object v0, p2, Lmaps/q/al;->a:Lmaps/q/h;

    check-cast v0, Lmaps/bq/d;

    iget-boolean v1, p0, Lmaps/q/ac;->q:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/q/ac;->c:Lmaps/t/bx;

    iget-boolean v2, p0, Lmaps/q/ac;->s:Z

    invoke-virtual {v0, v1, v2}, Lmaps/bq/d;->a(Lmaps/t/bx;Z)F

    move-result v1

    const/high16 v2, 0x3f800000

    invoke-virtual {v0, v2, v1}, Lmaps/bq/d;->a(FF)F

    move-result v1

    iput v1, p0, Lmaps/q/ac;->n:F

    :cond_1
    iget-boolean v1, p0, Lmaps/q/ac;->s:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmaps/q/ac;->c:Lmaps/t/bx;

    iget v2, p0, Lmaps/q/ac;->n:F

    iget-object v3, p0, Lmaps/q/ac;->k:[F

    invoke-static {v4, v0, v1, v2, v3}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V

    :goto_0
    iget-object v1, p0, Lmaps/q/ac;->e:Lmaps/q/k;

    invoke-virtual {v1}, Lmaps/q/k;->a()Lmaps/q/k;

    iget-object v1, p0, Lmaps/q/ac;->e:Lmaps/q/k;

    iget-object v2, p0, Lmaps/q/ac;->k:[F

    aget v2, v2, v6

    iget-object v3, p0, Lmaps/q/ac;->k:[F

    aget v3, v3, v5

    iget-object v4, p0, Lmaps/q/ac;->k:[F

    aget v4, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Lmaps/q/k;->a(FFF)Lmaps/q/k;

    iget-boolean v1, p0, Lmaps/q/ac;->t:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/q/ac;->e:Lmaps/q/k;

    sget-object v2, Lmaps/q/ac;->m:Lmaps/q/bf;

    invoke-virtual {v0}, Lmaps/bq/d;->q()F

    move-result v3

    neg-float v3, v3

    invoke-virtual {v1, v2, v3}, Lmaps/q/k;->a(Lmaps/q/bf;F)Lmaps/q/k;

    iget-object v1, p0, Lmaps/q/ac;->e:Lmaps/q/k;

    sget-object v2, Lmaps/q/ac;->l:Lmaps/q/bf;

    invoke-virtual {v0}, Lmaps/bq/d;->r()F

    move-result v0

    invoke-virtual {v1, v2, v0}, Lmaps/q/k;->a(Lmaps/q/bf;F)Lmaps/q/k;

    :cond_2
    iget v0, p0, Lmaps/q/ac;->r:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/q/ac;->e:Lmaps/q/k;

    sget-object v1, Lmaps/q/ac;->m:Lmaps/q/bf;

    iget v2, p0, Lmaps/q/ac;->r:F

    invoke-virtual {v0, v1, v2}, Lmaps/q/k;->a(Lmaps/q/bf;F)Lmaps/q/k;

    :cond_3
    iget-object v0, p0, Lmaps/q/ac;->k:[F

    const/4 v1, 0x3

    aget v0, v0, v1

    iget-object v1, p0, Lmaps/q/ac;->e:Lmaps/q/k;

    iget-object v2, p0, Lmaps/q/ac;->o:[F

    aget v2, v2, v6

    mul-float/2addr v2, v0

    iget-object v3, p0, Lmaps/q/ac;->o:[F

    aget v3, v3, v5

    mul-float/2addr v3, v0

    iget-object v4, p0, Lmaps/q/ac;->o:[F

    aget v4, v4, v7

    mul-float/2addr v0, v4

    invoke-virtual {v1, v2, v3, v0}, Lmaps/q/k;->b(FFF)Lmaps/q/k;

    iget-object v0, p0, Lmaps/q/ac;->e:Lmaps/q/k;

    iget-object v1, p0, Lmaps/q/ac;->p:[F

    aget v1, v1, v6

    iget-object v2, p0, Lmaps/q/ac;->p:[F

    aget v2, v2, v5

    iget-object v3, p0, Lmaps/q/ac;->p:[F

    aget v3, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lmaps/q/k;->c(FFF)Lmaps/q/k;

    iput-boolean v5, p0, Lmaps/q/ac;->f:Z

    :cond_4
    invoke-super {p0, p1, p2}, Lmaps/q/aq;->a(Lmaps/q/ad;Lmaps/q/al;)V

    return-void

    :cond_5
    iget-object v1, p0, Lmaps/q/ac;->c:Lmaps/t/bx;

    iget v2, p0, Lmaps/q/ac;->n:F

    iget-object v3, p0, Lmaps/q/ac;->k:[F

    invoke-static {v4, v0, v1, v2, v3}, Lmaps/af/d;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V

    goto :goto_0
.end method

.method public a(Lmaps/q/k;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lmaps/t/bx;F)V
    .locals 0

    invoke-virtual {p0, p1, p2, p2, p2}, Lmaps/q/ac;->a(Lmaps/t/bx;FFF)V

    return-void
.end method

.method public a(Lmaps/t/bx;FFF)V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lmaps/q/ac;->j:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/q/ac;->c:Lmaps/t/bx;

    invoke-virtual {v0, p1}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/q/ac;->o:[F

    const/4 v1, 0x0

    aput p2, v0, v1

    iget-object v0, p0, Lmaps/q/ac;->o:[F

    aput p3, v0, v2

    iget-object v0, p0, Lmaps/q/ac;->o:[F

    const/4 v1, 0x2

    aput p4, v0, v1

    iput-boolean v2, p0, Lmaps/q/ac;->q:Z

    iput-boolean v2, p0, Lmaps/q/ac;->f:Z

    return-void
.end method

.method public b(Lmaps/t/bx;F)V
    .locals 1

    iget-boolean v0, p0, Lmaps/q/ac;->j:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/q/bd;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/q/ac;->c:Lmaps/t/bx;

    invoke-virtual {v0, p1}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    iput p2, p0, Lmaps/q/ac;->n:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/q/ac;->q:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/q/ac;->f:Z

    return-void
.end method
