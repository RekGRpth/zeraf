.class public abstract Lmaps/q/z;
.super Ljava/lang/Object;


# static fields
.field public static final a:I

.field public static final b:I


# instance fields
.field protected c:Z

.field protected d:I

.field protected e:Lmaps/q/ad;

.field final f:Lmaps/q/ap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lmaps/q/ap;->values()[Lmaps/q/ap;

    move-result-object v0

    array-length v0, v0

    sput v0, Lmaps/q/z;->a:I

    sget-object v0, Lmaps/q/ap;->b:Lmaps/q/ap;

    invoke-virtual {v0}, Lmaps/q/ap;->a()I

    move-result v0

    sput v0, Lmaps/q/z;->b:I

    return-void
.end method

.method protected constructor <init>(Lmaps/q/ap;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/q/z;->c:Z

    iput v0, p0, Lmaps/q/z;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/q/z;->e:Lmaps/q/ad;

    iput-object p1, p0, Lmaps/q/z;->f:Lmaps/q/ap;

    return-void
.end method


# virtual methods
.method abstract a(Lmaps/q/ad;Lmaps/q/z;)V
.end method

.method a(Lmaps/q/ba;)V
    .locals 1

    iget v0, p0, Lmaps/q/z;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/q/z;->d:I

    return-void
.end method

.method a(Lmaps/q/ad;Lmaps/q/f;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p2, Lmaps/q/f;->e:Z

    iget-boolean v2, p0, Lmaps/q/z;->c:Z

    if-ne v1, v2, :cond_1

    iget-boolean v1, p2, Lmaps/q/f;->f:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p2, Lmaps/q/f;->e:Z

    if-nez v1, :cond_2

    iget-boolean v1, p2, Lmaps/q/f;->f:Z

    if-nez v1, :cond_2

    iget v1, p0, Lmaps/q/z;->d:I

    if-nez v1, :cond_0

    :cond_2
    iget-boolean v0, p2, Lmaps/q/f;->e:Z

    iput-boolean v0, p0, Lmaps/q/z;->c:Z

    iget-boolean v0, p0, Lmaps/q/z;->c:Z

    if-eqz v0, :cond_3

    :goto_1
    iput-object p1, p0, Lmaps/q/z;->e:Lmaps/q/ad;

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    goto :goto_1
.end method

.method abstract b(Lmaps/q/ad;Lmaps/q/z;)V
.end method

.method b(Lmaps/q/ba;)V
    .locals 1

    iget v0, p0, Lmaps/q/z;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/q/z;->d:I

    return-void
.end method
