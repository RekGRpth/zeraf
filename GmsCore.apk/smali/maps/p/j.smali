.class public final enum Lmaps/p/j;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/p/j;

.field public static final enum b:Lmaps/p/j;

.field public static final enum c:Lmaps/p/j;

.field public static final enum d:Lmaps/p/j;

.field public static final enum e:Lmaps/p/j;

.field public static final enum f:Lmaps/p/j;

.field public static final enum g:Lmaps/p/j;

.field public static final enum h:Lmaps/p/j;

.field public static final enum i:Lmaps/p/j;

.field private static final synthetic j:[Lmaps/p/j;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lmaps/p/j;

    const-string v1, "BASE"

    invoke-direct {v0, v1, v3}, Lmaps/p/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/p/j;->a:Lmaps/p/j;

    new-instance v0, Lmaps/p/j;

    const-string v1, "DROP_SHADOWS_OUTER"

    invoke-direct {v0, v1, v4}, Lmaps/p/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/p/j;->b:Lmaps/p/j;

    new-instance v0, Lmaps/p/j;

    const-string v1, "ELEVATED_COLOR"

    invoke-direct {v0, v1, v5}, Lmaps/p/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/p/j;->c:Lmaps/p/j;

    new-instance v0, Lmaps/p/j;

    const-string v1, "UNDERGROUND_MODE_MASK"

    invoke-direct {v0, v1, v6}, Lmaps/p/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/p/j;->d:Lmaps/p/j;

    new-instance v0, Lmaps/p/j;

    const-string v1, "UNDERGROUND_STENCIL"

    invoke-direct {v0, v1, v7}, Lmaps/p/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/p/j;->e:Lmaps/p/j;

    new-instance v0, Lmaps/p/j;

    const-string v1, "UNDERGROUND_COLOR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lmaps/p/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/p/j;->f:Lmaps/p/j;

    new-instance v0, Lmaps/p/j;

    const-string v1, "DROP_SHADOWS_INNER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lmaps/p/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/p/j;->g:Lmaps/p/j;

    new-instance v0, Lmaps/p/j;

    const-string v1, "ANIMATED_ELEVATED_COLOR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lmaps/p/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/p/j;->h:Lmaps/p/j;

    new-instance v0, Lmaps/p/j;

    const-string v1, "DEFAULT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lmaps/p/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/p/j;->i:Lmaps/p/j;

    const/16 v0, 0x9

    new-array v0, v0, [Lmaps/p/j;

    sget-object v1, Lmaps/p/j;->a:Lmaps/p/j;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/p/j;->b:Lmaps/p/j;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/p/j;->c:Lmaps/p/j;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/p/j;->d:Lmaps/p/j;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/p/j;->e:Lmaps/p/j;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/p/j;->f:Lmaps/p/j;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/p/j;->g:Lmaps/p/j;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/p/j;->h:Lmaps/p/j;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lmaps/p/j;->i:Lmaps/p/j;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/p/j;->j:[Lmaps/p/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/p/j;
    .locals 1

    const-class v0, Lmaps/p/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/p/j;

    return-object v0
.end method

.method public static values()[Lmaps/p/j;
    .locals 1

    sget-object v0, Lmaps/p/j;->j:[Lmaps/p/j;

    invoke-virtual {v0}, [Lmaps/p/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/p/j;

    return-object v0
.end method
