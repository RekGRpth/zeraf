.class public Lmaps/p/k;
.super Ljava/lang/Object;


# static fields
.field private static final b:Lmaps/t/ah;


# instance fields
.field private final a:Lmaps/p/t;

.field private final c:Ljava/util/Map;

.field private d:Lmaps/t/bx;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lmaps/t/ah;

    invoke-direct {v0, v1, v1, v1}, Lmaps/t/ah;-><init>(III)V

    sput-object v0, Lmaps/p/k;->b:Lmaps/t/ah;

    return-void
.end method

.method private constructor <init>(Lmaps/p/t;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/p/k;->c:Ljava/util/Map;

    iput-object p1, p0, Lmaps/p/k;->a:Lmaps/p/t;

    return-void
.end method

.method private a(IIILmaps/o/c;Lmaps/p/t;)Lmaps/p/ap;
    .locals 6

    iget-object v0, p0, Lmaps/p/k;->a:Lmaps/p/t;

    if-eq p5, v0, :cond_1

    invoke-virtual {p5, p4}, Lmaps/p/t;->a(Lmaps/o/c;)Lmaps/p/ap;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p5, p4}, Lmaps/p/t;->a(Lmaps/o/c;)Lmaps/p/ap;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "ZoomTableQuadTree"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No zoom table for tile type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lmaps/p/ap;->a:Lmaps/p/ap;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    add-int/lit8 v3, p3, -0x1

    invoke-virtual {p5, p1, p2, v3}, Lmaps/p/t;->a(III)I

    move-result v0

    invoke-virtual {p5, v0}, Lmaps/p/t;->a(I)Lmaps/p/t;

    move-result-object v5

    if-nez v5, :cond_2

    iget-object v0, p0, Lmaps/p/k;->a:Lmaps/p/t;

    invoke-virtual {v0, p4}, Lmaps/p/t;->a(Lmaps/o/c;)Lmaps/p/ap;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "ZoomTableQuadTree"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No root zoom table for tile type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lmaps/p/ap;->a:Lmaps/p/ap;

    goto :goto_0

    :cond_2
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lmaps/p/k;->a(IIILmaps/o/c;Lmaps/p/t;)Lmaps/p/ap;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lmaps/bb/c;)Lmaps/p/k;
    .locals 15

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmaps/bb/c;->j(I)I

    move-result v9

    if-nez v9, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "ZoomTableQuadTree.fromProto"

    invoke-static {v0}, Lmaps/ah/a;->a(Ljava/lang/String;)V

    new-instance v0, Lmaps/p/t;

    invoke-direct {v0}, Lmaps/p/t;-><init>()V

    const/4 v1, 0x0

    move v8, v1

    :goto_1
    if-ge v8, v9, :cond_7

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v8}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v10

    const/4 v1, 0x3

    invoke-virtual {v10, v1}, Lmaps/bb/c;->d(I)I

    move-result v3

    const/4 v1, 0x2

    invoke-virtual {v10, v1}, Lmaps/bb/c;->j(I)I

    move-result v4

    const/4 v1, 0x5

    invoke-virtual {v10, v1}, Lmaps/bb/c;->d(I)I

    move-result v6

    if-lez v4, :cond_2

    new-array v1, v4, [I

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_3

    const/4 v5, 0x2

    invoke-virtual {v10, v5, v2}, Lmaps/bb/c;->c(II)I

    move-result v5

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v3, 0x1

    sub-int/2addr v1, v6

    new-array v1, v1, [I

    const/4 v2, 0x0

    :goto_3
    sub-int v4, v3, v6

    if-gt v2, v4, :cond_3

    add-int v4, v2, v6

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Lmaps/bb/c;->d(I)I

    move-result v2

    new-instance v5, Lmaps/p/ap;

    invoke-direct {v5, v1, v6, v2, v3}, Lmaps/p/ap;-><init>([IIII)V

    const/4 v1, 0x4

    invoke-virtual {v10, v1}, Lmaps/bb/c;->j(I)I

    move-result v11

    const/4 v1, 0x0

    move v7, v1

    :goto_4
    if-ge v7, v11, :cond_6

    const/4 v1, 0x4

    invoke-virtual {v10, v1, v7}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v12

    const/4 v1, 0x2

    invoke-virtual {v12, v1}, Lmaps/bb/c;->d(I)I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {v12, v2}, Lmaps/bb/c;->d(I)I

    move-result v2

    const/4 v3, 0x4

    invoke-virtual {v12, v3}, Lmaps/bb/c;->d(I)I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v12, v4}, Lmaps/bb/c;->j(I)I

    move-result v13

    new-instance v14, Lmaps/t/ah;

    invoke-direct {v14, v1, v2, v3}, Lmaps/t/ah;-><init>(III)V

    const/4 v1, 0x0

    move v6, v1

    :goto_5
    if-ge v6, v13, :cond_5

    const/4 v1, 0x1

    invoke-virtual {v12, v1, v6}, Lmaps/bb/c;->c(II)I

    move-result v1

    invoke-static {v1}, Lmaps/o/c;->a(I)Lmaps/o/c;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v14}, Lmaps/t/ah;->d()I

    move-result v1

    invoke-virtual {v14}, Lmaps/t/ah;->e()I

    move-result v2

    invoke-virtual {v14}, Lmaps/t/ah;->c()I

    move-result v3

    invoke-virtual/range {v0 .. v5}, Lmaps/p/t;->a(IIILmaps/o/c;Lmaps/p/ap;)V

    :cond_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_5

    :cond_5
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_4

    :cond_6
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_1

    :cond_7
    const-string v1, "ZoomTableQuadTree.fromProto"

    invoke-static {v1}, Lmaps/ah/a;->b(Ljava/lang/String;)V

    new-instance v1, Lmaps/p/k;

    invoke-direct {v1, v0}, Lmaps/p/k;-><init>(Lmaps/p/t;)V

    move-object v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Lmaps/t/bx;Lmaps/o/c;)Lmaps/p/ap;
    .locals 6

    iget-object v0, p0, Lmaps/p/k;->d:Lmaps/t/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/k;->d:Lmaps/t/bx;

    invoke-virtual {v0, p1}, Lmaps/t/bx;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/p/k;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/ap;

    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/p/k;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_1
    const/16 v0, 0x1e

    invoke-static {v0, p1}, Lmaps/t/ah;->a(ILmaps/t/bx;)Lmaps/t/ah;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lmaps/p/k;->b:Lmaps/t/ah;

    :cond_2
    iput-object p1, p0, Lmaps/p/k;->d:Lmaps/t/bx;

    invoke-virtual {v0}, Lmaps/t/ah;->d()I

    move-result v1

    invoke-virtual {v0}, Lmaps/t/ah;->e()I

    move-result v2

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v3

    iget-object v5, p0, Lmaps/p/k;->a:Lmaps/p/t;

    move-object v0, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lmaps/p/k;->a(IIILmaps/o/c;Lmaps/p/t;)Lmaps/p/ap;

    move-result-object v0

    iget-object v1, p0, Lmaps/p/k;->c:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
