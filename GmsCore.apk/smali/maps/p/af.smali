.class Lmaps/p/af;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lmaps/p/l;

.field private final c:Lmaps/t/cv;

.field private final d:F

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lmaps/p/l;Lmaps/t/cv;FIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/p/af;->a:Ljava/lang/String;

    iput-object p2, p0, Lmaps/p/af;->b:Lmaps/p/l;

    iput-object p3, p0, Lmaps/p/af;->c:Lmaps/t/cv;

    iput p4, p0, Lmaps/p/af;->d:F

    iput p5, p0, Lmaps/p/af;->e:I

    iput p6, p0, Lmaps/p/af;->f:I

    iput p7, p0, Lmaps/p/af;->g:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/p/af;

    if-eqz v2, :cond_4

    check-cast p1, Lmaps/p/af;

    iget v2, p1, Lmaps/p/af;->d:F

    iget v3, p0, Lmaps/p/af;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p1, Lmaps/p/af;->e:I

    iget v3, p0, Lmaps/p/af;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p1, Lmaps/p/af;->f:I

    iget v3, p0, Lmaps/p/af;->f:I

    if-ne v2, v3, :cond_3

    iget v2, p1, Lmaps/p/af;->g:I

    iget v3, p0, Lmaps/p/af;->g:I

    if-ne v2, v3, :cond_3

    iget-object v2, p1, Lmaps/p/af;->b:Lmaps/p/l;

    iget-object v3, p0, Lmaps/p/af;->b:Lmaps/p/l;

    if-ne v2, v3, :cond_3

    iget-object v2, p1, Lmaps/p/af;->c:Lmaps/t/cv;

    iget-object v3, p0, Lmaps/p/af;->c:Lmaps/t/cv;

    if-eq v2, v3, :cond_2

    iget-object v2, p1, Lmaps/p/af;->c:Lmaps/t/cv;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lmaps/p/af;->c:Lmaps/t/cv;

    iget-object v3, p0, Lmaps/p/af;->c:Lmaps/t/cv;

    invoke-virtual {v2, v3}, Lmaps/t/cv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p1, Lmaps/p/af;->a:Ljava/lang/String;

    iget-object v3, p0, Lmaps/p/af;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lmaps/p/af;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/p/af;->b:Lmaps/p/l;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/p/af;->c:Lmaps/t/cv;

    if-eqz v1, :cond_0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lmaps/p/af;->c:Lmaps/t/cv;

    invoke-virtual {v1}, Lmaps/t/cv;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/p/af;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/p/af;->e:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/p/af;->f:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/p/af;->g:I

    add-int/2addr v0, v1

    return v0
.end method
