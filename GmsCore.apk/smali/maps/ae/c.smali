.class public Lmaps/ae/c;
.super Lmaps/ae/b;


# static fields
.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;

.field public static h:[Ljava/lang/String;

.field private static k:Ljava/lang/String;

.field private static volatile l:Ljava/lang/Boolean;

.field private static r:Ljava/lang/Thread;


# instance fields
.field protected i:Landroid/content/Context;

.field protected j:I

.field private final m:Lmaps/bg/a;

.field private final n:Lmaps/bg/b;

.field private o:F

.field private final p:F

.field private final q:F

.field private volatile s:Z

.field private t:Lmaps/ae/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lmaps/ae/h;->c:Ljava/lang/String;

    sput-object v0, Lmaps/ae/c;->f:Ljava/lang/String;

    sget-object v0, Lmaps/ae/h;->a:Ljava/lang/String;

    sput-object v0, Lmaps/ae/c;->g:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    const/high16 v2, 0x43200000

    const/4 v1, 0x0

    invoke-direct {p0}, Lmaps/ae/b;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ae/c;->s:Z

    invoke-static {}, Lmaps/ah/b;->a()V

    iput-object v1, p0, Lmaps/ae/c;->i:Landroid/content/Context;

    iput-object v1, p0, Lmaps/ae/c;->m:Lmaps/bg/a;

    iput-object v1, p0, Lmaps/ae/c;->n:Lmaps/bg/b;

    const/16 v0, 0xa0

    iput v0, p0, Lmaps/ae/c;->j:I

    iput v2, p0, Lmaps/ae/c;->p:F

    iput v2, p0, Lmaps/ae/c;->q:F

    const/high16 v0, 0x3f800000

    iput v0, p0, Lmaps/ae/c;->o:F

    invoke-direct {p0}, Lmaps/ae/c;->y()V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    new-instance v1, Lmaps/ce/a;

    invoke-direct {v1, p1}, Lmaps/ce/a;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v1}, Lmaps/ae/c;-><init>(Landroid/content/Context;Lmaps/bj/b;Lmaps/bg/a;)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lmaps/bj/b;Lmaps/bg/a;)V
    .locals 6

    const/16 v0, 0xa0

    const-wide/high16 v4, 0x3fd0000000000000L

    invoke-direct {p0, p1, p2}, Lmaps/ae/b;-><init>(Landroid/content/Context;Lmaps/bj/b;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/ae/c;->s:Z

    iput-object p1, p0, Lmaps/ae/c;->i:Landroid/content/Context;

    iput-object p3, p0, Lmaps/ae/c;->m:Lmaps/bg/a;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    sput-object v1, Lmaps/ae/c;->r:Ljava/lang/Thread;

    invoke-direct {p0}, Lmaps/ae/c;->y()V

    if-eqz p1, :cond_2

    sget-boolean v1, Lmaps/ae/h;->L:Z

    if-eqz v1, :cond_1

    :goto_0
    iput v0, p0, Lmaps/ae/c;->j:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lmaps/ae/c;->o:F

    :goto_1
    iget v0, p0, Lmaps/ae/c;->j:I

    int-to-float v0, v0

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v2, v1, Landroid/util/DisplayMetrics;->xdpi:F

    sub-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v2, v2

    cmpl-double v2, v2, v4

    if-gtz v2, :cond_0

    iget v2, v1, Landroid/util/DisplayMetrics;->ydpi:F

    sub-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v2, v2

    cmpl-double v2, v2, v4

    if-lez v2, :cond_3

    :cond_0
    iput v0, p0, Lmaps/ae/c;->p:F

    iput v0, p0, Lmaps/ae/c;->q:F

    :goto_2
    new-instance v0, Lmaps/ce/b;

    invoke-direct {v0}, Lmaps/ce/b;-><init>()V

    iput-object v0, p0, Lmaps/ae/c;->n:Lmaps/bg/b;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ae/c;->a(Ljava/util/Locale;)V

    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    goto :goto_0

    :cond_2
    iput v0, p0, Lmaps/ae/c;->j:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lmaps/ae/c;->o:F

    goto :goto_1

    :cond_3
    iget v0, v1, Landroid/util/DisplayMetrics;->xdpi:F

    iput v0, p0, Lmaps/ae/c;->p:F

    iget v0, v1, Landroid/util/DisplayMetrics;->ydpi:F

    iput v0, p0, Lmaps/ae/c;->q:F

    goto :goto_2

    :cond_4
    iput v0, p0, Lmaps/ae/c;->p:F

    iput v0, p0, Lmaps/ae/c;->q:F

    goto :goto_2
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lmaps/ae/c;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.google.settings/partner"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "name=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "value"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v6

    :goto_2
    :try_start_2
    sget-boolean v2, Lmaps/ae/h;->f:Z

    if-eqz v2, :cond_1

    const-string v2, "MAPS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error getting distribution channel for key "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2

    :cond_3
    move-object v0, v6

    goto :goto_1

    :cond_4
    move-object v0, v6

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {p0, p1}, Lmaps/ae/g;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lmaps/ae/g;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lmaps/ae/g;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/cp/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lmaps/cp/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lmaps/ae/c;
    .locals 2

    sget-object v1, Lmaps/ae/c;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/ae/c;->a:Lmaps/ae/b;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ae/c;

    invoke-direct {v0, p0}, Lmaps/ae/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lmaps/ae/c;->a:Lmaps/ae/b;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-boolean v0, Lmaps/ae/h;->k:Z

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/ae/c;->a:Lmaps/ae/b;

    instance-of v0, v0, Lmaps/ae/c;

    invoke-static {v0}, Lmaps/ah/d;->a(Z)V

    :cond_1
    sget-object v0, Lmaps/ae/c;->a:Lmaps/ae/b;

    check-cast v0, Lmaps/ae/c;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static e()Lmaps/ae/c;
    .locals 1

    sget-object v0, Lmaps/ae/c;->a:Lmaps/ae/b;

    check-cast v0, Lmaps/ae/c;

    return-object v0
.end method

.method public static declared-synchronized g()Ljava/lang/String;
    .locals 2

    const-class v1, Lmaps/ae/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lmaps/ae/c;->a:Lmaps/ae/b;

    check-cast v0, Lmaps/ae/c;

    iget-object v0, v0, Lmaps/ae/c;->t:Lmaps/ae/g;

    invoke-virtual {v0}, Lmaps/ae/g;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static i()Ljava/lang/String;
    .locals 1

    sget-object v0, Lmaps/ae/c;->k:Ljava/lang/String;

    return-object v0
.end method

.method public static x()Ljava/lang/String;
    .locals 7

    const/16 v6, 0x5f

    const/16 v5, 0x2d

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "android:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "-"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private y()V
    .locals 1

    iget-object v0, p0, Lmaps/ae/c;->t:Lmaps/ae/g;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lmaps/ae/g;->a(Ljava/lang/String;)Lmaps/ae/g;

    move-result-object v0

    iput-object v0, p0, Lmaps/ae/c;->t:Lmaps/ae/g;

    :cond_0
    invoke-virtual {p0}, Lmaps/ae/c;->f()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/ae/c;->k:Ljava/lang/String;

    sget-object v0, Lmaps/ae/c;->k:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "unknown"

    sput-object v0, Lmaps/ae/c;->k:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private z()[Ljava/lang/String;
    .locals 2

    sget-object v0, Lmaps/ae/c;->h:[Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lmaps/ae/c;->g:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lmaps/cp/a;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ae/c;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/ae/c;->h:[Ljava/lang/String;

    :cond_0
    sget-object v0, Lmaps/ae/c;->h:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)I
    .locals 4

    int-to-double v0, p1

    invoke-virtual {p0}, Lmaps/ae/c;->o()D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Lmaps/ah/g;->a(D)I

    move-result v0

    return v0
.end method

.method public a(Ljava/util/Locale;)V
    .locals 3

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ae/c;->h()Lmaps/ae/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/ae/g;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, Lmaps/ae/c;->h()Lmaps/ae/g;

    move-result-object v1

    invoke-direct {p0}, Lmaps/ae/c;->z()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lmaps/ae/c;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/ae/g;->d(Ljava/lang/String;)V

    return-void
.end method

.method public a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Lmaps/ae/c;->u()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    invoke-static {v3}, Lmaps/ae/g;->g(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_1
.end method

.method public b()Lmaps/ae/d;
    .locals 1

    iget-object v0, p0, Lmaps/ae/c;->e:Lmaps/ae/d;

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 3

    const-string v0, "Web"

    const-string v0, "maps_client_id"

    invoke-direct {p0, v0}, Lmaps/ae/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lmaps/cp/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, "Web"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    sget-boolean v0, Lmaps/ae/h;->n:Z

    if-eqz v0, :cond_0

    const-string v0, "-dogfood"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method protected h()Lmaps/ae/g;
    .locals 1

    iget-object v0, p0, Lmaps/ae/c;->t:Lmaps/ae/g;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    sget-object v0, Lmaps/ae/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method public k()Lmaps/ae/f;
    .locals 2

    new-instance v0, Lmaps/ae/f;

    invoke-virtual {p0}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/ae/f;-><init>(Lmaps/bj/b;)V

    return-object v0
.end method

.method public l()I
    .locals 1

    iget v0, p0, Lmaps/ae/c;->j:I

    return v0
.end method

.method public m()F
    .locals 1

    iget v0, p0, Lmaps/ae/c;->p:F

    return v0
.end method

.method public n()F
    .locals 1

    iget v0, p0, Lmaps/ae/c;->q:F

    return v0
.end method

.method public o()D
    .locals 2

    iget v0, p0, Lmaps/ae/c;->o:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public p()Z
    .locals 2

    invoke-virtual {p0}, Lmaps/ae/c;->l()I

    move-result v0

    const/16 v1, 0xc8

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->L:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmaps/ae/c;->s:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-boolean v0, Lmaps/ae/h;->k:Z

    if-eqz v0, :cond_0

    const-string v0, "This code was designed to run on the UI thread but is being called from a different thread."

    sget-object v1, Lmaps/ae/c;->r:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lmaps/ah/d;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public r()V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->L:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmaps/ae/c;->s:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-boolean v0, Lmaps/ae/h;->k:Z

    if-eqz v0, :cond_0

    const-string v0, "This code was designed to run on a non-UI thread but is being called from the UI thread."

    sget-object v1, Lmaps/ae/c;->r:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lmaps/ah/d;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public s()Z
    .locals 2

    sget-object v0, Lmaps/ae/c;->r:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 2

    sget-object v0, Lmaps/ae/c;->l:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ae/c;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lmaps/ae/c;->l:Ljava/lang/Boolean;

    :cond_0
    sget-object v0, Lmaps/ae/c;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method protected u()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Landroid/location/LocationManager;
    .locals 2

    iget-object v0, p0, Lmaps/ae/c;->i:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    return-object v0
.end method

.method public w()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lmaps/ae/c;->i:Landroid/content/Context;

    return-object v0
.end method
