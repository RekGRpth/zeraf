.class public Lmaps/ay/e;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lmaps/ay/e;


# instance fields
.field private final b:Lmaps/ay/m;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/ay/a;

    invoke-direct {v0}, Lmaps/ay/a;-><init>()V

    sput-object v0, Lmaps/ay/e;->a:Lmaps/ay/e;

    return-void
.end method

.method private constructor <init>(Lmaps/ay/m;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ay/e;->c:I

    iput-object p1, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ay/m;Lmaps/ay/u;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ay/e;-><init>(Lmaps/ay/m;)V

    return-void
.end method

.method public static a(Lmaps/ay/m;)Lmaps/ay/e;
    .locals 2

    new-instance v0, Lmaps/ay/e;

    invoke-virtual {p0}, Lmaps/ay/m;->a()Lmaps/ay/m;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/ay/e;-><init>(Lmaps/ay/m;)V

    return-object v0
.end method

.method private static a(DDD)Z
    .locals 2

    sub-double v0, p0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    cmpg-double v0, v0, p4

    if-lez v0, :cond_0

    cmpl-double v0, p0, p2

    if-eqz v0, :cond_0

    cmpl-double v0, p0, p0

    if-eqz v0, :cond_1

    cmpl-double v0, p2, p2

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()Lmaps/ay/e;
    .locals 1

    sget-object v0, Lmaps/ay/e;->a:Lmaps/ay/e;

    return-object v0
.end method


# virtual methods
.method public a()D
    .locals 6

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    iget v3, v3, Lmaps/ay/m;->c:I

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    add-int/lit8 v4, v0, 0x1

    add-int/lit8 v5, v0, 0x2

    invoke-virtual {v3, v0, v4, v5}, Lmaps/ay/m;->b(III)D

    move-result-wide v3

    add-double/2addr v1, v3

    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method public a(I)D
    .locals 2

    if-ltz p1, :cond_0

    iget-object v0, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v0}, Lmaps/ay/m;->c()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v0, p1}, Lmaps/ay/m;->a(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public a(II)I
    .locals 2

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lmaps/ay/e;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-ltz p2, :cond_0

    const/4 v0, 0x3

    if-lt p2, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    mul-int/lit8 v1, p1, 0x3

    add-int/2addr v1, p2

    invoke-virtual {v0, v1}, Lmaps/ay/m;->i(I)I

    move-result v0

    return v0
.end method

.method public a(III)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v2, p1, p2, p3}, Lmaps/ay/m;->c(III)D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    const/4 v3, 0x3

    new-array v3, v3, [I

    aput p1, v3, v1

    aput p2, v3, v0

    const/4 v1, 0x2

    aput p3, v3, v1

    invoke-virtual {v2, v3}, Lmaps/ay/m;->a([I)V

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/ay/e;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ay/e;->c:I

    move v0, v1

    goto :goto_0
.end method

.method public a(Lmaps/ay/q;)Z
    .locals 6

    invoke-virtual {p1}, Lmaps/ay/q;->a()D

    move-result-wide v0

    invoke-virtual {p0}, Lmaps/ay/e;->a()D

    move-result-wide v2

    const-wide v4, 0x3f50624dd2f1a9fcL

    mul-double/2addr v4, v0

    invoke-static/range {v0 .. v5}, Lmaps/ay/e;->a(DDD)Z

    move-result v0

    return v0
.end method

.method public b(I)D
    .locals 2

    if-ltz p1, :cond_0

    iget-object v0, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v0}, Lmaps/ay/m;->c()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v0, p1}, Lmaps/ay/m;->b(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    iget v0, v0, Lmaps/ay/m;->c:I

    div-int/lit8 v0, v0, 0x3

    goto :goto_0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v0}, Lmaps/ay/m;->c()I

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    iget v0, p0, Lmaps/ay/e;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ay/e;->c:I

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lmaps/ay/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lmaps/ay/e;

    iget-object v0, p1, Lmaps/ay/e;->b:Lmaps/ay/m;

    iget-object v1, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v0, v1}, Lmaps/ay/m;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v0}, Lmaps/ay/m;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\nint[] triangles = {\n    "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    iget v3, v1, Lmaps/ay/m;->c:I

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v4, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v4, v1}, Lmaps/ay/m;->i(I)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v4, v5}, Lmaps/ay/m;->i(I)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    add-int/lit8 v5, v1, 0x2

    invoke-virtual {v4, v5}, Lmaps/ay/m;->i(I)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    iget v4, v4, Lmaps/ay/m;->c:I

    add-int/lit8 v4, v4, -0x3

    if-ne v1, v4, :cond_0

    const-string v4, "\n};\n\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v1, v1, 0x3

    goto :goto_0

    :cond_0
    const-string v4, ",\n    "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v1}, Lmaps/ay/m;->c()I

    move-result v1

    :goto_2
    if-ge v0, v1, :cond_2

    const-string v3, "cutVertices.add(new Vertex2d("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v3, v0}, Lmaps/ay/m;->a(I)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lmaps/ay/e;->b:Lmaps/ay/m;

    invoke-virtual {v3, v0}, Lmaps/ay/m;->b(I)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v3, "));\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
