.class public Lmaps/ag/j;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ag/s;


# static fields
.field private static final a:Lmaps/t/o;

.field private static final b:[B


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Lmaps/ag/e;

.field private final f:Ljava/util/HashMap;

.field private g:Lmaps/ag/l;

.field private final h:I

.field private final i:Lmaps/o/c;

.field private j:I

.field private k:Lmaps/ae/d;

.field private l:Lmaps/ag/t;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/t/ck;

    invoke-direct {v0}, Lmaps/t/ck;-><init>()V

    sput-object v0, Lmaps/ag/j;->a:Lmaps/t/o;

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lmaps/ag/j;->b:[B

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILmaps/ag/e;Lmaps/o/c;Lmaps/ag/g;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ag/j;->j:I

    new-instance v0, Lmaps/co/a;

    invoke-direct {v0}, Lmaps/co/a;-><init>()V

    iput-object v0, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    iput-object p1, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    iput p2, p0, Lmaps/ag/j;->d:I

    iput-object p3, p0, Lmaps/ag/j;->e:Lmaps/ag/e;

    invoke-static {}, Lmaps/ag/j;->b()I

    move-result v0

    iput v0, p0, Lmaps/ag/j;->h:I

    iget v0, p0, Lmaps/ag/j;->h:I

    invoke-static {v0}, Lmaps/f/cs;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    iput-object p4, p0, Lmaps/ag/j;->i:Lmaps/o/c;

    if-eqz p5, :cond_0

    new-instance v0, Lmaps/ag/t;

    iget-object v1, p0, Lmaps/ag/j;->i:Lmaps/o/c;

    invoke-direct {v0, v1, p5}, Lmaps/ag/t;-><init>(Lmaps/o/c;Lmaps/ag/g;)V

    iput-object v0, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    :cond_0
    return-void
.end method

.method private a(Lmaps/t/ah;Lmaps/t/o;[BI)V
    .locals 12

    const-wide/16 v4, 0x0

    const-wide/16 v8, -0x1

    iget-object v1, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Uninitialized"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    instance-of v1, p2, Lmaps/t/f;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Can\'t insert a MutableVectorTile into SD cache"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v1

    const/16 v2, 0x15

    if-le v1, v2, :cond_2

    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lmaps/ag/j;->i:Lmaps/o/c;

    invoke-static {v1, p1}, Lmaps/bt/a;->a(Lmaps/o/c;Lmaps/t/ah;)Landroid/util/Pair;

    move-result-object v10

    array-length v1, p3

    if-lez v1, :cond_9

    instance-of v1, p2, Lmaps/t/bv;

    if-eqz v1, :cond_8

    move-object v1, p2

    check-cast v1, Lmaps/t/bv;

    invoke-interface {v1}, Lmaps/t/bv;->f()J

    move-result-wide v1

    cmp-long v3, v1, v8

    if-eqz v3, :cond_7

    iget-object v3, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v3}, Lmaps/ae/d;->b()J

    move-result-wide v6

    sub-long/2addr v1, v6

    iget-object v3, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v3}, Lmaps/ae/d;->a()J

    move-result-wide v6

    add-long/2addr v1, v6

    cmp-long v3, v1, v4

    if-gez v3, :cond_7

    move-wide v2, v4

    :goto_1
    move-object v1, p2

    check-cast v1, Lmaps/t/bv;

    invoke-interface {v1}, Lmaps/t/bv;->r()J

    move-result-wide v6

    cmp-long v1, v6, v8

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v1}, Lmaps/ae/d;->b()J

    move-result-wide v8

    sub-long/2addr v6, v8

    iget-object v1, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v1}, Lmaps/ae/d;->a()J

    move-result-wide v8

    add-long/2addr v6, v8

    cmp-long v1, v6, v4

    if-gez v1, :cond_6

    :goto_2
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    array-length v1, p3

    add-int/lit8 v1, v1, 0x18

    invoke-direct {v7, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v8, Ljava/io/DataOutputStream;

    invoke-direct {v8, v7}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v1, 0x18

    :try_start_0
    invoke-virtual {v8, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v8, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-virtual {v8, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-virtual {v8, p3}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    :try_start_1
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_3
    iget-object v11, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    monitor-enter v11

    :try_start_2
    iget-object v1, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lmaps/ag/ae;

    move-object v9, v0

    if-eqz v9, :cond_5

    iget v1, v9, Lmaps/ag/ae;->d:I

    move/from16 v0, p4

    invoke-static {v1, v0}, Lmaps/an/w;->a(II)I

    move-result v5

    :goto_4
    new-instance v1, Lmaps/ag/ae;

    iget-object v2, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    if-eqz v9, :cond_4

    iget-object v8, v9, Lmaps/ag/ae;->h:Lmaps/bx/b;

    :goto_5
    move-object v7, p2

    invoke-direct/range {v1 .. v8}, Lmaps/ag/ae;-><init>(JLjava/lang/String;I[BLmaps/t/o;Lmaps/bx/b;)V

    iget v2, p0, Lmaps/ag/j;->j:I

    iget v3, p0, Lmaps/ag/j;->h:I

    if-ge v2, v3, :cond_3

    iget-object v2, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v9, :cond_3

    iget v1, p0, Lmaps/ag/j;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/ag/j;->j:I

    :cond_3
    monitor-exit v11

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :catch_0
    move-exception v1

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    :try_start_3
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_3

    :catchall_1
    move-exception v1

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    :try_start_4
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_6
    throw v1

    :cond_4
    const/4 v8, 0x0

    goto :goto_5

    :catch_2
    move-exception v2

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_3

    :cond_5
    move/from16 v5, p4

    goto :goto_4

    :cond_6
    move-wide v4, v6

    goto/16 :goto_2

    :cond_7
    move-wide v2, v1

    goto/16 :goto_1

    :cond_8
    move-wide v4, v8

    move-wide v2, v8

    goto/16 :goto_2

    :cond_9
    move-object v6, p3

    goto :goto_3
.end method

.method private a(ILjava/util/Locale;)Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v0, p1, p2}, Lmaps/ag/l;->a(ILjava/util/Locale;)V

    invoke-direct {p0}, Lmaps/ag/j;->j()V

    iget-object v0, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    invoke-virtual {v0}, Lmaps/ag/t;->a()V

    :cond_0
    monitor-exit v1

    const/4 v0, 0x1

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b()I
    .locals 2

    invoke-static {}, Lmaps/af/w;->f()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    const/16 v1, 0x100

    mul-int/lit8 v0, v0, 0x20

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private b(Ljava/io/File;)Z
    .locals 10

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-eqz v0, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    new-instance v4, Lmaps/bv/d;

    invoke-direct {v4, p1}, Lmaps/bv/d;-><init>(Ljava/io/File;)V

    iget-object v0, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v0}, Lmaps/ae/d;->c()J

    move-result-wide v8

    :try_start_0
    iget-object v0, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    iget-object v1, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    invoke-static {v0, v4, v1}, Lmaps/ag/l;->a(Ljava/lang/String;Lmaps/bv/c;Lmaps/ag/aj;)Lmaps/ag/l;

    move-result-object v0

    iput-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v7

    :goto_1
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lmaps/ag/j;->g()J

    move-result-wide v0

    iget-object v2, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v2}, Lmaps/ag/l;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    :cond_1
    move v0, v6

    :goto_2
    if-eqz v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    iget v1, p0, Lmaps/ag/j;->d:I

    const/4 v2, -0x1

    new-instance v3, Ljava/util/Locale;

    const-string v5, ""

    invoke-direct {v3, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    invoke-static/range {v0 .. v5}, Lmaps/ag/l;->a(Ljava/lang/String;IILjava/util/Locale;Lmaps/bv/c;Lmaps/ag/aj;)Lmaps/ag/l;

    move-result-object v0

    iput-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-direct {p0}, Lmaps/ag/j;->j()V

    iget-object v0, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    invoke-virtual {v0}, Lmaps/ag/t;->b()V

    iget-object v0, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    invoke-virtual {v0}, Lmaps/ag/t;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    iget-object v0, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v0}, Lmaps/ae/d;->c()J

    move-result-wide v0

    sub-long/2addr v0, v8

    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_3

    const-string v2, "SDCardTileCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Loaded cache: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v4}, Lmaps/ag/l;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " entries, data version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v4}, Lmaps/ag/l;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", locale: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v4}, Lmaps/ag/l;->c()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms, creationTime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v1}, Lmaps/ag/l;->b()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move v0, v6

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move v0, v6

    goto/16 :goto_1

    :cond_4
    move v0, v7

    goto/16 :goto_2

    :catch_1
    move-exception v0

    const-string v1, "SDCardTileCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Creating cache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    goto/16 :goto_0
.end method

.method private j()V
    .locals 4

    new-instance v0, Lmaps/cs/b;

    invoke-direct {v0}, Lmaps/cs/b;-><init>()V

    iget-object v1, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v1}, Lmaps/ag/l;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmaps/cs/b;->writeLong(J)V

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/cs/b;->a()[B

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disk_creation_time_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lmaps/bj/b;->a([BLjava/lang/String;)I

    return-void
.end method


# virtual methods
.method public a(Lmaps/t/ah;Lmaps/bx/b;I)V
    .locals 10

    iget-object v1, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Uninitialized"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v9, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    monitor-enter v9

    :try_start_0
    iget-object v1, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lmaps/ag/ae;

    move-object v7, v0

    if-eqz v7, :cond_3

    iget-object v1, v7, Lmaps/ag/ae;->h:Lmaps/bx/b;

    if-eqz v1, :cond_4

    if-eqz p2, :cond_1

    new-instance v8, Lmaps/bx/a;

    iget-object v1, v7, Lmaps/ag/ae;->h:Lmaps/bx/b;

    invoke-direct {v8, v1, p2}, Lmaps/bx/a;-><init>(Lmaps/bx/b;Lmaps/bx/b;)V

    :goto_0
    iget-object v1, v7, Lmaps/ag/ae;->f:Lmaps/t/o;

    if-eqz v1, :cond_2

    new-instance v1, Lmaps/ag/ae;

    iget-wide v2, v7, Lmaps/ag/ae;->b:J

    iget-object v4, v7, Lmaps/ag/ae;->c:Ljava/lang/String;

    iget v5, v7, Lmaps/ag/ae;->d:I

    invoke-static {v5, p3}, Lmaps/an/w;->a(II)I

    move-result v5

    iget-object v6, v7, Lmaps/ag/ae;->e:[B

    iget-object v7, v7, Lmaps/ag/ae;->f:Lmaps/t/o;

    invoke-direct/range {v1 .. v8}, Lmaps/ag/ae;-><init>(JLjava/lang/String;I[BLmaps/t/o;Lmaps/bx/b;)V

    :goto_1
    iget-object v2, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    monitor-exit v9

    return-void

    :cond_1
    iget-object v8, v7, Lmaps/ag/ae;->h:Lmaps/bx/b;

    goto :goto_0

    :cond_2
    new-instance v2, Lmaps/ag/ae;

    iget-wide v3, v7, Lmaps/ag/ae;->b:J

    iget-object v5, v7, Lmaps/ag/ae;->c:Ljava/lang/String;

    iget v1, v7, Lmaps/ag/ae;->d:I

    invoke-static {v1, p3}, Lmaps/an/w;->a(II)I

    move-result v6

    move-object v7, p1

    invoke-direct/range {v2 .. v8}, Lmaps/ag/ae;-><init>(JLjava/lang/String;ILmaps/t/ah;Lmaps/bx/b;)V

    move-object v1, v2

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lmaps/ag/j;->i:Lmaps/o/c;

    invoke-static {v1, p1}, Lmaps/bt/a;->a(Lmaps/o/c;Lmaps/t/ah;)Landroid/util/Pair;

    move-result-object v4

    new-instance v1, Lmaps/ag/ae;

    iget-object v2, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    move v5, p3

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lmaps/ag/ae;-><init>(JLjava/lang/String;ILmaps/t/ah;Lmaps/bx/b;)V

    iget-object v2, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_4
    move-object v8, p2

    goto :goto_0
.end method

.method public a(Lmaps/t/ah;Lmaps/t/o;)V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Don\'t store unencrypted tiles into SD cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lmaps/t/ah;Lmaps/t/o;[B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lmaps/ag/j;->a(Lmaps/t/ah;Lmaps/t/o;[BI)V

    return-void
.end method

.method public a()Z
    .locals 2

    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v0}, Lmaps/ag/l;->a()I

    move-result v0

    iget-object v1, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v1}, Lmaps/ag/l;->c()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lmaps/ag/j;->a(ILjava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public a(I)Z
    .locals 2

    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v0, p1}, Lmaps/ag/l;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/io/File;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "SDCardTileCache.initialize"

    invoke-static {v0}, Lmaps/ah/a;->a(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lmaps/ag/j;->b(Ljava/io/File;)Z

    move-result v0

    const-string v1, "SDCardTileCache.initialize"

    invoke-static {v1}, Lmaps/ah/a;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/Locale;)Z
    .locals 2

    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v0}, Lmaps/ag/l;->a()I

    move-result v0

    invoke-direct {p0, v0, p1}, Lmaps/ag/j;->a(ILjava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public a(Lmaps/t/ah;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v0

    const/16 v3, 0x15

    if-le v0, v3, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/ag/j;->i:Lmaps/o/c;

    invoke-static {v0, p1}, Lmaps/bt/a;->a(Lmaps/o/c;Lmaps/t/ah;)Landroid/util/Pair;

    move-result-object v3

    iget-object v4, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v0}, Lmaps/ag/l;->b(JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/ae;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lmaps/ag/ae;->e:[B

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a(Lmaps/t/o;)Z
    .locals 1

    sget-object v0, Lmaps/ag/j;->a:Lmaps/t/o;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lmaps/t/ah;)Lmaps/t/o;
    .locals 13

    const-wide/16 v9, -0x1

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v0

    const/16 v1, 0x15

    if-le v0, v1, :cond_1

    move-object v0, v8

    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/ae;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lmaps/ag/ae;->f:Lmaps/t/o;

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/ag/j;->i:Lmaps/o/c;

    invoke-static {v0, p1}, Lmaps/bt/a;->a(Lmaps/o/c;Lmaps/t/ah;)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, Lmaps/ag/l;->a(JLjava/lang/String;)[B

    move-result-object v2

    if-nez v2, :cond_3

    move-object v0, v8

    goto :goto_0

    :cond_3
    :try_start_2
    array-length v0, v2

    if-nez v0, :cond_4

    sget-object v0, Lmaps/ag/j;->a:Lmaps/t/o;

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    cmp-long v0, v4, v9

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v0}, Lmaps/ae/d;->a()J

    move-result-wide v11

    sub-long/2addr v4, v11

    iget-object v0, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v11

    add-long/2addr v4, v11

    cmp-long v0, v4, v6

    if-gez v0, :cond_5

    move-wide v4, v6

    :cond_5
    const/16 v0, 0x10

    if-le v3, v0, :cond_7

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    cmp-long v9, v0, v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v9}, Lmaps/ae/d;->a()J

    move-result-wide v9

    sub-long/2addr v0, v9

    iget-object v9, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v9}, Lmaps/ae/d;->b()J

    move-result-wide v9

    add-long/2addr v0, v9

    cmp-long v9, v0, v6

    if-gez v9, :cond_6

    :goto_1
    iget-object v0, p0, Lmaps/ag/j;->e:Lmaps/ag/e;

    move-object v1, p1

    invoke-interface/range {v0 .. v7}, Lmaps/ag/e;->a(Lmaps/t/ah;[BIJJ)Lmaps/t/o;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SDCardTileCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not unpack tile in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v8

    goto/16 :goto_0

    :cond_6
    move-wide v6, v0

    goto :goto_1

    :cond_7
    move-wide v6, v9

    goto :goto_1
.end method

.method public declared-synchronized c()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v0}, Lmaps/ag/l;->f()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "SDCardTileCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "shutDown(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public c(Lmaps/t/ah;)V
    .locals 2

    sget-object v0, Lmaps/ag/j;->a:Lmaps/t/o;

    sget-object v1, Lmaps/ag/j;->b:[B

    invoke-virtual {p0, p1, v0, v1}, Lmaps/ag/j;->a(Lmaps/t/ah;Lmaps/t/o;[B)V

    return-void
.end method

.method public d()I
    .locals 2

    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v0}, Lmaps/ag/l;->a()I

    move-result v0

    return v0
.end method

.method public d(Lmaps/t/ah;)[B
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lmaps/t/ah;->c()I

    move-result v0

    const/16 v1, 0x15

    if-le v0, v1, :cond_1

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_1
    iget-object v3, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/ae;

    if-eqz v0, :cond_7

    iget-object v1, v0, Lmaps/ag/ae;->e:[B

    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_2

    iget-object v0, p0, Lmaps/ag/j;->i:Lmaps/o/c;

    invoke-static {v0, p1}, Lmaps/bt/a;->a(Lmaps/o/c;Lmaps/t/ah;)Landroid/util/Pair;

    move-result-object v1

    iget-object v3, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v0}, Lmaps/ag/l;->a(JLjava/lang/String;)[B

    move-result-object v1

    :cond_2
    if-eqz v1, :cond_3

    array-length v0, v1

    if-nez v0, :cond_4

    :cond_3
    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    :try_start_2
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    array-length v0, v1

    sub-int v4, v0, v3

    if-ltz v4, :cond_5

    const/16 v0, 0x18

    if-le v3, v0, :cond_6

    :cond_5
    const-string v0, "SDCardTileCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid tile data length["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] in "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_0

    :cond_6
    new-array v0, v4, [B
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    const/4 v2, 0x0

    :try_start_3
    invoke-static {v1, v3, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    :goto_2
    const-string v2, "SDCardTileCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid tile data in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    goto :goto_2

    :cond_7
    move-object v1, v2

    goto/16 :goto_1
.end method

.method public e()Ljava/util/Locale;
    .locals 2

    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v0}, Lmaps/ag/l;->c()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method g()J
    .locals 5

    const-wide/16 v0, 0x0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disk_creation_time_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lmaps/bj/b;->b(Ljava/lang/String;)[B

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    new-instance v3, Lmaps/cs/a;

    invoke-direct {v3, v2}, Lmaps/cs/a;-><init>([B)V

    :try_start_0
    invoke-virtual {v3}, Lmaps/cs/a;->readLong()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disk_creation_time_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lmaps/bj/b;->a(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public h()V
    .locals 15

    iget-object v0, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v0}, Lmaps/ae/d;->c()J

    move-result-wide v6

    const-wide/16 v0, 0x0

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v8

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v9

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v10

    iget-object v3, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move-wide v4, v0

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/ae;

    iget-object v1, v0, Lmaps/ag/ae;->e:[B

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    int-to-long v1, v1

    add-long/2addr v1, v4

    iget v4, v0, Lmaps/ag/ae;->a:I

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_2
    move-wide v4, v1

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lmaps/ag/ae;->e:[B

    array-length v1, v1

    goto :goto_1

    :pswitch_0
    iget-wide v4, v0, Lmaps/ag/ae;->b:J

    iget-object v12, v0, Lmaps/ag/ae;->c:Ljava/lang/String;

    iget v13, v0, Lmaps/ag/ae;->d:I

    iget-object v14, v0, Lmaps/ag/ae;->e:[B

    invoke-static {v4, v5, v12, v13, v14}, Lmaps/ag/l;->a(JLjava/lang/String;I[B)Lmaps/ag/m;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    iget-object v5, v0, Lmaps/ag/ae;->f:Lmaps/t/o;

    invoke-virtual {v4, v5}, Lmaps/ag/t;->a(Lmaps/t/o;)V

    :cond_2
    iget-object v4, v0, Lmaps/ag/ae;->h:Lmaps/bx/b;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lmaps/ag/ae;->h:Lmaps/bx/b;

    iget-object v0, v0, Lmaps/ag/ae;->f:Lmaps/t/o;

    invoke-static {v4, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_1
    :try_start_1
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmaps/ag/j;->j:I

    iget-object v0, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    const/4 v0, 0x0

    :try_start_2
    iget-object v1, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v1, v8}, Lmaps/ag/l;->a(Ljava/util/Collection;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    const/4 v0, 0x1

    :cond_4
    move v3, v0

    :goto_3
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmaps/bx/b;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lmaps/t/o;

    invoke-interface {v2}, Lmaps/t/o;->g()Lmaps/t/ah;

    move-result-object v2

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lmaps/t/o;

    invoke-interface {v1, v2, v3, v0}, Lmaps/bx/b;->a(Lmaps/t/ah;ILmaps/t/o;)V

    goto :goto_4

    :catch_0
    move-exception v0

    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x1

    move v3, v0

    goto :goto_3

    :cond_5
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/ae;

    const/4 v1, 0x0

    iget v3, v0, Lmaps/ag/ae;->d:I

    if-lez v3, :cond_7

    :try_start_3
    iget-object v3, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    iget-wide v9, v0, Lmaps/ag/ae;->b:J

    iget-object v11, v0, Lmaps/ag/ae;->c:Ljava/lang/String;

    iget v12, v0, Lmaps/ag/ae;->d:I

    invoke-virtual {v3, v9, v10, v11, v12}, Lmaps/ag/l;->a(JLjava/lang/String;I)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-result v3

    const/4 v9, -0x1

    if-ne v3, v9, :cond_7

    const/4 v1, 0x2

    :cond_7
    :goto_6
    iget-object v3, v0, Lmaps/ag/ae;->h:Lmaps/bx/b;

    if-eqz v3, :cond_6

    iget-object v3, v0, Lmaps/ag/ae;->h:Lmaps/bx/b;

    iget-object v0, v0, Lmaps/ag/ae;->g:Lmaps/t/ah;

    const/4 v9, 0x0

    invoke-interface {v3, v0, v1, v9}, Lmaps/bx/b;->a(Lmaps/t/ah;ILmaps/t/o;)V

    goto :goto_5

    :catch_1
    move-exception v1

    const/4 v1, 0x1

    goto :goto_6

    :cond_8
    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/ag/j;->k:Lmaps/ae/d;

    invoke-interface {v0}, Lmaps/ae/d;->c()J

    move-result-wide v0

    sub-long/2addr v0, v6

    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_9

    const-string v2, "SDCardTileCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lmaps/ag/j;->c:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ": Commit inserted "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " tiles totaling "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms. Number of tiles in DiskCache is now "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ag/j;->g:Lmaps/ag/l;

    invoke-virtual {v1}, Lmaps/ag/l;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    invoke-virtual {v0}, Lmaps/ag/t;->c()V

    iget-object v0, p0, Lmaps/ag/j;->l:Lmaps/ag/t;

    invoke-virtual {v0}, Lmaps/ag/t;->a()V

    :cond_a
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, Lmaps/ag/j;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
