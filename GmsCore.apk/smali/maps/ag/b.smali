.class public Lmaps/ag/b;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ag/a;


# static fields
.field protected static final b:Lmaps/t/o;


# instance fields
.field protected final a:Lmaps/be/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/t/ck;

    invoke-direct {v0}, Lmaps/t/ck;-><init>()V

    sput-object v0, Lmaps/ag/b;->b:Lmaps/t/o;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/be/i;

    invoke-direct {v0, p1}, Lmaps/be/i;-><init>(I)V

    iput-object v0, p0, Lmaps/ag/b;->a:Lmaps/be/i;

    return-void
.end method


# virtual methods
.method public a(Lmaps/t/ah;Lmaps/t/o;)V
    .locals 2

    iget-object v1, p0, Lmaps/ag/b;->a:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/b;->a:Lmaps/be/i;

    invoke-virtual {v0, p1, p2}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()Z
    .locals 2

    iget-object v1, p0, Lmaps/ag/b;->a:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/b;->a:Lmaps/be/i;

    invoke-virtual {v0}, Lmaps/be/i;->a()V

    monitor-exit v1

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lmaps/t/ah;)Z
    .locals 2

    iget-object v1, p0, Lmaps/ag/b;->a:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/b;->a:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lmaps/t/o;)Z
    .locals 1

    sget-object v0, Lmaps/ag/b;->b:Lmaps/t/o;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lmaps/t/ah;)Lmaps/t/o;
    .locals 2

    iget-object v1, p0, Lmaps/ag/b;->a:Lmaps/be/i;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ag/b;->a:Lmaps/be/i;

    invoke-virtual {v0, p1}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/o;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(Lmaps/t/ah;)V
    .locals 1

    sget-object v0, Lmaps/ag/b;->b:Lmaps/t/o;

    invoke-virtual {p0, p1, v0}, Lmaps/ag/b;->a(Lmaps/t/ah;Lmaps/t/o;)V

    return-void
.end method
