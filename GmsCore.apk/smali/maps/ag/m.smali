.class public Lmaps/ag/m;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lmaps/ag/r;

.field public final b:[B

.field public final c:I

.field public final d:[B


# direct methods
.method private constructor <init>(Lmaps/ag/r;I[B)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    array-length v0, p3

    const v1, 0xffffff

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "data too large"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/ag/m;->a:Lmaps/ag/r;

    invoke-virtual {p1}, Lmaps/ag/r;->b()[B

    move-result-object v0

    iput-object v0, p0, Lmaps/ag/m;->b:[B

    iput p2, p0, Lmaps/ag/m;->c:I

    iput-object p3, p0, Lmaps/ag/m;->d:[B

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ag/r;I[BLmaps/ag/q;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmaps/ag/m;-><init>(Lmaps/ag/r;I[B)V

    return-void
.end method

.method private constructor <init>(Lmaps/ag/r;[B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lmaps/ag/m;-><init>(Lmaps/ag/r;I[B)V

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ag/r;[BLmaps/ag/q;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/ag/m;-><init>(Lmaps/ag/r;[B)V

    return-void
.end method
