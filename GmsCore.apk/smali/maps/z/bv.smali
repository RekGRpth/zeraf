.class public final Lmaps/z/bv;
.super Lcom/google/android/gms/maps/model/internal/IMarkerDelegate$Stub;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lmaps/z/br;

.field private final c:Lmaps/l/ay;

.field private final d:Lmaps/z/by;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private final g:Lmaps/bf/b;


# direct methods
.method constructor <init>(Ljava/lang/String;Lmaps/z/br;Lmaps/l/ay;Lmaps/z/by;Lmaps/bf/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/IMarkerDelegate$Stub;-><init>()V

    iput-object p1, p0, Lmaps/z/bv;->a:Ljava/lang/String;

    iput-object p2, p0, Lmaps/z/bv;->b:Lmaps/z/br;

    iput-object p3, p0, Lmaps/z/bv;->c:Lmaps/l/ay;

    iput-object p4, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    iput-object p5, p0, Lmaps/z/bv;->g:Lmaps/bf/b;

    return-void
.end method

.method static synthetic a(Lmaps/z/bv;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lmaps/z/bv;->f:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lmaps/z/bv;)Lmaps/l/ay;
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->c:Lmaps/l/ay;

    return-object v0
.end method

.method static synthetic b(Lmaps/z/bv;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lmaps/z/bv;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lmaps/z/bv;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lmaps/z/bv;)Lmaps/z/br;
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->b:Lmaps/z/br;

    return-object v0
.end method

.method static synthetic d(Lmaps/z/bv;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public equalsRemote(Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->c:Lmaps/l/ay;

    invoke-virtual {v0}, Lmaps/l/ay;->c_()Lmaps/t/bx;

    move-result-object v0

    invoke-static {v0}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public getSnippet()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->e:Ljava/lang/String;

    return-object v0
.end method

.method public hashCodeRemote()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public hideInfoWindow()V
    .locals 2

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->g:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->j:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0, p0}, Lmaps/z/by;->c(Lmaps/z/by;Lmaps/z/bv;)V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->b(Lmaps/z/by;)V

    return-void
.end method

.method public isDraggable()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->c:Lmaps/l/ay;

    invoke-virtual {v0}, Lmaps/l/ay;->a()Z

    move-result v0

    return v0
.end method

.method public isInfoWindowShown()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0, p0}, Lmaps/z/by;->d(Lmaps/z/by;Lmaps/z/bv;)Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->c:Lmaps/l/ay;

    invoke-virtual {v0}, Lmaps/l/ay;->b()Z

    move-result v0

    return v0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->g:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->c:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0, p0}, Lmaps/z/by;->a(Lmaps/z/by;Lmaps/z/bv;)V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->b(Lmaps/z/by;)V

    return-void
.end method

.method public setDraggable(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->g:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->g:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/bv;->c:Lmaps/l/ay;

    invoke-virtual {v0, p1}, Lmaps/l/ay;->a(Z)V

    return-void
.end method

.method public setPosition(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->g:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->d:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/bv;->c:Lmaps/l/ay;

    invoke-static {p1}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/l/ay;->a(Lmaps/t/bx;)V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->c(Lmaps/z/by;)V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->b(Lmaps/z/by;)V

    return-void
.end method

.method public setSnippet(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->g:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->f:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iput-object p1, p0, Lmaps/z/bv;->f:Ljava/lang/String;

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->b(Lmaps/z/by;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->g:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->e:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iput-object p1, p0, Lmaps/z/bv;->e:Ljava/lang/String;

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->b(Lmaps/z/by;)V

    return-void
.end method

.method public setVisible(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->g:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->h:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0, p0}, Lmaps/z/by;->c(Lmaps/z/by;Lmaps/z/bv;)V

    :cond_0
    iget-object v0, p0, Lmaps/z/bv;->c:Lmaps/l/ay;

    invoke-virtual {v0, p1}, Lmaps/l/ay;->b(Z)V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->b(Lmaps/z/by;)V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->c(Lmaps/z/by;)V

    return-void
.end method

.method public showInfoWindow()V
    .locals 2

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->a(Lmaps/z/by;)Lmaps/au/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/bv;->g:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->i:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0, p0}, Lmaps/z/by;->b(Lmaps/z/by;Lmaps/z/bv;)V

    iget-object v0, p0, Lmaps/z/bv;->d:Lmaps/z/by;

    invoke-static {v0}, Lmaps/z/by;->b(Lmaps/z/by;)V

    return-void
.end method
