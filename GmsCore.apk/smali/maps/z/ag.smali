.class public final Lmaps/z/ag;
.super Lcom/google/android/gms/maps/internal/IGoogleMapDelegate$Stub;

# interfaces
.implements Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;


# static fields
.field static final a:Lcom/google/android/gms/maps/model/CameraPosition;

.field static final synthetic b:Z


# instance fields
.field private A:Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;

.field private final B:Lmaps/aa/b;

.field private C:Lmaps/z/ae;

.field private D:Z

.field private E:Z

.field private final c:Lmaps/z/x;

.field private final d:Lmaps/i/c;

.field private final e:Lmaps/i/h;

.field private final f:Lmaps/z/bu;

.field private final g:Lmaps/z/bn;

.field private final h:Lmaps/ch/b;

.field private final i:Lmaps/z/z;

.field private final j:Lmaps/z/as;

.field private final k:Lmaps/aa/g;

.field private final l:Lmaps/z/bg;

.field private final m:Lmaps/z/w;

.field private final n:Lmaps/au/e;

.field private final o:Landroid/view/View;

.field private final p:Lmaps/bf/b;

.field private final q:Lcom/google/android/gms/maps/GoogleMapOptions;

.field private final r:Lmaps/z/bc;

.field private final s:Lmaps/af/h;

.field private final t:Landroid/content/res/Resources;

.field private final u:Lmaps/i/m;

.field private v:Lmaps/y/bc;

.field private w:Lmaps/y/bc;

.field private x:I

.field private y:Lmaps/i/n;

.field private z:Lmaps/y/u;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-wide/16 v1, 0x0

    const-class v0, Lmaps/z/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmaps/z/ag;->b:Z

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, v1, v2, v1, v2}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition;->fromLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    sput-object v0, Lmaps/z/ag;->a:Lcom/google/android/gms/maps/model/CameraPosition;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/view/View;Lmaps/i/h;Lmaps/i/c;Lmaps/z/bu;Lmaps/ch/b;Lmaps/z/z;Lmaps/z/bn;Lmaps/z/as;Lmaps/z/x;Lmaps/aa/g;Lmaps/z/bg;Lmaps/z/w;Lmaps/au/e;Lmaps/bf/b;Lcom/google/android/gms/maps/GoogleMapOptions;Lmaps/z/bc;Landroid/content/res/Resources;Lmaps/af/h;Lmaps/i/m;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate$Stub;-><init>()V

    const/4 v1, 0x1

    iput v1, p0, Lmaps/z/ag;->x:I

    new-instance v1, Lmaps/z/ai;

    invoke-direct {v1, p0}, Lmaps/z/ai;-><init>(Lmaps/z/ag;)V

    iput-object v1, p0, Lmaps/z/ag;->B:Lmaps/aa/b;

    iput-object p1, p0, Lmaps/z/ag;->o:Landroid/view/View;

    iput-object p2, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iput-object p3, p0, Lmaps/z/ag;->d:Lmaps/i/c;

    iput-object p4, p0, Lmaps/z/ag;->f:Lmaps/z/bu;

    iput-object p5, p0, Lmaps/z/ag;->h:Lmaps/ch/b;

    iput-object p6, p0, Lmaps/z/ag;->i:Lmaps/z/z;

    iput-object p7, p0, Lmaps/z/ag;->g:Lmaps/z/bn;

    iput-object p8, p0, Lmaps/z/ag;->j:Lmaps/z/as;

    iput-object p9, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    iput-object p10, p0, Lmaps/z/ag;->k:Lmaps/aa/g;

    iput-object p11, p0, Lmaps/z/ag;->l:Lmaps/z/bg;

    iput-object p12, p0, Lmaps/z/ag;->m:Lmaps/z/w;

    iput-object p13, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    move-object/from16 v0, p14

    iput-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    move-object/from16 v0, p15

    iput-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    move-object/from16 v0, p16

    iput-object v0, p0, Lmaps/z/ag;->r:Lmaps/z/bc;

    move-object/from16 v0, p18

    iput-object v0, p0, Lmaps/z/ag;->s:Lmaps/af/h;

    move-object/from16 v0, p17

    iput-object v0, p0, Lmaps/z/ag;->t:Landroid/content/res/Resources;

    move-object/from16 v0, p19

    iput-object v0, p0, Lmaps/z/ag;->u:Lmaps/i/m;

    return-void
.end method

.method static synthetic a(Lmaps/z/ag;)Lmaps/bf/b;
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    return-object v0
.end method

.method public static a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/GoogleMapOptions;Z)Lmaps/z/ag;
    .locals 1

    const-string v0, ""

    invoke-static {p0, p1, p2, v0}, Lmaps/z/ag;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/GoogleMapOptions;ZLjava/lang/String;)Lmaps/z/ag;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/GoogleMapOptions;ZLjava/lang/String;)Lmaps/z/ag;
    .locals 29

    invoke-static/range {p1 .. p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lmaps/az/c;->d()Lmaps/az/g;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lmaps/az/g;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    new-instance v24, Lmaps/z/c;

    new-instance v3, Lmaps/co/a;

    invoke-direct {v3}, Lmaps/co/a;-><init>()V

    const-string v4, "map_start_up"

    move-object/from16 v0, v24

    invoke-direct {v0, v3, v4, v2}, Lmaps/z/c;-><init>(Lmaps/ae/d;Ljava/lang/String;Z)V

    invoke-interface/range {v24 .. v24}, Lmaps/z/bc;->a()V

    const-string v2, "init"

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Lmaps/z/bc;->a(Ljava/lang/String;)Lmaps/z/bp;

    move-result-object v28

    const-string v2, "map_load"

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Lmaps/z/bc;->a(Ljava/lang/String;)Lmaps/z/bp;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {}, Lmaps/z/y;->a()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v10}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lmaps/z/ab;->a(Landroid/content/Context;Landroid/content/res/Resources;)V

    new-instance v9, Landroid/widget/FrameLayout;

    invoke-direct {v9, v10}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lmaps/bf/d;->b()Lmaps/bf/b;

    move-result-object v7

    new-instance v11, Lmaps/y/q;

    invoke-direct {v11, v2}, Lmaps/y/q;-><init>(Landroid/content/res/Resources;)V

    new-instance v3, Lmaps/i/d;

    invoke-direct {v3, v11}, Lmaps/i/d;-><init>(Lmaps/y/q;)V

    const/high16 v4, 0x42870000

    invoke-interface {v3, v4}, Lmaps/i/c;->a(F)V

    invoke-static/range {p1 .. p1}, Lmaps/z/ag;->a(Lcom/google/android/gms/maps/GoogleMapOptions;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    invoke-static {}, Lmaps/z/ag;->h()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {}, Lmaps/bm/f;->c()Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz p2, :cond_3

    new-instance v5, Lmaps/i/k;

    invoke-direct {v5, v10, v2}, Lmaps/i/k;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lmaps/i/k;->b(Z)V

    invoke-virtual {v5, v11}, Lmaps/i/k;->a(Lmaps/y/q;)V

    invoke-virtual {v5, v4}, Lmaps/i/k;->h(Z)V

    invoke-virtual {v9, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    move-object v4, v5

    :goto_2
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v13, Lmaps/z/al;

    invoke-direct {v13, v5}, Lmaps/z/al;-><init>(Landroid/os/Handler;)V

    invoke-static {v5}, Lmaps/i/o;->a(Landroid/os/Handler;)Lmaps/i/o;

    move-result-object v20

    invoke-static {v4, v3, v13}, Lmaps/z/aw;->a(Lmaps/i/h;Lmaps/i/c;Ljava/util/concurrent/Executor;)Lmaps/z/aw;

    move-result-object v17

    new-instance v18, Lmaps/aa/f;

    move-object/from16 v0, v18

    invoke-direct {v0, v10, v2}, Lmaps/aa/f;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    invoke-interface/range {v18 .. v18}, Lmaps/aa/g;->a()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v9, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    invoke-static {v4, v3, v10, v2, v7}, Lmaps/z/by;->a(Lmaps/i/h;Lmaps/i/c;Landroid/content/Context;Landroid/content/res/Resources;Lmaps/bf/b;)Lmaps/z/by;

    move-result-object v12

    invoke-static {v4, v7}, Lmaps/z/bi;->a(Lmaps/i/h;Lmaps/bf/b;)Lmaps/z/bn;

    move-result-object v15

    invoke-static {v10}, Lmaps/ch/a;->a(Landroid/content/Context;)Lmaps/ch/a;

    move-result-object v6

    invoke-interface/range {v18 .. v18}, Lmaps/aa/g;->c()Lmaps/aa/c;

    move-result-object v5

    invoke-static/range {v2 .. v7}, Lmaps/z/bh;->a(Landroid/content/res/Resources;Lmaps/i/c;Lmaps/i/h;Lmaps/aa/c;Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;Lmaps/bf/b;)Lmaps/z/bh;

    move-result-object v14

    invoke-static {v3, v7}, Lmaps/z/bm;->a(Lmaps/i/c;Lmaps/bf/b;)Lmaps/z/bm;

    move-result-object v5

    invoke-interface {v4, v5}, Lmaps/i/h;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-interface {v4}, Lmaps/i/h;->p()Lmaps/af/v;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/af/v;->j()Lmaps/u/a;

    move-result-object v5

    new-instance v16, Lmaps/u/d;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lmaps/u/d;-><init>(Landroid/content/res/Resources;)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lmaps/u/a;->a(Lmaps/u/c;)V

    invoke-static {}, Lmaps/b/r;->a()Lmaps/b/r;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-static {v5, v13, v7}, Lmaps/i/t;->a(Lmaps/b/r;Ljava/util/concurrent/Executor;Lmaps/bf/b;)Lmaps/i/m;

    move-result-object v27

    invoke-interface/range {v18 .. v18}, Lmaps/aa/g;->d()Lmaps/i/u;

    move-result-object v13

    invoke-interface {v13}, Lmaps/i/u;->a()Lmaps/n/b;

    move-result-object v13

    invoke-static {v13}, Lmaps/bp/b;->a(Lmaps/n/b;)Lmaps/bp/b;

    :goto_3
    new-instance v26, Lmaps/af/h;

    invoke-static {}, Lmaps/ak/n;->e()Lmaps/ak/n;

    move-result-object v13

    move-object/from16 v0, v26

    invoke-direct {v0, v13}, Lmaps/af/h;-><init>(Lmaps/ak/a;)V

    new-instance v13, Lmaps/z/am;

    invoke-direct {v13, v6}, Lmaps/z/am;-><init>(Lmaps/ch/a;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lmaps/af/h;->a(Lmaps/af/i;)V

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Lmaps/y/q;->a(Lmaps/af/h;)V

    new-instance v16, Lmaps/z/an;

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v5}, Lmaps/z/an;-><init>(Ljava/lang/String;Lmaps/b/r;)V

    invoke-static/range {p3 .. p3}, Lmaps/ap/p;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lmaps/o/c;->a:Lmaps/o/c;

    invoke-interface {v4}, Lmaps/i/h;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    move-object/from16 v0, v16

    invoke-interface {v0, v5, v11}, Lmaps/z/as;->c(Lmaps/o/c;Landroid/content/res/Resources;)Lmaps/y/bc;

    move-result-object v5

    check-cast v5, Lmaps/y/ab;

    invoke-interface {v4, v5}, Lmaps/i/h;->a(Lmaps/y/ab;)V

    sget-object v5, Lmaps/o/c;->j:Lmaps/o/c;

    invoke-static {v5, v10, v2}, Lmaps/af/w;->a(Lmaps/o/c;Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/an/y;

    sget-object v5, Lmaps/o/c;->m:Lmaps/o/c;

    invoke-static {v5, v10, v2}, Lmaps/af/w;->a(Lmaps/o/c;Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/an/y;

    :cond_0
    new-instance v19, Lmaps/z/ao;

    invoke-direct/range {v19 .. v19}, Lmaps/z/ao;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-static {v4, v0, v1, v8}, Lmaps/z/w;->a(Lmaps/i/h;Lmaps/i/v;Lmaps/z/bc;Lmaps/z/bp;)Lmaps/z/w;

    move-result-object v20

    invoke-static {}, Lmaps/au/e;->a()Lmaps/au/e;

    move-result-object v21

    new-instance v8, Lmaps/z/ag;

    move-object v10, v4

    move-object v11, v3

    move-object v13, v6

    move-object/from16 v22, v7

    move-object/from16 v23, p1

    move-object/from16 v25, v2

    invoke-direct/range {v8 .. v27}, Lmaps/z/ag;-><init>(Landroid/view/View;Lmaps/i/h;Lmaps/i/c;Lmaps/z/bu;Lmaps/ch/b;Lmaps/z/z;Lmaps/z/bn;Lmaps/z/as;Lmaps/z/x;Lmaps/aa/g;Lmaps/z/bg;Lmaps/z/w;Lmaps/au/e;Lmaps/bf/b;Lcom/google/android/gms/maps/GoogleMapOptions;Lmaps/z/bc;Landroid/content/res/Resources;Lmaps/af/h;Lmaps/i/m;)V

    invoke-virtual {v8}, Lmaps/z/ag;->a()V

    sget-object v2, Lmaps/bf/c;->a:Lmaps/bf/c;

    invoke-interface {v7, v2}, Lmaps/bf/b;->a(Lmaps/bf/c;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lmaps/z/bc;->a(Lmaps/z/bp;)V

    return-object v8

    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_3
    new-instance v5, Lmaps/i/b;

    invoke-direct {v5, v10, v2}, Lmaps/i/b;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lmaps/i/b;->i(Z)V

    invoke-virtual {v5, v11}, Lmaps/i/b;->a(Lmaps/y/q;)V

    invoke-virtual {v5, v4}, Lmaps/i/b;->h(Z)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/maps/GoogleMapOptions;->getZOrderOnTop()Ljava/lang/Boolean;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/maps/GoogleMapOptions;->getZOrderOnTop()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v5, v4}, Lmaps/i/b;->setZOrderOnTop(Z)V

    :cond_4
    invoke-virtual {v9, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    move-object v4, v5

    goto/16 :goto_2

    :cond_5
    const/16 v27, 0x0

    goto/16 :goto_3
.end method

.method static synthetic a(Lmaps/y/ab;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lmaps/z/ag;->b(Lmaps/y/ab;Ljava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/z/ag;->u:Lmaps/i/m;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/z/ag;->k()Z

    move-result v0

    if-eq v0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/z/ag;->u:Lmaps/i/m;

    invoke-interface {v0}, Lmaps/i/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/ag;->k:Lmaps/aa/g;

    invoke-interface {v0}, Lmaps/aa/g;->d()Lmaps/i/u;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lmaps/i/u;->a(I)V

    iget-object v0, p0, Lmaps/z/ag;->k:Lmaps/aa/g;

    invoke-interface {v0}, Lmaps/aa/g;->d()Lmaps/i/u;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/ag;->u:Lmaps/i/m;

    invoke-interface {v0, v1}, Lmaps/i/u;->a(Lmaps/i/m;)V

    iget-object v0, p0, Lmaps/z/ag;->j:Lmaps/z/as;

    iget-object v1, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v1}, Lmaps/i/h;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lmaps/z/ag;->t:Landroid/content/res/Resources;

    invoke-interface {v0, v1, v2}, Lmaps/z/as;->a(Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/i/n;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/ag;->y:Lmaps/i/n;

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/ag;->y:Lmaps/i/n;

    invoke-interface {v1}, Lmaps/i/n;->a()Lmaps/y/bc;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/i/h;->a(Lmaps/y/bc;)V

    iget-object v0, p0, Lmaps/z/ag;->s:Lmaps/af/h;

    new-instance v1, Lmaps/z/af;

    iget-object v2, p0, Lmaps/z/ag;->u:Lmaps/i/m;

    invoke-direct {v1, v2, v3}, Lmaps/z/af;-><init>(Lmaps/i/m;Lmaps/z/ai;)V

    invoke-virtual {v0, v1}, Lmaps/af/h;->a(Lmaps/af/u;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/z/ag;->s:Lmaps/af/h;

    invoke-virtual {v0, v3}, Lmaps/af/h;->a(Lmaps/af/u;)V

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/ag;->y:Lmaps/i/n;

    invoke-interface {v1}, Lmaps/i/n;->a()Lmaps/y/bc;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/i/h;->b(Lmaps/y/bc;)V

    iget-object v0, p0, Lmaps/z/ag;->y:Lmaps/i/n;

    invoke-interface {v0}, Lmaps/i/n;->b()V

    iput-object v3, p0, Lmaps/z/ag;->y:Lmaps/i/n;

    iget-object v0, p0, Lmaps/z/ag;->k:Lmaps/aa/g;

    invoke-interface {v0}, Lmaps/aa/g;->d()Lmaps/i/u;

    move-result-object v0

    invoke-interface {v0, v3}, Lmaps/i/u;->a(Lmaps/i/m;)V

    iget-object v0, p0, Lmaps/z/ag;->k:Lmaps/aa/g;

    invoke-interface {v0}, Lmaps/aa/g;->d()Lmaps/i/u;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lmaps/i/u;->a(I)V

    iget-object v0, p0, Lmaps/z/ag;->u:Lmaps/i/m;

    invoke-interface {v0}, Lmaps/i/m;->b()V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/maps/GoogleMapOptions;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getUseViewLifecycleInFragment()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getUseViewLifecycleInFragment()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lmaps/z/ag;)Lmaps/i/c;
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->d:Lmaps/i/c;

    return-object v0
.end method

.method private static b(Lmaps/y/ab;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lmaps/ap/p;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lmaps/t/cc;

    invoke-direct {v0}, Lmaps/t/cc;-><init>()V

    invoke-virtual {v0, p1}, Lmaps/t/cc;->a(Ljava/lang/String;)Lmaps/t/cc;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cc;->a()Lmaps/t/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/y/ab;->a(Lmaps/t/ao;)Z

    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lmaps/z/ag;->D:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/z/ag;->D:Z

    iget-object v0, p0, Lmaps/z/ag;->k:Lmaps/aa/g;

    invoke-interface {v0}, Lmaps/aa/g;->b()Lmaps/aa/h;

    move-result-object v0

    if-eqz p1, :cond_1

    new-instance v1, Lmaps/z/ae;

    iget-object v2, p0, Lmaps/z/ag;->d:Lmaps/i/c;

    invoke-direct {v1, v2, v0}, Lmaps/z/ae;-><init>(Lmaps/i/c;Lmaps/aa/h;)V

    iput-object v1, p0, Lmaps/z/ag;->C:Lmaps/z/ae;

    iget-object v1, p0, Lmaps/z/ag;->C:Lmaps/z/ae;

    invoke-virtual {p0}, Lmaps/z/ag;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/z/ae;->onCameraChange(Lcom/google/android/gms/maps/model/CameraPosition;)V

    iget-object v1, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    iget-object v2, p0, Lmaps/z/ag;->C:Lmaps/z/ae;

    invoke-interface {v1, v2}, Lmaps/z/x;->a(Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;)V

    iget-object v1, p0, Lmaps/z/ag;->B:Lmaps/aa/b;

    invoke-interface {v0, v1}, Lmaps/aa/h;->a(Lmaps/aa/b;)V

    :goto_0
    invoke-interface {v0, p1}, Lmaps/aa/h;->a(Z)V

    :cond_0
    return-void

    :cond_1
    invoke-interface {v0, v3}, Lmaps/aa/h;->a(Lmaps/aa/b;)V

    iget-object v1, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    iget-object v2, p0, Lmaps/z/ag;->C:Lmaps/z/ae;

    invoke-interface {v1, v2}, Lmaps/z/x;->b(Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;)V

    iput-object v3, p0, Lmaps/z/ag;->C:Lmaps/z/ae;

    goto :goto_0
.end method

.method static synthetic c(Lmaps/z/ag;)F
    .locals 1

    invoke-direct {p0}, Lmaps/z/ag;->i()F

    move-result v0

    return v0
.end method

.method private c(Z)V
    .locals 2

    iget-boolean v0, p0, Lmaps/z/ag;->E:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/z/ag;->E:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/ag;->z:Lmaps/y/u;

    invoke-interface {v0, v1}, Lmaps/i/h;->a(Lmaps/y/bc;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/ag;->z:Lmaps/y/u;

    invoke-interface {v0, v1}, Lmaps/i/h;->b(Lmaps/y/bc;)V

    goto :goto_0
.end method

.method static synthetic d(Lmaps/z/ag;)F
    .locals 1

    invoke-direct {p0}, Lmaps/z/ag;->j()F

    move-result v0

    return v0
.end method

.method private d(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->i:Lmaps/z/z;

    invoke-interface {v0, p1}, Lmaps/z/z;->a(Z)V

    return-void
.end method

.method private e(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0, p1}, Lmaps/i/h;->c(Z)V

    return-void
.end method

.method private f(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0, p1}, Lmaps/i/h;->d(Z)V

    return-void
.end method

.method private g(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0, p1}, Lmaps/i/h;->e(Z)V

    return-void
.end method

.method private h(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0, p1}, Lmaps/i/h;->f(Z)V

    return-void
.end method

.method private static h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method private i()F
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->d:Lmaps/i/c;

    iget-object v1, p0, Lmaps/z/ag;->d:Lmaps/i/c;

    invoke-interface {v1}, Lmaps/i/c;->a()Lmaps/bq/a;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/i/c;->a(Lmaps/t/bx;)F

    move-result v0

    return v0
.end method

.method private j()F
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->d:Lmaps/i/c;

    invoke-interface {v0}, Lmaps/i/c;->b()F

    move-result v0

    return v0
.end method

.method private k()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->u:Lmaps/i/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/ag;->u:Lmaps/i/m;

    invoke-interface {v0}, Lmaps/i/m;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lmaps/z/ag;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    iget-object v2, p0, Lmaps/z/ag;->t:Landroid/content/res/Resources;

    invoke-virtual {v0, v2}, Lmaps/k/b;->a(Landroid/content/res/Resources;)Z

    move-result v0

    iget-object v2, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/cf/a;->u:Lmaps/cf/a;

    :goto_0
    invoke-interface {v2, v0}, Lmaps/i/h;->a(Lmaps/cf/a;)V

    sget-object v0, Lmaps/y/bp;->a:Lmaps/y/bp;

    new-instance v2, Lmaps/y/u;

    iget-object v3, p0, Lmaps/z/ag;->t:Landroid/content/res/Resources;

    invoke-direct {v2, v3, v0}, Lmaps/y/u;-><init>(Landroid/content/res/Resources;Lmaps/y/bp;)V

    iput-object v2, p0, Lmaps/z/ag;->z:Lmaps/y/u;

    iget-object v0, p0, Lmaps/z/ag;->z:Lmaps/y/u;

    invoke-virtual {v0, v5}, Lmaps/y/u;->b_(Z)V

    iget-object v0, p0, Lmaps/z/ag;->z:Lmaps/y/u;

    new-instance v2, Lmaps/z/aj;

    invoke-direct {v2, p0}, Lmaps/z/aj;-><init>(Lmaps/z/ag;)V

    invoke-virtual {v0, v2}, Lmaps/y/u;->a(Lmaps/y/p;)V

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getCompassEnabled()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getCompassEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/z/ag;->setCompassEnabled(Z)V

    :goto_1
    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v2, p0, Lmaps/z/ag;->j:Lmaps/z/as;

    sget-object v3, Lmaps/o/c;->o:Lmaps/o/c;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lmaps/z/as;->c(Lmaps/o/c;Landroid/content/res/Resources;)Lmaps/y/bc;

    move-result-object v2

    invoke-interface {v0, v2}, Lmaps/i/h;->a(Lmaps/y/bc;)V

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v2, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v2}, Lmaps/i/h;->p()Lmaps/af/v;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/af/v;->k()Lmaps/y/ab;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/y/ab;->r()Lmaps/cq/b;

    move-result-object v2

    sget-object v3, Lmaps/o/c;->a:Lmaps/o/c;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lmaps/y/ab;->a(Lmaps/cq/b;Lmaps/o/c;Landroid/content/res/Resources;)Lmaps/y/ab;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/i/h;->a(Lmaps/y/bc;)V

    invoke-direct {p0, v5}, Lmaps/z/ag;->a(Z)V

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/ag;->f:Lmaps/z/bu;

    invoke-interface {v1}, Lmaps/z/bu;->b()Lmaps/y/bc;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/i/h;->a(Lmaps/y/bc;)V

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/ag;->g:Lmaps/z/bn;

    invoke-interface {v1}, Lmaps/z/bn;->j()Lmaps/y/bc;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/i/h;->a(Lmaps/y/bc;)V

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getZoomControlsEnabled()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getZoomControlsEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/z/ag;->setZoomControlsEnabled(Z)V

    :goto_2
    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getMapType()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getMapType()I

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/z/ag;->setMapType(I)V

    :cond_0
    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getZoomGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getZoomGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/z/ag;->setZoomGesturesEnabled(Z)V

    :goto_3
    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getScrollGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getScrollGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/z/ag;->setScrollGesturesEnabled(Z)V

    :goto_4
    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getTiltGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getTiltGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/z/ag;->setTiltGesturesEnabled(Z)V

    :goto_5
    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getRotateGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getRotateGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/z/ag;->setRotateGesturesEnabled(Z)V

    :goto_6
    invoke-direct {p0, v5}, Lmaps/z/ag;->d(Z)V

    return-void

    :cond_1
    sget-object v0, Lmaps/cf/a;->t:Lmaps/cf/a;

    goto/16 :goto_0

    :cond_2
    invoke-direct {p0, v5}, Lmaps/z/ag;->c(Z)V

    goto/16 :goto_1

    :cond_3
    invoke-direct {p0, v5}, Lmaps/z/ag;->b(Z)V

    goto :goto_2

    :cond_4
    invoke-direct {p0, v5}, Lmaps/z/ag;->f(Z)V

    goto :goto_3

    :cond_5
    invoke-direct {p0, v5}, Lmaps/z/ag;->e(Z)V

    goto :goto_4

    :cond_6
    invoke-direct {p0, v5}, Lmaps/z/ag;->g(Z)V

    goto :goto_5

    :cond_7
    invoke-direct {p0, v5}, Lmaps/z/ag;->h(Z)V

    goto :goto_6
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/z/ag;->r:Lmaps/z/bc;

    const-string v1, "on_create"

    invoke-interface {v0, v1}, Lmaps/z/bc;->a(Ljava/lang/String;)Lmaps/z/bp;

    move-result-object v1

    const-string v0, "camera"

    invoke-static {p1, v0}, Lcom/google/android/gms/maps/internal/MapStateHelper;->getParcelableFromMapStateBundle(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/CameraPosition;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getCamera()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;->getCamera()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    :cond_0
    :goto_0
    iget-object v2, p0, Lmaps/z/ag;->d:Lmaps/i/c;

    invoke-static {v0}, Lmaps/au/c;->a(Lcom/google/android/gms/maps/model/CameraPosition;)Lmaps/bq/a;

    move-result-object v0

    invoke-interface {v2, v0, v3, v3}, Lmaps/i/c;->a(Lmaps/bq/b;II)V

    iget-object v0, p0, Lmaps/z/ag;->r:Lmaps/z/bc;

    invoke-interface {v0, v1}, Lmaps/z/bc;->a(Lmaps/z/bp;)V

    return-void

    :cond_1
    sget-object v0, Lmaps/z/ag;->a:Lcom/google/android/gms/maps/model/CameraPosition;

    goto :goto_0
.end method

.method public addCircle(Lcom/google/android/gms/maps/model/CircleOptions;)Lcom/google/android/gms/maps/model/internal/ICircleDelegate;
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->E:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->g:Lmaps/z/bn;

    invoke-interface {v0, p1}, Lmaps/z/bn;->b(Lcom/google/android/gms/maps/model/CircleOptions;)Lcom/google/android/gms/maps/model/internal/ICircleDelegate;

    move-result-object v0

    return-object v0
.end method

.method public addGroundOverlay(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lcom/google/android/gms/maps/model/internal/IGroundOverlayDelegate;
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->N:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->g:Lmaps/z/bn;

    invoke-interface {v0, p1}, Lmaps/z/bn;->b(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lcom/google/android/gms/maps/model/internal/IGroundOverlayDelegate;

    move-result-object v0

    return-object v0
.end method

.method public addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->b:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->f:Lmaps/z/bu;

    invoke-interface {v0, p1}, Lmaps/z/bu;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/internal/IMarkerDelegate;

    move-result-object v0

    return-object v0
.end method

.method public addPolygon(Lcom/google/android/gms/maps/model/PolygonOptions;)Lcom/google/android/gms/maps/model/internal/IPolygonDelegate;
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->u:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->g:Lmaps/z/bn;

    invoke-interface {v0, p1}, Lmaps/z/bn;->b(Lcom/google/android/gms/maps/model/PolygonOptions;)Lcom/google/android/gms/maps/model/internal/IPolygonDelegate;

    move-result-object v0

    return-object v0
.end method

.method public addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/internal/IPolylineDelegate;
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->m:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->g:Lmaps/z/bn;

    invoke-interface {v0, p1}, Lmaps/z/bn;->b(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/internal/IPolylineDelegate;

    move-result-object v0

    return-object v0
.end method

.method public addTileOverlay(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lcom/google/android/gms/maps/model/internal/ITileOverlayDelegate;
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->V:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->g:Lmaps/z/bn;

    invoke-interface {v0, p1}, Lmaps/z/bn;->b(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lcom/google/android/gms/maps/model/internal/ITileOverlayDelegate;

    move-result-object v0

    return-object v0
.end method

.method public animateCamera(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    .locals 4

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->aa:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/b;

    iget-object v1, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, Lmaps/z/x;->a(Lmaps/z/b;ILcom/google/android/gms/maps/internal/ICancelableCallback;)V

    return-void
.end method

.method public animateCameraWithCallback(Lcom/google/android/gms/dynamic/IObjectWrapper;Lcom/google/android/gms/maps/internal/ICancelableCallback;)V
    .locals 3

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ab:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/b;

    iget-object v1, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    const/4 v2, -0x1

    invoke-interface {v1, v0, v2, p2}, Lmaps/z/x;->a(Lmaps/z/b;ILcom/google/android/gms/maps/internal/ICancelableCallback;)V

    return-void
.end method

.method public animateCameraWithDurationAndCallback(Lcom/google/android/gms/dynamic/IObjectWrapper;ILcom/google/android/gms/maps/internal/ICancelableCallback;)V
    .locals 3

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ac:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/b;

    if-lez p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "durationMs must be positive"

    invoke-static {v1, v2}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    invoke-interface {v1, v0, p2, p3}, Lmaps/z/x;->a(Lmaps/z/b;ILcom/google/android/gms/maps/internal/ICancelableCallback;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    invoke-interface {v0}, Lmaps/bf/b;->a()V

    invoke-direct {p0, v1}, Lmaps/z/ag;->a(Z)V

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0, v1}, Lmaps/i/h;->h(Z)V

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0}, Lmaps/i/h;->d()V

    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "MapOptions"

    iget-object v1, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/maps/internal/MapStateHelper;->saveState(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "camera"

    iget-object v1, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    invoke-interface {v1}, Lmaps/z/x;->b()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/maps/internal/MapStateHelper;->saveState(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->r:Lmaps/z/bc;

    const-string v1, "on_resume"

    invoke-interface {v0, v1}, Lmaps/z/bc;->a(Ljava/lang/String;)Lmaps/z/bp;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/ag;->l:Lmaps/z/bg;

    invoke-interface {v1}, Lmaps/z/bg;->a()V

    iget-object v1, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v1}, Lmaps/i/h;->c()V

    iget-object v1, p0, Lmaps/z/ag;->h:Lmaps/ch/b;

    invoke-interface {v1}, Lmaps/ch/b;->a()V

    iget-object v1, p0, Lmaps/z/ag;->r:Lmaps/z/bc;

    invoke-interface {v1, v0}, Lmaps/z/bc;->a(Lmaps/z/bp;)V

    return-void
.end method

.method public clear()V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->af:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->f:Lmaps/z/bu;

    invoke-interface {v0}, Lmaps/z/bu;->a()V

    iget-object v0, p0, Lmaps/z/ag;->g:Lmaps/z/bn;

    invoke-interface {v0}, Lmaps/z/bn;->i()V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->h:Lmaps/ch/b;

    invoke-interface {v0}, Lmaps/ch/b;->b()V

    iget-object v0, p0, Lmaps/z/ag;->l:Lmaps/z/bg;

    invoke-interface {v0}, Lmaps/z/bg;->b()V

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0}, Lmaps/i/h;->b()V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0}, Lmaps/i/h;->e()V

    return-void
.end method

.method public f()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->o:Landroid/view/View;

    return-object v0
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->q:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-static {v0}, Lmaps/z/ag;->a(Lcom/google/android/gms/maps/GoogleMapOptions;)Z

    move-result v0

    return v0
.end method

.method public getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    invoke-interface {v0}, Lmaps/z/x;->b()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method public getMapType()I
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget v0, p0, Lmaps/z/ag;->x:I

    return v0
.end method

.method public getMaxZoomLevel()F
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    invoke-direct {p0}, Lmaps/z/ag;->i()F

    move-result v0

    return v0
.end method

.method public getMinZoomLevel()F
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    invoke-direct {p0}, Lmaps/z/ag;->j()F

    move-result v0

    return v0
.end method

.method public getMyLocation()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->i:Lmaps/z/z;

    invoke-interface {v0}, Lmaps/z/z;->e()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public getProjection()Lcom/google/android/gms/maps/internal/IProjectionDelegate;
    .locals 3

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    new-instance v0, Lmaps/z/bf;

    iget-object v1, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v1}, Lmaps/i/h;->h()Lmaps/bq/d;

    move-result-object v1

    iget-object v2, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    invoke-direct {v0, v1, v2}, Lmaps/z/bf;-><init>(Lmaps/bq/d;Lmaps/bf/b;)V

    return-object v0
.end method

.method public getTestingHelper()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public getUiSettings()Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->A:Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/z/ak;

    invoke-direct {v0, p0}, Lmaps/z/ak;-><init>(Lmaps/z/ag;)V

    iput-object v0, p0, Lmaps/z/ag;->A:Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;

    :cond_0
    iget-object v0, p0, Lmaps/z/ag;->A:Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;

    return-object v0
.end method

.method public isCompassEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/z/ag;->E:Z

    return v0
.end method

.method public isIndoorEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    invoke-direct {p0}, Lmaps/z/ag;->k()Z

    move-result v0

    return v0
.end method

.method public isMyLocationButtonEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->i:Lmaps/z/z;

    invoke-interface {v0}, Lmaps/z/z;->d()Z

    move-result v0

    return v0
.end method

.method public isMyLocationEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->i:Lmaps/z/z;

    invoke-interface {v0}, Lmaps/z/z;->c()Z

    move-result v0

    return v0
.end method

.method public isRotateGesturesEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0}, Lmaps/i/h;->o()Z

    move-result v0

    return v0
.end method

.method public isScrollGesturesEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0}, Lmaps/i/h;->l()Z

    move-result v0

    return v0
.end method

.method public isTiltGesturesEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0}, Lmaps/i/h;->n()Z

    move-result v0

    return v0
.end method

.method public isTrafficEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->v:Lmaps/y/bc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isZoomControlsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/z/ag;->D:Z

    return v0
.end method

.method public isZoomGesturesEnabled()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0}, Lmaps/i/h;->m()Z

    move-result v0

    return v0
.end method

.method public moveCamera(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    .locals 4

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ad:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/b;

    iget-object v1, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, Lmaps/z/x;->a(Lmaps/z/b;ILcom/google/android/gms/maps/internal/ICancelableCallback;)V

    return-void
.end method

.method public setAllGesturesEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v1, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/bf/c;->aT:Lmaps/bf/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->e(Z)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->f(Z)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->g(Z)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->h(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/bf/c;->aU:Lmaps/bf/c;

    goto :goto_0
.end method

.method public setCompassEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v1, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/bf/c;->aF:Lmaps/bf/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->c(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/bf/c;->aC:Lmaps/bf/c;

    goto :goto_0
.end method

.method public setIndoorEnabled(Z)Z
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v1, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/bf/c;->be:Lmaps/bf/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->a(Z)V

    invoke-direct {p0}, Lmaps/z/ag;->k()Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lmaps/bf/c;->bf:Lmaps/bf/c;

    goto :goto_0
.end method

.method public setInfoWindowAdapter(Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->k:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->f:Lmaps/z/bu;

    invoke-interface {v0, p1}, Lmaps/z/bu;->a(Lcom/google/android/gms/maps/internal/IInfoWindowAdapter;)V

    return-void
.end method

.method public setLocationSource(Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ap:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    :goto_0
    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->i:Lmaps/z/z;

    invoke-interface {v0, p1}, Lmaps/z/z;->a(Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;)V

    return-void

    :cond_0
    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->an:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    goto :goto_0
.end method

.method public setMapType(I)V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ag:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    sget-object v1, Lmaps/o/c;->a:Lmaps/o/c;

    sget-object v0, Lmaps/af/q;->a:Lmaps/af/q;

    :goto_0
    iget-object v3, p0, Lmaps/z/ag;->w:Lmaps/y/bc;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v4, p0, Lmaps/z/ag;->w:Lmaps/y/bc;

    invoke-interface {v3, v4}, Lmaps/i/h;->b(Lmaps/y/bc;)V

    iput-object v2, p0, Lmaps/z/ag;->w:Lmaps/y/bc;

    :cond_0
    if-eqz v1, :cond_1

    sget-object v2, Lmaps/o/c;->a:Lmaps/o/c;

    if-eq v1, v2, :cond_1

    iget-object v2, p0, Lmaps/z/ag;->j:Lmaps/z/as;

    iget-object v3, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v3}, Lmaps/i/h;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lmaps/z/as;->d(Lmaps/o/c;Landroid/content/res/Resources;)Lmaps/y/bc;

    move-result-object v1

    iput-object v1, p0, Lmaps/z/ag;->w:Lmaps/y/bc;

    iget-object v1, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v2, p0, Lmaps/z/ag;->w:Lmaps/y/bc;

    invoke-interface {v1, v2}, Lmaps/i/h;->a(Lmaps/y/bc;)V

    :cond_1
    iget-object v2, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    sget-object v1, Lmaps/af/q;->f:Lmaps/af/q;

    if-eq v0, v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-interface {v2, v1}, Lmaps/i/h;->g(Z)V

    iget-object v1, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v1, v0}, Lmaps/i/h;->a(Lmaps/af/q;)V

    iput p1, p0, Lmaps/z/ag;->x:I

    return-void

    :pswitch_1
    sget-object v0, Lmaps/af/q;->f:Lmaps/af/q;

    move-object v1, v2

    goto :goto_0

    :pswitch_2
    sget-object v1, Lmaps/o/c;->d:Lmaps/o/c;

    sget-object v0, Lmaps/af/q;->e:Lmaps/af/q;

    goto :goto_0

    :pswitch_3
    sget-object v1, Lmaps/o/c;->d:Lmaps/o/c;

    sget-object v0, Lmaps/af/q;->b:Lmaps/af/q;

    goto :goto_0

    :pswitch_4
    sget-object v1, Lmaps/o/c;->e:Lmaps/o/c;

    sget-object v0, Lmaps/af/q;->d:Lmaps/af/q;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public setMyLocationButtonEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v1, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/bf/c;->aG:Lmaps/bf/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->d(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/bf/c;->aD:Lmaps/bf/c;

    goto :goto_0
.end method

.method public setMyLocationEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->am:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->i:Lmaps/z/z;

    invoke-interface {v0}, Lmaps/z/z;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->al:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->i:Lmaps/z/z;

    invoke-interface {v0}, Lmaps/z/z;->b()V

    goto :goto_0
.end method

.method public setOnCameraChangeListener(Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->at:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    invoke-interface {v0, p1}, Lmaps/z/x;->c(Lcom/google/android/gms/maps/internal/IOnCameraChangeListener;)V

    return-void
.end method

.method public setOnInfoWindowClickListener(Lcom/google/android/gms/maps/internal/IOnInfoWindowClickListener;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ar:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->f:Lmaps/z/bu;

    invoke-interface {v0, p1}, Lmaps/z/bu;->a(Lcom/google/android/gms/maps/internal/IOnInfoWindowClickListener;)V

    return-void
.end method

.method public setOnMapClickListener(Lcom/google/android/gms/maps/internal/IOnMapClickListener;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->aw:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0, p1}, Lmaps/i/h;->a(Lcom/google/android/gms/maps/internal/IOnMapClickListener;)V

    return-void
.end method

.method public setOnMapLongClickListener(Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ax:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v0, p1}, Lmaps/i/h;->a(Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;)V

    return-void
.end method

.method public setOnMarkerClickListener(Lcom/google/android/gms/maps/internal/IOnMarkerClickListener;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ay:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->f:Lmaps/z/bu;

    invoke-interface {v0, p1}, Lmaps/z/bu;->a(Lcom/google/android/gms/maps/internal/IOnMarkerClickListener;)V

    return-void
.end method

.method public setOnMarkerDragListener(Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->az:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->f:Lmaps/z/bu;

    invoke-interface {v0, p1}, Lmaps/z/bu;->a(Lcom/google/android/gms/maps/internal/IOnMarkerDragListener;)V

    return-void
.end method

.method public setOnMyLocationChangeListener(Lcom/google/android/gms/maps/internal/IOnMyLocationChangeListener;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->aq:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->i:Lmaps/z/z;

    invoke-interface {v0, p1}, Lmaps/z/z;->a(Lcom/google/android/gms/maps/internal/IOnMyLocationChangeListener;)V

    return-void
.end method

.method public setRotateGesturesEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v1, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/bf/c;->aP:Lmaps/bf/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->h(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/bf/c;->aQ:Lmaps/bf/c;

    goto :goto_0
.end method

.method public setScrollGesturesEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v1, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/bf/c;->aL:Lmaps/bf/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->e(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/bf/c;->aM:Lmaps/bf/c;

    goto :goto_0
.end method

.method public setTiltGesturesEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v1, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/bf/c;->aR:Lmaps/bf/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->g(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/bf/c;->aS:Lmaps/bf/c;

    goto :goto_0
.end method

.method public setTrafficEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ai:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->v:Lmaps/y/bc;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/z/ag;->j:Lmaps/z/as;

    iget-object v1, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    invoke-interface {v1}, Lmaps/i/h;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/z/as;->b(Landroid/content/res/Resources;)Lmaps/y/bc;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/ag;->v:Lmaps/y/bc;

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/ag;->v:Lmaps/y/bc;

    invoke-interface {v0, v1}, Lmaps/i/h;->a(Lmaps/y/bc;)V

    :cond_0
    :goto_0
    sget-boolean v0, Lmaps/z/ag;->b:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/z/ag;->v:Lmaps/y/bc;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eq p1, v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ah:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->v:Lmaps/y/bc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/ag;->e:Lmaps/i/h;

    iget-object v1, p0, Lmaps/z/ag;->v:Lmaps/y/bc;

    invoke-interface {v0, v1}, Lmaps/i/h;->b(Lmaps/y/bc;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/z/ag;->v:Lmaps/y/bc;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    return-void
.end method

.method public setZoomControlsEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v1, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/bf/c;->aH:Lmaps/bf/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->b(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/bf/c;->aE:Lmaps/bf/c;

    goto :goto_0
.end method

.method public setZoomGesturesEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v1, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    if-eqz p1, :cond_0

    sget-object v0, Lmaps/bf/c;->aN:Lmaps/bf/c;

    :goto_0
    invoke-interface {v1, v0}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    invoke-direct {p0, p1}, Lmaps/z/ag;->f(Z)V

    return-void

    :cond_0
    sget-object v0, Lmaps/bf/c;->aO:Lmaps/bf/c;

    goto :goto_0
.end method

.method public stopAnimation()V
    .locals 2

    iget-object v0, p0, Lmaps/z/ag;->n:Lmaps/au/e;

    invoke-virtual {v0}, Lmaps/au/e;->b()V

    iget-object v0, p0, Lmaps/z/ag;->p:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->ae:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v0, p0, Lmaps/z/ag;->c:Lmaps/z/x;

    invoke-interface {v0}, Lmaps/z/x;->a()V

    return-void
.end method
