.class final Lmaps/z/bd;
.super Lcom/google/android/gms/maps/model/internal/IPolygonDelegate$Stub;

# interfaces
.implements Lmaps/z/a;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lmaps/z/bi;

.field private c:F

.field private d:I

.field private e:I

.field private final f:Ljava/util/List;

.field private final g:Ljava/util/List;

.field private h:Lmaps/t/cg;

.field private i:Ljava/util/List;

.field private j:Lmaps/y/bk;

.field private k:F

.field private l:Z

.field private m:Z

.field private final n:Lmaps/bf/b;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/PolygonOptions;Lmaps/z/bi;Lmaps/bf/b;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/IPolygonDelegate$Stub;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->getStrokeWidth()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lmaps/z/bd;->a:Ljava/lang/String;

    iput-object p3, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    iput-object p4, p0, Lmaps/z/bd;->n:Lmaps/bf/b;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->getStrokeWidth()F

    move-result v0

    iput v0, p0, Lmaps/z/bd;->c:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->getStrokeColor()I

    move-result v0

    iput v0, p0, Lmaps/z/bd;->d:I

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->getFillColor()I

    move-result v0

    iput v0, p0, Lmaps/z/bd;->e:I

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->getZIndex()F

    move-result v0

    iput v0, p0, Lmaps/z/bd;->k:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->isVisible()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/z/bd;->l:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->isGeodesic()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/z/bd;->m:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->getPoints()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/bd;->f:Ljava/util/List;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/bd;->g:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/PolygonOptions;->getHoles()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v2, p0, Lmaps/z/bd;->g:Ljava/util/List;

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    return-void
.end method

.method static a(Ljava/util/List;)V
    .locals 3

    const/4 v2, 0x0

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method static b(Ljava/util/List;)Lmaps/t/cg;
    .locals 3

    new-instance v1, Lmaps/t/by;

    invoke-direct {v1}, Lmaps/t/by;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cg;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/t/cg;->c(I)Lmaps/t/cg;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/cg;->f()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lmaps/t/cg;->h()Lmaps/t/cg;

    move-result-object v0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lmaps/bq/d;Lmaps/cr/c;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/bd;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/z/bd;->j:Lmaps/y/bk;

    invoke-virtual {v0, p1, p2}, Lmaps/y/bk;->a(Lmaps/bq/d;Lmaps/cr/c;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/bd;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/z/bd;->j:Lmaps/y/bk;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/y/bk;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method c()V
    .locals 6

    iget-object v0, p0, Lmaps/z/bd;->f:Ljava/util/List;

    invoke-static {v0}, Lmaps/z/bd;->a(Ljava/util/List;)V

    iget-boolean v0, p0, Lmaps/z/bd;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/z/bd;->f:Ljava/util/List;

    invoke-static {v0}, Lmaps/au/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lmaps/z/bd;->b(Ljava/util/List;)Lmaps/t/cg;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/bd;->h:Lmaps/t/cg;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/bd;->i:Ljava/util/List;

    iget-object v0, p0, Lmaps/z/bd;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lmaps/z/bd;->a(Ljava/util/List;)V

    iget-object v2, p0, Lmaps/z/bd;->i:Ljava/util/List;

    iget-boolean v3, p0, Lmaps/z/bd;->m:Z

    if-eqz v3, :cond_0

    invoke-static {v0}, Lmaps/au/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lmaps/z/bd;->b(Ljava/util/List;)Lmaps/t/cg;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lmaps/z/bd;->f:Ljava/util/List;

    goto :goto_0

    :cond_2
    new-instance v0, Lmaps/y/bk;

    iget-object v1, p0, Lmaps/z/bd;->h:Lmaps/t/cg;

    iget-object v2, p0, Lmaps/z/bd;->i:Ljava/util/List;

    iget v3, p0, Lmaps/z/bd;->c:F

    float-to-int v3, v3

    iget v4, p0, Lmaps/z/bd;->d:I

    invoke-static {v4}, Lmaps/au/c;->a(I)I

    move-result v4

    iget v5, p0, Lmaps/z/bd;->e:I

    invoke-static {v5}, Lmaps/au/c;->a(I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/y/bk;-><init>(Lmaps/t/cg;Ljava/util/List;III)V

    iput-object v0, p0, Lmaps/z/bd;->j:Lmaps/y/bk;

    return-void
.end method

.method public equalsRemote(Lcom/google/android/gms/maps/model/internal/IPolygonDelegate;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getFillColor()I
    .locals 1

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/bd;->e:I

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getHoles()Ljava/util/List;
    .locals 3

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bd;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/bd;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPoints()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bd;->f:Ljava/util/List;

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getStrokeColor()I
    .locals 1

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/bd;->d:I

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getStrokeWidth()F
    .locals 1

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/bd;->c:F

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getZIndex()F
    .locals 1

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget v0, p0, Lmaps/z/bd;->k:F

    return v0
.end method

.method public hashCodeRemote()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isGeodesic()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/bd;->m:Z

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isVisible()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/bd;->l:Z

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/bd;->n:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->v:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->a(Lmaps/z/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setFillColor(I)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/bd;->n:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->A:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/bd;->e:I

    iget-object v0, p0, Lmaps/z/bd;->j:Lmaps/y/bk;

    invoke-static {p1}, Lmaps/au/c;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/y/bk;->c(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setGeodesic(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/bd;->n:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->D:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/bd;->m:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/z/bd;->m:Z

    invoke-virtual {p0}, Lmaps/z/bd;->c()V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setHoles(Ljava/util/List;)V
    .locals 3

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/bd;->n:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->x:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bd;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v2, p0, Lmaps/z/bd;->g:Ljava/util/List;

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lmaps/z/bd;->c()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void
.end method

.method public setPoints(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/bd;->n:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->w:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/bd;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/z/bd;->f:Ljava/util/List;

    invoke-static {v0, p1}, Lmaps/f/fa;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    invoke-virtual {p0}, Lmaps/z/bd;->c()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setStrokeColor(I)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/bd;->n:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->z:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/bd;->d:I

    iget-object v0, p0, Lmaps/z/bd;->j:Lmaps/y/bk;

    invoke-static {p1}, Lmaps/au/c;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/y/bk;->b(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setStrokeWidth(F)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/bd;->n:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->y:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/bd;->c:F

    iget-object v0, p0, Lmaps/z/bd;->j:Lmaps/y/bk;

    float-to-int v1, p1

    invoke-virtual {v0, v1}, Lmaps/y/bk;->d(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setVisible(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/bd;->n:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->C:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/z/bd;->l:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setZIndex(F)V
    .locals 2

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/bd;->n:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->B:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->b(Lmaps/z/a;)V

    iput p1, p0, Lmaps/z/bd;->k:F

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->c(Lmaps/z/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lmaps/z/bd;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "points"

    iget-object v2, p0, Lmaps/z/bd;->f:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "holes"

    iget-object v2, p0, Lmaps/z/bd;->g:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "strokeWidth"

    iget v2, p0, Lmaps/z/bd;->c:F

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;F)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "strokeColor"

    iget v2, p0, Lmaps/z/bd;->d:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    move-result-object v0

    const-string v1, "fillColor"

    iget v2, p0, Lmaps/z/bd;->e:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
