.class public Lmaps/z/r;
.super Lcom/google/android/gms/maps/internal/IMapFragmentDelegate$Stub;


# instance fields
.field private final a:Lmaps/z/av;

.field private b:Lmaps/z/ag;

.field private c:Lcom/google/android/gms/maps/GoogleMapOptions;


# direct methods
.method public constructor <init>(Lmaps/z/av;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/IMapFragmentDelegate$Stub;-><init>()V

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/av;

    iput-object v0, p0, Lmaps/z/r;->a:Lmaps/z/av;

    return-void
.end method

.method public static a(Landroid/app/Activity;)Lmaps/z/r;
    .locals 3

    invoke-static {p0}, Lmaps/au/a;->a(Landroid/app/Activity;)Z

    move-result v0

    new-instance v1, Lmaps/z/r;

    new-instance v2, Lmaps/z/bw;

    invoke-direct {v2, v0}, Lmaps/z/bw;-><init>(Z)V

    invoke-direct {v1, v2}, Lmaps/z/r;-><init>(Lmaps/z/av;)V

    return-object v1
.end method


# virtual methods
.method public getMap()Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;
    .locals 1

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    return-object v0
.end method

.method public isReady()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmaps/z/r;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    if-nez v0, :cond_0

    const-string v0, "MapOptions"

    invoke-static {p1, v0}, Lcom/google/android/gms/maps/internal/MapStateHelper;->getParcelableFromMapStateBundle(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/GoogleMapOptions;

    iput-object v0, p0, Lmaps/z/r;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    :cond_0
    iget-object v0, p0, Lmaps/z/r;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;-><init>()V

    iput-object v0, p0, Lmaps/z/r;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    :cond_1
    return-void
.end method

.method public onCreateView(Lcom/google/android/gms/dynamic/IObjectWrapper;Lcom/google/android/gms/dynamic/IObjectWrapper;Landroid/os/Bundle;)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 3

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v1, p0, Lmaps/z/r;->a:Lmaps/z/av;

    iget-object v2, p0, Lmaps/z/r;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v1, v0, v2}, Lmaps/z/av;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/GoogleMapOptions;)Lmaps/z/ag;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    invoke-virtual {v0, p3}, Lmaps/z/ag;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->f()Landroid/view/View;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->f()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->b()V

    iput-object v1, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    :cond_0
    iput-object v1, p0, Lmaps/z/r;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    :cond_0
    return-void
.end method

.method public onInflate(Lcom/google/android/gms/dynamic/IObjectWrapper;Lcom/google/android/gms/maps/GoogleMapOptions;Landroid/os/Bundle;)V
    .locals 0

    iput-object p2, p0, Lmaps/z/r;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->e()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->d()V

    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    invoke-virtual {v0}, Lmaps/z/ag;->c()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/z/r;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    if-eqz v0, :cond_0

    const-string v0, "MapOptions"

    iget-object v1, p0, Lmaps/z/r;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/maps/internal/MapStateHelper;->saveState(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/z/r;->b:Lmaps/z/ag;

    invoke-virtual {v0, p1}, Lmaps/z/ag;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method
