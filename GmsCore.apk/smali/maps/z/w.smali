.class public Lmaps/z/w;
.super Lmaps/af/e;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lmaps/i/v;

.field private c:Z

.field private d:Lmaps/ao/b;

.field private e:Lmaps/z/bc;

.field private final f:Lmaps/z/bp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/z/w;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmaps/z/w;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lmaps/i/v;Lmaps/z/bc;Lmaps/z/bp;)V
    .locals 0

    invoke-direct {p0}, Lmaps/af/e;-><init>()V

    iput-object p2, p0, Lmaps/z/w;->e:Lmaps/z/bc;

    iput-object p1, p0, Lmaps/z/w;->b:Lmaps/i/v;

    iput-object p3, p0, Lmaps/z/w;->f:Lmaps/z/bp;

    return-void
.end method

.method public static a(Lmaps/i/h;Lmaps/i/v;Lmaps/z/bc;Lmaps/z/bp;)Lmaps/z/w;
    .locals 1

    new-instance v0, Lmaps/z/w;

    invoke-direct {v0, p1, p2, p3}, Lmaps/z/w;-><init>(Lmaps/i/v;Lmaps/z/bc;Lmaps/z/bp;)V

    invoke-interface {p0, v0}, Lmaps/i/h;->a(Lmaps/af/e;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(Z)Z
    .locals 3

    const/4 v0, 0x1

    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lmaps/z/w;->c:Z

    iget-object v1, p0, Lmaps/z/w;->e:Lmaps/z/bc;

    iget-object v2, p0, Lmaps/z/w;->f:Lmaps/z/bp;

    invoke-interface {v1, v2}, Lmaps/z/bc;->a(Lmaps/z/bp;)V

    iget-object v1, p0, Lmaps/z/w;->e:Lmaps/z/bc;

    invoke-interface {v1}, Lmaps/z/bc;->b()V

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/z/w;->e:Lmaps/z/bc;

    iget-object v1, p0, Lmaps/z/w;->b:Lmaps/i/v;

    invoke-interface {v1, p0}, Lmaps/i/v;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized run()V
    .locals 1

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lmaps/z/w;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/z/w;->c:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/z/w;->d:Lmaps/ao/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/z/w;->d:Lmaps/ao/b;

    invoke-interface {v0}, Lmaps/ao/b;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/z/w;->d:Lmaps/ao/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void
.end method
