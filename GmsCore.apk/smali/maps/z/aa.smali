.class public final Lmaps/z/aa;
.super Lcom/google/android/gms/maps/model/internal/IGroundOverlayDelegate$Stub;

# interfaces
.implements Lmaps/z/a;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lmaps/z/bi;

.field private final c:Lmaps/z/br;

.field private final d:Lmaps/z/bl;

.field private final e:F

.field private final f:F

.field private final g:Landroid/graphics/Bitmap;

.field private final h:I

.field private final i:I

.field private final j:Lmaps/t/bx;

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:Z

.field private p:F

.field private q:Lmaps/t/bw;

.field private r:Z

.field private s:Lmaps/al/o;

.field private t:Lmaps/cr/a;

.field private u:Lmaps/al/q;

.field private final v:Lmaps/bf/b;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lmaps/z/bl;Lcom/google/android/gms/maps/model/GroundOverlayOptions;Lmaps/z/bi;Lmaps/bf/b;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/IGroundOverlayDelegate$Stub;-><init>()V

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    iput-object p1, p0, Lmaps/z/aa;->a:Ljava/lang/String;

    iput-object p4, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    iput-object p2, p0, Lmaps/z/aa;->d:Lmaps/z/bl;

    iput-object p5, p0, Lmaps/z/aa;->v:Lmaps/bf/b;

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getZIndex()F

    move-result v0

    iput v0, p0, Lmaps/z/aa;->n:F

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->isVisible()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/z/aa;->o:Z

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getTransparency()F

    move-result v0

    iput v0, p0, Lmaps/z/aa;->p:F

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getImage()Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Options doesn\'t specify an image"

    invoke-static {v0, v3}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getImage()Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/BitmapDescriptor;->getRemoteObject()Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/z/br;

    iput-object v0, p0, Lmaps/z/aa;->c:Lmaps/z/br;

    iget-object v0, p0, Lmaps/z/aa;->d:Lmaps/z/bl;

    iget-object v3, p0, Lmaps/z/aa;->c:Lmaps/z/br;

    invoke-virtual {v0, v3}, Lmaps/z/bl;->a(Lmaps/z/br;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/aa;->g:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lmaps/z/aa;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lmaps/z/aa;->h:I

    iget-object v0, p0, Lmaps/z/aa;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lmaps/z/aa;->i:I

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getAnchorU()F

    move-result v0

    iput v0, p0, Lmaps/z/aa;->e:F

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getAnchorV()F

    move-result v0

    iput v0, p0, Lmaps/z/aa;->f:F

    invoke-direct {p0}, Lmaps/z/aa;->e()V

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getLocation()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getBounds()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    const-string v0, "Options doesn\'t specify a position"

    invoke-static {v2, v0}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getBounds()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getBounds()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/z/aa;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    iget-object v2, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    iget-wide v3, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v5, v1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {v2, v3, v4, v5, v6}, Lmaps/t/bx;->b(DD)V

    invoke-direct {p0, v0}, Lmaps/z/aa;->b(Lcom/google/android/gms/maps/model/LatLngBounds;)F

    move-result v1

    iput v1, p0, Lmaps/z/aa;->k:F

    invoke-direct {p0, v0}, Lmaps/z/aa;->c(Lcom/google/android/gms/maps/model/LatLngBounds;)F

    move-result v0

    iput v0, p0, Lmaps/z/aa;->l:F

    :goto_1
    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getBearing()F

    move-result v0

    iput v0, p0, Lmaps/z/aa;->m:F

    invoke-direct {p0}, Lmaps/z/aa;->f()V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getLocation()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {v1, v2, v3, v4, v5}, Lmaps/t/bx;->b(DD)V

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getWidth()F

    move-result v0

    iput v0, p0, Lmaps/z/aa;->k:F

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getHeight()F

    move-result v0

    const/high16 v1, -0x40800000

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->getHeight()F

    move-result v0

    :goto_2
    iput v0, p0, Lmaps/z/aa;->l:F

    goto :goto_1

    :cond_4
    iget v0, p0, Lmaps/z/aa;->i:I

    int-to-float v0, v0

    iget v1, p0, Lmaps/z/aa;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lmaps/z/aa;->k:F

    mul-float/2addr v0, v1

    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 8

    const/high16 v7, 0x3f800000

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iget-object v2, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v0

    new-instance v1, Lmaps/t/bx;

    const/high16 v2, 0x40000000

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v0, v1}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    :goto_0
    iget-object v1, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v1

    iget v2, p0, Lmaps/z/aa;->e:F

    iget v3, p0, Lmaps/z/aa;->f:F

    new-instance v4, Lmaps/t/bx;

    sub-float v5, v7, v2

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v2, v6

    add-float/2addr v2, v5

    float-to-int v2, v2

    sub-float v5, v7, v3

    invoke-virtual {v0}, Lmaps/t/bx;->g()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v5

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {v4, v2, v0}, Lmaps/t/bx;-><init>(II)V

    invoke-static {v4}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/maps/model/LatLngBounds;)F
    .locals 6

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iget-object v2, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-object v2, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    const-wide v4, 0x4076800000000000L

    add-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lmaps/t/bx;->a(DD)Lmaps/t/bx;

    move-result-object v0

    :goto_0
    iget-object v1, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v0

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-double v0, v0

    iget-object v2, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->e()D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lmaps/cr/c;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lmaps/cr/a;

    invoke-direct {v0, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    iput-object v0, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    iget-object v0, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    invoke-virtual {v0, v1}, Lmaps/cr/a;->c(Z)V

    iget-object v0, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    invoke-virtual {v0, v1}, Lmaps/cr/a;->d(Z)V

    invoke-static {}, Lmaps/bm/f;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    iget-object v1, p0, Lmaps/z/aa;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/cr/a;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    iget-object v1, p0, Lmaps/z/aa;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/cr/a;->b(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private c()F
    .locals 2

    iget v0, p0, Lmaps/z/aa;->e:F

    iget v1, p0, Lmaps/z/aa;->h:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private c(Lcom/google/android/gms/maps/model/LatLngBounds;)F
    .locals 4

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/au/c;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/t/bx;->g()I

    move-result v0

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-double v0, v0

    iget-object v2, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->e()D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private d()F
    .locals 2

    iget v0, p0, Lmaps/z/aa;->f:F

    iget v1, p0, Lmaps/z/aa;->i:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private e()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Lmaps/al/o;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lmaps/al/o;-><init>(I)V

    iput-object v0, p0, Lmaps/z/aa;->s:Lmaps/al/o;

    iget-object v0, p0, Lmaps/z/aa;->s:Lmaps/al/o;

    invoke-direct {p0}, Lmaps/z/aa;->c()F

    move-result v1

    neg-float v1, v1

    invoke-direct {p0}, Lmaps/z/aa;->d()F

    move-result v2

    invoke-virtual {v0, v1, v2, v4}, Lmaps/al/o;->a(FFF)V

    iget-object v0, p0, Lmaps/z/aa;->s:Lmaps/al/o;

    invoke-direct {p0}, Lmaps/z/aa;->c()F

    move-result v1

    neg-float v1, v1

    invoke-direct {p0}, Lmaps/z/aa;->d()F

    move-result v2

    iget v3, p0, Lmaps/z/aa;->i:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2, v4}, Lmaps/al/o;->a(FFF)V

    iget-object v0, p0, Lmaps/z/aa;->s:Lmaps/al/o;

    iget v1, p0, Lmaps/z/aa;->h:I

    int-to-float v1, v1

    invoke-direct {p0}, Lmaps/z/aa;->c()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-direct {p0}, Lmaps/z/aa;->d()F

    move-result v2

    invoke-virtual {v0, v1, v2, v4}, Lmaps/al/o;->a(FFF)V

    iget-object v0, p0, Lmaps/z/aa;->s:Lmaps/al/o;

    iget v1, p0, Lmaps/z/aa;->h:I

    int-to-float v1, v1

    invoke-direct {p0}, Lmaps/z/aa;->c()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-direct {p0}, Lmaps/z/aa;->d()F

    move-result v2

    iget v3, p0, Lmaps/z/aa;->i:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2, v4}, Lmaps/al/o;->a(FFF)V

    return-void
.end method

.method private f()V
    .locals 6

    iget v0, p0, Lmaps/z/aa;->m:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/z/aa;->i()Lmaps/t/bw;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/aa;->q:Lmaps/t/bw;

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lmaps/z/aa;->k:F

    iget v1, p0, Lmaps/z/aa;->k:F

    mul-float/2addr v0, v1

    iget v1, p0, Lmaps/z/aa;->l:F

    iget v2, p0, Lmaps/z/aa;->l:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    new-instance v2, Lmaps/t/bx;

    iget-object v3, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    invoke-virtual {v3}, Lmaps/t/bx;->e()D

    move-result-wide v3

    mul-double/2addr v3, v0

    double-to-int v3, v3

    iget-object v4, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    invoke-virtual {v4}, Lmaps/t/bx;->e()D

    move-result-wide v4

    mul-double/2addr v0, v4

    double-to-int v0, v0

    invoke-direct {v2, v3, v0}, Lmaps/t/bx;-><init>(II)V

    new-instance v0, Lmaps/t/ax;

    iget-object v1, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    invoke-virtual {v1, v2}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v1

    iget-object v3, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    invoke-virtual {v3, v2}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v0}, Lmaps/t/bw;->a(Lmaps/t/ax;)Lmaps/t/bw;

    move-result-object v0

    iput-object v0, p0, Lmaps/z/aa;->q:Lmaps/t/bw;

    goto :goto_0
.end method

.method private g()V
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    :cond_0
    return-void
.end method

.method private h()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/z/aa;->u:Lmaps/al/q;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/al/q;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lmaps/al/q;-><init>(I)V

    iput-object v0, p0, Lmaps/z/aa;->u:Lmaps/al/q;

    iget-object v0, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->b()F

    move-result v0

    iget-object v1, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    invoke-virtual {v1}, Lmaps/cr/a;->c()F

    move-result v1

    iget-object v2, p0, Lmaps/z/aa;->u:Lmaps/al/q;

    invoke-virtual {v2, v3, v3}, Lmaps/al/q;->a(FF)V

    iget-object v2, p0, Lmaps/z/aa;->u:Lmaps/al/q;

    invoke-virtual {v2, v3, v1}, Lmaps/al/q;->a(FF)V

    iget-object v2, p0, Lmaps/z/aa;->u:Lmaps/al/q;

    invoke-virtual {v2, v0, v3}, Lmaps/al/q;->a(FF)V

    iget-object v2, p0, Lmaps/z/aa;->u:Lmaps/al/q;

    invoke-virtual {v2, v0, v1}, Lmaps/al/q;->a(FF)V

    :cond_0
    return-void
.end method

.method private i()Lmaps/t/bw;
    .locals 7

    new-instance v0, Lmaps/t/ax;

    iget-object v1, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    new-instance v2, Lmaps/t/bx;

    invoke-direct {p0}, Lmaps/z/aa;->c()F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {p0, v3, v4}, Lmaps/z/aa;->a(D)D

    move-result-wide v3

    double-to-int v3, v3

    iget v4, p0, Lmaps/z/aa;->i:I

    int-to-float v4, v4

    invoke-direct {p0}, Lmaps/z/aa;->d()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {p0, v4, v5}, Lmaps/z/aa;->b(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-direct {v2, v3, v4}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v1, v2}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    new-instance v3, Lmaps/t/bx;

    iget v4, p0, Lmaps/z/aa;->h:I

    int-to-float v4, v4

    invoke-direct {p0}, Lmaps/z/aa;->c()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-double v4, v4

    invoke-virtual {p0, v4, v5}, Lmaps/z/aa;->a(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-direct {p0}, Lmaps/z/aa;->d()F

    move-result v5

    float-to-double v5, v5

    invoke-virtual {p0, v5, v6}, Lmaps/z/aa;->b(D)D

    move-result-wide v5

    double-to-int v5, v5

    invoke-direct {v3, v4, v5}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v2, v3}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-static {v0}, Lmaps/t/bw;->a(Lmaps/t/ax;)Lmaps/t/bw;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a(D)D
    .locals 4

    iget-object v0, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    invoke-virtual {v0}, Lmaps/t/bx;->e()D

    move-result-wide v0

    mul-double/2addr v0, p1

    iget v2, p0, Lmaps/z/aa;->k:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lmaps/z/aa;->h:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lmaps/bq/d;Lmaps/cr/c;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/aa;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/z/aa;->q:Lmaps/t/bw;

    invoke-virtual {v0}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v0

    iget-object v1, p0, Lmaps/z/aa;->q:Lmaps/t/bw;

    invoke-virtual {v1}, Lmaps/t/bw;->g()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v1

    if-gt v0, v1, :cond_1

    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/aa;->q:Lmaps/t/bw;

    invoke-virtual {v1}, Lmaps/t/bw;->b()Lmaps/t/ax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/t/u;->b(Lmaps/t/an;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lmaps/z/aa;->r:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public declared-synchronized a(Lmaps/cr/c;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/z/aa;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 6

    const/high16 v5, 0x3f800000

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/aa;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lmaps/z/aa;->r:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/aa;->s:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    invoke-direct {p0, p1}, Lmaps/z/aa;->b(Lmaps/cr/c;)V

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const/high16 v1, 0x3f800000

    const/high16 v2, 0x3f800000

    const/high16 v3, 0x3f800000

    iget v4, p0, Lmaps/z/aa;->p:F

    sub-float v4, v5, v4

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    invoke-direct {p0}, Lmaps/z/aa;->h()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    const/high16 v2, 0x3f800000

    invoke-static {p1, p2, v1, v2}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    iget v1, p0, Lmaps/z/aa;->m:F

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, -0x40800000

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const-wide/high16 v1, 0x3ff0000000000000L

    invoke-virtual {p0, v1, v2}, Lmaps/z/aa;->a(D)D

    move-result-wide v1

    double-to-float v1, v1

    const-wide/high16 v2, 0x3ff0000000000000L

    invoke-virtual {p0, v2, v3}, Lmaps/z/aa;->b(D)D

    move-result-wide v2

    double-to-float v2, v2

    const/high16 v3, 0x3f800000

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget-object v1, p0, Lmaps/z/aa;->u:Lmaps/al/q;

    invoke-virtual {v1, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/z/aa;->t:Lmaps/cr/a;

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method b(D)D
    .locals 4

    iget-object v0, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    invoke-virtual {v0}, Lmaps/t/bx;->e()D

    move-result-wide v0

    mul-double/2addr v0, p1

    iget v2, p0, Lmaps/z/aa;->l:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lmaps/z/aa;->i:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public declared-synchronized b()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/aa;->d:Lmaps/z/bl;

    iget-object v1, p0, Lmaps/z/aa;->c:Lmaps/z/br;

    invoke-virtual {v0, v1}, Lmaps/z/bl;->b(Lmaps/z/br;)V

    invoke-direct {p0}, Lmaps/z/aa;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equalsRemote(Lcom/google/android/gms/maps/model/internal/IGroundOverlayDelegate;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getBearing()F
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/aa;->m:F

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBounds()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/z/aa;->i()Lmaps/t/bw;

    move-result-object v0

    invoke-static {v0}, Lmaps/au/c;->a(Lmaps/t/bw;)Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getHeight()F
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/aa;->l:F

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    invoke-static {v0}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getTransparency()F
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/aa;->p:F

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getWidth()F
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/z/aa;->k:F

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getZIndex()F
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget v0, p0, Lmaps/z/aa;->n:F

    return v0
.end method

.method public hashCodeRemote()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/z/aa;->o:Z

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/aa;->v:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->O:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->a(Lmaps/z/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setBearing(F)V
    .locals 2

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/aa;->v:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->P:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/aa;->m:F

    invoke-direct {p0}, Lmaps/z/aa;->f()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setDimensions(F)V
    .locals 1

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    const/high16 v0, -0x40800000

    invoke-virtual {p0, p1, v0}, Lmaps/z/aa;->setDimensionsWithHeight(FF)V

    return-void
.end method

.method public setDimensionsWithHeight(FF)V
    .locals 2

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/aa;->v:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->Q:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/aa;->k:F

    const/high16 v0, -0x40800000

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_0

    :goto_0
    iput p2, p0, Lmaps/z/aa;->l:F

    invoke-direct {p0}, Lmaps/z/aa;->f()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lmaps/z/aa;->i:I

    int-to-float v0, v0

    iget v1, p0, Lmaps/z/aa;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lmaps/z/aa;->k:F

    mul-float p2, v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setPosition(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 5

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/aa;->v:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->R:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    iget-wide v1, p1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v3, p1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {v0, v1, v2, v3, v4}, Lmaps/t/bx;->b(DD)V

    invoke-direct {p0}, Lmaps/z/aa;->f()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setPositionFromBounds(Lcom/google/android/gms/maps/model/LatLngBounds;)V
    .locals 6

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lmaps/z/aa;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iget-object v1, p0, Lmaps/z/aa;->j:Lmaps/t/bx;

    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {v1, v2, v3, v4, v5}, Lmaps/t/bx;->b(DD)V

    invoke-direct {p0, p1}, Lmaps/z/aa;->b(Lcom/google/android/gms/maps/model/LatLngBounds;)F

    move-result v0

    iput v0, p0, Lmaps/z/aa;->k:F

    invoke-direct {p0, p1}, Lmaps/z/aa;->c(Lcom/google/android/gms/maps/model/LatLngBounds;)F

    move-result v0

    iput v0, p0, Lmaps/z/aa;->l:F

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setTransparency(F)V
    .locals 2

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/aa;->v:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->U:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Transparency must be in the range [0..1]"

    invoke-static {v0, v1}, Lmaps/ap/q;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/z/aa;->p:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setVisible(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/aa;->v:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->T:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/z/aa;->o:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setZIndex(F)V
    .locals 2

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->e()V

    iget-object v0, p0, Lmaps/z/aa;->v:Lmaps/bf/b;

    sget-object v1, Lmaps/bf/c;->S:Lmaps/bf/c;

    invoke-interface {v0, v1}, Lmaps/bf/b;->b(Lmaps/bf/c;)V

    iget-object v1, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->b(Lmaps/z/a;)V

    iput p1, p0, Lmaps/z/aa;->n:F

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0, p0}, Lmaps/z/bi;->c(Lmaps/z/a;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/z/aa;->b:Lmaps/z/bi;

    invoke-virtual {v0}, Lmaps/z/bi;->h()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
