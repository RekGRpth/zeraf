.class Lmaps/af/b;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ak/i;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/os/Handler;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/af/y;

    invoke-direct {v0, p0}, Lmaps/af/y;-><init>(Lmaps/af/b;)V

    iput-object v0, p0, Lmaps/af/b;->a:Landroid/os/Handler;

    new-instance v0, Lmaps/af/x;

    invoke-direct {v0, p0}, Lmaps/af/x;-><init>(Lmaps/af/b;)V

    iput-object v0, p0, Lmaps/af/b;->b:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lmaps/af/z;)V
    .locals 0

    invoke-direct {p0}, Lmaps/af/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/ak/j;)V
    .locals 2

    iget-object v0, p0, Lmaps/af/b;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public b(Lmaps/ak/j;)V
    .locals 2

    iget-object v0, p0, Lmaps/af/b;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
