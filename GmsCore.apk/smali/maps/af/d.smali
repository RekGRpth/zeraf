.class public Lmaps/af/d;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljavax/microedition/khronos/opengles/GL10;Lmaps/bq/d;)V
    .locals 4

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/bq/d;->q()F

    move-result v0

    neg-float v0, v0

    invoke-interface {p0, v0, v2, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v0

    const/high16 v1, 0x42b40000

    sub-float/2addr v0, v1

    invoke-interface {p0, v0, v3, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    return-void
.end method

.method public static a(Ljavax/microedition/khronos/opengles/GL10;[F)V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    aget v0, p1, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    const/4 v2, 0x2

    aget v2, p1, v2

    invoke-interface {p0, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    aget v0, p1, v3

    aget v1, p1, v3

    aget v2, p1, v3

    invoke-interface {p0, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    return-void
.end method

.method public static a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V
    .locals 2

    iget-object v0, p0, Lmaps/cr/c;->l:[F

    invoke-static {p0, p1, p2, p3, v0}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, Lmaps/cr/c;->l:[F

    invoke-static {v0, v1}, Lmaps/af/d;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    return-void
.end method

.method private static a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;FZ[F)V
    .locals 4

    if-nez p0, :cond_1

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1}, Lmaps/t/bx;-><init>()V

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    :goto_0
    invoke-virtual {p1, v1}, Lmaps/bq/d;->a(Lmaps/t/bx;)V

    invoke-static {p2, v1, v0}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    if-eqz p4, :cond_0

    invoke-virtual {v0, v0}, Lmaps/t/bx;->i(Lmaps/t/bx;)V

    :cond_0
    invoke-virtual {p1}, Lmaps/bq/d;->x()F

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v1

    aput v3, p5, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Lmaps/t/bx;->g()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v1

    aput v3, p5, v2

    const/4 v2, 0x2

    invoke-virtual {v0}, Lmaps/t/bx;->h()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    aput v0, p5, v2

    const/4 v0, 0x3

    mul-float/2addr v1, p3

    aput v1, p5, v0

    return-void

    :cond_1
    iget-object v1, p0, Lmaps/cr/c;->m:Lmaps/t/bx;

    iget-object v0, p0, Lmaps/cr/c;->n:Lmaps/t/bx;

    goto :goto_0
.end method

.method public static a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V
    .locals 6

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;FZ[F)V

    return-void
.end method

.method public static b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V
    .locals 2

    iget-object v0, p0, Lmaps/cr/c;->l:[F

    invoke-static {p0, p1, p2, p3, v0}, Lmaps/af/d;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, Lmaps/cr/c;->l:[F

    invoke-static {v0, v1}, Lmaps/af/d;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    return-void
.end method

.method public static b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;FZ[F)V

    return-void
.end method
