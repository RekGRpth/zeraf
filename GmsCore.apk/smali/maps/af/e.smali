.class public abstract Lmaps/af/e;
.super Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:Lmaps/af/v;

.field private c:Z


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    return-void
.end method

.method protected a(Lmaps/ac/c;)V
    .locals 0

    return-void
.end method

.method protected a(Lmaps/ac/c;Z)V
    .locals 0

    return-void
.end method

.method final a(Lmaps/af/v;)V
    .locals 0

    iput-object p1, p0, Lmaps/af/e;->b:Lmaps/af/v;

    return-void
.end method

.method protected a(Lmaps/y/bc;)V
    .locals 0

    return-void
.end method

.method protected a(Z)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected b(Lmaps/y/bc;)V
    .locals 0

    return-void
.end method

.method final b(Z)V
    .locals 2

    invoke-virtual {p0, p1}, Lmaps/af/e;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/e;->b:Lmaps/af/v;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/af/v;->a(Lmaps/af/e;)V

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/af/e;->a:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final b()Z
    .locals 2

    iget-boolean v0, p0, Lmaps/af/e;->c:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/af/e;->c:Z

    return v0
.end method
