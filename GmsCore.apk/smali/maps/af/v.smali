.class public Lmaps/af/v;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ax/c;
.implements Lmaps/p/b;


# static fields
.field public static volatile a:Z

.field public static final b:Ljava/lang/ThreadLocal;

.field private static final f:[I

.field private static final g:[I

.field private static final h:[I

.field private static final i:[I

.field private static final l:Ljava/util/Comparator;


# instance fields
.field private final A:Lmaps/y/an;

.field private final B:Lmaps/y/an;

.field private final C:Lmaps/y/z;

.field private final D:Lmaps/y/f;

.field private final E:Lmaps/u/a;

.field private F:Lmaps/u/b;

.field private final G:Ljava/util/HashSet;

.field private final H:Ljava/util/HashSet;

.field private final I:[I

.field private final J:Ljava/util/List;

.field private K:J

.field private L:Z

.field private final M:Lmaps/y/h;

.field private N:Ljava/util/List;

.field private O:Z

.field private P:Landroid/graphics/Bitmap;

.field private Q:Z

.field private R:F

.field private S:J

.field private volatile T:Lmaps/af/q;

.field private final U:Ljava/util/List;

.field private final V:Ljava/util/List;

.field private W:Lmaps/af/e;

.field private volatile X:Lmaps/af/e;

.field private volatile Y:Lmaps/af/e;

.field private volatile Z:Z

.field private aa:J

.field private ab:I

.field private volatile ac:Lmaps/y/ab;

.field private ad:Z

.field private volatile ae:F

.field private volatile af:Z

.field private ag:Z

.field private final ah:Ljava/lang/Object;

.field private ai:Z

.field private volatile aj:I

.field private ak:Z

.field private al:I

.field private am:J

.field private an:Z

.field private final ao:Lmaps/q/g;

.field private final ap:Lmaps/q/ad;

.field private aq:Lmaps/cm/f;

.field private volatile ar:J

.field private final as:Ljava/lang/Object;

.field protected c:Ljava/util/Map;

.field protected d:Ljava/util/List;

.field protected e:Z

.field private volatile j:Lmaps/cf/a;

.field private volatile k:Lmaps/bq/a;

.field private m:Lmaps/cr/c;

.field private volatile n:I

.field private volatile o:I

.field private final p:Ljava/util/LinkedList;

.field private final q:Ljava/util/ArrayList;

.field private final r:Ljava/util/ArrayList;

.field private final s:Ljava/util/ArrayList;

.field private final t:Lmaps/bq/d;

.field private final u:Lmaps/p/au;

.field private final v:Lmaps/s/h;

.field private final w:Landroid/content/res/Resources;

.field private final x:F

.field private y:Lmaps/ac/c;

.field private z:Lmaps/ac/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x4

    sget-boolean v0, Lmaps/ae/h;->q:Z

    sput-boolean v0, Lmaps/af/v;->a:Z

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/af/v;->f:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lmaps/af/v;->g:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lmaps/af/v;->h:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lmaps/af/v;->i:[I

    new-instance v0, Lmaps/af/j;

    invoke-direct {v0}, Lmaps/af/j;-><init>()V

    sput-object v0, Lmaps/af/v;->b:Ljava/lang/ThreadLocal;

    new-instance v0, Lmaps/af/k;

    invoke-direct {v0}, Lmaps/af/k;-><init>()V

    sput-object v0, Lmaps/af/v;->l:Ljava/util/Comparator;

    return-void

    :array_0
    .array-data 4
        0xed00
        0xea00
        0xe200
        0x10000
    .end array-data

    :array_1
    .array-data 4
        0x8000
        0x8000
        0x8000
        0x10000
    .end array-data

    :array_2
    .array-data 4
        0x8000
        0x8000
        0x8000
        0x10000
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Lmaps/p/au;Landroid/content/res/Resources;Lmaps/bq/d;Lmaps/y/ab;Lmaps/q/ad;Lmaps/q/g;)V
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lmaps/af/v;->o:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->G:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->H:Ljava/util/HashSet;

    new-array v0, v3, [I

    iput-object v0, p0, Lmaps/af/v;->I:[I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->J:Ljava/util/List;

    iput-wide v5, p0, Lmaps/af/v;->K:J

    new-instance v0, Lmaps/af/l;

    invoke-direct {v0, p0}, Lmaps/af/l;-><init>(Lmaps/af/v;)V

    iput-object v0, p0, Lmaps/af/v;->M:Lmaps/y/h;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->U:Ljava/util/List;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/af/v;->V:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->ah:Ljava/lang/Object;

    iput-boolean v1, p0, Lmaps/af/v;->ai:Z

    iput v1, p0, Lmaps/af/v;->aj:I

    iput-boolean v1, p0, Lmaps/af/v;->ak:Z

    const v0, 0x7fffffff

    iput v0, p0, Lmaps/af/v;->al:I

    iput-wide v5, p0, Lmaps/af/v;->am:J

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->c:Ljava/util/Map;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/af/v;->d:Ljava/util/List;

    iput-boolean v3, p0, Lmaps/af/v;->e:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/af/v;->ar:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->as:Ljava/lang/Object;

    sget-object v0, Lmaps/cf/a;->s:Lmaps/cf/a;

    iput-object v0, p0, Lmaps/af/v;->j:Lmaps/cf/a;

    sget-object v0, Lmaps/af/q;->a:Lmaps/af/q;

    iput-object v0, p0, Lmaps/af/v;->T:Lmaps/af/q;

    iput-object p1, p0, Lmaps/af/v;->u:Lmaps/p/au;

    new-instance v0, Lmaps/s/h;

    invoke-direct {v0, p0}, Lmaps/s/h;-><init>(Lmaps/af/v;)V

    iput-object v0, p0, Lmaps/af/v;->v:Lmaps/s/h;

    iput-object p2, p0, Lmaps/af/v;->w:Landroid/content/res/Resources;

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lmaps/af/v;->x:F

    iget v0, p0, Lmaps/af/v;->x:F

    invoke-static {v0}, Lmaps/l/ar;->a(F)V

    iget v0, p0, Lmaps/af/v;->x:F

    invoke-static {v0}, Lmaps/l/q;->a(F)V

    iput-object p3, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->q:Ljava/util/ArrayList;

    iput-object p4, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->r:Ljava/util/ArrayList;

    iget-object v0, p0, Lmaps/af/v;->r:Ljava/util/ArrayList;

    iget-object v1, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->s:Ljava/util/ArrayList;

    new-instance v0, Lmaps/y/f;

    invoke-direct {v0, p2}, Lmaps/y/f;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lmaps/af/v;->D:Lmaps/y/f;

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_2

    iput-object v4, p0, Lmaps/af/v;->A:Lmaps/y/an;

    iput-object v4, p0, Lmaps/af/v;->B:Lmaps/y/an;

    :goto_0
    new-instance v0, Lmaps/y/z;

    invoke-direct {v0}, Lmaps/y/z;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->C:Lmaps/y/z;

    new-instance v0, Lmaps/u/a;

    invoke-direct {v0}, Lmaps/u/a;-><init>()V

    iput-object v0, p0, Lmaps/af/v;->E:Lmaps/u/a;

    iget-object v0, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    invoke-direct {p0, v0}, Lmaps/af/v;->c(Lmaps/y/bc;)V

    iget-object v0, p0, Lmaps/af/v;->A:Lmaps/y/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->A:Lmaps/y/an;

    invoke-direct {p0, v0}, Lmaps/af/v;->c(Lmaps/y/bc;)V

    :cond_0
    iget-object v0, p0, Lmaps/af/v;->D:Lmaps/y/f;

    invoke-direct {p0, v0}, Lmaps/af/v;->c(Lmaps/y/bc;)V

    iget-object v0, p0, Lmaps/af/v;->E:Lmaps/u/a;

    invoke-direct {p0, v0}, Lmaps/af/v;->c(Lmaps/y/bc;)V

    iget-object v0, p0, Lmaps/af/v;->C:Lmaps/y/z;

    invoke-direct {p0, v0}, Lmaps/af/v;->c(Lmaps/y/bc;)V

    new-instance v0, Lmaps/y/ay;

    sget-object v1, Lmaps/y/am;->d:Lmaps/y/am;

    invoke-direct {v0, v1}, Lmaps/y/ay;-><init>(Lmaps/y/am;)V

    sget-object v1, Lmaps/af/q;->c:Lmaps/af/q;

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v1, v2}, Lmaps/y/ay;->a(Lmaps/af/q;I)V

    invoke-direct {p0, v0}, Lmaps/af/v;->c(Lmaps/y/bc;)V

    new-instance v0, Lmaps/y/be;

    invoke-direct {v0}, Lmaps/y/be;-><init>()V

    invoke-direct {p0, v0}, Lmaps/af/v;->c(Lmaps/y/bc;)V

    invoke-static {}, Lmaps/af/w;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lmaps/u/b;

    invoke-direct {v0, p2}, Lmaps/u/b;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lmaps/af/v;->F:Lmaps/u/b;

    iget-object v0, p0, Lmaps/af/v;->E:Lmaps/u/a;

    iget-object v1, p0, Lmaps/af/v;->F:Lmaps/u/b;

    invoke-virtual {v0, v1}, Lmaps/u/a;->a(Lmaps/u/c;)V

    :goto_1
    iget-object v0, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    iget-object v1, p0, Lmaps/af/v;->M:Lmaps/y/h;

    invoke-virtual {v0, v1}, Lmaps/y/ab;->a(Lmaps/y/h;)V

    iget-object v0, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    invoke-virtual {v0, v3}, Lmaps/y/ab;->b(Z)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/af/v;->S:J

    iput-object p5, p0, Lmaps/af/v;->ap:Lmaps/q/ad;

    iput-object p6, p0, Lmaps/af/v;->ao:Lmaps/q/g;

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    new-instance v0, Lmaps/q/h;

    iget-object v1, p0, Lmaps/af/v;->ao:Lmaps/q/g;

    const/16 v2, 0x10

    const/high16 v3, -0x40800000

    const/high16 v4, 0x3f800000

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/q/h;-><init>(Lmaps/q/av;IFF)V

    iget-object v1, p0, Lmaps/af/v;->ap:Lmaps/q/ad;

    invoke-virtual {v1, v0}, Lmaps/q/ad;->a(Lmaps/q/h;)V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Lmaps/y/an;

    const/4 v1, 0x2

    iget-object v2, p0, Lmaps/af/v;->D:Lmaps/y/f;

    invoke-direct {v0, v1, v2}, Lmaps/y/an;-><init>(ILmaps/y/f;)V

    iput-object v0, p0, Lmaps/af/v;->A:Lmaps/y/an;

    new-instance v0, Lmaps/y/an;

    iget-object v1, p0, Lmaps/af/v;->D:Lmaps/y/f;

    invoke-direct {v0, v3, v1}, Lmaps/y/an;-><init>(ILmaps/y/f;)V

    iput-object v0, p0, Lmaps/af/v;->B:Lmaps/y/an;

    goto/16 :goto_0

    :cond_3
    iput-object v4, p0, Lmaps/af/v;->F:Lmaps/u/b;

    goto :goto_1
.end method

.method static a(IIF)F
    .locals 2

    add-int v0, p0, p1

    int-to-float v0, v0

    const/high16 v1, 0x43800000

    mul-float/2addr v1, p2

    div-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    const/high16 v1, 0x3f800000

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000

    invoke-static {v0}, Lmaps/bq/d;->a(F)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lmaps/af/v;)Lmaps/u/b;
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->F:Lmaps/u/b;

    return-object v0
.end method

.method private a(I)V
    .locals 8

    const/4 v2, 0x1

    const/4 v1, 0x0

    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v3}, Lmaps/cr/c;->C()Lmaps/p/au;

    move-result-object v3

    invoke-virtual {v3, v0}, Lmaps/p/au;->a(Z)V

    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0}, Lmaps/p/au;->h()Z

    move-result v0

    iget-object v3, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    invoke-virtual {v3}, Lmaps/bq/d;->g()J

    move-result-wide v3

    iget-wide v5, p0, Lmaps/af/v;->K:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_a

    move v3, v2

    :goto_1
    if-eqz v3, :cond_1

    iget-object v0, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    invoke-virtual {v0}, Lmaps/bq/d;->g()J

    move-result-wide v4

    iput-wide v4, p0, Lmaps/af/v;->K:J

    move v0, v2

    :cond_1
    invoke-direct {p0, v3}, Lmaps/af/v;->e(Z)V

    iget-object v3, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v3}, Lmaps/cr/c;->f()V

    invoke-direct {p0}, Lmaps/af/v;->q()V

    iget-object v3, p0, Lmaps/af/v;->X:Lmaps/af/e;

    iput-object v3, p0, Lmaps/af/v;->W:Lmaps/af/e;

    iget-object v3, p0, Lmaps/af/v;->W:Lmaps/af/e;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lmaps/af/v;->W:Lmaps/af/e;

    invoke-virtual {v3, p0}, Lmaps/af/e;->a(Lmaps/af/v;)V

    iget-object v3, p0, Lmaps/af/v;->W:Lmaps/af/e;

    invoke-virtual {v3}, Lmaps/af/e;->a()V

    :cond_2
    iget-object v3, p0, Lmaps/af/v;->Y:Lmaps/af/e;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lmaps/af/v;->Y:Lmaps/af/e;

    invoke-virtual {v3}, Lmaps/af/e;->a()V

    :cond_3
    iget-object v3, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    invoke-virtual {v3}, Lmaps/bq/d;->p()F

    move-result v3

    const/high16 v4, 0x3f800000

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    iget-object v4, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    sget-boolean v3, Lmaps/ae/h;->o:Z

    if-nez v3, :cond_b

    move v3, v2

    :goto_2
    invoke-direct {p0, v4, p1, v0, v3}, Lmaps/af/v;->a(Lmaps/bq/d;IZZ)V

    iget-object v3, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v3}, Lmaps/cr/c;->g()V

    :cond_4
    iget-object v3, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v3}, Lmaps/p/au;->i()Z

    move-result v3

    if-eqz v3, :cond_c

    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_c

    move v3, v2

    :goto_3
    iget-boolean v4, p0, Lmaps/af/v;->an:Z

    if-eqz v4, :cond_d

    iget-object v4, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v4}, Lmaps/ac/c;->c()Z

    move-result v4

    if-nez v4, :cond_d

    iget-object v4, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v4}, Lmaps/cr/c;->d()Z

    move-result v4

    if-nez v4, :cond_d

    if-nez v3, :cond_d

    :goto_4
    iget-object v3, p0, Lmaps/af/v;->W:Lmaps/af/e;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lmaps/af/v;->W:Lmaps/af/e;

    invoke-virtual {v3, v2}, Lmaps/af/e;->b(Z)V

    iget-object v2, p0, Lmaps/af/v;->W:Lmaps/af/e;

    invoke-virtual {v2}, Lmaps/af/e;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v2, v1, v1}, Lmaps/p/au;->a(ZZ)V

    :cond_5
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/af/v;->O:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_6

    invoke-direct {p0}, Lmaps/af/v;->o()Landroid/graphics/Bitmap;

    move-result-object v2

    monitor-enter p0

    :try_start_1
    iput-object v2, p0, Lmaps/af/v;->P:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lmaps/af/v;->O:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_6
    iget-object v2, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v2}, Lmaps/ac/c;->c()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v2}, Lmaps/cr/c;->d()Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_7
    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0, v1, v1}, Lmaps/p/au;->a(ZZ)V

    :cond_8
    :goto_5
    return-void

    :cond_9
    move v0, v1

    goto/16 :goto_0

    :cond_a
    move v3, v1

    goto/16 :goto_1

    :cond_b
    move v3, v1

    goto :goto_2

    :cond_c
    move v3, v1

    goto :goto_3

    :cond_d
    move v2, v1

    goto :goto_4

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_e
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-ge v2, v3, :cond_f

    if-nez v0, :cond_f

    if-nez p1, :cond_f

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lmaps/af/v;->S:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x2710

    cmp-long v0, v4, v6

    if-lez v0, :cond_f

    invoke-static {}, Ljava/lang/System;->gc()V

    iput-wide v2, p0, Lmaps/af/v;->S:J

    :cond_f
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_8

    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0, v1, v1}, Lmaps/p/au;->a(ZZ)V

    goto :goto_5
.end method

.method private a(Lmaps/bq/d;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v0

    invoke-virtual {p1}, Lmaps/bq/d;->m()I

    move-result v1

    if-lez v0, :cond_1

    if-lez v1, :cond_1

    sget-boolean v2, Lmaps/ae/h;->F:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    invoke-virtual {p1}, Lmaps/bq/d;->B()[F

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/bq/d;->b([F)V

    :cond_0
    iget-object v2, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v2}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    const/16 v3, 0x1701

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-interface {v2, v4, v4, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    invoke-virtual {p1}, Lmaps/bq/d;->B()[F

    move-result-object v3

    invoke-interface {v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    sget-boolean v3, Lmaps/ae/h;->F:Z

    if-nez v3, :cond_1

    const/16 v3, 0xc11

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    invoke-interface {v2, v4, v4, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glScissor(IIII)V

    :cond_1
    return-void
.end method

.method private a(Lmaps/bq/d;IZZ)V
    .locals 11

    invoke-direct {p0}, Lmaps/af/v;->p()Lmaps/af/a;

    move-result-object v8

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Lmaps/af/a;->b()[Lmaps/y/ab;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lmaps/y/ab;->k()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->G()V

    iget-object v2, p0, Lmaps/af/v;->T:Lmaps/af/q;

    invoke-virtual {p1}, Lmaps/bq/d;->o()F

    move-result v0

    iget v1, p0, Lmaps/af/v;->R:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lmaps/af/v;->a(Lmaps/bq/d;)V

    invoke-virtual {p1}, Lmaps/bq/d;->o()F

    move-result v0

    iput v0, p0, Lmaps/af/v;->R:F

    :cond_1
    invoke-direct {p0, p1}, Lmaps/af/v;->b(Lmaps/bq/d;)V

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v8}, Lmaps/af/a;->a()[Lmaps/y/bc;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_2

    invoke-virtual {v8}, Lmaps/af/a;->a()[Lmaps/y/bc;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1, p2}, Lmaps/y/bc;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    sget-boolean v0, Lmaps/ae/h;->A:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_3

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/f;->d()V

    :cond_3
    if-eqz p3, :cond_9

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v8}, Lmaps/af/a;->a()[Lmaps/y/bc;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_4

    invoke-virtual {v8}, Lmaps/af/a;->a()[Lmaps/y/bc;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v3, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v1, p1, v3}, Lmaps/y/bc;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lmaps/af/v;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    sget-boolean v0, Lmaps/af/v;->a:Z

    if-eqz v0, :cond_8

    invoke-virtual {v8}, Lmaps/af/a;->b()[Lmaps/y/ab;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    :goto_3
    if-ge v3, v5, :cond_8

    aget-object v0, v4, v3

    invoke-virtual {v0}, Lmaps/y/ab;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/al;

    invoke-interface {v0}, Lmaps/l/al;->j()Lmaps/l/u;

    move-result-object v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v1, v0, p1}, Lmaps/ac/c;->a(Lmaps/l/al;Lmaps/bq/d;)Lmaps/l/u;

    move-result-object v1

    invoke-interface {v0, v1}, Lmaps/l/al;->a(Lmaps/l/u;)V

    :cond_6
    move-object v0, v1

    if-eqz v0, :cond_5

    iget-object v1, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0, p1, v1}, Lmaps/l/u;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    iget-object v1, p0, Lmaps/af/v;->J:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lmaps/af/v;->C:Lmaps/y/z;

    iget-object v1, p0, Lmaps/af/v;->J:Ljava/util/List;

    invoke-virtual {v0, v1}, Lmaps/y/z;->b(Ljava/util/List;)V

    :cond_9
    sget-boolean v0, Lmaps/ae/h;->A:Z

    if-eqz v0, :cond_a

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_a

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/f;->e()V

    :cond_a
    sget-boolean v0, Lmaps/ae/h;->A:Z

    if-eqz v0, :cond_b

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_b

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/f;->f()V

    :cond_b
    if-eqz p4, :cond_c

    sget-object v0, Lmaps/af/q;->f:Lmaps/af/q;

    if-eq v2, v0, :cond_c

    sget-object v0, Lmaps/af/q;->e:Lmaps/af/q;

    if-eq v2, v0, :cond_c

    invoke-virtual {v8}, Lmaps/af/a;->b()[Lmaps/y/ab;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lmaps/af/v;->a(Lmaps/bq/d;IZ[Lmaps/y/ab;)V

    :cond_c
    sget-boolean v0, Lmaps/ae/h;->A:Z

    if-eqz v0, :cond_d

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_d

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/f;->g()V

    :cond_d
    iget-object v0, p0, Lmaps/af/v;->F:Lmaps/u/b;

    if-eqz v0, :cond_f

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/af/v;->L:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/af/v;->L:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lmaps/af/v;->G:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lmaps/af/v;->H:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lmaps/af/v;->I:[I

    const/4 v1, 0x0

    const/4 v3, -0x1

    aput v3, v0, v1

    iget-object v0, p0, Lmaps/af/v;->I:[I

    const/4 v1, 0x0

    aget v6, v0, v1

    invoke-virtual {v8}, Lmaps/af/a;->b()[Lmaps/y/ab;

    move-result-object v9

    array-length v10, v9

    const/4 v0, 0x0

    move v7, v0

    :goto_5
    if-ge v7, v10, :cond_e

    aget-object v0, v9, v7

    iget-object v3, p0, Lmaps/af/v;->G:Ljava/util/HashSet;

    iget-object v4, p0, Lmaps/af/v;->H:Ljava/util/HashSet;

    iget-object v5, p0, Lmaps/af/v;->I:[I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lmaps/y/ab;->a(Lmaps/bq/d;Lmaps/af/q;Ljava/util/HashSet;Ljava/util/HashSet;[I)V

    iget-object v0, p0, Lmaps/af/v;->I:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-le v0, v6, :cond_1f

    iget-object v0, p0, Lmaps/af/v;->I:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    :goto_6
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v6, v0

    goto :goto_5

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_e
    iget-object v0, p0, Lmaps/af/v;->F:Lmaps/u/b;

    iget-object v1, p0, Lmaps/af/v;->G:Ljava/util/HashSet;

    iget-object v3, p0, Lmaps/af/v;->H:Ljava/util/HashSet;

    iget-object v4, p0, Lmaps/af/v;->T:Lmaps/af/q;

    invoke-virtual {v0, v1, v3, v6, v4}, Lmaps/u/b;->a(Ljava/util/HashSet;Ljava/util/HashSet;ILmaps/af/q;)V

    :cond_f
    if-nez p3, :cond_10

    if-eqz p2, :cond_16

    :cond_10
    const/4 v0, 0x1

    :goto_7
    invoke-virtual {p0, v8, v0}, Lmaps/af/v;->a(Lmaps/af/a;Z)V

    new-instance v3, Lmaps/af/t;

    const/4 v0, 0x0

    invoke-direct {v3, v2, v0}, Lmaps/af/t;-><init>(Lmaps/af/q;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/af/v;->an:Z

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_17

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_11

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/s/a;

    invoke-virtual {v0}, Lmaps/s/a;->a()V

    :cond_11
    iget-object v0, p0, Lmaps/af/v;->ao:Lmaps/q/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/q/g;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_12

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->E()V

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->t()V

    :cond_12
    :goto_8
    iget-object v0, p0, Lmaps/af/v;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/av;

    invoke-virtual {v3, v0}, Lmaps/af/t;->a(Lmaps/p/av;)V

    invoke-virtual {v0}, Lmaps/p/av;->a()Lmaps/y/bc;

    move-result-object v1

    iget-object v0, p0, Lmaps/af/v;->W:Lmaps/af/e;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lmaps/af/v;->W:Lmaps/af/e;

    invoke-virtual {v0, v1}, Lmaps/af/e;->a(Lmaps/y/bc;)V

    :cond_14
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_18

    :goto_a
    instance-of v0, v1, Lmaps/y/ab;

    if-eqz v0, :cond_15

    iget-boolean v0, p0, Lmaps/af/v;->an:Z

    if-eqz v0, :cond_19

    move-object v0, v1

    check-cast v0, Lmaps/y/ab;

    invoke-virtual {v0}, Lmaps/y/ab;->q()Z

    move-result v0

    if-eqz v0, :cond_19

    const/4 v0, 0x1

    :goto_b
    iput-boolean v0, p0, Lmaps/af/v;->an:Z

    :cond_15
    iget-object v0, p0, Lmaps/af/v;->W:Lmaps/af/e;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lmaps/af/v;->W:Lmaps/af/e;

    invoke-virtual {v0, v1}, Lmaps/af/e;->b(Lmaps/y/bc;)V

    goto :goto_9

    :cond_16
    const/4 v0, 0x0

    goto :goto_7

    :cond_17
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->o()V

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->t()V

    goto :goto_8

    :cond_18
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->D()V

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v1, v0, p1, v3}, Lmaps/y/bc;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->F()V

    goto :goto_a

    :cond_19
    const/4 v0, 0x0

    goto :goto_b

    :cond_1a
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->G()V

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetError()I

    move-result v1

    if-eqz v1, :cond_1c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x505

    if-ne v1, v2, :cond_1b

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lmaps/af/v;->aa:J

    sub-long/2addr v2, v4

    const-string v4, "\nTime in current GL context: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lmaps/ca/a;->a()Lmaps/ca/a;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ca/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lmaps/af/v;->Z:Z

    :cond_1b
    const-string v2, "Renderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GL Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Landroid/opengl/GLU;->gluErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lmaps/bt/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Renderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "drawFrameInternal GL ERROR: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1c
    iget-boolean v0, p0, Lmaps/af/v;->Z:Z

    if-eqz v0, :cond_1d

    sget-boolean v0, Lmaps/ae/h;->Q:Z

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lmaps/j/d;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/j/d;

    invoke-virtual {v0}, Lmaps/j/d;->a()V

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_1d

    const-string v0, "Renderer"

    invoke-static {}, Lmaps/ca/a;->a()Lmaps/ca/a;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ca/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1d
    const/16 v0, 0x505

    if-ne v1, v0, :cond_1e

    invoke-virtual {v8}, Lmaps/af/a;->a()[Lmaps/y/bc;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_c
    if-ge v0, v2, :cond_1e

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lmaps/y/bc;->e_()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_1e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/af/v;->Z:Z

    return-void

    :cond_1f
    move v0, v6

    goto/16 :goto_6
.end method

.method private a(Lmaps/bq/d;IZ[Lmaps/y/ab;)V
    .locals 11

    iget-object v0, p0, Lmaps/af/v;->W:Lmaps/af/e;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/af/v;->W:Lmaps/af/e;

    iget-object v2, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    if-nez p3, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lmaps/af/e;->a(Lmaps/ac/c;Z)V

    :cond_0
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v1, p2}, Lmaps/ac/c;->a(I)V

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {p1}, Lmaps/bq/d;->D()Lmaps/t/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ac/c;->a(Lmaps/t/bf;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/af/v;->Q:Z

    :cond_1
    :goto_2
    iget-object v0, p0, Lmaps/af/v;->W:Lmaps/af/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/af/v;->W:Lmaps/af/e;

    iget-object v1, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v0, v1}, Lmaps/af/e;->a(Lmaps/ac/c;)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    if-nez p3, :cond_6

    iget-boolean v0, p0, Lmaps/af/v;->Q:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lmaps/ac/c;->b(I)V

    iget-object v0, p0, Lmaps/af/v;->z:Lmaps/ac/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/af/v;->z:Lmaps/ac/a;

    iget-object v1, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-interface {v0, v1}, Lmaps/ac/a;->a(Lmaps/cr/c;)V

    goto :goto_2

    :cond_6
    new-instance v5, Lmaps/p/ac;

    invoke-direct {v5}, Lmaps/p/ac;-><init>()V

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, Lmaps/bq/d;->D()Lmaps/t/u;

    move-result-object v2

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_7

    invoke-virtual {p1}, Lmaps/bq/d;->q()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_4
    const/4 v3, 0x0

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v7

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v8

    array-length v4, p4

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v4, :cond_a

    aget-object v9, p4, v1

    invoke-virtual {v9}, Lmaps/y/ab;->m()Z

    move-result v10

    if-nez v10, :cond_9

    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    :cond_8
    move-object v0, v2

    goto :goto_4

    :cond_9
    invoke-virtual {v9, v0, v5, v6}, Lmaps/y/ab;->a(Lmaps/t/u;Lmaps/p/ac;Ljava/util/Set;)I

    move-result v10

    invoke-static {v3, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v9, v7, v8}, Lmaps/y/ab;->a(Ljava/util/Set;Ljava/util/Map;)V

    goto :goto_6

    :cond_a
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/af/v;->N:Ljava/util/List;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lmaps/af/v;->N:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_7
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    const/16 v9, 0x14

    iget-object v1, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    invoke-virtual {v1}, Lmaps/y/ab;->o()Lmaps/o/c;

    move-result-object v10

    move-object v1, p1

    invoke-virtual/range {v0 .. v10}, Lmaps/ac/c;->a(Lmaps/bq/d;Lmaps/t/bf;ILjava/util/Iterator;Lmaps/p/ac;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;ILmaps/o/c;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/af/v;->Q:Z

    iget-object v0, p0, Lmaps/af/v;->z:Lmaps/ac/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/af/v;->z:Lmaps/ac/a;

    iget-object v1, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-interface {v0, v1}, Lmaps/ac/a;->a(Lmaps/cr/c;)V

    goto/16 :goto_2

    :cond_b
    const/4 v4, 0x0

    goto :goto_7

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Lmaps/cr/c;)V
    .locals 3

    iget-object v1, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/bc;

    invoke-virtual {v0, p1}, Lmaps/y/bc;->a_(Lmaps/cr/c;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lmaps/af/v;->ak:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic a(Lmaps/af/v;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmaps/af/v;->L:Z

    return p1
.end method

.method public static a(Lmaps/af/q;)[I
    .locals 2

    sget-object v0, Lmaps/af/m;->a:[I

    invoke-virtual {p0}, Lmaps/af/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lmaps/af/v;->i:[I

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lmaps/af/v;->f:[I

    goto :goto_0

    :pswitch_1
    sget-object v0, Lmaps/af/v;->f:[I

    goto :goto_0

    :pswitch_2
    sget-object v0, Lmaps/af/v;->f:[I

    goto :goto_0

    :pswitch_3
    sget-object v0, Lmaps/af/v;->g:[I

    goto :goto_0

    :pswitch_4
    sget-object v0, Lmaps/af/v;->h:[I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private b(Lmaps/bq/d;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    invoke-virtual {p1}, Lmaps/bq/d;->A()[F

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/bq/d;->a([F)V

    :cond_0
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, Lmaps/bq/d;->A()[F

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    return-void
.end method

.method private c(Lmaps/af/q;)V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-static {p1}, Lmaps/af/v;->a(Lmaps/af/q;)[I

    move-result-object v0

    aget v2, v0, v6

    const/4 v3, 0x1

    aget v3, v0, v3

    const/4 v4, 0x2

    aget v4, v0, v4

    const/4 v5, 0x3

    aget v0, v0, v5

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glClearColorx(IIII)V

    const/16 v0, 0x4000

    iget-object v2, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v2}, Lmaps/cr/c;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x4100

    iget-object v2, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v2}, Lmaps/cr/c;->i()V

    :cond_0
    iget-object v2, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v2}, Lmaps/cr/c;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1, v6}, Ljavax/microedition/khronos/opengles/GL10;->glClearStencil(I)V

    or-int/lit16 v0, v0, 0x400

    iget-object v2, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v2}, Lmaps/cr/c;->k()V

    :cond_1
    invoke-interface {v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V

    return-void
.end method

.method private c(Lmaps/y/bc;)V
    .locals 5

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p1, Lmaps/y/ab;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lmaps/y/ab;

    iget-object v1, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/y/bc;

    instance-of v3, v1, Lmaps/y/ab;

    if-eqz v3, :cond_0

    check-cast v1, Lmaps/y/ab;

    invoke-virtual {v1}, Lmaps/y/ab;->s()Lmaps/ca/e;

    move-result-object v3

    invoke-virtual {v0}, Lmaps/y/ab;->s()Lmaps/ca/e;

    move-result-object v4

    if-ne v3, v4, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The added tile Overlay "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lmaps/y/ab;->o()Lmaps/o/c;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " shares the same GLTileCache with "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lmaps/y/ab;->o()Lmaps/o/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {p1, v0}, Lmaps/y/bc;->b(Lmaps/cr/c;)V

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/af/v;->e:Z

    iget-object v0, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private declared-synchronized e(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/af/v;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/af/p;

    invoke-interface {v0, p1}, Lmaps/af/p;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method private m()V
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v0}, Lmaps/ac/c;->a()V

    :cond_0
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->K()Lmaps/s/l;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/s/l;->a()V

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/s/i;->a()V

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->G()V

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->H()V

    :cond_1
    return-void
.end method

.method private n()V
    .locals 4

    iget v0, p0, Lmaps/af/v;->n:I

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lmaps/af/v;->ad:Z

    if-eqz v0, :cond_2

    const/16 v0, 0xa

    :goto_1
    :try_start_0
    iget v1, p0, Lmaps/af/v;->n:I

    invoke-static {v1, v0}, Landroid/os/Process;->setThreadPriority(II)V

    sget-boolean v1, Lmaps/ae/h;->j:Z

    if-eqz v1, :cond_0

    const-string v1, "Renderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Renderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lmaps/af/v;->o:I

    goto :goto_1
.end method

.method private o()Landroid/graphics/Bitmap;
    .locals 14

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v2, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    invoke-virtual {v2}, Lmaps/bq/d;->l()I

    move-result v3

    iget-object v2, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    invoke-virtual {v2}, Lmaps/bq/d;->m()I

    move-result v4

    iget-object v2, p0, Lmaps/af/v;->v:Lmaps/s/h;

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v3, v4, v5}, Lmaps/s/h;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-static {v13}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    invoke-static {v13}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    mul-int v2, v3, v4

    invoke-static {v2}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v7

    move v2, v1

    invoke-interface/range {v0 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    invoke-virtual {v7}, Ljava/nio/IntBuffer;->array()[I

    move-result-object v6

    move-object v5, v13

    move v7, v1

    move v8, v3

    move v9, v1

    move v10, v1

    move v11, v3

    move v12, v4

    invoke-virtual/range {v5 .. v12}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    return-object v13
.end method

.method private p()Lmaps/af/a;
    .locals 9

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    iget-object v7, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    monitor-enter v7

    :try_start_0
    iget-boolean v0, p0, Lmaps/af/v;->ak:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/bc;

    iget-object v4, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    iget-object v6, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0, v4, v6}, Lmaps/y/bc;->a(Lmaps/cr/c;Lmaps/af/f;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lmaps/af/v;->ak:Z

    :cond_1
    move v6, v1

    move v2, v1

    move v4, v1

    :goto_1
    iget-object v0, p0, Lmaps/af/v;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_7

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/af/v;->e:Z

    iget-object v0, p0, Lmaps/af/v;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/af/r;

    sget-object v1, Lmaps/af/m;->b:[I

    iget-object v8, v0, Lmaps/af/r;->b:Lmaps/af/c;

    invoke-virtual {v8}, Lmaps/af/c;->ordinal()I

    move-result v8

    aget v1, v1, v8

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_2
    move v0, v2

    move v1, v4

    :goto_3
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v4, v1

    move v2, v0

    goto :goto_1

    :pswitch_0
    iget-object v1, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    iget-object v8, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lmaps/y/bc;->a_(Lmaps/cr/c;)V

    iget-object v1, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    instance-of v1, v1, Lmaps/y/ab;

    if-eqz v1, :cond_e

    iget-object v1, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    check-cast v1, Lmaps/y/ab;

    iget-object v4, p0, Lmaps/af/v;->M:Lmaps/y/h;

    invoke-virtual {v1, v4}, Lmaps/y/ab;->a(Lmaps/y/h;)V

    iget-object v4, p0, Lmaps/af/v;->r:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lmaps/y/ab;->b()Lmaps/y/am;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/y/am;->a()I

    move-result v4

    iget v8, p0, Lmaps/af/v;->al:I

    if-ge v4, v8, :cond_3

    move v4, v3

    :goto_4
    invoke-virtual {v1}, Lmaps/y/ab;->n()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lmaps/af/v;->s:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/o/c;

    invoke-virtual {v1, v2}, Lmaps/y/ab;->a(Lmaps/o/c;)V

    goto :goto_5

    :cond_3
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lmaps/y/ab;->b(Z)V

    move v4, v2

    goto :goto_4

    :cond_4
    move v1, v4

    :goto_6
    iget-object v2, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    invoke-direct {p0, v2}, Lmaps/af/v;->c(Lmaps/y/bc;)V

    iget-object v2, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    invoke-virtual {v2}, Lmaps/y/bc;->g()Lmaps/af/n;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v2}, Lmaps/af/v;->a(Lmaps/af/n;)V

    :cond_5
    iget-object v0, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    iget-object v2, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    iget-object v4, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0, v2, v4}, Lmaps/y/bc;->a(Lmaps/cr/c;Lmaps/af/f;)V

    move v0, v1

    move v1, v3

    goto :goto_3

    :pswitch_1
    iget-object v1, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    iget-object v8, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    instance-of v1, v1, Lmaps/y/ab;

    if-eqz v1, :cond_d

    iget-object v1, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    invoke-virtual {v1}, Lmaps/y/bc;->b()Lmaps/y/am;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/y/am;->a()I

    move-result v1

    iget v8, p0, Lmaps/af/v;->al:I

    if-ne v1, v8, :cond_c

    move v1, v3

    :goto_7
    iget-object v2, p0, Lmaps/af/v;->r:Ljava/util/ArrayList;

    iget-object v8, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :goto_8
    iget-object v2, p0, Lmaps/af/v;->c:Ljava/util/Map;

    iget-object v8, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    invoke-interface {v2, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    iget-object v8, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v2, v8}, Lmaps/y/bc;->a_(Lmaps/cr/c;)V

    iget-object v0, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    invoke-virtual {v0}, Lmaps/y/bc;->g()Lmaps/af/n;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0, v0}, Lmaps/af/v;->b(Lmaps/af/n;)V

    :cond_6
    move v0, v1

    move v1, v4

    goto/16 :goto_3

    :pswitch_2
    iget-object v0, v0, Lmaps/af/r;->a:Lmaps/y/bc;

    check-cast v0, Lmaps/y/ab;

    iput-object v0, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lmaps/af/v;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz v2, :cond_9

    const v0, 0x7fffffff

    iput v0, p0, Lmaps/af/v;->al:I

    iget-object v0, p0, Lmaps/af/v;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v5

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/ab;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lmaps/y/ab;->b(Z)V

    invoke-virtual {v0}, Lmaps/y/ab;->b()Lmaps/y/am;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/y/am;->a()I

    move-result v5

    iget v6, p0, Lmaps/af/v;->al:I

    if-ge v5, v6, :cond_b

    invoke-virtual {v0}, Lmaps/y/ab;->b()Lmaps/y/am;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/y/am;->a()I

    move-result v1

    iput v1, p0, Lmaps/af/v;->al:I

    :goto_a
    move-object v1, v0

    goto :goto_9

    :cond_8
    if-eqz v1, :cond_9

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lmaps/y/ab;->b(Z)V

    :cond_9
    if-eqz v4, :cond_a

    iget-object v0, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v3, :cond_a

    iget-object v0, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    sget-object v1, Lmaps/af/v;->l:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_a
    new-instance v0, Lmaps/af/a;

    iget-object v1, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    iget-object v2, p0, Lmaps/af/v;->r:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lmaps/af/a;-><init>(Ljava/util/List;Ljava/util/List;)V

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    :cond_b
    move-object v0, v1

    goto :goto_a

    :cond_c
    move v1, v2

    goto/16 :goto_7

    :cond_d
    move v1, v2

    goto/16 :goto_8

    :cond_e
    move v1, v2

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private q()V
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lmaps/af/v;->ab:I

    const/4 v2, 0x0

    iput v2, p0, Lmaps/af/v;->ab:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_4

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    const-string v2, "Renderer"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLowMemory"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v1, :cond_2

    const-string v0, " critical"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v0, v1}, Lmaps/ac/c;->a(Z)V

    iget-object v2, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/bc;

    invoke-virtual {v0, v1}, Lmaps/y/bc;->a(Z)V

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1

    :cond_3
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_4
    return-void
.end method


# virtual methods
.method public a(Lmaps/t/bx;)F
    .locals 4

    const/high16 v0, 0x41a80000

    iget-object v2, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lmaps/af/v;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/ab;

    invoke-virtual {v0, p1}, Lmaps/y/ab;->a(Lmaps/t/bx;)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_0
    monitor-exit v2

    return v1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lmaps/y/am;)Lmaps/y/k;
    .locals 2

    new-instance v0, Lmaps/y/k;

    iget-object v1, p0, Lmaps/af/v;->D:Lmaps/y/f;

    invoke-direct {v0, p1, v1}, Lmaps/y/k;-><init>(Lmaps/y/am;Lmaps/y/f;)V

    return-object v0
.end method

.method public a()V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->Q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-direct {p0, v0}, Lmaps/af/v;->a(Lmaps/cr/c;)V

    invoke-direct {p0}, Lmaps/af/v;->m()V

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lmaps/j/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Lmaps/j/d;

    invoke-virtual {v0}, Lmaps/j/d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GL memory leaked!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 7

    const-wide/16 v3, -0x1

    const/4 v2, 0x1

    const/4 v6, 0x0

    sget-boolean v0, Lmaps/ae/h;->A:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/f;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lmaps/s/a;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GL object has changed since onSurfaceCreated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const-string v1, "DA:Renderer"

    invoke-static {v1, v0}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "OpenGL error during initialization."

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/k/b;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/cr/c;->a(Z)V

    iget-object v0, p0, Lmaps/af/v;->T:Lmaps/af/q;

    invoke-direct {p0, v0}, Lmaps/af/v;->c(Lmaps/af/q;)V

    iget v0, p0, Lmaps/af/v;->aj:I

    if-lez v0, :cond_3

    iget v0, p0, Lmaps/af/v;->aj:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lmaps/af/v;->aj:I

    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0, v2, v2}, Lmaps/p/au;->a(ZZ)V

    sget-boolean v0, Lmaps/ae/h;->A:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_2

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/f;->c()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-boolean v0, p0, Lmaps/af/v;->af:Z

    if-eqz v0, :cond_5

    iget-object v1, p0, Lmaps/af/v;->ah:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/af/v;->af:Z

    iget-object v0, p0, Lmaps/af/v;->ah:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    :goto_1
    iget-boolean v0, p0, Lmaps/af/v;->ag:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_4

    :try_start_1
    iget-object v0, p0, Lmaps/af/v;->ah:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_4
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_5
    iget-wide v0, p0, Lmaps/af/v;->ar:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_7

    iget-object v1, p0, Lmaps/af/v;->as:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-wide v2, p0, Lmaps/af/v;->ar:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_6

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lmaps/af/v;->ar:J

    :cond_6
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0}, Lmaps/p/au;->e()V

    :cond_7
    invoke-static {}, Lmaps/be/b;->d()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/af/v;->am:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-lez v2, :cond_9

    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Skip onDrawFrame because parameters have not been fetched."

    invoke-static {v2, v3}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iput-wide v0, p0, Lmaps/af/v;->am:J

    :cond_9
    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0, v6, v6}, Lmaps/p/au;->a(ZZ)V

    sget-boolean v0, Lmaps/ae/h;->A:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_2

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/f;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :cond_a
    invoke-virtual {p0}, Lmaps/af/v;->g()I

    move-result v0

    invoke-direct {p0, v0}, Lmaps/af/v;->a(I)V

    sget-boolean v0, Lmaps/ae/h;->A:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_2

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/f;->c()V

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/f;->h()Lmaps/p/a;

    move-result-object v0

    iget-object v1, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    iget-object v2, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmaps/p/a;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto/16 :goto_0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lmaps/s/a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GL object has changed since onSurfaceCreated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const-string v1, "DA:Renderer"

    invoke-static {v1, v0}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "OpenGL error during initialization."

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    if-lez p2, :cond_1

    if-gtz p3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    iget v1, p0, Lmaps/af/v;->x:F

    invoke-virtual {v0, p2, p3, v1}, Lmaps/bq/d;->a(IIF)V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/af/v;->ao:Lmaps/q/g;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/q/g;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    :cond_3
    iget-object v0, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    invoke-direct {p0, v0}, Lmaps/af/v;->a(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    invoke-virtual {v0}, Lmaps/bq/d;->o()F

    move-result v0

    iput v0, p0, Lmaps/af/v;->R:F

    iget v0, p0, Lmaps/af/v;->x:F

    invoke-static {p2, p3, v0}, Lmaps/af/v;->a(IIF)F

    move-result v0

    iput v0, p0, Lmaps/af/v;->ae:F

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_4

    const-string v0, "Renderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Surface changed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/p/au;->a(ZZ)V

    goto :goto_0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 7

    const/4 v1, 0x0

    const/4 v6, 0x1

    const-string v0, "Renderer.onSurfaceCreated"

    invoke-static {v0}, Lmaps/ah/a;->a(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    iput v0, p0, Lmaps/af/v;->n:I

    invoke-direct {p0}, Lmaps/af/v;->n()V

    const/16 v0, 0x1f01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmaps/bm/b;->a(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lmaps/af/v;->a(Lmaps/cr/c;)V

    sget-boolean v0, Lmaps/ae/h;->A:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->C:Z

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/p/f;->a()Lmaps/p/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/f;->h()Lmaps/p/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/a;->a()V

    :cond_0
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_1

    invoke-direct {p0}, Lmaps/af/v;->m()V

    iput-object v1, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    :cond_1
    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    if-nez v0, :cond_6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/af/v;->aa:J

    new-instance v0, Lmaps/cr/c;

    sget-boolean v1, Lmaps/ae/h;->t:Z

    if-eqz v1, :cond_4

    new-instance v1, Lmaps/s/a;

    invoke-direct {v1}, Lmaps/s/a;-><init>()V

    :goto_0
    iget-object v2, p0, Lmaps/af/v;->v:Lmaps/s/h;

    iget-object v3, p0, Lmaps/af/v;->u:Lmaps/p/au;

    iget-object v4, p0, Lmaps/af/v;->ap:Lmaps/q/ad;

    iget-object v5, p0, Lmaps/af/v;->w:Landroid/content/res/Resources;

    invoke-direct/range {v0 .. v5}, Lmaps/cr/c;-><init>(Ljavax/microedition/khronos/opengles/GL10;Lmaps/s/h;Lmaps/p/au;Lmaps/q/ad;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->C()Lmaps/p/au;

    move-result-object v0

    iget-boolean v1, p0, Lmaps/af/v;->ad:Z

    invoke-virtual {v0, v1}, Lmaps/p/au;->b(Z)V

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->J()I

    move-result v0

    invoke-static {v0}, Lmaps/l/ar;->a(I)V

    new-instance v0, Lmaps/ac/c;

    iget-object v1, p0, Lmaps/af/v;->j:Lmaps/cf/a;

    iget-object v2, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    iget-object v3, p0, Lmaps/af/v;->w:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2, v3}, Lmaps/ac/c;-><init>(Lmaps/cf/a;Lmaps/cr/c;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    iget-object v0, p0, Lmaps/af/v;->aq:Lmaps/cm/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    iget-object v1, p0, Lmaps/af/v;->aq:Lmaps/cm/f;

    invoke-virtual {v0, v1}, Lmaps/ac/c;->a(Lmaps/cm/f;)V

    :cond_2
    iget-object v0, p0, Lmaps/af/v;->z:Lmaps/ac/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/af/v;->z:Lmaps/ac/a;

    iget-object v1, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-interface {v0, v1}, Lmaps/ac/a;->a(Lmaps/ac/c;)V

    :cond_3
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/bc;

    iget-object v2, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0, v2}, Lmaps/y/bc;->b(Lmaps/cr/c;)V

    goto :goto_1

    :cond_4
    move-object v1, p1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lmaps/af/v;->A:Lmaps/y/an;

    iget-object v1, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v0, v1}, Lmaps/y/an;->a(Lmaps/ac/c;)V

    iget-object v0, p0, Lmaps/af/v;->B:Lmaps/y/an;

    iget-object v1, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v0, v1}, Lmaps/y/an;->a(Lmaps/ac/c;)V

    :cond_6
    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0, v6}, Lmaps/p/au;->c(Z)V

    iput-boolean v6, p0, Lmaps/af/v;->Q:Z

    iget-boolean v0, p0, Lmaps/af/v;->ai:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x2

    iput v0, p0, Lmaps/af/v;->aj:I

    :cond_7
    iput-boolean v6, p0, Lmaps/af/v;->ai:Z

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_8

    const-string v0, "Renderer"

    const-string v1, "Surface created"

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    const-string v0, "Renderer.onSurfaceCreated"

    invoke-static {v0}, Lmaps/ah/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method a(Lmaps/af/a;Z)V
    .locals 9

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget-object v5, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    monitor-enter v5

    :try_start_0
    iget-boolean v0, p0, Lmaps/af/v;->e:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    monitor-exit v5

    :goto_0
    return-void

    :cond_0
    iget-boolean v2, p0, Lmaps/af/v;->e:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/af/v;->e:Z

    invoke-virtual {p1}, Lmaps/af/a;->a()[Lmaps/y/bc;

    move-result-object v6

    array-length v7, v6

    move v4, v1

    :goto_1
    if-ge v4, v7, :cond_2

    aget-object v8, v6, v4

    iget-object v0, p0, Lmaps/af/v;->c:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_1

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lmaps/af/v;->c:Ljava/util/Map;

    invoke-interface {v2, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    :cond_1
    invoke-virtual {v8, v0}, Lmaps/y/bc;->a(Ljava/util/List;)Z

    move-result v0

    or-int/2addr v2, v0

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_8

    iget-object v0, p0, Lmaps/af/v;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p1}, Lmaps/af/a;->a()[Lmaps/y/bc;

    move-result-object v2

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v0, v2, v1

    iget-object v4, p0, Lmaps/af/v;->c:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v4, p0, Lmaps/af/v;->d:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lmaps/af/v;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v1

    iget-object v0, p0, Lmaps/af/v;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/av;

    invoke-virtual {v0}, Lmaps/p/av;->a()Lmaps/y/bc;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lmaps/p/av;->a(Z)V

    invoke-virtual {v0}, Lmaps/p/av;->a()Lmaps/y/bc;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Lmaps/p/av;->a(Z)V

    goto :goto_3

    :cond_6
    invoke-interface {v1}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmaps/af/v;->d:Ljava/util/List;

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/p/av;

    invoke-virtual {v0}, Lmaps/p/av;->a()Lmaps/y/bc;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lmaps/p/av;->b(Z)V

    invoke-virtual {v0}, Lmaps/p/av;->a()Lmaps/y/bc;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lmaps/p/av;->b(Z)V

    goto :goto_4

    :cond_8
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public a(Lmaps/af/e;)V
    .locals 0

    iput-object p1, p0, Lmaps/af/v;->X:Lmaps/af/e;

    return-void
.end method

.method public a(Lmaps/af/n;)V
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->U:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lmaps/cf/a;)V
    .locals 3

    iget-object v0, p0, Lmaps/af/v;->j:Lmaps/cf/a;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lmaps/af/v;->j:Lmaps/cf/a;

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v0, p1}, Lmaps/ac/c;->a(Lmaps/cf/a;)V

    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/p/au;->a(ZZ)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/y/ab;)V
    .locals 4

    iget-object v1, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmaps/af/r;

    sget-object v2, Lmaps/af/c;->a:Lmaps/af/c;

    invoke-direct {v0, v2, p1}, Lmaps/af/r;-><init>(Lmaps/af/c;Lmaps/y/bc;)V

    iget-object v2, p0, Lmaps/af/v;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmaps/af/r;

    sget-object v2, Lmaps/af/c;->c:Lmaps/af/c;

    invoke-direct {v0, v2, p1}, Lmaps/af/r;-><init>(Lmaps/af/c;Lmaps/y/bc;)V

    iget-object v2, p0, Lmaps/af/v;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmaps/af/r;

    sget-object v2, Lmaps/af/c;->b:Lmaps/af/c;

    iget-object v3, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    invoke-direct {v0, v2, v3}, Lmaps/af/r;-><init>(Lmaps/af/c;Lmaps/y/bc;)V

    iget-object v2, p0, Lmaps/af/v;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v0}, Lmaps/ac/c;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/p/au;->a(ZZ)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lmaps/y/bc;)V
    .locals 3

    iget-object v1, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmaps/af/r;

    sget-object v2, Lmaps/af/c;->a:Lmaps/af/c;

    invoke-direct {v0, v2, p1}, Lmaps/af/r;-><init>(Lmaps/af/c;Lmaps/y/bc;)V

    iget-object v2, p0, Lmaps/af/v;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/p/au;->a(ZZ)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lmaps/y/bh;Lmaps/y/ah;)V
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->D:Lmaps/y/f;

    invoke-virtual {v0, p1, p2}, Lmaps/y/f;->a(Lmaps/y/bh;Lmaps/y/ah;)V

    return-void
.end method

.method public declared-synchronized a(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/af/v;->ad:Z

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->C()Lmaps/p/au;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/p/au;->b(Z)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0}, Lmaps/cr/c;->C()Lmaps/p/au;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/p/au;->c()V

    :cond_0
    invoke-direct {p0}, Lmaps/af/v;->n()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lmaps/af/v;->ae:F

    return v0
.end method

.method public b(Z)Lmaps/y/x;
    .locals 3

    new-instance v0, Lmaps/y/x;

    iget-object v1, p0, Lmaps/af/v;->w:Landroid/content/res/Resources;

    iget-object v2, p0, Lmaps/af/v;->D:Lmaps/y/f;

    invoke-direct {v0, v1, v2, p1}, Lmaps/y/x;-><init>(Landroid/content/res/Resources;Lmaps/y/f;Z)V

    return-object v0
.end method

.method public b(Lmaps/af/n;)V
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->U:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Lmaps/af/q;)V
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->T:Lmaps/af/q;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lmaps/af/v;->T:Lmaps/af/q;

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/af/v;->L:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/af/v;->e()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lmaps/y/bc;)V
    .locals 3

    iget-object v1, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmaps/af/r;

    sget-object v2, Lmaps/af/c;->b:Lmaps/af/c;

    invoke-direct {v0, v2, p1}, Lmaps/af/r;-><init>(Lmaps/af/c;Lmaps/y/bc;)V

    iget-object v2, p0, Lmaps/af/v;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/p/au;->a(ZZ)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 2

    invoke-static {}, Lmaps/ca/a;->a()Lmaps/ca/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    invoke-virtual {v0, v1}, Lmaps/ca/a;->a(Lmaps/cr/c;)V

    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 2

    const/4 v1, 0x0

    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    :try_start_0
    iput v0, p0, Lmaps/af/v;->ab:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0, v1, v1}, Lmaps/p/au;->a(ZZ)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public d()V
    .locals 6

    iget-object v1, p0, Lmaps/af/v;->as:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lmaps/af/v;->ar:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x7d0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lmaps/af/v;->ar:J

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/af/v;->F:Lmaps/u/b;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/af/v;->E:Lmaps/u/a;

    iget-object v1, p0, Lmaps/af/v;->F:Lmaps/u/b;

    invoke-virtual {v0, v1}, Lmaps/u/a;->c(Lmaps/u/c;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->E:Lmaps/u/a;

    iget-object v1, p0, Lmaps/af/v;->F:Lmaps/u/b;

    invoke-virtual {v0, v1}, Lmaps/u/a;->a(Lmaps/u/c;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/af/v;->E:Lmaps/u/a;

    iget-object v1, p0, Lmaps/af/v;->F:Lmaps/u/b;

    invoke-virtual {v0, v1}, Lmaps/u/a;->c(Lmaps/u/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->E:Lmaps/u/a;

    iget-object v1, p0, Lmaps/af/v;->F:Lmaps/u/b;

    invoke-virtual {v0, v1}, Lmaps/u/a;->b(Lmaps/u/c;)V

    goto :goto_0
.end method

.method public e()V
    .locals 3

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/af/v;->y:Lmaps/ac/c;

    invoke-virtual {v0}, Lmaps/ac/c;->b()V

    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/p/au;->a(ZZ)V

    :cond_0
    return-void
.end method

.method public f()Ljava/util/ArrayList;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/af/v;->p:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public g()I
    .locals 8

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lmaps/af/v;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v2

    move v3, v4

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/af/n;

    iget-object v6, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    invoke-interface {v0, v6}, Lmaps/af/n;->b(Lmaps/bq/d;)I

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Lmaps/af/n;->i()Lmaps/bq/a;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-interface {v0}, Lmaps/af/n;->i()Lmaps/bq/a;

    move-result-object v0

    :goto_1
    or-int v1, v3, v6

    move v3, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    if-eqz v3, :cond_3

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/af/v;->k:Lmaps/bq/a;

    invoke-virtual {v1, v0}, Lmaps/bq/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    invoke-virtual {v0, v1}, Lmaps/y/ab;->a(Lmaps/bq/a;)V

    iput-object v1, p0, Lmaps/af/v;->k:Lmaps/bq/a;

    :cond_1
    iget-object v0, p0, Lmaps/af/v;->u:Lmaps/p/au;

    invoke-virtual {v0, v4, v4}, Lmaps/p/au;->a(ZZ)V

    :goto_2
    iget-object v0, p0, Lmaps/af/v;->t:Lmaps/bq/d;

    if-eqz v3, :cond_2

    const/4 v4, 0x1

    :cond_2
    invoke-virtual {v0, v4}, Lmaps/bq/d;->a(Z)V

    return v3

    :cond_3
    iget-object v0, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    invoke-virtual {v0, v2}, Lmaps/y/ab;->a(Lmaps/bq/a;)V

    iput-object v2, p0, Lmaps/af/v;->k:Lmaps/bq/a;

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->D:Lmaps/y/f;

    invoke-virtual {v0}, Lmaps/y/f;->e()V

    return-void
.end method

.method public i()Lmaps/y/f;
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->D:Lmaps/y/f;

    return-object v0
.end method

.method public j()Lmaps/u/a;
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->E:Lmaps/u/a;

    return-object v0
.end method

.method public k()Lmaps/y/ab;
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->ac:Lmaps/y/ab;

    return-object v0
.end method

.method public l()Lmaps/cr/c;
    .locals 1

    iget-object v0, p0, Lmaps/af/v;->m:Lmaps/cr/c;

    return-object v0
.end method
