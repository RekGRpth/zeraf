.class public Lmaps/u/a;
.super Lmaps/y/bc;


# instance fields
.field private final a:Ljava/util/Vector;

.field private b:Lmaps/u/c;

.field private c:Ljava/lang/ref/SoftReference;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    return-void
.end method

.method private a(Lmaps/cr/c;Lmaps/bq/d;)V
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    const/16 v2, 0x1701

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p2}, Lmaps/bq/d;->l()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Lmaps/bq/d;->m()I

    move-result v3

    int-to-float v4, v3

    const/high16 v5, -0x40800000

    const/high16 v6, 0x3f800000

    move v3, v1

    invoke-interface/range {v0 .. v6}, Ljavax/microedition/khronos/opengles/GL10;->glOrthof(FFFFFF)V

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    return-void
.end method

.method private d(Lmaps/cr/c;)V
    .locals 2

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1701

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method


# virtual methods
.method public a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 2

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0, p1, p2}, Lmaps/u/c;->a(Lmaps/cr/c;Lmaps/af/f;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 4

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0}, Lmaps/u/c;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, p1, p2, p3}, Lmaps/u/c;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    invoke-direct {p0, p1, p2}, Lmaps/u/a;->a(Lmaps/cr/c;Lmaps/bq/d;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0}, Lmaps/u/c;->e()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0, p1, p2, p3}, Lmaps/u/c;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_3
    invoke-direct {p0, p1}, Lmaps/u/a;->d(Lmaps/cr/c;)V

    throw v0

    :cond_4
    invoke-direct {p0, p1}, Lmaps/u/a;->d(Lmaps/cr/c;)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public a(Lmaps/u/c;)V
    .locals 2

    iget-object v1, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p1, p0}, Lmaps/u/c;->a(Lmaps/u/a;)V

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/u/a;->c:Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/u/a;->c:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/c;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lmaps/u/c;->b(Lmaps/cr/c;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/u/a;->d()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(FFLmaps/bq/d;)Z
    .locals 3

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/u/c;->a(FFLmaps/bq/d;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object v0, p0, Lmaps/u/a;->b:Lmaps/u/c;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 2

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmaps/u/c;->a(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 2

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0, p1, p2}, Lmaps/u/c;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2}, Lmaps/y/bc;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    move-result v0

    return v0
.end method

.method public a_()V
    .locals 1

    iget-object v0, p0, Lmaps/u/a;->b:Lmaps/u/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/u/a;->b:Lmaps/u/c;

    invoke-virtual {v0}, Lmaps/u/c;->a_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/u/a;->b:Lmaps/u/c;

    :cond_0
    return-void
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 2

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0, p1}, Lmaps/u/c;->a_(Lmaps/cr/c;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b()Lmaps/y/am;
    .locals 1

    sget-object v0, Lmaps/y/am;->B:Lmaps/y/am;

    return-object v0
.end method

.method public b(Lmaps/cr/c;)V
    .locals 3

    iget-object v1, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/u/a;->c:Ljava/lang/ref/SoftReference;

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0, p1}, Lmaps/u/c;->b(Lmaps/cr/c;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public b(Lmaps/u/c;)V
    .locals 2

    iget-object v1, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/u/a;->d()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(FFLmaps/bq/d;)Z
    .locals 2

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/u/c;->b(FFLmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 2

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmaps/u/c;->b(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0}, Lmaps/u/c;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lmaps/u/c;)Z
    .locals 1

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lmaps/u/a;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/u/c;

    invoke-virtual {v0}, Lmaps/u/c;->h()V

    goto :goto_0

    :cond_0
    return-void
.end method
