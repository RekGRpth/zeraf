.class public Lmaps/u/b;
.super Lmaps/u/c;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lmaps/p/y;

.field private final c:F

.field private d:[F

.field private e:Lmaps/cr/a;

.field private final f:Lmaps/al/q;

.field private volatile g:Ljava/lang/String;

.field private volatile h:I

.field private volatile i:I

.field private j:Lmaps/af/q;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lmaps/u/c;-><init>()V

    iput-object v0, p0, Lmaps/u/b;->e:Lmaps/cr/a;

    iput-object v0, p0, Lmaps/u/b;->g:Ljava/lang/String;

    const/16 v0, 0x32

    iput v0, p0, Lmaps/u/b;->h:I

    const/4 v0, 0x4

    iput v0, p0, Lmaps/u/b;->i:I

    sget-object v0, Lmaps/af/q;->f:Lmaps/af/q;

    iput-object v0, p0, Lmaps/u/b;->j:Lmaps/af/q;

    iput-object p1, p0, Lmaps/u/b;->a:Landroid/content/res/Resources;

    sget v0, Lmaps/ad/b;->j:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lmaps/u/b;->c:F

    new-instance v0, Lmaps/p/y;

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lmaps/p/y;-><init>(F)V

    iput-object v0, p0, Lmaps/u/b;->b:Lmaps/p/y;

    new-instance v0, Lmaps/al/q;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lmaps/al/q;-><init>(I)V

    iput-object v0, p0, Lmaps/u/b;->f:Lmaps/al/q;

    return-void
.end method

.method private static a(Ljava/util/HashSet;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_1

    const-string v1, ", "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lmaps/cr/c;Lmaps/af/q;)V
    .locals 10

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/high16 v6, -0x1000000

    sget-object v0, Lmaps/af/q;->b:Lmaps/af/q;

    if-ne p2, v0, :cond_1

    const/4 v6, -0x1

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/u/b;->b:Lmaps/p/y;

    iget-object v2, p0, Lmaps/u/b;->g:Ljava/lang/String;

    sget-object v3, Lmaps/p/y;->b:Lmaps/p/l;

    sget-object v4, Lmaps/p/y;->a:Lmaps/t/cv;

    iget v5, p0, Lmaps/u/b;->c:F

    move-object v1, p1

    move v8, v7

    invoke-virtual/range {v0 .. v8}, Lmaps/p/y;->a(Lmaps/cr/c;Ljava/lang/String;Lmaps/p/l;Lmaps/t/cv;FIII)Lmaps/cr/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/u/b;->e:Lmaps/cr/a;

    iget-object v2, p0, Lmaps/u/b;->b:Lmaps/p/y;

    iget-object v3, p0, Lmaps/u/b;->g:Ljava/lang/String;

    sget-object v4, Lmaps/p/y;->b:Lmaps/p/l;

    sget-object v5, Lmaps/p/y;->a:Lmaps/t/cv;

    iget v6, p0, Lmaps/u/b;->c:F

    invoke-virtual/range {v2 .. v7}, Lmaps/p/y;->a(Ljava/lang/String;Lmaps/p/l;Lmaps/t/cv;FZ)[F

    move-result-object v0

    iput-object v0, p0, Lmaps/u/b;->d:[F

    iget-object v0, p0, Lmaps/u/b;->f:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/u/b;->e:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->b()F

    move-result v0

    iget-object v1, p0, Lmaps/u/b;->e:Lmaps/cr/a;

    invoke-virtual {v1}, Lmaps/cr/a;->c()F

    move-result v1

    iget-object v2, p0, Lmaps/u/b;->f:Lmaps/al/q;

    invoke-virtual {v2, v9, v9}, Lmaps/al/q;->a(FF)V

    iget-object v2, p0, Lmaps/u/b;->f:Lmaps/al/q;

    invoke-virtual {v2, v9, v1}, Lmaps/al/q;->a(FF)V

    iget-object v2, p0, Lmaps/u/b;->f:Lmaps/al/q;

    invoke-virtual {v2, v0, v9}, Lmaps/al/q;->a(FF)V

    iget-object v2, p0, Lmaps/u/b;->f:Lmaps/al/q;

    invoke-virtual {v2, v0, v1}, Lmaps/al/q;->a(FF)V

    return-void

    :cond_1
    sget-object v0, Lmaps/af/q;->c:Lmaps/af/q;

    if-ne p2, v0, :cond_0

    const v6, -0x3f3f40

    goto :goto_0
.end method

.method private i()V
    .locals 1

    iget-object v0, p0, Lmaps/u/b;->e:Lmaps/cr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/u/b;->e:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/u/b;->e:Lmaps/cr/a;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/util/HashSet;Ljava/util/HashSet;ILmaps/af/q;)V
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v0, -0x1

    if-eq p3, v0, :cond_1

    :goto_0
    invoke-static {p1}, Lmaps/u/b;->a(Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Lmaps/u/b;->a(Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lmaps/u/b;->a:Landroid/content/res/Resources;

    sget v3, Lmaps/ad/c;->c:I

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v1, v4, v8

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v9

    const/4 v1, 0x4

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lmaps/u/b;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lmaps/u/b;->g:Ljava/lang/String;

    invoke-direct {p0}, Lmaps/u/b;->i()V

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result p3

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Lmaps/u/b;->a:Landroid/content/res/Resources;

    sget v1, Lmaps/ad/c;->d:I

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v1, p0, Lmaps/u/b;->a:Landroid/content/res/Resources;

    sget v2, Lmaps/ad/c;->f:I

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v0, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lmaps/u/b;->a:Landroid/content/res/Resources;

    sget v2, Lmaps/ad/c;->e:I

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v1, v3, v8

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lmaps/u/b;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v1

    iget-object v2, p0, Lmaps/u/b;->j:Lmaps/af/q;

    if-eq v1, v2, :cond_1

    invoke-direct {p0}, Lmaps/u/b;->i()V

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v1

    iput-object v1, p0, Lmaps/u/b;->j:Lmaps/af/q;

    :cond_1
    iget-object v1, p0, Lmaps/u/b;->e:Lmaps/cr/a;

    if-nez v1, :cond_2

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lmaps/u/b;->a(Lmaps/cr/c;Lmaps/af/q;)V

    :cond_2
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p2}, Lmaps/bq/d;->l()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lmaps/u/b;->d:[F

    aget v2, v2, v4

    sub-float/2addr v1, v2

    iget v2, p0, Lmaps/u/b;->h:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p0, Lmaps/u/b;->i:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v1, p0, Lmaps/u/b;->d:[F

    aget v1, v1, v4

    iget-object v2, p0, Lmaps/u/b;->d:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    const/high16 v3, 0x3f800000

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v1, p0, Lmaps/u/b;->f:Lmaps/al/q;

    invoke-virtual {v1, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    iget-object v1, p1, Lmaps/cr/c;->f:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/u/b;->e:Lmaps/cr/a;

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p1}, Lmaps/cr/c;->s()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 1

    invoke-direct {p0}, Lmaps/u/b;->i()V

    iget-object v0, p0, Lmaps/u/b;->b:Lmaps/p/y;

    invoke-virtual {v0}, Lmaps/p/y;->a()V

    return-void
.end method
