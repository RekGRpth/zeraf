.class Lmaps/y/b;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/x;


# instance fields
.field a:Lmaps/q/i;

.field final b:Lmaps/q/ad;

.field final c:Lmaps/q/ac;

.field d:Z

.field e:Lmaps/s/r;

.field f:Lmaps/s/t;

.field final g:Lmaps/q/bb;

.field h:I

.field i:Z

.field final j:Ljava/lang/Object;

.field k:Lmaps/t/ax;

.field l:B

.field m:Z

.field n:F

.field o:Lmaps/ay/e;

.field final p:Ljava/lang/Object;

.field final q:Lmaps/t/ax;

.field r:Lmaps/bq/d;


# direct methods
.method constructor <init>(Lmaps/q/ac;Lmaps/q/ad;Lmaps/t/ax;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/y/b;->j:Ljava/lang/Object;

    invoke-static {}, Lmaps/ay/e;->d()Lmaps/ay/e;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/b;->o:Lmaps/ay/e;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/y/b;->p:Ljava/lang/Object;

    iput-object p2, p0, Lmaps/y/b;->b:Lmaps/q/ad;

    iput-object p1, p0, Lmaps/y/b;->c:Lmaps/q/ac;

    iput-object p3, p0, Lmaps/y/b;->q:Lmaps/t/ax;

    sget-object v0, Lmaps/q/ap;->d:Lmaps/q/ap;

    invoke-virtual {p1, v0}, Lmaps/q/ac;->a(Lmaps/q/ap;)Lmaps/q/z;

    move-result-object v0

    check-cast v0, Lmaps/q/bb;

    iput-object v0, p0, Lmaps/y/b;->g:Lmaps/q/bb;

    new-instance v0, Lmaps/s/t;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2, v2}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v0, p0, Lmaps/y/b;->f:Lmaps/s/t;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method a(I)V
    .locals 3

    iget-object v1, p0, Lmaps/y/b;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lmaps/y/b;->h:I

    invoke-static {v0}, Lmaps/y/bk;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lmaps/y/bk;->e(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v2, p0, Lmaps/y/b;->p:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {}, Lmaps/ay/e;->d()Lmaps/ay/e;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/b;->o:Lmaps/ay/e;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/b;->m:Z

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    iput p1, p0, Lmaps/y/b;->h:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/b;->i:Z

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method a(Lmaps/ay/e;)V
    .locals 2

    iget-object v1, p0, Lmaps/y/b;->p:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lmaps/y/b;->o:Lmaps/ay/e;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/b;->m:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method declared-synchronized a(Lmaps/bq/d;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/y/b;->r:Lmaps/bq/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/q/i;)V
    .locals 2

    iput-object p1, p0, Lmaps/y/b;->a:Lmaps/q/i;

    iget-object v0, p0, Lmaps/y/b;->a:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    return-void
.end method

.method public declared-synchronized b(Lmaps/q/i;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/b;->r:Lmaps/bq/d;

    if-nez v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->h:Z

    if-eqz v0, :cond_0

    const-string v0, "GLPolygonOverlay"

    const-string v1, "commit() called before updateCamera()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lmaps/y/b;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/y/b;->r:Lmaps/bq/d;

    invoke-virtual {p0, v0}, Lmaps/y/b;->b(Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/y/b;->r:Lmaps/bq/d;

    invoke-virtual {p0, v0}, Lmaps/y/b;->c(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/y/b;->c:Lmaps/q/ac;

    iget-object v1, p0, Lmaps/y/b;->e:Lmaps/s/r;

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/r;)V

    iget-boolean v0, p0, Lmaps/y/b;->d:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/y/b;->b:Lmaps/q/ad;

    iget-object v1, p0, Lmaps/y/b;->c:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/ba;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/b;->d:Z

    :cond_2
    iget-object v1, p0, Lmaps/y/b;->j:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-boolean v0, p0, Lmaps/y/b;->i:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/b;->g:Lmaps/q/bb;

    iget v2, p0, Lmaps/y/b;->h:I

    invoke-virtual {v0, v2}, Lmaps/q/bb;->a(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/b;->i:Z

    :cond_3
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v0, p0, Lmaps/y/b;->c:Lmaps/q/ac;

    iget-object v1, p0, Lmaps/y/b;->k:Lmaps/t/ax;

    invoke-virtual {v1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/b;->k:Lmaps/t/ax;

    invoke-virtual {v2}, Lmaps/t/ax;->g()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lmaps/q/ac;->b(Lmaps/t/bx;F)V

    iget-object v0, p0, Lmaps/y/b;->a:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/b;->a:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method b()Z
    .locals 2

    iget-object v1, p0, Lmaps/y/b;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lmaps/y/b;->h:I

    invoke-static {v0}, Lmaps/y/bk;->e(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method b(Lmaps/bq/d;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v6, 0x3fa00000

    iget-object v2, p0, Lmaps/y/b;->p:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lmaps/y/b;->m:Z

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/b;->m:Z

    monitor-exit v2

    :goto_0
    return v1

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v2

    iget-byte v3, p0, Lmaps/y/b;->l:B

    iget-object v4, p0, Lmaps/y/b;->q:Lmaps/t/ax;

    invoke-static {p1}, Lmaps/y/bk;->a(Lmaps/bq/d;)Lmaps/t/ax;

    move-result-object v5

    invoke-static {v4, v5}, Lmaps/y/bk;->a(Lmaps/t/ax;Lmaps/t/ax;)B

    move-result v4

    if-ne v3, v4, :cond_1

    iget v3, p0, Lmaps/y/b;->n:F

    mul-float/2addr v3, v6

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_1

    iget v3, p0, Lmaps/y/b;->n:F

    div-float/2addr v3, v6

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method c(Lmaps/bq/d;)V
    .locals 12

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Lmaps/y/bk;->a(Lmaps/bq/d;)Lmaps/t/ax;

    move-result-object v6

    iget-object v3, p0, Lmaps/y/b;->p:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v7, p0, Lmaps/y/b;->o:Lmaps/ay/e;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/y/b;->q:Lmaps/t/ax;

    invoke-static {v0, v6}, Lmaps/y/bk;->a(Lmaps/t/ax;Lmaps/t/ax;)B

    move-result v8

    and-int/lit8 v0, v8, 0x1

    if-eqz v0, :cond_4

    move v5, v1

    :goto_0
    if-eqz v5, :cond_5

    move v0, v1

    :goto_1
    and-int/lit8 v3, v8, 0x2

    if-eqz v3, :cond_6

    move v4, v1

    :goto_2
    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    and-int/lit8 v3, v8, 0x4

    if-eqz v3, :cond_7

    move v3, v1

    :goto_3
    if-eqz v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    invoke-virtual {v7}, Lmaps/ay/e;->c()I

    move-result v9

    iget-object v10, p0, Lmaps/y/b;->f:Lmaps/s/t;

    invoke-virtual {v10}, Lmaps/s/t;->h()V

    iget-object v10, p0, Lmaps/y/b;->f:Lmaps/s/t;

    mul-int v11, v9, v0

    invoke-virtual {v10, v11}, Lmaps/s/t;->d(I)V

    iget-object v10, p0, Lmaps/y/b;->f:Lmaps/s/t;

    invoke-virtual {v7}, Lmaps/ay/e;->b()I

    move-result v11

    mul-int/lit8 v11, v11, 0x3

    mul-int/2addr v0, v11

    invoke-virtual {v10, v0}, Lmaps/s/t;->b(I)V

    invoke-virtual {v6}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v10

    invoke-virtual {v6}, Lmaps/t/ax;->g()I

    move-result v11

    if-eqz v5, :cond_8

    iget-object v0, p0, Lmaps/y/b;->f:Lmaps/s/t;

    invoke-static {v7, v0, v2}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/b;I)V

    iget-object v0, p0, Lmaps/y/b;->f:Lmaps/s/t;

    invoke-static {v7, v0, v10, v11}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/l;Lmaps/t/bx;I)V

    move v0, v1

    :goto_4
    if-eqz v4, :cond_2

    iget-object v1, p0, Lmaps/y/b;->f:Lmaps/s/t;

    mul-int v2, v9, v0

    invoke-static {v7, v1, v2}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/b;I)V

    iget-object v1, p0, Lmaps/y/b;->f:Lmaps/s/t;

    invoke-static {}, Lmaps/y/bk;->e()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v10, v2}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v2

    invoke-static {v7, v1, v2, v11}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/l;Lmaps/t/bx;I)V

    add-int/lit8 v0, v0, 0x1

    :cond_2
    if-eqz v3, :cond_3

    iget-object v1, p0, Lmaps/y/b;->f:Lmaps/s/t;

    mul-int/2addr v0, v9

    invoke-static {v7, v1, v0}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/b;I)V

    iget-object v0, p0, Lmaps/y/b;->f:Lmaps/s/t;

    invoke-static {}, Lmaps/y/bk;->h()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v10, v1}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v1

    invoke-static {v7, v0, v1, v11}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/l;Lmaps/t/bx;I)V

    :cond_3
    new-instance v0, Lmaps/t/ax;

    invoke-virtual {v6}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v6}, Lmaps/t/ax;->e()Lmaps/t/bx;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    iput-object v0, p0, Lmaps/y/b;->k:Lmaps/t/ax;

    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v0

    iput v0, p0, Lmaps/y/b;->n:F

    iput-byte v8, p0, Lmaps/y/b;->l:B

    iget-object v0, p0, Lmaps/y/b;->f:Lmaps/s/t;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lmaps/s/t;->f(I)Lmaps/s/r;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/b;->e:Lmaps/s/r;

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    move v5, v2

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_1

    :cond_6
    move v4, v2

    goto/16 :goto_2

    :cond_7
    move v3, v2

    goto/16 :goto_3

    :cond_8
    move v0, v2

    goto :goto_4
.end method
