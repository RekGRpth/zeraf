.class public Lmaps/y/q;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/af/n;


# static fields
.field private static final b:F

.field private static final c:Lmaps/bq/a;

.field private static d:F


# instance fields
.field protected final a:Landroid/content/res/Resources;

.field private e:Lmaps/y/ak;

.field private final f:Z

.field private final g:Lmaps/ax/b;

.field private volatile h:Lmaps/bq/a;

.field private volatile i:Lmaps/bq/a;

.field private volatile j:F

.field private volatile k:Lmaps/bq/b;

.field private volatile l:Z

.field private m:Z

.field private n:Lmaps/cq/a;

.field private o:Lmaps/af/f;

.field private p:Lmaps/af/h;

.field private q:Lmaps/af/o;

.field private r:Lmaps/af/g;

.field private s:Lmaps/y/ar;

.field private t:Z

.field private u:I

.field private v:F

.field private final w:Lmaps/b/r;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/high16 v0, 0x3ff0000000000000L

    const-wide/high16 v2, 0x4000000000000000L

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lmaps/y/q;->b:F

    const/4 v0, 0x0

    sput-object v0, Lmaps/y/q;->c:Lmaps/bq/a;

    const/high16 v0, 0x41a80000

    sput v0, Lmaps/y/q;->d:F

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    iput v0, p0, Lmaps/y/q;->u:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lmaps/y/q;->v:F

    iput-object p1, p0, Lmaps/y/q;->a:Landroid/content/res/Resources;

    sget-object v0, Lmaps/bq/d;->d:Lmaps/bq/a;

    iput-object v0, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    sget-object v0, Lmaps/bq/d;->d:Lmaps/bq/a;

    iput-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    :goto_0
    new-instance v2, Lmaps/ax/b;

    int-to-float v0, v0

    invoke-direct {v2, v0}, Lmaps/ax/b;-><init>(F)V

    iput-object v2, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/b;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lmaps/y/q;->f:Z

    invoke-static {}, Lmaps/b/r;->a()Lmaps/b/r;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/q;->w:Lmaps/b/r;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static a(Lmaps/bq/a;Lmaps/bq/d;FF)Lmaps/bq/a;
    .locals 6

    invoke-virtual {p1}, Lmaps/bq/d;->z()F

    move-result v0

    mul-float/2addr v0, p2

    neg-float v1, p3

    invoke-virtual {p1}, Lmaps/bq/d;->z()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v2

    const v3, 0x3c8efa35

    mul-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    div-float/2addr v1, v2

    invoke-virtual {p1}, Lmaps/bq/d;->v()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {p1}, Lmaps/bq/d;->w()Lmaps/t/bx;

    move-result-object v3

    new-instance v4, Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v5

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v2

    invoke-direct {v4, v5, v2}, Lmaps/t/bx;-><init>(II)V

    new-instance v5, Lmaps/t/bx;

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v2

    invoke-virtual {v3}, Lmaps/t/bx;->g()I

    move-result v3

    invoke-direct {v5, v2, v3}, Lmaps/t/bx;-><init>(II)V

    invoke-static {v4, v0, v4}, Lmaps/t/bx;->b(Lmaps/t/bx;FLmaps/t/bx;)V

    invoke-static {v5, v1, v5}, Lmaps/t/bx;->b(Lmaps/t/bx;FLmaps/t/bx;)V

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/bq/a;->a()F

    move-result v2

    invoke-virtual {v0}, Lmaps/t/bx;->h()I

    move-result v3

    invoke-virtual {v0, v4}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v1

    invoke-static {v1, v5, v1}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-virtual {v1, v3}, Lmaps/t/bx;->b(I)V

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {p0}, Lmaps/bq/a;->d()F

    move-result v3

    invoke-virtual {p0}, Lmaps/bq/a;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    return-object v0
.end method

.method public static a(Lmaps/bq/a;Lmaps/bq/d;Lmaps/ax/b;FFF)Lmaps/bq/a;
    .locals 8

    const/high16 v1, 0x40000000

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    sub-float v6, p4, v0

    invoke-virtual {p1}, Lmaps/bq/d;->m()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    sub-float v7, p5, v0

    invoke-static {p0, p1, v6, v7}, Lmaps/y/q;->a(Lmaps/bq/a;Lmaps/bq/d;FF)Lmaps/bq/a;

    move-result-object v0

    invoke-virtual {p2, v0}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v4

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {v4}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v1

    sget v2, Lmaps/y/q;->d:F

    invoke-virtual {v4}, Lmaps/bq/a;->a()F

    move-result v3

    add-float/2addr v3, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {v4}, Lmaps/bq/a;->d()F

    move-result v3

    invoke-virtual {v4}, Lmaps/bq/a;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    invoke-virtual {p1, v0}, Lmaps/bq/d;->a(Lmaps/bq/a;)V

    neg-float v1, v6

    neg-float v2, v7

    invoke-static {v0, p1, v1, v2}, Lmaps/y/q;->a(Lmaps/bq/a;Lmaps/bq/d;FF)Lmaps/bq/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lmaps/bq/d;Lmaps/ax/b;Lmaps/t/bx;F)Lmaps/bq/a;
    .locals 6

    invoke-virtual {p0}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v1

    invoke-virtual {p2}, Lmaps/t/bx;->f()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Lmaps/t/bx;->g()I

    move-result v0

    invoke-virtual {p2}, Lmaps/t/bx;->g()I

    move-result v2

    sub-int/2addr v0, v2

    float-to-double v2, p3

    const-wide v4, 0x400921fb54442d18L

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L

    div-double/2addr v2, v4

    double-to-float v2, v2

    neg-float v3, v2

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v3

    neg-float v2, v2

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    int-to-float v4, v1

    mul-float/2addr v4, v2

    int-to-float v5, v0

    mul-float/2addr v5, v3

    sub-float/2addr v4, v5

    int-to-float v1, v1

    mul-float/2addr v1, v3

    int-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    new-instance v1, Lmaps/t/bx;

    invoke-virtual {p2}, Lmaps/t/bx;->f()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v4

    float-to-int v2, v2

    invoke-virtual {p2}, Lmaps/t/bx;->g()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v0, v3

    float-to-int v0, v0

    invoke-direct {v1, v2, v0}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {p0}, Lmaps/bq/d;->q()F

    move-result v0

    add-float/2addr v0, p3

    invoke-static {v0}, Lmaps/y/q;->d(F)F

    move-result v4

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {p0}, Lmaps/bq/d;->s()F

    move-result v2

    invoke-virtual {p0}, Lmaps/bq/d;->r()F

    move-result v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    invoke-virtual {p1, v0}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v0

    return-object v0
.end method

.method private a(Lmaps/bq/b;)V
    .locals 1

    sget-object v0, Lmaps/y/q;->c:Lmaps/bq/a;

    invoke-direct {p0, p1, v0}, Lmaps/y/q;->a(Lmaps/bq/b;Lmaps/bq/a;)V

    instance-of v0, p1, Lmaps/ax/d;

    if-eqz v0, :cond_0

    check-cast p1, Lmaps/ax/d;

    invoke-interface {p1}, Lmaps/ax/d;->a()V

    :cond_0
    return-void
.end method

.method private a(Lmaps/bq/b;Lmaps/bq/a;)V
    .locals 3

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/q;->s:Lmaps/y/ar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/q;->s:Lmaps/y/ar;

    invoke-interface {v0}, Lmaps/y/ar;->c()V

    :cond_0
    instance-of v0, p1, Lmaps/ax/d;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/q;->l:Z

    :cond_1
    iput-object p1, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/q;->m:Z

    iget-object v0, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    iget-object v1, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    invoke-interface {v1}, Lmaps/bq/b;->b()Lmaps/bq/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    if-eqz p2, :cond_2

    iget-object v0, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    invoke-virtual {v0, p2}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object p2

    :cond_2
    iput-object p2, p0, Lmaps/y/q;->i:Lmaps/bq/a;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/y/q;->o:Lmaps/af/f;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/q;->o:Lmaps/af/f;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v2}, Lmaps/af/f;->a(ZZ)V

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic b(F)F
    .locals 1

    invoke-static {p0}, Lmaps/y/q;->d(F)F

    move-result v0

    return v0
.end method

.method private c(F)F
    .locals 8

    const-wide/high16 v0, 0x4000000000000000L

    const-wide v2, 0x40031eb851eb851fL

    float-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    sget v6, Lmaps/y/q;->b:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x404d5ae147ae147bL

    sub-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method static synthetic d()F
    .locals 1

    sget v0, Lmaps/y/q;->d:F

    return v0
.end method

.method private static d(F)F
    .locals 7

    const-wide v5, 0x4076800000000000L

    move v0, p0

    :goto_0
    float-to-double v1, v0

    cmpl-double v1, v1, v5

    if-ltz v1, :cond_0

    float-to-double v0, v0

    sub-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_0

    :cond_0
    :goto_1
    float-to-double v1, v0

    const-wide/16 v3, 0x0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1

    float-to-double v0, v0

    add-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_1

    :cond_1
    return v0
.end method


# virtual methods
.method public a()F
    .locals 1

    iget-object v0, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    invoke-virtual {v0}, Lmaps/ax/b;->a()Lmaps/ax/c;

    move-result-object v0

    if-nez v0, :cond_0

    const/high16 v0, 0x40000000

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Lmaps/ax/c;->b()F

    move-result v0

    goto :goto_0
.end method

.method public declared-synchronized a(FFF)F
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    iget-object v7, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {v6}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v6}, Lmaps/bq/a;->a()F

    move-result v2

    add-float/2addr v2, p1

    invoke-virtual {v6}, Lmaps/bq/a;->d()F

    move-result v3

    invoke-virtual {v6}, Lmaps/bq/a;->e()F

    move-result v4

    invoke-virtual {v6}, Lmaps/bq/a;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    invoke-virtual {v7, v0}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v0

    invoke-virtual {v6}, Lmaps/bq/a;->a()F

    move-result v1

    invoke-virtual {v0}, Lmaps/bq/a;->a()F

    move-result v0

    cmpl-float v0, v1, v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Lmaps/bq/a;->a()F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    instance-of v0, v0, Lmaps/y/bi;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    check-cast v0, Lmaps/y/bi;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lmaps/y/bi;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    :goto_1
    iput v0, p0, Lmaps/y/q;->j:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    new-instance v0, Lmaps/y/bi;

    iget-object v1, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    invoke-interface {v1}, Lmaps/bq/b;->b()Lmaps/bq/a;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    invoke-direct {v0, v1, v2}, Lmaps/y/bi;-><init>(Lmaps/bq/a;Lmaps/ax/b;)V

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lmaps/y/bi;->a(FFFFFF)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v1, v1, v2

    sget-object v2, Lmaps/y/q;->c:Lmaps/bq/a;

    invoke-direct {p0, v0, v2}, Lmaps/y/q;->a(Lmaps/bq/b;Lmaps/bq/a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    goto :goto_1
.end method

.method public a(FFFI)F
    .locals 7

    iget-object v1, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->a()F

    move-result v0

    add-float/2addr v0, p1

    iput v0, p0, Lmaps/y/q;->j:F

    new-instance v0, Lmaps/y/ap;

    iget-object v2, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lmaps/y/ap;-><init>(Lmaps/bq/a;Lmaps/ax/b;FFFI)V

    sget-object v2, Lmaps/y/q;->c:Lmaps/bq/a;

    invoke-direct {p0, v0, v2}, Lmaps/y/q;->a(Lmaps/bq/b;Lmaps/bq/a;)V

    invoke-virtual {v1}, Lmaps/bq/a;->a()F

    move-result v0

    add-float/2addr v0, p1

    sget v1, Lmaps/y/q;->d:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, 0x40000000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public a(FI)F
    .locals 7

    iget-object v2, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    iget-object v6, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {v2}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v2}, Lmaps/bq/a;->d()F

    move-result v3

    invoke-virtual {v2}, Lmaps/bq/a;->e()F

    move-result v4

    invoke-virtual {v2}, Lmaps/bq/a;->f()F

    move-result v5

    move v2, p1

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    invoke-virtual {v6, v0}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lmaps/y/q;->a(Lmaps/bq/b;I)V

    invoke-virtual {v0}, Lmaps/bq/a;->a()F

    move-result v0

    return v0
.end method

.method public a(Lmaps/t/bx;)F
    .locals 1

    iget-object v0, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    invoke-virtual {v0}, Lmaps/ax/b;->a()Lmaps/ax/c;

    move-result-object v0

    if-nez v0, :cond_0

    sget v0, Lmaps/y/q;->d:F

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0, p1}, Lmaps/ax/c;->a(Lmaps/t/bx;)F

    move-result v0

    goto :goto_0
.end method

.method public a(F)V
    .locals 1

    iget-object v0, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    invoke-virtual {v0, p1}, Lmaps/ax/b;->a(F)V

    return-void
.end method

.method public declared-synchronized a(FF)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    instance-of v0, v0, Lmaps/y/bi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    check-cast v0, Lmaps/y/bi;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/y/bi;->a(FFFFFF)[F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lmaps/y/bi;

    iget-object v1, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    invoke-interface {v1}, Lmaps/bq/b;->b()Lmaps/bq/a;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    invoke-direct {v0, v1, v2}, Lmaps/y/bi;-><init>(Lmaps/bq/a;Lmaps/ax/b;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/y/bi;->a(FFFFFF)[F

    sget-object v1, Lmaps/y/q;->c:Lmaps/bq/a;

    invoke-direct {p0, v0, v1}, Lmaps/y/q;->a(Lmaps/bq/b;Lmaps/bq/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/af/f;)V
    .locals 1

    iput-object p1, p0, Lmaps/y/q;->o:Lmaps/af/f;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/q;->t:Z

    return-void
.end method

.method public a(Lmaps/af/h;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/q;->p:Lmaps/af/h;

    return-void
.end method

.method public declared-synchronized a(Lmaps/af/o;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/y/q;->q:Lmaps/af/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/ax/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    invoke-virtual {v0, p1}, Lmaps/ax/b;->a(Lmaps/ax/c;)V

    return-void
.end method

.method public a(Lmaps/bq/b;I)V
    .locals 1

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lmaps/y/q;->a(Lmaps/bq/b;II)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(Lmaps/bq/b;II)V
    .locals 11

    const/4 v10, -0x1

    const/high16 v9, 0x442f0000

    const/high16 v8, 0x41000000

    const/4 v0, 0x0

    iget-boolean v1, p0, Lmaps/y/q;->t:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lmaps/y/q;->t:Z

    if-eqz v1, :cond_1

    move p3, v0

    move p2, v0

    :cond_1
    iput-boolean v0, p0, Lmaps/y/q;->t:Z

    iget-object v1, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    invoke-interface {p1}, Lmaps/bq/b;->b()Lmaps/bq/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmaps/bq/a;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/bq/a;->a()F

    move-result v2

    iput v2, p0, Lmaps/y/q;->j:F

    invoke-static {}, Lmaps/be/b;->a()Lmaps/be/o;

    move-result-object v2

    invoke-virtual {v6}, Lmaps/bq/a;->a()F

    move-result v3

    invoke-virtual {v1}, Lmaps/bq/a;->a()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-virtual {v2}, Lmaps/be/o;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lmaps/be/o;->c()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v3, v2

    if-lez v2, :cond_3

    :cond_2
    invoke-direct {p0, p1}, Lmaps/y/q;->a(Lmaps/bq/b;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v6}, Lmaps/bq/a;->a()F

    move-result v2

    invoke-virtual {v1}, Lmaps/bq/a;->a()F

    move-result v4

    add-float/2addr v2, v4

    const/high16 v4, 0x3f000000

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    const/16 v4, 0x1e

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/high16 v4, 0x40000000

    shr-int v2, v4, v2

    invoke-virtual {v6}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v4

    invoke-virtual {v1}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmaps/t/bx;->c(Lmaps/t/bx;)F

    move-result v5

    int-to-float v4, v2

    div-float v7, v5, v4

    iget v4, p0, Lmaps/y/q;->u:I

    int-to-float v4, v4

    cmpg-float v4, v7, v4

    if-gtz v4, :cond_4

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_6

    if-nez p2, :cond_5

    invoke-direct {p0, p1}, Lmaps/y/q;->a(Lmaps/bq/b;)V

    goto :goto_0

    :cond_4
    move v4, v0

    goto :goto_1

    :cond_5
    if-ne p2, v10, :cond_9

    invoke-static {v3, v8}, Ljava/lang/Math;->min(FF)F

    move-result v0

    div-float/2addr v0, v8

    const/high16 v2, 0x3f400000

    mul-float/2addr v0, v2

    const/high16 v2, 0x3fc00000

    add-float/2addr v0, v2

    mul-float/2addr v0, v9

    float-to-int v3, v0

    :goto_2
    const/4 v5, 0x0

    new-instance v0, Lmaps/y/ad;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lmaps/y/ad;-><init>(Lmaps/bq/a;Lmaps/bq/b;IZF)V

    sget-object v1, Lmaps/y/q;->c:Lmaps/bq/a;

    invoke-direct {p0, v0, v1}, Lmaps/y/q;->a(Lmaps/bq/b;Lmaps/bq/a;)V

    goto/16 :goto_0

    :cond_6
    if-nez p3, :cond_7

    invoke-direct {p0, p1}, Lmaps/y/q;->a(Lmaps/bq/b;)V

    goto/16 :goto_0

    :cond_7
    if-ne p3, v10, :cond_8

    iget v0, p0, Lmaps/y/q;->u:I

    int-to-float v0, v0

    sub-float v0, v7, v0

    const/high16 v3, 0x4e800000

    int-to-float v2, v2

    div-float v2, v3, v2

    iget v3, p0, Lmaps/y/q;->u:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    const v2, 0x40833333

    mul-float/2addr v0, v2

    const v2, 0x3fb33333

    add-float/2addr v0, v2

    mul-float/2addr v0, v9

    float-to-int v0, v0

    const/16 v2, 0x9c4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lmaps/y/q;->v:F

    mul-float/2addr v0, v2

    float-to-int v3, v0

    :goto_3
    invoke-direct {p0, v5}, Lmaps/y/q;->c(F)F

    move-result v5

    new-instance v0, Lmaps/y/ad;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lmaps/y/ad;-><init>(Lmaps/bq/a;Lmaps/bq/b;IZF)V

    invoke-direct {p0, v0, v6}, Lmaps/y/q;->a(Lmaps/bq/b;Lmaps/bq/a;)V

    goto/16 :goto_0

    :cond_8
    move v3, p3

    goto :goto_3

    :cond_9
    move v3, p2

    goto :goto_2
.end method

.method public a(Lmaps/t/bx;I)V
    .locals 6

    iget-object v1, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->a()F

    move-result v2

    invoke-virtual {v1}, Lmaps/bq/a;->d()F

    move-result v3

    invoke-virtual {v1}, Lmaps/bq/a;->e()F

    move-result v4

    const/4 v5, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    invoke-virtual {p0, v0, p2}, Lmaps/y/q;->a(Lmaps/bq/b;I)V

    return-void
.end method

.method public declared-synchronized a(Lmaps/y/ar;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/y/q;->s:Lmaps/y/ar;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(ZZZII)V
    .locals 6

    iget-object v0, p0, Lmaps/y/q;->e:Lmaps/y/ak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/q;->e:Lmaps/y/ak;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lmaps/y/ak;->a(ZZZII)V

    :cond_0
    return-void
.end method

.method public declared-synchronized b(FFF)F
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    instance-of v0, v0, Lmaps/y/bi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    check-cast v0, Lmaps/y/bi;

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/y/bi;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    new-instance v0, Lmaps/y/bi;

    iget-object v1, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    iget-object v2, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    invoke-direct {v0, v1, v2}, Lmaps/y/bi;-><init>(Lmaps/bq/a;Lmaps/ax/b;)V

    sget-object v1, Lmaps/y/q;->c:Lmaps/bq/a;

    invoke-direct {p0, v0, v1}, Lmaps/y/q;->a(Lmaps/bq/b;Lmaps/bq/a;)V

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/y/bi;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(FI)F
    .locals 7

    iget-object v5, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    iget-object v6, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {v5}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v5}, Lmaps/bq/a;->a()F

    move-result v2

    add-float/2addr v2, p1

    invoke-virtual {v5}, Lmaps/bq/a;->d()F

    move-result v3

    invoke-virtual {v5}, Lmaps/bq/a;->e()F

    move-result v4

    invoke-virtual {v5}, Lmaps/bq/a;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    invoke-virtual {v6, v0}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lmaps/y/q;->a(Lmaps/bq/b;I)V

    invoke-virtual {v0}, Lmaps/bq/a;->a()F

    move-result v0

    return v0
.end method

.method public b(Lmaps/bq/d;)I
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    instance-of v1, v1, Lmaps/ax/d;

    if-eqz v1, :cond_5

    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    check-cast v0, Lmaps/ax/d;

    invoke-interface {v0}, Lmaps/ax/d;->c()I

    move-result v1

    invoke-interface {v0, p1}, Lmaps/ax/d;->a(Lmaps/bq/d;)Lmaps/bq/b;

    move-result-object v2

    iput-object v2, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    iget-object v2, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    iget-object v3, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    invoke-interface {v3}, Lmaps/bq/b;->b()Lmaps/bq/a;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v2

    iput-object v2, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    invoke-interface {v0}, Lmaps/ax/d;->c()I

    move-result v0

    or-int/2addr v0, v1

    :goto_0
    iget-object v1, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    invoke-virtual {p1, v1}, Lmaps/bq/d;->a(Lmaps/bq/a;)V

    iget-boolean v1, p0, Lmaps/y/q;->m:Z

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    iget-object v1, p0, Lmaps/y/q;->q:Lmaps/af/o;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/y/q;->n:Lmaps/cq/a;

    if-nez v1, :cond_0

    new-instance v1, Lmaps/y/n;

    new-instance v2, Lmaps/o/o;

    invoke-direct {v2}, Lmaps/o/o;-><init>()V

    invoke-direct {v1, v2}, Lmaps/y/n;-><init>(Lmaps/o/l;)V

    iput-object v1, p0, Lmaps/y/q;->n:Lmaps/cq/a;

    :cond_0
    iget-object v1, p0, Lmaps/y/q;->n:Lmaps/cq/a;

    invoke-interface {v1, p1}, Lmaps/cq/a;->a(Lmaps/bq/d;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/q;->q:Lmaps/af/o;

    iget-object v3, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    invoke-interface {v2, v3, v1}, Lmaps/af/o;->a(Lmaps/bq/a;Ljava/util/List;)V

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/y/q;->m:Z

    :cond_2
    iget-object v1, p0, Lmaps/y/q;->r:Lmaps/af/g;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/y/q;->r:Lmaps/af/g;

    iget-object v2, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    invoke-interface {v1, v2}, Lmaps/af/g;->a(Lmaps/bq/a;)V

    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_4

    iget-object v1, p0, Lmaps/y/q;->p:Lmaps/af/h;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/y/q;->p:Lmaps/af/h;

    invoke-virtual {v1, p1}, Lmaps/af/h;->a(Lmaps/bq/d;)V

    :cond_4
    return v0

    :cond_5
    :try_start_1
    iget-object v1, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    iget-object v2, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    invoke-interface {v2}, Lmaps/bq/b;->b()Lmaps/bq/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v1

    iput-object v1, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    iget-object v1, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    invoke-interface {v1}, Lmaps/bq/b;->b()Lmaps/bq/a;

    move-result-object v1

    iput-object v1, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/y/q;->l:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()Lmaps/bq/a;
    .locals 1

    iget-object v0, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    return-object v0
.end method

.method public declared-synchronized b(FF)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    instance-of v0, v0, Lmaps/y/au;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/y/au;

    iget-object v1, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    invoke-interface {v1}, Lmaps/bq/b;->b()Lmaps/bq/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/y/au;-><init>(Lmaps/bq/a;)V

    sget-object v1, Lmaps/y/q;->c:Lmaps/bq/a;

    invoke-direct {p0, v0, v1}, Lmaps/y/q;->a(Lmaps/bq/b;Lmaps/bq/a;)V

    :cond_0
    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    check-cast v0, Lmaps/y/au;

    invoke-virtual {v0, p1, p2}, Lmaps/y/au;->a(FF)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    float-to-int v4, p1

    float-to-int v5, p2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lmaps/y/q;->a(ZZZII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()F
    .locals 1

    iget v0, p0, Lmaps/y/q;->j:F

    return v0
.end method

.method public declared-synchronized c(FI)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/y/q;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    instance-of v0, v0, Lmaps/y/bd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/q;->k:Lmaps/bq/b;

    check-cast v0, Lmaps/y/bd;

    invoke-virtual {v0, p1}, Lmaps/y/bd;->a(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lmaps/y/bd;

    iget-object v1, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    iget-object v2, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    invoke-direct {v0, v1, v2}, Lmaps/y/bd;-><init>(Lmaps/bq/a;Lmaps/ax/b;)V

    sget-object v1, Lmaps/y/q;->c:Lmaps/bq/a;

    invoke-direct {p0, v0, v1}, Lmaps/y/q;->a(Lmaps/bq/b;Lmaps/bq/a;)V

    invoke-virtual {v0, p1}, Lmaps/y/bd;->a(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v5, p0, Lmaps/y/q;->h:Lmaps/bq/a;

    invoke-virtual {v5}, Lmaps/bq/a;->d()F

    move-result v0

    add-float v3, v0, p1

    iget-object v6, p0, Lmaps/y/q;->g:Lmaps/ax/b;

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {v5}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v5}, Lmaps/bq/a;->a()F

    move-result v2

    invoke-virtual {v5}, Lmaps/bq/a;->e()F

    move-result v4

    invoke-virtual {v5}, Lmaps/bq/a;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    invoke-virtual {v6, v0}, Lmaps/ax/b;->a(Lmaps/bq/a;)Lmaps/bq/a;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lmaps/y/q;->a(Lmaps/bq/b;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public i()Lmaps/bq/a;
    .locals 1

    iget-object v0, p0, Lmaps/y/q;->i:Lmaps/bq/a;

    return-object v0
.end method
