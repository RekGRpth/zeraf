.class public Lmaps/y/bl;
.super Ljava/lang/Object;


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field private final h:Ljava/lang/Boolean;

.field private final i:Ljava/lang/Boolean;

.field private final j:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;ZIIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/y/bl;->h:Ljava/lang/Boolean;

    iput-object p2, p0, Lmaps/y/bl;->i:Ljava/lang/Boolean;

    iput-object p3, p0, Lmaps/y/bl;->j:Ljava/lang/Boolean;

    iput-boolean p4, p0, Lmaps/y/bl;->a:Z

    iput p5, p0, Lmaps/y/bl;->b:I

    iput p6, p0, Lmaps/y/bl;->c:I

    iput p7, p0, Lmaps/y/bl;->d:I

    iput p8, p0, Lmaps/y/bl;->e:I

    iput p9, p0, Lmaps/y/bl;->f:I

    iput p10, p0, Lmaps/y/bl;->g:I

    return-void
.end method

.method public static a()Lmaps/y/as;
    .locals 1

    new-instance v0, Lmaps/y/as;

    invoke-direct {v0}, Lmaps/y/as;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(ZZZ)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/y/bl;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/y/bl;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/y/bl;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/y/bl;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, p2, :cond_0

    :cond_2
    iget-object v1, p0, Lmaps/y/bl;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/y/bl;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, p3, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
