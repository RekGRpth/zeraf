.class public Lmaps/y/bk;
.super Lmaps/y/bc;


# static fields
.field private static final a:Lmaps/t/bx;

.field private static final b:Lmaps/t/bx;


# instance fields
.field private final c:Ljava/util/List;

.field private final d:Lmaps/t/ax;

.field private final e:Ljava/util/List;

.field private final f:Ljava/lang/Object;

.field private g:Lmaps/ay/e;

.field private h:Lmaps/t/ax;

.field private i:F

.field private j:F

.field private k:B

.field private l:Z

.field private m:Z

.field private n:Lmaps/al/o;

.field private o:Lmaps/al/i;

.field private p:Lmaps/q/ac;

.field private q:Lmaps/y/b;

.field private r:Lmaps/t/ax;

.field private s:I

.field private final t:Ljava/lang/Object;

.field private u:I

.field private v:I

.field private final w:Ljava/util/List;

.field private x:Z

.field private y:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lmaps/t/bx;

    const/high16 v1, -0x40000000

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    sput-object v0, Lmaps/y/bk;->a:Lmaps/t/bx;

    new-instance v0, Lmaps/t/bx;

    const/high16 v1, 0x40000000

    invoke-direct {v0, v1, v2}, Lmaps/t/bx;-><init>(II)V

    sput-object v0, Lmaps/y/bk;->b:Lmaps/t/bx;

    return-void
.end method

.method public constructor <init>(Lmaps/t/cg;Ljava/util/List;III)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lmaps/y/bk;-><init>(Lmaps/t/cg;Ljava/util/List;IIIZ)V

    return-void
.end method

.method public constructor <init>(Lmaps/t/cg;Ljava/util/List;IIIZ)V
    .locals 9

    const/4 v6, 0x0

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bk;->e:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/y/bk;->f:Ljava/lang/Object;

    invoke-static {}, Lmaps/ay/e;->d()Lmaps/ay/e;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bk;->g:Lmaps/ay/e;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/y/bk;->t:Ljava/lang/Object;

    iput-boolean v6, p0, Lmaps/y/bk;->x:Z

    iput-boolean v6, p0, Lmaps/y/bk;->y:Z

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bk;->c:Ljava/util/List;

    invoke-virtual {p1}, Lmaps/t/cg;->g()I

    move-result v1

    iget-object v0, p0, Lmaps/y/bk;->c:Ljava/util/List;

    invoke-virtual {p1, v1}, Lmaps/t/cg;->c(I)Lmaps/t/cg;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    iget-object v3, p0, Lmaps/y/bk;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Lmaps/t/cg;->c(I)Lmaps/t/cg;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/y/bk;->c:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->a()Lmaps/t/ax;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bk;->d:Lmaps/t/ax;

    iput p3, p0, Lmaps/y/bk;->v:I

    iput p4, p0, Lmaps/y/bk;->u:I

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bk;->w:Ljava/util/List;

    iget-object v0, p0, Lmaps/y/bk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/t/cg;

    iget-object v8, p0, Lmaps/y/bk;->w:Ljava/util/List;

    new-instance v0, Lmaps/y/a;

    iget v2, p0, Lmaps/y/bk;->v:I

    int-to-float v2, v2

    iget v3, p0, Lmaps/y/bk;->u:I

    const/4 v4, 0x0

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lmaps/y/a;-><init>(Lmaps/t/cg;FILmaps/t/bg;Z)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iput p5, p0, Lmaps/y/bk;->s:I

    iput-boolean p6, p0, Lmaps/y/bk;->y:Z

    invoke-static {}, Lmaps/ay/e;->d()Lmaps/ay/e;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bk;->g:Lmaps/ay/e;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p5}, Lmaps/y/bk;->f(I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v6

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x78

    const-string v2, "t"

    invoke-static {v1, v2, v0}, Lmaps/bh/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    const/4 v0, 0x1

    goto :goto_2
.end method

.method static synthetic a(Lmaps/t/ax;Lmaps/t/ax;)B
    .locals 1

    invoke-static {p0, p1}, Lmaps/y/bk;->b(Lmaps/t/ax;Lmaps/t/ax;)B

    move-result v0

    return v0
.end method

.method static a(IZ)I
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x2

    rsub-int/lit8 v1, p0, 0x1e

    shl-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    rsub-int/lit8 v1, p0, 0x1e

    shl-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method static synthetic a(Lmaps/y/bk;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/y/bk;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lmaps/y/bk;Lmaps/ay/e;)Lmaps/ay/e;
    .locals 0

    iput-object p1, p0, Lmaps/y/bk;->g:Lmaps/ay/e;

    return-object p1
.end method

.method static synthetic a(Lmaps/bq/d;)Lmaps/t/ax;
    .locals 1

    invoke-static {p0}, Lmaps/y/bk;->f(Lmaps/bq/d;)Lmaps/t/ax;

    move-result-object v0

    return-object v0
.end method

.method private a(Lmaps/bq/d;Z)V
    .locals 1

    if-nez p2, :cond_0

    invoke-direct {p0, p1}, Lmaps/y/bk;->b(Lmaps/bq/d;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lmaps/y/bk;->c(Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lmaps/y/bk;->g(Lmaps/bq/d;)V

    :cond_1
    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lmaps/y/bk;->d(Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0, p1}, Lmaps/y/bk;->h(Lmaps/bq/d;)V

    invoke-direct {p0}, Lmaps/y/bk;->j()V

    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lmaps/cr/c;Lmaps/bq/d;)V
    .locals 3

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/bk;->r:Lmaps/t/ax;

    invoke-virtual {v1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/bk;->r:Lmaps/t/ax;

    invoke-virtual {v2}, Lmaps/t/ax;->g()I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1, p2, v1, v2}, Lmaps/af/d;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    iget-object v1, p0, Lmaps/y/bk;->n:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/y/bk;->t:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v2, p0, Lmaps/y/bk;->s:I

    invoke-static {v0, v2}, Lmaps/s/k;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/y/bk;->o:Lmaps/al/i;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lmaps/y/bk;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmaps/y/bk;->m:Z

    return p1
.end method

.method private static b(Lmaps/t/ax;Lmaps/t/ax;)B
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1, p0}, Lmaps/t/ax;->a(Lmaps/t/an;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    int-to-byte v0, v0

    :cond_0
    sget-object v1, Lmaps/y/bk;->b:Lmaps/t/bx;

    new-instance v2, Lmaps/t/ax;

    invoke-virtual {p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {p1}, Lmaps/t/ax;->e()Lmaps/t/bx;

    move-result-object v4

    invoke-virtual {v4, v1}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-virtual {v2, p0}, Lmaps/t/ax;->a(Lmaps/t/an;)Z

    move-result v2

    if-eqz v2, :cond_1

    or-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    :cond_1
    new-instance v2, Lmaps/t/ax;

    invoke-virtual {p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {p1}, Lmaps/t/ax;->e()Lmaps/t/bx;

    move-result-object v4

    invoke-virtual {v4, v1}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    invoke-virtual {v2, p0}, Lmaps/t/ax;->a(Lmaps/t/an;)Z

    move-result v1

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    :cond_2
    return v0
.end method

.method static synthetic b(Lmaps/y/bk;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/y/bk;->e:Ljava/util/List;

    return-object v0
.end method

.method private b(Lmaps/cr/c;Lmaps/bq/d;)V
    .locals 12

    invoke-static {p2}, Lmaps/y/bk;->f(Lmaps/bq/d;)Lmaps/t/ax;

    move-result-object v4

    iget-object v1, p0, Lmaps/y/bk;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v5, p0, Lmaps/y/bk;->g:Lmaps/ay/e;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/y/bk;->d:Lmaps/t/ax;

    invoke-static {v0, v4}, Lmaps/y/bk;->b(Lmaps/t/ax;Lmaps/t/ax;)B

    move-result v6

    and-int/lit8 v0, v6, 0x1

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    move v3, v0

    :goto_0
    if-eqz v3, :cond_6

    const/4 v0, 0x1

    :goto_1
    and-int/lit8 v1, v6, 0x2

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    and-int/lit8 v1, v6, 0x4

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    invoke-virtual {v5}, Lmaps/ay/e;->c()I

    move-result v7

    new-instance v8, Lmaps/al/o;

    mul-int v9, v7, v0

    invoke-direct {v8, v9}, Lmaps/al/o;-><init>(I)V

    new-instance v9, Lmaps/al/i;

    invoke-virtual {v5}, Lmaps/ay/e;->b()I

    move-result v10

    mul-int/lit8 v10, v10, 0x3

    mul-int/2addr v0, v10

    invoke-direct {v9, v0}, Lmaps/al/i;-><init>(I)V

    const/4 v0, 0x0

    invoke-virtual {v4}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v10

    invoke-virtual {v4}, Lmaps/t/ax;->g()I

    move-result v11

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    invoke-static {v5, v9, v0}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/b;I)V

    invoke-static {v5, v8, v10, v11}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/l;Lmaps/t/bx;I)V

    const/4 v0, 0x1

    :cond_2
    if-eqz v2, :cond_3

    mul-int v2, v7, v0

    invoke-static {v5, v9, v2}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/b;I)V

    sget-object v2, Lmaps/y/bk;->a:Lmaps/t/bx;

    invoke-virtual {v10, v2}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v2

    invoke-static {v5, v8, v2, v11}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/l;Lmaps/t/bx;I)V

    add-int/lit8 v0, v0, 0x1

    :cond_3
    if-eqz v1, :cond_4

    mul-int/2addr v0, v7

    invoke-static {v5, v9, v0}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/b;I)V

    sget-object v0, Lmaps/y/bk;->b:Lmaps/t/bx;

    invoke-virtual {v10, v0}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    invoke-static {v5, v8, v0, v11}, Lmaps/ay/f;->a(Lmaps/ay/e;Lmaps/al/l;Lmaps/t/bx;I)V

    :cond_4
    iput-object v8, p0, Lmaps/y/bk;->n:Lmaps/al/o;

    iput-object v9, p0, Lmaps/y/bk;->o:Lmaps/al/i;

    new-instance v0, Lmaps/t/ax;

    invoke-virtual {v4}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v4}, Lmaps/t/ax;->e()Lmaps/t/bx;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    iput-object v0, p0, Lmaps/y/bk;->r:Lmaps/t/ax;

    invoke-virtual {p2}, Lmaps/bq/d;->p()F

    move-result v0

    iput v0, p0, Lmaps/y/bk;->j:F

    iput-byte v6, p0, Lmaps/y/bk;->k:B

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v1, 0x0

    move v2, v1

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method

.method private b(Lmaps/bq/d;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/y/bk;->h:Lmaps/t/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bk;->d:Lmaps/t/ax;

    iget-object v3, p0, Lmaps/y/bk;->h:Lmaps/t/ax;

    invoke-static {v0, v3}, Lmaps/y/bk;->b(Lmaps/t/ax;Lmaps/t/ax;)B

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lmaps/y/bk;->d:Lmaps/t/ax;

    invoke-static {p1}, Lmaps/y/bk;->f(Lmaps/bq/d;)Lmaps/t/ax;

    move-result-object v4

    invoke-static {v3, v4}, Lmaps/y/bk;->b(Lmaps/t/ax;Lmaps/t/ax;)B

    move-result v3

    if-nez v3, :cond_3

    move v3, v1

    :goto_1
    if-eq v0, v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1
.end method

.method static synthetic c(Lmaps/y/bk;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/y/bk;->c:Ljava/util/List;

    return-object v0
.end method

.method private c(Lmaps/bq/d;)Z
    .locals 3

    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v0

    iget v1, p0, Lmaps/y/bk;->i:F

    const/high16 v2, 0x40000000

    div-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lmaps/y/bk;)Lmaps/y/b;
    .locals 1

    iget-object v0, p0, Lmaps/y/bk;->q:Lmaps/y/b;

    return-object v0
.end method

.method private d(Lmaps/bq/d;)Z
    .locals 3

    const/high16 v2, 0x40000000

    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v0

    iget v1, p0, Lmaps/y/bk;->i:F

    mul-float/2addr v1, v2

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_0

    iget v1, p0, Lmaps/y/bk;->i:F

    div-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e()Lmaps/t/bx;
    .locals 1

    sget-object v0, Lmaps/y/bk;->a:Lmaps/t/bx;

    return-object v0
.end method

.method static synthetic e(I)Z
    .locals 1

    invoke-static {p0}, Lmaps/y/bk;->f(I)Z

    move-result v0

    return v0
.end method

.method private e(Lmaps/bq/d;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v6, 0x3fa00000

    iget-object v2, p0, Lmaps/y/bk;->f:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lmaps/y/bk;->m:Z

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/bk;->m:Z

    monitor-exit v2

    :goto_0
    return v1

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v2

    iget-byte v3, p0, Lmaps/y/bk;->k:B

    iget-object v4, p0, Lmaps/y/bk;->d:Lmaps/t/ax;

    invoke-static {p1}, Lmaps/y/bk;->f(Lmaps/bq/d;)Lmaps/t/ax;

    move-result-object v5

    invoke-static {v4, v5}, Lmaps/y/bk;->b(Lmaps/t/ax;Lmaps/t/ax;)B

    move-result v4

    if-ne v3, v4, :cond_1

    iget v3, p0, Lmaps/y/bk;->j:F

    mul-float/2addr v3, v6

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_1

    iget v3, p0, Lmaps/y/bk;->j:F

    div-float/2addr v3, v6

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static f(Lmaps/bq/d;)Lmaps/t/ax;
    .locals 5

    const/high16 v4, 0x20000000

    invoke-virtual {p0}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/u;->b()Lmaps/t/ax;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/ax;->g()I

    move-result v0

    invoke-virtual {v2}, Lmaps/t/ax;->h()I

    move-result v1

    const v3, 0x71c71c7

    if-gt v0, v3, :cond_0

    if-le v1, v3, :cond_1

    :cond_0
    new-instance v1, Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/ax;->f()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v0

    sub-int/2addr v0, v4

    const/high16 v3, -0x20000000

    invoke-direct {v1, v0, v3}, Lmaps/t/bx;-><init>(II)V

    new-instance v0, Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/ax;->f()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v2

    add-int/2addr v2, v4

    add-int/lit8 v2, v2, -0x1

    const v3, 0x1fffffff

    invoke-direct {v0, v2, v3}, Lmaps/t/bx;-><init>(II)V

    :goto_0
    new-instance v2, Lmaps/t/ax;

    invoke-direct {v2, v1, v0}, Lmaps/t/ax;-><init>(Lmaps/t/bx;Lmaps/t/bx;)V

    return-object v2

    :cond_1
    new-instance v3, Lmaps/t/bx;

    mul-int/lit8 v0, v0, 0x4

    mul-int/lit8 v1, v1, 0x4

    invoke-direct {v3, v0, v1}, Lmaps/t/bx;-><init>(II)V

    invoke-virtual {v2}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v2}, Lmaps/t/ax;->e()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmaps/t/bx;->e(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    goto :goto_0
.end method

.method private static f(I)Z
    .locals 1

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Lmaps/bq/d;)V
    .locals 4

    invoke-static {p1}, Lmaps/y/bk;->f(Lmaps/bq/d;)Lmaps/t/ax;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/bk;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/y/bk;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lmaps/y/bk;->d:Lmaps/t/ax;

    invoke-static {v2, v0}, Lmaps/y/bk;->b(Lmaps/t/ax;Lmaps/t/ax;)B

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/y/bk;->e:Ljava/util/List;

    iget-object v3, p0, Lmaps/y/bk;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iput-object v0, p0, Lmaps/y/bk;->h:Lmaps/t/ax;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic h()Lmaps/t/bx;
    .locals 1

    sget-object v0, Lmaps/y/bk;->b:Lmaps/t/bx;

    return-object v0
.end method

.method private h(Lmaps/bq/d;)V
    .locals 6

    iget-object v1, p0, Lmaps/y/bk;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/y/bk;->e:Ljava/util/List;

    invoke-static {v0}, Lmaps/f/fd;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    iget-boolean v1, p0, Lmaps/y/bk;->y:Z

    invoke-static {v0, v1}, Lmaps/y/bk;->a(IZ)I

    move-result v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    int-to-float v5, v3

    invoke-virtual {v0, v5}, Lmaps/t/cg;->b(F)Lmaps/t/cg;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    iget-object v1, p0, Lmaps/y/bk;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lmaps/y/bk;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/y/bk;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v0

    iput v0, p0, Lmaps/y/bk;->i:F

    return-void

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private i()Z
    .locals 2

    iget-object v1, p0, Lmaps/y/bk;->t:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lmaps/y/bk;->s:I

    invoke-static {v0}, Lmaps/y/bk;->f(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private j()V
    .locals 3

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lmaps/y/av;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lmaps/y/av;-><init>(Lmaps/y/bk;Lmaps/y/e;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-boolean v1, p0, Lmaps/y/bk;->x:Z

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/y/bk;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lmaps/y/bk;->t:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lmaps/y/bk;->l:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lmaps/y/bk;->l:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v1, p0, Lmaps/y/bk;->h:Lmaps/t/ax;

    if-nez v1, :cond_2

    invoke-direct {p0, p2, v0}, Lmaps/y/bk;->a(Lmaps/bq/d;Z)V

    :cond_2
    invoke-direct {p0, p2}, Lmaps/y/bk;->e(Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, p2}, Lmaps/y/bk;->b(Lmaps/cr/c;Lmaps/bq/d;)V

    :cond_3
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lmaps/y/bk;->n:Lmaps/al/o;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/y/bk;->n:Lmaps/al/o;

    invoke-virtual {v1}, Lmaps/al/o;->c()I

    move-result v1

    if-lez v1, :cond_4

    invoke-direct {p0, p1, p2}, Lmaps/y/bk;->a(Lmaps/cr/c;Lmaps/bq/d;)V

    :cond_4
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    :cond_5
    invoke-virtual {p1}, Lmaps/cr/c;->F()V

    iget-object v1, p0, Lmaps/y/bk;->w:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/y/bk;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/a;

    invoke-virtual {p1}, Lmaps/cr/c;->D()V

    invoke-virtual {v0, p1, p2, p3}, Lmaps/y/a;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    invoke-virtual {p1}, Lmaps/cr/c;->F()V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_6
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {p1}, Lmaps/cr/c;->D()V

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 3

    invoke-direct {p0}, Lmaps/y/bk;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/y/bk;->t:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lmaps/y/bk;->l:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lmaps/y/bk;->l:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-direct {p0, p1, v0}, Lmaps/y/bk;->a(Lmaps/bq/d;Z)V

    :cond_0
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/bk;->q:Lmaps/y/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/bk;->q:Lmaps/y/b;

    invoke-virtual {v0, p1}, Lmaps/y/b;->a(Lmaps/bq/d;)V

    :cond_1
    iget-object v1, p0, Lmaps/y/bk;->w:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/y/bk;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/a;

    invoke-virtual {v0, p1, p2}, Lmaps/y/a;->a(Lmaps/bq/d;Lmaps/cr/c;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_2
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v0, 0x1

    return v0
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/y/bk;->c(Lmaps/cr/c;)V

    return-void
.end method

.method public b()Lmaps/y/am;
    .locals 1

    sget-object v0, Lmaps/y/am;->a:Lmaps/y/am;

    return-object v0
.end method

.method public b(I)V
    .locals 3

    iput p1, p0, Lmaps/y/bk;->u:I

    iget-object v1, p0, Lmaps/y/bk;->w:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/y/bk;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/a;

    invoke-virtual {v0, p1}, Lmaps/y/a;->a_(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public b(Lmaps/cr/c;)V
    .locals 4

    new-instance v0, Lmaps/q/ac;

    sget-object v1, Lmaps/y/am;->a:Lmaps/y/am;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmaps/q/ac;-><init>(Lmaps/y/am;Z)V

    iput-object v0, p0, Lmaps/y/bk;->p:Lmaps/q/ac;

    iget-object v0, p0, Lmaps/y/bk;->p:Lmaps/q/ac;

    const-string v1, "Polygon"

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/y/bk;->p:Lmaps/q/ac;

    new-instance v1, Lmaps/q/n;

    const/4 v2, 0x1

    const/16 v3, 0x303

    invoke-direct {v1, v2, v3}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/y/bk;->p:Lmaps/q/ac;

    new-instance v1, Lmaps/q/bb;

    invoke-direct {v1}, Lmaps/q/bb;-><init>()V

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    new-instance v1, Lmaps/y/b;

    iget-object v2, p0, Lmaps/y/bk;->p:Lmaps/q/ac;

    iget-object v3, p0, Lmaps/y/bk;->d:Lmaps/t/ax;

    invoke-direct {v1, v2, v0, v3}, Lmaps/y/b;-><init>(Lmaps/q/ac;Lmaps/q/ad;Lmaps/t/ax;)V

    iput-object v1, p0, Lmaps/y/bk;->q:Lmaps/y/b;

    iget-object v1, p0, Lmaps/y/bk;->q:Lmaps/y/b;

    iget v2, p0, Lmaps/y/bk;->s:I

    invoke-virtual {v1, v2}, Lmaps/y/b;->a(I)V

    iget-object v1, p0, Lmaps/y/bk;->q:Lmaps/y/b;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/x;)V

    iget-object v0, p0, Lmaps/y/bk;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/a;

    invoke-virtual {v0, p1}, Lmaps/y/a;->b(Lmaps/cr/c;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 3

    iget-object v1, p0, Lmaps/y/bk;->t:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lmaps/y/bk;->s:I

    invoke-static {v0}, Lmaps/y/bk;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lmaps/y/bk;->f(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v2, p0, Lmaps/y/bk;->f:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {}, Lmaps/ay/e;->d()Lmaps/ay/e;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bk;->g:Lmaps/ay/e;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/bk;->m:Z

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lmaps/y/bk;->l:Z

    :cond_0
    iput p1, p0, Lmaps/y/bk;->s:I

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/bk;->q:Lmaps/y/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/bk;->q:Lmaps/y/b;

    invoke-virtual {v0, p1}, Lmaps/y/b;->a(I)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/bk;->p:Lmaps/q/ac;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/bk;->p:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    iget-object v1, p0, Lmaps/y/bk;->q:Lmaps/y/b;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/x;)V

    iput-object v2, p0, Lmaps/y/bk;->p:Lmaps/q/ac;

    iput-object v2, p0, Lmaps/y/bk;->q:Lmaps/y/b;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/y/bk;->n:Lmaps/al/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/bk;->n:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/y/bk;->o:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    goto :goto_0
.end method

.method public d(I)V
    .locals 4

    iput p1, p0, Lmaps/y/bk;->v:I

    iget-object v1, p0, Lmaps/y/bk;->w:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/y/bk;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/a;

    int-to-float v3, p1

    invoke-virtual {v0, v3}, Lmaps/y/a;->a(F)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
