.class public Lmaps/y/aw;
.super Lmaps/y/bc;


# static fields
.field private static k:Lmaps/al/o;

.field private static l:Lmaps/al/i;

.field private static m:Lmaps/al/o;

.field private static n:Lmaps/al/i;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/List;

.field private c:Lmaps/y/w;

.field private d:Lmaps/t/bx;

.field private e:Lmaps/t/bg;

.field private f:Lmaps/t/ax;

.field private g:I

.field private h:F

.field private i:I

.field private j:I

.field private o:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x64

    new-instance v0, Lmaps/al/o;

    invoke-direct {v0, v1}, Lmaps/al/o;-><init>(I)V

    sput-object v0, Lmaps/y/aw;->k:Lmaps/al/o;

    new-instance v0, Lmaps/al/i;

    invoke-direct {v0, v1}, Lmaps/al/i;-><init>(I)V

    sput-object v0, Lmaps/y/aw;->l:Lmaps/al/i;

    new-instance v0, Lmaps/al/o;

    const/16 v1, 0x65

    invoke-direct {v0, v1}, Lmaps/al/o;-><init>(I)V

    sput-object v0, Lmaps/y/aw;->m:Lmaps/al/o;

    new-instance v0, Lmaps/al/i;

    const/16 v1, 0x66

    invoke-direct {v0, v1}, Lmaps/al/i;-><init>(I)V

    sput-object v0, Lmaps/y/aw;->n:Lmaps/al/i;

    sget-object v0, Lmaps/y/aw;->k:Lmaps/al/o;

    sget-object v1, Lmaps/y/aw;->l:Lmaps/al/i;

    invoke-static {v0, v1}, Lmaps/s/s;->a(Lmaps/al/l;Lmaps/al/b;)V

    sget-object v0, Lmaps/y/aw;->m:Lmaps/al/o;

    sget-object v1, Lmaps/y/aw;->n:Lmaps/al/i;

    invoke-static {v0, v1}, Lmaps/s/s;->b(Lmaps/al/l;Lmaps/al/b;)V

    return-void
.end method

.method public constructor <init>(Lmaps/t/bx;IIILmaps/t/bg;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    iput-object p1, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    iput p2, p0, Lmaps/y/aw;->g:I

    invoke-direct {p0}, Lmaps/y/aw;->e()V

    iput p3, p0, Lmaps/y/aw;->i:I

    iput p4, p0, Lmaps/y/aw;->j:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lmaps/y/aw;->o:F

    iput-object p5, p0, Lmaps/y/aw;->e:Lmaps/t/bg;

    iput-object p6, p0, Lmaps/y/aw;->a:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(ILmaps/t/bx;)F
    .locals 1

    invoke-static {p0, p1}, Lmaps/y/aw;->b(ILmaps/t/bx;)F

    move-result v0

    return v0
.end method

.method private a(Lmaps/cr/c;F)V
    .locals 3

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, p2, p2, p2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    sget-object v1, Lmaps/y/aw;->m:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget v1, p0, Lmaps/y/aw;->j:I

    invoke-static {v0, v1}, Lmaps/s/k;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    sget-object v1, Lmaps/y/aw;->n:Lmaps/al/i;

    const/4 v2, 0x6

    invoke-virtual {v1, p1, v2}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    sget-object v1, Lmaps/y/aw;->k:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget v1, p0, Lmaps/y/aw;->i:I

    invoke-static {v0, v1}, Lmaps/s/k;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget v1, p0, Lmaps/y/aw;->o:F

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    sget-object v0, Lmaps/y/aw;->l:Lmaps/al/i;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    return-void
.end method

.method private static b(ILmaps/t/bx;)F
    .locals 3

    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    int-to-float v0, p0

    invoke-virtual {p1}, Lmaps/t/bx;->e()D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method private e()V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/y/aw;->g:I

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    invoke-static {v0, v1}, Lmaps/y/aw;->b(ILmaps/t/bx;)F

    move-result v0

    iput v0, p0, Lmaps/y/aw;->h:F

    iget-object v0, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    iget v1, p0, Lmaps/y/aw;->h:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lmaps/t/ax;->a(Lmaps/t/bx;I)Lmaps/t/ax;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/aw;->f:Lmaps/t/ax;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 0

    iput p1, p0, Lmaps/y/aw;->o:F

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 4

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/y/aw;->h:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/u;->a()Lmaps/t/bw;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/aw;->f:Lmaps/t/ax;

    invoke-virtual {v0, v1}, Lmaps/t/bw;->b(Lmaps/t/an;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lmaps/y/aw;->e:Lmaps/t/bg;

    if-eqz v1, :cond_2

    invoke-static {}, Lmaps/b/r;->a()Lmaps/b/r;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/aw;->e:Lmaps/t/bg;

    invoke-virtual {v1, v2}, Lmaps/b/r;->e(Lmaps/t/bg;)Lmaps/b/z;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    iget-object v3, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    invoke-virtual {v1, p2, v3}, Lmaps/b/z;->a(Lmaps/bq/d;Lmaps/t/bx;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2, v1}, Lmaps/t/bx;->b(I)V

    :cond_2
    invoke-virtual {p2}, Lmaps/bq/d;->z()F

    move-result v1

    iget-object v2, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    invoke-static {p1, p2, v2, v1}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    iget v2, p0, Lmaps/y/aw;->h:F

    div-float v1, v2, v1

    invoke-direct {p0, p1, v1}, Lmaps/y/aw;->a(Lmaps/cr/c;F)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public a(Lmaps/t/bx;I)V
    .locals 3

    iget-object v0, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    if-ne p1, v0, :cond_0

    iget v0, p0, Lmaps/y/aw;->g:I

    if-eq v0, p2, :cond_1

    :cond_0
    iput-object p1, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    iput p2, p0, Lmaps/y/aw;->g:I

    invoke-direct {p0}, Lmaps/y/aw;->e()V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/aw;->c:Lmaps/y/w;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/aw;->c:Lmaps/y/w;

    iget-object v1, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    iget v2, p0, Lmaps/y/aw;->g:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Lmaps/y/w;->a(Lmaps/t/bx;I)V

    :cond_1
    return-void
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/aw;->c:Lmaps/y/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/aw;->c:Lmaps/y/w;

    invoke-virtual {v0, p1}, Lmaps/y/w;->a(Lmaps/bq/d;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmaps/y/aw;->c(Lmaps/cr/c;)V

    return-void
.end method

.method public b()Lmaps/y/am;
    .locals 1

    sget-object v0, Lmaps/y/am;->a:Lmaps/y/am;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lmaps/y/aw;->i:I

    return-void
.end method

.method public b(Lmaps/cr/c;)V
    .locals 7

    const/16 v6, 0x303

    const/16 v5, 0x302

    const/16 v4, 0x8

    new-instance v0, Lmaps/q/ba;

    invoke-direct {v0}, Lmaps/q/ba;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "accuracy circle outline "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/aw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/ba;->a(Ljava/lang/String;)V

    new-instance v1, Lmaps/q/bb;

    iget v2, p0, Lmaps/y/aw;->i:I

    invoke-direct {v1, v2}, Lmaps/q/bb;-><init>(I)V

    invoke-virtual {v0, v1, v4}, Lmaps/q/ba;->a(Lmaps/q/z;I)V

    new-instance v1, Lmaps/q/n;

    invoke-direct {v1, v5, v6}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v0, v1, v4}, Lmaps/q/ba;->a(Lmaps/q/z;I)V

    invoke-virtual {p1}, Lmaps/cr/c;->n()Lmaps/s/s;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lmaps/s/s;->a(I)Lmaps/q/r;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lmaps/q/ba;->a(Lmaps/q/r;I)V

    new-instance v1, Lmaps/q/ba;

    invoke-direct {v1}, Lmaps/q/ba;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "accuracy circle fill "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/y/aw;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/q/ba;->a(Ljava/lang/String;)V

    new-instance v2, Lmaps/q/bb;

    iget v3, p0, Lmaps/y/aw;->j:I

    invoke-direct {v2, v3}, Lmaps/q/bb;-><init>(I)V

    invoke-virtual {v1, v2, v4}, Lmaps/q/ba;->a(Lmaps/q/z;I)V

    new-instance v2, Lmaps/q/n;

    invoke-direct {v2, v5, v6}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v1, v2, v4}, Lmaps/q/ba;->a(Lmaps/q/z;I)V

    invoke-virtual {p1}, Lmaps/cr/c;->n()Lmaps/s/s;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lmaps/s/s;->a(I)Lmaps/q/r;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lmaps/q/ba;->a(Lmaps/q/r;I)V

    const/4 v2, 0x2

    new-array v2, v2, [Lmaps/q/ba;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object v1, v2, v0

    invoke-static {v2}, Lmaps/f/fd;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/aw;->b:Ljava/util/List;

    new-instance v0, Lmaps/y/w;

    iget-object v1, p0, Lmaps/y/aw;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Lmaps/y/w;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lmaps/y/aw;->c:Lmaps/y/w;

    iget-object v0, p0, Lmaps/y/aw;->c:Lmaps/y/w;

    iget-object v1, p0, Lmaps/y/aw;->d:Lmaps/t/bx;

    iget v2, p0, Lmaps/y/aw;->g:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Lmaps/y/w;->a(Lmaps/t/bx;I)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/aw;->c:Lmaps/y/w;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/x;)V

    iget-object v0, p0, Lmaps/y/aw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/ba;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/q/ad;->a(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Lmaps/y/aw;->j:I

    return-void
.end method

.method public c(Lmaps/cr/c;)V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/aw;->b:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/aw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/ba;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/aw;->c:Lmaps/y/w;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/x;)V

    iput-object v3, p0, Lmaps/y/aw;->b:Ljava/util/List;

    iput-object v3, p0, Lmaps/y/aw;->c:Lmaps/y/w;

    :cond_1
    return-void
.end method
