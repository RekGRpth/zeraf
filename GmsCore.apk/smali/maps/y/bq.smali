.class Lmaps/y/bq;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/cq/b;


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Lmaps/cq/b;

.field private final c:I

.field private d:Lmaps/an/i;

.field private e:Lmaps/b/u;

.field private final f:Lmaps/b/r;


# direct methods
.method public constructor <init>(Lmaps/cq/b;ILandroid/content/Context;Lmaps/b/r;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lmaps/y/bq;->a:Landroid/content/Context;

    iput-object p1, p0, Lmaps/y/bq;->b:Lmaps/cq/b;

    iput p2, p0, Lmaps/y/bq;->c:I

    iput-object p4, p0, Lmaps/y/bq;->f:Lmaps/b/r;

    return-void
.end method


# virtual methods
.method public a(Lmaps/o/c;ZLmaps/o/l;)Lmaps/bu/c;
    .locals 1

    new-instance v0, Lmaps/bu/c;

    invoke-direct {v0, p1, p3}, Lmaps/bu/c;-><init>(Lmaps/o/c;Lmaps/o/l;)V

    return-object v0
.end method

.method public a(Lmaps/o/c;IZLmaps/o/l;)Lmaps/cq/a;
    .locals 6

    iget-object v0, p0, Lmaps/y/bq;->b:Lmaps/cq/b;

    invoke-interface {v0, p1, p2, p3, p4}, Lmaps/cq/b;->a(Lmaps/o/c;IZLmaps/o/l;)Lmaps/cq/a;

    move-result-object v1

    iget-object v0, p0, Lmaps/y/bq;->d:Lmaps/an/i;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/an/i;->b()Lmaps/an/i;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bq;->d:Lmaps/an/i;

    :cond_0
    iget-object v0, p0, Lmaps/y/bq;->e:Lmaps/b/u;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/y/bq;->f:Lmaps/b/r;

    invoke-virtual {v0}, Lmaps/b/r;->j()Lmaps/b/u;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/bq;->e:Lmaps/b/u;

    :cond_1
    new-instance v0, Lmaps/bu/a;

    iget-object v2, p0, Lmaps/y/bq;->e:Lmaps/b/u;

    iget-object v3, p0, Lmaps/y/bq;->d:Lmaps/an/i;

    iget v4, p0, Lmaps/y/bq;->c:I

    iget-object v5, p0, Lmaps/y/bq;->f:Lmaps/b/r;

    invoke-direct/range {v0 .. v5}, Lmaps/bu/a;-><init>(Lmaps/cq/a;Lmaps/b/u;Lmaps/an/i;ILmaps/b/r;)V

    return-object v0
.end method
