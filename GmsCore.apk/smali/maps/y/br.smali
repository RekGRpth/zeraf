.class Lmaps/y/br;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/x;


# static fields
.field static final a:Lmaps/q/bf;


# instance fields
.field final b:Lmaps/q/ac;

.field final c:Ljava/util/List;

.field final d:Ljava/lang/ref/WeakReference;

.field final e:Landroid/content/res/Resources;

.field f:Lmaps/q/i;

.field g:Lmaps/bq/d;

.field final h:Lmaps/w/o;

.field i:Z

.field final j:Lmaps/t/aj;

.field k:Lmaps/y/bl;

.field l:I

.field final m:Ljava/util/Map;

.field n:F

.field o:F

.field p:F

.field q:F

.field r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lmaps/q/bf;

    const/high16 v1, 0x3f800000

    invoke-direct {v0, v2, v2, v1}, Lmaps/q/bf;-><init>(FFF)V

    sput-object v0, Lmaps/y/br;->a:Lmaps/q/bf;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lmaps/q/ac;Lmaps/cr/c;Landroid/content/res/Resources;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean v0, Lmaps/ae/h;->x:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/w/n;

    invoke-direct {v0}, Lmaps/w/n;-><init>()V

    :goto_0
    iput-object v0, p0, Lmaps/y/br;->h:Lmaps/w/o;

    new-instance v0, Lmaps/t/aj;

    invoke-direct {v0}, Lmaps/t/aj;-><init>()V

    iput-object v0, p0, Lmaps/y/br;->j:Lmaps/t/aj;

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/br;->m:Ljava/util/Map;

    iput-object p1, p0, Lmaps/y/br;->c:Ljava/util/List;

    iput-object p2, p0, Lmaps/y/br;->b:Lmaps/q/ac;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/y/br;->d:Ljava/lang/ref/WeakReference;

    iput-object p4, p0, Lmaps/y/br;->e:Landroid/content/res/Resources;

    return-void

    :cond_0
    new-instance v0, Lmaps/w/i;

    invoke-direct {v0}, Lmaps/w/i;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method declared-synchronized a(Lmaps/cr/c;I)Lmaps/cr/a;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/br;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/a;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    new-instance v0, Lmaps/cr/a;

    invoke-direct {v0, p1, v1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/cr/a;->c(Z)V

    iget-object v1, p0, Lmaps/y/br;->e:Landroid/content/res/Resources;

    invoke-virtual {v0, v1, p2}, Lmaps/cr/a;->a(Landroid/content/res/Resources;I)V

    iget-object v1, p0, Lmaps/y/br;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method declared-synchronized a(FFF)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/y/br;->n:F

    iput p2, p0, Lmaps/y/br;->o:F

    iput p3, p0, Lmaps/y/br;->p:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Lmaps/bq/d;F)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/y/br;->g:Lmaps/bq/d;

    iput p2, p0, Lmaps/y/br;->q:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/br;->i:Z

    iget-object v0, p0, Lmaps/y/br;->f:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/br;->f:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/q/i;)V
    .locals 1

    iput-object p1, p0, Lmaps/y/br;->f:Lmaps/q/i;

    sget-object v0, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {p1, p0, v0}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    return-void
.end method

.method declared-synchronized a(Lmaps/t/aj;Lmaps/y/bl;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/br;->h:Lmaps/w/o;

    invoke-interface {v0, p1}, Lmaps/w/o;->b(Lmaps/t/aj;)V

    iput-object p2, p0, Lmaps/y/br;->k:Lmaps/y/bl;

    iget-object v0, p0, Lmaps/y/br;->f:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/br;->f:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lmaps/q/i;)V
    .locals 8

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/br;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v4, p0, Lmaps/y/br;->h:Lmaps/w/o;

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v5

    invoke-interface {v4, v5, v6}, Lmaps/w/o;->a(J)I

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    iput-boolean v4, p0, Lmaps/y/br;->i:Z

    :cond_2
    iget-boolean v4, p0, Lmaps/y/br;->r:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lmaps/y/br;->k:Lmaps/y/bl;

    iget v4, v4, Lmaps/y/bl;->c:I

    iget-object v5, p0, Lmaps/y/br;->k:Lmaps/y/bl;

    iget v5, v5, Lmaps/y/bl;->b:I

    if-eq v4, v5, :cond_4

    :goto_1
    if-eqz v2, :cond_5

    iget-object v2, p0, Lmaps/y/br;->j:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->j()F

    move-result v2

    cmpl-float v2, v2, v1

    if-nez v2, :cond_5

    iget-object v2, p0, Lmaps/y/br;->k:Lmaps/y/bl;

    iget v2, v2, Lmaps/y/bl;->c:I

    :goto_2
    iget v3, p0, Lmaps/y/br;->l:I

    if-eq v3, v2, :cond_3

    iput v2, p0, Lmaps/y/br;->l:I

    const/4 v2, 0x1

    iput-boolean v2, p0, Lmaps/y/br;->i:Z

    :cond_3
    iget-boolean v2, p0, Lmaps/y/br;->i:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lmaps/y/br;->i:Z

    iget-object v2, p0, Lmaps/y/br;->b:Lmaps/q/ac;

    iget v3, p0, Lmaps/y/br;->l:I

    invoke-virtual {p0, v0, v3}, Lmaps/y/br;->a(Lmaps/cr/c;I)Lmaps/cr/a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/y/br;->h:Lmaps/w/o;

    iget-object v2, p0, Lmaps/y/br;->j:Lmaps/t/aj;

    invoke-interface {v0, v2}, Lmaps/w/o;->a(Lmaps/t/aj;)Z

    iget-object v0, p0, Lmaps/y/br;->j:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v3

    iget-object v0, p0, Lmaps/y/br;->g:Lmaps/bq/d;

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    iget-object v0, p0, Lmaps/y/br;->j:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->c()I

    move-result v0

    int-to-double v4, v0

    invoke-virtual {v3}, Lmaps/t/bx;->e()D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v2, v4

    iget-object v0, p0, Lmaps/y/br;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/ac;

    invoke-virtual {v0, v3, v2}, Lmaps/q/ac;->b(Lmaps/t/bx;F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v2, v3

    goto :goto_1

    :cond_5
    :try_start_2
    iget-object v2, p0, Lmaps/y/br;->k:Lmaps/y/bl;

    iget v2, v2, Lmaps/y/bl;->b:I

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lmaps/y/br;->k:Lmaps/y/bl;

    iget-boolean v0, v0, Lmaps/y/bl;->a:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lmaps/y/br;->o:F

    move v2, v0

    :goto_4
    iget v4, p0, Lmaps/y/br;->q:F

    iget-object v0, p0, Lmaps/y/br;->k:Lmaps/y/bl;

    iget-boolean v0, v0, Lmaps/y/bl;->a:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_5
    mul-float/2addr v0, v4

    mul-float/2addr v0, v2

    iget-object v1, p0, Lmaps/y/br;->b:Lmaps/q/ac;

    invoke-virtual {v1, v3, v0}, Lmaps/q/ac;->a(Lmaps/t/bx;F)V

    iget-object v0, p0, Lmaps/y/br;->k:Lmaps/y/bl;

    iget-boolean v0, v0, Lmaps/y/bl;->a:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmaps/y/br;->b:Lmaps/q/ac;

    iget-object v1, p0, Lmaps/y/br;->j:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->b()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(F)V

    :cond_7
    iget-object v0, p0, Lmaps/y/br;->f:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/br;->f:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    goto/16 :goto_0

    :cond_8
    iget v0, p0, Lmaps/y/br;->n:F

    move v2, v0

    goto :goto_4

    :cond_9
    iget v0, p0, Lmaps/y/br;->p:F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5
.end method
