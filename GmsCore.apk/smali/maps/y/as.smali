.class public Lmaps/y/as;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Z

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x73217bce

    iput v0, p0, Lmaps/y/as;->i:I

    const v0, 0x338cc6ef

    iput v0, p0, Lmaps/y/as;->j:I

    return-void
.end method


# virtual methods
.method public a()Lmaps/y/as;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/as;->h:Z

    return-object p0
.end method

.method public a(I)Lmaps/y/as;
    .locals 1

    invoke-virtual {p0, p1, p1}, Lmaps/y/as;->a(II)Lmaps/y/as;

    move-result-object v0

    return-object v0
.end method

.method public a(II)Lmaps/y/as;
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/as;->d:Ljava/lang/Integer;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/as;->e:Ljava/lang/Integer;

    return-object p0
.end method

.method public a(Z)Lmaps/y/as;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/as;->a:Ljava/lang/Boolean;

    return-object p0
.end method

.method public b()Lmaps/y/as;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/as;->h:Z

    return-object p0
.end method

.method public b(II)Lmaps/y/as;
    .locals 0

    iput p1, p0, Lmaps/y/as;->i:I

    iput p2, p0, Lmaps/y/as;->j:I

    return-object p0
.end method

.method public b(Z)Lmaps/y/as;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/as;->b:Ljava/lang/Boolean;

    return-object p0
.end method

.method public c(Z)Lmaps/y/as;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/as;->c:Ljava/lang/Boolean;

    return-object p0
.end method

.method public c()Lmaps/y/bl;
    .locals 11

    const/4 v8, 0x0

    iget-object v0, p0, Lmaps/y/as;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Texture ID must be specified."

    invoke-static {v0, v1}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    new-instance v0, Lmaps/y/bl;

    iget-object v1, p0, Lmaps/y/as;->a:Ljava/lang/Boolean;

    iget-object v2, p0, Lmaps/y/as;->b:Ljava/lang/Boolean;

    iget-object v3, p0, Lmaps/y/as;->c:Ljava/lang/Boolean;

    iget-boolean v4, p0, Lmaps/y/as;->h:Z

    iget-object v5, p0, Lmaps/y/as;->d:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lmaps/y/as;->e:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, p0, Lmaps/y/as;->f:Ljava/lang/Integer;

    if-nez v7, :cond_1

    move v7, v8

    :goto_1
    iget-object v9, p0, Lmaps/y/as;->g:Ljava/lang/Integer;

    if-nez v9, :cond_2

    :goto_2
    iget v9, p0, Lmaps/y/as;->i:I

    iget v10, p0, Lmaps/y/as;->j:I

    invoke-direct/range {v0 .. v10}, Lmaps/y/bl;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;ZIIIIII)V

    return-object v0

    :cond_0
    move v0, v8

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lmaps/y/as;->f:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    goto :goto_1

    :cond_2
    iget-object v8, p0, Lmaps/y/as;->g:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto :goto_2
.end method
