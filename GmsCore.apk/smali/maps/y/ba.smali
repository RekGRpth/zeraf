.class public abstract Lmaps/y/ba;
.super Lmaps/y/bc;


# instance fields
.field private a:Lmaps/y/bn;

.field protected final c:Lmaps/y/f;

.field protected d:Z

.field protected e:I


# direct methods
.method public constructor <init>(Lmaps/y/f;)V
    .locals 0

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    iput-object p1, p0, Lmaps/y/ba;->c:Lmaps/y/f;

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/util/List;FFLmaps/t/bx;Lmaps/bq/d;I)V
.end method

.method protected a(Lmaps/y/bh;)V
    .locals 1

    invoke-virtual {p0}, Lmaps/y/ba;->f()V

    iget-object v0, p0, Lmaps/y/ba;->a:Lmaps/y/bn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/ba;->a:Lmaps/y/bn;

    invoke-interface {v0, p0, p1}, Lmaps/y/bn;->a(Lmaps/y/ba;Lmaps/y/bh;)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/y/bn;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/ba;->a:Lmaps/y/bn;

    return-void
.end method

.method public f_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public n()I
    .locals 2

    invoke-virtual {p0}, Lmaps/y/ba;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lmaps/y/ba;->e:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/y/ba;->d:Z

    return v0
.end method
