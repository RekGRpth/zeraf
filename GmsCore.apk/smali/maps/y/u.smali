.class public Lmaps/y/u;
.super Lmaps/y/bc;


# instance fields
.field private a:Landroid/content/res/Resources;

.field private b:Lmaps/cr/a;

.field private c:Lmaps/t/bx;

.field private d:F

.field private e:I

.field private f:I

.field private final g:I

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lmaps/w/v;

.field private volatile m:I

.field private final n:I

.field private final o:Lmaps/y/bp;

.field private p:Lmaps/y/ao;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lmaps/y/bp;)V
    .locals 1

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/u;->j:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/u;->k:Z

    const/high16 v0, 0x10000

    iput v0, p0, Lmaps/y/u;->m:I

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/y/u;->p:Lmaps/y/ao;

    iput-object p1, p0, Lmaps/y/u;->a:Landroid/content/res/Resources;

    iput-object p2, p0, Lmaps/y/u;->o:Lmaps/y/bp;

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lmaps/ad/b;->h:I

    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lmaps/y/u;->g:I

    sget v0, Lmaps/ad/a;->H:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lmaps/y/u;->n:I

    return-void

    :cond_0
    sget v0, Lmaps/ad/b;->g:I

    goto :goto_0
.end method

.method private a(FF)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    iget v2, p0, Lmaps/y/u;->m:I

    if-nez v2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lmaps/y/u;->f:I

    int-to-float v2, v2

    sub-float v2, p2, v2

    sget-object v3, Lmaps/y/j;->a:[I

    iget-object v4, p0, Lmaps/y/u;->o:Lmaps/y/bp;

    invoke-virtual {v4}, Lmaps/y/bp;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    iget v3, p0, Lmaps/y/u;->e:I

    int-to-float v3, v3

    add-float/2addr v3, p1

    iget v4, p0, Lmaps/y/u;->h:I

    iget v5, p0, Lmaps/y/u;->g:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_2

    iget v4, p0, Lmaps/y/u;->h:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_2

    cmpl-float v3, v2, v6

    if-ltz v3, :cond_2

    iget v3, p0, Lmaps/y/u;->g:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-lez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :pswitch_0
    iget v3, p0, Lmaps/y/u;->e:I

    int-to-float v3, v3

    sub-float v3, p1, v3

    cmpl-float v4, v3, v6

    if-ltz v4, :cond_3

    iget v4, p0, Lmaps/y/u;->g:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_3

    cmpl-float v3, v2, v6

    if-ltz v3, :cond_3

    iget v3, p0, Lmaps/y/u;->g:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-lez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private e()Z
    .locals 2

    const/4 v0, 0x0

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/y/u;->p:Lmaps/y/ao;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lmaps/y/u;->p:Lmaps/y/ao;

    invoke-virtual {v0}, Lmaps/y/ao;->b()Z

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lmaps/y/u;->l:Lmaps/w/v;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 8

    const/4 v5, 0x1

    const/high16 v7, 0x420c0000

    const/high16 v1, 0x3f800000

    const/4 v6, 0x0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lmaps/y/u;->c:Lmaps/t/bx;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/y/u;->m:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    if-nez v0, :cond_2

    new-instance v0, Lmaps/cr/a;

    invoke-direct {v0, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    iput-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    iget-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    invoke-virtual {v0, v5}, Lmaps/cr/a;->c(Z)V

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/b;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lmaps/ad/e;->q:I

    :goto_1
    iget-object v2, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    iget-object v3, p0, Lmaps/y/u;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v3, v0}, Lmaps/cr/a;->a(Landroid/content/res/Resources;I)V

    :cond_2
    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    const/16 v0, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x2100

    invoke-interface {v2, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const/16 v0, 0x303

    invoke-interface {v2, v5, v0}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    iget-object v0, p1, Lmaps/cr/c;->d:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    invoke-direct {p0}, Lmaps/y/u;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/u;->l:Lmaps/w/v;

    invoke-virtual {v0, p1}, Lmaps/w/v;->a(Lmaps/cr/c;)I

    move-result v0

    iput v0, p0, Lmaps/y/u;->m:I

    iget v0, p0, Lmaps/y/u;->m:I

    if-nez v0, :cond_3

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/y/u;->l:Lmaps/w/v;

    :cond_3
    iget-boolean v0, p0, Lmaps/y/u;->i:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lmaps/y/u;->n:I

    invoke-static {v2, v0}, Lmaps/y/u;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    :goto_2
    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v3, p0, Lmaps/y/u;->d:F

    iget-boolean v0, p0, Lmaps/y/u;->i:Z

    if-eqz v0, :cond_7

    const v0, 0x3faa3d71

    :goto_3
    mul-float/2addr v0, v3

    iget-object v3, p0, Lmaps/y/u;->c:Lmaps/t/bx;

    invoke-static {p1, p2, v3, v0}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    invoke-virtual {p2}, Lmaps/bq/d;->r()F

    move-result v0

    cmpl-float v3, v0, v7

    if-lez v3, :cond_4

    invoke-virtual {p2}, Lmaps/bq/d;->q()F

    move-result v3

    neg-float v3, v3

    invoke-interface {v2, v3, v6, v6, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    sub-float/2addr v0, v7

    invoke-interface {v2, v0, v1, v6, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    invoke-virtual {p2}, Lmaps/bq/d;->q()F

    move-result v0

    invoke-interface {v2, v0, v6, v6, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :cond_4
    iget-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    invoke-virtual {v0, v2}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p1, Lmaps/cr/c;->h:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v3, 0x4

    invoke-interface {v2, v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0

    :cond_5
    sget v0, Lmaps/ad/e;->p:I

    goto/16 :goto_1

    :cond_6
    iget v0, p0, Lmaps/y/u;->m:I

    iget v3, p0, Lmaps/y/u;->m:I

    iget v4, p0, Lmaps/y/u;->m:I

    iget v5, p0, Lmaps/y/u;->m:I

    invoke-interface {v2, v0, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method public a(FFLmaps/bq/d;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2}, Lmaps/y/u;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_1

    iput-boolean v0, p0, Lmaps/y/u;->i:Z

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/y/u;->p:Lmaps/y/ao;

    invoke-virtual {v1, v0}, Lmaps/y/ao;->a(Z)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lmaps/y/u;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/y/u;->f()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget v0, p0, Lmaps/y/u;->g:I

    div-int/lit8 v0, v0, 0x2

    sget-object v1, Lmaps/y/j;->a:[I

    iget-object v2, p0, Lmaps/y/u;->o:Lmaps/y/bp;

    invoke-virtual {v2}, Lmaps/y/bp;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v1

    iget v2, p0, Lmaps/y/u;->e:I

    add-int/2addr v2, v0

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lmaps/y/u;->f:I

    add-int/2addr v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v1

    iput-object v1, p0, Lmaps/y/u;->c:Lmaps/t/bx;

    :goto_0
    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v1

    iput v1, p0, Lmaps/y/u;->h:I

    iget-object v1, p0, Lmaps/y/u;->c:Lmaps/t/bx;

    if-eqz v1, :cond_0

    int-to-float v0, v0

    iget-object v1, p0, Lmaps/y/u;->c:Lmaps/t/bx;

    invoke-virtual {p1, v1, v3}, Lmaps/bq/d;->a(Lmaps/t/bx;Z)F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmaps/bq/d;->a(FF)F

    move-result v0

    iput v0, p0, Lmaps/y/u;->d:F

    :cond_0
    iget-boolean v0, p0, Lmaps/y/u;->j:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lmaps/bq/d;->q()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lmaps/bq/d;->r()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lmaps/y/u;->k:Z

    if-nez v0, :cond_1

    iput-boolean v6, p0, Lmaps/y/u;->k:Z

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/u;->p:Lmaps/y/ao;

    const/16 v1, 0x7d0

    const/16 v2, 0x1f4

    sget-object v3, Lmaps/w/q;->b:Lmaps/w/q;

    invoke-virtual {v0, v1, v2, v3}, Lmaps/y/ao;->a(IILmaps/w/q;)V

    :cond_1
    :goto_1
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    new-array v1, v0, [F

    iget v2, p0, Lmaps/y/u;->d:F

    iget-boolean v0, p0, Lmaps/y/u;->i:Z

    if-eqz v0, :cond_6

    const v0, 0x3faa3d71

    :goto_2
    mul-float/2addr v0, v2

    iget-object v2, p0, Lmaps/y/u;->c:Lmaps/t/bx;

    invoke-static {v7, p1, v2, v0, v1}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V

    iget-object v0, p0, Lmaps/y/u;->p:Lmaps/y/ao;

    const/4 v2, 0x3

    aget v2, v1, v2

    invoke-virtual {v0, v1, v2}, Lmaps/y/ao;->a([FF)V

    :cond_2
    return v6

    :pswitch_0
    iget v1, p0, Lmaps/y/u;->e:I

    add-int/2addr v1, v0

    int-to-float v1, v1

    iget v2, p0, Lmaps/y/u;->f:I

    add-int/2addr v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v1

    iput-object v1, p0, Lmaps/y/u;->c:Lmaps/t/bx;

    goto :goto_0

    :cond_3
    new-instance v0, Lmaps/w/v;

    const-wide/16 v1, 0x7d0

    const-wide/16 v3, 0x1f4

    sget-object v5, Lmaps/w/j;->b:Lmaps/w/j;

    invoke-direct/range {v0 .. v5}, Lmaps/w/v;-><init>(JJLmaps/w/j;)V

    iput-object v0, p0, Lmaps/y/u;->l:Lmaps/w/v;

    goto :goto_1

    :cond_4
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lmaps/y/u;->k:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/y/u;->p:Lmaps/y/ao;

    const/16 v1, 0x64

    sget-object v2, Lmaps/w/q;->a:Lmaps/w/q;

    invoke-virtual {v0, v3, v1, v2}, Lmaps/y/ao;->a(IILmaps/w/q;)V

    :cond_5
    iput-object v7, p0, Lmaps/y/u;->l:Lmaps/w/v;

    iput-boolean v3, p0, Lmaps/y/u;->k:Z

    const/high16 v0, 0x10000

    iput v0, p0, Lmaps/y/u;->m:I

    goto :goto_1

    :cond_6
    const/high16 v0, 0x3f800000

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a_()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/y/u;->i:Z

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/u;->p:Lmaps/y/ao;

    invoke-virtual {v0, v1}, Lmaps/y/ao;->a(Z)V

    :cond_0
    return-void
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    :cond_0
    return-void
.end method

.method public b()Lmaps/y/am;
    .locals 1

    sget-object v0, Lmaps/y/am;->y:Lmaps/y/am;

    return-object v0
.end method

.method public b(Lmaps/cr/c;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v1, Lmaps/q/aq;

    sget-object v0, Lmaps/y/am;->B:Lmaps/y/am;

    invoke-direct {v1, v0}, Lmaps/q/aq;-><init>(Lmaps/y/am;)V

    const-string v0, "Compass needle"

    invoke-virtual {v1, v0}, Lmaps/q/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/cr/a;

    invoke-direct {v0, p1, v4}, Lmaps/cr/a;-><init>(Lmaps/cr/c;Z)V

    iput-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    iget-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    invoke-virtual {v0, v4}, Lmaps/cr/a;->c(Z)V

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/b;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lmaps/ad/e;->q:I

    :goto_0
    iget-object v2, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    iget-object v3, p0, Lmaps/y/u;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v3, v0}, Lmaps/cr/a;->a(Landroid/content/res/Resources;I)V

    :cond_0
    iget-object v0, p0, Lmaps/y/u;->b:Lmaps/cr/a;

    invoke-virtual {v1, v0}, Lmaps/q/aq;->a(Lmaps/q/z;)V

    new-instance v0, Lmaps/q/n;

    const/16 v2, 0x303

    invoke-direct {v0, v4, v2}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v1, v0}, Lmaps/q/aq;->a(Lmaps/q/z;)V

    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    new-instance v2, Lmaps/q/ar;

    invoke-direct {v2, v5, v0}, Lmaps/q/ar;-><init>(I[F)V

    invoke-virtual {v1, v2}, Lmaps/q/aq;->a(Lmaps/q/z;)V

    invoke-virtual {p1}, Lmaps/cr/c;->n()Lmaps/s/s;

    move-result-object v0

    invoke-virtual {v0, v5}, Lmaps/s/s;->a(I)Lmaps/q/r;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/q/aq;->a(Lmaps/q/r;)V

    new-instance v0, Lmaps/y/ao;

    iget v3, p0, Lmaps/y/u;->n:I

    invoke-direct {v0, v1, v2, v3}, Lmaps/y/ao;-><init>(Lmaps/q/ba;Lmaps/q/bg;I)V

    iput-object v0, p0, Lmaps/y/u;->p:Lmaps/y/ao;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v2, p0, Lmaps/y/u;->p:Lmaps/y/ao;

    invoke-virtual {v0, v2}, Lmaps/q/ad;->a(Lmaps/q/x;)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/ba;)V

    return-void

    :cond_1
    sget v0, Lmaps/ad/e;->p:I

    goto :goto_0

    :array_0
    .array-data 4
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
    .end array-data
.end method

.method public b_(Z)V
    .locals 1

    iput-boolean p1, p0, Lmaps/y/u;->j:Z

    iget-boolean v0, p0, Lmaps/y/u;->j:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmaps/y/u;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/y/u;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lmaps/y/u;->m:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/high16 v0, 0x10000

    iput v0, p0, Lmaps/y/u;->m:I

    goto :goto_0
.end method
