.class Lmaps/y/y;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/q/x;


# instance fields
.field a:Lmaps/q/i;

.field b:Lmaps/q/ad;

.field c:Lmaps/q/ac;

.field d:Z

.field e:Lmaps/s/r;

.field f:Lmaps/s/t;

.field g:Lmaps/bq/d;

.field h:F

.field i:I

.field j:Z

.field k:Lmaps/t/ax;

.field l:F

.field m:Z

.field final n:Lmaps/q/ar;

.field o:Ljava/util/List;

.field p:I


# direct methods
.method constructor <init>(Lmaps/q/ac;Lmaps/q/ad;I)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/y/y;->c:Lmaps/q/ac;

    iput-object p2, p0, Lmaps/y/y;->b:Lmaps/q/ad;

    sget-object v0, Lmaps/q/ap;->d:Lmaps/q/ap;

    invoke-virtual {p1, v0}, Lmaps/q/ac;->a(Lmaps/q/ap;)Lmaps/q/z;

    move-result-object v0

    check-cast v0, Lmaps/q/ar;

    iput-object v0, p0, Lmaps/y/y;->n:Lmaps/q/ar;

    iput p3, p0, Lmaps/y/y;->p:I

    new-instance v0, Lmaps/s/t;

    const/16 v1, 0x9

    const/4 v2, 0x1

    invoke-direct {v0, p3, v1, v2}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v0, p0, Lmaps/y/y;->f:Lmaps/s/t;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(F)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/y/y;->h:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/y;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/y/y;->i:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/y;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Lmaps/bq/d;Ljava/util/List;Lmaps/t/ax;Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/y/y;->g:Lmaps/bq/d;

    iput-object p2, p0, Lmaps/y/y;->o:Ljava/util/List;

    iput-object p3, p0, Lmaps/y/y;->k:Lmaps/t/ax;

    iput-boolean p4, p0, Lmaps/y/y;->m:Z

    iget-object v0, p0, Lmaps/y/y;->a:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/y;->a:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/q/i;)V
    .locals 2

    iput-object p1, p0, Lmaps/y/y;->a:Lmaps/q/i;

    iget-object v0, p0, Lmaps/y/y;->a:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V

    return-void
.end method

.method a(Lmaps/t/cg;Lmaps/bq/d;)V
    .locals 11

    iget-object v0, p0, Lmaps/y/y;->k:Lmaps/t/ax;

    invoke-virtual {v0}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v4

    iget-object v0, p0, Lmaps/y/y;->k:Lmaps/t/ax;

    invoke-virtual {v0}, Lmaps/t/ax;->g()I

    move-result v5

    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Lmaps/bq/d;->z()F

    move-result v0

    iget v1, p0, Lmaps/y/y;->h:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    mul-float/2addr v0, v1

    const/high16 v1, 0x40e00000

    div-float/2addr v0, v1

    const/high16 v1, 0x41000000

    mul-float v2, v0, v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v6, 0x0

    const/high16 v7, 0x10000

    invoke-static {}, Lmaps/s/q;->a()Lmaps/s/q;

    move-result-object v0

    iget-object v8, p0, Lmaps/y/y;->f:Lmaps/s/t;

    iget-object v9, p0, Lmaps/y/y;->f:Lmaps/s/t;

    iget-object v10, p0, Lmaps/y/y;->f:Lmaps/s/t;

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v0 .. v10}, Lmaps/s/q;->a(Lmaps/t/cg;FFLmaps/t/bx;IIILmaps/al/l;Lmaps/al/b;Lmaps/al/c;)V

    iget-object v0, p0, Lmaps/y/y;->f:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->d()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a(Lmaps/bq/d;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v4, 0x3fa00000

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lmaps/y/y;->m:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/y;->m:Z

    monitor-exit p0

    :goto_0
    return v1

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v2

    iget v3, p0, Lmaps/y/y;->l:F

    mul-float/2addr v3, v4

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_1

    iget v3, p0, Lmaps/y/y;->l:F

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method b(Lmaps/bq/d;)V
    .locals 2

    iget-object v0, p0, Lmaps/y/y;->f:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->h()V

    iget-object v0, p0, Lmaps/y/y;->f:Lmaps/s/t;

    iget v1, p0, Lmaps/y/y;->p:I

    invoke-virtual {v0, v1}, Lmaps/s/t;->d(I)V

    iget-object v0, p0, Lmaps/y/y;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cg;

    invoke-virtual {p0, v0, p1}, Lmaps/y/y;->a(Lmaps/t/cg;Lmaps/bq/d;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/y/y;->f:Lmaps/s/t;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lmaps/s/t;->f(I)Lmaps/s/r;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/y;->e:Lmaps/s/r;

    invoke-virtual {p1}, Lmaps/bq/d;->p()F

    move-result v0

    iput v0, p0, Lmaps/y/y;->l:F

    return-void
.end method

.method public declared-synchronized b(Lmaps/q/i;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/y;->k:Lmaps/t/ax;

    if-nez v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->h:Z

    if-eqz v0, :cond_0

    const-string v0, "GLColoredPolylineOverlay"

    const-string v1, "Commit() called before updateCamera()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/y/y;->g:Lmaps/bq/d;

    invoke-virtual {p0, v0}, Lmaps/y/y;->a(Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/y/y;->g:Lmaps/bq/d;

    invoke-virtual {p0, v0}, Lmaps/y/y;->b(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/y/y;->c:Lmaps/q/ac;

    iget-object v1, p0, Lmaps/y/y;->e:Lmaps/s/r;

    invoke-virtual {v0, v1}, Lmaps/q/ac;->a(Lmaps/q/r;)V

    iget-boolean v0, p0, Lmaps/y/y;->d:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/y/y;->b:Lmaps/q/ad;

    iget-object v1, p0, Lmaps/y/y;->c:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/ba;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/y;->d:Z

    :cond_2
    iget-boolean v0, p0, Lmaps/y/y;->j:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/y;->n:Lmaps/q/ar;

    iget v1, p0, Lmaps/y/y;->i:I

    invoke-virtual {v0, v1}, Lmaps/q/ar;->a(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/y;->j:Z

    :cond_3
    iget-object v0, p0, Lmaps/y/y;->c:Lmaps/q/ac;

    iget-object v1, p0, Lmaps/y/y;->k:Lmaps/t/ax;

    invoke-virtual {v1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/y;->k:Lmaps/t/ax;

    invoke-virtual {v2}, Lmaps/t/ax;->g()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lmaps/q/ac;->b(Lmaps/t/bx;F)V

    iget-object v0, p0, Lmaps/y/y;->a:Lmaps/q/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/y;->a:Lmaps/q/i;

    sget-object v1, Lmaps/q/ay;->b:Lmaps/q/ay;

    invoke-interface {v0, p0, v1}, Lmaps/q/i;->a(Lmaps/q/x;Lmaps/q/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
