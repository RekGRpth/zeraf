.class public Lmaps/y/k;
.super Lmaps/y/ba;


# instance fields
.field private final a:Ljava/util/LinkedList;

.field private b:Z

.field private final f:Ljava/util/HashMap;

.field private final g:Ljava/util/HashMap;

.field private h:Lmaps/t/u;

.field private i:Ljava/util/List;

.field private final j:Lmaps/y/am;

.field private k:I

.field private l:Z

.field private m:Lmaps/l/ay;

.field private n:Lmaps/y/al;

.field private o:Lmaps/y/ax;


# direct methods
.method public constructor <init>(Lmaps/y/am;Lmaps/y/f;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Lmaps/y/ba;-><init>(Lmaps/y/f;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/y/k;->a:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmaps/y/k;->f:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmaps/y/k;->g:Ljava/util/HashMap;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    iput v1, p0, Lmaps/y/k;->k:I

    iput-boolean v1, p0, Lmaps/y/k;->l:Z

    iput-object p1, p0, Lmaps/y/k;->j:Lmaps/y/am;

    return-void
.end method

.method static synthetic a(Lmaps/y/k;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    return-object v0
.end method

.method private a(Ljava/lang/RuntimeException;Ljava/util/Iterator;I)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "#:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmaps/y/k;->k:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lmaps/y/k;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " T:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " E:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " C:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " numM:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "GLMarkerOverlay"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lmaps/y/k;Lmaps/bq/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/y/k;->b(Lmaps/bq/d;)V

    return-void
.end method

.method private b(Lmaps/bq/d;)V
    .locals 5

    invoke-static {}, Lmaps/b/r;->a()Lmaps/b/r;

    move-result-object v1

    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    invoke-virtual {v0}, Lmaps/l/ay;->d_()Lmaps/t/bb;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmaps/b/r;->e(Lmaps/t/bg;)Lmaps/b/z;

    move-result-object v3

    if-eqz v3, :cond_0

    monitor-enter v0

    :try_start_0
    invoke-virtual {v0}, Lmaps/l/ay;->c_()Lmaps/t/bx;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lmaps/b/z;->a(Lmaps/bq/d;Lmaps/t/bx;)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v4, v3}, Lmaps/t/bx;->b(I)V

    invoke-virtual {v0, v4}, Lmaps/l/ay;->a(Lmaps/t/bx;)V

    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    return-void
.end method

.method static synthetic b(Lmaps/y/k;Lmaps/bq/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/y/k;->c(Lmaps/bq/d;)V

    return-void
.end method

.method private c(FFLmaps/bq/d;)V
    .locals 2

    const/high16 v0, 0x428c0000

    sub-float v0, p2, v0

    invoke-virtual {p3, p1, v0}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/k;->m:Lmaps/l/ay;

    invoke-virtual {v1, v0}, Lmaps/l/ay;->a(Lmaps/t/bx;)V

    return-void
.end method

.method private c(Lmaps/bq/d;)V
    .locals 2

    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    invoke-virtual {v0, p1}, Lmaps/l/ay;->b(Lmaps/bq/d;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method private d(Lmaps/l/ay;)V
    .locals 2

    invoke-virtual {p1}, Lmaps/l/ay;->q()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/k;->g:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/l/ay;->l()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1}, Lmaps/l/ay;->r()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/y/k;->g:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/l/ay;->m()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private e(Lmaps/l/ay;)V
    .locals 1

    iget-object v0, p0, Lmaps/y/k;->o:Lmaps/y/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/k;->o:Lmaps/y/ax;

    invoke-virtual {v0, p1}, Lmaps/y/ax;->a(Lmaps/l/ay;)V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    iget-object v2, p0, Lmaps/y/k;->o:Lmaps/y/ax;

    invoke-static {v2, v0}, Lmaps/y/ax;->a(Lmaps/y/ax;Lmaps/l/ay;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/k;->b:Z

    return-void
.end method

.method private declared-synchronized j()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/k;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    invoke-virtual {v0}, Lmaps/l/ay;->q()I

    invoke-virtual {v0}, Lmaps/l/ay;->r()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/y/k;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/util/List;FFLmaps/t/bx;Lmaps/bq/d;I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p5}, Lmaps/y/k;->a(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    invoke-interface {v0}, Lmaps/y/bh;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, p2, p3, p4, p5}, Lmaps/y/bh;->c(FFLmaps/t/bx;Lmaps/bq/d;)I

    move-result v2

    if-ge v2, p6, :cond_0

    new-instance v3, Lmaps/y/d;

    invoke-direct {v3, v0, p0, v2}, Lmaps/y/d;-><init>(Lmaps/y/bh;Lmaps/y/ba;I)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method a(Lmaps/bq/d;)V
    .locals 8

    const v7, -0x41b33333

    invoke-virtual {p1}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v1

    iget-boolean v0, p0, Lmaps/y/k;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/k;->h:Lmaps/t/u;

    invoke-virtual {v1, v0}, Lmaps/t/u;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    :goto_1
    invoke-virtual {v1}, Lmaps/t/u;->a()Lmaps/t/bw;

    move-result-object v2

    invoke-virtual {v1}, Lmaps/t/u;->d()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v0, v3, v7}, Lmaps/t/bx;->a(Lmaps/t/bx;F)Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v1}, Lmaps/t/u;->e()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lmaps/t/bx;->a(Lmaps/t/bx;F)Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v1}, Lmaps/t/u;->g()Lmaps/t/bx;

    move-result-object v4

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v4, v5, v7}, Lmaps/t/bx;->a(Lmaps/t/bx;F)Lmaps/t/bx;

    move-result-object v4

    invoke-virtual {v1}, Lmaps/t/u;->f()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {p1}, Lmaps/bq/d;->h()Lmaps/t/bx;

    move-result-object v6

    invoke-virtual {v5, v6, v7}, Lmaps/t/bx;->a(Lmaps/t/bx;F)Lmaps/t/bx;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lmaps/t/u;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/u;

    move-result-object v3

    iget-object v0, p0, Lmaps/y/k;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    :try_start_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    invoke-virtual {v0}, Lmaps/l/ay;->b()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lmaps/l/ay;->l()Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lmaps/l/ay;->c_()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {v2, v5}, Lmaps/t/bw;->a(Lmaps/t/bx;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v1, v5}, Lmaps/t/u;->a(Lmaps/t/bx;)Z

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    invoke-virtual {v3, v5}, Lmaps/t/u;->a(Lmaps/t/bx;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0, p1}, Lmaps/l/ay;->a(Lmaps/bq/d;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_3
    iget-object v5, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    iget-object v2, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {p0, v0, v4, v2}, Lmaps/y/k;->a(Ljava/lang/RuntimeException;Ljava/util/Iterator;I)V

    :cond_4
    iput-object v1, p0, Lmaps/y/k;->h:Lmaps/t/u;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/k;->b:Z

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/16 :goto_1
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 6

    const/4 v1, 0x1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/k;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p2}, Lmaps/y/k;->a(Lmaps/bq/d;)V

    invoke-direct {p0, p2}, Lmaps/y/k;->b(Lmaps/bq/d;)V

    invoke-direct {p0, p2}, Lmaps/y/k;->c(Lmaps/bq/d;)V

    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    const/4 v2, 0x1

    const/16 v3, 0x303

    invoke-interface {v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v2, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x1e01

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v0, p1, Lmaps/cr/c;->g:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    new-instance v3, Lmaps/af/t;

    invoke-direct {v3, p3}, Lmaps/af/t;-><init>(Lmaps/af/s;)V

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lmaps/af/t;->a(I)V

    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    invoke-virtual {v0}, Lmaps/l/ay;->m()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0, p1, p2, v3}, Lmaps/l/ay;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lmaps/af/t;->a(I)V

    const/4 v0, 0x0

    iget-object v2, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    invoke-virtual {v0}, Lmaps/l/ay;->o()Z

    move-result v5

    if-eqz v5, :cond_5

    move-object v2, v0

    :cond_5
    invoke-virtual {v0, p1, p2, v3}, Lmaps/l/ay;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto :goto_2

    :cond_6
    invoke-virtual {p2}, Lmaps/bq/d;->r()F

    move-result v0

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lmaps/af/t;->a(I)V

    if-eqz v2, :cond_7

    invoke-virtual {v2, p1, p2, v3}, Lmaps/l/ay;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    :cond_7
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x2

    goto :goto_3
.end method

.method public declared-synchronized a(Lmaps/l/ay;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/k;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/l/ay;->n()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, p0}, Lmaps/l/ay;->a(Lmaps/y/k;)V

    iget-object v0, p0, Lmaps/y/k;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/y/k;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/l/ay;->n()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lmaps/y/k;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lmaps/y/al;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/k;->n:Lmaps/y/al;

    return-void
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/k;->o:Lmaps/y/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/k;->o:Lmaps/y/ax;

    invoke-static {v0, p1}, Lmaps/y/ax;->a(Lmaps/y/ax;Lmaps/bq/d;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 2

    iget-object v1, p0, Lmaps/y/k;->c:Lmaps/y/f;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-direct {p0}, Lmaps/y/k;->j()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public b()Lmaps/y/am;
    .locals 1

    iget-object v0, p0, Lmaps/y/k;->j:Lmaps/y/am;

    return-object v0
.end method

.method public b(Lmaps/cr/c;)V
    .locals 2

    new-instance v0, Lmaps/y/ax;

    invoke-direct {v0, p0, p1}, Lmaps/y/ax;-><init>(Lmaps/y/k;Lmaps/cr/c;)V

    iput-object v0, p0, Lmaps/y/k;->o:Lmaps/y/ax;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/k;->o:Lmaps/y/ax;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/x;)V

    return-void
.end method

.method public b(Lmaps/l/ay;)V
    .locals 3

    iget-object v1, p0, Lmaps/y/k;->c:Lmaps/y/f;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/y/k;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/l/ay;->n()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/k;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/l/ay;->n()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/y/k;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lmaps/l/ay;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmaps/l/ay;->c(Z)V

    iget-object v0, p0, Lmaps/y/k;->c:Lmaps/y/f;

    invoke-virtual {v0}, Lmaps/y/f;->e()V

    :cond_0
    invoke-direct {p0, p1}, Lmaps/y/k;->d(Lmaps/l/ay;)V

    invoke-direct {p0, p1}, Lmaps/y/k;->e(Lmaps/l/ay;)V

    invoke-direct {p0}, Lmaps/y/k;->i()V

    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public b(FFLmaps/bq/d;)Z
    .locals 2

    iget-boolean v0, p0, Lmaps/y/k;->l:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lmaps/y/k;->c(FFLmaps/bq/d;)V

    iget-object v0, p0, Lmaps/y/k;->n:Lmaps/y/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/k;->n:Lmaps/y/al;

    iget-object v1, p0, Lmaps/y/k;->m:Lmaps/l/ay;

    invoke-interface {v0, v1}, Lmaps/y/al;->b(Lmaps/l/ay;)V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lmaps/y/k;->l:Z

    if-eqz v1, :cond_2

    invoke-direct {p0, p1, p2, p4}, Lmaps/y/k;->c(FFLmaps/bq/d;)V

    iget-object v1, p0, Lmaps/y/k;->m:Lmaps/l/ay;

    invoke-virtual {v1}, Lmaps/l/ay;->c()Lmaps/l/ab;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/y/k;->m:Lmaps/l/ay;

    invoke-virtual {v1}, Lmaps/l/ay;->c()Lmaps/l/ab;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/k;->m:Lmaps/l/ay;

    invoke-virtual {v2}, Lmaps/l/ay;->c_()Lmaps/t/bx;

    move-result-object v2

    invoke-interface {v1, v2}, Lmaps/l/ab;->a(Lmaps/t/bx;)V

    :cond_0
    iget-object v1, p0, Lmaps/y/k;->n:Lmaps/y/al;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/y/k;->n:Lmaps/y/al;

    iget-object v2, p0, Lmaps/y/k;->m:Lmaps/l/ay;

    invoke-interface {v1, v2}, Lmaps/y/al;->c(Lmaps/l/ay;)V

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/y/k;->m:Lmaps/l/ay;

    iput-boolean v0, p0, Lmaps/y/k;->l:Z

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public declared-synchronized c(Lmaps/l/ay;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/k;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/l/ay;->n()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/k;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/l/ay;->n()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/y/k;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lmaps/y/k;->d(Lmaps/l/ay;)V

    invoke-direct {p0, p1}, Lmaps/y/k;->e(Lmaps/l/ay;)V

    invoke-direct {p0}, Lmaps/y/k;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/y/k;->l:Z

    return v0
.end method

.method public d()V
    .locals 3

    iget-object v1, p0, Lmaps/y/k;->c:Lmaps/y/f;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/y/k;->c:Lmaps/y/f;

    invoke-virtual {v0}, Lmaps/y/f;->d()Lmaps/y/bh;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v2, v0, Lmaps/l/ay;

    if-eqz v2, :cond_0

    check-cast v0, Lmaps/l/ay;

    invoke-virtual {v0}, Lmaps/l/ay;->k()Lmaps/y/k;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lmaps/y/k;->c:Lmaps/y/f;

    invoke-virtual {v0}, Lmaps/y/f;->e()V

    :cond_0
    invoke-direct {p0}, Lmaps/y/k;->j()V

    iget-object v0, p0, Lmaps/y/k;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    invoke-direct {p0, v0}, Lmaps/y/k;->e(Lmaps/l/ay;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_1
    :try_start_3
    iget-object v0, p0, Lmaps/y/k;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lmaps/y/k;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    invoke-direct {p0}, Lmaps/y/k;->i()V

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void
.end method

.method public d(FFLmaps/t/bx;Lmaps/bq/d;)Z
    .locals 4

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ay;

    invoke-virtual {v0}, Lmaps/l/ay;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, p1, p2, p3, p4}, Lmaps/l/ay;->a(FFLmaps/t/bx;Lmaps/bq/d;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lmaps/y/k;->l:Z

    iput-object v0, p0, Lmaps/y/k;->m:Lmaps/l/ay;

    invoke-direct {p0, p1, p2, p4}, Lmaps/y/k;->c(FFLmaps/bq/d;)V

    iget-object v0, p0, Lmaps/y/k;->n:Lmaps/y/al;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/k;->n:Lmaps/y/al;

    iget-object v2, p0, Lmaps/y/k;->m:Lmaps/l/ay;

    invoke-interface {v0, v2}, Lmaps/y/al;->a(Lmaps/l/ay;)V

    :cond_1
    monitor-exit p0

    move v0, v1

    :goto_0
    return v0

    :cond_2
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized e()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/y/k;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lmaps/y/k;->g:Ljava/util/HashMap;

    return-object v0
.end method
