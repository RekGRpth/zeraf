.class public Lmaps/y/x;
.super Lmaps/y/ba;

# interfaces
.implements Lmaps/af/n;
.implements Lmaps/y/bh;


# static fields
.field private static J:Lmaps/al/a;

.field private static final f:F


# instance fields
.field private A:F

.field private B:F

.field private C:Lmaps/y/am;

.field private final D:I

.field private E:F

.field private F:Z

.field private G:Ljava/util/List;

.field private H:Lmaps/q/ac;

.field private I:Lmaps/y/br;

.field protected volatile a:Lmaps/af/f;

.field public b:Ljava/util/List;

.field private g:F

.field private h:F

.field private i:F

.field private final j:Landroid/content/res/Resources;

.field private final k:Z

.field private l:I

.field private final m:Ljava/util/Map;

.field private n:Lmaps/w/o;

.field private o:Lmaps/y/aw;

.field private p:Ljava/lang/String;

.field private final q:Lmaps/t/aj;

.field private final r:Lmaps/t/aj;

.field private final s:Lmaps/t/aj;

.field private t:Z

.field private u:F

.field private v:F

.field private w:I

.field private x:Z

.field private volatile y:Lmaps/t/bb;

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x4

    sget-boolean v0, Lmaps/ae/h;->x:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x40800000

    sput v0, Lmaps/y/x;->f:F

    :goto_0
    new-instance v0, Lmaps/al/a;

    invoke-direct {v0, v2}, Lmaps/al/a;-><init>(I)V

    sput-object v0, Lmaps/y/x;->J:Lmaps/al/a;

    sget-object v0, Lmaps/y/x;->J:Lmaps/al/a;

    const v1, 0x73217bce

    invoke-virtual {v0, v1, v2}, Lmaps/al/a;->b(II)V

    return-void

    :cond_0
    const/high16 v0, 0x40000000

    sput v0, Lmaps/y/x;->f:F

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lmaps/y/f;Z)V
    .locals 6

    const/high16 v5, 0x3f000000

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p2}, Lmaps/y/ba;-><init>(Lmaps/y/f;)V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/x;->m:Ljava/util/Map;

    iput-boolean v3, p0, Lmaps/y/x;->t:Z

    iput-boolean v4, p0, Lmaps/y/x;->z:Z

    new-instance v0, Lmaps/t/aj;

    invoke-direct {v0}, Lmaps/t/aj;-><init>()V

    iput-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    new-instance v0, Lmaps/t/aj;

    invoke-direct {v0}, Lmaps/t/aj;-><init>()V

    iput-object v0, p0, Lmaps/y/x;->r:Lmaps/t/aj;

    new-instance v0, Lmaps/t/aj;

    invoke-direct {v0}, Lmaps/t/aj;-><init>()V

    iput-object v0, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    iput-object p1, p0, Lmaps/y/x;->j:Landroid/content/res/Resources;

    iput-boolean p3, p0, Lmaps/y/x;->k:Z

    const/4 v0, 0x2

    new-array v0, v0, [Lmaps/y/bl;

    invoke-static {}, Lmaps/y/bl;->a()Lmaps/y/as;

    move-result-object v1

    invoke-virtual {v1, v4}, Lmaps/y/as;->a(Z)Lmaps/y/as;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/y/as;->a()Lmaps/y/as;

    move-result-object v1

    sget v2, Lmaps/ad/e;->n:I

    invoke-virtual {v1, v2}, Lmaps/y/as;->a(I)Lmaps/y/as;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/y/as;->c()Lmaps/y/bl;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {}, Lmaps/y/bl;->a()Lmaps/y/as;

    move-result-object v1

    invoke-virtual {v1, v3}, Lmaps/y/as;->a(Z)Lmaps/y/as;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/y/as;->b()Lmaps/y/as;

    move-result-object v1

    sget v2, Lmaps/ad/e;->m:I

    invoke-virtual {v1, v2}, Lmaps/y/as;->a(I)Lmaps/y/as;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/y/as;->c()Lmaps/y/bl;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, Lmaps/y/x;->a([Lmaps/y/bl;)V

    const/high16 v0, 0x42800000

    iput v0, p0, Lmaps/y/x;->v:F

    iput v5, p0, Lmaps/y/x;->A:F

    iput v5, p0, Lmaps/y/x;->B:F

    const/high16 v0, 0x3f800000

    invoke-virtual {p0, v0}, Lmaps/y/x;->a(F)V

    const/16 v0, 0x4000

    invoke-virtual {p0, v0}, Lmaps/y/x;->b(I)V

    sget-object v0, Lmaps/y/am;->t:Lmaps/y/am;

    iput-object v0, p0, Lmaps/y/x;->C:Lmaps/y/am;

    const/high16 v0, 0x41800000

    iput v0, p0, Lmaps/y/x;->g:F

    const/high16 v0, 0x41400000

    iput v0, p0, Lmaps/y/x;->h:F

    const/high16 v0, 0x3f400000

    iput v0, p0, Lmaps/y/x;->i:F

    iget-object v0, p0, Lmaps/y/x;->j:Landroid/content/res/Resources;

    sget v1, Lmaps/ad/b;->p:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lmaps/y/x;->D:I

    sget-boolean v0, Lmaps/ae/h;->x:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/w/n;

    invoke-direct {v0}, Lmaps/w/n;-><init>()V

    iput-object v0, p0, Lmaps/y/x;->n:Lmaps/w/o;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lmaps/w/i;

    invoke-direct {v0}, Lmaps/w/i;-><init>()V

    iput-object v0, p0, Lmaps/y/x;->n:Lmaps/w/o;

    goto :goto_0
.end method

.method private declared-synchronized a(Lmaps/cr/c;I)Lmaps/cr/a;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/x;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/a;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/cr/a;

    invoke-direct {v0, p1}, Lmaps/cr/a;-><init>(Lmaps/cr/c;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/cr/a;->c(Z)V

    iget-object v1, p0, Lmaps/y/x;->j:Landroid/content/res/Resources;

    invoke-virtual {v0, v1, p2}, Lmaps/cr/a;->a(Landroid/content/res/Resources;I)V

    iget-object v1, p0, Lmaps/y/x;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(ZZZ)Lmaps/y/bl;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/y/bl;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/y/bl;->a(ZZZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 11

    const/4 v7, 0x0

    const/high16 v10, 0x3f800000

    const/4 v9, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->c()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lmaps/y/x;->o:Lmaps/y/aw;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/y/aw;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "MyLocation"

    invoke-direct/range {v0 .. v6}, Lmaps/y/aw;-><init>(Lmaps/t/bx;IIILmaps/t/bg;Ljava/lang/String;)V

    iput-object v0, p0, Lmaps/y/x;->o:Lmaps/y/aw;

    iget-object v0, p0, Lmaps/y/x;->o:Lmaps/y/aw;

    sget v1, Lmaps/y/x;->f:F

    invoke-virtual {v0, v1}, Lmaps/y/aw;->a(F)V

    :cond_0
    iget-object v0, p0, Lmaps/y/x;->o:Lmaps/y/aw;

    iget-object v1, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->d()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->c()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Lmaps/y/aw;->a(Lmaps/t/bx;I)V

    invoke-direct {p0}, Lmaps/y/x;->w()Lmaps/y/bl;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/x;->o:Lmaps/y/aw;

    iget v2, v0, Lmaps/y/bl;->f:I

    invoke-virtual {v1, v2}, Lmaps/y/aw;->b(I)V

    iget-object v1, p0, Lmaps/y/x;->o:Lmaps/y/aw;

    iget v0, v0, Lmaps/y/bl;->g:I

    invoke-virtual {v1, v0}, Lmaps/y/aw;->c(I)V

    iget-object v0, p0, Lmaps/y/x;->o:Lmaps/y/aw;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/y/aw;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    :cond_1
    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v1}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, Lmaps/bq/d;->a(Lmaps/t/bx;Z)F

    move-result v1

    invoke-direct {p0}, Lmaps/y/x;->t()Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lmaps/y/x;->u:F

    :goto_0
    invoke-virtual {p2, v0, v1}, Lmaps/bq/d;->a(FF)F

    move-result v0

    iget v1, p0, Lmaps/y/x;->E:F

    mul-float/2addr v0, v1

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    iget-object v2, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v2

    invoke-static {p1, p2, v2, v0}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F)V

    invoke-virtual {p1}, Lmaps/cr/c;->r()V

    iget-object v0, p1, Lmaps/cr/c;->h:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v0, p1, Lmaps/cr/c;->d:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    invoke-direct {p0}, Lmaps/y/x;->w()Lmaps/y/bl;

    move-result-object v2

    iget-boolean v0, p0, Lmaps/y/x;->F:Z

    if-nez v0, :cond_7

    iget v0, v2, Lmaps/y/bl;->c:I

    iget v3, v2, Lmaps/y/bl;->b:I

    if-eq v0, v3, :cond_7

    const/4 v0, 0x1

    :goto_1
    iget-boolean v3, v2, Lmaps/y/bl;->a:Z

    if-nez v3, :cond_2

    iget v3, p0, Lmaps/y/x;->w:I

    iget v4, p0, Lmaps/y/x;->w:I

    iget v5, p0, Lmaps/y/x;->w:I

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    :cond_2
    const/4 v3, 0x1

    const/16 v4, 0x303

    invoke-interface {v1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x1e01

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget v3, v2, Lmaps/y/bl;->d:I

    if-eqz v3, :cond_3

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x2100

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget v3, v2, Lmaps/y/bl;->d:I

    invoke-direct {p0, p1, v3}, Lmaps/y/x;->a(Lmaps/cr/c;I)Lmaps/cr/a;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v3, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v3}, Lmaps/t/aj;->h()F

    move-result v3

    const/high16 v4, 0x41200000

    mul-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v4}, Lmaps/t/aj;->h()F

    move-result v4

    const/high16 v5, 0x40400000

    mul-float/2addr v4, v5

    sub-float v4, v10, v4

    iget-object v5, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v5}, Lmaps/t/aj;->h()F

    move-result v5

    const/high16 v6, 0x40800000

    mul-float/2addr v5, v6

    iget-object v6, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v6}, Lmaps/t/aj;->h()F

    move-result v6

    const/high16 v7, -0x3f800000

    mul-float/2addr v6, v7

    invoke-interface {v1, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    const/4 v4, 0x0

    invoke-interface {v1, v5, v6, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-interface {v1, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/4 v4, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-interface {v1, v4, v7, v8}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    div-float v4, v10, v3

    div-float v7, v10, v3

    div-float v3, v10, v3

    invoke-interface {v1, v4, v7, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    neg-float v3, v5

    neg-float v4, v6

    const/4 v5, 0x0

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x1e01

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    :cond_3
    iget-object v3, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v3}, Lmaps/t/aj;->h()F

    move-result v3

    cmpl-float v3, v3, v9

    if-eqz v3, :cond_4

    iget-object v3, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v3}, Lmaps/t/aj;->h()F

    move-result v3

    iget-object v4, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v4}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/bx;->e()D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    :cond_4
    iget-object v3, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v3}, Lmaps/t/aj;->e()Z

    move-result v3

    if-eqz v3, :cond_5

    iget v3, v2, Lmaps/y/bl;->e:I

    if-eqz v3, :cond_5

    iget-object v3, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v3}, Lmaps/t/aj;->b()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/high16 v3, 0x40000000

    const/high16 v4, 0x40000000

    const/high16 v5, 0x40000000

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget v3, v2, Lmaps/y/bl;->e:I

    invoke-direct {p0, p1, v3}, Lmaps/y/x;->a(Lmaps/cr/c;I)Lmaps/cr/a;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v3, 0x5

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    const/high16 v3, 0x3f000000

    const/high16 v4, 0x3f000000

    const/high16 v5, 0x3f000000

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget-object v3, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v3}, Lmaps/t/aj;->b()F

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :cond_5
    invoke-direct {p0}, Lmaps/y/x;->t()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v3}, Lmaps/t/aj;->b()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :goto_2
    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->j()F

    move-result v0

    cmpl-float v0, v0, v10

    if-nez v0, :cond_9

    iget v0, v2, Lmaps/y/bl;->c:I

    invoke-direct {p0, p1, v0}, Lmaps/y/x;->a(Lmaps/cr/c;I)Lmaps/cr/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    :goto_3
    const/4 v0, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_6
    :try_start_1
    iget v0, p0, Lmaps/y/x;->v:F

    goto/16 :goto_0

    :cond_7
    move v0, v7

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p2}, Lmaps/bq/d;->q()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    invoke-virtual {p2}, Lmaps/bq/d;->r()F

    move-result v3

    const/high16 v4, 0x3f800000

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_9
    :try_start_2
    iget v0, v2, Lmaps/y/bl;->b:I

    invoke-direct {p0, p1, v0}, Lmaps/y/x;->a(Lmaps/cr/c;I)Lmaps/cr/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private c(Lmaps/bq/d;)F
    .locals 4

    const/high16 v0, 0x3f800000

    invoke-virtual {p1}, Lmaps/bq/d;->s()F

    move-result v1

    iget v2, p0, Lmaps/y/x;->g:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    iget v2, p0, Lmaps/y/x;->h:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_1

    iget v0, p0, Lmaps/y/x;->i:F

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lmaps/y/x;->i:F

    sub-float/2addr v0, v2

    iget v2, p0, Lmaps/y/x;->g:F

    iget v3, p0, Lmaps/y/x;->h:F

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    iget v2, p0, Lmaps/y/x;->i:F

    iget v3, p0, Lmaps/y/x;->h:F

    sub-float/2addr v1, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_0
.end method

.method private p()V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->I:Lmaps/y/br;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->I:Lmaps/y/br;

    iget-object v1, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-direct {p0}, Lmaps/y/x;->w()Lmaps/y/bl;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/y/br;->a(Lmaps/t/aj;Lmaps/y/bl;)V

    :cond_0
    return-void
.end method

.method private declared-synchronized q()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/x;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/y/x;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private r()V
    .locals 5

    iget-object v0, p0, Lmaps/y/x;->I:Lmaps/y/br;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->I:Lmaps/y/br;

    iget v1, p0, Lmaps/y/x;->v:F

    iget v2, p0, Lmaps/y/x;->u:F

    iget v3, p0, Lmaps/y/x;->w:I

    int-to-float v3, v3

    const/high16 v4, 0x47800000

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lmaps/y/br;->a(FFF)V

    :cond_0
    return-void
.end method

.method private s()F
    .locals 2

    invoke-direct {p0}, Lmaps/y/x;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/y/x;->u:F

    iget v1, p0, Lmaps/y/x;->v:F

    div-float/2addr v0, v1

    :goto_0
    iget v1, p0, Lmaps/y/x;->E:F

    mul-float/2addr v1, v0

    invoke-direct {p0}, Lmaps/y/x;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/y/x;->u:F

    :goto_1
    mul-float/2addr v1, v0

    invoke-direct {p0}, Lmaps/y/x;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lmaps/y/x;->B:F

    :goto_2
    mul-float/2addr v0, v1

    return v0

    :cond_0
    const/high16 v0, 0x3e800000

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/y/x;->v:F

    goto :goto_1

    :cond_2
    iget v0, p0, Lmaps/y/x;->A:F

    goto :goto_2
.end method

.method private t()Z
    .locals 1

    invoke-direct {p0}, Lmaps/y/x;->w()Lmaps/y/bl;

    move-result-object v0

    iget-boolean v0, v0, Lmaps/y/bl;->a:Z

    return v0
.end method

.method private declared-synchronized u()Lmaps/t/aj;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/y/x;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->r:Lmaps/t/aj;

    iget-object v1, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0, v1}, Lmaps/t/aj;->a(Lmaps/t/aj;)V

    iget-object v0, p0, Lmaps/y/x;->n:Lmaps/w/o;

    iget-object v1, p0, Lmaps/y/x;->r:Lmaps/t/aj;

    invoke-interface {v0, v1}, Lmaps/w/o;->a(Lmaps/t/aj;)Z

    iget-object v0, p0, Lmaps/y/x;->r:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->r:Lmaps/t/aj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private v()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/y/x;->a:Lmaps/af/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->a:Lmaps/af/f;

    invoke-interface {v0, v1, v1}, Lmaps/af/f;->a(ZZ)V

    :cond_0
    return-void
.end method

.method private w()Lmaps/y/bl;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/y/x;->u()Lmaps/t/aj;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/aj;->e()Z

    move-result v1

    invoke-virtual {v0}, Lmaps/t/aj;->g()Z

    move-result v2

    invoke-virtual {v0}, Lmaps/t/aj;->i()Z

    move-result v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, v1, v0, v2}, Lmaps/y/x;->a(ZZZ)Lmaps/y/bl;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(F)V
    .locals 1

    iget v0, p0, Lmaps/y/x;->v:F

    mul-float/2addr v0, p1

    iput v0, p0, Lmaps/y/x;->u:F

    invoke-direct {p0}, Lmaps/y/x;->r()V

    return-void
.end method

.method public a(FFF)V
    .locals 0

    iput p1, p0, Lmaps/y/x;->g:F

    iput p2, p0, Lmaps/y/x;->h:F

    iput p3, p0, Lmaps/y/x;->i:F

    return-void
.end method

.method public a(FII)V
    .locals 3

    const/high16 v2, 0x42c80000

    iget v0, p0, Lmaps/y/x;->u:F

    iget v1, p0, Lmaps/y/x;->v:F

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    mul-float/2addr v1, p1

    iput v1, p0, Lmaps/y/x;->v:F

    int-to-float v1, p2

    div-float/2addr v1, v2

    iput v1, p0, Lmaps/y/x;->A:F

    int-to-float v1, p3

    div-float/2addr v1, v2

    iput v1, p0, Lmaps/y/x;->B:F

    invoke-virtual {p0, v0}, Lmaps/y/x;->a(F)V

    invoke-direct {p0}, Lmaps/y/x;->r()V

    return-void
.end method

.method public a(Ljava/util/List;FFLmaps/t/bx;Lmaps/bq/d;I)V
    .locals 2

    invoke-virtual {p0}, Lmaps/y/x;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2, p3, p4, p5}, Lmaps/y/x;->c(FFLmaps/t/bx;Lmaps/bq/d;)I

    move-result v0

    if-ge v0, p6, :cond_0

    new-instance v1, Lmaps/y/d;

    invoke-direct {v1, p0, p0, v0}, Lmaps/y/d;-><init>(Lmaps/y/bh;Lmaps/y/ba;I)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/af/f;)V
    .locals 0

    iput-object p2, p0, Lmaps/y/x;->a:Lmaps/af/f;

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 4

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-gtz v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->l()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/y/x;->t:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/y/x;->n:Lmaps/w/o;

    invoke-virtual {p1}, Lmaps/cr/c;->e()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lmaps/w/o;->a(J)I

    move-result v0

    iput v0, p0, Lmaps/y/x;->l:I

    :goto_1
    iget v0, p0, Lmaps/y/x;->l:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/x;->n:Lmaps/w/o;

    iget-object v1, p0, Lmaps/y/x;->r:Lmaps/t/aj;

    invoke-interface {v0, v1}, Lmaps/w/o;->a(Lmaps/t/aj;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/x;->r:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    iget-object v1, p0, Lmaps/y/x;->r:Lmaps/t/aj;

    invoke-virtual {v0, v1}, Lmaps/t/aj;->a(Lmaps/t/aj;)V

    invoke-virtual {p1}, Lmaps/cr/c;->e()J

    move-result-wide v0

    const-wide/16 v2, 0xc8

    add-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lmaps/cr/c;->a(J)V

    :goto_2
    iget-object v0, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->l()Z

    move-result v0

    if-nez v0, :cond_4

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lmaps/y/x;->l:I

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    iget-object v1, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0, v1}, Lmaps/t/aj;->a(Lmaps/t/aj;)V

    goto :goto_2

    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    const/4 v0, 0x0

    iget-object v2, p0, Lmaps/y/x;->y:Lmaps/t/bb;

    if-eqz v2, :cond_5

    invoke-interface {p3}, Lmaps/af/s;->c()Lmaps/p/av;

    move-result-object v0

    iget-object v2, p0, Lmaps/y/x;->y:Lmaps/t/bb;

    invoke-virtual {v2}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/p/av;->a(Lmaps/t/v;)Lmaps/p/z;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v2, p0, Lmaps/y/x;->s:Lmaps/t/aj;

    invoke-virtual {v2}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v2

    invoke-interface {v0, p1, p2, p3, v2}, Lmaps/p/z;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;Lmaps/t/bx;)V

    :cond_5
    invoke-direct {p0, p1, p2, p3}, Lmaps/y/x;->b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    if-eqz v0, :cond_6

    invoke-interface {v0, p1, p3}, Lmaps/p/z;->a(Lmaps/cr/c;Lmaps/af/s;)V

    :cond_6
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0
.end method

.method public declared-synchronized a(Lmaps/t/aj;)V
    .locals 2

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->l()Z

    move-result v0

    invoke-virtual {p1}, Lmaps/t/aj;->l()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->f()Lmaps/t/bb;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/t/aj;->f()Lmaps/t/bb;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->g()Z

    move-result v0

    invoke-virtual {p1}, Lmaps/t/aj;->g()Z

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/x;->z:Z

    :cond_1
    iget-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0, p1}, Lmaps/t/aj;->a(Lmaps/t/aj;)V

    iget-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/y/x;->n:Lmaps/w/o;

    iget-object v1, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-interface {v0, v1}, Lmaps/w/o;->b(Lmaps/t/aj;)V

    :goto_0
    invoke-direct {p0}, Lmaps/y/x;->p()V

    invoke-direct {p0}, Lmaps/y/x;->v()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    sget-boolean v0, Lmaps/ae/h;->x:Z

    if-eqz v0, :cond_3

    new-instance v0, Lmaps/w/n;

    invoke-direct {v0}, Lmaps/w/n;-><init>()V

    iput-object v0, p0, Lmaps/y/x;->n:Lmaps/w/o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    new-instance v0, Lmaps/w/i;

    invoke-direct {v0}, Lmaps/w/i;-><init>()V

    iput-object v0, p0, Lmaps/y/x;->n:Lmaps/w/o;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public a(Lmaps/y/am;)V
    .locals 0

    iput-object p1, p0, Lmaps/y/x;->C:Lmaps/y/am;

    return-void
.end method

.method public varargs declared-synchronized a([Lmaps/y/bl;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lmaps/f/ef;->a([Ljava/lang/Object;)Lmaps/f/ef;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/x;->b:Ljava/util/List;

    invoke-direct {p0}, Lmaps/y/x;->q()V

    sget-boolean v0, Lmaps/ae/h;->m:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/y/x;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/List;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lmaps/y/x;->z:Z

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lmaps/y/x;->z:Z

    invoke-virtual {p0}, Lmaps/y/x;->d_()Lmaps/t/bb;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/x;->y:Lmaps/t/bb;

    const/4 v0, 0x0

    iget-object v3, p0, Lmaps/y/x;->y:Lmaps/t/bb;

    if-eqz v3, :cond_0

    invoke-static {}, Lmaps/b/r;->a()Lmaps/b/r;

    move-result-object v0

    iget-object v3, p0, Lmaps/y/x;->y:Lmaps/t/bb;

    invoke-virtual {v3}, Lmaps/t/bb;->a()Lmaps/t/bg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/b/r;->e(Lmaps/t/bg;)Lmaps/b/z;

    move-result-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object v3, p0, Lmaps/y/x;->y:Lmaps/t/bb;

    if-eqz v3, :cond_1

    if-nez v0, :cond_2

    :cond_1
    invoke-super {p0, p1}, Lmaps/y/ba;->a(Ljava/util/List;)Z

    move-result v0

    :goto_0
    return v0

    :cond_2
    sget-object v3, Lmaps/p/j;->i:Lmaps/p/j;

    new-array v4, v1, [Lmaps/p/z;

    aput-object v0, v4, v2

    invoke-virtual {p0, v3, v4}, Lmaps/y/x;->a(Lmaps/p/j;[Lmaps/p/z;)Lmaps/p/av;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public a(Lmaps/bq/d;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/y/x;->c_()Lmaps/t/bx;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1, v2}, Lmaps/bq/d;->b(Lmaps/t/bx;)[I

    move-result-object v2

    invoke-direct {p0}, Lmaps/y/x;->s()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    aget v4, v2, v1

    sub-int/2addr v4, v3

    aget v5, v2, v1

    add-int/2addr v5, v3

    aget v6, v2, v0

    sub-int/2addr v6, v3

    aget v2, v2, v0

    add-int/2addr v2, v3

    invoke-virtual {p1}, Lmaps/bq/d;->l()I

    move-result v3

    if-ge v4, v3, :cond_1

    if-ltz v5, :cond_1

    invoke-virtual {p1}, Lmaps/bq/d;->m()I

    move-result v3

    if-ge v6, v3, :cond_1

    if-ltz v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 2

    invoke-direct {p0, p1}, Lmaps/y/x;->c(Lmaps/bq/d;)F

    move-result v0

    iput v0, p0, Lmaps/y/x;->E:F

    iget-object v0, p0, Lmaps/y/x;->n:Lmaps/w/o;

    invoke-interface {v0, p1}, Lmaps/w/o;->a(Lmaps/bq/d;)V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->I:Lmaps/y/br;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->I:Lmaps/y/br;

    iget v1, p0, Lmaps/y/x;->E:F

    invoke-virtual {v0, p1, v1}, Lmaps/y/br;->a(Lmaps/bq/d;F)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a_(Lmaps/cr/c;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/y/x;->q()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/x;->z:Z

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->H:Lmaps/q/ac;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/x;->H:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/x;->I:Lmaps/y/br;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->b(Lmaps/q/x;)V

    iput-object v2, p0, Lmaps/y/x;->H:Lmaps/q/ac;

    iput-object v2, p0, Lmaps/y/x;->I:Lmaps/y/br;

    :cond_0
    return-void
.end method

.method public b(Lmaps/bq/d;)I
    .locals 1

    iget v0, p0, Lmaps/y/x;->l:I

    return v0
.end method

.method public b()Lmaps/y/am;
    .locals 1

    iget-object v0, p0, Lmaps/y/x;->C:Lmaps/y/am;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lmaps/y/x;->w:I

    invoke-direct {p0}, Lmaps/y/x;->r()V

    return-void
.end method

.method public b(Lmaps/cr/c;)V
    .locals 9

    const/16 v8, 0x302

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v5, 0x303

    invoke-direct {p0}, Lmaps/y/x;->w()Lmaps/y/bl;

    move-result-object v0

    new-instance v1, Lmaps/q/ac;

    sget-object v2, Lmaps/y/am;->x:Lmaps/y/am;

    invoke-direct {v1, v2}, Lmaps/q/ac;-><init>(Lmaps/y/am;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "accuracy circle outline "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/y/x;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/q/ac;->a(Ljava/lang/String;)V

    new-instance v2, Lmaps/q/bb;

    iget v3, v0, Lmaps/y/bl;->f:I

    invoke-direct {v2, v3}, Lmaps/q/bb;-><init>(I)V

    invoke-virtual {v1, v2}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    new-instance v2, Lmaps/q/n;

    invoke-direct {v2, v8, v5}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v1, v2}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    invoke-virtual {p1}, Lmaps/cr/c;->n()Lmaps/s/s;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lmaps/s/s;->a(I)Lmaps/q/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/q/ac;->a(Lmaps/q/r;)V

    new-instance v2, Lmaps/q/ac;

    sget-object v3, Lmaps/y/am;->x:Lmaps/y/am;

    invoke-direct {v2, v3}, Lmaps/q/ac;-><init>(Lmaps/y/am;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "accuracy circle fill "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmaps/y/x;->p:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/q/ac;->a(Ljava/lang/String;)V

    new-instance v3, Lmaps/q/bb;

    iget v0, v0, Lmaps/y/bl;->g:I

    invoke-direct {v3, v0}, Lmaps/q/bb;-><init>(I)V

    invoke-virtual {v2, v3}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    new-instance v0, Lmaps/q/n;

    invoke-direct {v0, v8, v5}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v2, v0}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    invoke-virtual {p1}, Lmaps/cr/c;->n()Lmaps/s/s;

    move-result-object v0

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lmaps/s/s;->a(I)Lmaps/q/r;

    move-result-object v0

    invoke-virtual {v2, v0}, Lmaps/q/ac;->a(Lmaps/q/r;)V

    new-instance v0, Lmaps/q/ac;

    sget-object v3, Lmaps/y/am;->x:Lmaps/y/am;

    invoke-direct {v0, v3}, Lmaps/q/ac;-><init>(Lmaps/y/am;)V

    iput-object v0, p0, Lmaps/y/x;->H:Lmaps/q/ac;

    iget-object v0, p0, Lmaps/y/x;->H:Lmaps/q/ac;

    const-string v3, "My Location marker"

    invoke-virtual {v0, v3}, Lmaps/q/ac;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/y/x;->H:Lmaps/q/ac;

    new-instance v3, Lmaps/q/ar;

    invoke-direct {v3}, Lmaps/q/ar;-><init>()V

    invoke-virtual {v0, v3}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/y/x;->H:Lmaps/q/ac;

    new-instance v3, Lmaps/q/n;

    invoke-direct {v3, v6, v5}, Lmaps/q/n;-><init>(II)V

    invoke-virtual {v0, v3}, Lmaps/q/ac;->a(Lmaps/q/z;)V

    iget-object v0, p0, Lmaps/y/x;->H:Lmaps/q/ac;

    invoke-virtual {p1}, Lmaps/cr/c;->n()Lmaps/s/s;

    move-result-object v3

    invoke-virtual {v3, v7}, Lmaps/s/s;->a(I)Lmaps/q/r;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/q/ac;->a(Lmaps/q/r;)V

    new-array v0, v7, [Lmaps/q/ac;

    const/4 v3, 0x0

    aput-object v1, v0, v3

    aput-object v2, v0, v6

    invoke-static {v0}, Lmaps/f/fd;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/y/x;->G:Ljava/util/List;

    new-instance v0, Lmaps/y/br;

    iget-object v1, p0, Lmaps/y/x;->G:Ljava/util/List;

    iget-object v2, p0, Lmaps/y/x;->H:Lmaps/q/ac;

    iget-object v3, p0, Lmaps/y/x;->j:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2, p1, v3}, Lmaps/y/br;-><init>(Ljava/util/List;Lmaps/q/ac;Lmaps/cr/c;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lmaps/y/x;->I:Lmaps/y/br;

    iget-object v0, p0, Lmaps/y/x;->I:Lmaps/y/br;

    iget-object v1, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-direct {p0}, Lmaps/y/x;->w()Lmaps/y/bl;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/y/br;->a(Lmaps/t/aj;Lmaps/y/bl;)V

    invoke-direct {p0}, Lmaps/y/x;->r()V

    iget-object v0, p0, Lmaps/y/x;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/ac;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/q/ad;->a(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/x;->H:Lmaps/q/ac;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/ba;)V

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/x;->I:Lmaps/y/br;

    invoke-virtual {v0, v1}, Lmaps/q/ad;->a(Lmaps/q/x;)V

    return-void
.end method

.method public c(FFLmaps/t/bx;Lmaps/bq/d;)I
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->l()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->a()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {p4, v0}, Lmaps/bq/d;->b(Lmaps/t/bx;)[I

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    aget v1, v0, v2

    int-to-float v1, v1

    sub-float v1, p1, v1

    aget v2, v0, v2

    int-to-float v2, v2

    sub-float v2, p1, v2

    mul-float/2addr v1, v2

    aget v2, v0, v3

    int-to-float v2, v2

    sub-float v2, p2, v2

    aget v0, v0, v3

    int-to-float v0, v0

    sub-float v0, p2, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized c_()Lmaps/t/bx;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/y/x;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->n:Lmaps/w/o;

    iget-object v1, p0, Lmaps/y/x;->r:Lmaps/t/aj;

    invoke-interface {v0, v1}, Lmaps/w/o;->a(Lmaps/t/aj;)Z

    iget-object v0, p0, Lmaps/y/x;->r:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/y/x;->r:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->a()Lmaps/t/bx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/y/x;->q:Lmaps/t/aj;

    invoke-virtual {v0}, Lmaps/t/aj;->a()Lmaps/t/bx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/y/x;->x:Z

    return-void
.end method

.method public declared-synchronized d_()Lmaps/t/bb;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/y/x;->u()Lmaps/t/aj;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/aj;->f()Lmaps/t/bb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/y/x;->x:Z

    return-void
.end method

.method public g()Lmaps/af/n;
    .locals 0

    return-object p0
.end method

.method public h()I
    .locals 2

    iget v0, p0, Lmaps/y/x;->D:I

    int-to-float v0, v0

    iget v1, p0, Lmaps/y/x;->E:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public i()Lmaps/bq/a;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/y/x;->k:Z

    return v0
.end method

.method declared-synchronized k()V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    new-array v5, v0, [Z

    move v4, v2

    :goto_0
    array-length v0, v5

    shl-int v0, v1, v0

    if-ge v4, v0, :cond_3

    move v3, v2

    :goto_1
    array-length v0, v5

    if-ge v3, v0, :cond_1

    shr-int v0, v4, v3

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_2
    aput-boolean v0, v5, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    aget-boolean v0, v5, v0

    const/4 v3, 0x1

    aget-boolean v3, v5, v3

    const/4 v6, 0x2

    aget-boolean v6, v5, v6

    invoke-direct {p0, v0, v3, v6}, Lmaps/y/x;->a(ZZZ)Lmaps/y/bl;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No texture rule matches "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v5}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_3
    monitor-exit p0

    return-void
.end method

.method public o_()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
