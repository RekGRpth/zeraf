.class public Lmaps/bl/b;
.super Ljava/lang/Object;


# static fields
.field public static final a:[B


# instance fields
.field private b:Lmaps/bb/c;

.field private c:Lmaps/bb/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/bl/b;->a:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x4ct
        0x54t
        0x49t
        0x50t
        0xat
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([BI[B)Z
    .locals 4

    const/4 v1, 0x0

    array-length v0, p0

    array-length v2, p2

    add-int/2addr v2, p1

    if-ge v0, v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_2

    add-int v2, p1, v0

    aget-byte v2, p0, v2

    aget-byte v3, p2, v0

    if-ne v2, v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private b([B)I
    .locals 5

    sget-object v0, Lmaps/bl/b;->a:[B

    array-length v0, v0

    const/4 v1, 0x4

    new-array v1, v1, [B

    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v1}, Lmaps/ah/e;->a([B)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/lit8 v4, v0, 0x4

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1, v4, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    if-gez v2, :cond_0

    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    :goto_0
    add-int v1, v4, v3

    new-instance v1, Lmaps/bb/c;

    sget-object v2, Lmaps/c/bi;->a:Lmaps/bb/d;

    invoke-direct {v1, v2}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    iput-object v1, p0, Lmaps/bl/b;->b:Lmaps/bb/c;

    iget-object v1, p0, Lmaps/bl/b;->b:Lmaps/bb/c;

    invoke-virtual {v1, v0}, Lmaps/bb/c;->a(Ljava/io/InputStream;)Lmaps/bb/c;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return v3

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private d()Lmaps/bb/c;
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lmaps/bl/b;->c:Lmaps/bb/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bl/b;->b:Lmaps/bb/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bl/b;->b:Lmaps/bb/c;

    invoke-virtual {v0, v1}, Lmaps/bb/c;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bl/b;->b:Lmaps/bb/c;

    invoke-virtual {v0, v1}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v0

    iput-object v0, p0, Lmaps/bl/b;->c:Lmaps/bb/c;

    :cond_0
    iget-object v0, p0, Lmaps/bl/b;->c:Lmaps/bb/c;

    return-object v0
.end method


# virtual methods
.method public a([B)[B
    .locals 4

    const/4 v1, 0x0

    sget-object v0, Lmaps/bl/b;->a:[B

    invoke-static {p1, v1, v0}, Lmaps/bl/b;->a([BI[B)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0, p1}, Lmaps/bl/b;->b([B)I

    move-result v0

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/bl/b;->c:Lmaps/bb/c;

    sget-object v1, Lmaps/bl/b;->a:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v1, v0

    array-length v0, p1

    sub-int/2addr v0, v1

    new-array v0, v0, [B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x0

    :try_start_1
    array-length v3, v0

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object p1, v0

    :cond_0
    :goto_0
    return-object p1

    :catch_0
    move-exception v0

    :goto_1
    const-string v1, "IOException reading map tile info"

    invoke-static {v1, v0}, Lmaps/bh/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    move-object p1, v0

    move-object v0, v1

    goto :goto_1
.end method

.method public a()[Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lmaps/bl/b;->d()Lmaps/bb/c;

    move-result-object v2

    if-nez v2, :cond_1

    new-array v0, v1, [Ljava/lang/String;

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {v2, v5}, Lmaps/bb/c;->j(I)I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v5, v1}, Lmaps/bb/c;->h(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public b()[Ljava/lang/String;
    .locals 6

    const/4 v5, 0x2

    const/4 v1, 0x0

    invoke-direct {p0}, Lmaps/bl/b;->d()Lmaps/bb/c;

    move-result-object v2

    if-nez v2, :cond_1

    new-array v0, v1, [Ljava/lang/String;

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {v2, v5}, Lmaps/bb/c;->j(I)I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v5, v1}, Lmaps/bb/c;->h(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public c()I
    .locals 4

    const/4 v3, 0x3

    const/4 v0, -0x1

    invoke-direct {p0}, Lmaps/bl/b;->d()Lmaps/bb/c;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v3}, Lmaps/bb/c;->i(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v3}, Lmaps/bb/c;->d(I)I

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "year=0"

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lmaps/bh/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
