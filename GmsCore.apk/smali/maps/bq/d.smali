.class public Lmaps/bq/d;
.super Lmaps/q/h;


# static fields
.field private static final F:Ljava/util/concurrent/atomic/AtomicLong;

.field static final c:F

.field public static final d:Lmaps/bq/a;

.field private static final e:F

.field private static final f:Lmaps/t/bx;

.field private static final g:F


# instance fields
.field private A:[F

.field private B:[F

.field private C:[F

.field private final D:[F

.field private final E:Lmaps/t/bx;

.field private volatile G:J

.field private H:Lmaps/bq/c;

.field private final h:Ljava/lang/Thread;

.field private i:Lmaps/bq/a;

.field private j:Z

.field private k:Lmaps/t/bx;

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:I

.field private q:I

.field private r:F

.field private s:Lmaps/t/bx;

.field private t:Lmaps/t/bx;

.field private u:Lmaps/t/bx;

.field private v:Lmaps/t/u;

.field private w:F

.field private x:[D

.field private y:[F

.field private z:[F


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v3, 0x0

    const/4 v6, 0x0

    const-wide/high16 v0, 0x3ff0000000000000L

    const-wide/high16 v4, 0x4000000000000000L

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    div-double/2addr v0, v4

    double-to-float v0, v0

    sput v0, Lmaps/bq/d;->e:F

    const/high16 v0, 0x48800000

    const-wide v1, 0x3ff4f1a6c638d03fL

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    sput v0, Lmaps/bq/d;->c:F

    new-instance v0, Lmaps/t/bx;

    const/4 v1, 0x1

    invoke-direct {v0, v6, v6, v1}, Lmaps/t/bx;-><init>(III)V

    sput-object v0, Lmaps/bq/d;->f:Lmaps/t/bx;

    new-instance v0, Lmaps/bq/a;

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1, v6, v6}, Lmaps/t/bx;-><init>(II)V

    const/high16 v2, 0x41a00000

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    sput-object v0, Lmaps/bq/d;->d:Lmaps/bq/a;

    const-wide/high16 v0, 0x3fe0000000000000L

    const-wide v2, 0x3fd0c152382d7365L

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lmaps/bq/d;->g:F

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x1

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lmaps/bq/d;->F:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(Lmaps/bq/a;IIF)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIFLjava/lang/Thread;)V

    return-void
.end method

.method public constructor <init>(Lmaps/bq/a;IIFLjava/lang/Thread;)V
    .locals 10

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v0, 0x10

    new-array v8, v0, [F

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v9, v6

    invoke-direct/range {v0 .. v9}, Lmaps/bq/d;-><init>(Lmaps/bq/a;IIFLjava/lang/Thread;Lmaps/q/av;I[FLmaps/q/ad;)V

    return-void
.end method

.method public constructor <init>(Lmaps/bq/a;IIFLjava/lang/Thread;Lmaps/q/av;I[FLmaps/q/ad;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p6, p7, p8}, Lmaps/q/h;-><init>(Lmaps/q/av;I[F)V

    sget v0, Lmaps/bq/d;->g:F

    iput v0, p0, Lmaps/bq/d;->n:F

    iput-object v1, p0, Lmaps/bq/d;->y:[F

    iput-object v1, p0, Lmaps/bq/d;->z:[F

    iput-object v1, p0, Lmaps/bq/d;->A:[F

    iput-object v1, p0, Lmaps/bq/d;->B:[F

    iput-object v1, p0, Lmaps/bq/d;->C:[F

    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/bq/d;->D:[F

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/bq/d;->E:Lmaps/t/bx;

    sget-object v0, Lmaps/bq/d;->F:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/bq/d;->G:J

    iput p3, p0, Lmaps/bq/d;->q:I

    iput p2, p0, Lmaps/bq/d;->p:I

    iput p4, p0, Lmaps/bq/d;->r:F

    iput-object p5, p0, Lmaps/bq/d;->h:Ljava/lang/Thread;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bq/d;->j:Z

    const/high16 v0, 0x41f00000

    invoke-virtual {p0, v0}, Lmaps/bq/d;->b(F)V

    invoke-direct {p0, p1}, Lmaps/bq/d;->b(Lmaps/bq/a;)V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    if-eqz p9, :cond_0

    new-instance v0, Lmaps/bq/c;

    invoke-direct {v0, p0}, Lmaps/bq/c;-><init>(Lmaps/bq/d;)V

    iput-object v0, p0, Lmaps/bq/d;->H:Lmaps/bq/c;

    iget-object v0, p0, Lmaps/bq/d;->H:Lmaps/bq/c;

    invoke-virtual {p9, v0}, Lmaps/q/ad;->a(Lmaps/q/x;)V

    :cond_0
    return-void
.end method

.method private E()V
    .locals 12

    const-wide v6, 0x3f91df46a2529d39L

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->e()F

    move-result v0

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iget-object v2, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v2}, Lmaps/bq/a;->d()F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v2, v6

    iget-object v4, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v4}, Lmaps/bq/a;->f()F

    move-result v4

    const/high16 v5, 0x3f000000

    mul-float/2addr v4, v5

    iget v5, p0, Lmaps/bq/d;->o:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v4, v6

    iget v6, p0, Lmaps/bq/d;->l:F

    float-to-double v6, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    mul-double/2addr v2, v10

    sub-double v2, v8, v2

    mul-double/2addr v2, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v4, v2

    double-to-int v4, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v3

    add-int/2addr v3, v4

    invoke-virtual {v1}, Lmaps/t/bx;->g()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {v2, v3, v0}, Lmaps/t/bx;->d(II)V

    return-void
.end method

.method private F()V
    .locals 6

    const-wide/high16 v0, 0x4000000000000000L

    const/high16 v2, 0x41f00000

    iget-object v3, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v3}, Lmaps/bq/a;->a()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iget v2, p0, Lmaps/bq/d;->q:I

    int-to-double v2, v2

    const/high16 v4, 0x43800000

    iget v5, p0, Lmaps/bq/d;->r:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    div-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iget v1, p0, Lmaps/bq/d;->n:F

    mul-float/2addr v0, v1

    iput v0, p0, Lmaps/bq/d;->l:F

    return-void
.end method

.method private G()[D
    .locals 10

    const-wide v6, 0x3f91df46a2529d39L

    const-wide v4, 0x4076800000000000L

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->x:[D

    if-nez v0, :cond_1

    const-wide v0, 0x4056800000000000L

    iget-object v2, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v2}, Lmaps/bq/a;->e()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_2

    add-double/2addr v0, v4

    :cond_0
    :goto_0
    iget-object v2, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v2}, Lmaps/bq/a;->d()F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v2, v6

    mul-double/2addr v0, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    const/4 v6, 0x3

    new-array v6, v6, [D

    iput-object v6, p0, Lmaps/bq/d;->x:[D

    iget-object v6, p0, Lmaps/bq/d;->x:[D

    const/4 v7, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v8, v4

    aput-wide v8, v6, v7

    iget-object v6, p0, Lmaps/bq/d;->x:[D

    const/4 v7, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    aput-wide v0, v6, v7

    iget-object v0, p0, Lmaps/bq/d;->x:[D

    const/4 v1, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    neg-double v2, v2

    aput-wide v2, v0, v1

    :cond_1
    iget-object v0, p0, Lmaps/bq/d;->x:[D

    return-object v0

    :cond_2
    cmpl-double v2, v0, v4

    if-ltz v2, :cond_0

    sub-double/2addr v0, v4

    goto :goto_0
.end method

.method private H()V
    .locals 8

    const v6, 0x3dcccccd

    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/bq/d;->z:[F

    invoke-virtual {p0}, Lmaps/bq/d;->y()F

    move-result v0

    iget v1, p0, Lmaps/bq/d;->o:F

    float-to-double v1, v1

    const-wide v3, 0x3f91df46a2529d39L

    mul-double/2addr v1, v3

    const-wide/high16 v3, 0x3fe0000000000000L

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->tan(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float v5, v6, v1

    neg-float v4, v5

    mul-float v2, v4, v0

    mul-float v3, v5, v0

    iget-object v0, p0, Lmaps/bq/d;->z:[F

    const/4 v1, 0x0

    const/high16 v7, 0x41a00000

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->frustumM([FIFFFFFF)V

    return-void
.end method

.method private I()V
    .locals 7

    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/bq/d;->y:[F

    invoke-virtual {p0}, Lmaps/bq/d;->x()F

    move-result v0

    iget-object v1, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {p0}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/bx;->f()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-virtual {v3}, Lmaps/t/bx;->g()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-virtual {v3}, Lmaps/t/bx;->h()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    invoke-virtual {p0}, Lmaps/bq/d;->w()Lmaps/t/bx;

    move-result-object v6

    iget-object v0, p0, Lmaps/bq/d;->y:[F

    invoke-virtual {v6}, Lmaps/t/bx;->f()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v6}, Lmaps/t/bx;->g()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v6}, Lmaps/t/bx;->h()I

    move-result v6

    int-to-float v6, v6

    invoke-static/range {v0 .. v6}, Lmaps/bq/d;->b([FFFFFFF)V

    return-void
.end method

.method private J()V
    .locals 6

    const/high16 v5, 0x3f800000

    const/high16 v4, 0x3f000000

    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/bq/d;->A:[F

    iget-object v0, p0, Lmaps/bq/d;->A:[F

    const/4 v1, 0x0

    iget v2, p0, Lmaps/bq/d;->p:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/bq/d;->A:[F

    const/4 v1, 0x5

    iget v2, p0, Lmaps/bq/d;->q:I

    int-to-float v2, v2

    const/high16 v3, -0x41000000

    mul-float/2addr v2, v3

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/bq/d;->A:[F

    const/16 v1, 0xa

    aput v5, v0, v1

    iget-object v0, p0, Lmaps/bq/d;->A:[F

    const/16 v1, 0xf

    aput v5, v0, v1

    iget-object v0, p0, Lmaps/bq/d;->A:[F

    const/16 v1, 0xc

    iget v2, p0, Lmaps/bq/d;->p:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/bq/d;->A:[F

    const/16 v1, 0xd

    iget v2, p0, Lmaps/bq/d;->q:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    aput v2, v0, v1

    return-void
.end method

.method private K()V
    .locals 8

    const/16 v2, 0x10

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/bq/d;->y:[F

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/bq/d;->I()V

    :cond_0
    iget-object v0, p0, Lmaps/bq/d;->z:[F

    if-nez v0, :cond_1

    invoke-direct {p0}, Lmaps/bq/d;->H()V

    :cond_1
    iget-object v0, p0, Lmaps/bq/d;->A:[F

    if-nez v0, :cond_2

    invoke-direct {p0}, Lmaps/bq/d;->J()V

    :cond_2
    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/bq/d;->B:[F

    new-array v0, v2, [F

    iget-object v2, p0, Lmaps/bq/d;->A:[F

    iget-object v4, p0, Lmaps/bq/d;->z:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v2, p0, Lmaps/bq/d;->B:[F

    iget-object v6, p0, Lmaps/bq/d;->y:[F

    move v3, v1

    move-object v4, v0

    move v5, v1

    move v7, v1

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iput-object v0, p0, Lmaps/bq/d;->C:[F

    iget-object v0, p0, Lmaps/bq/d;->C:[F

    iget-object v2, p0, Lmaps/bq/d;->B:[F

    invoke-static {v0, v1, v2, v1}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    return-void
.end method

.method private final L()V
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->m:Z

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lmaps/bq/d;->h:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmaps/bq/d;->h:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on render thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public static a(F)F
    .locals 2

    float-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    double-to-float v0, v0

    sget v1, Lmaps/bq/d;->e:F

    mul-float/2addr v0, v1

    return v0
.end method

.method private b(Lmaps/bq/a;)V
    .locals 2

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0, p1}, Lmaps/bq/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lmaps/bq/d;->F:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/bq/d;->G:J

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmaps/bq/d;->b(Z)V

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/bq/a;->e()F

    move-result v0

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->e()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lmaps/bq/a;->d()F

    move-result v0

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->d()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/bq/d;->x:[D

    :cond_3
    iput-object p1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-direct {p0}, Lmaps/bq/d;->F()V

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->f()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-direct {p0}, Lmaps/bq/d;->E()V

    :goto_1
    invoke-virtual {p0}, Lmaps/bq/d;->a()V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/d;->H:Lmaps/bq/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/d;->H:Lmaps/bq/c;

    invoke-virtual {v0, p1}, Lmaps/bq/c;->a(Lmaps/bq/a;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v0

    iput-object v0, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    goto :goto_1
.end method

.method private b(Z)V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/bq/d;->s:Lmaps/t/bx;

    iput-object v1, p0, Lmaps/bq/d;->t:Lmaps/t/bx;

    iput-object v1, p0, Lmaps/bq/d;->u:Lmaps/t/bx;

    iput-object v1, p0, Lmaps/bq/d;->v:Lmaps/t/u;

    const/high16 v0, -0x40800000

    iput v0, p0, Lmaps/bq/d;->w:F

    iput-object v1, p0, Lmaps/bq/d;->y:[F

    iput-object v1, p0, Lmaps/bq/d;->B:[F

    iput-object v1, p0, Lmaps/bq/d;->C:[F

    if-eqz p1, :cond_0

    iput-object v1, p0, Lmaps/bq/d;->z:[F

    iput-object v1, p0, Lmaps/bq/d;->A:[F

    :cond_0
    return-void
.end method

.method private static b([FFFFFFF)V
    .locals 10

    neg-float v0, p1

    neg-float v1, p2

    neg-float v2, p3

    const/high16 v3, 0x3f800000

    invoke-static {v0, v1, v2}, Landroid/opengl/Matrix;->length(FFF)F

    move-result v4

    div-float/2addr v3, v4

    mul-float/2addr v0, v3

    mul-float/2addr v1, v3

    mul-float/2addr v2, v3

    mul-float v3, v1, p6

    mul-float v4, v2, p5

    sub-float/2addr v3, v4

    mul-float v4, v2, p4

    mul-float v5, v0, p6

    sub-float/2addr v4, v5

    mul-float v5, v0, p5

    mul-float v6, v1, p4

    sub-float/2addr v5, v6

    const/high16 v6, 0x3f800000

    invoke-static {v3, v4, v5}, Landroid/opengl/Matrix;->length(FFF)F

    move-result v7

    div-float/2addr v6, v7

    mul-float/2addr v3, v6

    mul-float/2addr v4, v6

    mul-float/2addr v5, v6

    mul-float v6, v4, v2

    mul-float v7, v5, v1

    sub-float/2addr v6, v7

    mul-float v7, v5, v0

    mul-float v8, v3, v2

    sub-float/2addr v7, v8

    mul-float v8, v3, v1

    mul-float v9, v4, v0

    sub-float/2addr v8, v9

    const/4 v9, 0x0

    aput v3, p0, v9

    const/4 v3, 0x1

    aput v6, p0, v3

    const/4 v3, 0x2

    neg-float v0, v0

    aput v0, p0, v3

    const/4 v0, 0x3

    const/4 v3, 0x0

    aput v3, p0, v0

    const/4 v0, 0x4

    aput v4, p0, v0

    const/4 v0, 0x5

    aput v7, p0, v0

    const/4 v0, 0x6

    neg-float v1, v1

    aput v1, p0, v0

    const/4 v0, 0x7

    const/4 v1, 0x0

    aput v1, p0, v0

    const/16 v0, 0x8

    aput v5, p0, v0

    const/16 v0, 0x9

    aput v8, p0, v0

    const/16 v0, 0xa

    neg-float v1, v2

    aput v1, p0, v0

    const/16 v0, 0xb

    const/4 v1, 0x0

    aput v1, p0, v0

    const/16 v0, 0xc

    const/4 v1, 0x0

    aput v1, p0, v0

    const/16 v0, 0xd

    const/4 v1, 0x0

    aput v1, p0, v0

    const/16 v0, 0xe

    const/4 v1, 0x0

    aput v1, p0, v0

    const/16 v0, 0xf

    const/high16 v1, 0x3f800000

    aput v1, p0, v0

    const/4 v0, 0x0

    neg-float v1, p1

    neg-float v2, p2

    neg-float v3, p3

    invoke-static {p0, v0, v1, v2, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    return-void
.end method

.method public static c(F)F
    .locals 2

    sget v0, Lmaps/bq/d;->c:F

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    const/high16 v0, 0x48800000

    div-float v0, p0, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    double-to-float v0, v0

    const v1, 0x42652ee1

    mul-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x42960000

    goto :goto_0
.end method


# virtual methods
.method public A()[F
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->y:[F

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/bq/d;->I()V

    :cond_0
    iget-object v0, p0, Lmaps/bq/d;->y:[F

    return-object v0
.end method

.method public B()[F
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->z:[F

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/bq/d;->H()V

    :cond_0
    iget-object v0, p0, Lmaps/bq/d;->z:[F

    return-object v0
.end method

.method public C()Lmaps/t/u;
    .locals 3

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->v:Lmaps/t/u;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->d()F

    move-result v0

    iget v1, p0, Lmaps/bq/d;->o:F

    const/high16 v2, 0x3f000000

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lmaps/bq/d;->d(F)Lmaps/t/u;

    move-result-object v0

    iput-object v0, p0, Lmaps/bq/d;->v:Lmaps/t/u;

    :cond_0
    iget-object v0, p0, Lmaps/bq/d;->v:Lmaps/t/u;

    return-object v0
.end method

.method public D()Lmaps/t/u;
    .locals 6

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->d()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/bq/d;->C()Lmaps/t/u;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->d()F

    move-result v0

    const v1, 0x3c8efa35

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    iget v2, p0, Lmaps/bq/d;->m:F

    div-float/2addr v1, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    float-to-double v4, v1

    add-double v1, v2, v4

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->atan(D)D

    move-result-wide v1

    double-to-float v1, v1

    sub-float v0, v1, v0

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->d()F

    move-result v1

    const v2, 0x42652ee1

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lmaps/bq/d;->d(F)Lmaps/t/u;

    move-result-object v0

    goto :goto_0
.end method

.method public a(FF)F
    .locals 3

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    mul-float v0, p2, p1

    iget v1, p0, Lmaps/bq/d;->m:F

    iget v2, p0, Lmaps/bq/d;->q:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method public a(Lmaps/t/bx;Z)F
    .locals 7

    invoke-direct {p0}, Lmaps/bq/d;->G()[D

    move-result-object v0

    invoke-virtual {p0}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v1

    iget-object v2, p0, Lmaps/bq/d;->E:Lmaps/t/bx;

    invoke-static {p1, v1, v2}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    if-eqz p2, :cond_0

    iget-object v1, p0, Lmaps/bq/d;->E:Lmaps/t/bx;

    iget-object v2, p0, Lmaps/bq/d;->E:Lmaps/t/bx;

    invoke-virtual {v1, v2}, Lmaps/t/bx;->i(Lmaps/t/bx;)V

    :cond_0
    iget-object v1, p0, Lmaps/bq/d;->E:Lmaps/t/bx;

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v1

    int-to-double v1, v1

    const/4 v3, 0x0

    aget-wide v3, v0, v3

    mul-double/2addr v1, v3

    iget-object v3, p0, Lmaps/bq/d;->E:Lmaps/t/bx;

    invoke-virtual {v3}, Lmaps/t/bx;->g()I

    move-result v3

    int-to-double v3, v3

    const/4 v5, 0x1

    aget-wide v5, v0, v5

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    iget-object v3, p0, Lmaps/bq/d;->E:Lmaps/t/bx;

    invoke-virtual {v3}, Lmaps/t/bx;->h()I

    move-result v3

    int-to-double v3, v3

    const/4 v5, 0x2

    aget-wide v5, v0, v5

    mul-double/2addr v3, v5

    add-double v0, v1, v3

    double-to-float v0, v0

    return v0
.end method

.method public a(FFFF)Lmaps/t/ak;
    .locals 21

    invoke-direct/range {p0 .. p0}, Lmaps/bq/d;->L()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bq/d;->C:[F

    if-nez v2, :cond_0

    invoke-direct/range {p0 .. p0}, Lmaps/bq/d;->K()V

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v8

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v9

    invoke-virtual {v2}, Lmaps/t/bx;->h()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v2

    sub-int v11, v2, v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v2

    sub-int v12, v2, v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bq/d;->D:[F

    const/4 v3, 0x0

    aput p1, v2, v3

    const/4 v3, 0x1

    aput p3, v2, v3

    const/4 v3, 0x2

    const/high16 v4, 0x3f800000

    aput v4, v2, v3

    const/4 v3, 0x3

    const/high16 v4, 0x3f800000

    aput v4, v2, v3

    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/bq/d;->C:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    const/high16 v3, 0x3f800000

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    const/4 v13, 0x6

    aget v13, v2, v13

    mul-float/2addr v3, v13

    float-to-double v13, v3

    const-wide/high16 v15, 0x3ff0000000000000L

    cmpl-double v3, v13, v15

    if-ltz v3, :cond_1

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    const-wide/high16 v15, 0x3ff0000000000000L

    const-wide/high16 v17, 0x3ff0000000000000L

    sub-double v13, v17, v13

    div-double v13, v15, v13

    int-to-double v15, v10

    mul-double v3, v4, v15

    int-to-double v15, v11

    add-double/2addr v3, v15

    mul-double/2addr v3, v13

    int-to-double v15, v8

    add-double/2addr v3, v15

    int-to-double v15, v10

    mul-double v5, v6, v15

    int-to-double v15, v12

    add-double/2addr v5, v15

    mul-double/2addr v5, v13

    int-to-double v13, v9

    add-double/2addr v5, v13

    new-instance v13, Lmaps/t/bx;

    double-to-int v3, v3

    double-to-int v4, v5

    invoke-direct {v13, v3, v4}, Lmaps/t/bx;-><init>(II)V

    const/4 v3, 0x0

    aput p2, v2, v3

    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/bq/d;->C:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    const/high16 v3, 0x3f800000

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    const/4 v14, 0x6

    aget v14, v2, v14

    mul-float/2addr v3, v14

    float-to-double v14, v3

    const-wide/high16 v16, 0x3ff0000000000000L

    cmpl-double v3, v14, v16

    if-ltz v3, :cond_2

    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const-wide/high16 v16, 0x3ff0000000000000L

    const-wide/high16 v18, 0x3ff0000000000000L

    sub-double v14, v18, v14

    div-double v14, v16, v14

    int-to-double v0, v10

    move-wide/from16 v16, v0

    mul-double v3, v4, v16

    int-to-double v0, v11

    move-wide/from16 v16, v0

    add-double v3, v3, v16

    mul-double/2addr v3, v14

    int-to-double v0, v8

    move-wide/from16 v16, v0

    add-double v3, v3, v16

    int-to-double v0, v10

    move-wide/from16 v16, v0

    mul-double v5, v6, v16

    int-to-double v0, v12

    move-wide/from16 v16, v0

    add-double v5, v5, v16

    mul-double/2addr v5, v14

    int-to-double v14, v9

    add-double/2addr v5, v14

    new-instance v14, Lmaps/t/bx;

    double-to-int v3, v3

    double-to-int v4, v5

    invoke-direct {v14, v3, v4}, Lmaps/t/bx;-><init>(II)V

    const/4 v3, 0x1

    aput p4, v2, v3

    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/bq/d;->C:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    const/high16 v3, 0x3f800000

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    const/4 v15, 0x6

    aget v15, v2, v15

    mul-float/2addr v3, v15

    float-to-double v15, v3

    const-wide/high16 v17, 0x3ff0000000000000L

    cmpl-double v3, v15, v17

    if-ltz v3, :cond_3

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_3
    const-wide/high16 v17, 0x3ff0000000000000L

    const-wide/high16 v19, 0x3ff0000000000000L

    sub-double v15, v19, v15

    div-double v15, v17, v15

    int-to-double v0, v10

    move-wide/from16 v17, v0

    mul-double v3, v4, v17

    int-to-double v0, v11

    move-wide/from16 v17, v0

    add-double v3, v3, v17

    mul-double/2addr v3, v15

    int-to-double v0, v8

    move-wide/from16 v17, v0

    add-double v3, v3, v17

    int-to-double v0, v10

    move-wide/from16 v17, v0

    mul-double v5, v6, v17

    int-to-double v0, v12

    move-wide/from16 v17, v0

    add-double v5, v5, v17

    mul-double/2addr v5, v15

    int-to-double v15, v9

    add-double/2addr v5, v15

    new-instance v15, Lmaps/t/bx;

    double-to-int v3, v3

    double-to-int v4, v5

    invoke-direct {v15, v3, v4}, Lmaps/t/bx;-><init>(II)V

    const/4 v3, 0x0

    aput p1, v2, v3

    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/bq/d;->C:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    const/high16 v3, 0x3f800000

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    const/16 v16, 0x6

    aget v2, v2, v16

    mul-float/2addr v2, v3

    float-to-double v2, v2

    const-wide/high16 v16, 0x3ff0000000000000L

    cmpl-double v16, v2, v16

    if-ltz v16, :cond_4

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_4
    const-wide/high16 v16, 0x3ff0000000000000L

    const-wide/high16 v18, 0x3ff0000000000000L

    sub-double v2, v18, v2

    div-double v2, v16, v2

    int-to-double v0, v10

    move-wide/from16 v16, v0

    mul-double v4, v4, v16

    int-to-double v0, v11

    move-wide/from16 v16, v0

    add-double v4, v4, v16

    mul-double/2addr v4, v2

    int-to-double v0, v8

    move-wide/from16 v16, v0

    add-double v4, v4, v16

    int-to-double v10, v10

    mul-double/2addr v6, v10

    int-to-double v10, v12

    add-double/2addr v6, v10

    mul-double/2addr v2, v6

    int-to-double v6, v9

    add-double/2addr v2, v6

    new-instance v6, Lmaps/t/bx;

    double-to-int v4, v4

    double-to-int v2, v2

    invoke-direct {v6, v4, v2}, Lmaps/t/bx;-><init>(II)V

    invoke-static {v6, v15, v13, v14}, Lmaps/t/ak;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/ak;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public a(IIF)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    sget-object v0, Lmaps/bq/d;->F:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/bq/d;->G:J

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lmaps/bq/d;->p:I

    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lmaps/bq/d;->q:I

    iput p3, p0, Lmaps/bq/d;->r:F

    invoke-direct {p0}, Lmaps/bq/d;->F()V

    invoke-direct {p0}, Lmaps/bq/d;->E()V

    invoke-direct {p0, v2}, Lmaps/bq/d;->b(Z)V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/d;->H:Lmaps/bq/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/d;->H:Lmaps/bq/c;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/bq/c;->a(IIF)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/bq/a;)V
    .locals 0

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    invoke-direct {p0, p1}, Lmaps/bq/d;->b(Lmaps/bq/a;)V

    return-void
.end method

.method public a(Lmaps/t/bx;)V
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {p1, v0}, Lmaps/t/bx;->b(Lmaps/t/bx;)V

    return-void
.end method

.method public a(Lmaps/t/bx;[F)V
    .locals 7

    const/high16 v4, 0x40000000

    const/4 v1, 0x4

    const/4 v6, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/bq/d;->B:[F

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/bq/d;->K()V

    :cond_0
    invoke-virtual {p1}, Lmaps/t/bx;->f()I

    move-result v0

    iget-object v2, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v2

    sub-int/2addr v0, v2

    const/high16 v2, 0x20000000

    if-lt v0, v2, :cond_2

    sub-int/2addr v0, v4

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->h()I

    move-result v2

    int-to-float v0, v0

    aput v0, p2, v3

    invoke-virtual {p1}, Lmaps/t/bx;->g()I

    move-result v0

    iget-object v4, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v4}, Lmaps/t/bx;->g()I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v0, v0

    aput v0, p2, v6

    const/4 v0, 0x2

    invoke-virtual {p1}, Lmaps/t/bx;->h()I

    move-result v4

    int-to-float v4, v4

    aput v4, p2, v0

    const/4 v0, 0x3

    int-to-float v2, v2

    aput v2, p2, v0

    iget-object v2, p0, Lmaps/bq/d;->B:[F

    move-object v0, p2

    move-object v4, p2

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    const/high16 v0, 0x3f800000

    const/4 v2, 0x7

    aget v2, p2, v2

    div-float/2addr v0, v2

    aget v1, p2, v1

    mul-float/2addr v1, v0

    aput v1, p2, v3

    const/4 v1, 0x5

    aget v1, p2, v1

    mul-float/2addr v0, v1

    aput v0, p2, v6

    return-void

    :cond_2
    const/high16 v2, -0x20000000

    if-ge v0, v2, :cond_1

    add-int/2addr v0, v4

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    iget-boolean v0, p0, Lmaps/bq/d;->j:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/bq/d;->j:Z

    sget-object v0, Lmaps/bq/d;->F:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/bq/d;->G:J

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/d;->H:Lmaps/bq/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bq/d;->H:Lmaps/bq/c;

    invoke-virtual {v0, p1}, Lmaps/bq/c;->a(Z)V

    :cond_0
    return-void
.end method

.method public b(FF)F
    .locals 2

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget v0, p0, Lmaps/bq/d;->m:F

    mul-float/2addr v0, p1

    iget v1, p0, Lmaps/bq/d;->q:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    div-float/2addr v0, p2

    return v0
.end method

.method public b(F)V
    .locals 8

    const/4 v7, 0x0

    const-wide/high16 v5, 0x3fe0000000000000L

    invoke-static {v7, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x42b40000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, Lmaps/bq/d;->o:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    iput v0, p0, Lmaps/bq/d;->o:F

    sget-object v1, Lmaps/bq/d;->F:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v1

    iput-wide v1, p0, Lmaps/bq/d;->G:J

    iget v1, p0, Lmaps/bq/d;->o:F

    float-to-double v1, v1

    const-wide v3, 0x3f91df46a2529d39L

    mul-double/2addr v1, v3

    mul-double/2addr v1, v5

    invoke-static {v1, v2}, Ljava/lang/Math;->tan(D)D

    move-result-wide v1

    div-double v1, v5, v1

    double-to-float v1, v1

    iput v1, p0, Lmaps/bq/d;->m:F

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lmaps/bq/d;->b(Z)V

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->f()F

    move-result v1

    cmpl-float v1, v1, v7

    if-eqz v1, :cond_0

    new-instance v1, Lmaps/t/bx;

    invoke-direct {v1}, Lmaps/t/bx;-><init>()V

    iput-object v1, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-direct {p0}, Lmaps/bq/d;->E()V

    :cond_0
    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/bq/d;->H:Lmaps/bq/c;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/bq/d;->H:Lmaps/bq/c;

    invoke-virtual {v1, v0}, Lmaps/bq/c;->a(F)V

    :cond_1
    return-void
.end method

.method public b(Lmaps/t/bx;)[I
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    const/16 v0, 0x8

    new-array v0, v0, [F

    invoke-virtual {p0, p1, v0}, Lmaps/bq/d;->a(Lmaps/t/bx;[F)V

    const/4 v1, 0x2

    new-array v1, v1, [I

    aget v2, v0, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v1, v3

    aget v0, v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    aput v0, v1, v4

    return-object v1
.end method

.method public c(FF)F
    .locals 4

    const/high16 v0, 0x41f00000

    div-float v1, p1, p2

    const/high16 v2, 0x43800000

    iget v3, p0, Lmaps/bq/d;->r:F

    mul-float/2addr v2, v3

    mul-float/2addr v1, v2

    invoke-static {v1}, Lmaps/bq/d;->a(F)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public d(FF)Lmaps/t/bx;
    .locals 11

    const/4 v1, 0x4

    const/4 v3, 0x0

    const/high16 v6, 0x3f800000

    const-wide/high16 v9, 0x3ff0000000000000L

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->C:[F

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/bq/d;->K()V

    :cond_0
    iget-object v0, p0, Lmaps/bq/d;->D:[F

    aput p1, v0, v3

    const/4 v2, 0x1

    aput p2, v0, v2

    const/4 v2, 0x2

    aput v6, v0, v2

    const/4 v2, 0x3

    aput v6, v0, v2

    iget-object v2, p0, Lmaps/bq/d;->C:[F

    move-object v4, v0

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    const/4 v2, 0x7

    aget v2, v0, v2

    div-float v2, v6, v2

    aget v1, v0, v1

    mul-float/2addr v1, v2

    float-to-double v3, v1

    const/4 v1, 0x5

    aget v1, v0, v1

    mul-float/2addr v1, v2

    float-to-double v5, v1

    const/4 v1, 0x6

    aget v0, v0, v1

    mul-float/2addr v0, v2

    float-to-double v0, v0

    cmpl-double v2, v0, v9

    if-ltz v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v7

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v8

    invoke-virtual {v2}, Lmaps/t/bx;->h()I

    move-result v2

    sub-double v0, v9, v0

    div-double v0, v9, v0

    int-to-double v9, v2

    mul-double/2addr v3, v9

    iget-object v9, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v9}, Lmaps/t/bx;->f()I

    move-result v9

    int-to-double v9, v9

    add-double/2addr v3, v9

    int-to-double v9, v7

    sub-double/2addr v3, v9

    mul-double/2addr v3, v0

    int-to-double v9, v7

    add-double/2addr v3, v9

    int-to-double v9, v2

    mul-double/2addr v5, v9

    iget-object v2, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v2

    int-to-double v9, v2

    add-double/2addr v5, v9

    int-to-double v9, v8

    sub-double/2addr v5, v9

    mul-double/2addr v0, v5

    int-to-double v5, v8

    add-double v1, v0, v5

    new-instance v0, Lmaps/t/bx;

    double-to-int v3, v3

    double-to-int v1, v1

    invoke-direct {v0, v3, v1}, Lmaps/t/bx;-><init>(II)V

    goto :goto_0
.end method

.method public d(F)Lmaps/t/u;
    .locals 6

    const/4 v5, 0x0

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    invoke-virtual {p0}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->h()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Lmaps/bq/d;->c(F)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->d()F

    move-result v1

    sub-float v1, v0, v1

    const v2, 0x3c8efa35

    mul-float/2addr v1, v2

    iget v2, p0, Lmaps/bq/d;->q:I

    int-to-float v2, v2

    iget v3, p0, Lmaps/bq/d;->m:F

    mul-float/2addr v2, v3

    float-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->tan(D)D

    move-result-wide v3

    double-to-float v1, v3

    mul-float/2addr v1, v2

    iget v2, p0, Lmaps/bq/d;->q:I

    int-to-float v2, v2

    const/high16 v3, 0x3f000000

    mul-float/2addr v2, v3

    sub-float v1, v2, v1

    invoke-static {v1}, Landroid/util/FloatMath;->ceil(F)F

    move-result v1

    iget v2, p0, Lmaps/bq/d;->q:I

    int-to-float v2, v2

    invoke-virtual {p0, v5, v2}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v2

    iget v3, p0, Lmaps/bq/d;->p:I

    int-to-float v3, v3

    iget v4, p0, Lmaps/bq/d;->q:I

    int-to-float v4, v4

    invoke-virtual {p0, v3, v4}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {p0, v5, v1}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v4

    iget v5, p0, Lmaps/bq/d;->p:I

    int-to-float v5, v5

    invoke-virtual {p0, v5, v1}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v5

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-nez v5, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v4}, Lmaps/bq/a;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " farAngle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " size: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lmaps/bq/d;->p:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lmaps/bq/d;->q:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " top:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v2, v3, v4, v5}, Lmaps/t/u;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)Lmaps/t/u;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/bq/d;

    if-eqz v2, :cond_3

    check-cast p1, Lmaps/bq/d;

    iget v2, p0, Lmaps/bq/d;->l:F

    iget v3, p1, Lmaps/bq/d;->l:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget-object v2, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    iget-object v3, p1, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v2, v3}, Lmaps/bq/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lmaps/bq/d;->o:F

    iget v3, p1, Lmaps/bq/d;->o:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lmaps/bq/d;->q:I

    iget v3, p1, Lmaps/bq/d;->q:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lmaps/bq/d;->p:I

    iget v3, p1, Lmaps/bq/d;->p:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lmaps/bq/d;->r:F

    iget v3, p1, Lmaps/bq/d;->r:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public g()J
    .locals 2

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-wide v0, p0, Lmaps/bq/d;->G:J

    return-wide v0
.end method

.method public h()Lmaps/t/bx;
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-static {v0}, Lmaps/t/bx;->a(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lmaps/bq/d;->l:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/bq/d;->q:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/bq/d;->p:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/bq/d;->o:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lmaps/bq/d;->r:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public i()Lmaps/t/bx;
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v0

    return-object v0
.end method

.method public j()F
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->f()F

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/bq/d;->j:Z

    return v0
.end method

.method public l()I
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget v0, p0, Lmaps/bq/d;->p:I

    return v0
.end method

.method public m()I
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget v0, p0, Lmaps/bq/d;->q:I

    return v0
.end method

.method public n()F
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget v0, p0, Lmaps/bq/d;->r:F

    return v0
.end method

.method public o()F
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget v0, p0, Lmaps/bq/d;->o:F

    return v0
.end method

.method public p()F
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget v0, p0, Lmaps/bq/d;->l:F

    return v0
.end method

.method public q()F
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->e()F

    move-result v0

    return v0
.end method

.method public r()F
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->d()F

    move-result v0

    return v0
.end method

.method public s()F
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->a()F

    move-result v0

    return v0
.end method

.method public t()F
    .locals 1

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->a()F

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Lmaps/t/bx;

    iget-object v1, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v1}, Lmaps/t/bx;->f()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v3}, Lmaps/t/bx;->h()I

    move-result v3

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lmaps/t/bx;-><init>(III)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/bq/d;->l:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->e()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->d()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/bq/d;->o:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Lmaps/t/bx;
    .locals 8

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->s:Lmaps/t/bx;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/bq/d;->G()[D

    move-result-object v0

    new-instance v1, Lmaps/t/bx;

    iget v2, p0, Lmaps/bq/d;->l:F

    neg-float v2, v2

    float-to-double v2, v2

    const/4 v4, 0x0

    aget-wide v4, v0, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    iget v3, p0, Lmaps/bq/d;->l:F

    neg-float v3, v3

    float-to-double v3, v3

    const/4 v5, 0x1

    aget-wide v5, v0, v5

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    iget v4, p0, Lmaps/bq/d;->l:F

    neg-float v4, v4

    float-to-double v4, v4

    const/4 v6, 0x2

    aget-wide v6, v0, v6

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v0, v4

    invoke-direct {v1, v2, v3, v0}, Lmaps/t/bx;-><init>(III)V

    iput-object v1, p0, Lmaps/bq/d;->s:Lmaps/t/bx;

    iget-object v0, p0, Lmaps/bq/d;->s:Lmaps/t/bx;

    iget-object v1, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    iget-object v2, p0, Lmaps/bq/d;->s:Lmaps/t/bx;

    invoke-static {v0, v1, v2}, Lmaps/t/bx;->a(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    :cond_0
    iget-object v0, p0, Lmaps/bq/d;->s:Lmaps/t/bx;

    return-object v0
.end method

.method public v()Lmaps/t/bx;
    .locals 8

    const-wide/high16 v6, 0x40f0000000000000L

    const-wide v4, 0x4076800000000000L

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->u:Lmaps/t/bx;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v0

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->d()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v1

    iget-object v2, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v2

    if-ne v1, v2, :cond_4

    invoke-virtual {v0}, Lmaps/t/bx;->g()I

    move-result v1

    iget-object v2, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v2

    if-ne v1, v2, :cond_4

    :cond_0
    iget-object v0, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v0}, Lmaps/bq/a;->e()F

    move-result v0

    neg-float v0, v0

    float-to-double v0, v0

    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_3

    add-double/2addr v0, v4

    :cond_1
    :goto_0
    const-wide v2, 0x3f91df46a2529d39L

    mul-double/2addr v0, v2

    new-instance v2, Lmaps/t/bx;

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double/2addr v3, v6

    double-to-int v3, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v6

    double-to-int v0, v0

    invoke-direct {v2, v3, v0}, Lmaps/t/bx;-><init>(II)V

    iput-object v2, p0, Lmaps/bq/d;->u:Lmaps/t/bx;

    :cond_2
    :goto_1
    iget-object v0, p0, Lmaps/bq/d;->u:Lmaps/t/bx;

    return-object v0

    :cond_3
    cmpl-double v2, v0, v4

    if-ltz v2, :cond_1

    sub-double/2addr v0, v4

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v1, v0}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    sget-object v1, Lmaps/bq/d;->f:Lmaps/t/bx;

    invoke-virtual {v0, v1}, Lmaps/t/bx;->g(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    const/high16 v1, 0x47800000

    invoke-static {v0, v1, v0}, Lmaps/t/bx;->b(Lmaps/t/bx;FLmaps/t/bx;)V

    iput-object v0, p0, Lmaps/bq/d;->u:Lmaps/t/bx;

    goto :goto_1
.end method

.method public w()Lmaps/t/bx;
    .locals 10

    const-wide/high16 v8, 0x40f0000000000000L

    const-wide/16 v6, 0x0

    const-wide v4, 0x4076800000000000L

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget-object v0, p0, Lmaps/bq/d;->t:Lmaps/t/bx;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v0

    iget-object v1, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v1}, Lmaps/bq/a;->d()F

    move-result v1

    float-to-double v1, v1

    cmpl-double v1, v1, v6

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lmaps/t/bx;->f()I

    move-result v1

    iget-object v2, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->f()I

    move-result v2

    if-ne v1, v2, :cond_4

    invoke-virtual {v0}, Lmaps/t/bx;->g()I

    move-result v1

    iget-object v2, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v2}, Lmaps/t/bx;->g()I

    move-result v2

    if-ne v1, v2, :cond_4

    :cond_0
    const-wide v0, 0x4056800000000000L

    iget-object v2, p0, Lmaps/bq/d;->i:Lmaps/bq/a;

    invoke-virtual {v2}, Lmaps/bq/a;->e()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    cmpg-double v2, v0, v6

    if-gez v2, :cond_3

    add-double/2addr v0, v4

    :cond_1
    :goto_0
    const-wide v2, 0x3f91df46a2529d39L

    mul-double/2addr v0, v2

    new-instance v2, Lmaps/t/bx;

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double/2addr v3, v8

    double-to-int v3, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v8

    double-to-int v0, v0

    invoke-direct {v2, v3, v0}, Lmaps/t/bx;-><init>(II)V

    iput-object v2, p0, Lmaps/bq/d;->t:Lmaps/t/bx;

    :cond_2
    :goto_1
    iget-object v0, p0, Lmaps/bq/d;->t:Lmaps/t/bx;

    return-object v0

    :cond_3
    cmpl-double v2, v0, v4

    if-ltz v2, :cond_1

    sub-double/2addr v0, v4

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lmaps/bq/d;->k:Lmaps/t/bx;

    invoke-virtual {v1, v0}, Lmaps/t/bx;->f(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    const/high16 v1, 0x46800000

    invoke-static {v0, v1, v0}, Lmaps/t/bx;->b(Lmaps/t/bx;FLmaps/t/bx;)V

    invoke-virtual {p0}, Lmaps/bq/d;->v()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/t/bx;->g(Lmaps/t/bx;)Lmaps/t/bx;

    move-result-object v0

    const/high16 v1, 0x47800000

    invoke-static {v0, v1, v0}, Lmaps/t/bx;->b(Lmaps/t/bx;FLmaps/t/bx;)V

    iput-object v0, p0, Lmaps/bq/d;->t:Lmaps/t/bx;

    goto :goto_1
.end method

.method public x()F
    .locals 2

    iget v0, p0, Lmaps/bq/d;->w:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/high16 v0, 0x3f800000

    invoke-virtual {p0}, Lmaps/bq/d;->u()Lmaps/t/bx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/bx;->h()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lmaps/bq/d;->w:F

    :cond_0
    iget v0, p0, Lmaps/bq/d;->w:F

    return v0
.end method

.method public y()F
    .locals 2

    invoke-direct {p0}, Lmaps/bq/d;->L()V

    iget v0, p0, Lmaps/bq/d;->p:I

    int-to-float v0, v0

    const/high16 v1, 0x3f800000

    mul-float/2addr v0, v1

    iget v1, p0, Lmaps/bq/d;->q:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public z()F
    .locals 2

    const/high16 v0, 0x3f800000

    iget v1, p0, Lmaps/bq/d;->l:F

    invoke-virtual {p0, v0, v1}, Lmaps/bq/d;->a(FF)F

    move-result v0

    return v0
.end method
