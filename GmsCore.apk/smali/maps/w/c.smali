.class Lmaps/w/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field final synthetic a:Lmaps/w/n;


# direct methods
.method constructor <init>(Lmaps/w/n;)V
    .locals 0

    iput-object p1, p0, Lmaps/w/c;->a:Lmaps/w/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4

    const/high16 v3, 0x40400000

    const/high16 v2, 0x3f800000

    const/high16 v0, 0x40a00000

    mul-float/2addr v0, p1

    cmpg-float v1, v0, v2

    if-gez v1, :cond_0

    iget-object v1, p0, Lmaps/w/c;->a:Lmaps/w/n;

    iget v1, v1, Lmaps/w/n;->a:F

    sub-float v0, v2, v0

    mul-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    cmpg-float v1, v0, v3

    if-gez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/high16 v1, 0x40800000

    cmpg-float v1, v0, v1

    if-gez v1, :cond_2

    sub-float/2addr v0, v3

    iget-object v1, p0, Lmaps/w/c;->a:Lmaps/w/n;

    iget v1, v1, Lmaps/w/n;->b:F

    mul-float/2addr v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmaps/w/c;->a:Lmaps/w/n;

    iget v0, v0, Lmaps/w/n;->b:F

    goto :goto_0
.end method
