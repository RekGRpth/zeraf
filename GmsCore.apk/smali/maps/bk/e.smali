.class Lmaps/bk/e;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bx/b;


# instance fields
.field final synthetic a:Lmaps/bk/b;


# direct methods
.method private constructor <init>(Lmaps/bk/b;)V
    .locals 0

    iput-object p1, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmaps/bk/b;Lmaps/bk/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/bk/e;-><init>(Lmaps/bk/b;)V

    return-void
.end method

.method private a(Lmaps/bk/a;)Lmaps/l/al;
    .locals 6

    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    iget-object v1, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    invoke-static {v0, v1}, Lmaps/bk/b;->a(Lmaps/bk/b;Lmaps/t/ah;)Lmaps/l/al;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->d(Lmaps/bk/b;)Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v1, v0}, Lmaps/l/al;->a(Lmaps/ae/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    iget-object v2, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    invoke-static {v0, v2, v1}, Lmaps/bk/b;->a(Lmaps/bk/b;Lmaps/t/ah;Lmaps/l/al;)Z

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    if-nez v1, :cond_6

    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->e(Lmaps/bk/b;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    if-eqz v0, :cond_4

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-boolean v1, Lmaps/ae/h;->k:Z

    if-eqz v1, :cond_1

    const-string v2, "Inconsistent state: shouldn\'t have pending local request."

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmaps/bk/a;

    iget-boolean v1, v1, Lmaps/bk/a;->b:Z

    invoke-static {v2, v1}, Lmaps/ah/d;->b(Ljava/lang/String;Z)V

    :cond_1
    sget-boolean v1, Lmaps/ae/h;->k:Z

    if-eqz v1, :cond_2

    const-string v2, "Inconsistent state: the old pending request should be from older provider."

    iget-object v1, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v1}, Lmaps/bk/b;->f(Lmaps/bk/b;)Lmaps/bk/f;

    move-result-object v3

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmaps/bk/a;

    invoke-virtual {v3, v1}, Lmaps/bk/f;->a(Lmaps/bk/a;)Z

    move-result v1

    invoke-static {v2, v1}, Lmaps/ah/d;->b(Ljava/lang/String;Z)V

    :cond_2
    iget-boolean v1, p1, Lmaps/bk/a;->b:Z

    if-eqz v1, :cond_3

    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->e(Lmaps/bk/b;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->g(Lmaps/bk/b;)I

    :goto_1
    invoke-static {}, Lmaps/bk/b;->l()Lmaps/l/al;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v1}, Lmaps/bk/b;->e(Lmaps/bk/b;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {p1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->f(Lmaps/bk/b;)Lmaps/bk/f;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->f(Lmaps/bk/b;)Lmaps/bk/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/bk/f;->a(Lmaps/bk/a;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->e(Lmaps/bk/b;)Ljava/util/Map;

    move-result-object v0

    iget-object v3, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p1, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->h(Lmaps/bk/b;)I

    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    iget-object v3, p1, Lmaps/bk/a;->a:Lmaps/t/ah;

    iget-boolean v4, p1, Lmaps/bk/a;->b:Z

    iget-object v5, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v5}, Lmaps/bk/b;->i(Lmaps/bk/b;)Lmaps/bx/b;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lmaps/bk/b;->a(Lmaps/bk/b;Lmaps/t/ah;ZLmaps/bx/b;)V

    :cond_5
    monitor-exit v2

    move-object v0, v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_6
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private a(Lmaps/bk/a;Z)V
    .locals 2

    :goto_0
    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->f(Lmaps/bk/b;)Lmaps/bk/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/bk/f;->a(Lmaps/bk/a;Z)Lmaps/bk/a;

    move-result-object p1

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lmaps/bk/e;->a(Lmaps/bk/a;)Lmaps/l/al;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/bk/b;->l()Lmaps/l/al;

    move-result-object v1

    if-eq v0, v1, :cond_2

    const/4 p2, 0x1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/t/ah;ILmaps/t/o;)V
    .locals 7

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    sget-object v0, Lmaps/an/y;->h:Lmaps/t/ah;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->f(Lmaps/bk/b;)Lmaps/bk/f;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->j(Lmaps/bk/b;)Lmaps/bk/a;

    move-result-object v0

    iget-object v3, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lmaps/bk/b;->a(Lmaps/bk/b;Lmaps/bk/a;)Lmaps/bk/a;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, v0, v2}, Lmaps/bk/e;->a(Lmaps/bk/a;Z)V

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v0}, Lmaps/bk/b;->e(Lmaps/bk/b;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    if-nez v0, :cond_2

    const-string v0, "TileFetcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received an unknown tile "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v1}, Lmaps/bk/b;->f(Lmaps/bk/b;)Lmaps/bk/f;

    move-result-object v5

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmaps/bk/a;

    invoke-virtual {v5, v1}, Lmaps/bk/f;->a(Lmaps/bk/a;)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    move v2, v3

    :goto_1
    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v1}, Lmaps/bk/b;->e(Lmaps/bk/b;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v1}, Lmaps/bk/b;->g(Lmaps/bk/b;)I

    :cond_3
    if-eqz v2, :cond_4

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmaps/bk/a;

    invoke-direct {p0, v1, v3}, Lmaps/bk/e;->a(Lmaps/bk/a;Z)V

    :cond_4
    if-eqz v4, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long v0, v1, v5

    iget-object v2, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v2, p1, v4, v0, v1}, Lmaps/bk/b;->a(Lmaps/bk/b;Lmaps/t/ah;Lmaps/l/al;J)V

    goto :goto_0

    :cond_5
    const/4 v1, 0x3

    if-ne p2, v1, :cond_6

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmaps/bk/a;

    iget-boolean v1, v1, Lmaps/bk/a;->b:Z

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lmaps/bk/e;->a:Lmaps/bk/b;

    invoke-static {v1, p1, p2, p3}, Lmaps/bk/b;->a(Lmaps/bk/b;Lmaps/t/ah;ILmaps/t/o;)Lmaps/l/al;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-static {}, Lmaps/bk/b;->l()Lmaps/l/al;

    move-result-object v1

    if-eq v4, v1, :cond_7

    move v1, v2

    :goto_2
    move v3, v1

    move v1, v2

    goto :goto_1

    :cond_7
    move v1, v3

    goto :goto_2
.end method
