.class public Lmaps/ak/l;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ak/a;


# instance fields
.field final synthetic a:Lmaps/ak/n;

.field private b:Ljava/util/Vector;

.field private c:Z

.field private final d:Lmaps/bb/c;


# direct methods
.method private constructor <init>(Lmaps/ak/n;)V
    .locals 2

    iput-object p1, p0, Lmaps/ak/l;->a:Lmaps/ak/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lmaps/ak/l;->b:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ak/l;->c:Z

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/c/w;->a:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    iput-object v0, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ak/n;Lmaps/ak/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ak/l;-><init>(Lmaps/ak/n;)V

    return-void
.end method

.method private constructor <init>(Lmaps/ak/n;Lmaps/bb/c;)V
    .locals 1

    iput-object p1, p0, Lmaps/ak/l;->a:Lmaps/ak/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lmaps/ak/l;->b:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ak/l;->c:Z

    invoke-virtual {p2}, Lmaps/bb/c;->a()Lmaps/bb/c;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ak/n;Lmaps/bb/c;Lmaps/ak/g;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/ak/l;-><init>(Lmaps/ak/n;Lmaps/bb/c;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    return-void
.end method

.method static synthetic a(Lmaps/ak/l;)V
    .locals 0

    invoke-direct {p0}, Lmaps/ak/l;->f()V

    return-void
.end method

.method static synthetic a(Lmaps/ak/l;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ak/l;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lmaps/ak/l;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ak/l;->b(Z)V

    return-void
.end method

.method static synthetic b(Lmaps/ak/l;Z)Lmaps/ak/k;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/ak/l;->c(Z)Lmaps/ak/k;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lmaps/ak/l;)Lmaps/bb/c;
    .locals 1

    iget-object v0, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    return-object v0
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    const/16 v1, 0x24

    invoke-virtual {v0, v1, p1}, Lmaps/bb/c;->a(IZ)Lmaps/bb/c;

    return-void
.end method

.method private c(Z)Lmaps/ak/k;
    .locals 10

    const/4 v0, 0x0

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lmaps/ak/l;->b:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_0

    monitor-exit p0

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v2, p0, Lmaps/ak/l;->c:Z

    if-nez v2, :cond_1

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v2, Lmaps/ak/k;

    iget-object v0, p0, Lmaps/ak/l;->a:Lmaps/ak/n;

    iget-object v3, p0, Lmaps/ak/l;->b:Ljava/util/Vector;

    iget-object v4, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    invoke-direct {v2, v0, v3, v4}, Lmaps/ak/k;-><init>(Lmaps/ak/n;Ljava/util/Vector;Lmaps/bb/c;)V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lmaps/ak/l;->b:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ak/l;->c:Z

    sget-boolean v0, Lmaps/ae/h;->n:Z

    if-eqz v0, :cond_3

    iget-object v0, v2, Lmaps/ak/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ak/j;

    invoke-interface {v0}, Lmaps/ak/j;->o()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DRD:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Lmaps/ak/j;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lmaps/ak/l;->a:Lmaps/ak/n;

    invoke-static {v6}, Lmaps/ak/n;->b(Lmaps/ak/n;)Lmaps/ae/d;

    move-result-object v6

    invoke-interface {v6}, Lmaps/ae/d;->b()J

    move-result-wide v6

    invoke-interface {v0}, Lmaps/ak/j;->o()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lmaps/bh/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    move-object v0, v2

    goto/16 :goto_0
.end method

.method static synthetic c(Lmaps/ak/l;)Z
    .locals 1

    invoke-direct {p0}, Lmaps/ak/l;->g()Z

    move-result v0

    return v0
.end method

.method private f()V
    .locals 3

    const/16 v2, 0x19

    iget-object v0, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    invoke-virtual {v0, v2}, Lmaps/bb/c;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lmaps/bb/c;->i(II)V

    :cond_0
    return-void
.end method

.method private g()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ak/l;->c:Z

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    const/16 v1, 0x19

    invoke-virtual {v0, v1, p1}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    return-void
.end method

.method public final a(I[BZZ)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lmaps/ak/l;->a(I[BZZZ)V

    return-void
.end method

.method public final a(I[BZZZ)V
    .locals 7

    new-instance v0, Lmaps/ak/b;

    const/4 v6, 0x0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lmaps/ak/b;-><init>(I[BZZZLjava/lang/Object;)V

    invoke-virtual {p0, v0}, Lmaps/ak/l;->a(Lmaps/ak/j;)V

    return-void
.end method

.method public a(Lmaps/ak/i;)V
    .locals 1

    invoke-static {}, Lmaps/ak/n;->x()Lmaps/ak/n;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/ak/n;->a(Lmaps/ak/i;)V

    return-void
.end method

.method public a(Lmaps/ak/j;)V
    .locals 4

    const/4 v3, 0x1

    sget-boolean v0, Lmaps/ae/h;->n:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lmaps/ak/j;->k_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/ak/l;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/l;->a:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->b(Lmaps/ak/n;)Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Lmaps/ak/j;->a(Ljava/lang/Long;)V

    :cond_0
    sget-boolean v0, Lmaps/ae/h;->f:Z

    if-eqz v0, :cond_1

    const-string v0, "REQUEST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Add Data Request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lmaps/ak/j;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lmaps/ak/l;->a:Lmaps/ak/n;

    iget-boolean v0, v0, Lmaps/ak/n;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ak/l;->a:Lmaps/ak/n;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lmaps/ak/n;->a(IZLjava/lang/String;)V

    :cond_2
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lmaps/ak/j;->k_()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ak/l;->c:Z

    :cond_3
    iget-object v0, p0, Lmaps/ak/l;->b:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p1}, Lmaps/ak/j;->k_()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lmaps/ak/l;->e()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lmaps/ak/l;->a:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->e:Lmaps/ak/m;

    invoke-static {v0}, Lmaps/ak/m;->a(Lmaps/ak/m;)V

    :cond_4
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/ak/l;->d:Lmaps/bb/c;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lmaps/bb/c;->a(IZ)Lmaps/bb/c;

    return-void
.end method

.method public d()J
    .locals 2

    invoke-static {}, Lmaps/ak/n;->x()Lmaps/ak/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ak/n;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized e()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lmaps/ak/n;->x()Lmaps/ak/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ak/n;->i()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
