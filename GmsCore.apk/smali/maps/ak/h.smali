.class public Lmaps/ak/h;
.super Ljava/lang/Object;


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Boolean;

.field private p:I

.field private q:Lmaps/ak/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/ak/n;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmaps/ak/h;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ak/h;->f:Z

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ak/h;->p:I

    return-void
.end method


# virtual methods
.method public a(I)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput p1, p0, Lmaps/ak/h;->g:I

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/ak/h;->b:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-boolean p1, p0, Lmaps/ak/h;->f:Z

    return-object p0
.end method

.method public a()Lmaps/ak/n;
    .locals 5

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ak/h;->b:Ljava/lang/String;

    iget-object v1, p0, Lmaps/ak/h;->c:Ljava/lang/String;

    iget-object v2, p0, Lmaps/ak/h;->d:Ljava/lang/String;

    iget-object v3, p0, Lmaps/ak/h;->e:Ljava/lang/String;

    iget-boolean v4, p0, Lmaps/ak/h;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lmaps/ak/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lmaps/ak/n;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0, v1}, Lmaps/ak/n;->a(Lmaps/ak/n;Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    const-string v1, "SYSTEM"

    invoke-virtual {v0, v1}, Lmaps/ak/n;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    iget-object v1, p0, Lmaps/ak/h;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmaps/ak/n;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    iget v1, p0, Lmaps/ak/h;->g:I

    invoke-static {v0, v1}, Lmaps/ak/n;->a(Lmaps/ak/n;I)V

    iget-object v0, p0, Lmaps/ak/h;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    iget-object v1, p0, Lmaps/ak/h;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/ak/n;->a(Lmaps/ak/n;Z)V

    :cond_1
    iget-object v0, p0, Lmaps/ak/h;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    iget-object v1, p0, Lmaps/ak/h;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/ak/n;->b(Lmaps/ak/n;Z)V

    :cond_2
    iget-object v0, p0, Lmaps/ak/h;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    iget-object v1, p0, Lmaps/ak/h;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/ak/n;->c(Lmaps/ak/n;Z)V

    :cond_3
    iget-object v0, p0, Lmaps/ak/h;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    iget-object v1, p0, Lmaps/ak/h;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/ak/n;->d(Lmaps/ak/n;Z)V

    :cond_4
    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    iget-object v0, v0, Lmaps/ak/n;->f:Lmaps/ak/l;

    iget-object v1, p0, Lmaps/ak/h;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmaps/ak/h;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/ak/l;->a(Lmaps/ak/l;Z)V

    :cond_5
    iget-object v1, p0, Lmaps/ak/h;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/ak/h;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lmaps/ak/l;->a(Lmaps/ak/l;Ljava/lang/String;)V

    :cond_6
    iget v1, p0, Lmaps/ak/h;->p:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    iget v1, p0, Lmaps/ak/h;->p:I

    invoke-virtual {v0, v1}, Lmaps/ak/l;->a(I)V

    :cond_7
    iget-object v1, p0, Lmaps/ak/h;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/ak/h;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ak/l;->a(Z)V

    :cond_8
    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    return-object v0
.end method

.method public b()Lmaps/ak/a;
    .locals 3

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    invoke-static {v0}, Lmaps/ak/n;->a(Lmaps/ak/n;)Lmaps/ak/l;

    move-result-object v0

    iget-object v1, p0, Lmaps/ak/h;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/ak/h;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/ak/l;->a(Lmaps/ak/l;Z)V

    :cond_1
    iget-object v1, p0, Lmaps/ak/h;->i:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/ak/h;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lmaps/ak/l;->a(Lmaps/ak/l;Ljava/lang/String;)V

    :cond_2
    iget v1, p0, Lmaps/ak/h;->p:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    iget v1, p0, Lmaps/ak/h;->p:I

    invoke-virtual {v0, v1}, Lmaps/ak/l;->a(I)V

    :goto_0
    iget-object v1, p0, Lmaps/ak/h;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmaps/ak/h;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ak/l;->a(Z)V

    :cond_3
    return-object v0

    :cond_4
    invoke-static {v0}, Lmaps/ak/l;->a(Lmaps/ak/l;)V

    goto :goto_0
.end method

.method public b(I)Lmaps/ak/h;
    .locals 0

    iput p1, p0, Lmaps/ak/h;->p:I

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/ak/h;->c:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/h;->j:Ljava/lang/Boolean;

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/ak/h;->d:Ljava/lang/String;

    return-object p0
.end method

.method public c(Z)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/h;->k:Ljava/lang/Boolean;

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/ak/h;->e:Ljava/lang/String;

    return-object p0
.end method

.method public d(Z)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/h;->m:Ljava/lang/Boolean;

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/ak/h;->h:Ljava/lang/String;

    return-object p0
.end method

.method public e(Z)Lmaps/ak/h;
    .locals 1

    sget-boolean v0, Lmaps/ak/h;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ak/h;->q:Lmaps/ak/n;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/h;->l:Ljava/lang/Boolean;

    return-object p0
.end method

.method public f(Ljava/lang/String;)Lmaps/ak/h;
    .locals 0

    iput-object p1, p0, Lmaps/ak/h;->i:Ljava/lang/String;

    return-object p0
.end method

.method public f(Z)Lmaps/ak/h;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/ak/h;->n:Ljava/lang/Boolean;

    return-object p0
.end method
