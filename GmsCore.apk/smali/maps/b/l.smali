.class public Lmaps/b/l;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/b/u;


# instance fields
.field private final b:Lmaps/f/at;

.field private final c:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/ax;->a()Lmaps/f/ax;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/l;->b:Lmaps/f/at;

    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/b/l;->c:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Ljava/util/Collection;Lmaps/t/bw;)V
    .locals 15

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/ax;->b()Lmaps/f/bz;

    move-result-object v5

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-static {}, Lmaps/f/a;->a()Ljava/util/HashSet;

    move-result-object v6

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v2

    move v2, v1

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/b/x;

    invoke-virtual {v1}, Lmaps/b/x;->b()Lmaps/t/ax;

    move-result-object v3

    invoke-static {v3}, Lmaps/t/bw;->a(Lmaps/t/ax;)Lmaps/t/bw;

    move-result-object v8

    invoke-virtual {v8}, Lmaps/t/bw;->d()I

    move-result v3

    int-to-double v9, v3

    invoke-virtual {v8}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/bx;->e()D

    move-result-wide v11

    div-double/2addr v9, v11

    invoke-virtual {v8}, Lmaps/t/bw;->e()I

    move-result v3

    int-to-double v11, v3

    invoke-virtual {v8}, Lmaps/t/bw;->f()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/bx;->e()D

    move-result-wide v13

    div-double/2addr v11, v13

    const-wide v13, 0x40bb580000000000L

    cmpl-double v3, v9, v13

    if-gez v3, :cond_1

    const-wide v13, 0x40bb580000000000L

    cmpl-double v3, v11, v13

    if-ltz v3, :cond_3

    :cond_1
    sget-boolean v3, Lmaps/ae/h;->g:Z

    if-eqz v3, :cond_2

    const-string v3, "BuildingBounds"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Ignoring building with overly large width/height ("

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "m width, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v11, v12}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "m height, id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lmaps/b/x;->a()Lmaps/t/bg;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v6}, Lmaps/b/x;->a(Ljava/util/Set;)V

    if-eqz p2, :cond_4

    invoke-virtual {v1}, Lmaps/b/x;->b()Lmaps/t/ax;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lmaps/t/bw;->b(Lmaps/t/an;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_4
    add-int/lit8 v3, v2, 0x1

    const/16 v2, 0xf

    invoke-static {v8, v2}, Lmaps/t/ah;->a(Lmaps/t/bw;I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/t/ah;

    invoke-virtual {v5, v2, v1}, Lmaps/f/bz;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/f/bz;

    goto :goto_1

    :cond_5
    move v2, v3

    goto/16 :goto_0

    :cond_6
    invoke-static {v6}, Lmaps/f/bd;->a(Ljava/util/Collection;)Lmaps/f/bd;

    move-result-object v1

    iput-object v1, p0, Lmaps/b/l;->c:Ljava/util/Set;

    invoke-virtual {v5}, Lmaps/f/bz;->a()Lmaps/f/ax;

    move-result-object v1

    iput-object v1, p0, Lmaps/b/l;->b:Lmaps/f/at;

    sget-boolean v1, Lmaps/ae/h;->f:Z

    if-eqz v1, :cond_7

    const-string v1, "BuildingBounds"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " buildings, "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " loaded, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " skipped, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmaps/b/l;->b:Lmaps/f/at;

    invoke-interface {v3}, Lmaps/f/at;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " tiles."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    return-void
.end method

.method public static a(Ljava/io/Reader;)Lmaps/b/l;
    .locals 2

    invoke-static {p0}, Lmaps/b/l;->b(Ljava/io/Reader;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmaps/b/l;->a(Ljava/util/Collection;Lmaps/t/bw;)Lmaps/b/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/Reader;Lmaps/t/bw;)Lmaps/b/l;
    .locals 1

    invoke-static {p0}, Lmaps/b/l;->b(Ljava/io/Reader;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lmaps/b/l;->a(Ljava/util/Collection;Lmaps/t/bw;)Lmaps/b/l;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/Collection;Lmaps/t/bw;)Lmaps/b/l;
    .locals 1

    new-instance v0, Lmaps/b/l;

    invoke-direct {v0, p0, p1}, Lmaps/b/l;-><init>(Ljava/util/Collection;Lmaps/t/bw;)V

    return-object v0
.end method

.method static b(Ljava/io/Reader;)Ljava/util/List;
    .locals 3

    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, p0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-static {}, Lmaps/f/fd;->b()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lmaps/b/x;->a(Ljava/lang/String;)Lmaps/b/x;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v2
.end method


# virtual methods
.method public a(Lmaps/t/ah;)Ljava/util/Collection;
    .locals 3

    const/16 v2, 0xf

    invoke-virtual {p1}, Lmaps/t/ah;->b()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->c()I

    move-result v1

    if-ge v1, v2, :cond_0

    invoke-static {}, Lmaps/f/bd;->b()Lmaps/f/bd;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lmaps/b/l;->b:Lmaps/f/at;

    invoke-interface {v1, v0}, Lmaps/f/at;->a(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Lmaps/t/ah;->a(I)Lmaps/t/ah;

    move-result-object v1

    iget-object v2, p0, Lmaps/b/l;->b:Lmaps/f/at;

    invoke-interface {v2, v1}, Lmaps/f/at;->a(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/b/x;->a(Ljava/util/Collection;Lmaps/t/an;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lmaps/b/g;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/t/v;)Z
    .locals 1

    iget-object v0, p0, Lmaps/b/l;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Lmaps/b/g;)V
    .locals 0

    return-void
.end method
