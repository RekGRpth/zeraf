.class public Lmaps/an/v;
.super Ljava/lang/Object;


# instance fields
.field final a:Lmaps/t/ah;

.field final b:Lmaps/bx/b;

.field final c:Z

.field final d:Lmaps/an/ad;

.field final e:Z

.field final f:Z

.field final g:Z

.field final h:Lmaps/o/c;

.field i:I

.field volatile j:Z

.field private k:Lmaps/an/v;


# direct methods
.method protected constructor <init>(Lmaps/o/c;Lmaps/t/ah;Lmaps/bx/b;)V
    .locals 9

    const/4 v5, 0x0

    sget-object v4, Lmaps/an/ad;->b:Lmaps/an/ad;

    const/4 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, v5

    move v8, v5

    invoke-direct/range {v0 .. v8}, Lmaps/an/v;-><init>(Lmaps/o/c;Lmaps/t/ah;Lmaps/bx/b;Lmaps/an/ad;ZZIZ)V

    return-void
.end method

.method protected constructor <init>(Lmaps/o/c;Lmaps/t/ah;Lmaps/bx/b;Lmaps/an/ad;ZZIZ)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/an/v;->j:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/an/v;->k:Lmaps/an/v;

    iput-object p1, p0, Lmaps/an/v;->h:Lmaps/o/c;

    iput-object p2, p0, Lmaps/an/v;->a:Lmaps/t/ah;

    iput-object p3, p0, Lmaps/an/v;->b:Lmaps/bx/b;

    iput-object p4, p0, Lmaps/an/v;->d:Lmaps/an/ad;

    sget-object v1, Lmaps/an/ad;->e:Lmaps/an/ad;

    invoke-virtual {p4, v1}, Lmaps/an/ad;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lmaps/an/ad;->d:Lmaps/an/ad;

    invoke-virtual {p4, v1}, Lmaps/an/ad;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lmaps/an/v;->c:Z

    iput-boolean p5, p0, Lmaps/an/v;->e:Z

    iput p7, p0, Lmaps/an/v;->i:I

    iput-boolean p6, p0, Lmaps/an/v;->f:Z

    iput-boolean p8, p0, Lmaps/an/v;->g:Z

    return-void
.end method

.method protected constructor <init>(Lmaps/o/c;Lmaps/t/ah;Lmaps/bx/b;ZZ)V
    .locals 9

    sget-object v4, Lmaps/an/ad;->b:Lmaps/an/ad;

    const/4 v7, -0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p5

    move v6, p4

    invoke-direct/range {v0 .. v8}, Lmaps/an/v;-><init>(Lmaps/o/c;Lmaps/t/ah;Lmaps/bx/b;Lmaps/an/ad;ZZIZ)V

    return-void
.end method

.method private a(ILmaps/t/o;)V
    .locals 2

    iget-object v0, p0, Lmaps/an/v;->b:Lmaps/bx/b;

    iget-object v1, p0, Lmaps/an/v;->a:Lmaps/t/ah;

    invoke-interface {v0, v1, p1, p2}, Lmaps/bx/b;->a(Lmaps/t/ah;ILmaps/t/o;)V

    return-void
.end method

.method static synthetic a(Lmaps/an/v;ILmaps/t/o;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/an/v;->a(ILmaps/t/o;)V

    return-void
.end method

.method static synthetic b(Lmaps/an/v;)Lmaps/an/v;
    .locals 1

    iget-object v0, p0, Lmaps/an/v;->k:Lmaps/an/v;

    return-object v0
.end method


# virtual methods
.method protected a(I)V
    .locals 0

    iput p1, p0, Lmaps/an/v;->i:I

    return-void
.end method

.method a(Lmaps/an/v;)V
    .locals 1

    iget-object v0, p0, Lmaps/an/v;->k:Lmaps/an/v;

    iput-object v0, p1, Lmaps/an/v;->k:Lmaps/an/v;

    iput-object p1, p0, Lmaps/an/v;->k:Lmaps/an/v;

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/an/v;->j:Z

    return v0
.end method

.method public c()Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/an/v;->a:Lmaps/t/ah;

    return-object v0
.end method

.method protected d()Lmaps/an/ad;
    .locals 1

    iget-object v0, p0, Lmaps/an/v;->d:Lmaps/an/ad;

    return-object v0
.end method

.method protected e()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/an/v;->c:Z

    return v0
.end method

.method protected f()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/an/v;->e:Z

    return v0
.end method

.method protected g()I
    .locals 1

    iget v0, p0, Lmaps/an/v;->i:I

    return v0
.end method

.method protected h()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/an/v;->f:Z

    return v0
.end method

.method protected i()Lmaps/o/c;
    .locals 1

    iget-object v0, p0, Lmaps/an/v;->h:Lmaps/o/c;

    return-object v0
.end method

.method protected j()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/an/v;->g:Z

    return v0
.end method

.method protected k()Z
    .locals 1

    iget-object v0, p0, Lmaps/an/v;->k:Lmaps/an/v;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmaps/an/v;->h:Lmaps/o/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/an/v;->a:Lmaps/t/ah;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
