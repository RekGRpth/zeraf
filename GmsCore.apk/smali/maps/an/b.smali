.class Lmaps/an/b;
.super Lmaps/be/l;


# instance fields
.field final synthetic a:Lmaps/an/f;

.field private volatile b:Z

.field private volatile c:Z


# direct methods
.method constructor <init>(Lmaps/an/f;)V
    .locals 2

    iput-object p1, p0, Lmaps/an/b;->a:Lmaps/an/f;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CacheCommitter:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/an/f;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/be/l;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lmaps/an/f;->d(Lmaps/an/f;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/b;->c:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lmaps/an/b;->start()V

    goto :goto_0
.end method


# virtual methods
.method b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/b;->b:Z

    return-void
.end method

.method c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/an/b;->c:Z

    return v0
.end method

.method public n_()V
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    invoke-static {}, Lmaps/af/w;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lmaps/an/b;->a:Lmaps/an/f;

    iget-object v0, v0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v0}, Lmaps/an/m;->b()Lmaps/ag/s;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/an/b;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lmaps/an/b;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/an/b;->a:Lmaps/an/f;

    invoke-static {v0}, Lmaps/an/f;->e(Lmaps/an/f;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/an/b;->a:Lmaps/an/f;

    invoke-static {v0, v4}, Lmaps/an/f;->a(Lmaps/an/f;Z)Z

    invoke-interface {v1}, Lmaps/ag/s;->h()V

    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/b;->c:Z

    iget-object v0, p0, Lmaps/an/b;->a:Lmaps/an/f;

    invoke-static {v0}, Lmaps/an/f;->f(Lmaps/an/f;)V

    goto :goto_1

    :cond_1
    iput-boolean v4, p0, Lmaps/an/b;->b:Z

    :try_start_1
    iget-object v0, p0, Lmaps/an/b;->a:Lmaps/an/f;

    invoke-static {v0}, Lmaps/an/f;->d(Lmaps/an/f;)I

    move-result v0

    :goto_3
    if-lez v0, :cond_2

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Lmaps/an/b;->sleep(J)V

    iget-object v2, p0, Lmaps/an/b;->a:Lmaps/an/f;

    invoke-static {v2}, Lmaps/an/f;->e(Lmaps/an/f;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-boolean v0, p0, Lmaps/an/b;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/an/b;->a:Lmaps/an/f;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lmaps/an/f;->a(Lmaps/an/f;Z)Z

    invoke-interface {v1}, Lmaps/ag/s;->h()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1

    :cond_3
    add-int/lit16 v0, v0, -0x3e8

    goto :goto_3
.end method
