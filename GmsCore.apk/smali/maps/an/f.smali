.class public abstract Lmaps/an/f;
.super Lmaps/be/l;

# interfaces
.implements Lmaps/ak/i;
.implements Lmaps/an/af;
.implements Lmaps/an/y;


# instance fields
.field protected a:Lmaps/an/m;

.field protected b:Lmaps/an/g;

.field volatile c:I

.field protected d:Lmaps/ae/d;

.field volatile e:I

.field volatile f:I

.field protected final g:Lmaps/o/c;

.field private i:Ljava/util/Locale;

.field private volatile j:Lmaps/an/b;

.field private final k:Ljava/util/concurrent/locks/ReentrantLock;

.field private final l:Lmaps/ak/a;

.field private m:Landroid/os/Handler;

.field private n:Landroid/os/Looper;

.field private o:Z

.field private final p:Ljava/util/List;

.field private final q:Lmaps/be/i;

.field private final r:Ljava/util/Map;

.field private final s:I

.field private t:Z

.field private u:Lmaps/k/f;

.field private final v:Ljava/util/ArrayList;

.field private volatile w:Z

.field private x:Lmaps/bx/b;


# direct methods
.method protected constructor <init>(Lmaps/ak/a;Lmaps/o/c;Ljava/lang/String;Lmaps/ag/a;Lmaps/ag/s;IZILjava/util/Locale;Ljava/io/File;)V
    .locals 8

    invoke-direct {p0, p3}, Lmaps/be/l;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lmaps/an/f;->k:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lmaps/an/f;->p:Ljava/util/List;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lmaps/an/f;->r:Ljava/util/Map;

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v1

    iput-object v1, p0, Lmaps/an/f;->d:Lmaps/ae/d;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/an/f;->t:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/an/f;->w:Z

    new-instance v1, Lmaps/an/r;

    invoke-direct {v1, p0}, Lmaps/an/r;-><init>(Lmaps/an/f;)V

    iput-object v1, p0, Lmaps/an/f;->x:Lmaps/bx/b;

    iput-object p2, p0, Lmaps/an/f;->g:Lmaps/o/c;

    new-instance v1, Lmaps/an/m;

    invoke-virtual {p0}, Lmaps/an/f;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v3, p4

    move-object v4, p5

    move v5, p7

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    invoke-direct/range {v1 .. v7}, Lmaps/an/m;-><init>(Ljava/lang/String;Lmaps/ag/a;Lmaps/ag/s;ZLjava/util/Locale;Ljava/io/File;)V

    iput-object v1, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iput p6, p0, Lmaps/an/f;->s:I

    move-object/from16 v0, p9

    iput-object v0, p0, Lmaps/an/f;->i:Ljava/util/Locale;

    iput-object p1, p0, Lmaps/an/f;->l:Lmaps/ak/a;

    invoke-virtual {p0}, Lmaps/an/f;->h()Lmaps/an/g;

    move-result-object v1

    iput-object v1, p0, Lmaps/an/f;->b:Lmaps/an/g;

    iget-object v1, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-static {v1, p0}, Lmaps/an/g;->a(Lmaps/an/g;Lmaps/an/f;)Lmaps/an/f;

    new-instance v1, Lmaps/an/o;

    move/from16 v0, p8

    invoke-direct {v1, p0, v0}, Lmaps/an/o;-><init>(Lmaps/an/f;I)V

    iput-object v1, p0, Lmaps/an/f;->q:Lmaps/be/i;

    return-void
.end method

.method private a(Lmaps/an/v;ZZ)Landroid/util/Pair;
    .locals 7

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/an/f;->i()Lmaps/o/c;

    move-result-object v4

    invoke-virtual {v1, v4}, Lmaps/t/ah;->a(Lmaps/o/c;)Lmaps/t/ah;

    move-result-object v4

    iget-object v1, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v1, v1, Lmaps/an/m;->a:Lmaps/ag/a;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v1, v1, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v1, v4}, Lmaps/ag/a;->b(Lmaps/t/ah;)Lmaps/t/o;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v5, p0, Lmaps/an/f;->d:Lmaps/ae/d;

    invoke-interface {v1, v5}, Lmaps/t/o;->a(Lmaps/ae/d;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v4, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v4, v4, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v4, v1}, Lmaps/ag/a;->a(Lmaps/t/o;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1, v0}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    move-object v1, v0

    move v0, v2

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lmaps/an/v;->e()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lmaps/an/f;->u:Lmaps/k/f;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lmaps/an/f;->u:Lmaps/k/f;

    invoke-virtual {v4}, Lmaps/k/f;->a()V

    :cond_1
    invoke-direct {p0, v1, p1, p3}, Lmaps/an/f;->a(Lmaps/t/o;Lmaps/an/v;Z)Lmaps/an/v;

    move-result-object v4

    if-nez p3, :cond_3

    invoke-virtual {p1}, Lmaps/an/v;->e()Z

    move-result v5

    if-eqz v5, :cond_2

    :goto_2
    invoke-virtual {p0, p1, v3, v0}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    move v0, v2

    move-object v1, v4

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move v0, v3

    move-object v1, v4

    goto :goto_0

    :cond_4
    if-eqz p2, :cond_b

    iget-object v1, p0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v1}, Lmaps/an/m;->b()Lmaps/ag/s;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-virtual {p1}, Lmaps/an/v;->e()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v1, v4}, Lmaps/ag/s;->a(Lmaps/t/ah;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p0, p1, v3, v0}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_5
    invoke-interface {v1, v4}, Lmaps/ag/s;->b(Lmaps/t/ah;)Lmaps/t/o;

    move-result-object v5

    if-eqz v5, :cond_b

    iget-object v6, p0, Lmaps/an/f;->d:Lmaps/ae/d;

    invoke-interface {v5, v6}, Lmaps/t/o;->a(Lmaps/ae/d;)Z

    move-result v6

    if-nez v6, :cond_b

    invoke-interface {v1, v5}, Lmaps/ag/s;->a(Lmaps/t/o;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Lmaps/an/v;->e()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lmaps/an/f;->u:Lmaps/k/f;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/an/f;->u:Lmaps/k/f;

    invoke-virtual {v1}, Lmaps/k/f;->c()V

    :cond_6
    invoke-direct {p0, p1, v4}, Lmaps/an/f;->a(Lmaps/an/v;Lmaps/t/ah;)V

    :goto_3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_7
    invoke-virtual {p1}, Lmaps/an/v;->e()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lmaps/an/f;->u:Lmaps/k/f;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lmaps/an/f;->u:Lmaps/k/f;

    invoke-virtual {v0}, Lmaps/k/f;->b()V

    :cond_8
    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v0, v0, Lmaps/an/m;->a:Lmaps/ag/a;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v0, v0, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v0, v4, v5}, Lmaps/ag/a;->a(Lmaps/t/ah;Lmaps/t/o;)V

    :cond_9
    invoke-direct {p0, v5, p1, p3}, Lmaps/an/f;->a(Lmaps/t/o;Lmaps/an/v;Z)Lmaps/an/v;

    move-result-object v0

    if-nez p3, :cond_a

    invoke-virtual {p0, p1, v3, v5}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    goto :goto_3

    :cond_a
    move v2, v3

    goto :goto_3

    :cond_b
    invoke-virtual {p1}, Lmaps/an/v;->e()Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lmaps/an/f;->u:Lmaps/k/f;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lmaps/an/f;->u:Lmaps/k/f;

    invoke-virtual {v1}, Lmaps/k/f;->c()V

    :cond_c
    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Lmaps/an/v;->a(I)V

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private a(Lmaps/an/v;Lmaps/o/c;Lmaps/t/ah;)Lmaps/an/v;
    .locals 9

    new-instance v0, Lmaps/an/v;

    iget-object v3, p1, Lmaps/an/v;->b:Lmaps/bx/b;

    iget-object v4, p1, Lmaps/an/v;->d:Lmaps/an/ad;

    iget-boolean v5, p1, Lmaps/an/v;->e:Z

    iget-boolean v6, p1, Lmaps/an/v;->f:Z

    iget v7, p1, Lmaps/an/v;->i:I

    iget-boolean v8, p1, Lmaps/an/v;->g:Z

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v8}, Lmaps/an/v;-><init>(Lmaps/o/c;Lmaps/t/ah;Lmaps/bx/b;Lmaps/an/ad;ZZIZ)V

    return-object v0
.end method

.method private a(Lmaps/t/o;Lmaps/an/v;Z)Lmaps/an/v;
    .locals 9

    const/4 v6, 0x0

    const/4 v7, -0x1

    const/4 v5, 0x1

    invoke-virtual {p0}, Lmaps/an/f;->c()I

    move-result v1

    const/4 v0, 0x0

    if-eq v1, v7, :cond_1

    invoke-interface {p1}, Lmaps/t/o;->i()I

    move-result v2

    if-eq v1, v2, :cond_1

    move v1, v5

    :goto_0
    if-eqz v1, :cond_0

    new-instance v0, Lmaps/an/v;

    iget-object v1, p0, Lmaps/an/f;->g:Lmaps/o/c;

    invoke-virtual {p2}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v2

    iget-object v3, p0, Lmaps/an/f;->g:Lmaps/o/c;

    invoke-virtual {v2, v3}, Lmaps/t/ah;->a(Lmaps/o/c;)Lmaps/t/ah;

    move-result-object v2

    iget-object v3, p0, Lmaps/an/f;->x:Lmaps/bx/b;

    sget-object v4, Lmaps/an/ad;->b:Lmaps/an/ad;

    move v8, v5

    invoke-direct/range {v0 .. v8}, Lmaps/an/v;-><init>(Lmaps/o/c;Lmaps/t/ah;Lmaps/bx/b;Lmaps/an/ad;ZZIZ)V

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/k/b;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lmaps/t/o;->g()Lmaps/t/ah;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/ah;->c()I

    move-result v1

    invoke-direct {p0, v7, v1}, Lmaps/an/f;->a(II)V

    :cond_0
    return-object v0

    :cond_1
    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/k/b;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Lmaps/an/v;->e()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lmaps/an/f;->d:Lmaps/ae/d;

    invoke-interface {p1, v1}, Lmaps/t/o;->b(Lmaps/ae/d;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p3, :cond_3

    :cond_2
    invoke-interface {p1}, Lmaps/t/o;->j()I

    move-result v7

    move v1, v5

    goto :goto_0

    :cond_3
    move v1, v6

    goto :goto_0
.end method

.method private a(I)V
    .locals 1

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v0, p1}, Lmaps/an/m;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/an/f;->j()V

    :cond_0
    return-void
.end method

.method private a(II)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/an/f;->l:Lmaps/ak/a;

    invoke-interface {v0}, Lmaps/ak/a;->d()J

    move-result-wide v3

    const-wide/16 v5, 0x64

    rem-long/2addr v3, v5

    const-wide/16 v5, 0x8

    cmp-long v0, v3, v5

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    move v0, v1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "v="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "d="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lmaps/t/ar;->t()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "z="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x6d

    const-string v6, "u"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    aput-object v0, v7, v2

    aput-object v3, v7, v1

    const/4 v0, 0x2

    aput-object v4, v7, v0

    invoke-static {v7}, Lmaps/bh/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v0}, Lmaps/bh/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method private a(IIIIII)V
    .locals 12

    iget-object v1, p0, Lmaps/an/f;->l:Lmaps/ak/a;

    invoke-interface {v1}, Lmaps/ak/a;->d()J

    move-result-wide v1

    const-wide/16 v3, 0x64

    rem-long/2addr v1, v3

    const-wide/16 v3, 0x8

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v1

    if-nez v1, :cond_0

    sget-boolean v1, Lmaps/ae/h;->m:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "f="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "p="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "r="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "n="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "v="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "d="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lmaps/t/ar;->t()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x6d

    const-string v9, "b"

    const/4 v10, 0x7

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    const/4 v1, 0x1

    aput-object v2, v10, v1

    const/4 v1, 0x2

    aput-object v3, v10, v1

    const/4 v1, 0x3

    aput-object v4, v10, v1

    const/4 v1, 0x4

    aput-object v5, v10, v1

    const/4 v1, 0x5

    aput-object v6, v10, v1

    const/4 v1, 0x6

    aput-object v7, v10, v1

    invoke-static {v10}, Lmaps/bh/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v9, v1}, Lmaps/bh/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lmaps/an/f;)V
    .locals 0

    invoke-direct {p0}, Lmaps/an/f;->k()V

    return-void
.end method

.method static synthetic a(Lmaps/an/f;Lmaps/an/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/an/f;->a(Lmaps/an/g;)V

    return-void
.end method

.method static synthetic a(Lmaps/an/f;Lmaps/an/v;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/an/f;->b(Lmaps/an/v;)V

    return-void
.end method

.method static synthetic a(Lmaps/an/f;Lmaps/t/as;Lmaps/an/ad;Lmaps/bx/b;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmaps/an/f;->b(Lmaps/t/as;Lmaps/an/ad;Lmaps/bx/b;)V

    return-void
.end method

.method static synthetic a(Lmaps/an/f;Lmaps/t/o;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/an/f;->a(Lmaps/t/o;)V

    return-void
.end method

.method private a(Lmaps/an/g;)V
    .locals 17

    invoke-direct/range {p0 .. p0}, Lmaps/an/f;->o()V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/an/f;->t:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/an/f;->t:Z

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/an/f;->q:Lmaps/be/i;

    invoke-virtual {v1}, Lmaps/be/i;->b()I

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/an/f;->q:Lmaps/be/i;

    invoke-virtual {v1}, Lmaps/be/i;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/an/v;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lmaps/an/f;->b(Lmaps/an/v;)V

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lmaps/an/g;->g()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lmaps/an/f;->c()I

    move-result v2

    if-eq v1, v2, :cond_2

    sget-boolean v2, Lmaps/ae/h;->j:Z

    if-eqz v2, :cond_1

    invoke-virtual/range {p0 .. p0}, Lmaps/an/f;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Cached version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lmaps/an/f;->c()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Clear: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v4}, Lmaps/an/m;->g()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lmaps/an/f;->a(I)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/an/f;->p:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-boolean v1, Lmaps/ae/h;->j:Z

    if-eqz v1, :cond_3

    invoke-virtual/range {p0 .. p0}, Lmaps/an/f;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Request not found in list of outstanding requests"

    invoke-static {v1, v2}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v1}, Lmaps/an/m;->b()Lmaps/ag/s;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lmaps/an/g;->e()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x0

    move v9, v1

    :goto_2
    invoke-virtual/range {p1 .. p1}, Lmaps/an/g;->e()I

    move-result v1

    if-ge v9, v1, :cond_14

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lmaps/an/g;->b(I)Lmaps/an/v;

    move-result-object v12

    invoke-virtual {v12}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v1

    invoke-virtual {v12}, Lmaps/an/v;->i()Lmaps/o/c;

    move-result-object v8

    invoke-virtual {v1, v8}, Lmaps/t/ah;->a(Lmaps/o/c;)Lmaps/t/ah;

    move-result-object v13

    invoke-virtual {v12}, Lmaps/an/v;->g()I

    move-result v1

    const/4 v8, -0x1

    if-eq v1, v8, :cond_17

    add-int/lit8 v10, v7, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/an/f;->r:Ljava/util/Map;

    invoke-interface {v1, v13}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget v1, v0, Lmaps/an/f;->c:I

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lmaps/an/f;->c:I

    :try_start_0
    invoke-virtual {v12}, Lmaps/an/v;->e()Z

    move-result v1

    if-eqz v1, :cond_d

    move-object/from16 v0, p0

    iget v1, v0, Lmaps/an/f;->f:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lmaps/an/f;->f:I

    :goto_4
    const/4 v1, 0x0

    if-eqz v11, :cond_5

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lmaps/an/g;->c(I)[B

    move-result-object v7

    if-eqz v7, :cond_5

    array-length v1, v7

    new-array v1, v1, [B

    const/4 v8, 0x0

    const/4 v14, 0x0

    array-length v15, v7

    invoke-static {v7, v8, v1, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lmaps/an/f;->d(Lmaps/an/v;)Lmaps/o/c;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lmaps/an/g;->a(I)Lmaps/t/o;

    move-result-object v8

    if-eqz v8, :cond_11

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v7, v7, Lmaps/an/m;->a:Lmaps/ag/a;

    if-eqz v7, :cond_7

    invoke-virtual {v12}, Lmaps/an/v;->e()Z

    move-result v7

    if-nez v7, :cond_7

    invoke-interface {v8}, Lmaps/t/o;->h()Lmaps/o/c;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-interface {v8}, Lmaps/t/o;->h()Lmaps/o/c;

    move-result-object v7

    if-eq v7, v14, :cond_7

    :cond_6
    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v7, v7, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v7, v13, v8}, Lmaps/ag/a;->a(Lmaps/t/ah;Lmaps/t/o;)V

    :cond_7
    if-eqz v11, :cond_9

    invoke-interface {v8}, Lmaps/t/o;->h()Lmaps/o/c;

    move-result-object v7

    if-eqz v7, :cond_8

    invoke-interface {v8}, Lmaps/t/o;->h()Lmaps/o/c;

    move-result-object v7

    if-eq v7, v14, :cond_9

    :cond_8
    invoke-interface {v11, v13, v8, v1}, Lmaps/ag/s;->a(Lmaps/t/ah;Lmaps/t/o;[B)V

    :cond_9
    invoke-interface {v8}, Lmaps/t/o;->h()Lmaps/o/c;

    move-result-object v1

    if-eqz v1, :cond_16

    invoke-interface {v8}, Lmaps/t/o;->h()Lmaps/o/c;

    move-result-object v1

    if-ne v1, v14, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/an/f;->g:Lmaps/o/c;

    invoke-virtual {v13, v1}, Lmaps/t/ah;->a(Lmaps/o/c;)Lmaps/t/ah;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v7, v7, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v7, v1}, Lmaps/ag/a;->b(Lmaps/t/ah;)Lmaps/t/o;

    move-result-object v1

    if-eqz v1, :cond_a

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v7, v7, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v7, v1}, Lmaps/ag/a;->a(Lmaps/t/o;)Z

    move-result v7

    if-eqz v7, :cond_e

    :cond_a
    sget-boolean v7, Lmaps/ae/h;->i:Z

    if-eqz v7, :cond_b

    const-string v7, "DashServerTileStore"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " No base tile for a diff tile: coords: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " baseTile: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v15, " diff tile type: "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    const/4 v1, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v1, v8}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    :cond_c
    :goto_5
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    move v7, v10

    goto/16 :goto_2

    :cond_d
    move-object/from16 v0, p0

    iget v1, v0, Lmaps/an/f;->e:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lmaps/an/f;->e:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_4

    :catch_0
    move-exception v1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lmaps/an/f;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": Could not parse tile: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v1}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v1, v7}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    goto :goto_5

    :cond_e
    :try_start_1
    check-cast v1, Lmaps/t/ar;

    move-object v0, v8

    check-cast v0, Lmaps/t/ar;

    move-object v7, v0

    invoke-static {v1, v7}, Lmaps/t/f;->b(Lmaps/t/ar;Lmaps/t/ar;)Lmaps/t/ar;

    move-result-object v1

    :goto_6
    invoke-virtual {v12}, Lmaps/an/v;->e()Z

    move-result v7

    if-eqz v7, :cond_10

    add-int/lit8 v4, v4, 0x1

    :goto_7
    if-eqz v14, :cond_f

    invoke-interface {v8}, Lmaps/t/o;->h()Lmaps/o/c;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/an/f;->g:Lmaps/o/c;

    if-eq v7, v8, :cond_c

    :cond_f
    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v7, v1}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    goto :goto_5

    :cond_10
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_11
    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/k/b;->j()Z

    move-result v1

    if-eqz v1, :cond_13

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lmaps/an/f;->b(Lmaps/an/v;Lmaps/t/ah;)Z

    move-result v1

    if-eqz v1, :cond_12

    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_12
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lmaps/an/f;->a(Lmaps/an/v;Lmaps/t/ah;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_13
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lmaps/an/f;->a(Lmaps/an/v;Lmaps/t/ah;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_5

    :cond_14
    sget-boolean v1, Lmaps/ae/h;->j:Z

    if-eqz v1, :cond_15

    invoke-virtual/range {p0 .. p0}, Lmaps/an/f;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Response received. Total tiles: prefetch: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lmaps/an/f;->f:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " normal: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lmaps/an/f;->e:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    invoke-direct/range {p0 .. p0}, Lmaps/an/f;->p()V

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/k/b;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct/range {p0 .. p0}, Lmaps/an/f;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lmaps/an/f;->a(IIIIII)V

    goto/16 :goto_1

    :cond_16
    move-object v1, v8

    goto/16 :goto_6

    :cond_17
    move v10, v7

    goto/16 :goto_3
.end method

.method private a(Lmaps/an/v;)V
    .locals 2

    iget-object v0, p0, Lmaps/an/f;->j:Lmaps/an/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/f;->j:Lmaps/an/b;

    invoke-virtual {v0}, Lmaps/an/b;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private a(Lmaps/an/v;Lmaps/t/ah;)V
    .locals 2

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v0, v0, Lmaps/an/m;->a:Lmaps/ag/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v0, v0, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v0, p2}, Lmaps/ag/a;->c(Lmaps/t/ah;)V

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    return-void
.end method

.method private a(Lmaps/t/o;)V
    .locals 4

    iget-object v2, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    monitor-enter v2

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/ag;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0, p1}, Lmaps/an/ag;->a(Lmaps/an/y;Lmaps/t/o;)V

    move v0, v1

    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic a(Lmaps/an/f;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmaps/an/f;->w:Z

    return p1
.end method

.method private b(Lmaps/t/ah;)Lmaps/t/o;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v1, v1, Lmaps/an/m;->a:Lmaps/ag/a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v1, v1, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v1, p1}, Lmaps/ag/a;->a(Lmaps/t/ah;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v0, v0, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v0, p1}, Lmaps/ag/a;->b(Lmaps/t/ah;)Lmaps/t/o;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v1}, Lmaps/an/m;->b()Lmaps/ag/s;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Lmaps/ag/s;->a(Lmaps/t/ah;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1, p1}, Lmaps/ag/s;->b(Lmaps/t/ah;)Lmaps/t/o;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lmaps/an/f;)V
    .locals 0

    invoke-direct {p0}, Lmaps/an/f;->m()V

    return-void
.end method

.method private b(Lmaps/an/v;)V
    .locals 12

    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lmaps/an/f;->o()V

    invoke-virtual {p1}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v0

    iget-object v2, p0, Lmaps/an/f;->g:Lmaps/o/c;

    invoke-virtual {v0, v2}, Lmaps/t/ah;->a(Lmaps/o/c;)Lmaps/t/ah;

    move-result-object v5

    sget-object v0, Lmaps/an/y;->h:Lmaps/t/ah;

    invoke-virtual {v0, v5}, Lmaps/t/ah;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, v9, v10}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/an/f;->r:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/v;

    invoke-direct {p0, p1}, Lmaps/an/f;->d(Lmaps/an/v;)Lmaps/o/c;

    move-result-object v6

    if-eqz v6, :cond_4

    move v2, v3

    :goto_1
    invoke-virtual {p1}, Lmaps/an/v;->h()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p1}, Lmaps/an/v;->f()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-boolean v4, Lmaps/ae/h;->j:Z

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lmaps/an/f;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v7, "Local tile request shouldn\'t be have \'mMustSkipCache = true\'."

    invoke-static {v4, v7}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0, p1, v3, v1}, Lmaps/an/f;->a(Lmaps/an/v;ZZ)Landroid/util/Pair;

    move-result-object v7

    iget-object v1, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v1, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lmaps/an/v;

    move v11, v4

    move-object v4, v1

    move v1, v11

    :goto_2
    if-nez v1, :cond_3

    invoke-virtual {p0, p1, v9, v10}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    :cond_3
    if-eqz v4, :cond_0

    invoke-static {v4}, Lmaps/an/f;->c(Lmaps/an/v;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_a

    invoke-virtual {v4}, Lmaps/an/v;->j()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, v4}, Lmaps/an/v;->a(Lmaps/an/v;)V

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lmaps/an/v;->f()Z

    move-result v4

    if-eqz v4, :cond_6

    move-object v4, p1

    goto :goto_2

    :cond_6
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lmaps/an/v;->k()Z

    move-result v4

    if-nez v4, :cond_7

    invoke-virtual {v0}, Lmaps/an/v;->j()Z

    move-result v4

    if-nez v4, :cond_8

    :cond_7
    move-object v4, p1

    goto :goto_2

    :cond_8
    invoke-direct {p0, p1, v3, v2}, Lmaps/an/f;->a(Lmaps/an/v;ZZ)Landroid/util/Pair;

    move-result-object v7

    iget-object v1, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v1, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lmaps/an/v;

    move v11, v4

    move-object v4, v1

    move v1, v11

    goto :goto_2

    :cond_9
    move v1, v4

    move-object v4, p1

    goto :goto_2

    :cond_a
    iget-boolean v0, p0, Lmaps/an/f;->t:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lmaps/an/f;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {v4}, Lmaps/an/v;->e()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lmaps/an/f;->q:Lmaps/be/i;

    invoke-virtual {v0, v5}, Lmaps/be/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/v;

    if-eqz v0, :cond_b

    invoke-virtual {v0, v4}, Lmaps/an/v;->a(Lmaps/an/v;)V

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lmaps/an/f;->q:Lmaps/be/i;

    invoke-virtual {v0, v5, v4}, Lmaps/be/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lmaps/an/f;->r:Ljava/util/Map;

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-virtual {v0, v4}, Lmaps/an/g;->a(Lmaps/an/v;)Z

    move-result v0

    if-eqz v0, :cond_d

    if-eqz v2, :cond_e

    iget-object v0, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-static {v0, v8}, Lmaps/an/g;->a(Lmaps/an/g;I)Z

    move-result v0

    if-nez v0, :cond_e

    :cond_d
    invoke-direct {p0}, Lmaps/an/f;->l()V

    :cond_e
    iget-object v0, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-virtual {p0}, Lmaps/an/f;->i()Lmaps/o/c;

    move-result-object v1

    invoke-static {v1, v5}, Lmaps/bt/a;->a(Lmaps/o/c;Lmaps/t/ah;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lmaps/an/g;->a(Landroid/util/Pair;Lmaps/an/v;)V

    if-eqz v2, :cond_f

    invoke-virtual {p1}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0, v6}, Lmaps/t/ah;->a(Lmaps/o/c;)Lmaps/t/ah;

    move-result-object v0

    invoke-direct {p0, v4, v6, v0}, Lmaps/an/f;->a(Lmaps/an/v;Lmaps/o/c;Lmaps/t/ah;)Lmaps/an/v;

    move-result-object v1

    iget-object v2, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-virtual {v1}, Lmaps/an/v;->i()Lmaps/o/c;

    move-result-object v5

    invoke-static {v5, v0}, Lmaps/bt/a;->a(Lmaps/o/c;Lmaps/t/ah;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lmaps/an/g;->a(Landroid/util/Pair;Lmaps/an/v;)V

    :cond_f
    iget v0, p0, Lmaps/an/f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/an/f;->c:I

    iget-object v0, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-virtual {v0}, Lmaps/an/g;->f()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-virtual {v4}, Lmaps/an/v;->f()Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    invoke-direct {p0}, Lmaps/an/f;->l()V

    goto/16 :goto_0

    :cond_11
    iget-boolean v0, p0, Lmaps/an/f;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    invoke-virtual {v0, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    const-wide/16 v4, 0x32

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iput-boolean v3, p0, Lmaps/an/f;->o:Z

    goto/16 :goto_0
.end method

.method private b(Lmaps/t/as;Lmaps/an/ad;Lmaps/bx/b;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lmaps/an/w;->a(Lmaps/an/ad;Z)I

    move-result v0

    iget-object v1, p0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v1}, Lmaps/an/m;->b()Lmaps/ag/s;

    move-result-object v1

    :goto_0
    invoke-interface {p1}, Lmaps/t/as;->c()Lmaps/t/ah;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_0

    invoke-interface {v1, v2, p3, v0}, Lmaps/ag/s;->a(Lmaps/t/ah;Lmaps/bx/b;I)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    invoke-interface {p3, v2, v4, v3}, Lmaps/bx/b;->a(Lmaps/t/ah;ILmaps/t/o;)V

    goto :goto_0

    :cond_1
    iput-boolean v4, p0, Lmaps/an/f;->w:Z

    invoke-direct {p0}, Lmaps/an/f;->p()V

    return-void
.end method

.method private b(Lmaps/an/v;Lmaps/t/ah;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, p2}, Lmaps/an/f;->b(Lmaps/t/ah;)Lmaps/t/o;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lmaps/t/o;->j()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lmaps/an/f;->d:Lmaps/ae/d;

    invoke-interface {v1, v2}, Lmaps/t/o;->c(Lmaps/ae/d;)V

    iget-object v2, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v2, v2, Lmaps/an/m;->a:Lmaps/ag/a;

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lmaps/an/v;->e()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v2, v2, Lmaps/an/m;->a:Lmaps/ag/a;

    invoke-interface {v2, p2, v1}, Lmaps/ag/a;->a(Lmaps/t/ah;Lmaps/t/o;)V

    :cond_0
    iget-object v2, p0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v2}, Lmaps/an/m;->b()Lmaps/ag/s;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2, p2}, Lmaps/ag/s;->d(Lmaps/t/ah;)[B

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v2, p2, v1, v3}, Lmaps/ag/s;->a(Lmaps/t/ah;Lmaps/t/o;[B)V

    :cond_1
    invoke-virtual {p0, p1, v0, v1}, Lmaps/an/f;->a(Lmaps/an/v;ILmaps/t/o;)V

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method static synthetic c(Lmaps/an/f;)V
    .locals 0

    invoke-direct {p0}, Lmaps/an/f;->n()V

    return-void
.end method

.method private static c(Lmaps/an/v;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lmaps/an/v;->e()Z

    move-result v2

    if-nez v2, :cond_1

    sget-boolean v2, Lmaps/ae/h;->M:Z

    if-eqz v2, :cond_1

    invoke-static {}, Lmaps/k/b;->a()Lmaps/k/b;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/k/b;->i()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lmaps/bm/e;->e()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic d(Lmaps/an/f;)I
    .locals 1

    iget v0, p0, Lmaps/an/f;->s:I

    return v0
.end method

.method private d(Lmaps/an/v;)Lmaps/o/c;
    .locals 3

    invoke-virtual {p1}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ah;->l()Lmaps/t/al;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/al;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/cm;

    invoke-virtual {v0}, Lmaps/t/cm;->a()Lmaps/o/c;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lmaps/t/cm;->a()Lmaps/o/c;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lmaps/an/f;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/an/f;->w:Z

    return v0
.end method

.method static synthetic f(Lmaps/an/f;)V
    .locals 0

    invoke-direct {p0}, Lmaps/an/f;->p()V

    return-void
.end method

.method private j()V
    .locals 4

    iget-object v2, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    monitor-enter v2

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/ag;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lmaps/an/ag;->a(Lmaps/an/y;)V

    move v0, v1

    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private k()V
    .locals 1

    invoke-direct {p0}, Lmaps/an/f;->o()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/an/f;->o:Z

    invoke-direct {p0}, Lmaps/an/f;->l()V

    return-void
.end method

.method private l()V
    .locals 3

    invoke-direct {p0}, Lmaps/an/f;->o()V

    iget-object v0, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-virtual {v0}, Lmaps/an/g;->e()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Lmaps/j/j;

    const-string v1, "addRequest"

    iget-object v2, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-direct {v0, v1, v2}, Lmaps/j/j;-><init>(Ljava/lang/String;Lmaps/ak/j;)V

    invoke-static {v0}, Lmaps/j/k;->b(Lmaps/j/h;)V

    iget-object v0, p0, Lmaps/an/f;->l:Lmaps/ak/a;

    iget-object v1, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-interface {v0, v1}, Lmaps/ak/a;->a(Lmaps/ak/j;)V

    iget-object v0, p0, Lmaps/an/f;->p:Ljava/util/List;

    iget-object v1, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lmaps/an/f;->h()Lmaps/an/g;

    move-result-object v0

    iput-object v0, p0, Lmaps/an/f;->b:Lmaps/an/g;

    iget-object v0, p0, Lmaps/an/f;->b:Lmaps/an/g;

    invoke-static {v0, p0}, Lmaps/an/g;->a(Lmaps/an/g;Lmaps/an/f;)Lmaps/an/f;

    :cond_0
    return-void
.end method

.method private m()V
    .locals 1

    invoke-direct {p0}, Lmaps/an/f;->o()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/f;->t:Z

    return-void
.end method

.method private n()V
    .locals 5

    invoke-direct {p0}, Lmaps/an/f;->o()V

    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p0, Lmaps/an/f;->p:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lmaps/an/f;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/g;

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Lmaps/an/g;->e()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-virtual {v0, v1}, Lmaps/an/g;->b(I)Lmaps/an/v;

    move-result-object v3

    iget-object v4, p0, Lmaps/an/f;->r:Ljava/util/Map;

    invoke-virtual {v3}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v3, p0, Lmaps/an/f;->c:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lmaps/an/f;->c:I

    invoke-virtual {v0, v1}, Lmaps/an/g;->b(I)Lmaps/an/v;

    move-result-object v3

    invoke-direct {p0, v3}, Lmaps/an/f;->b(Lmaps/an/v;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final o()V
    .locals 2

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on DashServerTileStore thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private p()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lmaps/an/f;->k:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v0, v0, Lmaps/an/m;->b:Lmaps/ag/s;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    iget-object v0, v0, Lmaps/an/m;->b:Lmaps/ag/s;

    invoke-interface {v0}, Lmaps/ag/s;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/an/f;->j:Lmaps/an/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/f;->j:Lmaps/an/b;

    invoke-virtual {v0}, Lmaps/an/b;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Lmaps/an/b;

    invoke-direct {v0, p0}, Lmaps/an/b;-><init>(Lmaps/an/f;)V

    iput-object v0, p0, Lmaps/an/f;->j:Lmaps/an/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v0, p0, Lmaps/an/f;->k:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/an/f;->k:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private q()Z
    .locals 2

    invoke-virtual {p0}, Lmaps/an/f;->i()Lmaps/o/c;

    move-result-object v0

    sget-object v1, Lmaps/o/c;->a:Lmaps/o/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/an/f;->i()Lmaps/o/c;

    move-result-object v0

    sget-object v1, Lmaps/o/c;->b:Lmaps/o/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/an/f;->i()Lmaps/o/c;

    move-result-object v0

    sget-object v1, Lmaps/o/c;->o:Lmaps/o/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lmaps/t/ah;Z)Lmaps/t/o;
    .locals 5

    new-instance v1, Lmaps/an/aa;

    invoke-direct {v1}, Lmaps/an/aa;-><init>()V

    new-instance v0, Lmaps/an/v;

    iget-object v2, p0, Lmaps/an/f;->g:Lmaps/o/c;

    invoke-direct {v0, v2, p1, v1}, Lmaps/an/v;-><init>(Lmaps/o/c;Lmaps/t/ah;Lmaps/bx/b;)V

    const/4 v2, 0x0

    invoke-direct {p0, v0, p2, v2}, Lmaps/an/f;->a(Lmaps/an/v;ZZ)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lmaps/an/v;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    iget-object v3, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    invoke-static {v1}, Lmaps/an/aa;->a(Lmaps/an/aa;)Lmaps/t/o;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/an/f;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Network Error! "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p3, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public a(Lmaps/ak/j;)V
    .locals 3

    instance-of v0, p1, Lmaps/an/g;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lmaps/an/g;

    invoke-static {v0}, Lmaps/an/g;->a(Lmaps/an/g;)Lmaps/an/f;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    iget-object v1, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public a(Lmaps/an/ag;)V
    .locals 3

    iget-object v1, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/an/f;->v:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Lmaps/an/v;ILmaps/t/o;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    move-object v2, p1

    :goto_0
    if-eqz v2, :cond_2

    if-nez p2, :cond_1

    invoke-virtual {v2}, Lmaps/an/v;->b()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lmaps/an/v;->d()Lmaps/an/ad;

    move-result-object v3

    invoke-static {v3}, Lmaps/an/w;->a(Lmaps/an/ad;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p3}, Lmaps/t/o;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v0}, Lmaps/an/m;->b()Lmaps/ag/s;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/an/v;->c()Lmaps/t/ah;

    move-result-object v3

    iget-object v4, v2, Lmaps/an/v;->b:Lmaps/bx/b;

    invoke-virtual {v2}, Lmaps/an/v;->d()Lmaps/an/ad;

    move-result-object v5

    invoke-static {v5, v1}, Lmaps/an/w;->a(Lmaps/an/ad;Z)I

    move-result v5

    invoke-interface {v0, v3, v4, v5}, Lmaps/ag/s;->a(Lmaps/t/ah;Lmaps/bx/b;I)V

    move v0, v1

    :goto_1
    invoke-static {v2}, Lmaps/an/v;->b(Lmaps/an/v;)Lmaps/an/v;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v3, 0x4

    invoke-static {v2, v3, p3}, Lmaps/an/v;->a(Lmaps/an/v;ILmaps/t/o;)V

    goto :goto_1

    :cond_1
    invoke-static {v2, p2, p3}, Lmaps/an/v;->a(Lmaps/an/v;ILmaps/t/o;)V

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    iput-boolean v1, p0, Lmaps/an/f;->w:Z

    invoke-direct {p0}, Lmaps/an/f;->p()V

    :cond_3
    return-void
.end method

.method public a(Lmaps/k/f;)V
    .locals 0

    iput-object p1, p0, Lmaps/an/f;->u:Lmaps/k/f;

    return-void
.end method

.method public a(Lmaps/t/ah;Lmaps/bx/b;)V
    .locals 2

    new-instance v0, Lmaps/an/v;

    iget-object v1, p0, Lmaps/an/f;->g:Lmaps/o/c;

    invoke-direct {v0, v1, p1, p2}, Lmaps/an/v;-><init>(Lmaps/o/c;Lmaps/t/ah;Lmaps/bx/b;)V

    invoke-direct {p0, v0}, Lmaps/an/f;->a(Lmaps/an/v;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v0}, Lmaps/an/m;->c()V

    invoke-direct {p0}, Lmaps/an/f;->j()V

    return-void
.end method

.method public b(Lmaps/ak/j;)V
    .locals 0

    return-void
.end method

.method public b(Lmaps/t/ah;Lmaps/bx/b;)V
    .locals 6

    const/4 v4, 0x1

    new-instance v0, Lmaps/an/v;

    iget-object v1, p0, Lmaps/an/f;->g:Lmaps/o/c;

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lmaps/an/v;-><init>(Lmaps/o/c;Lmaps/t/ah;Lmaps/bx/b;ZZ)V

    invoke-direct {p0, v0}, Lmaps/an/f;->a(Lmaps/an/v;)V

    return-void
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v0}, Lmaps/an/m;->d()I

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lmaps/an/f;->l:Lmaps/ak/a;

    invoke-interface {v0, p0}, Lmaps/ak/a;->a(Lmaps/ak/i;)V

    invoke-virtual {p0}, Lmaps/an/f;->start()V

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v0, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :goto_1
    return-void

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v0}, Lmaps/an/m;->f()V

    return-void
.end method

.method protected abstract h()Lmaps/an/g;
.end method

.method public i()Lmaps/o/c;
    .locals 1

    iget-object v0, p0, Lmaps/an/f;->g:Lmaps/o/c;

    return-object v0
.end method

.method public n_()V
    .locals 4

    :try_start_0
    invoke-static {}, Lmaps/af/w;->d()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lmaps/an/f;->n:Landroid/os/Looper;

    new-instance v0, Lmaps/an/p;

    invoke-direct {v0, p0}, Lmaps/an/p;-><init>(Lmaps/an/f;)V

    iput-object v0, p0, Lmaps/an/f;->m:Landroid/os/Handler;

    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/an/f;->a:Lmaps/an/m;

    invoke-virtual {v0}, Lmaps/an/m;->a()V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/an/f;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
