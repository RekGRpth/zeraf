.class Lmaps/an/p;
.super Landroid/os/Handler;


# instance fields
.field final synthetic a:Lmaps/an/f;


# direct methods
.method constructor <init>(Lmaps/an/f;)V
    .locals 0

    iput-object p1, p0, Lmaps/an/p;->a:Lmaps/an/f;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lmaps/an/p;->a:Lmaps/an/f;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmaps/an/v;

    invoke-static {v1, v0}, Lmaps/an/f;->a(Lmaps/an/f;Lmaps/an/v;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lmaps/an/p;->a:Lmaps/an/f;

    invoke-static {v0}, Lmaps/an/f;->a(Lmaps/an/f;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lmaps/an/p;->a:Lmaps/an/f;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmaps/an/g;

    invoke-static {v1, v0}, Lmaps/an/f;->a(Lmaps/an/f;Lmaps/an/g;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lmaps/an/p;->a:Lmaps/an/f;

    invoke-static {v0}, Lmaps/an/f;->b(Lmaps/an/f;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/an/p;->a:Lmaps/an/f;

    invoke-static {v0}, Lmaps/an/f;->c(Lmaps/an/f;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v3, p0, Lmaps/an/p;->a:Lmaps/an/f;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmaps/t/as;

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lmaps/an/ad;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lmaps/bx/b;

    invoke-static {v3, v1, v2, v0}, Lmaps/an/f;->a(Lmaps/an/f;Lmaps/t/as;Lmaps/an/ad;Lmaps/bx/b;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
