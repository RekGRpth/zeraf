.class public Lmaps/by/c;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private b:I

.field private c:Z

.field private d:[B

.field private e:Ljava/lang/ref/WeakReference;

.field private final f:Ljava/util/concurrent/CountDownLatch;

.field private g:J

.field private h:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/by/c;->b:I

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lmaps/by/c;->f:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/by/c;->a:Ljava/util/ArrayList;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/by/c;->h:J

    return-void
.end method

.method private h()Landroid/graphics/Bitmap;
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    iget-object v1, p0, Lmaps/by/c;->d:[B

    const/4 v2, 0x0

    iget-object v3, p0, Lmaps/by/c;->d:[B

    array-length v3, v3

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    iput v4, p0, Lmaps/by/c;->b:I

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/by/c;->d:[B

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lmaps/by/c;->h:J

    return-void
.end method

.method a(Lmaps/by/a;)V
    .locals 2

    iget-object v1, p0, Lmaps/by/c;->a:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/by/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/by/c;->c:Z

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/by/c;->c:Z

    return v0
.end method

.method public a(Lmaps/bb/c;)Z
    .locals 8

    const/4 v7, 0x6

    const/4 v6, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1, v6}, Lmaps/bb/c;->d(I)I

    move-result v2

    const/4 v3, 0x7

    invoke-virtual {p1, v3}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xc8

    if-ne v2, v4, :cond_4

    if-eqz v3, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lmaps/bb/c;->e(I)J

    move-result-wide v4

    iput-wide v4, p0, Lmaps/by/c;->g:J

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "image/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1, v7}, Lmaps/bb/c;->c(I)[B

    move-result-object v2

    iput-object v2, p0, Lmaps/by/c;->d:[B

    const/4 v2, 0x2

    iput v2, p0, Lmaps/by/c;->b:I

    :goto_0
    iget v2, p0, Lmaps/by/c;->b:I

    if-eq v2, v0, :cond_3

    :goto_1
    return v0

    :cond_0
    const-string v3, "application/binary"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1, v7}, Lmaps/bb/c;->c(I)[B

    move-result-object v2

    iput-object v2, p0, Lmaps/by/c;->d:[B

    iput v6, p0, Lmaps/by/c;->b:I

    goto :goto_0

    :cond_1
    sget-boolean v3, Lmaps/ae/h;->j:Z

    if-eqz v3, :cond_2

    const-string v3, "ResourceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unhandled content-type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iput v0, p0, Lmaps/by/c;->b:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    const/16 v3, 0x130

    if-ne v2, v3, :cond_5

    :goto_2
    move v0, v1

    goto :goto_1

    :cond_5
    iput v0, p0, Lmaps/by/c;->b:I

    goto :goto_2
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lmaps/by/c;->b:I

    if-eqz v1, :cond_0

    iget v1, p0, Lmaps/by/c;->b:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Landroid/graphics/Bitmap;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/by/c;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/by/c;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    if-nez v0, :cond_1

    iget v2, p0, Lmaps/by/c;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lmaps/by/c;->d:[B

    if-eqz v2, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/by/c;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/by/c;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_1
    if-nez v0, :cond_0

    iget-object v1, p0, Lmaps/by/c;->d:[B

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lmaps/by/c;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lmaps/by/c;->e:Ljava/lang/ref/WeakReference;

    :cond_0
    monitor-exit p0

    :cond_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()[B
    .locals 2

    iget v0, p0, Lmaps/by/c;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/by/c;->d:[B

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, Lmaps/by/c;->g:J

    return-wide v0
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lmaps/by/c;->h:J

    return-wide v0
.end method

.method g()V
    .locals 3

    iget-object v2, p0, Lmaps/by/c;->a:Ljava/util/ArrayList;

    monitor-enter v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lmaps/by/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lmaps/by/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/by/a;

    invoke-interface {v0, p0}, Lmaps/by/a;->a(Lmaps/by/c;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/by/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/by/c;->f:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
