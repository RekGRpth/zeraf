.class public Lmaps/ax/b;
.super Ljava/lang/Object;


# instance fields
.field private volatile a:F

.field private final b:F

.field private c:Lmaps/ax/c;


# direct methods
.method public constructor <init>(F)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x42960000

    iput v0, p0, Lmaps/ax/b;->a:F

    iput p1, p0, Lmaps/ax/b;->b:F

    return-void
.end method

.method public static b(F)F
    .locals 5

    const/high16 v4, 0x41600000

    const/high16 v3, 0x41200000

    const/high16 v0, 0x41f00000

    const/high16 v1, 0x41800000

    cmpl-float v1, p0, v1

    if-ltz v1, :cond_1

    const/high16 v0, 0x42960000

    :cond_0
    :goto_0
    return v0

    :cond_1
    cmpl-float v1, p0, v4

    if-lez v1, :cond_2

    const/high16 v1, 0x40000000

    const/high16 v2, 0x42340000

    sub-float v3, p0, v4

    mul-float/2addr v0, v3

    div-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_0

    :cond_2
    cmpl-float v1, p0, v3

    if-lez v1, :cond_0

    const/high16 v1, 0x40800000

    const/high16 v2, 0x41700000

    sub-float v3, p0, v3

    mul-float/2addr v2, v3

    div-float v1, v2, v1

    add-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Lmaps/ax/c;
    .locals 1

    iget-object v0, p0, Lmaps/ax/b;->c:Lmaps/ax/c;

    return-object v0
.end method

.method public a(Lmaps/bq/a;)Lmaps/bq/a;
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p1}, Lmaps/bq/a;->d()F

    move-result v4

    iget v0, p0, Lmaps/ax/b;->a:F

    invoke-virtual {p1}, Lmaps/bq/a;->a()F

    move-result v1

    invoke-static {v1}, Lmaps/ax/b;->b(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const/high16 v1, 0x41a80000

    const/high16 v0, 0x40000000

    iget-object v2, p0, Lmaps/ax/b;->c:Lmaps/ax/c;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/ax/b;->c:Lmaps/ax/c;

    invoke-virtual {p1}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v5

    invoke-interface {v2, v5}, Lmaps/ax/c;->a(Lmaps/t/bx;)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lmaps/ax/b;->c:Lmaps/ax/c;

    invoke-interface {v2}, Lmaps/ax/c;->b()F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :cond_0
    invoke-virtual {p1}, Lmaps/bq/a;->a()F

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual {p1}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v1

    cmpl-float v0, v4, v3

    if-lez v0, :cond_1

    invoke-virtual {v1, v1}, Lmaps/t/bx;->h(Lmaps/t/bx;)V

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {p1}, Lmaps/bq/a;->e()F

    move-result v4

    invoke-virtual {p1}, Lmaps/bq/a;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    :goto_0
    return-object v0

    :cond_1
    cmpg-float v0, v4, v6

    if-gez v0, :cond_2

    invoke-virtual {v1, v1}, Lmaps/t/bx;->h(Lmaps/t/bx;)V

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {p1}, Lmaps/bq/a;->e()F

    move-result v4

    invoke-virtual {p1}, Lmaps/bq/a;->f()F

    move-result v5

    move v3, v6

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/bq/a;->a()F

    move-result v0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lmaps/bq/a;->c()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/bx;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {v1, v1}, Lmaps/t/bx;->h(Lmaps/t/bx;)V

    new-instance v0, Lmaps/bq/a;

    invoke-virtual {p1}, Lmaps/bq/a;->d()F

    move-result v3

    invoke-virtual {p1}, Lmaps/bq/a;->e()F

    move-result v4

    invoke-virtual {p1}, Lmaps/bq/a;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/bq/a;-><init>(Lmaps/t/bx;FFFF)V

    goto :goto_0

    :cond_4
    move-object v0, p1

    goto :goto_0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lmaps/ax/b;->a:F

    return-void
.end method

.method public a(Lmaps/ax/c;)V
    .locals 0

    iput-object p1, p0, Lmaps/ax/b;->c:Lmaps/ax/c;

    return-void
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lmaps/ax/b;->a:F

    return v0
.end method
