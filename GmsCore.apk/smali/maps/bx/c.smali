.class public Lmaps/bx/c;
.super Lmaps/ak/e;


# instance fields
.field private final a:Lmaps/t/bg;

.field private final b:Ljava/util/List;

.field private c:Lmaps/bb/c;

.field private d:Z


# direct methods
.method public constructor <init>(Lmaps/t/bg;)V
    .locals 1

    invoke-direct {p0}, Lmaps/ak/e;-><init>()V

    iput-object p1, p0, Lmaps/bx/c;->a:Lmaps/t/bg;

    invoke-static {}, Lmaps/f/fd;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lmaps/bx/c;->b:Ljava/util/List;

    return-void
.end method

.method private q()I
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/bx/c;->c:Lmaps/bb/c;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lmaps/bx/c;->c:Lmaps/bb/c;

    invoke-virtual {v1, v0}, Lmaps/bb/c;->d(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x76

    return v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 3

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/c/ff;->a:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/bx/c;->a:Lmaps/t/bg;

    invoke-virtual {v2}, Lmaps/t/bg;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    invoke-static {p1, v0}, Lmaps/bb/b;->a(Ljava/io/DataOutput;Lmaps/bb/c;)V

    return-void
.end method

.method public a(Lmaps/bx/d;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/bx/c;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Lmaps/t/e;)V
    .locals 4

    invoke-direct {p0}, Lmaps/bx/c;->q()I

    move-result v0

    if-nez p1, :cond_2

    if-nez v0, :cond_2

    sget-boolean v0, Lmaps/ae/h;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "IndoorBuildingRequest"

    const-string v1, "Unexpected OK status"

    invoke-static {v0, v1}, Lmaps/bt/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/bx/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bx/d;

    iget-object v3, p0, Lmaps/bx/c;->a:Lmaps/t/bg;

    invoke-interface {v0, v3, v1, p1}, Lmaps/bx/d;->a(Lmaps/t/bg;ILmaps/t/e;)V

    goto :goto_1

    :cond_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 1

    sget-object v0, Lmaps/c/ff;->b:Lmaps/bb/d;

    invoke-static {v0, p1}, Lmaps/bb/b;->a(Lmaps/bb/d;Ljava/io/DataInput;)Lmaps/bb/c;

    move-result-object v0

    iput-object v0, p0, Lmaps/bx/c;->c:Lmaps/bb/c;

    const/4 v0, 0x1

    return v0
.end method

.method public f()Lmaps/t/bg;
    .locals 1

    iget-object v0, p0, Lmaps/bx/c;->a:Lmaps/t/bg;

    return-object v0
.end method

.method public g()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/bx/c;->d:Z

    return-void
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/bx/c;->d:Z

    return v0
.end method

.method public i()Lmaps/bb/c;
    .locals 2

    iget-object v0, p0, Lmaps/bx/c;->c:Lmaps/bb/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/bx/c;->c:Lmaps/bb/c;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/bb/c;->f(I)Lmaps/bb/c;

    move-result-object v0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    invoke-direct {p0}, Lmaps/bx/c;->q()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
