.class public Lmaps/br/c;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lmaps/br/b;

.field private static final b:[Lmaps/br/a;


# instance fields
.field private final c:I

.field private final d:J

.field private final e:[Lmaps/br/b;

.field private final f:Lmaps/t/cg;

.field private final g:I

.field private h:[Lmaps/br/a;

.field private final i:I

.field private final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lmaps/br/b;

    const-string v1, "Unknown Road"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lmaps/br/b;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    sput-object v0, Lmaps/br/c;->a:Lmaps/br/b;

    new-array v0, v3, [Lmaps/br/a;

    sput-object v0, Lmaps/br/c;->b:[Lmaps/br/a;

    return-void
.end method

.method public constructor <init>(J[Lmaps/br/b;Lmaps/t/cg;IIII)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    array-length v0, p3

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Segments must have at least one name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput p5, p0, Lmaps/br/c;->c:I

    iput-wide p1, p0, Lmaps/br/c;->d:J

    iput-object p3, p0, Lmaps/br/c;->e:[Lmaps/br/b;

    iput-object p4, p0, Lmaps/br/c;->f:Lmaps/t/cg;

    iput p6, p0, Lmaps/br/c;->g:I

    sget-object v0, Lmaps/br/c;->b:[Lmaps/br/a;

    iput-object v0, p0, Lmaps/br/c;->h:[Lmaps/br/a;

    iput p7, p0, Lmaps/br/c;->i:I

    iput p8, p0, Lmaps/br/c;->j:I

    return-void
.end method

.method public static a(Lmaps/t/ah;I)J
    .locals 5

    invoke-virtual {p0}, Lmaps/t/ah;->d()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x30

    shl-long/2addr v0, v2

    invoke-virtual {p0}, Lmaps/t/ah;->e()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, p1

    or-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public a(I)Lmaps/t/bx;
    .locals 1

    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    invoke-virtual {p0, p1, v0}, Lmaps/br/c;->a(ILmaps/t/bx;)V

    return-object v0
.end method

.method public a(ILmaps/t/bx;)V
    .locals 1

    iget v0, p0, Lmaps/br/c;->c:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/br/c;->f:Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    :cond_0
    iget-object v0, p0, Lmaps/br/c;->f:Lmaps/t/cg;

    invoke-virtual {v0, p1, p2}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    return-void
.end method

.method public a(Lmaps/t/bx;)V
    .locals 1

    iget-object v0, p0, Lmaps/br/c;->f:Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, p1}, Lmaps/br/c;->a(ILmaps/t/bx;)V

    return-void
.end method

.method public varargs a([Lmaps/br/a;)V
    .locals 1

    array-length v0, p1

    if-nez v0, :cond_0

    sget-object v0, Lmaps/br/c;->b:[Lmaps/br/a;

    iput-object v0, p0, Lmaps/br/c;->h:[Lmaps/br/a;

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lmaps/br/c;->h:[Lmaps/br/a;

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget v0, p0, Lmaps/br/c;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    iget v0, p0, Lmaps/br/c;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    iget v0, p0, Lmaps/br/c;->c:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lmaps/t/bx;
    .locals 1

    iget-object v0, p0, Lmaps/br/c;->f:Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lmaps/br/c;->a(I)Lmaps/t/bx;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Lmaps/br/c;->f:Lmaps/t/cg;

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Lmaps/br/c;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lmaps/br/c;->d:J

    check-cast p1, Lmaps/br/c;

    iget-wide v3, p1, Lmaps/br/c;->d:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    const-wide/16 v5, 0xff

    iget-wide v0, p0, Lmaps/br/c;->d:J

    const/16 v2, 0x30

    ushr-long/2addr v0, v2

    and-long/2addr v0, v5

    const/16 v2, 0x18

    shl-long/2addr v0, v2

    iget-wide v2, p0, Lmaps/br/c;->d:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-wide v2, p0, Lmaps/br/c;->d:J

    const-wide/32 v4, 0xffff

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/br/c;->e:[Lmaps/br/b;

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " unroutable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/br/c;->c()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " leaves-region: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/br/c;->a()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " enters-region: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/br/c;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " num-points: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/br/c;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " first-point: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v3}, Lmaps/br/c;->a(I)Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " last-point: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/br/c;->d()Lmaps/t/bx;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/bx;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " num-arcs: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/br/c;->h:[Lmaps/br/a;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
