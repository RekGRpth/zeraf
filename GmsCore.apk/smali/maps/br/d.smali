.class public Lmaps/br/d;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/t/bv;


# instance fields
.field private final a:Lmaps/t/ah;

.field private final b:I

.field private final c:J

.field private final d:[Lmaps/br/c;


# direct methods
.method private constructor <init>(Lmaps/t/ah;Lmaps/bb/c;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/br/d;->a:Lmaps/t/ah;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lmaps/bb/c;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/br/d;->b:I

    iput-wide p3, p0, Lmaps/br/d;->c:J

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Lmaps/bb/c;->j(I)I

    move-result v0

    new-array v0, v0, [Lmaps/br/c;

    iput-object v0, p0, Lmaps/br/d;->d:[Lmaps/br/c;

    invoke-direct {p0, p2}, Lmaps/br/d;->a(Lmaps/bb/c;)[Lmaps/t/bx;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lmaps/br/d;->a(Lmaps/bb/c;[Lmaps/t/bx;)V

    invoke-direct {p0, p2}, Lmaps/br/d;->b(Lmaps/bb/c;)V

    return-void
.end method

.method public static a([BI)I
    .locals 4

    new-instance v0, Lmaps/cs/a;

    invoke-direct {v0, p0}, Lmaps/cs/a;-><init>([B)V

    invoke-virtual {v0, p1}, Lmaps/cs/a;->skipBytes(I)I

    invoke-virtual {v0}, Lmaps/cs/a;->readInt()I

    move-result v1

    const v2, 0x45504752

    if-eq v1, v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FORMAT_MAGIC expected. Found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lmaps/cs/a;->readUnsignedShort()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Version mismatch: 1 expected, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lmaps/cs/a;->readInt()I

    move-result v0

    return v0
.end method

.method public static a(Lmaps/t/ah;[BIJ)Lmaps/br/d;
    .locals 4

    invoke-static {p1, p2}, Lmaps/br/d;->a([BI)I

    move-result v0

    add-int/lit8 v1, p2, 0xa

    invoke-static {p0, v0, p1, v1}, Lmaps/br/d;->a(Lmaps/t/ah;I[BI)V

    array-length v0, p1

    sub-int/2addr v0, v1

    new-instance v2, Ljava/util/zip/Inflater;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/zip/Inflater;-><init>(Z)V

    :try_start_0
    invoke-static {p1, v1, v0}, Lmaps/be/d;->a([BII)Lmaps/be/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/be/a;->a()[B

    move-result-object v1

    invoke-virtual {v0}, Lmaps/be/a;->b()I

    move-result v0

    invoke-static {p0, v1, v0, p3, p4}, Lmaps/br/d;->b(Lmaps/t/ah;[BIJ)Lmaps/br/d;
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->end()V

    return-object v0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->end()V

    throw v0
.end method

.method private static a([BLmaps/t/bx;Lmaps/t/bx;)Lmaps/t/cg;
    .locals 7

    const/4 v2, 0x0

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    new-instance v0, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v0}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v1

    :goto_0
    new-instance v5, Lmaps/t/by;

    add-int/lit8 v3, v1, 0x2

    invoke-direct {v5, v3}, Lmaps/t/by;-><init>(I)V

    if-eqz p1, :cond_0

    invoke-virtual {v5, p1}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    :cond_0
    move v3, v2

    move v4, v2

    :goto_1
    if-ge v2, v1, :cond_1

    invoke-static {v0}, Lmaps/t/bq;->b(Ljava/io/DataInput;)I

    move-result v6

    add-int/2addr v4, v6

    invoke-static {v0}, Lmaps/t/bq;->b(Ljava/io/DataInput;)I

    move-result v6

    add-int/2addr v3, v6

    invoke-static {v4, v3}, Lmaps/t/bx;->c(II)Lmaps/t/bx;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {v5, p2}, Lmaps/t/by;->a(Lmaps/t/bx;)Z

    :cond_2
    invoke-virtual {v5}, Lmaps/t/by;->c()Lmaps/t/cg;

    move-result-object v0

    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private a(Lmaps/bb/c;[Lmaps/t/bx;)V
    .locals 17

    const/4 v1, 0x0

    move v10, v1

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/br/d;->d:[Lmaps/br/c;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    if-ge v10, v1, :cond_4

    mul-int/lit8 v12, v10, 0x2

    mul-int/lit8 v1, v10, 0x2

    add-int/lit8 v13, v1, 0x1

    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v6

    const/4 v1, 0x2

    invoke-static {v6, v1}, Lmaps/bb/b;->a(Lmaps/bb/c;I)I

    move-result v7

    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v13}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v11

    const/4 v1, 0x2

    invoke-static {v11, v1}, Lmaps/bb/b;->a(Lmaps/bb/c;I)I

    move-result v14

    const/4 v1, 0x0

    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lmaps/bb/c;->i(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Lmaps/bb/c;->c(I)[B

    move-result-object v1

    :cond_0
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lmaps/bb/b;->a(Lmaps/bb/c;II)I

    move-result v9

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lmaps/bb/b;->a(Lmaps/bb/c;II)I

    move-result v8

    move-object/from16 v0, p1

    invoke-static {v6, v8, v0}, Lmaps/br/d;->a(Lmaps/bb/c;ILmaps/bb/c;)[Lmaps/br/b;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v2, 0x4

    aget-object v15, p2, v13

    aget-object v16, p2, v12

    move-object/from16 v0, v16

    invoke-static {v1, v15, v0}, Lmaps/br/d;->a([BLmaps/t/bx;Lmaps/t/bx;)Lmaps/t/cg;

    move-result-object v5

    if-nez v15, :cond_1

    if-nez v16, :cond_1

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Both polyline endpoints are missing for segment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/br/d;->d:[Lmaps/br/c;

    aget-object v3, v3, v12

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in tile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/br/d;->a:Lmaps/t/ah;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    if-nez v15, :cond_2

    const/4 v2, 0x2

    const/4 v1, 0x5

    :goto_1
    const/4 v3, 0x4

    const/4 v15, 0x0

    invoke-static {v6, v3, v15}, Lmaps/bb/b;->a(Lmaps/bb/c;II)I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_6

    or-int/lit8 v6, v2, 0x8

    :goto_2
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v11, v2, v3}, Lmaps/bb/b;->a(Lmaps/bb/c;II)I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    or-int/lit8 v1, v1, 0x8

    move v11, v1

    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/br/d;->d:[Lmaps/br/c;

    new-instance v1, Lmaps/br/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/br/d;->a:Lmaps/t/ah;

    invoke-static {v2, v12}, Lmaps/br/c;->a(Lmaps/t/ah;I)J

    move-result-wide v2

    invoke-direct/range {v1 .. v9}, Lmaps/br/c;-><init>(J[Lmaps/br/b;Lmaps/t/cg;IIII)V

    aput-object v1, v15, v12

    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/br/d;->d:[Lmaps/br/c;

    new-instance v1, Lmaps/br/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/br/d;->a:Lmaps/t/ah;

    invoke-static {v2, v13}, Lmaps/br/c;->a(Lmaps/t/ah;I)J

    move-result-wide v2

    move v6, v11

    move v7, v14

    invoke-direct/range {v1 .. v9}, Lmaps/br/c;-><init>(J[Lmaps/br/b;Lmaps/t/cg;IIII)V

    aput-object v1, v15, v13

    invoke-virtual {v5}, Lmaps/t/cg;->b()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_3

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Segment polyline had fewer than two points for segment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/br/d;->d:[Lmaps/br/c;

    aget-object v3, v3, v12

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in tile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/br/d;->a:Lmaps/t/ah;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    if-nez v16, :cond_7

    const/4 v2, 0x1

    const/4 v1, 0x6

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto/16 :goto_0

    :cond_4
    return-void

    :cond_5
    move v11, v1

    goto :goto_3

    :cond_6
    move v6, v2

    goto :goto_2

    :cond_7
    move v1, v2

    move v2, v3

    goto/16 :goto_1
.end method

.method private static a(Lmaps/t/ah;I[BI)V
    .locals 4

    const/16 v0, 0x20

    new-array v0, v0, [B

    invoke-virtual {p0}, Lmaps/t/ah;->d()I

    move-result v1

    invoke-virtual {p0}, Lmaps/t/ah;->e()I

    move-result v2

    invoke-virtual {p0}, Lmaps/t/ah;->c()I

    move-result v3

    invoke-static {v1, v2, v3, p1, v0}, Lmaps/an/a;->a(IIII[B)V

    new-instance v1, Lmaps/an/a;

    invoke-direct {v1}, Lmaps/an/a;-><init>()V

    const/16 v2, 0x100

    invoke-virtual {v1, v0, v2}, Lmaps/an/a;->b([BI)V

    array-length v0, p2

    sub-int/2addr v0, p3

    invoke-virtual {v1, p2, p3, v0}, Lmaps/an/a;->a([BII)V

    return-void
.end method

.method private static a([B)[I
    .locals 5

    new-instance v1, Ljava/io/DataInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v3, v2, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-static {v1}, Lmaps/t/bq;->a(Ljava/io/DataInput;)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method private static a(Lmaps/bb/c;ILmaps/bb/c;)[Lmaps/br/b;
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0, v9}, Lmaps/bb/c;->j(I)I

    move-result v5

    if-ne p1, v4, :cond_1

    move v3, v4

    :goto_0
    if-lez v5, :cond_3

    new-array v1, v5, [Lmaps/br/b;

    :goto_1
    if-ge v2, v5, :cond_2

    invoke-virtual {p0, v9, v2}, Lmaps/bb/c;->c(II)I

    move-result v0

    const/4 v6, 0x5

    invoke-virtual {p2, v6, v0}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v6

    const/4 v0, 0x0

    invoke-virtual {v6, v8}, Lmaps/bb/c;->i(I)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6, v8}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v7, Lmaps/br/b;

    invoke-virtual {v6, v4}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6, v0, v3}, Lmaps/br/b;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v7, v1, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v3, v2

    goto :goto_0

    :cond_2
    move-object v0, v1

    :goto_2
    return-object v0

    :cond_3
    new-array v0, v4, [Lmaps/br/b;

    sget-object v1, Lmaps/br/c;->a:Lmaps/br/b;

    aput-object v1, v0, v2

    goto :goto_2
.end method

.method private a(Lmaps/bb/c;)[Lmaps/t/bx;
    .locals 9

    const/4 v8, 0x4

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/br/d;->d:[Lmaps/br/c;

    array-length v0, v0

    new-array v3, v0, [Lmaps/t/bx;

    invoke-virtual {p1, v8}, Lmaps/bb/c;->j(I)I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v8, v2}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v0

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lmaps/bb/c;->d(I)I

    move-result v5

    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lmaps/bb/c;->d(I)I

    move-result v6

    invoke-static {v5, v6}, Lmaps/t/bx;->a(II)Lmaps/t/bx;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lmaps/bb/c;->c(I)[B

    move-result-object v0

    invoke-static {v0}, Lmaps/br/d;->a([B)[I

    move-result-object v6

    move v0, v1

    :goto_1
    array-length v7, v6

    if-ge v0, v7, :cond_0

    aget v7, v6, v0

    aput-object v5, v3, v7

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-object v3
.end method

.method private static b(Lmaps/t/ah;[BIJ)Lmaps/br/d;
    .locals 2

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/as/e;->e:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v1, p2}, Lmaps/bb/c;->a(Ljava/io/InputStream;I)I

    new-instance v1, Lmaps/br/d;

    invoke-direct {v1, p0, v0, p3, p4}, Lmaps/br/d;-><init>(Lmaps/t/ah;Lmaps/bb/c;J)V

    return-object v1
.end method

.method private b(Lmaps/bb/c;)V
    .locals 18

    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lmaps/bb/c;->j(I)I

    move-result v7

    new-instance v8, Lmaps/t/bx;

    invoke-direct {v8}, Lmaps/t/bx;-><init>()V

    new-instance v9, Lmaps/t/bx;

    invoke-direct {v9}, Lmaps/t/bx;-><init>()V

    const/4 v1, 0x0

    move v6, v1

    :goto_0
    if-ge v6, v7, :cond_4

    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lmaps/bb/c;->c(I)[B

    move-result-object v2

    invoke-static {v2}, Lmaps/br/d;->a([B)[I

    move-result-object v10

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lmaps/bb/c;->c(I)[B

    move-result-object v1

    invoke-static {v1}, Lmaps/br/d;->a([B)[I

    move-result-object v11

    const/4 v2, 0x0

    array-length v1, v10

    new-array v12, v1, [Lmaps/br/a;

    const/4 v1, 0x0

    :goto_1
    array-length v3, v10

    if-ge v1, v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/br/d;->d:[Lmaps/br/c;

    aget v4, v10, v1

    aget-object v13, v3, v4

    const/4 v4, 0x0

    const/4 v3, 0x0

    move/from16 v17, v3

    move v3, v4

    move v4, v1

    move/from16 v1, v17

    :goto_2
    array-length v5, v10

    if-ge v1, v5, :cond_2

    array-length v5, v11

    if-lt v2, v5, :cond_1

    array-length v1, v10

    move v4, v1

    :cond_0
    :goto_3
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    aget v14, v11, v2

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/br/d;->d:[Lmaps/br/c;

    aget v15, v10, v1

    xor-int/lit8 v15, v15, 0x1

    aget-object v15, v5, v15

    add-int/lit8 v5, v3, 0x1

    new-instance v16, Lmaps/br/a;

    move-object/from16 v0, v16

    invoke-direct {v0, v15, v14}, Lmaps/br/a;-><init>(Lmaps/br/c;I)V

    aput-object v16, v12, v3

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v13, v8}, Lmaps/br/c;->a(Lmaps/t/bx;)V

    const/4 v3, 0x0

    invoke-virtual {v15, v3, v9}, Lmaps/br/c;->a(ILmaps/t/bx;)V

    invoke-virtual {v8, v9}, Lmaps/t/bx;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Polylines did not line up when creating arc:  intersection: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " fromIndex: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " toIndex: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " point1: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Lmaps/t/bx;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " point2: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Lmaps/t/bx;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    new-array v1, v3, [Lmaps/br/a;

    const/4 v3, 0x0

    const/4 v5, 0x0

    array-length v14, v1

    invoke-static {v12, v3, v1, v5, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v13, v1}, Lmaps/br/c;->a([Lmaps/br/a;)V

    add-int/lit8 v1, v4, 0x1

    goto/16 :goto_1

    :cond_3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto/16 :goto_0

    :cond_4
    return-void

    :cond_5
    move v3, v5

    goto/16 :goto_3
.end method


# virtual methods
.method public a(Lmaps/ae/d;)Z
    .locals 4

    iget-wide v0, p0, Lmaps/br/d;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lmaps/ae/d;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/br/d;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lmaps/ae/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Lmaps/ae/d;)V
    .locals 0

    return-void
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lmaps/br/d;->c:J

    return-wide v0
.end method

.method public g()Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/br/d;->a:Lmaps/t/ah;

    return-object v0
.end method

.method public h()Lmaps/o/c;
    .locals 1

    sget-object v0, Lmaps/o/c;->i:Lmaps/o/c;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lmaps/br/d;->b:I

    return v0
.end method

.method public j()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public k()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public r()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method
