.class Lmaps/ca/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:Landroid/util/Pair;

.field public b:Lmaps/t/ah;

.field public c:Lmaps/ca/c;


# direct methods
.method public constructor <init>(Landroid/util/Pair;Lmaps/t/ah;Lmaps/ca/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ca/b;->a:Landroid/util/Pair;

    iput-object p2, p0, Lmaps/ca/b;->b:Lmaps/t/ah;

    iput-object p3, p0, Lmaps/ca/b;->c:Lmaps/ca/c;

    return-void
.end method


# virtual methods
.method public a(Lmaps/ca/b;)I
    .locals 3

    iget-object v0, p0, Lmaps/ca/b;->c:Lmaps/ca/c;

    iget-wide v0, v0, Lmaps/ca/c;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p1, Lmaps/ca/b;->c:Lmaps/ca/c;

    iget-wide v1, v1, Lmaps/ca/c;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/ca/b;

    invoke-virtual {p0, p1}, Lmaps/ca/b;->a(Lmaps/ca/b;)I

    move-result v0

    return v0
.end method
