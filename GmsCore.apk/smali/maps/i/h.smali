.class public interface abstract Lmaps/i/h;
.super Ljava/lang/Object;


# virtual methods
.method public abstract a(Lmaps/y/am;)Lmaps/y/k;
.end method

.method public abstract a(Z)Lmaps/y/x;
.end method

.method public abstract a()V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/IOnMapClickListener;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;)V
.end method

.method public abstract a(Lmaps/af/e;)V
.end method

.method public abstract a(Lmaps/af/q;)V
.end method

.method public abstract a(Lmaps/cf/a;)V
.end method

.method public abstract a(Lmaps/y/ab;)V
.end method

.method public abstract a(Lmaps/y/bc;)V
.end method

.method public abstract a(Lmaps/y/bh;Lmaps/y/ah;)V
.end method

.method public abstract a(ZZ)V
.end method

.method public abstract b()V
.end method

.method public abstract b(Lmaps/y/bc;)V
.end method

.method public abstract c()V
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()V
.end method

.method public abstract d(Z)V
.end method

.method public abstract e()V
.end method

.method public abstract e(Z)V
.end method

.method public abstract f(Z)V
.end method

.method public abstract g(Z)V
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getHeight()I
.end method

.method public abstract getResources()Landroid/content/res/Resources;
.end method

.method public abstract getWidth()I
.end method

.method public abstract h()Lmaps/bq/d;
.end method

.method public abstract h(Z)V
.end method

.method public abstract j()V
.end method

.method public abstract l()Z
.end method

.method public abstract m()Z
.end method

.method public abstract n()Z
.end method

.method public abstract o()Z
.end method

.method public abstract p()Lmaps/af/v;
.end method

.method public abstract setOnKeyListener(Landroid/view/View$OnKeyListener;)V
.end method
