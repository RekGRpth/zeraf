.class public Lmaps/i/a;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/i/u;


# instance fields
.field private final a:Lmaps/n/b;

.field private final b:Landroid/view/View;


# direct methods
.method constructor <init>(Lmaps/n/b;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/i/a;->a:Lmaps/n/b;

    iput-object p2, p0, Lmaps/i/a;->b:Landroid/view/View;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/i/u;
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lmaps/n/b;

    invoke-direct {v0, p0, p1}, Lmaps/n/b;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    sget v2, Lmaps/cc/h;->c:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v0, v1}, Lmaps/n/b;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v1, Lmaps/cc/d;->Y:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/n/b;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v4}, Lmaps/n/b;->setCacheColorHint(I)V

    invoke-virtual {v0, v5}, Lmaps/n/b;->setChoiceMode(I)V

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lmaps/n/b;->setDivider(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v4}, Lmaps/n/b;->setVerticalScrollBarEnabled(Z)V

    invoke-virtual {v0, v5}, Lmaps/n/b;->setScrollingCacheEnabled(Z)V

    invoke-virtual {v0, v5}, Lmaps/n/b;->setSmoothScrollbarEnabled(Z)V

    invoke-virtual {v0, v6}, Lmaps/n/b;->setVisibility(I)V

    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    new-instance v2, Lmaps/i/a;

    invoke-direct {v2, v0, v1}, Lmaps/i/a;-><init>(Lmaps/n/b;Landroid/view/View;)V

    return-object v2
.end method


# virtual methods
.method public a()Lmaps/n/b;
    .locals 1

    iget-object v0, p0, Lmaps/i/a;->a:Lmaps/n/b;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lmaps/i/a;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public a(Lmaps/i/m;)V
    .locals 2

    iget-object v1, p0, Lmaps/i/a;->a:Lmaps/n/b;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lmaps/n/b;->c(Lmaps/b/r;)V

    return-void

    :cond_0
    invoke-interface {p1}, Lmaps/i/m;->d()Lmaps/b/r;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmaps/i/a;->b:Landroid/view/View;

    return-object v0
.end method
