.class Lmaps/i/s;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/y/bm;


# instance fields
.field final synthetic a:Lmaps/i/b;


# direct methods
.method constructor <init>(Lmaps/i/b;)V
    .locals 0

    iput-object p1, p0, Lmaps/i/s;->a:Lmaps/i/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lmaps/y/v;FF)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/y/v;FFF)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/y/v;Lmaps/t/bx;)V
    .locals 2

    iget-object v0, p0, Lmaps/i/s;->a:Lmaps/i/b;

    invoke-static {v0}, Lmaps/i/b;->a(Lmaps/i/b;)Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lmaps/i/s;->a:Lmaps/i/b;

    invoke-static {v0}, Lmaps/i/b;->a(Lmaps/i/b;)Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;

    move-result-object v0

    invoke-static {p2}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IOnMapLongClickListener;->onMapLongClick(Lcom/google/android/gms/maps/model/LatLng;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public b(Lmaps/y/v;Lmaps/t/bx;)V
    .locals 2

    iget-object v0, p0, Lmaps/i/s;->a:Lmaps/i/b;

    invoke-static {v0}, Lmaps/i/b;->b(Lmaps/i/b;)Lcom/google/android/gms/maps/internal/IOnMapClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lmaps/i/s;->a:Lmaps/i/b;

    invoke-static {v0}, Lmaps/i/b;->b(Lmaps/i/b;)Lcom/google/android/gms/maps/internal/IOnMapClickListener;

    move-result-object v0

    invoke-static {p2}, Lmaps/au/c;->a(Lmaps/t/bx;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IOnMapClickListener;->onMapClick(Lcom/google/android/gms/maps/model/LatLng;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
