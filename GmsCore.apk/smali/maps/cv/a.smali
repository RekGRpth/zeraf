.class public Lmaps/cv/a;
.super Lmaps/e/l;


# instance fields
.field private final a:[Lmaps/bl/a;

.field private final b:J


# direct methods
.method public constructor <init>(Lmaps/ae/d;J)V
    .locals 8

    const/16 v7, 0x14

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lmaps/e/l;-><init>(ZLmaps/ae/d;)V

    iput-wide p2, p0, Lmaps/cv/a;->b:J

    new-instance v1, Lmaps/bl/a;

    const v0, 0x23df0ff

    const v2, -0x74b8e5f

    invoke-direct {v1, v0, v2}, Lmaps/bl/a;-><init>(II)V

    new-instance v2, Lmaps/bl/a;

    const v0, 0x23b63b8

    const v3, -0x747d71b

    invoke-direct {v2, v0, v3}, Lmaps/bl/a;-><init>(II)V

    new-array v0, v7, [Lmaps/bl/a;

    iput-object v0, p0, Lmaps/cv/a;->a:[Lmaps/bl/a;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v7, :cond_0

    iget-object v3, p0, Lmaps/cv/a;->a:[Lmaps/bl/a;

    invoke-virtual {v2}, Lmaps/bl/a;->a()I

    move-result v4

    invoke-virtual {v1}, Lmaps/bl/a;->a()I

    move-result v5

    sub-int/2addr v4, v5

    mul-int/2addr v4, v0

    div-int/lit8 v4, v4, 0x14

    invoke-virtual {v2}, Lmaps/bl/a;->b()I

    move-result v5

    invoke-virtual {v1}, Lmaps/bl/a;->b()I

    move-result v6

    sub-int/2addr v5, v6

    mul-int/2addr v5, v0

    div-int/lit8 v5, v5, 0x14

    invoke-virtual {v1, v4, v5}, Lmaps/bl/a;->b(II)Lmaps/bl/a;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public f()Ljava/lang/String;
    .locals 1

    const-string v0, "bogus"

    return-object v0
.end method

.method protected g()V
    .locals 0

    return-void
.end method

.method protected h()V
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmaps/cv/a;->b(I)V

    move v0, v1

    move v2, v1

    :cond_0
    :goto_0
    iget-boolean v3, p0, Lmaps/cv/a;->g:Z

    if-nez v3, :cond_1

    new-instance v3, Lmaps/e/r;

    invoke-direct {v3}, Lmaps/e/r;-><init>()V

    const-string v4, "gps"

    invoke-virtual {v3, v4}, Lmaps/e/r;->a(Ljava/lang/String;)Lmaps/e/r;

    move-result-object v3

    iget-object v4, p0, Lmaps/cv/a;->a:[Lmaps/bl/a;

    aget-object v4, v4, v2

    invoke-virtual {v3, v4}, Lmaps/e/r;->a(Lmaps/bl/a;)Lmaps/e/r;

    move-result-object v3

    const/high16 v4, 0x3f800000

    invoke-virtual {v3, v4}, Lmaps/e/r;->a(F)Lmaps/e/r;

    move-result-object v3

    const/high16 v4, 0x41200000

    invoke-virtual {v3, v4}, Lmaps/e/r;->c(F)Lmaps/e/r;

    move-result-object v3

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Lmaps/e/r;->b(F)Lmaps/e/r;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/e/r;->c()Lmaps/e/a;

    move-result-object v3

    invoke-virtual {p0, v3}, Lmaps/cv/a;->b(Lmaps/e/a;)V

    add-int/lit8 v0, v0, 0xa

    iget-object v3, p0, Lmaps/cv/a;->h:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lmaps/cv/a;->h:Ljava/lang/Object;

    iget-wide v5, p0, Lmaps/cv/a;->b:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lmaps/cv/a;->a:[Lmaps/bl/a;

    array-length v3, v3

    if-lt v2, v3, :cond_0

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    return-void

    :catch_0
    move-exception v4

    goto :goto_1
.end method
