.class public Lmaps/k/d;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/bb/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static b()Lmaps/bb/c;
    .locals 3

    new-instance v0, Lmaps/bb/c;

    sget-object v1, Lmaps/c/w;->c:Lmaps/bb/d;

    invoke-direct {v0, v1}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const/4 v1, 0x1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lmaps/bb/c;->b(ILjava/lang/String;)Lmaps/bb/c;

    return-object v0
.end method

.method private b(Lmaps/bb/c;)Z
    .locals 5

    const/4 v3, 0x2

    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/k/d;->a:Lmaps/bb/c;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/k/d;->a:Lmaps/bb/c;

    invoke-virtual {v1, v0}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0}, Lmaps/bb/c;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/k/d;->a:Lmaps/bb/c;

    invoke-virtual {v1, v3}, Lmaps/bb/c;->e(I)J

    move-result-wide v1

    invoke-virtual {p1, v3}, Lmaps/bb/c;->e(I)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lmaps/bb/c;
    .locals 4

    iget-object v0, p0, Lmaps/k/d;->a:Lmaps/bb/c;

    if-nez v0, :cond_1

    :try_start_0
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v0

    const-string v1, "ShortbreadToken"

    invoke-interface {v0, v1}, Lmaps/bj/b;->c(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    sget-object v1, Lmaps/c/w;->c:Lmaps/bb/d;

    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v1, v2}, Lmaps/bb/b;->a(Lmaps/bb/d;Ljava/io/DataInput;)Lmaps/bb/c;

    move-result-object v0

    iput-object v0, p0, Lmaps/k/d;->a:Lmaps/bb/c;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/k/d;->a:Lmaps/bb/c;

    if-nez v0, :cond_1

    invoke-static {}, Lmaps/k/d;->b()Lmaps/bb/c;

    move-result-object v0

    iput-object v0, p0, Lmaps/k/d;->a:Lmaps/bb/c;

    :cond_1
    iget-object v0, p0, Lmaps/k/d;->a:Lmaps/bb/c;

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Lmaps/bb/c;)V
    .locals 3

    invoke-direct {p0, p1}, Lmaps/k/d;->b(Lmaps/bb/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lmaps/k/d;->a:Lmaps/bb/c;

    :try_start_0
    new-instance v0, Lmaps/cs/b;

    invoke-direct {v0}, Lmaps/cs/b;-><init>()V

    iget-object v1, p0, Lmaps/k/d;->a:Lmaps/bb/c;

    invoke-static {v0, v1}, Lmaps/bb/b;->a(Ljava/io/DataOutput;Lmaps/bb/c;)V

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ae/c;->c()Lmaps/bj/b;

    move-result-object v1

    const-string v2, "ShortbreadToken"

    invoke-virtual {v0}, Lmaps/cs/b;->a()[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lmaps/bj/b;->a(Ljava/lang/String;[B)Z

    invoke-static {}, Lmaps/bh/f;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/k/d;->a:Lmaps/bb/c;

    goto :goto_0
.end method
