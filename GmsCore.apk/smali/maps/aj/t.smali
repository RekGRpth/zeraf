.class public final enum Lmaps/aj/t;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/aj/t;

.field public static final enum b:Lmaps/aj/t;

.field public static final enum c:Lmaps/aj/t;

.field private static final synthetic d:[Lmaps/aj/t;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/aj/t;

    const-string v1, "USER_INITIATED"

    invoke-direct {v0, v1, v2}, Lmaps/aj/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/t;->a:Lmaps/aj/t;

    new-instance v0, Lmaps/aj/t;

    const-string v1, "NO_NETWORK_CONNECTION_IN_A_LONG_TIME"

    invoke-direct {v0, v1, v3}, Lmaps/aj/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/t;->b:Lmaps/aj/t;

    new-instance v0, Lmaps/aj/t;

    const-string v1, "OFFLINE_TURNED_OFF"

    invoke-direct {v0, v1, v4}, Lmaps/aj/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/t;->c:Lmaps/aj/t;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/aj/t;

    sget-object v1, Lmaps/aj/t;->a:Lmaps/aj/t;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/aj/t;->b:Lmaps/aj/t;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/aj/t;->c:Lmaps/aj/t;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/aj/t;->d:[Lmaps/aj/t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/aj/t;
    .locals 1

    const-class v0, Lmaps/aj/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/aj/t;

    return-object v0
.end method

.method public static values()[Lmaps/aj/t;
    .locals 1

    sget-object v0, Lmaps/aj/t;->d:[Lmaps/aj/t;

    invoke-virtual {v0}, [Lmaps/aj/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/aj/t;

    return-object v0
.end method
