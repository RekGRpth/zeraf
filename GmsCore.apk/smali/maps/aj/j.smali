.class final enum Lmaps/aj/j;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/aj/j;

.field public static final enum b:Lmaps/aj/j;

.field public static final enum c:Lmaps/aj/j;

.field public static final enum d:Lmaps/aj/j;

.field private static final synthetic e:[Lmaps/aj/j;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/aj/j;

    const-string v1, "PREFETCHING"

    invoke-direct {v0, v1, v2}, Lmaps/aj/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/j;->a:Lmaps/aj/j;

    new-instance v0, Lmaps/aj/j;

    const-string v1, "REMOVING"

    invoke-direct {v0, v1, v3}, Lmaps/aj/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/j;->b:Lmaps/aj/j;

    new-instance v0, Lmaps/aj/j;

    const-string v1, "SUSPENDED"

    invoke-direct {v0, v1, v4}, Lmaps/aj/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/j;->c:Lmaps/aj/j;

    new-instance v0, Lmaps/aj/j;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v5}, Lmaps/aj/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aj/j;->d:Lmaps/aj/j;

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/aj/j;

    sget-object v1, Lmaps/aj/j;->a:Lmaps/aj/j;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/aj/j;->b:Lmaps/aj/j;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/aj/j;->c:Lmaps/aj/j;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/aj/j;->d:Lmaps/aj/j;

    aput-object v1, v0, v5

    sput-object v0, Lmaps/aj/j;->e:[Lmaps/aj/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/aj/j;
    .locals 1

    const-class v0, Lmaps/aj/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/aj/j;

    return-object v0
.end method

.method public static values()[Lmaps/aj/j;
    .locals 1

    sget-object v0, Lmaps/aj/j;->e:[Lmaps/aj/j;

    invoke-virtual {v0}, [Lmaps/aj/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/aj/j;

    return-object v0
.end method
