.class public abstract Lmaps/o/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field private static final B:Ljava/util/Map;

.field private static final H:Lmaps/t/ac;

.field private static final I:Lmaps/t/s;

.field private static final J:Lmaps/t/s;

.field private static final K:Lmaps/t/s;

.field private static final L:Lmaps/t/cv;

.field private static final M:Lmaps/t/aa;

.field private static final N:Lmaps/t/aa;

.field private static final O:Lmaps/t/aa;

.field private static P:Lmaps/o/m;

.field public static final a:Lmaps/o/c;

.field public static final b:Lmaps/o/c;

.field public static final c:Lmaps/o/c;

.field public static final d:Lmaps/o/c;

.field public static final e:Lmaps/o/c;

.field public static final f:Lmaps/o/c;

.field public static final g:Lmaps/o/c;

.field public static final h:Lmaps/o/c;

.field public static final i:Lmaps/o/c;

.field public static final j:Lmaps/o/c;

.field public static final k:Lmaps/o/c;

.field public static final l:Lmaps/o/c;

.field public static final m:Lmaps/o/c;

.field public static final n:Lmaps/o/c;

.field public static final o:Lmaps/o/c;

.field public static final p:Lmaps/o/c;

.field public static final q:Lmaps/o/c;

.field public static final r:Lmaps/o/c;

.field public static final s:Lmaps/o/c;

.field public static final t:Lmaps/o/c;

.field public static final u:Lmaps/o/c;

.field public static final v:Lmaps/o/c;


# instance fields
.field public final A:Ljava/lang/String;

.field private final C:I

.field private final D:Z

.field private final E:Z

.field private final F:Z

.field private final G:Lmaps/ag/e;

.field public final w:I

.field public final x:I

.field public final y:Z

.field public final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/16 v10, 0xc

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmaps/o/c;->B:Ljava/util/Map;

    new-instance v0, Lmaps/o/i;

    const/16 v3, 0xa

    invoke-direct {v0, v3, v9}, Lmaps/o/i;-><init>(ILmaps/o/r;)V

    invoke-virtual {v0, v4}, Lmaps/o/i;->f(Z)Lmaps/o/k;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/k;->c(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->a:Lmaps/o/c;

    new-instance v0, Lmaps/o/i;

    const/16 v3, 0x16

    invoke-direct {v0, v3, v9}, Lmaps/o/i;-><init>(ILmaps/o/r;)V

    invoke-virtual {v0, v4}, Lmaps/o/i;->f(Z)Lmaps/o/k;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/k;->c(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->b:Lmaps/o/c;

    new-instance v0, Lmaps/o/i;

    const/16 v3, 0x14

    invoke-direct {v0, v3, v9}, Lmaps/o/i;-><init>(ILmaps/o/r;)V

    invoke-virtual {v0, v4}, Lmaps/o/i;->f(Z)Lmaps/o/k;

    move-result-object v0

    const-string v3, "_tran_base"

    invoke-virtual {v0, v3}, Lmaps/o/k;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->c(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->c:Lmaps/o/c;

    new-instance v0, Lmaps/o/f;

    const/4 v3, 0x3

    invoke-direct {v0, v3, v9}, Lmaps/o/f;-><init>(ILmaps/o/r;)V

    invoke-virtual {v0, v2}, Lmaps/o/f;->b(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->d:Lmaps/o/c;

    new-instance v0, Lmaps/o/f;

    invoke-direct {v0, v10, v9}, Lmaps/o/f;-><init>(ILmaps/o/r;)V

    const-string v3, "_ter"

    invoke-virtual {v0, v3}, Lmaps/o/f;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/o/b;->b(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->e:Lmaps/o/c;

    new-instance v0, Lmaps/o/k;

    const/4 v3, 0x4

    invoke-direct {v0, v3, v9}, Lmaps/o/k;-><init>(ILmaps/o/r;)V

    const-string v3, "_traf"

    invoke-virtual {v0, v3}, Lmaps/o/k;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->f:Lmaps/o/c;

    new-instance v0, Lmaps/o/k;

    const/16 v3, 0x17

    invoke-direct {v0, v3, v9}, Lmaps/o/k;-><init>(ILmaps/o/r;)V

    const-string v3, "_traf"

    invoke-virtual {v0, v3}, Lmaps/o/k;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->g:Lmaps/o/c;

    new-instance v0, Lmaps/o/a;

    const/16 v3, 0x8

    invoke-direct {v0, v3, v9}, Lmaps/o/a;-><init>(ILmaps/o/r;)V

    invoke-virtual {v0}, Lmaps/o/a;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->h:Lmaps/o/c;

    new-instance v0, Lmaps/o/t;

    const/16 v3, 0xb

    invoke-direct {v0, v3, v9}, Lmaps/o/t;-><init>(ILmaps/o/r;)V

    invoke-virtual {v0, v4}, Lmaps/o/t;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->i:Lmaps/o/c;

    sget-boolean v0, Lmaps/ae/h;->O:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/o/i;

    const/16 v3, 0x12

    invoke-direct {v0, v3, v9}, Lmaps/o/i;-><init>(ILmaps/o/r;)V

    invoke-virtual {v0, v4}, Lmaps/o/i;->e(Z)Lmaps/o/i;

    move-result-object v0

    const-string v3, "_vec_bic"

    invoke-virtual {v0, v3}, Lmaps/o/i;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->j:Lmaps/o/c;

    :goto_0
    new-instance v0, Lmaps/o/f;

    const/4 v3, 0x7

    invoke-direct {v0, v3, v9}, Lmaps/o/f;-><init>(ILmaps/o/r;)V

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, Lmaps/o/f;->a(I)Lmaps/o/b;

    move-result-object v0

    const-string v3, "_ter_bic"

    invoke-virtual {v0, v3}, Lmaps/o/b;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->k:Lmaps/o/c;

    new-instance v0, Lmaps/o/f;

    const/4 v3, 0x6

    invoke-direct {v0, v3, v9}, Lmaps/o/f;-><init>(ILmaps/o/r;)V

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, Lmaps/o/f;->a(I)Lmaps/o/b;

    move-result-object v0

    const-string v3, "_hy_bic"

    invoke-virtual {v0, v3}, Lmaps/o/b;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->l:Lmaps/o/c;

    new-instance v0, Lmaps/o/k;

    const/16 v3, 0xd

    invoke-direct {v0, v3, v9}, Lmaps/o/k;-><init>(ILmaps/o/r;)V

    const-string v3, "_tran"

    invoke-virtual {v0, v3}, Lmaps/o/k;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->m:Lmaps/o/c;

    new-instance v0, Lmaps/o/k;

    const/16 v3, 0xe

    invoke-direct {v0, v3, v9}, Lmaps/o/k;-><init>(ILmaps/o/r;)V

    invoke-virtual {v0, v4}, Lmaps/o/k;->d(Z)Lmaps/o/b;

    move-result-object v0

    const-string v3, "_inaka"

    invoke-virtual {v0, v3}, Lmaps/o/b;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->n:Lmaps/o/c;

    new-instance v0, Lmaps/o/u;

    const/16 v3, 0xf

    invoke-direct {v0, v3, v9}, Lmaps/o/u;-><init>(ILmaps/o/r;)V

    const-string v3, "_labl"

    invoke-virtual {v0, v3}, Lmaps/o/u;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->c(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->o:Lmaps/o/c;

    new-instance v0, Lmaps/o/u;

    const/16 v3, 0x15

    invoke-direct {v0, v3, v9}, Lmaps/o/u;-><init>(ILmaps/o/r;)V

    const-string v3, "_tran_labl"

    invoke-virtual {v0, v3}, Lmaps/o/u;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->c(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->p:Lmaps/o/c;

    new-instance v0, Lmaps/o/k;

    const/16 v3, 0x10

    invoke-direct {v0, v3, v9}, Lmaps/o/k;-><init>(ILmaps/o/r;)V

    const-string v3, "_psm"

    invoke-virtual {v0, v3}, Lmaps/o/k;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->a(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->q:Lmaps/o/c;

    new-instance v0, Lmaps/o/k;

    const/16 v3, 0x11

    invoke-direct {v0, v3, v9}, Lmaps/o/k;-><init>(ILmaps/o/r;)V

    const-string v3, "_related"

    invoke-virtual {v0, v3}, Lmaps/o/k;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->a(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->r:Lmaps/o/c;

    new-instance v0, Lmaps/o/k;

    const/16 v3, 0x18

    invoke-direct {v0, v3, v9}, Lmaps/o/k;-><init>(ILmaps/o/r;)V

    const-string v3, "_high"

    invoke-virtual {v0, v3}, Lmaps/o/k;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->a(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->s:Lmaps/o/c;

    new-instance v0, Lmaps/o/k;

    const/16 v3, 0x19

    invoke-direct {v0, v3, v9}, Lmaps/o/k;-><init>(ILmaps/o/r;)V

    const-string v3, "_api"

    invoke-virtual {v0, v3}, Lmaps/o/k;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->t:Lmaps/o/c;

    new-instance v0, Lmaps/o/k;

    invoke-direct {v0, v2, v9}, Lmaps/o/k;-><init>(ILmaps/o/r;)V

    const-string v3, "_star"

    invoke-virtual {v0, v3}, Lmaps/o/k;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->a(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->u:Lmaps/o/c;

    new-instance v0, Lmaps/o/k;

    const/16 v3, 0x1a

    invoke-direct {v0, v3, v9}, Lmaps/o/k;-><init>(ILmaps/o/r;)V

    const-string v3, "_spotlight"

    invoke-virtual {v0, v3}, Lmaps/o/k;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->v:Lmaps/o/c;

    new-instance v0, Lmaps/t/ac;

    const/4 v3, 0x0

    new-array v4, v2, [I

    invoke-direct {v0, v2, v3, v4, v2}, Lmaps/t/ac;-><init>(IF[II)V

    sput-object v0, Lmaps/o/c;->H:Lmaps/t/ac;

    new-instance v0, Lmaps/t/s;

    const/high16 v3, -0x1000000

    sget-object v4, Lmaps/o/c;->H:Lmaps/t/ac;

    invoke-direct {v0, v3, v4}, Lmaps/t/s;-><init>(ILmaps/t/ac;)V

    sput-object v0, Lmaps/o/c;->I:Lmaps/t/s;

    new-instance v0, Lmaps/t/s;

    const v3, -0xffff01

    sget-object v4, Lmaps/o/c;->H:Lmaps/t/ac;

    invoke-direct {v0, v3, v4}, Lmaps/t/s;-><init>(ILmaps/t/ac;)V

    sput-object v0, Lmaps/o/c;->J:Lmaps/t/s;

    new-instance v0, Lmaps/t/s;

    const/high16 v3, -0x10000

    sget-object v4, Lmaps/o/c;->H:Lmaps/t/ac;

    invoke-direct {v0, v3, v4}, Lmaps/t/s;-><init>(ILmaps/t/ac;)V

    sput-object v0, Lmaps/o/c;->K:Lmaps/t/s;

    new-instance v0, Lmaps/t/cv;

    const/16 v3, 0xa

    const v4, 0x3f99999a

    const/high16 v5, 0x3f800000

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lmaps/t/cv;-><init>(IIIFFI)V

    sput-object v0, Lmaps/o/c;->L:Lmaps/t/cv;

    new-instance v0, Lmaps/t/aa;

    sget-object v5, Lmaps/o/c;->L:Lmaps/t/cv;

    sget-object v6, Lmaps/o/c;->I:Lmaps/t/s;

    move v2, v10

    move-object v3, v9

    move-object v4, v9

    move-object v7, v9

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Lmaps/t/aa;-><init>(II[I[Lmaps/t/ac;Lmaps/t/cv;Lmaps/t/s;Lmaps/t/ac;Lmaps/t/c;)V

    sput-object v0, Lmaps/o/c;->M:Lmaps/t/aa;

    new-instance v0, Lmaps/t/aa;

    sget-object v5, Lmaps/o/c;->L:Lmaps/t/cv;

    sget-object v6, Lmaps/o/c;->K:Lmaps/t/s;

    move v2, v10

    move-object v3, v9

    move-object v4, v9

    move-object v7, v9

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Lmaps/t/aa;-><init>(II[I[Lmaps/t/ac;Lmaps/t/cv;Lmaps/t/s;Lmaps/t/ac;Lmaps/t/c;)V

    sput-object v0, Lmaps/o/c;->N:Lmaps/t/aa;

    new-instance v0, Lmaps/t/aa;

    sget-object v5, Lmaps/o/c;->L:Lmaps/t/cv;

    sget-object v6, Lmaps/o/c;->J:Lmaps/t/s;

    move v2, v10

    move-object v3, v9

    move-object v4, v9

    move-object v7, v9

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Lmaps/t/aa;-><init>(II[I[Lmaps/t/ac;Lmaps/t/cv;Lmaps/t/s;Lmaps/t/ac;Lmaps/t/c;)V

    sput-object v0, Lmaps/o/c;->O:Lmaps/t/aa;

    return-void

    :cond_0
    new-instance v0, Lmaps/o/f;

    const/4 v3, 0x2

    invoke-direct {v0, v3, v9}, Lmaps/o/f;-><init>(ILmaps/o/r;)V

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, Lmaps/o/f;->a(I)Lmaps/o/b;

    move-result-object v0

    const-string v3, "_bas_bic"

    invoke-virtual {v0, v3}, Lmaps/o/b;->a(Ljava/lang/String;)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmaps/o/b;->d(Z)Lmaps/o/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/o/b;->a()Lmaps/o/c;

    move-result-object v0

    sput-object v0, Lmaps/o/c;->j:Lmaps/o/c;

    goto/16 :goto_0
.end method

.method private constructor <init>(Lmaps/o/b;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/o/b;->a(Lmaps/o/b;)I

    move-result v0

    iput v0, p0, Lmaps/o/c;->w:I

    invoke-static {p1}, Lmaps/o/b;->b(Lmaps/o/b;)I

    move-result v0

    iput v0, p0, Lmaps/o/c;->x:I

    invoke-static {p1}, Lmaps/o/b;->c(Lmaps/o/b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/o/c;->A:Ljava/lang/String;

    invoke-static {p1}, Lmaps/o/b;->d(Lmaps/o/b;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/o/c;->y:Z

    invoke-static {p1}, Lmaps/o/b;->e(Lmaps/o/b;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/o/c;->D:Z

    invoke-static {p1}, Lmaps/o/b;->f(Lmaps/o/b;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/o/c;->E:Z

    invoke-static {p1}, Lmaps/o/b;->g(Lmaps/o/b;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/o/c;->F:Z

    iget-boolean v0, p0, Lmaps/o/c;->F:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/o/c;->k()Lmaps/ag/e;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lmaps/o/c;->G:Lmaps/ag/e;

    iget v0, p0, Lmaps/o/c;->w:I

    iget v1, p0, Lmaps/o/c;->x:I

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    iput v0, p0, Lmaps/o/c;->z:I

    sget-object v0, Lmaps/o/c;->B:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lmaps/o/c;->C:I

    iget v0, p0, Lmaps/o/c;->w:I

    iget v1, p0, Lmaps/o/c;->x:I

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lmaps/o/c;->B:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Tile type with key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " already defined"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v1, Lmaps/o/c;->B:Ljava/util/Map;

    invoke-interface {v1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lmaps/o/c;->B:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x20

    if-le v0, v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Currently maximum 32 tile types allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method

.method synthetic constructor <init>(Lmaps/o/b;Lmaps/o/r;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/o/c;-><init>(Lmaps/o/b;)V

    return-void
.end method

.method public static a(I)Lmaps/o/c;
    .locals 2

    sget-object v0, Lmaps/o/c;->B:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/o/c;

    return-object v0
.end method

.method static b()I
    .locals 2

    invoke-static {}, Lmaps/af/w;->f()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    const/16 v1, 0x80

    mul-int/lit8 v0, v0, 0x12

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x24

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method static synthetic b(I)I
    .locals 1

    invoke-static {p0}, Lmaps/o/c;->c(I)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lmaps/o/c;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/o/c;->D:Z

    return v0
.end method

.method static c()I
    .locals 2

    invoke-static {}, Lmaps/af/w;->f()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    const/16 v1, 0x100

    mul-int/lit8 v0, v0, 0x20

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private static c(I)I
    .locals 1

    const/16 v0, 0xa0

    if-le p0, v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(Lmaps/o/c;)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/o/c;->E:Z

    return v0
.end method

.method public static e()Ljava/lang/Iterable;
    .locals 1

    sget-object v0, Lmaps/o/c;->B:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l()Lmaps/o/m;
    .locals 1

    sget-object v0, Lmaps/o/c;->P:Lmaps/o/m;

    return-object v0
.end method

.method static synthetic m()Lmaps/t/aa;
    .locals 1

    sget-object v0, Lmaps/o/c;->M:Lmaps/t/aa;

    return-object v0
.end method

.method static synthetic n()Lmaps/t/aa;
    .locals 1

    sget-object v0, Lmaps/o/c;->N:Lmaps/t/aa;

    return-object v0
.end method

.method static synthetic o()Lmaps/t/aa;
    .locals 1

    sget-object v0, Lmaps/o/c;->O:Lmaps/t/aa;

    return-object v0
.end method


# virtual methods
.method abstract a()I
.end method

.method public a(ILmaps/af/q;)I
    .locals 0

    return p1
.end method

.method public a(Lmaps/o/c;)I
    .locals 2

    iget v0, p0, Lmaps/o/c;->C:I

    iget v1, p1, Lmaps/o/c;->C:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(Ljava/lang/String;ZLmaps/ag/g;)Lmaps/ag/s;
    .locals 6

    iget-boolean v0, p0, Lmaps/o/c;->F:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/ag/j;

    if-eqz p2, :cond_1

    const/4 v2, -0x1

    :goto_1
    iget-object v3, p0, Lmaps/o/c;->G:Lmaps/ag/e;

    move-object v1, p1

    move-object v4, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lmaps/ag/j;-><init>(Ljava/lang/String;ILmaps/ag/e;Lmaps/o/c;Lmaps/ag/g;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmaps/o/c;->a()I

    move-result v2

    goto :goto_1
.end method

.method public abstract a(Lmaps/ak/a;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lmaps/an/y;
.end method

.method public a(Lmaps/t/ax;)Lmaps/t/bx;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/o/c;

    invoke-virtual {p0, p1}, Lmaps/o/c;->a(Lmaps/o/c;)I

    move-result v0

    return v0
.end method

.method public d()Lmaps/ag/a;
    .locals 2

    new-instance v0, Lmaps/ag/ab;

    invoke-static {}, Lmaps/o/c;->c()I

    move-result v1

    invoke-direct {v0, v1}, Lmaps/ag/ab;-><init>(I)V

    return-object v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lmaps/o/c;->C:I

    return v0
.end method

.method public g()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public i()Lmaps/l/h;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public j()Lmaps/t/aa;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method abstract k()Lmaps/ag/e;
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    :try_start_0
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-ne p0, v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "unknown"

    goto :goto_1
.end method
