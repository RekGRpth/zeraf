.class Lmaps/o/s;
.super Lmaps/o/c;


# direct methods
.method private constructor <init>(Lmaps/o/a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmaps/o/c;-><init>(Lmaps/o/b;Lmaps/o/r;)V

    return-void
.end method

.method synthetic constructor <init>(Lmaps/o/a;Lmaps/o/r;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/o/s;-><init>(Lmaps/o/a;)V

    return-void
.end method


# virtual methods
.method a()I
    .locals 2

    invoke-static {}, Lmaps/ae/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LayerTileType does not support disk caches."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lmaps/ak/a;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lmaps/an/y;
    .locals 2

    const/16 v0, 0x100

    invoke-static {p3, v0}, Lmaps/y/ab;->a(Landroid/content/res/Resources;I)I

    move-result v0

    new-instance v1, Lmaps/an/k;

    invoke-direct {v1, p1, v0, p4, p5}, Lmaps/an/k;-><init>(Lmaps/ak/a;ILjava/util/Locale;Ljava/io/File;)V

    return-object v1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lmaps/o/c;

    invoke-super {p0, p1}, Lmaps/o/c;->a(Lmaps/o/c;)I

    move-result v0

    return v0
.end method

.method public k()Lmaps/ag/e;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
