.class public Lmaps/s/j;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/HashMap;

.field private final b:Ljava/util/List;

.field private c:B


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/f/cs;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/j;->a:Ljava/util/HashMap;

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/s/j;->b:Ljava/util/List;

    const/4 v0, 0x0

    iput-byte v0, p0, Lmaps/s/j;->c:B

    return-void
.end method


# virtual methods
.method public a(Lmaps/t/aa;)B
    .locals 3

    iget-object v0, p0, Lmaps/s/j;->a:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/t/aa;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    if-nez v0, :cond_1

    iget-byte v0, p0, Lmaps/s/j;->c:B

    add-int/lit8 v1, v0, 0x1

    int-to-byte v1, v1

    iput-byte v1, p0, Lmaps/s/j;->c:B

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    iget-object v1, p0, Lmaps/s/j;->a:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/t/aa;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    iget-object v2, p0, Lmaps/s/j;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lmaps/s/j;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/s/j;->b:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    invoke-interface {v1, v2, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    return v0
.end method

.method public a()Landroid/graphics/Bitmap;
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lmaps/s/j;->c()I

    move-result v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iget-object v0, p0, Lmaps/s/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/aa;

    invoke-virtual {v0}, Lmaps/t/aa;->c()I

    move-result v3

    if-lt v3, v7, :cond_0

    invoke-virtual {v0, v2}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/ac;->b()I

    move-result v3

    invoke-virtual {v0, v6}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->b()I

    move-result v0

    :goto_1
    invoke-virtual {v4, v2, v1, v3}, Landroid/graphics/Bitmap;->setPixel(III)V

    invoke-virtual {v4, v6, v1, v0}, Landroid/graphics/Bitmap;->setPixel(III)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lmaps/t/aa;->c()I

    move-result v3

    if-lt v3, v6, :cond_1

    const/high16 v3, -0x10000

    invoke-virtual {v0, v2}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->b()I

    move-result v0

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No Stroke styles."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v4
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lmaps/s/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public c()I
    .locals 2

    iget-byte v0, p0, Lmaps/s/j;->c:B

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmaps/cr/a;->a(II)I

    move-result v0

    return v0
.end method
