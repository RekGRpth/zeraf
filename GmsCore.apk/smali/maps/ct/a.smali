.class public Lmaps/ct/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Lmaps/ct/b;

.field private static b:Z

.field private static n:Lmaps/ct/a;

.field private static o:J

.field private static p:I


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/ct/a;->b:Z

    const-wide/32 v0, -0x927c0

    sput-wide v0, Lmaps/ct/a;->o:J

    const/4 v0, -0x1

    sput v0, Lmaps/ct/a;->p:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIIIIIJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ct/a;->c:Ljava/lang/String;

    iput p3, p0, Lmaps/ct/a;->j:I

    iput p4, p0, Lmaps/ct/a;->i:I

    iput p5, p0, Lmaps/ct/a;->h:I

    iput p6, p0, Lmaps/ct/a;->f:I

    iput p7, p0, Lmaps/ct/a;->g:I

    iput p8, p0, Lmaps/ct/a;->d:I

    iput p9, p0, Lmaps/ct/a;->e:I

    iput p2, p0, Lmaps/ct/a;->l:I

    iput p10, p0, Lmaps/ct/a;->k:I

    iput-wide p11, p0, Lmaps/ct/a;->m:J

    return-void
.end method

.method public static a()I
    .locals 6

    sget-boolean v0, Lmaps/ae/h;->L:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->b()J

    move-result-wide v0

    sget-wide v2, Lmaps/ct/a;->o:J

    sub-long v2, v0, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/32 v4, 0x927c0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    invoke-static {}, Lmaps/ct/a;->b()Lmaps/ct/a;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lmaps/ct/a;->d()I

    move-result v2

    sput v2, Lmaps/ct/a;->p:I

    :cond_1
    sput-wide v0, Lmaps/ct/a;->o:J

    :cond_2
    sget v0, Lmaps/ct/a;->p:I

    goto :goto_0
.end method

.method public static b()Lmaps/ct/a;
    .locals 1

    sget-object v0, Lmaps/ct/a;->a:Lmaps/ct/b;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/ct/a;->c()Lmaps/ct/a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lmaps/ct/a;->a:Lmaps/ct/b;

    invoke-interface {v0}, Lmaps/ct/b;->a()Lmaps/ct/a;

    move-result-object v0

    goto :goto_0
.end method

.method public static c()Lmaps/ct/a;
    .locals 13

    const/4 v2, -0x1

    sget-object v0, Lmaps/ct/a;->n:Lmaps/ct/a;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/ct/a;

    const/4 v1, 0x0

    invoke-static {}, Lmaps/ae/c;->e()Lmaps/ae/c;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ae/c;->b()Lmaps/ae/d;

    move-result-object v3

    invoke-interface {v3}, Lmaps/ae/d;->a()J

    move-result-wide v11

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    invoke-direct/range {v0 .. v12}, Lmaps/ct/a;-><init>(Ljava/lang/String;IIIIIIIIIJ)V

    sput-object v0, Lmaps/ct/a;->n:Lmaps/ct/a;

    :cond_0
    sget-object v0, Lmaps/ct/a;->n:Lmaps/ct/a;

    return-object v0
.end method


# virtual methods
.method public d()I
    .locals 1

    iget v0, p0, Lmaps/ct/a;->f:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lmaps/ct/a;

    iget v2, p0, Lmaps/ct/a;->j:I

    iget v3, p1, Lmaps/ct/a;->j:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lmaps/ct/a;->i:I

    iget v3, p1, Lmaps/ct/a;->i:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lmaps/ct/a;->j:I

    mul-int/lit8 v0, v0, 0x1d

    iget v1, p0, Lmaps/ct/a;->i:I

    add-int/2addr v0, v1

    return v0
.end method
