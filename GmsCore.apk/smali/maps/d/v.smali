.class public Lmaps/d/v;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/d/w;


# instance fields
.field protected a:Lmaps/d/m;

.field private b:Lmaps/d/a;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lmaps/d/m;)V
    .locals 1

    iput-object p2, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v0, Lmaps/d/a;

    invoke-direct {v0, p1, p0}, Lmaps/d/a;-><init>(Landroid/content/Context;Lmaps/d/w;)V

    iput-object v0, p0, Lmaps/d/v;->b:Lmaps/d/a;

    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/d/v;->b:Lmaps/d/a;

    invoke-virtual {v0, p1}, Lmaps/d/a;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public a(Lmaps/d/a;)Z
    .locals 3

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/k;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lmaps/d/k;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/d;)Z

    move-result v0

    return v0
.end method

.method public a(Lmaps/d/a;Z)Z
    .locals 3

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/l;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lmaps/d/l;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/b;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Lmaps/d/a;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/k;

    invoke-direct {v1, v2, p1}, Lmaps/d/k;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lmaps/d/v;->c:Z

    :cond_0
    return v0
.end method

.method public b(Lmaps/d/a;Z)Z
    .locals 3

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v2, Lmaps/d/l;

    invoke-direct {v2, v0, p1}, Lmaps/d/l;-><init>(ILmaps/d/a;)V

    invoke-interface {v1, v2}, Lmaps/d/m;->a(Lmaps/d/b;)Z

    move-result v0

    goto :goto_0
.end method

.method public c(Lmaps/d/a;)V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/d/v;->c:Z

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/k;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lmaps/d/k;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/d;)Z

    return-void
.end method

.method public c(Lmaps/d/a;Z)V
    .locals 3

    if-eqz p2, :cond_0

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/l;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p1}, Lmaps/d/l;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/b;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/l;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lmaps/d/l;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/b;)Z

    goto :goto_0
.end method

.method public d(Lmaps/d/a;)Z
    .locals 3

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/u;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lmaps/d/u;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/r;)Z

    move-result v0

    return v0
.end method

.method public e(Lmaps/d/a;)Z
    .locals 3

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/u;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Lmaps/d/u;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/r;)Z

    move-result v0

    return v0
.end method

.method public f(Lmaps/d/a;)V
    .locals 3

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/u;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lmaps/d/u;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/r;)Z

    return-void
.end method

.method public g(Lmaps/d/a;)Z
    .locals 3

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/x;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lmaps/d/x;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/r;)Z

    move-result v0

    return v0
.end method

.method public h(Lmaps/d/a;)Z
    .locals 3

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/x;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Lmaps/d/x;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/r;)Z

    move-result v0

    return v0
.end method

.method public i(Lmaps/d/a;)V
    .locals 3

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    new-instance v1, Lmaps/d/x;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lmaps/d/x;-><init>(ILmaps/d/a;)V

    invoke-interface {v0, v1}, Lmaps/d/m;->a(Lmaps/d/r;)Z

    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    invoke-interface {v0, p1}, Lmaps/d/m;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    invoke-interface {v0, p1}, Lmaps/d/m;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    invoke-interface {v0, p1}, Lmaps/d/m;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    invoke-interface {v0, p1, p2, p3, p4}, Lmaps/d/m;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    invoke-interface {v0, p1}, Lmaps/d/m;->onLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-boolean v0, p0, Lmaps/d/v;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    invoke-interface {v0, p1, p2, p3, p4}, Lmaps/d/m;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    invoke-interface {v0, p1}, Lmaps/d/m;->onShowPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    invoke-interface {v0, p1}, Lmaps/d/m;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmaps/d/v;->a:Lmaps/d/m;

    invoke-interface {v0, p1}, Lmaps/d/m;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
