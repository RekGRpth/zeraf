.class Lmaps/az/d;
.super Lmaps/ak/e;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Lmaps/ak/e;-><init>()V

    iput-object p1, p0, Lmaps/az/d;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lmaps/az/d;->b:Z

    return-void
.end method

.method static synthetic a(Lmaps/az/d;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/az/d;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x4b

    return v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 9

    const/4 v8, -0x1

    new-instance v1, Lmaps/bb/c;

    sget-object v0, Lmaps/c/ft;->a:Lmaps/bb/d;

    invoke-direct {v1, v0}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const-class v2, Lmaps/az/c;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lmaps/az/c;->f()Lmaps/bb/c;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lmaps/bb/c;->j(I)I

    move-result v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    invoke-static {}, Lmaps/az/c;->f()Lmaps/bb/c;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v4

    new-instance v5, Lmaps/bb/c;

    sget-object v6, Lmaps/c/ft;->d:Lmaps/bb/d;

    invoke-direct {v5, v6}, Lmaps/bb/c;-><init>(Lmaps/bb/d;)V

    const/4 v6, 0x1

    const/4 v7, -0x1

    invoke-static {v4, v6, v7}, Lmaps/bb/b;->a(Lmaps/bb/c;II)I

    move-result v6

    if-eq v6, v8, :cond_0

    const/4 v7, 0x1

    invoke-virtual {v5, v7, v6}, Lmaps/bb/c;->j(II)Lmaps/bb/c;

    :cond_0
    const/4 v6, 0x2

    invoke-virtual {v4, v6}, Lmaps/bb/c;->i(I)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x2

    invoke-virtual {v4, v6}, Lmaps/bb/c;->e(I)J

    move-result-wide v6

    const/4 v4, 0x2

    invoke-virtual {v5, v4, v6, v7}, Lmaps/bb/c;->b(IJ)Lmaps/bb/c;

    :cond_1
    const/4 v4, 0x1

    invoke-virtual {v1, v4, v5}, Lmaps/bb/c;->a(ILmaps/bb/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lmaps/az/c;->a(Lmaps/bb/c;)V

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lmaps/bb/c;->a(Ljava/io/OutputStream;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-object v2, Lmaps/c/ft;->c:Lmaps/bb/d;

    invoke-static {v2, p1}, Lmaps/bb/b;->a(Lmaps/bb/d;Ljava/io/DataInput;)Lmaps/bb/c;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmaps/bb/c;->j(I)I

    move-result v4

    const-class v5, Lmaps/az/c;

    monitor-enter v5

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    const/4 v6, 0x1

    :try_start_0
    invoke-virtual {v3, v6, v2}, Lmaps/bb/c;->g(II)Lmaps/bb/c;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lmaps/bb/c;->i(I)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v6}, Lmaps/az/c;->b(Lmaps/bb/c;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v6}, Lmaps/az/c;->c(Lmaps/bb/c;)V

    move v0, v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    invoke-static {}, Lmaps/az/c;->f()Lmaps/bb/c;

    move-result-object v2

    iget-object v3, p0, Lmaps/az/d;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lmaps/az/c;->a(Lmaps/bb/c;Ljava/lang/String;)Z

    :cond_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lmaps/az/d;->b:Z

    if-nez v0, :cond_4

    :cond_3
    invoke-static {}, Lmaps/az/c;->g()Lmaps/az/e;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lmaps/az/c;->h()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Lmaps/az/e;->a()V

    :cond_4
    invoke-static {}, Lmaps/az/c;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lmaps/az/c;->i()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Lmaps/az/c;->a(Z)Z

    invoke-static {}, Lmaps/az/c;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lmaps/ak/n;->e()Lmaps/ak/n;

    move-result-object v0

    iget-object v3, p0, Lmaps/az/d;->a:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lmaps/az/c;->a(Lmaps/ak/n;Ljava/lang/String;Z)V

    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_5
    return v1

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_6
    :try_start_3
    new-instance v0, Lmaps/az/k;

    invoke-static {}, Lmaps/k/j;->a()Lmaps/ai/d;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lmaps/az/k;-><init>(Lmaps/az/d;Lmaps/ai/d;)V

    invoke-static {v0}, Lmaps/az/c;->a(Lmaps/ai/b;)Lmaps/ai/b;

    invoke-static {}, Lmaps/az/c;->k()Lmaps/ai/b;

    move-result-object v0

    const-wide/32 v3, 0xa4cb80

    invoke-virtual {v0, v3, v4}, Lmaps/ai/b;->a(J)V

    invoke-static {}, Lmaps/az/c;->k()Lmaps/ai/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ai/b;->d()V

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public k_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public l_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
