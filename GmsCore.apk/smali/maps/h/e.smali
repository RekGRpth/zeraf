.class public Lmaps/h/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field protected volatile a:Landroid/os/Handler;

.field public b:Lmaps/h/a;

.field protected final c:Lmaps/h/c;

.field protected d:Lmaps/h/h;

.field private e:Lmaps/h/f;

.field private final f:Lmaps/h/d;

.field private g:Ljava/lang/Thread;

.field private h:Landroid/content/Context;

.field private final i:J

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/Object;

.field private n:Z

.field private o:Z

.field private p:J


# direct methods
.method public constructor <init>(Lmaps/h/d;Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lmaps/h/e;->k:Z

    iput-boolean v1, p0, Lmaps/h/e;->l:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/h/e;->m:Ljava/lang/Object;

    iput-boolean v1, p0, Lmaps/h/e;->n:Z

    iput-boolean v2, p0, Lmaps/h/e;->o:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/h/e;->p:J

    iput-object p1, p0, Lmaps/h/e;->f:Lmaps/h/d;

    invoke-direct {p0}, Lmaps/h/e;->k()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/h/e;->i:J

    iput-object p2, p0, Lmaps/h/e;->h:Landroid/content/Context;

    new-instance v0, Lmaps/h/c;

    invoke-direct {v0}, Lmaps/h/c;-><init>()V

    iput-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    new-instance v0, Lmaps/h/h;

    iget-object v1, p0, Lmaps/h/e;->f:Lmaps/h/d;

    iget-object v2, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-direct {v0, v1, v2}, Lmaps/h/h;-><init>(Lmaps/h/d;Lmaps/h/c;)V

    iput-object v0, p0, Lmaps/h/e;->d:Lmaps/h/h;

    new-instance v0, Lmaps/h/f;

    invoke-direct {v0}, Lmaps/h/f;-><init>()V

    iput-object v0, p0, Lmaps/h/e;->e:Lmaps/h/f;

    return-void
.end method

.method private a(J)J
    .locals 2

    iget-boolean v0, p0, Lmaps/h/e;->j:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lmaps/h/e;->i:J

    add-long/2addr p1, v0

    :cond_0
    return-wide p1
.end method

.method static synthetic a(Lmaps/h/e;)V
    .locals 0

    invoke-direct {p0}, Lmaps/h/e;->f()V

    return-void
.end method

.method private b(J)Z
    .locals 4

    const-wide/32 v0, 0xf4240

    div-long v0, p1, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v1}, Lmaps/h/c;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2}, Lmaps/h/e;->d(J)Lmaps/bw/a;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v1}, Lmaps/h/c;->f()Lmaps/t/bb;

    move-result-object v1

    invoke-static {v1}, Lmaps/h/i;->a(Lmaps/t/bb;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "wifi"

    invoke-static {p1, p2, v0, v1, v2}, Lmaps/h/i;->a(JLmaps/bw/a;Landroid/os/Bundle;Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    iget-object v1, p0, Lmaps/h/e;->d:Lmaps/h/h;

    invoke-virtual {v1, v0}, Lmaps/h/h;->a(Landroid/location/Location;)Z

    return-void
.end method

.method private d(J)Lmaps/bw/a;
    .locals 7

    iget-object v0, p0, Lmaps/h/e;->e:Lmaps/h/f;

    invoke-virtual {v0, p1, p2}, Lmaps/h/f;->a(J)Lmaps/bw/a;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/h/e;->d()J

    move-result-wide v1

    if-eqz v0, :cond_1

    iget v3, v0, Lmaps/bw/a;->d:I

    div-int/lit16 v3, v3, 0x3e8

    const/16 v4, 0x28

    if-le v3, v4, :cond_1

    iget-wide v3, p0, Lmaps/h/e;->p:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    iget-wide v3, p0, Lmaps/h/e;->p:J

    sub-long v3, v1, v3

    const-wide/16 v5, 0x7530

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    :cond_0
    iput-wide v1, p0, Lmaps/h/e;->p:J

    invoke-direct {p0}, Lmaps/h/e;->g()V

    iget-object v0, p0, Lmaps/h/e;->e:Lmaps/h/f;

    invoke-virtual {v0, p1, p2}, Lmaps/h/f;->a(J)Lmaps/bw/a;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/h/e;->o:Z

    if-eqz v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v0}, Lmaps/h/c;->b()V

    invoke-direct {p0}, Lmaps/h/e;->j()V

    iget-object v0, p0, Lmaps/h/e;->e:Lmaps/h/f;

    invoke-virtual {v0}, Lmaps/h/f;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/h/e;->n:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/h/e;->o:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private g()V
    .locals 1

    iget-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v0}, Lmaps/h/c;->b()V

    invoke-direct {p0}, Lmaps/h/e;->j()V

    iget-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v0}, Lmaps/h/c;->a()V

    invoke-virtual {p0}, Lmaps/h/e;->c()V

    return-void
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, Lmaps/h/e;->b:Lmaps/h/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/h/e;->b:Lmaps/h/a;

    invoke-virtual {v0}, Lmaps/h/a;->a()V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    iget-object v0, p0, Lmaps/h/e;->b:Lmaps/h/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/h/e;->b:Lmaps/h/a;

    invoke-virtual {v0}, Lmaps/h/a;->b()V

    :cond_0
    return-void
.end method

.method private j()V
    .locals 3

    iget-object v1, p0, Lmaps/h/e;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lmaps/h/e;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/h/e;->e:Lmaps/h/f;

    invoke-virtual {v0}, Lmaps/h/f;->b()V

    invoke-direct {p0}, Lmaps/h/e;->i()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/h/e;->l:Z

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    const-string v0, "LOCATION"

    const-string v2, "Now outside."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private k()J
    .locals 6

    invoke-virtual {p0}, Lmaps/h/e;->e()J

    move-result-wide v0

    invoke-virtual {p0}, Lmaps/h/e;->d()J

    move-result-wide v2

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    sub-long v0, v2, v0

    return-wide v0
.end method


# virtual methods
.method public a()V
    .locals 1

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lmaps/h/e;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    iget-object v0, p0, Lmaps/h/e;->g:Ljava/lang/Thread;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lmaps/h/e;->g:Ljava/lang/Thread;

    iget-object v0, p0, Lmaps/h/e;->g:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-enter p0

    :goto_1
    :try_start_4
    iget-object v0, p0, Lmaps/h/e;->a:Landroid/os/Handler;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-nez v0, :cond_2

    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    :cond_2
    const/4 v0, 0x0

    :try_start_7
    iput-boolean v0, p0, Lmaps/h/e;->o:Z

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    iget-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v0}, Lmaps/h/c;->a()V

    invoke-virtual {p0}, Lmaps/h/e;->c()V

    return-void
.end method

.method protected a(Landroid/hardware/SensorEvent;)V
    .locals 7

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, Lmaps/h/e;->k:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lmaps/h/e;->i:J

    invoke-virtual {p0}, Lmaps/h/e;->e()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p1, Landroid/hardware/SensorEvent;->timestamp:J

    sub-long/2addr v0, v2

    const-wide v2, 0x4e94914f0000L

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iput-boolean v5, p0, Lmaps/h/e;->j:Z

    :goto_0
    iput-boolean v4, p0, Lmaps/h/e;->k:Z

    :cond_0
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-direct {p0, v0, v1}, Lmaps/h/e;->a(J)J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lmaps/h/e;->b(J)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    return-void

    :cond_1
    iput-boolean v4, p0, Lmaps/h/e;->j:Z

    goto :goto_0

    :cond_2
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_2
    invoke-direct {p0, v1, v2}, Lmaps/h/e;->c(J)V

    goto :goto_1

    :pswitch_0
    iget-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v0}, Lmaps/h/c;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/h/e;->e:Lmaps/h/f;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v4

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v5

    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    invoke-virtual/range {v0 .. v5}, Lmaps/h/f;->a(JFFF)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/location/Location;Lmaps/t/bb;)V
    .locals 3

    iget-object v0, p0, Lmaps/h/e;->a:Landroid/os/Handler;

    if-nez v0, :cond_1

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    const-string v0, "LOCATION"

    const-string v1, "Warning: handler is null in locationChange."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    new-instance v2, Lmaps/h/g;

    invoke-direct {v2, p1, p2}, Lmaps/h/g;-><init>(Landroid/location/Location;Lmaps/t/bb;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method protected a(Landroid/location/Location;)Z
    .locals 4

    invoke-virtual {p0}, Lmaps/h/e;->d()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1b58

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/h/e;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lmaps/h/e;->n:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmaps/h/e;->n:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :cond_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected b(Landroid/location/Location;Lmaps/t/bb;)V
    .locals 4

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v0, p2}, Lmaps/h/c;->a(Lmaps/t/bb;)V

    if-eqz p2, :cond_3

    iget-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v0}, Lmaps/h/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v0}, Lmaps/h/c;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/h/e;->d:Lmaps/h/h;

    invoke-virtual {v0, p1}, Lmaps/h/h;->b(Landroid/location/Location;)Z

    iget-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v0}, Lmaps/h/c;->c()V

    :cond_2
    invoke-virtual {p0, p1}, Lmaps/h/e;->a(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/h/e;->c()V

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    const/high16 v2, -0x40800000

    invoke-static {p1, p2, v2}, Lmaps/h/i;->a(Landroid/location/Location;Lmaps/t/bb;F)Lmaps/bw/a;

    move-result-object v2

    iget-object v3, p0, Lmaps/h/e;->e:Lmaps/h/f;

    invoke-virtual {v3, v0, v1, v2}, Lmaps/h/f;->a(JLmaps/bw/a;)V

    invoke-direct {p0, v0, v1}, Lmaps/h/e;->c(J)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/h/e;->c:Lmaps/h/c;

    invoke-virtual {v0}, Lmaps/h/c;->d()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0}, Lmaps/h/e;->j()V

    :cond_4
    iget-object v0, p0, Lmaps/h/e;->d:Lmaps/h/h;

    invoke-virtual {v0, p1}, Lmaps/h/h;->b(Landroid/location/Location;)Z

    goto :goto_0
.end method

.method protected c()V
    .locals 3

    iget-object v1, p0, Lmaps/h/e;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lmaps/h/e;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/h/e;->e:Lmaps/h/f;

    invoke-virtual {v0}, Lmaps/h/f;->a()V

    invoke-direct {p0}, Lmaps/h/e;->h()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/h/e;->l:Z

    sget-boolean v0, Lmaps/ae/h;->g:Z

    if-eqz v0, :cond_0

    const-string v0, "LOCATION"

    const-string v2, "Now inside."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected d()J
    .locals 2

    iget-object v0, p0, Lmaps/h/e;->f:Lmaps/h/d;

    invoke-virtual {v0}, Lmaps/h/d;->o()Lmaps/ae/d;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/d;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method protected e()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public final run()V
    .locals 4

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lmaps/h/b;

    invoke-direct {v0, p0}, Lmaps/h/b;-><init>(Lmaps/h/e;)V

    iput-object v0, p0, Lmaps/h/e;->a:Landroid/os/Handler;

    new-instance v0, Lmaps/h/a;

    iget-object v1, p0, Lmaps/h/e;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    iget-object v3, p0, Lmaps/h/e;->h:Landroid/content/Context;

    invoke-direct {v0, v1, v2, v3}, Lmaps/h/a;-><init>(Landroid/os/Handler;ILandroid/content/Context;)V

    iput-object v0, p0, Lmaps/h/e;->b:Lmaps/h/a;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
