.class public Lmaps/l/ar;
.super Lmaps/l/ak;


# static fields
.field private static a:I

.field private static c:I

.field private static d:F

.field private static e:F


# instance fields
.field private final f:Lmaps/al/o;

.field private final g:Lmaps/s/g;

.field private h:Lmaps/s/t;

.field private i:Lmaps/s/r;

.field private final j:I

.field private final k:Lmaps/t/bx;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/high16 v1, 0x3f800000

    const/16 v0, 0x4000

    sput v0, Lmaps/l/ar;->a:I

    const/4 v0, 0x1

    sput v0, Lmaps/l/ar;->c:I

    sput v1, Lmaps/l/ar;->d:F

    sput v1, Lmaps/l/ar;->e:F

    return-void
.end method

.method private constructor <init>(IILjava/util/Set;Lmaps/cr/c;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0, p3}, Lmaps/l/ak;-><init>(Ljava/util/Set;)V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/s/t;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v0, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    iput-object v3, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    iput-object v3, p0, Lmaps/l/ar;->g:Lmaps/s/g;

    :goto_0
    new-instance v0, Lmaps/t/bx;

    invoke-direct {v0}, Lmaps/t/bx;-><init>()V

    iput-object v0, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    const/high16 v0, 0x10000

    mul-int/2addr v0, p2

    iput v0, p0, Lmaps/l/ar;->j:I

    return-void

    :cond_0
    new-instance v0, Lmaps/al/e;

    invoke-direct {v0, p1}, Lmaps/al/e;-><init>(I)V

    iput-object v0, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    new-instance v0, Lmaps/s/g;

    invoke-virtual {p4}, Lmaps/cr/c;->K()Lmaps/s/l;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lmaps/s/g;-><init>(ILmaps/s/l;)V

    iput-object v0, p0, Lmaps/l/ar;->g:Lmaps/s/g;

    goto :goto_0
.end method

.method private static a(Lmaps/t/ay;)I
    .locals 1

    invoke-virtual {p0}, Lmaps/t/ay;->c()Lmaps/t/cg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    return v0
.end method

.method public static a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;ILmaps/cr/c;)Lmaps/l/ar;
    .locals 11

    invoke-virtual {p0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v4

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, -0x1

    move v2, v0

    move v3, v1

    :goto_0
    invoke-interface {p2}, Lmaps/t/h;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {p2}, Lmaps/t/h;->a()Lmaps/t/ca;

    move-result-object v1

    instance-of v0, v1, Lmaps/t/ay;

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    if-le v2, v0, :cond_2

    move v0, v2

    :goto_1
    if-gez v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    new-instance v1, Lmaps/l/ar;

    invoke-direct {v1, v3, v0, v5, p4}, Lmaps/l/ar;-><init>(IILjava/util/Set;Lmaps/cr/c;)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ca;

    invoke-interface {v0}, Lmaps/t/ca;->a()I

    move-result v3

    const/4 v5, 0x5

    if-ne v3, v5, :cond_e

    check-cast v0, Lmaps/t/ay;

    invoke-direct {v1, p0, v4, v0}, Lmaps/l/ar;->a(Lmaps/t/ah;Lmaps/t/ax;Lmaps/t/ay;)V

    goto :goto_2

    :cond_2
    const/4 v2, 0x1

    move-object v0, v1

    check-cast v0, Lmaps/t/ay;

    invoke-static {v0}, Lmaps/l/ar;->a(Lmaps/t/ay;)I

    move-result v0

    sget v7, Lmaps/l/ar;->a:I

    if-le v0, v7, :cond_4

    sget-boolean v1, Lmaps/ae/h;->j:Z

    if-eqz v1, :cond_3

    const-string v1, "GLLineMesh"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "unable to handle the LineMesh with "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " vertices in "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " since the limit is "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v7, Lmaps/l/ar;->a:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-interface {p2}, Lmaps/t/h;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_4
    add-int v7, v0, v3

    sget v8, Lmaps/l/ar;->a:I

    if-le v7, v8, :cond_5

    move v0, v2

    goto :goto_1

    :cond_5
    add-int/2addr v0, v3

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v10, v2

    move v2, v0

    move v0, v10

    :goto_3
    invoke-interface {v1}, Lmaps/t/ca;->j()[I

    move-result-object v3

    array-length v7, v3

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v7, :cond_d

    aget v8, v3, v1

    if-ltz v8, :cond_6

    array-length v9, p1

    if-ge v8, v9, :cond_6

    aget-object v8, p1, v8

    invoke-virtual {v5, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_7
    instance-of v0, v1, Lmaps/t/az;

    if-eqz v0, :cond_10

    move-object v0, v1

    check-cast v0, Lmaps/t/az;

    invoke-static {v0}, Lmaps/l/ar;->a(Lmaps/t/az;)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Lmaps/t/ca;->h()Lmaps/t/aa;

    move-result-object v0

    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->c()F

    move-result v0

    invoke-static {v0}, Lmaps/l/ar;->b(F)I

    move-result v0

    if-eq v0, v2, :cond_9

    if-lez v2, :cond_8

    move v0, v2

    goto/16 :goto_1

    :cond_8
    move v2, v0

    :cond_9
    move-object v0, v1

    check-cast v0, Lmaps/t/az;

    invoke-static {v0}, Lmaps/l/ar;->b(Lmaps/t/az;)I

    move-result v0

    sget v7, Lmaps/l/ar;->a:I

    if-le v0, v7, :cond_b

    sget-boolean v1, Lmaps/ae/h;->j:Z

    if-eqz v1, :cond_a

    const-string v1, "GLLineMesh"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "unable to handle the Line with "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " vertices in "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " since the limit is "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v7, Lmaps/l/ar;->a:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/bt/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    invoke-interface {p2}, Lmaps/t/h;->next()Ljava/lang/Object;

    goto/16 :goto_0

    :cond_b
    add-int v7, v0, v3

    sget v8, Lmaps/l/ar;->a:I

    if-le v7, v8, :cond_c

    move v0, v2

    goto/16 :goto_1

    :cond_c
    add-int/2addr v0, v3

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v10, v2

    move v2, v0

    move v0, v10

    goto/16 :goto_3

    :cond_d
    invoke-interface {p2}, Lmaps/t/h;->next()Ljava/lang/Object;

    move v3, v2

    move v2, v0

    goto/16 :goto_0

    :cond_e
    invoke-interface {v0}, Lmaps/t/ca;->a()I

    move-result v3

    const/16 v5, 0x8

    if-ne v3, v5, :cond_1

    check-cast v0, Lmaps/t/az;

    invoke-direct {v1, p0, v4, v0}, Lmaps/l/ar;->a(Lmaps/t/ah;Lmaps/t/ax;Lmaps/t/az;)V

    goto/16 :goto_2

    :cond_f
    invoke-virtual {v1, p0, p3}, Lmaps/l/ar;->a(Lmaps/t/ah;I)V

    return-object v1

    :cond_10
    move v0, v2

    goto/16 :goto_1
.end method

.method public static declared-synchronized a(F)V
    .locals 4

    const-class v1, Lmaps/l/ar;

    monitor-enter v1

    :try_start_0
    sput p0, Lmaps/l/ar;->e:F

    const/high16 v0, 0x3f800000

    const/4 v2, 0x5

    sget v3, Lmaps/l/ar;->c:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    sget v3, Lmaps/l/ar;->e:F

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sput v0, Lmaps/l/ar;->d:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(I)V
    .locals 4

    const-class v1, Lmaps/l/ar;

    monitor-enter v1

    :try_start_0
    sput p0, Lmaps/l/ar;->c:I

    const/high16 v0, 0x3f800000

    const/4 v2, 0x5

    sget v3, Lmaps/l/ar;->c:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    sget v3, Lmaps/l/ar;->e:F

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sput v0, Lmaps/l/ar;->d:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lmaps/t/ah;Lmaps/t/ax;Lmaps/t/ay;)V
    .locals 9

    const/4 v1, 0x0

    invoke-virtual {p3}, Lmaps/t/ay;->h()Lmaps/t/aa;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/aa;->c()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->c()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Lmaps/t/ay;->c()Lmaps/t/cg;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cg;->b()I

    move-result v4

    invoke-virtual {p2}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v5

    invoke-virtual {p2}, Lmaps/t/ax;->g()I

    move-result v6

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_3

    iget-object v7, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    invoke-virtual {v3, v0, v7}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget-object v7, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    iget-object v8, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    invoke-static {v7, v5, v8}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    sget-boolean v7, Lmaps/ae/h;->F:Z

    if-eqz v7, :cond_2

    iget-object v7, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    iget-object v8, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    invoke-virtual {v7, v8, v6}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    iget-object v7, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    invoke-virtual {v2, v1}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v8

    invoke-virtual {v8}, Lmaps/t/ac;->b()I

    move-result v8

    invoke-virtual {v7, v8}, Lmaps/s/t;->e(I)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v7, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    iget-object v8, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    invoke-virtual {v7, v8, v6}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    goto :goto_2

    :cond_3
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->d()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lmaps/l/ar;->g:Lmaps/s/g;

    invoke-virtual {v2, v1}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/ac;->b()I

    move-result v1

    invoke-virtual {v0, v1, v4}, Lmaps/s/g;->a(II)V

    goto :goto_0
.end method

.method private a(Lmaps/t/ah;Lmaps/t/ax;Lmaps/t/az;)V
    .locals 10

    const/4 v1, 0x0

    invoke-virtual {p3}, Lmaps/t/az;->h()Lmaps/t/aa;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/aa;->c()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->c()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/ac;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Lmaps/t/az;->c()Lmaps/t/cg;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    mul-int/lit8 v5, v4, 0x2

    invoke-virtual {p2}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v6

    invoke-virtual {p2}, Lmaps/t/ax;->g()I

    move-result v7

    move v0, v1

    :goto_1
    if-gt v0, v4, :cond_4

    iget-object v8, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    invoke-virtual {v3, v0, v8}, Lmaps/t/cg;->a(ILmaps/t/bx;)V

    iget-object v8, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    iget-object v9, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    invoke-static {v8, v6, v9}, Lmaps/t/bx;->b(Lmaps/t/bx;Lmaps/t/bx;Lmaps/t/bx;)V

    sget-boolean v8, Lmaps/ae/h;->F:Z

    if-eqz v8, :cond_3

    iget-object v8, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    iget-object v9, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    invoke-virtual {v8, v9, v7}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    iget-object v8, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    invoke-virtual {v2, v1}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/ac;->b()I

    move-result v9

    invoke-virtual {v8, v9}, Lmaps/s/t;->e(I)V

    if-lez v0, :cond_2

    if-ge v0, v4, :cond_2

    iget-object v8, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    iget-object v9, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    invoke-virtual {v8, v9, v7}, Lmaps/s/t;->a(Lmaps/t/bx;I)V

    iget-object v8, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    invoke-virtual {v2, v1}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/t/ac;->b()I

    move-result v9

    invoke-virtual {v8, v9}, Lmaps/s/t;->e(I)V

    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v8, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    iget-object v9, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    invoke-virtual {v8, v9, v7}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    if-lez v0, :cond_2

    if-ge v0, v4, :cond_2

    iget-object v8, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    iget-object v9, p0, Lmaps/l/ar;->k:Lmaps/t/bx;

    invoke-virtual {v8, v9, v7}, Lmaps/al/o;->a(Lmaps/t/bx;I)V

    goto :goto_2

    :cond_4
    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->d()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lmaps/l/ar;->g:Lmaps/s/g;

    invoke-virtual {v2, v1}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/ac;->b()I

    move-result v1

    invoke-virtual {v0, v1, v5}, Lmaps/s/g;->a(II)V

    goto/16 :goto_0
.end method

.method public static declared-synchronized a(Lmaps/t/az;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-class v3, Lmaps/l/ar;

    monitor-enter v3

    :try_start_0
    invoke-virtual {p0}, Lmaps/t/az;->h()Lmaps/t/aa;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/aa;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/ac;->d()[I

    move-result-object v2

    array-length v2, v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    invoke-virtual {v4}, Lmaps/t/aa;->c()I

    move-result v5

    if-ne v5, v0, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/t/ac;->c()F

    move-result v4

    sget v5, Lmaps/l/ar;->d:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_1

    if-nez v2, :cond_1

    :goto_1
    monitor-exit v3

    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method static declared-synchronized b(F)I
    .locals 4

    const-class v1, Lmaps/l/ar;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    sget v2, Lmaps/l/ar;->c:I

    sget v3, Lmaps/l/ar;->e:F

    mul-float/2addr v3, p0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Lmaps/t/az;)I
    .locals 1

    invoke-virtual {p0}, Lmaps/t/az;->c()Lmaps/t/cg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method


# virtual methods
.method public a()I
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ar;->i:Lmaps/s/r;

    invoke-virtual {v0}, Lmaps/s/r;->d()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->d()I

    move-result v0

    iget-object v1, p0, Lmaps/l/ar;->g:Lmaps/s/g;

    invoke-virtual {v1}, Lmaps/s/g;->a()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/l/ar;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ar;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ar;->g:Lmaps/s/g;

    invoke-virtual {v0, p1}, Lmaps/s/g;->c(Lmaps/cr/c;)V

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 5

    const/high16 v4, 0x10000

    iget-object v0, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->c()I

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget v1, p0, Lmaps/l/ar;->j:I

    if-le v1, v4, :cond_2

    iget v1, p0, Lmaps/l/ar;->j:I

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    :cond_2
    iget-object v1, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    invoke-virtual {v1, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/ar;->g:Lmaps/s/g;

    invoke-virtual {v1, p1}, Lmaps/s/g;->a(Lmaps/cr/c;)V

    invoke-static {p1}, Lmaps/s/l;->c(Lmaps/cr/c;)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    invoke-virtual {v3}, Lmaps/al/o;->c()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    invoke-static {p1}, Lmaps/s/l;->d(Lmaps/cr/c;)V

    goto :goto_0
.end method

.method public a(Lmaps/t/ah;I)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/s/t;->f(I)Lmaps/s/r;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ar;->i:Lmaps/s/r;

    new-instance v0, Lmaps/q/aa;

    invoke-direct {v0, p2, p1}, Lmaps/q/aa;-><init>(ILmaps/t/ah;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LineMesh "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "   "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmaps/l/ar;->i:Lmaps/s/r;

    invoke-virtual {v2}, Lmaps/s/r;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/l/ar;->i:Lmaps/s/r;

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/r;)V

    new-instance v1, Lmaps/q/bb;

    invoke-direct {v1}, Lmaps/q/bb;-><init>()V

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/q/aa;->b(I)V

    iget-object v1, p0, Lmaps/l/ar;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/l/ar;->h:Lmaps/s/t;

    goto :goto_0
.end method

.method public b()I
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ar;->i:Lmaps/s/r;

    invoke-virtual {v0}, Lmaps/s/r;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x78

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x78

    iget-object v1, p0, Lmaps/l/ar;->g:Lmaps/s/g;

    invoke-virtual {v1}, Lmaps/s/g;->b()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/ar;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/ar;->f:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/ar;->g:Lmaps/s/g;

    invoke-virtual {v0, p1}, Lmaps/s/g;->b(Lmaps/cr/c;)V

    :cond_1
    return-void
.end method
