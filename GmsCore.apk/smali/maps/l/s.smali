.class public Lmaps/l/s;
.super Lmaps/l/ak;


# static fields
.field private static final a:[I

.field private static final c:I

.field private static volatile d:Z

.field private static final l:F


# instance fields
.field private final e:Lmaps/t/ah;

.field private final f:Lmaps/al/o;

.field private final g:Lmaps/al/a;

.field private final h:Lmaps/al/q;

.field private final i:Lmaps/al/i;

.field private j:Lmaps/s/r;

.field private k:Lmaps/s/t;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/l/s;->a:[I

    sget-object v0, Lmaps/l/s;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x64

    sput v0, Lmaps/l/s;->c:I

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/l/s;->d:Z

    const-wide/high16 v0, 0x4000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lmaps/l/s;->l:F

    return-void

    nop

    :array_0
    .array-data 4
        0xc
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
    .end array-data
.end method

.method private constructor <init>(Lmaps/t/ah;Lmaps/l/ah;Ljava/util/Set;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p3}, Lmaps/l/ak;-><init>(Ljava/util/Set;)V

    iput-object p1, p0, Lmaps/l/s;->e:Lmaps/t/ah;

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmaps/s/t;

    iget v1, p2, Lmaps/l/ah;->a:I

    const/16 v2, 0xd

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v0, p0, Lmaps/l/s;->k:Lmaps/s/t;

    iput-object v4, p0, Lmaps/l/s;->f:Lmaps/al/o;

    iput-object v4, p0, Lmaps/l/s;->g:Lmaps/al/a;

    iput-object v4, p0, Lmaps/l/s;->h:Lmaps/al/q;

    iput-object v4, p0, Lmaps/l/s;->i:Lmaps/al/i;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lmaps/al/o;

    iget v1, p2, Lmaps/l/ah;->a:I

    invoke-direct {v0, v1}, Lmaps/al/o;-><init>(I)V

    iput-object v0, p0, Lmaps/l/s;->f:Lmaps/al/o;

    new-instance v0, Lmaps/al/a;

    iget v1, p2, Lmaps/l/ah;->a:I

    invoke-direct {v0, v1}, Lmaps/al/a;-><init>(I)V

    iput-object v0, p0, Lmaps/l/s;->g:Lmaps/al/a;

    new-instance v0, Lmaps/al/q;

    iget v1, p2, Lmaps/l/ah;->a:I

    invoke-direct {v0, v1}, Lmaps/al/q;-><init>(I)V

    iput-object v0, p0, Lmaps/l/s;->h:Lmaps/al/q;

    new-instance v0, Lmaps/al/i;

    iget v1, p2, Lmaps/l/ah;->b:I

    invoke-direct {v0, v1}, Lmaps/al/i;-><init>(I)V

    iput-object v0, p0, Lmaps/l/s;->i:Lmaps/al/i;

    goto :goto_0
.end method

.method private static a(FI)F
    .locals 2

    int-to-float v0, p1

    mul-float/2addr v0, p0

    const/high16 v1, 0x3fa00000

    mul-float/2addr v0, v1

    const/high16 v1, 0x43800000

    div-float/2addr v0, v1

    return v0
.end method

.method private static a(IF)F
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/high16 v0, 0x42a00000

    div-float/2addr v0, p1

    :goto_0
    return v0

    :pswitch_0
    const/high16 v0, 0x43b40000

    div-float/2addr v0, p1

    goto :goto_0

    :pswitch_1
    const/high16 v0, 0x43700000

    div-float/2addr v0, p1

    goto :goto_0

    :pswitch_2
    const/high16 v0, 0x43200000

    div-float/2addr v0, p1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lmaps/t/ah;[Ljava/lang/String;Lmaps/t/h;)Lmaps/l/s;
    .locals 7

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    new-instance v3, Lmaps/l/ah;

    const/4 v0, 0x0

    invoke-direct {v3, v0}, Lmaps/l/ah;-><init>(Lmaps/l/l;)V

    invoke-interface {p2}, Lmaps/t/h;->b()V

    :cond_0
    invoke-interface {p2}, Lmaps/t/h;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Lmaps/t/h;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/t/ca;

    instance-of v1, v0, Lmaps/t/p;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lmaps/t/p;

    invoke-static {v1, v3}, Lmaps/l/s;->a(Lmaps/t/p;Lmaps/l/ah;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    invoke-interface {p2}, Lmaps/t/h;->c()V

    new-instance v1, Lmaps/l/s;

    invoke-direct {v1, p0, v3, v2}, Lmaps/l/s;-><init>(Lmaps/t/ah;Lmaps/l/ah;Ljava/util/Set;)V

    invoke-virtual {p0}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v2

    invoke-static {}, Lmaps/s/q;->a()Lmaps/s/q;

    move-result-object v3

    :goto_0
    invoke-interface {p2}, Lmaps/t/h;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Lmaps/t/h;->a()Lmaps/t/ca;

    move-result-object v0

    instance-of v4, v0, Lmaps/t/p;

    if-eqz v4, :cond_2

    check-cast v0, Lmaps/t/p;

    invoke-direct {v1, v2, v0, v3}, Lmaps/l/s;->a(Lmaps/t/ax;Lmaps/t/p;Lmaps/s/q;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_2
    invoke-direct {v1, p0}, Lmaps/l/s;->a(Lmaps/t/ah;)V

    return-object v1

    :cond_3
    invoke-interface {v0}, Lmaps/t/ca;->j()[I

    move-result-object v1

    array-length v4, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_0

    aget v5, v1, v0

    if-ltz v5, :cond_4

    array-length v6, p1

    if-ge v5, v6, :cond_4

    aget-object v5, p1, v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    invoke-interface {p2}, Lmaps/t/h;->next()Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(Lmaps/cr/c;Lmaps/af/q;)V
    .locals 4

    invoke-virtual {p0}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/cr/c;->r()V

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    return-void
.end method

.method private a(Lmaps/t/ah;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/s;->k:Lmaps/s/t;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lmaps/s/t;->f(I)Lmaps/s/r;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/s;->j:Lmaps/s/r;

    iget-object v0, p0, Lmaps/l/s;->k:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->h()V

    new-instance v0, Lmaps/q/aa;

    const/16 v1, 0xd

    invoke-direct {v0, v1, p1}, Lmaps/q/aa;-><init>(ILmaps/t/ah;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Traffic "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/l/s;->j:Lmaps/s/r;

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/r;)V

    new-instance v1, Lmaps/q/bb;

    invoke-direct {v1}, Lmaps/q/bb;-><init>()V

    invoke-virtual {v0, v1}, Lmaps/q/aa;->a(Lmaps/q/z;)V

    iget-object v1, p0, Lmaps/l/s;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/q/aa;->b(I)V

    goto :goto_0
.end method

.method private a(Lmaps/t/ax;Lmaps/t/p;Lmaps/s/q;)Z
    .locals 11

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {p2}, Lmaps/t/p;->c()Lmaps/t/cg;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/t/ax;->g()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f800000

    mul-float/2addr v1, v2

    const/high16 v2, 0x43800000

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lmaps/t/cg;->b(F)Lmaps/t/cg;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/lit8 v5, v0, -0x1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/s;->k:Lmaps/s/t;

    invoke-virtual {v0}, Lmaps/s/t;->c()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v5, 0x4

    add-int/2addr v0, v2

    invoke-virtual {p1}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v3

    invoke-virtual {p1}, Lmaps/t/ax;->g()I

    move-result v4

    invoke-virtual {p2}, Lmaps/t/p;->h()Lmaps/t/aa;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/aa;->c()I

    move-result v6

    if-gtz v6, :cond_1

    :goto_1
    return v10

    :cond_0
    iget-object v0, p0, Lmaps/l/s;->f:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->c()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/t/ac;->c()F

    move-result v6

    invoke-virtual {v2}, Lmaps/t/aa;->c()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v2, v7}, Lmaps/t/aa;->b(I)Lmaps/t/ac;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/ac;->b()I

    move-result v7

    invoke-static {v6, v4}, Lmaps/l/s;->a(FI)F

    move-result v2

    invoke-virtual {p2}, Lmaps/t/p;->d()Z

    move-result v8

    if-nez v8, :cond_2

    neg-float v2, v2

    :cond_2
    sget-boolean v8, Lmaps/ae/h;->F:Z

    if-eqz v8, :cond_3

    iget-object v0, p0, Lmaps/l/s;->k:Lmaps/s/t;

    mul-int/lit8 v5, v5, 0x4

    invoke-virtual {v0, v7, v5}, Lmaps/s/t;->b(II)V

    invoke-virtual {p2}, Lmaps/t/p;->e()I

    move-result v0

    invoke-static {v0, v6}, Lmaps/l/s;->a(IF)F

    move-result v5

    iget-object v6, p0, Lmaps/l/s;->k:Lmaps/s/t;

    iget-object v7, p0, Lmaps/l/s;->k:Lmaps/s/t;

    iget-object v8, p0, Lmaps/l/s;->k:Lmaps/s/t;

    move-object v0, p3

    invoke-virtual/range {v0 .. v9}, Lmaps/s/q;->a(Lmaps/t/cg;FLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;Lmaps/al/c;)V

    goto :goto_1

    :cond_3
    iget-object v8, p0, Lmaps/l/s;->g:Lmaps/al/a;

    invoke-virtual {v8, v0}, Lmaps/al/a;->b(I)V

    iget-object v0, p0, Lmaps/l/s;->g:Lmaps/al/a;

    mul-int/lit8 v5, v5, 0x4

    invoke-virtual {v0, v7, v5}, Lmaps/al/a;->b(II)V

    invoke-virtual {p2}, Lmaps/t/p;->e()I

    move-result v0

    invoke-static {v0, v6}, Lmaps/l/s;->a(IF)F

    move-result v5

    iget-object v6, p0, Lmaps/l/s;->f:Lmaps/al/o;

    iget-object v7, p0, Lmaps/l/s;->i:Lmaps/al/i;

    iget-object v8, p0, Lmaps/l/s;->h:Lmaps/al/q;

    move-object v0, p3

    invoke-virtual/range {v0 .. v9}, Lmaps/s/q;->a(Lmaps/t/cg;FLmaps/t/bx;IFLmaps/al/l;Lmaps/al/b;Lmaps/al/c;Lmaps/al/c;)V

    goto :goto_1
.end method

.method public static a(Lmaps/t/p;Lmaps/l/ah;)Z
    .locals 4

    invoke-virtual {p0}, Lmaps/t/p;->c()Lmaps/t/cg;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cg;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v1, v0, 0x4

    iget v2, p1, Lmaps/l/ah;->a:I

    add-int/2addr v2, v1

    const/16 v3, 0x1000

    if-le v2, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v2, p1, Lmaps/l/ah;->a:I

    add-int/2addr v1, v2

    iput v1, p1, Lmaps/l/ah;->a:I

    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    iget v2, p1, Lmaps/l/ah;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v2

    iput v0, p1, Lmaps/l/ah;->b:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lmaps/cr/c;)V
    .locals 6

    const/4 v5, 0x4

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {p1}, Lmaps/cr/c;->p()V

    iget-object v1, p0, Lmaps/l/s;->g:Lmaps/al/a;

    invoke-virtual {v1, p1}, Lmaps/al/a;->c(Lmaps/cr/c;)V

    iget-object v1, p0, Lmaps/l/s;->i:Lmaps/al/i;

    invoke-virtual {v1, p1, v5}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    invoke-virtual {p1}, Lmaps/cr/c;->e()J

    move-result-wide v1

    sget v3, Lmaps/l/s;->c:I

    int-to-long v3, v3

    rem-long/2addr v1, v3

    long-to-int v1, v1

    div-int/lit8 v1, v1, 0x64

    invoke-virtual {p1}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v2

    sget-object v3, Lmaps/l/s;->a:[I

    aget v1, v3, v1

    invoke-virtual {v2, v1}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {p1}, Lmaps/cr/c;->q()V

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v0, p0, Lmaps/l/s;->i:Lmaps/al/i;

    invoke-virtual {v0, p1, v5}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    invoke-virtual {p1}, Lmaps/cr/c;->b()V

    return-void
.end method

.method private b(Lmaps/cr/c;Lmaps/af/q;)V
    .locals 4

    const/4 v3, 0x4

    const/high16 v2, 0x10000

    sget-object v0, Lmaps/af/q;->a:Lmaps/af/q;

    if-eq p2, v0, :cond_0

    sget-object v0, Lmaps/af/q;->d:Lmaps/af/q;

    if-ne p2, v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v2, v2, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v0, p0, Lmaps/l/s;->i:Lmaps/al/i;

    invoke-virtual {v0, p1, v3}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    :cond_1
    invoke-virtual {p1}, Lmaps/cr/c;->a()Lmaps/s/i;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lmaps/s/i;->a(I)Lmaps/cr/a;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {p1}, Lmaps/cr/c;->p()V

    sget-boolean v0, Lmaps/l/s;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/l/s;->g:Lmaps/al/a;

    const/16 v1, 0xb0

    invoke-virtual {v0, v1}, Lmaps/al/a;->a(I)V

    :cond_2
    iget-object v0, p0, Lmaps/l/s;->g:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/s;->i:Lmaps/al/i;

    invoke-virtual {v0, p1, v3}, Lmaps/al/i;->a(Lmaps/cr/c;I)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/s;->j:Lmaps/s/r;

    invoke-virtual {v0}, Lmaps/s/r;->d()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/s;->f:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->d()I

    move-result v0

    iget-object v1, p0, Lmaps/l/s;->g:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/s;->h:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/s;->i:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lmaps/l/s;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/l/s;->f:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/s;->g:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/s;->h:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->e(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/s;->i:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->c(Lmaps/cr/c;)V

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 6

    const/16 v5, 0x1702

    const/16 v4, 0x1700

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/l/s;->i:Lmaps/al/i;

    invoke-virtual {v0}, Lmaps/al/i;->a()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/l/s;->f:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->d(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/s;->h:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->d(Lmaps/cr/c;)V

    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v0

    iget-object v1, p0, Lmaps/l/s;->e:Lmaps/t/ah;

    invoke-virtual {v1}, Lmaps/t/ah;->c()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    const/high16 v1, 0x3f000000

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    sget v2, Lmaps/l/s;->l:F

    invoke-interface {v1, v2, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :cond_2
    sget-boolean v1, Lmaps/ae/h;->K:Z

    if-eqz v1, :cond_4

    invoke-direct {p0, p1}, Lmaps/l/s;->b(Lmaps/cr/c;)V

    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v5}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lmaps/l/s;->b(Lmaps/cr/c;Lmaps/af/q;)V

    goto :goto_2
.end method

.method public b()I
    .locals 2

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/s;->j:Lmaps/s/r;

    invoke-virtual {v0}, Lmaps/s/r;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/s;->f:Lmaps/al/o;

    invoke-virtual {v0}, Lmaps/al/o;->a()I

    move-result v0

    add-int/lit16 v0, v0, 0xb8

    iget-object v1, p0, Lmaps/l/s;->g:Lmaps/al/a;

    invoke-virtual {v1}, Lmaps/al/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/s;->h:Lmaps/al/q;

    invoke-virtual {v1}, Lmaps/al/q;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/l/s;->i:Lmaps/al/i;

    invoke-virtual {v1}, Lmaps/al/i;->d()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 3

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/aa;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/s;->f:Lmaps/al/o;

    invoke-virtual {v0, p1}, Lmaps/al/o;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/s;->g:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->a(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/s;->h:Lmaps/al/q;

    invoke-virtual {v0, p1}, Lmaps/al/q;->b(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/s;->i:Lmaps/al/i;

    invoke-virtual {v0, p1}, Lmaps/al/i;->b(Lmaps/cr/c;)V

    :cond_1
    return-void
.end method
