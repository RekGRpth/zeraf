.class public Lmaps/l/g;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/l/t;


# instance fields
.field private a:Lmaps/l/w;

.field private final b:Lmaps/l/j;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private final e:Ljava/util/ArrayList;

.field private final f:Lmaps/al/l;

.field private final g:Lmaps/al/c;

.field private final h:Lmaps/s/t;

.field private final i:Lmaps/al/g;

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:Z

.field private o:Z


# direct methods
.method constructor <init>(Ljava/util/ArrayList;Lmaps/l/w;Lmaps/l/j;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmaps/l/g;->e:Ljava/util/ArrayList;

    new-instance v0, Lmaps/s/t;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, Lmaps/s/t;-><init>(IIZ)V

    iput-object v0, p0, Lmaps/l/g;->h:Lmaps/s/t;

    new-instance v0, Lmaps/al/g;

    iget-object v1, p0, Lmaps/l/g;->h:Lmaps/s/t;

    invoke-virtual {v1}, Lmaps/s/t;->e()I

    move-result v1

    invoke-direct {v0, v1}, Lmaps/al/g;-><init>(I)V

    iput-object v0, p0, Lmaps/l/g;->i:Lmaps/al/g;

    iget-object v0, p0, Lmaps/l/g;->h:Lmaps/s/t;

    iput-object v0, p0, Lmaps/l/g;->f:Lmaps/al/l;

    iget-object v0, p0, Lmaps/l/g;->h:Lmaps/s/t;

    iput-object v0, p0, Lmaps/l/g;->g:Lmaps/al/c;

    iput-object p2, p0, Lmaps/l/g;->a:Lmaps/l/w;

    iput-object p3, p0, Lmaps/l/g;->b:Lmaps/l/j;

    invoke-direct {p0}, Lmaps/l/g;->d()V

    iput-boolean v3, p0, Lmaps/l/g;->n:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/l/g;->o:Z

    return-void
.end method

.method public static a(Lmaps/t/bh;Lmaps/t/ca;Lmaps/bq/d;Lmaps/cr/b;Lmaps/p/y;Lmaps/cf/a;)Lmaps/l/g;
    .locals 10

    const/4 v8, 0x0

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    move v6, v0

    move-object v7, v1

    :goto_0
    invoke-virtual {p0}, Lmaps/t/bh;->b()I

    move-result v0

    if-ge v6, v0, :cond_8

    invoke-virtual {p0, v6}, Lmaps/t/bh;->a(I)Lmaps/t/a;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/a;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lmaps/t/a;->j()Lmaps/t/aa;

    move-result-object v4

    invoke-virtual {v1}, Lmaps/t/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {p4, p5, v0, v4, p2}, Lmaps/l/g;->a(Lmaps/p/y;Lmaps/cf/a;Ljava/lang/String;Lmaps/t/aa;Lmaps/bq/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v5, p5, Lmaps/cf/a;->d:Lmaps/p/l;

    instance-of v0, p1, Lmaps/t/bu;

    if-eqz v0, :cond_2

    iget-object v5, p5, Lmaps/cf/a;->a:Lmaps/p/l;

    :cond_0
    :goto_1
    new-instance v0, Lmaps/l/aj;

    invoke-virtual {v1}, Lmaps/t/a;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lmaps/bq/d;->n()F

    move-result v1

    invoke-static {v4, p5, v1}, Lmaps/l/aw;->a(Lmaps/t/aa;Lmaps/cf/a;F)I

    move-result v3

    move-object v1, p4

    invoke-direct/range {v0 .. v5}, Lmaps/l/aj;-><init>(Lmaps/p/y;Ljava/lang/String;ILmaps/t/aa;Lmaps/p/l;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object v1, v7

    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-object v7, v1

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lmaps/t/az;

    if-eqz v0, :cond_0

    iget-object v5, p5, Lmaps/cf/a;->h:Lmaps/p/l;

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Lmaps/t/a;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lmaps/by/b;->b()Lmaps/by/b;

    move-result-object v0

    invoke-virtual {v1}, Lmaps/t/a;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v8}, Lmaps/by/b;->a(Ljava/lang/String;Lmaps/by/a;)Lmaps/by/c;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/by/c;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lmaps/by/c;->c()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1}, Lmaps/t/a;->h()F

    move-result v0

    invoke-static {}, Lmaps/be/b;->a()Lmaps/be/o;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/be/o;->a()I

    invoke-virtual {v1}, Lmaps/t/a;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "/road_shields/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v1, p5, Lmaps/cf/a;->m:F

    mul-float/2addr v0, v1

    :goto_3
    invoke-virtual {p2}, Lmaps/bq/d;->n()F

    move-result v1

    mul-float/2addr v0, v1

    new-instance v1, Lmaps/l/x;

    invoke-direct {v1, v2, v0, p3}, Lmaps/l/x;-><init>(Landroid/graphics/Bitmap;FLmaps/cr/b;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto :goto_2

    :cond_4
    iget v1, p5, Lmaps/cf/a;->n:F

    mul-float/2addr v0, v1

    goto :goto_3

    :cond_5
    move-object v0, v8

    :goto_4
    return-object v0

    :cond_6
    invoke-virtual {v1}, Lmaps/t/a;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lmaps/l/ap;

    invoke-virtual {v1}, Lmaps/t/a;->k()F

    move-result v1

    invoke-direct {v0, v1}, Lmaps/l/ap;-><init>(F)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto :goto_2

    :cond_7
    invoke-virtual {v1}, Lmaps/t/a;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto :goto_2

    :cond_8
    new-instance v0, Lmaps/l/g;

    invoke-virtual {p0}, Lmaps/t/bh;->c()Lmaps/t/au;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/au;->b()I

    move-result v1

    invoke-static {v1}, Lmaps/l/w;->a(I)Lmaps/l/w;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/t/bh;->c()Lmaps/t/au;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/au;->c()I

    move-result v2

    invoke-static {v2}, Lmaps/l/j;->a(I)Lmaps/l/j;

    move-result-object v2

    invoke-direct {v0, v9, v1, v2}, Lmaps/l/g;-><init>(Ljava/util/ArrayList;Lmaps/l/w;Lmaps/l/j;)V

    goto :goto_4

    :cond_9
    move-object v1, v7

    goto/16 :goto_2
.end method

.method private a(Lmaps/af/q;)Z
    .locals 4

    iget-object v0, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this.textureArray should be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/az;

    instance-of v3, v0, Lmaps/l/ap;

    if-nez v3, :cond_2

    invoke-interface {v0, p1}, Lmaps/l/az;->a(Lmaps/af/q;)Lmaps/cr/a;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_4
    iget-object v3, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_2
.end method

.method static a(Lmaps/p/y;Lmaps/cf/a;Ljava/lang/String;Lmaps/t/aa;Lmaps/bq/d;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lmaps/t/aa;->f()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p3}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/cv;->d()I

    move-result v1

    invoke-virtual {p3}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/t/cv;->f()I

    move-result v2

    if-lez v2, :cond_0

    const/high16 v2, -0x1000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lmaps/cr/c;)V
    .locals 19

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/g;->h:Lmaps/s/t;

    invoke-virtual {v1}, Lmaps/s/t;->h()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/g;->i:Lmaps/al/g;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/al/g;->a(Lmaps/cr/c;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/g;->h:Lmaps/s/t;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    invoke-virtual {v1, v2}, Lmaps/s/t;->d(I)V

    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lmaps/l/g;->k:F

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/l/g;->l:F

    sub-float v2, v1, v2

    const/4 v1, 0x0

    move v4, v2

    move v5, v3

    move v3, v1

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v3, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    const/4 v6, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v7, v6

    move v6, v2

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/l/az;

    invoke-interface {v2}, Lmaps/l/az;->e()F

    move-result v9

    invoke-static {v7, v9}, Ljava/lang/Math;->max(FF)F

    move-result v7

    invoke-interface {v2}, Lmaps/l/az;->a()F

    move-result v2

    add-float/2addr v2, v6

    move v6, v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/g;->a:Lmaps/l/w;

    sget-object v9, Lmaps/l/w;->a:Lmaps/l/w;

    if-ne v8, v9, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/l/g;->j:F

    sub-float/2addr v2, v6

    const/high16 v6, 0x40000000

    div-float/2addr v2, v6

    :cond_2
    :goto_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v6, v2

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/az;

    instance-of v2, v1, Lmaps/l/ap;

    if-eqz v2, :cond_4

    invoke-interface {v1}, Lmaps/l/az;->a()F

    move-result v1

    add-float v2, v6, v1

    move v6, v2

    goto :goto_3

    :cond_3
    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/l/g;->a:Lmaps/l/w;

    sget-object v9, Lmaps/l/w;->c:Lmaps/l/w;

    if-ne v8, v9, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/l/g;->j:F

    sub-float/2addr v2, v6

    goto :goto_2

    :cond_4
    invoke-interface {v1}, Lmaps/l/az;->a()F

    move-result v10

    invoke-interface {v1}, Lmaps/l/az;->b()F

    move-result v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/g;->b:Lmaps/l/j;

    sget-object v8, Lmaps/l/j;->a:Lmaps/l/j;

    if-ne v2, v8, :cond_5

    invoke-interface {v1}, Lmaps/l/az;->e()F

    move-result v2

    sub-float v2, v7, v2

    const/high16 v8, 0x40000000

    div-float/2addr v2, v8

    sub-float v2, v4, v2

    :goto_4
    invoke-interface {v1}, Lmaps/l/az;->c()F

    move-result v8

    add-float v12, v2, v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    add-int/lit8 v8, v5, 0x1

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/cr/a;

    invoke-virtual {v2}, Lmaps/cr/a;->b()F

    move-result v5

    invoke-virtual {v2}, Lmaps/cr/a;->c()F

    move-result v2

    sget-boolean v13, Lmaps/ae/h;->F:Z

    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    iget v13, v0, Lmaps/l/g;->j:F

    const/high16 v14, 0x3f000000

    mul-float/2addr v13, v14

    sub-float v13, v6, v13

    add-float/2addr v10, v13

    move-object/from16 v0, p0

    iget v14, v0, Lmaps/l/g;->k:F

    const/high16 v15, 0x3f000000

    mul-float/2addr v14, v15

    sub-float/2addr v12, v14

    sub-float v11, v12, v11

    move-object/from16 v0, p0

    iget-object v14, v0, Lmaps/l/g;->e:Ljava/util/ArrayList;

    new-instance v15, Lmaps/q/ao;

    const/16 v16, 0x14

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput v10, v16, v17

    const/16 v17, 0x1

    aput v12, v16, v17

    const/16 v17, 0x2

    const/16 v18, 0x0

    aput v18, v16, v17

    const/16 v17, 0x3

    aput v5, v16, v17

    const/16 v17, 0x4

    const/16 v18, 0x0

    aput v18, v16, v17

    const/16 v17, 0x5

    aput v13, v16, v17

    const/16 v17, 0x6

    aput v12, v16, v17

    const/4 v12, 0x7

    const/16 v17, 0x0

    aput v17, v16, v12

    const/16 v12, 0x8

    const/16 v17, 0x0

    aput v17, v16, v12

    const/16 v12, 0x9

    const/16 v17, 0x0

    aput v17, v16, v12

    const/16 v12, 0xa

    aput v10, v16, v12

    const/16 v10, 0xb

    aput v11, v16, v10

    const/16 v10, 0xc

    const/4 v12, 0x0

    aput v12, v16, v10

    const/16 v10, 0xd

    aput v5, v16, v10

    const/16 v5, 0xe

    aput v2, v16, v5

    const/16 v5, 0xf

    aput v13, v16, v5

    const/16 v5, 0x10

    aput v11, v16, v5

    const/16 v5, 0x11

    const/4 v10, 0x0

    aput v10, v16, v5

    const/16 v5, 0x12

    const/4 v10, 0x0

    aput v10, v16, v5

    const/16 v5, 0x13

    aput v2, v16, v5

    const/16 v2, 0x9

    const/4 v5, 0x5

    move-object/from16 v0, v16

    invoke-direct {v15, v0, v2, v5}, Lmaps/q/ao;-><init>([FII)V

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_5
    invoke-interface {v1}, Lmaps/l/az;->a()F

    move-result v1

    add-float v2, v6, v1

    move v6, v2

    move v5, v8

    goto/16 :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/g;->b:Lmaps/l/j;

    sget-object v8, Lmaps/l/j;->c:Lmaps/l/j;

    if-ne v2, v8, :cond_a

    invoke-interface {v1}, Lmaps/l/az;->e()F

    move-result v2

    sub-float v2, v7, v2

    sub-float v2, v4, v2

    goto/16 :goto_4

    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/l/g;->f:Lmaps/al/l;

    const/4 v14, 0x0

    sub-float v15, v12, v11

    invoke-interface {v13, v6, v14, v15}, Lmaps/al/l;->a(FFF)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/l/g;->f:Lmaps/al/l;

    add-float v14, v6, v10

    const/4 v15, 0x0

    sub-float v11, v12, v11

    invoke-interface {v13, v14, v15, v11}, Lmaps/al/l;->a(FFF)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lmaps/l/g;->f:Lmaps/al/l;

    add-float/2addr v10, v6

    const/4 v13, 0x0

    invoke-interface {v11, v10, v13, v12}, Lmaps/al/l;->a(FFF)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/l/g;->f:Lmaps/al/l;

    const/4 v11, 0x0

    invoke-interface {v10, v6, v11, v12}, Lmaps/al/l;->a(FFF)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/l/g;->g:Lmaps/al/c;

    const/4 v11, 0x0

    invoke-interface {v10, v11, v2}, Lmaps/al/c;->a(FF)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/l/g;->g:Lmaps/al/c;

    invoke-interface {v10, v5, v2}, Lmaps/al/c;->a(FF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/g;->g:Lmaps/al/c;

    const/4 v10, 0x0

    invoke-interface {v2, v5, v10}, Lmaps/al/c;->a(FF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/g;->g:Lmaps/al/c;

    const/4 v5, 0x0

    const/4 v10, 0x0

    invoke-interface {v2, v5, v10}, Lmaps/al/c;->a(FF)V

    goto :goto_5

    :cond_7
    sub-float v2, v4, v7

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v4, v2

    goto/16 :goto_0

    :cond_8
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/l/g;->o:Z

    sget-boolean v1, Lmaps/ae/h;->F:Z

    if-nez v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/g;->h:Lmaps/s/t;

    invoke-virtual {v1}, Lmaps/s/t;->d()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/l/g;->i:Lmaps/al/g;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/l/g;->h:Lmaps/s/t;

    invoke-virtual {v2}, Lmaps/s/t;->f()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/al/g;->a(Ljava/nio/ByteBuffer;)V

    :cond_9
    return-void

    :cond_a
    move v2, v4

    goto/16 :goto_4
.end method

.method private d()V
    .locals 10

    const/4 v2, 0x0

    const/high16 v9, 0x40000000

    const/4 v4, 0x0

    iput v4, p0, Lmaps/l/g;->j:F

    move v1, v2

    move v3, v4

    :goto_0
    iget-object v0, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v5, v4

    move v6, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/az;

    invoke-interface {v0}, Lmaps/l/az;->a()F

    move-result v8

    add-float/2addr v6, v8

    invoke-interface {v0}, Lmaps/l/az;->e()F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v5, v0

    goto :goto_1

    :cond_0
    iget v0, p0, Lmaps/l/g;->j:F

    invoke-static {v0, v6}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lmaps/l/g;->j:F

    add-float/2addr v3, v5

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput v4, p0, Lmaps/l/g;->l:F

    iput v4, p0, Lmaps/l/g;->m:F

    sget-object v2, Lmaps/l/p;->a:[I

    iget-object v5, p0, Lmaps/l/g;->b:Lmaps/l/j;

    invoke-virtual {v5}, Lmaps/l/j;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    :cond_2
    :goto_2
    iget v0, p0, Lmaps/l/g;->l:F

    add-float/2addr v0, v3

    iget v1, p0, Lmaps/l/g;->m:F

    add-float/2addr v0, v1

    iput v0, p0, Lmaps/l/g;->k:F

    return-void

    :pswitch_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/az;

    iget v5, p0, Lmaps/l/g;->l:F

    invoke-interface {v0}, Lmaps/l/az;->c()F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lmaps/l/g;->l:F

    goto :goto_3

    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v4

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/az;

    invoke-interface {v0}, Lmaps/l/az;->e()F

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-interface {v0}, Lmaps/l/az;->d()F

    move-result v0

    add-float/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_4

    :cond_4
    cmpl-float v0, v4, v1

    if-lez v0, :cond_2

    sub-float v0, v4, v1

    iput v0, p0, Lmaps/l/g;->m:F

    goto :goto_2

    :pswitch_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v4

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/az;

    invoke-interface {v0}, Lmaps/l/az;->e()F

    move-result v6

    invoke-static {v2, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-interface {v0}, Lmaps/l/az;->c()F

    move-result v0

    add-float/2addr v0, v6

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_5

    :cond_5
    cmpl-float v0, v4, v2

    if-lez v0, :cond_6

    sub-float v0, v4, v2

    iput v0, p0, Lmaps/l/g;->l:F

    :cond_6
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/az;

    iget v2, p0, Lmaps/l/g;->m:F

    invoke-interface {v0}, Lmaps/l/az;->d()F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lmaps/l/g;->m:F

    goto :goto_6

    :pswitch_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v4

    move v5, v4

    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/az;

    invoke-interface {v0}, Lmaps/l/az;->e()F

    move-result v7

    div-float/2addr v7, v9

    invoke-static {v5, v7}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-interface {v0}, Lmaps/l/az;->c()F

    move-result v0

    add-float/2addr v0, v7

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v2, v0

    goto :goto_7

    :cond_7
    cmpl-float v0, v2, v5

    if-lez v0, :cond_8

    sub-float v0, v2, v5

    iput v0, p0, Lmaps/l/g;->l:F

    :cond_8
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v4

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/az;

    invoke-interface {v0}, Lmaps/l/az;->e()F

    move-result v5

    div-float/2addr v5, v9

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-interface {v0}, Lmaps/l/az;->d()F

    move-result v0

    add-float/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_8

    :cond_9
    cmpl-float v0, v4, v1

    if-lez v0, :cond_2

    sub-float v0, v4, v1

    iput v0, p0, Lmaps/l/g;->m:F

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lmaps/l/g;->j:F

    return v0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/l/g;->c(Lmaps/cr/c;)V

    iget-object v0, p0, Lmaps/l/g;->i:Lmaps/al/g;

    invoke-virtual {v0, p1}, Lmaps/al/g;->c(Lmaps/cr/c;)V

    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 5

    iget-boolean v0, p0, Lmaps/l/g;->n:Z

    if-nez v0, :cond_1

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lmaps/l/g;->a(Lmaps/cr/c;Lmaps/af/q;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lmaps/l/g;->o:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lmaps/l/g;->b(Lmaps/cr/c;)V

    :cond_2
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    iget-object v0, p0, Lmaps/l/g;->i:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->a()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/g;->i:Lmaps/al/g;

    invoke-virtual {v0, p1}, Lmaps/al/g;->b(Lmaps/cr/c;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/a;

    invoke-virtual {v0, v2}, Lmaps/cr/a;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v0, 0x6

    mul-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    invoke-interface {v2, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public a(Lmaps/l/w;)V
    .locals 1

    iget-object v0, p0, Lmaps/l/g;->a:Lmaps/l/w;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/l/g;->o:Z

    :cond_0
    iput-object p1, p0, Lmaps/l/g;->a:Lmaps/l/w;

    return-void
.end method

.method a(Lmaps/cr/c;Lmaps/af/q;)Z
    .locals 5

    const/4 v1, 0x1

    invoke-direct {p0, p2}, Lmaps/l/g;->a(Lmaps/af/q;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-nez v0, :cond_0

    const/16 v0, 0x2710

    invoke-virtual {p1, v0}, Lmaps/cr/c;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this.textureArray should be empty on initialize."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/az;

    instance-of v4, v0, Lmaps/l/ap;

    if-nez v4, :cond_3

    iget-object v4, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-interface {v0, p1, p2}, Lmaps/l/az;->a(Lmaps/cr/c;Lmaps/af/q;)Lmaps/cr/a;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-direct {p0, p1}, Lmaps/l/g;->b(Lmaps/cr/c;)V

    iput-boolean v1, p0, Lmaps/l/g;->n:Z

    move v0, v1

    goto :goto_0
.end method

.method public b()F
    .locals 1

    iget v0, p0, Lmaps/l/g;->k:F

    return v0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 4

    iget-object v0, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/cr/a;

    invoke-virtual {v0}, Lmaps/cr/a;->g()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/l/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/l/az;

    invoke-interface {v1}, Lmaps/l/az;->f()V

    goto :goto_2

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/l/g;->i:Lmaps/al/g;

    invoke-virtual {v0, p1}, Lmaps/al/g;->d(Lmaps/cr/c;)V

    return-void
.end method

.method public c()Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/l/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/l/g;->a()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/l/g;->b()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
