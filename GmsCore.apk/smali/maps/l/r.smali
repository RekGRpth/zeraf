.class public Lmaps/l/r;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/l/al;


# instance fields
.field private final a:Lmaps/t/ax;

.field private final b:Lmaps/t/ah;

.field private final c:Lmaps/o/c;

.field private d:[Ljava/lang/String;

.field private e:[Ljava/lang/String;

.field private f:I

.field private g:Lmaps/l/m;

.field private final h:[F

.field private i:J

.field private j:Lmaps/l/u;


# direct methods
.method private constructor <init>(Lmaps/t/ah;Lmaps/o/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/l/r;->h:[F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/l/r;->i:J

    iput-object p1, p0, Lmaps/l/r;->b:Lmaps/t/ah;

    iput-object p2, p0, Lmaps/l/r;->c:Lmaps/o/c;

    invoke-virtual {p1}, Lmaps/t/ah;->j()Lmaps/t/ax;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/r;->a:Lmaps/t/ax;

    return-void
.end method

.method public static a(Lmaps/t/o;Lmaps/cr/c;)Lmaps/l/r;
    .locals 3

    new-instance v0, Lmaps/l/r;

    invoke-interface {p0}, Lmaps/t/o;->g()Lmaps/t/ah;

    move-result-object v1

    invoke-interface {p0}, Lmaps/t/o;->h()Lmaps/o/c;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/l/r;-><init>(Lmaps/t/ah;Lmaps/o/c;)V

    instance-of v1, p0, Lmaps/t/bz;

    if-eqz v1, :cond_0

    check-cast p0, Lmaps/t/bz;

    invoke-direct {v0, p0, p1}, Lmaps/l/r;->a(Lmaps/t/bz;Lmaps/cr/c;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {v0}, Lmaps/l/r;->k()V

    goto :goto_0
.end method

.method private a(Lmaps/t/bz;Lmaps/cr/c;)V
    .locals 2

    invoke-virtual {p1}, Lmaps/t/bz;->e()[B

    move-result-object v0

    iget-object v1, p0, Lmaps/l/r;->b:Lmaps/t/ah;

    invoke-static {v0, v1, p2}, Lmaps/l/m;->a([BLmaps/t/ah;Lmaps/cr/c;)Lmaps/l/m;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    invoke-virtual {p1}, Lmaps/t/bz;->b()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/r;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lmaps/t/bz;->c()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/r;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Lmaps/t/bz;->d()I

    move-result v0

    iput v0, p0, Lmaps/l/r;->f:I

    return-void
.end method

.method private k()V
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lmaps/l/r;->d:[Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lmaps/l/r;->e:[Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lmaps/l/r;->f:I

    return-void
.end method


# virtual methods
.method public a(Lmaps/bq/d;Lmaps/af/q;)I
    .locals 2

    const/4 v0, 0x0

    sget-boolean v1, Lmaps/af/v;->a:Z

    if-eqz v1, :cond_0

    const v0, 0x8000

    :cond_0
    iget-object v1, p0, Lmaps/l/r;->g:Lmaps/l/m;

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    return v0
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    invoke-virtual {v0, p1, p2}, Lmaps/l/m;->a(J)V

    :cond_0
    return-void
.end method

.method public a(Lmaps/ac/g;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/bq/d;ILjava/util/Collection;)V
    .locals 4

    iget-object v1, p0, Lmaps/l/r;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {p3, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lmaps/bq/d;Ljava/util/Collection;)V
    .locals 4

    iget-object v1, p0, Lmaps/l/r;->e:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {p2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lmaps/bq/d;Lmaps/cr/c;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    invoke-virtual {v0, p1}, Lmaps/l/m;->a(Lmaps/cr/c;)V

    :cond_0
    iget-object v0, p0, Lmaps/l/r;->j:Lmaps/l/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/r;->j:Lmaps/l/u;

    invoke-virtual {v0, p1}, Lmaps/l/u;->a(Lmaps/cr/c;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/l/r;->j:Lmaps/l/u;

    :cond_1
    return-void
.end method

.method public a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    if-nez v0, :cond_0

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-ne v0, v6, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lmaps/cr/c;->B()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p2}, Lmaps/bq/d;->g()J

    move-result-wide v2

    iget-wide v4, p0, Lmaps/l/r;->i:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lmaps/bq/d;->g()J

    move-result-wide v2

    iput-wide v2, p0, Lmaps/l/r;->i:J

    iget-object v0, p0, Lmaps/l/r;->a:Lmaps/t/ax;

    invoke-virtual {v0}, Lmaps/t/ax;->d()Lmaps/t/bx;

    move-result-object v0

    invoke-virtual {p2}, Lmaps/bq/d;->k()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p2}, Lmaps/bq/d;->r()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_1

    invoke-virtual {p2}, Lmaps/bq/d;->q()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_1

    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v2

    invoke-virtual {p2}, Lmaps/bq/d;->s()F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    iget-object v2, p1, Lmaps/cr/c;->k:[F

    invoke-virtual {p2, v0, v2}, Lmaps/bq/d;->a(Lmaps/t/bx;[F)V

    iget-object v0, p1, Lmaps/cr/c;->k:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p1, Lmaps/cr/c;->k:[F

    aget v2, v2, v6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2, v0, v2}, Lmaps/bq/d;->d(FF)Lmaps/t/bx;

    move-result-object v0

    :cond_1
    iget-object v2, p0, Lmaps/l/r;->a:Lmaps/t/ax;

    invoke-virtual {v2}, Lmaps/t/ax;->g()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lmaps/l/r;->h:[F

    invoke-static {p1, p2, v0, v2, v3}, Lmaps/af/d;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/t/bx;F[F)V

    :cond_2
    iget-object v0, p0, Lmaps/l/r;->h:[F

    invoke-static {v1, v0}, Lmaps/af/d;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    if-ne v0, v6, :cond_4

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/l/m;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0

    :cond_4
    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v0

    const/16 v2, 0xf

    if-ne v0, v2, :cond_3

    sget-object v0, Lmaps/l/aa;->a:Lmaps/l/aa;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/l/aa;->a(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V

    goto :goto_1
.end method

.method public a(Lmaps/l/u;)V
    .locals 0

    iput-object p1, p0, Lmaps/l/r;->j:Lmaps/l/u;

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lmaps/ae/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Lmaps/p/ac;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lmaps/t/ah;
    .locals 1

    iget-object v0, p0, Lmaps/l/r;->b:Lmaps/t/ah;

    return-object v0
.end method

.method public b(Lmaps/cr/c;Lmaps/bq/d;Lmaps/af/s;)V
    .locals 3

    invoke-interface {p3}, Lmaps/af/s;->a()Lmaps/af/q;

    move-result-object v0

    invoke-interface {p3}, Lmaps/af/s;->b()I

    move-result v1

    sget-boolean v2, Lmaps/ae/h;->q:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    :cond_0
    invoke-static {p1, v0}, Lmaps/l/m;->a(Lmaps/cr/c;Lmaps/af/q;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/16 v0, 0xf

    if-ne v1, v0, :cond_1

    invoke-static {p1, p3}, Lmaps/l/aa;->a(Lmaps/cr/c;Lmaps/af/s;)V

    goto :goto_0
.end method

.method public b(Lmaps/ae/d;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 1

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    invoke-virtual {v0, p1}, Lmaps/l/m;->c(Lmaps/cr/c;)V

    :cond_0
    iget-object v0, p0, Lmaps/l/r;->j:Lmaps/l/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/l/r;->j:Lmaps/l/u;

    invoke-virtual {v0, p1}, Lmaps/l/u;->c(Lmaps/cr/c;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/l/r;->j:Lmaps/l/u;

    :cond_1
    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->d()V

    :cond_0
    return-void
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lmaps/l/r;->f:I

    return v0
.end method

.method public f()I
    .locals 2

    const/16 v0, 0x88

    iget-object v1, p0, Lmaps/l/r;->g:Lmaps/l/m;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/l/r;->g:Lmaps/l/m;

    invoke-virtual {v1}, Lmaps/l/m;->b()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public g()I
    .locals 1

    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/l/r;->g:Lmaps/l/m;

    invoke-virtual {v0}, Lmaps/l/m;->a()I

    move-result v0

    goto :goto_0
.end method

.method public h()Lmaps/o/c;
    .locals 1

    iget-object v0, p0, Lmaps/l/r;->c:Lmaps/o/c;

    return-object v0
.end method

.method public i()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public j()Lmaps/l/u;
    .locals 1

    iget-object v0, p0, Lmaps/l/r;->j:Lmaps/l/u;

    return-object v0
.end method
