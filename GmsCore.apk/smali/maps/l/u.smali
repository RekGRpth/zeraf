.class public abstract Lmaps/l/u;
.super Lmaps/y/bc;


# instance fields
.field protected final a:Lmaps/t/ca;

.field protected final b:Lmaps/t/aa;

.field protected final c:Lmaps/ac/g;

.field protected final d:F

.field protected final e:F

.field protected final f:I

.field protected final g:Z

.field protected h:Z

.field protected i:Z

.field protected j:Ljava/util/List;

.field protected k:I

.field private l:Z

.field private m:Z


# direct methods
.method protected constructor <init>(Lmaps/t/ca;Lmaps/ac/g;Lmaps/t/aa;FFIZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lmaps/y/bc;-><init>()V

    iput-boolean v0, p0, Lmaps/l/u;->m:Z

    iput-boolean v0, p0, Lmaps/l/u;->i:Z

    invoke-static {}, Lmaps/f/fd;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/u;->j:Ljava/util/List;

    const/high16 v0, 0x10000

    iput v0, p0, Lmaps/l/u;->k:I

    iput-object p1, p0, Lmaps/l/u;->a:Lmaps/t/ca;

    iput-object p3, p0, Lmaps/l/u;->b:Lmaps/t/aa;

    iput-object p2, p0, Lmaps/l/u;->c:Lmaps/ac/g;

    iput p4, p0, Lmaps/l/u;->d:F

    iput p5, p0, Lmaps/l/u;->e:F

    iput p6, p0, Lmaps/l/u;->f:I

    iput-boolean p7, p0, Lmaps/l/u;->g:Z

    iput-boolean p8, p0, Lmaps/l/u;->m:Z

    return-void
.end method

.method public static a(Lmaps/t/aa;FIIF)F
    .locals 3

    invoke-virtual {p0}, Lmaps/t/aa;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cv;->f()I

    move-result v0

    :goto_0
    int-to-float v0, v0

    mul-float/2addr v0, p1

    int-to-float v1, p2

    int-to-float v2, p3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, p4

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(F)I
    .locals 6

    const/high16 v4, 0x3e800000

    const/high16 v0, 0x3f800000

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    cmpl-float v0, p0, v4

    if-ltz v0, :cond_0

    const-wide/high16 v0, 0x40f0000000000000L

    const-wide v2, 0x3ff5555560000000L

    sub-float v4, p0, v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x10000

    goto :goto_0
.end method

.method private static a(II)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x18

    mul-int/2addr v0, p1

    div-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public static a(Lmaps/t/aa;Lmaps/af/q;)I
    .locals 3

    const/high16 v0, -0x1000000

    sget-object v1, Lmaps/l/ac;->a:[I

    invoke-virtual {p1}, Lmaps/af/q;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-virtual {p0}, Lmaps/t/aa;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/t/cv;->d()I

    move-result v1

    :goto_0
    if-nez v1, :cond_1

    :goto_1
    return v0

    :pswitch_0
    const/4 v0, -0x1

    goto :goto_1

    :pswitch_1
    const v0, -0x3f3f40

    goto :goto_1

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static b(I)I
    .locals 3

    const/16 v0, 0xa0

    invoke-static {p0, v0}, Lmaps/l/u;->a(II)I

    move-result v0

    invoke-static {p0}, Lmaps/l/u;->d(I)I

    move-result v1

    const/16 v2, 0xc0

    if-lt v1, v2, :cond_0

    const v1, 0x808080

    or-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const v1, 0xffffff

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method public static b(Lmaps/t/aa;Lmaps/af/q;)I
    .locals 2

    sget-object v0, Lmaps/l/ac;->a:[I

    invoke-virtual {p1}, Lmaps/af/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lmaps/t/aa;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/t/aa;->j()Lmaps/t/cv;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/cv;->e()I

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const/high16 v0, -0x60000000

    goto :goto_0

    :pswitch_1
    const/high16 v0, -0x80000000

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lmaps/l/u;->a(Lmaps/t/aa;Lmaps/af/q;)I

    move-result v0

    invoke-static {v0}, Lmaps/l/u;->b(I)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static c(I)I
    .locals 3

    const/16 v0, 0xff

    invoke-static {p0, v0}, Lmaps/l/u;->a(II)I

    move-result v0

    invoke-static {p0}, Lmaps/l/u;->d(I)I

    move-result v1

    const/16 v2, 0xc0

    if-lt v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const v1, 0xffffff

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method private static d(I)I
    .locals 2

    ushr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    mul-int/lit8 v0, v0, 0x4d

    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    mul-int/lit16 v1, v1, 0x97

    add-int/2addr v0, v1

    and-int/lit16 v1, p0, 0xff

    mul-int/lit8 v1, v1, 0x1c

    add-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lmaps/l/u;->i:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/cr/c;)V
    .locals 1

    invoke-super {p0, p1}, Lmaps/y/bc;->a(Lmaps/cr/c;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/l/u;->l:Z

    return-void
.end method

.method public a(Lmaps/t/bf;)Z
    .locals 2

    invoke-virtual {p1}, Lmaps/t/bf;->a()Lmaps/t/bw;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/l/u;->n()Lmaps/t/ax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/t/bw;->a(Lmaps/t/an;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/l/u;->m()Lmaps/t/an;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmaps/t/bf;->a(Lmaps/t/an;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/l/u;->h:Z

    return-void
.end method

.method public b()Lmaps/y/am;
    .locals 1

    sget-object v0, Lmaps/y/am;->l:Lmaps/y/am;

    return-object v0
.end method

.method public b(Lmaps/bq/d;Lmaps/cr/c;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Lmaps/cr/c;)V
    .locals 3

    invoke-super {p0, p1}, Lmaps/y/bc;->c(Lmaps/cr/c;)V

    sget-boolean v0, Lmaps/ae/h;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/l/u;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/q/ba;

    invoke-virtual {p1}, Lmaps/cr/c;->m()Lmaps/q/ad;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmaps/q/ad;->b(Lmaps/q/ba;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected k()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/l/u;->m:Z

    return v0
.end method

.method public abstract l()F
.end method

.method public abstract m()Lmaps/t/an;
.end method

.method public n()Lmaps/t/ax;
    .locals 1

    invoke-virtual {p0}, Lmaps/l/u;->m()Lmaps/t/an;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/t/an;->a()Lmaps/t/ax;

    move-result-object v0

    return-object v0
.end method

.method public o()Lmaps/ac/g;
    .locals 1

    iget-object v0, p0, Lmaps/l/u;->c:Lmaps/ac/g;

    return-object v0
.end method

.method public p()F
    .locals 1

    iget v0, p0, Lmaps/l/u;->d:F

    return v0
.end method

.method public q()F
    .locals 1

    iget v0, p0, Lmaps/l/u;->e:F

    return v0
.end method

.method public r()I
    .locals 1

    iget v0, p0, Lmaps/l/u;->f:I

    return v0
.end method

.method public abstract s()Ljava/lang/String;
.end method

.method public final t()Lmaps/t/ca;
    .locals 1

    iget-object v0, p0, Lmaps/l/u;->a:Lmaps/t/ca;

    return-object v0
.end method

.method public u()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/l/u;->g:Z

    return v0
.end method
