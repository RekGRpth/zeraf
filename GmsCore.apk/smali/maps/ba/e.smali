.class public Lmaps/ba/e;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ba/c;


# instance fields
.field private a:I

.field private b:[F

.field private c:[F

.field private d:[F

.field private e:I

.field private f:[F

.field private g:J

.field private h:F

.field private i:Lmaps/ba/d;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x32

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lmaps/ba/e;->a:I

    new-array v0, v1, [F

    iput-object v0, p0, Lmaps/ba/e;->b:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lmaps/ba/e;->c:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lmaps/ba/e;->d:[F

    iput v2, p0, Lmaps/ba/e;->e:I

    const/16 v0, 0xa

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/ba/e;->f:[F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/ba/e;->g:J

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ba/e;->h:F

    return-void
.end method


# virtual methods
.method public a(JFFF)V
    .locals 9

    const/high16 v8, 0x40800000

    const/16 v7, 0x32

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [F

    aput p3, v0, v4

    aput p4, v0, v5

    aput p5, v0, v6

    iget v1, p0, Lmaps/ba/e;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/ba/e;->a:I

    iget-object v1, p0, Lmaps/ba/e;->b:[F

    iget v2, p0, Lmaps/ba/e;->a:I

    rem-int/lit8 v2, v2, 0x32

    aget v3, v0, v4

    aput v3, v1, v2

    iget-object v1, p0, Lmaps/ba/e;->c:[F

    iget v2, p0, Lmaps/ba/e;->a:I

    rem-int/lit8 v2, v2, 0x32

    aget v3, v0, v5

    aput v3, v1, v2

    iget-object v1, p0, Lmaps/ba/e;->d:[F

    iget v2, p0, Lmaps/ba/e;->a:I

    rem-int/lit8 v2, v2, 0x32

    aget v3, v0, v6

    aput v3, v1, v2

    const/4 v1, 0x3

    new-array v1, v1, [F

    iget-object v2, p0, Lmaps/ba/e;->b:[F

    invoke-static {v2}, Lmaps/ba/g;->a([F)F

    move-result v2

    iget v3, p0, Lmaps/ba/e;->a:I

    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    aput v2, v1, v4

    iget-object v2, p0, Lmaps/ba/e;->c:[F

    invoke-static {v2}, Lmaps/ba/g;->a([F)F

    move-result v2

    iget v3, p0, Lmaps/ba/e;->a:I

    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    aput v2, v1, v5

    iget-object v2, p0, Lmaps/ba/e;->d:[F

    invoke-static {v2}, Lmaps/ba/g;->a([F)F

    move-result v2

    iget v3, p0, Lmaps/ba/e;->a:I

    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    aput v2, v1, v6

    invoke-static {v1}, Lmaps/ba/g;->b([F)F

    move-result v2

    aget v3, v1, v4

    div-float/2addr v3, v2

    aput v3, v1, v4

    aget v3, v1, v5

    div-float/2addr v3, v2

    aput v3, v1, v5

    aget v3, v1, v6

    div-float/2addr v3, v2

    aput v3, v1, v6

    invoke-static {v1, v0}, Lmaps/ba/g;->a([F[F)F

    move-result v0

    sub-float/2addr v0, v2

    iget v1, p0, Lmaps/ba/e;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/ba/e;->e:I

    iget-object v1, p0, Lmaps/ba/e;->f:[F

    iget v2, p0, Lmaps/ba/e;->e:I

    rem-int/lit8 v2, v2, 0xa

    aput v0, v1, v2

    iget-object v0, p0, Lmaps/ba/e;->f:[F

    invoke-static {v0}, Lmaps/ba/g;->a([F)F

    move-result v0

    cmpl-float v1, v0, v8

    if-lez v1, :cond_0

    iget v1, p0, Lmaps/ba/e;->h:F

    cmpg-float v1, v1, v8

    if-gtz v1, :cond_0

    iget-wide v1, p0, Lmaps/ba/e;->g:J

    sub-long v1, p1, v1

    const-wide/32 v3, 0xee6b280

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    iget-object v1, p0, Lmaps/ba/e;->i:Lmaps/ba/d;

    invoke-interface {v1, p1, p2}, Lmaps/ba/d;->a(J)V

    iput-wide p1, p0, Lmaps/ba/e;->g:J

    :cond_0
    iput v0, p0, Lmaps/ba/e;->h:F

    return-void
.end method

.method public a(Lmaps/ba/d;)V
    .locals 0

    iput-object p1, p0, Lmaps/ba/e;->i:Lmaps/ba/d;

    return-void
.end method
