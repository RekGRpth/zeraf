.class public Lmaps/bm/e;
.super Ljava/lang/Object;


# static fields
.field private static final a:Landroid/content/IntentFilter;

.field private static final b:Landroid/content/IntentFilter;

.field private static final c:Landroid/content/BroadcastReceiver;

.field private static volatile e:Landroid/net/NetworkInfo;

.field private static f:Lmaps/bm/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/bm/e;->a:Landroid/content/IntentFilter;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/bm/e;->b:Landroid/content/IntentFilter;

    new-instance v0, Lmaps/bm/c;

    invoke-direct {v0}, Lmaps/bm/c;-><init>()V

    sput-object v0, Lmaps/bm/e;->c:Landroid/content/BroadcastReceiver;

    sput-object v2, Lmaps/bm/e;->e:Landroid/net/NetworkInfo;

    sput-object v2, Lmaps/bm/e;->f:Lmaps/bm/d;

    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 1

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    sput-object v0, Lmaps/bm/e;->e:Landroid/net/NetworkInfo;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    sput-object v0, Lmaps/bm/e;->e:Landroid/net/NetworkInfo;

    goto :goto_0
.end method

.method public static e()Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lmaps/bm/e;->f:Lmaps/bm/d;

    if-eqz v1, :cond_1

    sget-object v0, Lmaps/bm/e;->f:Lmaps/bm/d;

    iget-boolean v0, v0, Lmaps/bm/d;->b:Z

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lmaps/bm/e;->e:Landroid/net/NetworkInfo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    goto :goto_0
.end method

.method public static f()Z
    .locals 1

    invoke-static {}, Lmaps/ae/h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/bm/e;->f:Lmaps/bm/d;

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/bm/e;->f:Lmaps/bm/d;

    iget-boolean v0, v0, Lmaps/bm/d;->a:Z

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lmaps/bm/e;->e:Landroid/net/NetworkInfo;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    goto :goto_0
.end method
