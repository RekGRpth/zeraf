.class Lmaps/v/ak;
.super Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field final a:Lmaps/v/r;

.field volatile b:I

.field c:I

.field d:I

.field e:I

.field volatile f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field final g:J

.field final h:Ljava/lang/ref/ReferenceQueue;

.field final i:Ljava/lang/ref/ReferenceQueue;

.field final j:Ljava/util/Queue;

.field final k:Ljava/util/concurrent/atomic/AtomicInteger;

.field final l:Ljava/util/Queue;

.field final m:Ljava/util/Queue;

.field final n:Lmaps/v/ab;


# direct methods
.method constructor <init>(Lmaps/v/r;IJLmaps/v/ab;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lmaps/v/ak;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iput-wide p3, p0, Lmaps/v/ak;->g:J

    iput-object p5, p0, Lmaps/v/ak;->n:Lmaps/v/ab;

    invoke-virtual {p0, p2}, Lmaps/v/ak;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/v/ak;->a(Ljava/util/concurrent/atomic/AtomicReferenceArray;)V

    invoke-virtual {p1}, Lmaps/v/r;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :goto_0
    iput-object v0, p0, Lmaps/v/ak;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {p1}, Lmaps/v/r;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :cond_0
    iput-object v1, p0, Lmaps/v/ak;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {p1}, Lmaps/v/r;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    :goto_1
    iput-object v0, p0, Lmaps/v/ak;->j:Ljava/util/Queue;

    invoke-virtual {p1}, Lmaps/v/r;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lmaps/v/j;

    invoke-direct {v0}, Lmaps/v/j;-><init>()V

    :goto_2
    iput-object v0, p0, Lmaps/v/ak;->l:Ljava/util/Queue;

    invoke-virtual {p1}, Lmaps/v/r;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lmaps/v/g;

    invoke-direct {v0}, Lmaps/v/g;-><init>()V

    :goto_3
    iput-object v0, p0, Lmaps/v/ak;->m:Ljava/util/Queue;

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Lmaps/v/r;->q()Ljava/util/Queue;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-static {}, Lmaps/v/r;->q()Ljava/util/Queue;

    move-result-object v0

    goto :goto_2

    :cond_4
    invoke-static {}, Lmaps/v/r;->q()Ljava/util/Queue;

    move-result-object v0

    goto :goto_3
.end method


# virtual methods
.method a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 12

    const/4 v7, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, Lmaps/v/ak;->c(J)V

    iget-object v10, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v11, p2, v0

    invoke-virtual {v10, v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/v/t;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/v/t;->i()I

    move-result v0

    if-ne v0, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->g:Lmaps/ap/a;

    invoke-virtual {v0, p1, v3}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v5

    invoke-interface {v5}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-interface {v5}, Lmaps/v/av;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v0, v0, -0x1

    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    sget-object v6, Lmaps/v/bu;->c:Lmaps/v/bu;

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/t;Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)Lmaps/v/t;

    move-result-object v0

    iget v1, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v10, v11, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/v/ak;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move-object v0, v7

    :goto_1
    return-object v0

    :cond_1
    :try_start_1
    iget v1, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/v/ak;->d:I

    sget-object v1, Lmaps/v/bu;->b:Lmaps/v/bu;

    invoke-virtual {p0, p1, p2, v5, v1}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-wide v5, v8

    invoke-virtual/range {v1 .. v6}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;Ljava/lang/Object;J)V

    invoke-virtual {p0}, Lmaps/v/ak;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-interface {v2}, Lmaps/v/t;->j()Lmaps/v/t;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move-object v0, v7

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    throw v0
.end method

.method a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 9

    const/4 v6, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lmaps/v/ak;->c(J)V

    iget v0, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lmaps/v/ak;->e:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->k()V

    iget v0, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v0, v0, 0x1

    :cond_0
    iget-object v7, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_5

    invoke-interface {v1}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Lmaps/v/t;->i()I

    move-result v3

    if-ne v3, p2, :cond_4

    if-eqz v2, :cond_4

    iget-object v3, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v3, v3, Lmaps/v/r;->g:Lmaps/ap/a;

    invoke-virtual {v3, p1, v2}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v0

    invoke-interface {v0}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_2

    iget v2, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmaps/v/ak;->d:I

    invoke-interface {v0}, Lmaps/v/av;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lmaps/v/bu;->c:Lmaps/v/bu;

    invoke-virtual {p0, p1, p2, v0, v2}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;Ljava/lang/Object;J)V

    iget v0, p0, Lmaps/v/ak;->b:I

    :goto_1
    iput v0, p0, Lmaps/v/ak;->b:I

    invoke-virtual {p0}, Lmaps/v/ak;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move-object v0, v6

    :goto_2
    return-object v0

    :cond_1
    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    :try_start_1
    invoke-virtual/range {v0 .. v5}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;Ljava/lang/Object;J)V

    iget v0, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual {p0, v1, v4, v5}, Lmaps/v/ak;->b(Lmaps/v/t;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move-object v0, v7

    goto :goto_2

    :cond_3
    :try_start_2
    iget v2, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmaps/v/ak;->d:I

    sget-object v2, Lmaps/v/bu;->b:Lmaps/v/bu;

    invoke-virtual {p0, p1, p2, v0, v2}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;Ljava/lang/Object;J)V

    invoke-virtual {p0}, Lmaps/v/ak;->i()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move-object v0, v7

    goto :goto_2

    :cond_4
    :try_start_3
    invoke-interface {v1}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v1

    goto :goto_0

    :cond_5
    iget v1, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/v/ak;->d:I

    invoke-virtual {p0, p1, p2, v0}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/t;)Lmaps/v/t;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;Ljava/lang/Object;J)V

    invoke-virtual {v7, v8, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iget v0, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->b:I

    invoke-virtual {p0}, Lmaps/v/ak;->i()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move-object v0, v6

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    throw v0
.end method

.method a(Ljava/lang/Object;ILmaps/v/af;)Ljava/lang/Object;
    .locals 8

    :try_start_0
    iget v0, p0, Lmaps/v/ak;->b:I

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lmaps/v/ak;->b(Ljava/lang/Object;I)Lmaps/v/t;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v5

    invoke-virtual {p0, v1, v5, v6}, Lmaps/v/ak;->c(Lmaps/v/t;J)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v1, v5, v6}, Lmaps/v/ak;->a(Lmaps/v/t;J)V

    iget-object v0, p0, Lmaps/v/ak;->n:Lmaps/v/ab;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lmaps/v/ab;->a(I)V

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;ILjava/lang/Object;JLmaps/v/af;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v0

    invoke-interface {v0}, Lmaps/v/av;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v1, p1, v0}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;Lmaps/v/av;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-virtual {p0, p1, p2, p3}, Lmaps/v/ak;->b(Ljava/lang/Object;ILmaps/v/af;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_3
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v2, v0, Ljava/lang/Error;

    if-eqz v2, :cond_2

    new-instance v1, Lmaps/ab/l;

    check-cast v0, Ljava/lang/Error;

    invoke-direct {v1, v0}, Lmaps/ab/l;-><init>(Ljava/lang/Error;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    throw v0

    :cond_2
    :try_start_4
    instance-of v2, v0, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_3

    new-instance v1, Lmaps/ab/g;

    invoke-direct {v1, v0}, Lmaps/ab/g;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method a(Ljava/lang/Object;ILmaps/v/h;Lmaps/ab/c;)Ljava/lang/Object;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p4}, Lmaps/ab/d;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Lmaps/v/u;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CacheLoader returned null for key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lmaps/v/u;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    if-nez v1, :cond_0

    iget-object v1, p0, Lmaps/v/ak;->n:Lmaps/v/ab;

    invoke-virtual {p3}, Lmaps/v/h;->d()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lmaps/v/ab;->b(J)V

    invoke-virtual {p0, p1, p2, p3}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/h;)Z

    :cond_0
    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/v/ak;->n:Lmaps/v/ab;

    invoke-virtual {p3}, Lmaps/v/h;->d()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lmaps/v/ab;->a(J)V

    invoke-virtual {p0, p1, p2, p3, v1}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/h;Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_2

    iget-object v0, p0, Lmaps/v/ak;->n:Lmaps/v/ab;

    invoke-virtual {p3}, Lmaps/v/h;->d()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lmaps/v/ab;->b(J)V

    invoke-virtual {p0, p1, p2, p3}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/h;)Z

    :cond_2
    return-object v1
.end method

.method a(Ljava/lang/Object;ILmaps/v/h;Lmaps/v/af;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p3, p1, p4}, Lmaps/v/h;->a(Ljava/lang/Object;Lmaps/v/af;)Lmaps/ab/c;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/h;Lmaps/ab/c;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method a(Lmaps/v/t;Ljava/lang/Object;ILjava/lang/Object;JLmaps/v/af;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lmaps/v/t;->e()J

    move-result-wide v0

    sub-long v0, p5, v0

    iget-object v2, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-wide v2, v2, Lmaps/v/r;->o:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0, p2, p3, p7}, Lmaps/v/ak;->c(Ljava/lang/Object;ILmaps/v/af;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object p4, v0

    :cond_0
    return-object p4
.end method

.method a(Lmaps/v/t;Ljava/lang/Object;Lmaps/v/av;)Ljava/lang/Object;
    .locals 4

    const/4 v1, 0x1

    invoke-interface {p3}, Lmaps/v/av;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "Recursive load"

    invoke-static {v0, v2}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    :try_start_0
    invoke-interface {p3}, Lmaps/v/av;->e()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lmaps/v/u;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CacheLoader returned null for key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lmaps/v/u;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v2, p0, Lmaps/v/ak;->n:Lmaps/v/ab;

    invoke-interface {v2, v1}, Lmaps/v/ab;->b(I)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v2, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v2, v2, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v2}, Lmaps/ap/c;->a()J

    move-result-wide v2

    invoke-virtual {p0, p1, v2, v3}, Lmaps/v/ak;->a(Lmaps/v/t;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v2, p0, Lmaps/v/ak;->n:Lmaps/v/ab;

    invoke-interface {v2, v1}, Lmaps/v/ab;->b(I)V

    return-object v0
.end method

.method a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    return-object v0
.end method

.method a(Ljava/lang/Object;I)Lmaps/v/h;
    .locals 6

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lmaps/v/ak;->c(J)V

    iget-object v2, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {v1}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Lmaps/v/t;->i()I

    move-result v5

    if-ne v5, p2, :cond_1

    if-eqz v4, :cond_1

    iget-object v5, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v5, v5, Lmaps/v/r;->g:Lmaps/ap/a;

    invoke-virtual {v5, p1, v4}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v2

    invoke-interface {v2}, Lmaps/v/av;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    :goto_1
    return-object v0

    :cond_0
    :try_start_1
    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    new-instance v0, Lmaps/v/h;

    invoke-direct {v0, v2}, Lmaps/v/h;-><init>(Lmaps/v/av;)V

    invoke-interface {v1, v0}, Lmaps/v/t;->a(Lmaps/v/av;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_1

    :cond_1
    :try_start_2
    invoke-interface {v1}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v1

    goto :goto_0

    :cond_2
    iget v1, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/v/ak;->d:I

    new-instance v1, Lmaps/v/h;

    invoke-direct {v1}, Lmaps/v/h;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/t;)Lmaps/v/t;

    move-result-object v0

    invoke-interface {v0, v1}, Lmaps/v/t;->a(Lmaps/v/av;)V

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    throw v0
.end method

.method a(Ljava/lang/Object;IJ)Lmaps/v/t;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, Lmaps/v/ak;->b(Ljava/lang/Object;I)Lmaps/v/t;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v2, v1, p3, p4}, Lmaps/v/r;->b(Lmaps/v/t;J)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, p3, p4}, Lmaps/v/ak;->a(J)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method a(Ljava/lang/Object;ILmaps/v/t;)Lmaps/v/t;
    .locals 1

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->s:Lmaps/v/bb;

    invoke-virtual {v0, p0, p1, p2, p3}, Lmaps/v/bb;->a(Lmaps/v/ak;Ljava/lang/Object;ILmaps/v/t;)Lmaps/v/t;

    move-result-object v0

    return-object v0
.end method

.method a(Lmaps/v/t;Lmaps/v/t;)Lmaps/v/t;
    .locals 3

    invoke-interface {p1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v0

    iget-object v1, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v1, v1, Lmaps/v/r;->s:Lmaps/v/bb;

    invoke-virtual {v1, p0, p1, p2}, Lmaps/v/bb;->a(Lmaps/v/ak;Lmaps/v/t;Lmaps/v/t;)Lmaps/v/t;

    move-result-object v1

    iget-object v2, p0, Lmaps/v/ak;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v0, v2, v1}, Lmaps/v/av;->a(Ljava/lang/ref/ReferenceQueue;Lmaps/v/t;)Lmaps/v/av;

    move-result-object v0

    invoke-interface {v1, v0}, Lmaps/v/t;->a(Lmaps/v/av;)V

    return-object v1
.end method

.method a(Lmaps/v/t;Lmaps/v/t;Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)Lmaps/v/t;
    .locals 1

    invoke-virtual {p0, p3, p4, p5, p6}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V

    iget-object v0, p0, Lmaps/v/ak;->l:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/v/ak;->m:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    invoke-interface {p5}, Lmaps/v/av;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p5, v0}, Lmaps/v/av;->b(Ljava/lang/Object;)V

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0, p1, p2}, Lmaps/v/ak;->b(Lmaps/v/t;Lmaps/v/t;)Lmaps/v/t;

    move-result-object p1

    goto :goto_0
.end method

.method a()V
    .locals 1

    invoke-virtual {p0}, Lmaps/v/ak;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lmaps/v/ak;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    throw v0
.end method

.method a(J)V
    .locals 1

    invoke-virtual {p0}, Lmaps/v/ak;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lmaps/v/ak;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    throw v0
.end method

.method a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V
    .locals 2

    iget v0, p0, Lmaps/v/ak;->c:I

    invoke-interface {p3}, Lmaps/v/av;->c()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lmaps/v/ak;->c:I

    invoke-virtual {p4}, Lmaps/v/bu;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/v/ak;->n:Lmaps/v/ab;

    invoke-interface {v0}, Lmaps/v/ab;->a()V

    :cond_0
    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->p:Ljava/util/Queue;

    sget-object v1, Lmaps/v/r;->w:Ljava/util/Queue;

    if-eq v0, v1, :cond_1

    invoke-interface {p3}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v0

    new-instance v1, Lmaps/v/bg;

    invoke-direct {v1, p1, v0, p4}, Lmaps/v/bg;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lmaps/v/bu;)V

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->p:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method a(Ljava/util/concurrent/atomic/AtomicReferenceArray;)V
    .locals 4

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/v/ak;->e:I

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/v/ak;->e:I

    int-to-long v0, v0

    iget-wide v2, p0, Lmaps/v/ak;->g:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/v/ak;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->e:I

    :cond_0
    iput-object p1, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    return-void
.end method

.method a(Lmaps/v/t;)V
    .locals 1

    sget-object v0, Lmaps/v/bu;->c:Lmaps/v/bu;

    invoke-virtual {p0, p1, v0}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/bu;)V

    iget-object v0, p0, Lmaps/v/ak;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/v/ak;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method a(Lmaps/v/t;IJ)V
    .locals 1

    invoke-virtual {p0}, Lmaps/v/ak;->h()V

    iget v0, p0, Lmaps/v/ak;->c:I

    add-int/2addr v0, p2

    iput v0, p0, Lmaps/v/ak;->c:I

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p3, p4}, Lmaps/v/t;->a(J)V

    :cond_0
    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1, p3, p4}, Lmaps/v/t;->b(J)V

    :cond_1
    iget-object v0, p0, Lmaps/v/ak;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/v/ak;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method a(Lmaps/v/t;J)V
    .locals 1

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p2, p3}, Lmaps/v/t;->a(J)V

    :cond_0
    iget-object v0, p0, Lmaps/v/ak;->j:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method a(Lmaps/v/t;Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 4

    invoke-interface {p1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v1

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->l:Lmaps/v/ay;

    invoke-interface {v0, p2, p3}, Lmaps/v/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-ltz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Weights must be non-negative"

    invoke-static {v0, v3}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->j:Lmaps/v/ac;

    invoke-virtual {v0, p0, p1, p3, v2}, Lmaps/v/ac;->a(Lmaps/v/ak;Lmaps/v/t;Ljava/lang/Object;I)Lmaps/v/av;

    move-result-object v0

    invoke-interface {p1, v0}, Lmaps/v/t;->a(Lmaps/v/av;)V

    invoke-virtual {p0, p1, v2, p4, p5}, Lmaps/v/ak;->a(Lmaps/v/t;IJ)V

    invoke-interface {v1, p3}, Lmaps/v/av;->b(Ljava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lmaps/v/t;Lmaps/v/bu;)V
    .locals 3

    invoke-interface {p1}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Lmaps/v/t;->i()I

    move-result v1

    invoke-interface {p1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V

    return-void
.end method

.method a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 11

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v7

    invoke-virtual {p0, v7, v8}, Lmaps/v/ak;->c(J)V

    iget-object v9, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v10, p2, v0

    invoke-virtual {v9, v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/v/t;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_4

    invoke-interface {v2}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/v/t;->i()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->g:Lmaps/ap/a;

    invoke-virtual {v0, p1, v3}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v5

    invoke-interface {v5}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-interface {v5}, Lmaps/v/av;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v0, v0, -0x1

    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    sget-object v6, Lmaps/v/bu;->c:Lmaps/v/bu;

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/t;Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)Lmaps/v/t;

    move-result-object v0

    iget v1, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v9, v10, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/v/ak;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    :goto_1
    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v1, v1, Lmaps/v/r;->h:Lmaps/ap/a;

    invoke-virtual {v1, p3, v0}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    sget-object v0, Lmaps/v/bu;->b:Lmaps/v/bu;

    invoke-virtual {p0, p1, p2, v5, v0}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V

    move-object v1, p0

    move-object v3, p1

    move-object v4, p4

    move-wide v5, v7

    invoke-virtual/range {v1 .. v6}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;Ljava/lang/Object;J)V

    invoke-virtual {p0}, Lmaps/v/ak;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-virtual {p0, v2, v7, v8}, Lmaps/v/ak;->b(Lmaps/v/t;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_1

    :cond_3
    :try_start_3
    invoke-interface {v2}, Lmaps/v/t;->j()Lmaps/v/t;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    throw v0
.end method

.method a(Ljava/lang/Object;ILmaps/v/av;)Z
    .locals 9

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget v1, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v1, v1, -0x1

    iget-object v7, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v8, p2, v1

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/v/t;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/v/t;->i()I

    move-result v4

    if-ne v4, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v4, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v4, v4, Lmaps/v/r;->g:Lmaps/ap/a;

    invoke-virtual {v4, p1, v3}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v4

    if-ne v4, p3, :cond_1

    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    sget-object v6, Lmaps/v/bu;->c:Lmaps/v/bu;

    move-object v0, p0

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/t;Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)Lmaps/v/t;

    move-result-object v0

    iget v1, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/v/ak;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    :cond_0
    :goto_1
    return v0

    :cond_1
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_1

    :cond_2
    :try_start_1
    invoke-interface {v2}, Lmaps/v/t;->j()Lmaps/v/t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    :cond_4
    throw v0
.end method

.method a(Ljava/lang/Object;ILmaps/v/h;)Z
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget-object v3, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2}, Lmaps/v/t;->i()I

    move-result v6

    if-ne v6, p2, :cond_2

    if-eqz v5, :cond_2

    iget-object v6, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v6, v6, Lmaps/v/r;->g:Lmaps/ap/a;

    invoke-virtual {v6, p1, v5}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v5

    if-ne v5, p3, :cond_1

    invoke-virtual {p3}, Lmaps/v/h;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p3}, Lmaps/v/h;->f()Lmaps/v/av;

    move-result-object v0

    invoke-interface {v2, v0}, Lmaps/v/t;->a(Lmaps/v/av;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    :goto_2
    return v0

    :cond_0
    :try_start_1
    invoke-virtual {p0, v0, v2}, Lmaps/v/ak;->b(Lmaps/v/t;Lmaps/v/t;)Lmaps/v/t;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move v0, v1

    goto :goto_2

    :cond_2
    :try_start_2
    invoke-interface {v2}, Lmaps/v/t;->j()Lmaps/v/t;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move v0, v1

    goto :goto_2
.end method

.method a(Ljava/lang/Object;ILmaps/v/h;Ljava/lang/Object;)Z
    .locals 11

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lmaps/v/ak;->c(J)V

    iget v0, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v7, v0, 0x1

    iget-object v8, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v9, p2, v0

    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_5

    invoke-interface {v1}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1}, Lmaps/v/t;->i()I

    move-result v10

    if-ne v10, p2, :cond_4

    if-eqz v3, :cond_4

    iget-object v10, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v10, v10, Lmaps/v/r;->g:Lmaps/ap/a;

    invoke-virtual {v10, p1, v3}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v0

    invoke-interface {v0}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    if-ne p3, v0, :cond_3

    :cond_0
    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    invoke-virtual {p3}, Lmaps/v/h;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v3, :cond_2

    sget-object v0, Lmaps/v/bu;->c:Lmaps/v/bu;

    :goto_1
    invoke-virtual {p0, p1, p2, p3, v0}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V

    add-int/lit8 v0, v7, -0x1

    move v7, v0

    :cond_1
    move-object v0, p0

    move-object v2, p1

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;Ljava/lang/Object;J)V

    iput v7, p0, Lmaps/v/ak;->b:I

    invoke-virtual {p0}, Lmaps/v/ak;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move v0, v6

    :goto_2
    return v0

    :cond_2
    :try_start_1
    sget-object v0, Lmaps/v/bu;->b:Lmaps/v/bu;

    goto :goto_1

    :cond_3
    new-instance v0, Lmaps/v/bn;

    const/4 v1, 0x0

    invoke-direct {v0, p4, v1}, Lmaps/v/bn;-><init>(Ljava/lang/Object;I)V

    sget-object v1, Lmaps/v/bu;->b:Lmaps/v/bu;

    invoke-virtual {p0, p1, p2, v0, v1}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move v0, v2

    goto :goto_2

    :cond_4
    :try_start_2
    invoke-interface {v1}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v1

    goto :goto_0

    :cond_5
    iget v1, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/v/ak;->d:I

    invoke-virtual {p0, p1, p2, v0}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/t;)Lmaps/v/t;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;Ljava/lang/Object;J)V

    invoke-virtual {v8, v9, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v7, p0, Lmaps/v/ak;->b:I

    invoke-virtual {p0}, Lmaps/v/ak;->i()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move v0, v6

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    throw v0
.end method

.method a(Lmaps/v/t;I)Z
    .locals 9

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget v0, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v7, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/v/t;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_1

    if-ne v2, p1, :cond_0

    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    invoke-interface {v2}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v5

    sget-object v6, Lmaps/v/bu;->c:Lmaps/v/bu;

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/t;Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)Lmaps/v/t;

    move-result-object v0

    iget v1, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/v/ak;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    :goto_1
    return v0

    :cond_0
    :try_start_1
    invoke-interface {v2}, Lmaps/v/t;->j()Lmaps/v/t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    throw v0
.end method

.method a(Lmaps/v/t;ILmaps/v/bu;)Z
    .locals 9

    iget v0, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v7, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/v/t;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_1

    if-ne v2, p1, :cond_0

    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    invoke-interface {v2}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v5

    move-object v0, p0

    move v4, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/t;Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)Lmaps/v/t;

    move-result-object v0

    iget v1, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/v/ak;->b:I

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-interface {v2}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method b(Ljava/lang/Object;ILmaps/v/af;)Ljava/lang/Object;
    .locals 18

    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x1

    invoke-virtual/range {p0 .. p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v4, v4, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v4}, Lmaps/ap/c;->a()J

    move-result-wide v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lmaps/v/ak;->c(J)V

    move-object/from16 v0, p0

    iget v4, v0, Lmaps/v/ak;->b:I

    add-int/lit8 v12, v4, -0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v13}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    and-int v14, p2, v4

    invoke-virtual {v13, v14}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmaps/v/t;

    move-object v6, v4

    :goto_0
    if-eqz v6, :cond_7

    invoke-interface {v6}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v15

    invoke-interface {v6}, Lmaps/v/t;->i()I

    move-result v9

    move/from16 v0, p2

    if-ne v9, v0, :cond_3

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v9, v9, Lmaps/v/r;->g:Lmaps/ap/a;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v15}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v6}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v9

    invoke-interface {v9}, Lmaps/v/av;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v7, 0x0

    move-object v8, v9

    :goto_1
    if-eqz v7, :cond_6

    new-instance v5, Lmaps/v/h;

    invoke-direct {v5}, Lmaps/v/h;-><init>()V

    if-nez v6, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2, v4}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/t;)Lmaps/v/t;

    move-result-object v4

    invoke-interface {v4, v5}, Lmaps/v/t;->a(Lmaps/v/av;)V

    invoke-virtual {v13, v14, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v17, v5

    move-object v5, v4

    move-object/from16 v4, v17

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual/range {p0 .. p0}, Lmaps/v/ak;->n()V

    if-eqz v7, :cond_5

    :try_start_1
    monitor-enter v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v2, v4, v3}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/h;Lmaps/v/af;)Ljava/lang/Object;

    move-result-object v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/v/ak;->n:Lmaps/v/ab;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lmaps/v/ab;->b(I)V

    :goto_3
    return-object v4

    :cond_0
    :try_start_3
    invoke-interface {v9}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_1

    sget-object v7, Lmaps/v/bu;->c:Lmaps/v/bu;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v15, v1, v9, v7}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/v/ak;->l:Ljava/util/Queue;

    invoke-interface {v7, v6}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/v/ak;->m:Ljava/util/Queue;

    invoke-interface {v7, v6}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iput v12, v0, Lmaps/v/ak;->b:I

    move v7, v8

    move-object v8, v9

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/v/ak;->a:Lmaps/v/r;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v10, v11}, Lmaps/v/r;->b(Lmaps/v/t;J)Z

    move-result v16

    if-eqz v16, :cond_2

    sget-object v7, Lmaps/v/bu;->d:Lmaps/v/bu;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v15, v1, v9, v7}, Lmaps/v/ak;->a(Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v4

    invoke-virtual/range {p0 .. p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual/range {p0 .. p0}, Lmaps/v/ak;->n()V

    throw v4

    :cond_2
    :try_start_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v10, v11}, Lmaps/v/ak;->b(Lmaps/v/t;J)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/v/ak;->n:Lmaps/v/ab;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Lmaps/v/ab;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-virtual/range {p0 .. p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual/range {p0 .. p0}, Lmaps/v/ak;->n()V

    move-object v4, v7

    goto :goto_3

    :cond_3
    :try_start_5
    invoke-interface {v6}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v6

    goto/16 :goto_0

    :cond_4
    invoke-interface {v6, v5}, Lmaps/v/t;->a(Lmaps/v/av;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v4, v5

    move-object v5, v6

    goto/16 :goto_2

    :catchall_1
    move-exception v4

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/v/ak;->n:Lmaps/v/ab;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lmaps/v/ab;->b(I)V

    throw v4

    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v5, v1, v8}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;Lmaps/v/av;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_3

    :cond_6
    move-object v4, v5

    move-object v5, v6

    goto/16 :goto_2

    :cond_7
    move/from16 v17, v8

    move-object v8, v7

    move/from16 v7, v17

    goto/16 :goto_1
.end method

.method b(Ljava/lang/Object;ILmaps/v/h;Lmaps/v/af;)Lmaps/ab/c;
    .locals 6

    invoke-virtual {p3, p1, p4}, Lmaps/v/h;->a(Ljava/lang/Object;Lmaps/v/af;)Lmaps/ab/c;

    move-result-object v5

    new-instance v0, Lmaps/v/b;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lmaps/v/b;-><init>(Lmaps/v/ak;Ljava/lang/Object;ILmaps/v/h;Lmaps/ab/c;)V

    sget-object v1, Lmaps/v/r;->b:Lmaps/ab/a;

    invoke-interface {v5, v0, v1}, Lmaps/ab/c;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-object v5
.end method

.method b(I)Lmaps/v/t;
    .locals 2

    iget-object v0, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    return-object v0
.end method

.method b(Ljava/lang/Object;I)Lmaps/v/t;
    .locals 3

    invoke-virtual {p0, p2}, Lmaps/v/ak;->b(I)Lmaps/v/t;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_3

    invoke-interface {v0}, Lmaps/v/t;->i()I

    move-result v1

    if-eq v1, p2, :cond_1

    :cond_0
    :goto_1
    invoke-interface {v0}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lmaps/v/ak;->a()V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v2, v2, Lmaps/v/r;->g:Lmaps/ap/a;

    invoke-virtual {v2, p1, v1}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_2
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method b(Lmaps/v/t;Lmaps/v/t;)Lmaps/v/t;
    .locals 3

    iget v1, p0, Lmaps/v/ak;->b:I

    invoke-interface {p2}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v0

    :goto_0
    if-eq p1, p2, :cond_1

    invoke-virtual {p0, p1}, Lmaps/v/ak;->b(Lmaps/v/t;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1}, Lmaps/v/ak;->a(Lmaps/v/t;)V

    add-int/lit8 v1, v1, -0x1

    :goto_1
    invoke-interface {p1}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, v0}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/t;)Lmaps/v/t;

    move-result-object v0

    goto :goto_1

    :cond_1
    iput v1, p0, Lmaps/v/ak;->b:I

    return-object v0
.end method

.method b()V
    .locals 1

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->c()V

    :cond_0
    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmaps/v/ak;->d()V

    :cond_1
    return-void
.end method

.method b(J)V
    .locals 3

    invoke-virtual {p0}, Lmaps/v/ak;->h()V

    :cond_0
    iget-object v0, p0, Lmaps/v/ak;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v1, v0, p1, p2}, Lmaps/v/r;->b(Lmaps/v/t;J)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lmaps/v/t;->i()I

    move-result v1

    sget-object v2, Lmaps/v/bu;->d:Lmaps/v/bu;

    invoke-virtual {p0, v0, v1, v2}, Lmaps/v/ak;->a(Lmaps/v/t;ILmaps/v/bu;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lmaps/v/ak;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v1, v0, p1, p2}, Lmaps/v/r;->b(Lmaps/v/t;J)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lmaps/v/t;->i()I

    move-result v1

    sget-object v2, Lmaps/v/bu;->d:Lmaps/v/bu;

    invoke-virtual {p0, v0, v1, v2}, Lmaps/v/ak;->a(Lmaps/v/t;ILmaps/v/bu;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    return-void
.end method

.method b(Lmaps/v/t;J)V
    .locals 1

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p2, p3}, Lmaps/v/t;->a(J)V

    :cond_0
    iget-object v0, p0, Lmaps/v/ak;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .locals 10

    const/4 v7, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lmaps/v/ak;->c(J)V

    iget v0, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v8, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v9, p2, v0

    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/v/t;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_4

    invoke-interface {v2}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/v/t;->i()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->g:Lmaps/ap/a;

    invoke-virtual {v0, p1, v3}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v5

    invoke-interface {v5}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v4, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v4, v4, Lmaps/v/r;->h:Lmaps/ap/a;

    invoke-virtual {v4, p3, v0}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v6, Lmaps/v/bu;->a:Lmaps/v/bu;

    :goto_1
    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/t;Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)Lmaps/v/t;

    move-result-object v0

    iget v1, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v8, v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/v/ak;->b:I

    sget-object v0, Lmaps/v/bu;->a:Lmaps/v/bu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v6, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move v7, v0

    :goto_3
    return v7

    :cond_0
    if-nez v0, :cond_1

    :try_start_1
    invoke-interface {v5}, Lmaps/v/av;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v6, Lmaps/v/bu;->c:Lmaps/v/bu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_3

    :cond_2
    move v0, v7

    goto :goto_2

    :cond_3
    :try_start_2
    invoke-interface {v2}, Lmaps/v/t;->j()Lmaps/v/t;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    throw v0
.end method

.method b(Lmaps/v/t;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-interface {p1}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v1

    invoke-interface {v1}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-interface {v1}, Lmaps/v/av;->b()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 8

    const/4 v0, 0x0

    :try_start_0
    iget v1, p0, Lmaps/v/ak;->b:I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v1, v1, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v1}, Lmaps/ap/c;->a()J

    move-result-wide v5

    invoke-virtual {p0, p1, p2, v5, v6}, Lmaps/v/ak;->a(Ljava/lang/Object;IJ)Lmaps/v/t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v2

    invoke-interface {v2}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v1, v5, v6}, Lmaps/v/ak;->a(Lmaps/v/t;J)V

    invoke-interface {v1}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v7, v0, Lmaps/v/r;->u:Lmaps/v/af;

    move-object v0, p0

    move v3, p2

    invoke-virtual/range {v0 .. v7}, Lmaps/v/ak;->a(Lmaps/v/t;Ljava/lang/Object;ILjava/lang/Object;JLmaps/v/af;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lmaps/v/ak;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    throw v0
.end method

.method c(Ljava/lang/Object;ILmaps/v/af;)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, Lmaps/v/ak;->a(Ljava/lang/Object;I)Lmaps/v/h;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1, p2, v1, p3}, Lmaps/v/ak;->b(Ljava/lang/Object;ILmaps/v/h;Lmaps/v/af;)Lmaps/ab/c;

    move-result-object v1

    invoke-interface {v1}, Lmaps/ab/c;->isDone()Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v1}, Lmaps/ab/c;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method c(Lmaps/v/t;J)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->a()V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v1

    invoke-interface {v1}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lmaps/v/ak;->a()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v2, p1, p2, p3}, Lmaps/v/r;->b(Lmaps/v/t;J)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, p2, p3}, Lmaps/v/ak;->a(J)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method c()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/v/ak;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lmaps/v/t;

    iget-object v2, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v2, v0}, Lmaps/v/r;->a(Lmaps/v/t;)V

    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method c(J)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lmaps/v/ak;->d(J)V

    return-void
.end method

.method d()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/v/ak;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lmaps/v/av;

    iget-object v2, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v2, v0}, Lmaps/v/r;->a(Lmaps/v/av;)V

    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method d(J)V
    .locals 2

    invoke-virtual {p0}, Lmaps/v/ak;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lmaps/v/ak;->b()V

    invoke-virtual {p0, p1, p2}, Lmaps/v/ak;->b(J)V

    iget-object v0, p0, Lmaps/v/ak;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    throw v0
.end method

.method d(Ljava/lang/Object;I)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget v1, p0, Lmaps/v/ak;->b:I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v1, v1, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v1}, Lmaps/ap/c;->a()J

    move-result-wide v1

    invoke-virtual {p0, p1, p2, v1, v2}, Lmaps/v/ak;->a(Ljava/lang/Object;IJ)Lmaps/v/t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    :goto_0
    return v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v1

    invoke-interface {v1}, Lmaps/v/av;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->m()V

    throw v0
.end method

.method e(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 10

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget-object v1, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v1, v1, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v1}, Lmaps/ap/c;->a()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lmaps/v/ak;->c(J)V

    iget v1, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v1, v1, -0x1

    iget-object v8, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v9, p2, v1

    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/v/t;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, Lmaps/v/t;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lmaps/v/t;->i()I

    move-result v4

    if-ne v4, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v4, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v4, v4, Lmaps/v/r;->g:Lmaps/ap/a;

    invoke-virtual {v4, p1, v3}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v5

    invoke-interface {v5}, Lmaps/v/av;->get()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_0

    sget-object v6, Lmaps/v/bu;->a:Lmaps/v/bu;

    :goto_1
    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/t;Ljava/lang/Object;ILmaps/v/av;Lmaps/v/bu;)Lmaps/v/t;

    move-result-object v0

    iget v1, p0, Lmaps/v/ak;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v8, v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, Lmaps/v/ak;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    move-object v0, v7

    :goto_2
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v5}, Lmaps/v/av;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v6, Lmaps/v/bu;->c:Lmaps/v/bu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_2

    :cond_2
    :try_start_2
    invoke-interface {v2}, Lmaps/v/t;->j()Lmaps/v/t;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    throw v0
.end method

.method e()V
    .locals 1

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->f()V

    :cond_0
    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmaps/v/ak;->g()V

    :cond_1
    return-void
.end method

.method f()V
    .locals 1

    :cond_0
    iget-object v0, p0, Lmaps/v/ak;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_0

    return-void
.end method

.method g()V
    .locals 1

    :cond_0
    iget-object v0, p0, Lmaps/v/ak;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_0

    return-void
.end method

.method h()V
    .locals 2

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/v/ak;->j:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmaps/v/ak;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/v/ak;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method i()V
    .locals 4

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lmaps/v/ak;->h()V

    :cond_2
    iget v0, p0, Lmaps/v/ak;->c:I

    int-to-long v0, v0

    iget-wide v2, p0, Lmaps/v/ak;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->j()Lmaps/v/t;

    move-result-object v0

    invoke-interface {v0}, Lmaps/v/t;->i()I

    move-result v1

    sget-object v2, Lmaps/v/bu;->e:Lmaps/v/bu;

    invoke-virtual {p0, v0, v1, v2}, Lmaps/v/ak;->a(Lmaps/v/t;ILmaps/v/bu;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method j()Lmaps/v/t;
    .locals 3

    iget-object v0, p0, Lmaps/v/ak;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    invoke-interface {v0}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v2

    invoke-interface {v2}, Lmaps/v/av;->c()I

    move-result v2

    if-lez v2, :cond_0

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method k()V
    .locals 11

    iget-object v7, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    const/high16 v0, 0x40000000

    if-lt v8, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v5, p0, Lmaps/v/ak;->b:I

    shl-int/lit8 v0, v8, 0x1

    invoke-virtual {p0, v0}, Lmaps/v/ak;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/v/ak;->e:I

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_5

    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    if-eqz v0, :cond_7

    invoke-interface {v0}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v3

    invoke-interface {v0}, Lmaps/v/t;->i()I

    move-result v1

    and-int v2, v1, v10

    if-nez v3, :cond_2

    invoke-virtual {v9, v2, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v1, v5

    :cond_1
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v5, v1

    goto :goto_1

    :cond_2
    move-object v4, v0

    :goto_3
    if-eqz v3, :cond_3

    invoke-interface {v3}, Lmaps/v/t;->i()I

    move-result v1

    and-int/2addr v1, v10

    if-eq v1, v2, :cond_6

    move-object v2, v3

    :goto_4
    invoke-interface {v3}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v3

    move-object v4, v2

    move v2, v1

    goto :goto_3

    :cond_3
    invoke-virtual {v9, v2, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v2, v0

    move v1, v5

    :goto_5
    if-eq v2, v4, :cond_1

    invoke-virtual {p0, v2}, Lmaps/v/ak;->b(Lmaps/v/t;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, v2}, Lmaps/v/ak;->a(Lmaps/v/t;)V

    add-int/lit8 v0, v1, -0x1

    :goto_6
    invoke-interface {v2}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v1

    move-object v2, v1

    move v1, v0

    goto :goto_5

    :cond_4
    invoke-interface {v2}, Lmaps/v/t;->i()I

    move-result v0

    and-int v3, v0, v10

    invoke-virtual {v9, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    invoke-virtual {p0, v2, v0}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/t;)Lmaps/v/t;

    move-result-object v0

    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v1

    goto :goto_6

    :cond_5
    iput-object v9, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput v5, p0, Lmaps/v/ak;->b:I

    goto :goto_0

    :cond_6
    move v1, v2

    move-object v2, v4

    goto :goto_4

    :cond_7
    move v1, v5

    goto :goto_2
.end method

.method l()V
    .locals 5

    const/4 v1, 0x0

    iget v0, p0, Lmaps/v/ak;->b:I

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lmaps/v/ak;->lock()V

    :try_start_0
    iget-object v3, p0, Lmaps/v/ak;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move v2, v1

    :goto_0
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/t;

    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lmaps/v/t;->h()Lmaps/v/av;

    move-result-object v4

    invoke-interface {v4}, Lmaps/v/av;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lmaps/v/bu;->a:Lmaps/v/bu;

    invoke-virtual {p0, v0, v4}, Lmaps/v/ak;->a(Lmaps/v/t;Lmaps/v/bu;)V

    :cond_0
    invoke-interface {v0}, Lmaps/v/t;->j()Lmaps/v/t;

    move-result-object v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_2
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lmaps/v/ak;->e()V

    iget-object v0, p0, Lmaps/v/ak;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lmaps/v/ak;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lmaps/v/ak;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget v0, p0, Lmaps/v/ak;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/v/ak;->d:I

    const/4 v0, 0x0

    iput v0, p0, Lmaps/v/ak;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    :cond_4
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/v/ak;->unlock()V

    invoke-virtual {p0}, Lmaps/v/ak;->n()V

    throw v0
.end method

.method m()V
    .locals 1

    iget-object v0, p0, Lmaps/v/ak;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/v/ak;->o()V

    :cond_0
    return-void
.end method

.method n()V
    .locals 0

    invoke-virtual {p0}, Lmaps/v/ak;->p()V

    return-void
.end method

.method o()V
    .locals 2

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    iget-object v0, v0, Lmaps/v/r;->r:Lmaps/ap/c;

    invoke-virtual {v0}, Lmaps/ap/c;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lmaps/v/ak;->d(J)V

    invoke-virtual {p0}, Lmaps/v/ak;->p()V

    return-void
.end method

.method p()V
    .locals 1

    invoke-virtual {p0}, Lmaps/v/ak;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/v/ak;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->r()V

    :cond_0
    return-void
.end method
