.class public final Lmaps/v/l;
.super Ljava/lang/Object;


# static fields
.field static final a:Lmaps/ap/k;

.field static final b:Lmaps/v/m;

.field static final c:Lmaps/ap/k;

.field static final d:Lmaps/ap/c;

.field private static final u:Ljava/util/logging/Logger;


# instance fields
.field e:Z

.field f:I

.field g:I

.field h:J

.field i:J

.field j:Lmaps/v/ay;

.field k:Lmaps/v/ac;

.field l:Lmaps/v/ac;

.field m:J

.field n:J

.field o:J

.field p:Lmaps/ap/a;

.field q:Lmaps/ap/a;

.field r:Lmaps/v/f;

.field s:Lmaps/ap/c;

.field t:Lmaps/ap/k;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const-wide/16 v1, 0x0

    new-instance v0, Lmaps/v/al;

    invoke-direct {v0}, Lmaps/v/al;-><init>()V

    invoke-static {v0}, Lmaps/ap/o;->a(Ljava/lang/Object;)Lmaps/ap/k;

    move-result-object v0

    sput-object v0, Lmaps/v/l;->a:Lmaps/ap/k;

    new-instance v0, Lmaps/v/m;

    move-wide v3, v1

    move-wide v5, v1

    move-wide v7, v1

    move-wide v9, v1

    move-wide v11, v1

    invoke-direct/range {v0 .. v12}, Lmaps/v/m;-><init>(JJJJJJ)V

    sput-object v0, Lmaps/v/l;->b:Lmaps/v/m;

    new-instance v0, Lmaps/v/ai;

    invoke-direct {v0}, Lmaps/v/ai;-><init>()V

    sput-object v0, Lmaps/v/l;->c:Lmaps/ap/k;

    new-instance v0, Lmaps/v/ah;

    invoke-direct {v0}, Lmaps/v/ah;-><init>()V

    sput-object v0, Lmaps/v/l;->d:Lmaps/ap/c;

    const-class v0, Lmaps/v/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lmaps/v/l;->u:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>()V
    .locals 4

    const/4 v3, -0x1

    const-wide/16 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/v/l;->e:Z

    iput v3, p0, Lmaps/v/l;->f:I

    iput v3, p0, Lmaps/v/l;->g:I

    iput-wide v1, p0, Lmaps/v/l;->h:J

    iput-wide v1, p0, Lmaps/v/l;->i:J

    iput-wide v1, p0, Lmaps/v/l;->m:J

    iput-wide v1, p0, Lmaps/v/l;->n:J

    iput-wide v1, p0, Lmaps/v/l;->o:J

    sget-object v0, Lmaps/v/l;->c:Lmaps/ap/k;

    iput-object v0, p0, Lmaps/v/l;->t:Lmaps/ap/k;

    return-void
.end method

.method public static a()Lmaps/v/l;
    .locals 1

    new-instance v0, Lmaps/v/l;

    invoke-direct {v0}, Lmaps/v/l;-><init>()V

    return-object v0
.end method

.method private p()V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, -0x1

    iget-object v2, p0, Lmaps/v/l;->j:Lmaps/v/ay;

    if-nez v2, :cond_2

    iget-wide v2, p0, Lmaps/v/l;->i:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    :goto_0
    const-string v1, "maximumWeight requires weigher"

    invoke-static {v0, v1}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lmaps/v/l;->e:Z

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lmaps/v/l;->i:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    :goto_2
    const-string v1, "weigher requires maximumWeight"

    invoke-static {v0, v1}, Lmaps/ap/q;->b(ZLjava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    iget-wide v0, p0, Lmaps/v/l;->i:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    sget-object v0, Lmaps/v/l;->u:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "ignoring weigher specified without maximumWeight"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method a(Z)Lmaps/ap/c;
    .locals 1

    iget-object v0, p0, Lmaps/v/l;->s:Lmaps/ap/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/v/l;->s:Lmaps/ap/c;

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {}, Lmaps/ap/c;->b()Lmaps/ap/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lmaps/v/l;->d:Lmaps/ap/c;

    goto :goto_0
.end method

.method a(Lmaps/v/ac;)Lmaps/v/l;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/v/l;->k:Lmaps/v/ac;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Key strength was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lmaps/v/l;->k:Lmaps/v/ac;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lmaps/ap/q;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lmaps/ap/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/ac;

    iput-object v0, p0, Lmaps/v/l;->k:Lmaps/v/ac;

    return-object p0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public a(Lmaps/v/af;)Lmaps/v/q;
    .locals 1

    invoke-direct {p0}, Lmaps/v/l;->p()V

    new-instance v0, Lmaps/v/a;

    invoke-direct {v0, p0, p1}, Lmaps/v/a;-><init>(Lmaps/v/l;Lmaps/v/af;)V

    return-object v0
.end method

.method b()Lmaps/ap/a;
    .locals 2

    iget-object v0, p0, Lmaps/v/l;->p:Lmaps/ap/a;

    invoke-virtual {p0}, Lmaps/v/l;->i()Lmaps/v/ac;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/v/ac;->a()Lmaps/ap/a;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ap/a;

    return-object v0
.end method

.method c()Lmaps/ap/a;
    .locals 2

    iget-object v0, p0, Lmaps/v/l;->q:Lmaps/ap/a;

    invoke-virtual {p0}, Lmaps/v/l;->j()Lmaps/v/ac;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/v/ac;->a()Lmaps/ap/a;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ap/a;

    return-object v0
.end method

.method d()I
    .locals 2

    iget v0, p0, Lmaps/v/l;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/v/l;->f:I

    goto :goto_0
.end method

.method e()I
    .locals 2

    iget v0, p0, Lmaps/v/l;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/v/l;->g:I

    goto :goto_0
.end method

.method f()J
    .locals 4

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lmaps/v/l;->m:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lmaps/v/l;->n:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lmaps/v/l;->j:Lmaps/v/ay;

    if-nez v0, :cond_2

    iget-wide v0, p0, Lmaps/v/l;->h:J

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lmaps/v/l;->i:J

    goto :goto_0
.end method

.method g()Lmaps/v/ay;
    .locals 2

    iget-object v0, p0, Lmaps/v/l;->j:Lmaps/v/ay;

    sget-object v1, Lmaps/v/az;->a:Lmaps/v/az;

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/ay;

    return-object v0
.end method

.method public h()Lmaps/v/l;
    .locals 1

    sget-object v0, Lmaps/v/ac;->c:Lmaps/v/ac;

    invoke-virtual {p0, v0}, Lmaps/v/l;->a(Lmaps/v/ac;)Lmaps/v/l;

    move-result-object v0

    return-object v0
.end method

.method i()Lmaps/v/ac;
    .locals 2

    iget-object v0, p0, Lmaps/v/l;->k:Lmaps/v/ac;

    sget-object v1, Lmaps/v/ac;->a:Lmaps/v/ac;

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/ac;

    return-object v0
.end method

.method j()Lmaps/v/ac;
    .locals 2

    iget-object v0, p0, Lmaps/v/l;->l:Lmaps/v/ac;

    sget-object v1, Lmaps/v/ac;->a:Lmaps/v/ac;

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/ac;

    return-object v0
.end method

.method k()J
    .locals 4

    iget-wide v0, p0, Lmaps/v/l;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lmaps/v/l;->m:J

    goto :goto_0
.end method

.method l()J
    .locals 4

    iget-wide v0, p0, Lmaps/v/l;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lmaps/v/l;->n:J

    goto :goto_0
.end method

.method m()J
    .locals 4

    iget-wide v0, p0, Lmaps/v/l;->o:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lmaps/v/l;->o:J

    goto :goto_0
.end method

.method n()Lmaps/v/f;
    .locals 2

    iget-object v0, p0, Lmaps/v/l;->r:Lmaps/v/f;

    sget-object v1, Lmaps/v/v;->a:Lmaps/v/v;

    invoke-static {v0, v1}, Lmaps/ap/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/v/f;

    return-object v0
.end method

.method o()Lmaps/ap/k;
    .locals 1

    iget-object v0, p0, Lmaps/v/l;->t:Lmaps/ap/k;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    const/4 v3, -0x1

    const-wide/16 v5, -0x1

    invoke-static {p0}, Lmaps/ap/e;->a(Ljava/lang/Object;)Lmaps/ap/n;

    move-result-object v0

    iget v1, p0, Lmaps/v/l;->f:I

    if-eq v1, v3, :cond_0

    const-string v1, "initialCapacity"

    iget v2, p0, Lmaps/v/l;->f:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    :cond_0
    iget v1, p0, Lmaps/v/l;->g:I

    if-eq v1, v3, :cond_1

    const-string v1, "concurrencyLevel"

    iget v2, p0, Lmaps/v/l;->g:I

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;I)Lmaps/ap/n;

    :cond_1
    iget-wide v1, p0, Lmaps/v/l;->i:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/v/l;->j:Lmaps/v/ay;

    if-nez v1, :cond_a

    const-string v1, "maximumSize"

    iget-wide v2, p0, Lmaps/v/l;->i:J

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ap/n;->a(Ljava/lang/String;J)Lmaps/ap/n;

    :cond_2
    :goto_0
    iget-wide v1, p0, Lmaps/v/l;->m:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_3

    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lmaps/v/l;->m:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    :cond_3
    iget-wide v1, p0, Lmaps/v/l;->n:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_4

    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lmaps/v/l;->n:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    :cond_4
    iget-object v1, p0, Lmaps/v/l;->k:Lmaps/v/ac;

    if-eqz v1, :cond_5

    const-string v1, "keyStrength"

    iget-object v2, p0, Lmaps/v/l;->k:Lmaps/v/ac;

    invoke-virtual {v2}, Lmaps/v/ac;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/ap/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    :cond_5
    iget-object v1, p0, Lmaps/v/l;->l:Lmaps/v/ac;

    if-eqz v1, :cond_6

    const-string v1, "valueStrength"

    iget-object v2, p0, Lmaps/v/l;->l:Lmaps/v/ac;

    invoke-virtual {v2}, Lmaps/v/ac;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/ap/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/ap/n;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/ap/n;

    :cond_6
    iget-object v1, p0, Lmaps/v/l;->p:Lmaps/ap/a;

    if-eqz v1, :cond_7

    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, Lmaps/ap/n;->a(Ljava/lang/Object;)Lmaps/ap/n;

    :cond_7
    iget-object v1, p0, Lmaps/v/l;->q:Lmaps/ap/a;

    if-eqz v1, :cond_8

    const-string v1, "valueEquivalence"

    invoke-virtual {v0, v1}, Lmaps/ap/n;->a(Ljava/lang/Object;)Lmaps/ap/n;

    :cond_8
    iget-object v1, p0, Lmaps/v/l;->r:Lmaps/v/f;

    if-eqz v1, :cond_9

    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, Lmaps/ap/n;->a(Ljava/lang/Object;)Lmaps/ap/n;

    :cond_9
    invoke-virtual {v0}, Lmaps/ap/n;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_a
    const-string v1, "maximumWeight"

    iget-wide v2, p0, Lmaps/v/l;->i:J

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ap/n;->a(Ljava/lang/String;J)Lmaps/ap/n;

    goto/16 :goto_0
.end method
