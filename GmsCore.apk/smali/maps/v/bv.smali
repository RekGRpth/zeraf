.class final enum Lmaps/v/bv;
.super Ljava/lang/Enum;

# interfaces
.implements Lmaps/v/t;


# static fields
.field public static final enum a:Lmaps/v/bv;

.field private static final synthetic b:[Lmaps/v/bv;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lmaps/v/bv;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lmaps/v/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bv;->a:Lmaps/v/bv;

    const/4 v0, 0x1

    new-array v0, v0, [Lmaps/v/bv;

    sget-object v1, Lmaps/v/bv;->a:Lmaps/v/bv;

    aput-object v1, v0, v2

    sput-object v0, Lmaps/v/bv;->b:[Lmaps/v/bv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/v/bv;
    .locals 1

    const-class v0, Lmaps/v/bv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/v/bv;

    return-object v0
.end method

.method public static values()[Lmaps/v/bv;
    .locals 1

    sget-object v0, Lmaps/v/bv;->b:[Lmaps/v/bv;

    invoke-virtual {v0}, [Lmaps/v/bv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/v/bv;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(J)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/v/av;)V
    .locals 0

    return-void
.end method

.method public a(Lmaps/v/t;)V
    .locals 0

    return-void
.end method

.method public b()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public b(J)V
    .locals 0

    return-void
.end method

.method public b(Lmaps/v/t;)V
    .locals 0

    return-void
.end method

.method public c()Lmaps/v/t;
    .locals 0

    return-object p0
.end method

.method public c(Lmaps/v/t;)V
    .locals 0

    return-void
.end method

.method public d()Lmaps/v/t;
    .locals 0

    return-object p0
.end method

.method public d(Lmaps/v/t;)V
    .locals 0

    return-void
.end method

.method public e()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public f()Lmaps/v/t;
    .locals 0

    return-object p0
.end method

.method public g()Lmaps/v/t;
    .locals 0

    return-object p0
.end method

.method public h()Lmaps/v/av;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public i()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public j()Lmaps/v/t;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
