.class public abstract enum Lmaps/v/bu;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/v/bu;

.field public static final enum b:Lmaps/v/bu;

.field public static final enum c:Lmaps/v/bu;

.field public static final enum d:Lmaps/v/bu;

.field public static final enum e:Lmaps/v/bu;

.field private static final synthetic f:[Lmaps/v/bu;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/v/ap;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, Lmaps/v/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bu;->a:Lmaps/v/bu;

    new-instance v0, Lmaps/v/au;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, Lmaps/v/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bu;->b:Lmaps/v/bu;

    new-instance v0, Lmaps/v/at;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, Lmaps/v/at;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bu;->c:Lmaps/v/bu;

    new-instance v0, Lmaps/v/as;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, Lmaps/v/as;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bu;->d:Lmaps/v/bu;

    new-instance v0, Lmaps/v/aq;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, Lmaps/v/aq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/v/bu;->e:Lmaps/v/bu;

    const/4 v0, 0x5

    new-array v0, v0, [Lmaps/v/bu;

    sget-object v1, Lmaps/v/bu;->a:Lmaps/v/bu;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/v/bu;->b:Lmaps/v/bu;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/v/bu;->c:Lmaps/v/bu;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/v/bu;->d:Lmaps/v/bu;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/v/bu;->e:Lmaps/v/bu;

    aput-object v1, v0, v6

    sput-object v0, Lmaps/v/bu;->f:[Lmaps/v/bu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILmaps/v/ap;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/v/bu;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/v/bu;
    .locals 1

    const-class v0, Lmaps/v/bu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/v/bu;

    return-object v0
.end method

.method public static values()[Lmaps/v/bu;
    .locals 1

    sget-object v0, Lmaps/v/bu;->f:[Lmaps/v/bu;

    invoke-virtual {v0}, [Lmaps/v/bu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/v/bu;

    return-object v0
.end method


# virtual methods
.method abstract a()Z
.end method
