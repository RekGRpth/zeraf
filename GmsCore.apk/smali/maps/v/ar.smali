.class final Lmaps/v/ar;
.super Ljava/util/AbstractSet;


# instance fields
.field final synthetic a:Lmaps/v/r;


# direct methods
.method constructor <init>(Lmaps/v/r;)V
    .locals 0

    iput-object p1, p0, Lmaps/v/ar;->a:Lmaps/v/r;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lmaps/v/ar;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x0

    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/v/ar;->a:Lmaps/v/r;

    invoke-virtual {v2, v1}, Lmaps/v/r;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/v/ar;->a:Lmaps/v/r;

    iget-object v2, v2, Lmaps/v/r;->h:Lmaps/ap/a;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lmaps/ap/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lmaps/v/ar;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lmaps/v/ax;

    iget-object v1, p0, Lmaps/v/ar;->a:Lmaps/v/r;

    invoke-direct {v0, v1}, Lmaps/v/ax;-><init>(Lmaps/v/r;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x0

    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmaps/v/ar;->a:Lmaps/v/r;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lmaps/v/r;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lmaps/v/ar;->a:Lmaps/v/r;

    invoke-virtual {v0}, Lmaps/v/r;->size()I

    move-result v0

    return v0
.end method
