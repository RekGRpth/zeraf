.class Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;
.super Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;
.source "MapView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/maps/MapView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MapViewLifecycleHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/dynamic/DeferredLifecycleHelper",
        "<",
        "Lcom/google/android/gms/maps/MapView$MapViewDelegate;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field protected mDelegateCreatedListener:Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/dynamic/OnDelegateCreatedListener",
            "<",
            "Lcom/google/android/gms/maps/MapView$MapViewDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final mOptions:Lcom/google/android/gms/maps/GoogleMapOptions;

.field private final mViewGroup:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-direct {p0}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->mViewGroup:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->mOptions:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method


# virtual methods
.method protected createDelegate(Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/dynamic/OnDelegateCreatedListener",
            "<",
            "Lcom/google/android/gms/maps/MapView$MapViewDelegate;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->mDelegateCreatedListener:Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;

    invoke-virtual {p0}, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->maybeCreateDelegate()V

    return-void
.end method

.method public maybeCreateDelegate()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->mDelegateCreatedListener:Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->getDelegate()Lcom/google/android/gms/dynamic/LifecycleDelegate;

    move-result-object v2

    if-nez v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/maps/internal/MapsDynamicJar;->getCreator(Landroid/content/Context;)Lcom/google/android/gms/maps/internal/ICreator;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->mOptions:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v2, v3, v4}, Lcom/google/android/gms/maps/internal/ICreator;->newMapViewDelegate(Lcom/google/android/gms/dynamic/IObjectWrapper;Lcom/google/android/gms/maps/GoogleMapOptions;)Lcom/google/android/gms/maps/internal/IMapViewDelegate;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->mDelegateCreatedListener:Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;

    new-instance v3, Lcom/google/android/gms/maps/MapView$MapViewDelegate;

    iget-object v4, p0, Lcom/google/android/gms/maps/MapView$MapViewLifecycleHelper;->mViewGroup:Landroid/view/ViewGroup;

    invoke-direct {v3, v4, v1}, Lcom/google/android/gms/maps/MapView$MapViewDelegate;-><init>(Landroid/view/ViewGroup;Lcom/google/android/gms/maps/internal/IMapViewDelegate;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/dynamic/OnDelegateCreatedListener;->onDelegateCreated(Lcom/google/android/gms/dynamic/LifecycleDelegate;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v2, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v2

    :catch_1
    move-exception v2

    goto :goto_0
.end method
