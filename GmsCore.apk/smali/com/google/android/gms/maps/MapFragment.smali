.class public Lcom/google/android/gms/maps/MapFragment;
.super Landroid/app/Fragment;
.source "MapFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;,
        Lcom/google/android/gms/maps/MapFragment$MapFragmentDelegate;
    }
.end annotation


# instance fields
.field private final mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    invoke-direct {v0, p0}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;-><init>(Landroid/app/Fragment;)V

    iput-object v0, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    # invokes: Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->setActivity(Landroid/app/Activity;)V
    invoke-static {v0, p1}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->access$000(Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;Landroid/app/Activity;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->onDestroy()V

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->onDestroyView()V

    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    # invokes: Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->setActivity(Landroid/app/Activity;)V
    invoke-static {v2, p1}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->access$000(Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;Landroid/app/Activity;)V

    invoke-static {p1, p2}, Lcom/google/android/gms/maps/GoogleMapOptions;->createFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MapOptions"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v2, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    invoke-virtual {v2, p1, v0, p3}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->onInflate(Landroid/app/Activity;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->onLowMemory()V

    invoke-super {p0}, Landroid/app/Fragment;->onLowMemory()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->onPause()V

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/gms/maps/MapFragment;->mLifecycleHelper:Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/MapFragment$MapFragmentLifecycleHelper;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-void
.end method
