.class public Lcom/google/android/gms/auth/NotificationStore$NotificationData;
.super Ljava/lang/Object;
.source "NotificationStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/NotificationStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NotificationData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/NotificationStore$NotificationData$NotificationDataBuilderException;,
        Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;,
        Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private notificationDataKey:Ljava/lang/String;

.field private notificationId:I

.field private requests:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private type:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/auth/NotificationStore$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/auth/NotificationStore$1;

    invoke-direct {p0}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/os/Bundle;Lcom/google/android/gms/auth/NotificationStore;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .locals 1
    .param p0    # Landroid/os/Bundle;
    .param p1    # Lcom/google/android/gms/auth/NotificationStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/NotificationStore$NotificationData$NotificationDataBuilderException;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->fromBundle(Landroid/os/Bundle;Lcom/google/android/gms/auth/NotificationStore;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationDataKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/gms/auth/NotificationStore$NotificationData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationDataKey:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;
    .locals 1
    .param p0    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->type:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/gms/auth/NotificationStore$NotificationData;Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;
    .locals 0
    .param p0    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .param p1    # Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    iput-object p1, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->type:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/gms/auth/NotificationStore$NotificationData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->account:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/gms/auth/NotificationStore$NotificationData;)I
    .locals 1
    .param p0    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    iget v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationId:I

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/gms/auth/NotificationStore$NotificationData;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationId:I

    return p1
.end method

.method private fireCallback(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const-string v7, "callback_intent"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    const-string v7, "callback_intent"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x1

    :try_start_0
    invoke-static {v4, v7}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->getInstance()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v7}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_1

    invoke-virtual {v1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v7, "NotificationStore"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "No receivers found for broadcast with action: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v7, "NotificationStore"

    const-string v8, "Error parsing URI to Intent"

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    const-string v7, "authority"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    const-string v7, "sync_extras"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_0

    const-string v7, "authAccount"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    new-instance v0, Landroid/accounts/Account;

    const-string v7, "authAccount"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.google"

    invoke-direct {v0, v7, v8}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "authority"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "sync_extras"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    invoke-static {v0, v7, v8}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private static fromBundle(Landroid/os/Bundle;Lcom/google/android/gms/auth/NotificationStore;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    .locals 7
    .param p0    # Landroid/os/Bundle;
    .param p1    # Lcom/google/android/gms/auth/NotificationStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/NotificationStore$NotificationData$NotificationDataBuilderException;
        }
    .end annotation

    new-instance v1, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;

    const/4 v4, 0x0

    invoke-direct {v1, p1, v4}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;-><init>(Lcom/google/android/gms/auth/NotificationStore;Lcom/google/android/gms/auth/NotificationStore$1;)V

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    invoke-static {v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->access$600(Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v4

    const-string v5, "notification_data_key"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationDataKey:Ljava/lang/String;

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    invoke-static {v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->access$600(Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v4

    const-string v5, "notification_id_key"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationId:I

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    invoke-static {v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->access$600(Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v4

    const-string v5, "authAccount"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->account:Ljava/lang/String;

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    invoke-static {v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->access$600(Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v4

    const-string v5, "requests_key"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;

    const-string v4, "type_key"

    invoke-virtual {p0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->valueOf(Ljava/lang/String;)Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    move-result-object v3

    const-string v4, "NotificationStore"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "NotificationStore"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(Notification) account: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    invoke-static {v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->access$600(Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v6

    iget-object v6, v6, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->account:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->data:Lcom/google/android/gms/auth/NotificationStore$NotificationData;
    invoke-static {v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->access$600(Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v4, "NotificationStore"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(request) app: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "package_key"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " scope: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v6}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v3}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Builder;->build(Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;)Lcom/google/android/gms/auth/NotificationStore$NotificationData;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public declared-synchronized addRequest(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public fireCallbacks()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->removeRequests()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->fireCallback(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getNotificationDataKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationDataKey:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationId:I

    return v0
.end method

.method public getRequest(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    const-string v2, "package_key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/gms/auth/login/RequestKey;->SERVICE:Lcom/google/android/gms/auth/login/RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasRequest()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized removeRequests()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public toBundle()Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "notification_data_key"

    iget-object v2, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationDataKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "authAccount"

    iget-object v2, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "notification_id_key"

    iget v2, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->notificationId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "type_key"

    iget-object v2, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->type:Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/NotificationStore$NotificationData$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "requests_key"

    iget-object v2, p0, Lcom/google/android/gms/auth/NotificationStore$NotificationData;->requests:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v0
.end method
