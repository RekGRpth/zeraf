.class Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;
.super Ljava/lang/Object;
.source "ScopeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/login/ScopeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScopeDetailsOnClickListener"
.end annotation


# instance fields
.field private final mAction:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field private final mDetails:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/gms/auth/login/ScopeFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/login/ScopeFragment;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;-><init>(Lcom/google/android/gms/auth/login/ScopeFragment;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/auth/login/ScopeFragment;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    iput-object p1, p0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;->this$0:Lcom/google/android/gms/auth/login/ScopeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;->mDetails:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;->mTitle:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;->mAction:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;->this$0:Lcom/google/android/gms/auth/login/ScopeFragment;

    # getter for: Lcom/google/android/gms/auth/login/ScopeFragment;->mScopeListener:Lcom/google/android/gms/auth/login/ScopeListener;
    invoke-static {v0}, Lcom/google/android/gms/auth/login/ScopeFragment;->access$000(Lcom/google/android/gms/auth/login/ScopeFragment;)Lcom/google/android/gms/auth/login/ScopeListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;->mDetails:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;->mTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ScopeFragment$ScopeDetailsOnClickListener;->mAction:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/auth/login/ScopeListener;->onDetailsClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)V

    return-void
.end method
