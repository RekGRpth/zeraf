.class public Lcom/google/android/gms/auth/login/LoginActivity;
.super Lcom/google/android/gms/auth/login/BaseActivity;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/login/LoginActivity$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected handleBack(IILandroid/content/Intent;Lcom/google/android/gms/auth/login/Status;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mAddAccount:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p2, p3}, Lcom/google/android/gms/auth/login/LoginActivity;->finish(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/gms/auth/login/LoginActivity;->finish(I)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x402

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleStatus(Lcom/google/android/gms/auth/login/Status;)V
    .locals 5

    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivity$1;->$SwitchMap$com$google$android$gms$auth$login$Status:[I

    invoke-virtual {p1}, Lcom/google/android/gms/auth/login/Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/ShowErrorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3f1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GLSUser;->isBrowser()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x402

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "authAccount"

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x3ec

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/CaptchaActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSSession;->mCallingPackage:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget v1, v1, Lcom/google/android/gms/auth/login/GLSSession;->mCallingUID:I

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mService:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v4, v4, Lcom/google/android/gms/auth/login/GLSSession;->mScopeData:Ljava/util/List;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->createIntent(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x403

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x6

    const/4 v4, 0x0

    const/16 v2, 0x3ec

    const/4 v1, 0x1

    const/4 v3, -0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    if-ne p2, v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->finish(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p3}, Lcom/google/android/gms/auth/login/Status;->fromExtra(Landroid/content/Intent;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v0

    if-nez p2, :cond_1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->handleBack(IILandroid/content/Intent;Lcom/google/android/gms/auth/login/Status;)V

    goto :goto_0

    :cond_1
    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :sswitch_0
    if-ne p2, v5, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    :sswitch_1
    if-ne p2, v3, :cond_3

    invoke-static {}, Lcom/google/android/gms/auth/login/LoginActivityTask;->createIntent()Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->finish(I)V

    goto :goto_0

    :sswitch_2
    invoke-static {p3}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->unpackResponse(Landroid/content/Intent;)Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;->getStatus()Lcom/google/android/gms/auth/login/Status;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/auth/login/Status;->SUCCESS:Lcom/google/android/gms/auth/login/Status;

    if-ne v1, v2, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity$Response;->getScopeData()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    invoke-static {}, Lcom/google/android/gms/auth/login/LoginActivityTask;->createIntent()Landroid/content/Intent;

    move-result-object v0

    :goto_1
    const/16 v1, 0x40c

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_5
    invoke-static {v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->createIntent(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :cond_6
    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/LoginActivity;->finish(I)V

    goto :goto_0

    :sswitch_3
    if-ne p2, v3, :cond_7

    iput-boolean v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mCallAuthenticatorResponseOnFinish:Z

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->accountAuthenticatorResult(Landroid/os/Bundle;)V

    invoke-virtual {p0, v3, p3}, Lcom/google/android/gms/auth/login/LoginActivity;->finish(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->handleStatus(Lcom/google/android/gms/auth/login/Status;)V

    goto :goto_0

    :sswitch_4
    if-ne p2, v3, :cond_8

    iput-boolean v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mCallAuthenticatorResponseOnFinish:Z

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->accountAuthenticatorResult(Landroid/os/Bundle;)V

    invoke-virtual {p0, v3, p3}, Lcom/google/android/gms/auth/login/LoginActivity;->finish(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_8
    sget-object v1, Lcom/google/android/gms/auth/login/Status;->BAD_AUTHENTICATION:Lcom/google/android/gms/auth/login/Status;

    if-ne v0, v1, :cond_9

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/ShowErrorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x409

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->handleStatus(Lcom/google/android/gms/auth/login/Status;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/GLSUser;->isBrowser()Z

    move-result v0

    if-nez v0, :cond_b

    :cond_a
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x402

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_b
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :sswitch_6
    if-ne p2, v5, :cond_c

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x5

    if-ne p2, v0, :cond_d

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x402

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_d
    if-ne p2, v3, :cond_e

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/LoginActivityTask;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p0, p2}, Lcom/google/android/gms/auth/login/LoginActivity;->finish(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_1
        0x3ea -> :sswitch_4
        0x3ec -> :sswitch_1
        0x3f1 -> :sswitch_6
        0x402 -> :sswitch_0
        0x403 -> :sswitch_2
        0x409 -> :sswitch_5
        0x40c -> :sswitch_3
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/gms/auth/login/Status;->fromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gms/auth/login/Status;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->handleStatus(Lcom/google/android/gms/auth/login/Status;)V

    goto :goto_0
.end method
