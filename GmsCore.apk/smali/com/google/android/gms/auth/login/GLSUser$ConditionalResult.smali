.class Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;
.super Ljava/lang/Object;
.source "GLSUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/login/GLSUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConditionalResult"
.end annotation


# instance fields
.field private final mFailureResponse:Ljava/lang/String;

.field private final mRequiredPairs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSuccessResponse:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;->mRequiredPairs:Ljava/util/Map;

    iput-object p2, p0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;->mSuccessResponse:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;->mFailureResponse:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/login/GLSUser$1;)V
    .locals 0
    .param p1    # Ljava/util/Map;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/auth/login/GLSUser$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;-><init>(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getResponse(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    .locals 7
    .param p1    # Lorg/apache/http/HttpEntity;

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Lorg/apache/http/HttpEntity;)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    new-instance v4, Ljava/util/HashSet;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;->mRequiredPairs:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/NameValuePair;

    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;->mRequiredPairs:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;->mRequiredPairs:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-interface {v1}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;->mFailureResponse:Ljava/lang/String;

    :goto_1
    return-object v5

    :cond_1
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;->mFailureResponse:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;->mSuccessResponse:Ljava/lang/String;

    goto :goto_1

    :catch_0
    move-exception v5

    goto :goto_0
.end method
