.class public Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;
.super Ljava/lang/Object;
.source "GLSUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/login/GLSUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HttpTestInjector"
.end annotation


# instance fields
.field mTestResponse:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;->mTestResponse:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public httpPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/apache/http/HttpEntity;
    .param p3    # Lorg/apache/http/Header;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x1

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;->mTestResponse:Ljava/util/Map;

    invoke-interface {v4, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v4, v0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;

    if-eqz v4, :cond_0

    check-cast v0, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;->getResponse(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    const/4 v3, 0x0

    if-eqz v1, :cond_1

    const-string v4, "GLSUser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Test result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lorg/apache/http/message/BasicHttpResponse;

    new-instance v4, Lorg/apache/http/message/BasicStatusLine;

    new-instance v5, Lorg/apache/http/ProtocolVersion;

    const-string v6, "HTTP"

    invoke-direct {v5, v6, v7, v7}, Lorg/apache/http/ProtocolVersion;-><init>(Ljava/lang/String;II)V

    const/16 v6, 0xc8

    const-string v7, ""

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/message/BasicStatusLine;-><init>(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    invoke-direct {v2, v4}, Lorg/apache/http/message/BasicHttpResponse;-><init>(Lorg/apache/http/StatusLine;)V

    new-instance v4, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v4, v1}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v4}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    :goto_1
    return-object v2

    :cond_0
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public injectConditionalTestResponse(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;->mTestResponse:Ljava/util/Map;

    new-instance v1, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;

    const/4 v2, 0x0

    invoke-direct {v1, p2, p3, p4, v2}, Lcom/google/android/gms/auth/login/GLSUser$ConditionalResult;-><init>(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/login/GLSUser$1;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public injectTestResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;->mTestResponse:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;->mTestResponse:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public reset()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GLSUser$HttpTestInjector;->mTestResponse:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method
