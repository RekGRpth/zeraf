.class Lcom/google/android/gms/recovery/AccountRecoveryActivity$3;
.super Landroid/text/LoginFilter$UsernameFilterGeneric;
.source "AccountRecoveryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/recovery/AccountRecoveryActivity;->initializeListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Z)V
    .locals 0
    .param p2    # Z

    iput-object p1, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$3;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    invoke-direct {p0, p2}, Landroid/text/LoginFilter$UsernameFilterGeneric;-><init>(Z)V

    return-void
.end method


# virtual methods
.method public isAllowed(C)Z
    .locals 3
    .param p1    # C

    const/4 v0, 0x1

    const/16 v1, 0x30

    if-gt v1, p1, :cond_1

    const/16 v1, 0x39

    if-gt p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x61

    if-gt v1, p1, :cond_2

    const/16 v1, 0x7a

    if-le p1, v1, :cond_0

    :cond_2
    const/16 v1, 0x41

    if-gt v1, p1, :cond_3

    const/16 v1, 0x5a

    if-le p1, v1, :cond_0

    :cond_3
    const-string v1, "@_-+.\'"

    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInvalidCharacter(C)V
    .locals 2
    .param p1    # C

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$3;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailError:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->access$502(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Z)Z

    return-void
.end method

.method public onStart()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/recovery/AccountRecoveryActivity$3;->this$0:Lcom/google/android/gms/recovery/AccountRecoveryActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/gms/recovery/AccountRecoveryActivity;->mSecondaryEmailError:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/recovery/AccountRecoveryActivity;->access$502(Lcom/google/android/gms/recovery/AccountRecoveryActivity;Z)Z

    return-void
.end method
