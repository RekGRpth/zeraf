.class public Lcom/google/android/gms/ads/settings/AdsCheckBox;
.super Landroid/widget/LinearLayout;
.source "AdsCheckBox.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/LinearLayout;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Ljava/lang/Boolean;",
        ">;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCaption:Landroid/widget/TextView;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mContext:Landroid/content/Context;

.field private mIsRunning:Z

.field private mNetworkConnectivityReceiver:Landroid/content/BroadcastReceiver;

.field private mProgress:Landroid/widget/ProgressBar;

.field private mRequestQueue:Lcom/android/volley/RequestQueue;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/ads/settings/AdsCheckBox;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mContext:Landroid/content/Context;

    return-void
.end method

.method private isDeviceOnline()Z
    .locals 4

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setVisibleState(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCaption:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCaption:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCaption:Landroid/widget/TextView;

    const v1, 0x7f0b0095

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCaption:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCaption:Landroid/widget/TextView;

    const v1, 0x7f0b0080

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v3}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCaption:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v3}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCaption:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public loadFromNetwork()V
    .locals 3

    iget-boolean v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mIsRunning:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->isDeviceOnline()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setVisibleState(I)V

    new-instance v0, Lcom/google/android/gms/ads/settings/AdPrefsRequest;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mContext:Landroid/content/Context;

    const-string v2, "mobile_view"

    invoke-direct {v0, v1, v2, p0, p0}, Lcom/google/android/gms/ads/settings/AdPrefsRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setVisibleState(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mIsRunning:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setVisibleState(I)V

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    if-ne p1, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    :cond_1
    :goto_1
    new-instance v1, Lcom/google/android/gms/ads/settings/AdPrefsRequest;

    iget-object v3, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_3

    const-string v2, "mobile_optin"

    :goto_2
    invoke-direct {v1, v3, v2, p0, p0}, Lcom/google/android/gms/ads/settings/AdPrefsRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v2, v1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const-string v2, "mobile_optout"

    goto :goto_2
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1    # Lcom/android/volley/VolleyError;

    const/4 v2, 0x2

    iget-boolean v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mIsRunning:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->TAG:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->TAG:Ljava/lang/String;

    const-string v1, "Failed to retrieve server value of personalized ads."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setVisibleState(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 4

    const/4 v3, 0x1

    const v1, 0x7f0a0006

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f0a0012

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    const v1, 0x7f0a0013

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mProgress:Landroid/widget/ProgressBar;

    const v1, 0x7f0a0014

    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCaption:Landroid/widget/TextView;

    invoke-virtual {p0, p0}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/android/volley/toolbox/BasicNetwork;

    new-instance v1, Lcom/google/android/gms/common/server/GmsHttpClientStack;

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/common/server/GmsHttpClientStack;-><init>(Landroid/content/Context;Z)V

    invoke-direct {v0, v1}, Lcom/android/volley/toolbox/BasicNetwork;-><init>(Lcom/android/volley/toolbox/HttpStack;)V

    new-instance v1, Lcom/android/volley/RequestQueue;

    new-instance v2, Lcom/android/volley/toolbox/NoCache;

    invoke-direct {v2}, Lcom/android/volley/toolbox/NoCache;-><init>()V

    invoke-direct {v1, v2, v0, v3}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;I)V

    iput-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mRequestQueue:Lcom/android/volley/RequestQueue;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mIsRunning:Z

    new-instance v1, Lcom/google/android/gms/ads/settings/AdsCheckBox$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/ads/settings/AdsCheckBox$1;-><init>(Lcom/google/android/gms/ads/settings/AdsCheckBox;)V

    iput-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mNetworkConnectivityReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public onResponse(Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;

    iget-boolean v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mIsRunning:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->TAG:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Successful response of server value of personalized ads. (response="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->setVisibleState(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    goto :goto_1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/ads/settings/AdsCheckBox;->onResponse(Ljava/lang/Boolean;)V

    return-void
.end method

.method public start()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1}, Lcom/android/volley/RequestQueue;->start()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mNetworkConnectivityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mIsRunning:Z

    return-void
.end method

.method public stop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0}, Lcom/android/volley/RequestQueue;->stop()V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mNetworkConnectivityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/settings/AdsCheckBox;->mIsRunning:Z

    return-void
.end method
