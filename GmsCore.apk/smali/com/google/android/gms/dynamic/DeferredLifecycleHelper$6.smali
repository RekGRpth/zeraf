.class Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$6;
.super Ljava/lang/Object;
.source "DeferredLifecycleHelper.java"

# interfaces
.implements Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$DeferredStateAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;


# direct methods
.method constructor <init>(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$6;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getState()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public run(Lcom/google/android/gms/dynamic/LifecycleDelegate;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/dynamic/LifecycleDelegate;

    iget-object v0, p0, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper$6;->this$0:Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;

    # getter for: Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->mDelegate:Lcom/google/android/gms/dynamic/LifecycleDelegate;
    invoke-static {v0}, Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;->access$000(Lcom/google/android/gms/dynamic/DeferredLifecycleHelper;)Lcom/google/android/gms/dynamic/LifecycleDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/LifecycleDelegate;->onResume()V

    return-void
.end method
