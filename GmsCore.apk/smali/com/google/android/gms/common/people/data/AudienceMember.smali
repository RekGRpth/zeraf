.class public Lcom/google/android/gms/common/people/data/AudienceMember;
.super Ljava/lang/Object;
.source "AudienceMember.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public static final DOMAIN:Lcom/google/android/gms/common/people/data/AudienceMember;

.field public static final EXTENDED_CIRCLES:Lcom/google/android/gms/common/people/data/AudienceMember;

.field public static final ONLY_YOU:Lcom/google/android/gms/common/people/data/AudienceMember;

.field public static final PUBLIC:Lcom/google/android/gms/common/people/data/AudienceMember;

.field public static final YOUR_CIRCLES:Lcom/google/android/gms/common/people/data/AudienceMember;


# instance fields
.field private final mAvatarUrl:Ljava/lang/String;

.field private final mHashCode:I

.field private final mHidden:Z

.field private final mId:Ljava/lang/String;

.field private final mMemberCount:I

.field private final mName:Ljava/lang/String;

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "PUBLIC"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/people/data/AudienceMember;->PUBLIC:Lcom/google/android/gms/common/people/data/AudienceMember;

    const-string v0, "DASHER_DOMAIN"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/people/data/AudienceMember;->DOMAIN:Lcom/google/android/gms/common/people/data/AudienceMember;

    const-string v0, "YOUR_CIRCLES"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/people/data/AudienceMember;->YOUR_CIRCLES:Lcom/google/android/gms/common/people/data/AudienceMember;

    const-string v0, "EXTENDED_CIRCLES"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/people/data/AudienceMember;->EXTENDED_CIRCLES:Lcom/google/android/gms/common/people/data/AudienceMember;

    const-string v0, "ONLY_YOU"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/people/data/AudienceMember;->ONLY_YOU:Lcom/google/android/gms/common/people/data/AudienceMember;

    new-instance v0, Lcom/google/android/gms/common/people/data/AudienceMember$1;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/data/AudienceMember$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/people/data/AudienceMember;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput p1, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mType:I

    iput-object p3, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mId:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mMemberCount:I

    iput-object p4, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mAvatarUrl:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mHidden:Z

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mHashCode:I

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 7
    .param p1    # Landroid/os/Parcel;

    const/4 v6, 0x1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v6, :cond_0

    :goto_0
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/people/data/AudienceMember;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    return-void

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static forCircle(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->forCircle(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    return-object v0
.end method

.method public static forCircle(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    new-instance v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    const/4 v1, -0x2

    const/4 v4, 0x0

    move-object v2, p0

    move-object v3, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/people/data/AudienceMember;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    return-object v0
.end method

.method public static forDomain(Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 7
    .param p0    # Ljava/lang/String;

    const/4 v1, -0x1

    new-instance v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    const-string v2, "DASHER_DOMAIN"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v3, p0

    move v5, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/people/data/AudienceMember;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    return-object v0
.end method

.method public static forEmail(Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 7
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    const/4 v1, -0x4

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/people/data/AudienceMember;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    return-object v0
.end method

.method public static forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    return-object v0
.end method

.method public static forGroup(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    const/4 v1, -0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v3, p1

    move v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/people/data/AudienceMember;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    return-object v0
.end method

.method public static forPerson(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    const/4 v1, -0x3

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/people/data/AudienceMember;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/gms/common/people/data/AudienceMember;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    iget v3, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mType:I

    iget v4, v0, Lcom/google/android/gms/common/people/data/AudienceMember;->mType:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gms/common/people/data/AudienceMember;->mId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getMemberCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mMemberCount:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mType:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mHashCode:I

    return v0
.end method

.method public isCircle()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mType:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mHidden:Z

    return v0
.end method

.method public isPerson()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mType:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSystemGroup()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mMemberCount:I

    if-ltz v0, :cond_0

    const-string v0, "[%s] %s (%d)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mId:Ljava/lang/String;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mName:Ljava/lang/String;

    aput-object v2, v1, v4

    iget v2, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mMemberCount:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "[%s] %s"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mId:Ljava/lang/String;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mName:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mAvatarUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mMemberCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/data/AudienceMember;->mHidden:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
