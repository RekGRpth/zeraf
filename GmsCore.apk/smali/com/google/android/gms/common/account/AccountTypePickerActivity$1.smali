.class Lcom/google/android/gms/common/account/AccountTypePickerActivity$1;
.super Ljava/lang/Object;
.source "AccountTypePickerActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/common/account/AccountTypePickerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/common/account/AccountTypePickerActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/account/AccountTypePickerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$1;->this$0:Lcom/google/android/gms/common/account/AccountTypePickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$1;->this$0:Lcom/google/android/gms/common/account/AccountTypePickerActivity;

    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$1;->this$0:Lcom/google/android/gms/common/account/AccountTypePickerActivity;

    # getter for: Lcom/google/android/gms/common/account/AccountTypePickerActivity;->mAuthenticatorInfosToDisplay:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/gms/common/account/AccountTypePickerActivity;->access$000(Lcom/google/android/gms/common/account/AccountTypePickerActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;

    iget-object v0, v0, Lcom/google/android/gms/common/account/AccountTypePickerActivity$AuthInfo;->desc:Landroid/accounts/AuthenticatorDescription;

    iget-object v0, v0, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    # invokes: Lcom/google/android/gms/common/account/AccountTypePickerActivity;->setResultAndFinish(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/google/android/gms/common/account/AccountTypePickerActivity;->access$100(Lcom/google/android/gms/common/account/AccountTypePickerActivity;Ljava/lang/String;)V

    return-void
.end method
