.class public final Lcom/google/android/gms/common/ui/SignInButtonCreatorImpl;
.super Lcom/google/android/gms/common/internal/ISignInButtonCreator$Stub;
.source "SignInButtonCreatorImpl.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/ISignInButtonCreator$Stub;-><init>()V

    return-void
.end method

.method private getGmsResources(Landroid/content/Context;)Landroid/content/res/Resources;
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v1, 0x0

    :try_start_0
    const-string v3, "com.google.android.gms"

    const/4 v4, 0x4

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    :cond_0
    move-object v3, v2

    :goto_0
    return-object v3

    :catch_0
    move-exception v0

    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public newSignInButton(Lcom/google/android/gms/dynamic/IObjectWrapper;II)Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 5
    .param p1    # Lcom/google/android/gms/dynamic/IObjectWrapper;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/gms/dynamic/ObjectWrapper;->unwrap(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/ui/SignInButtonCreatorImpl;->getGmsResources(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v3, Landroid/os/RemoteException;

    const-string v4, "Could not load GMS resources!"

    invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    new-instance v2, Lcom/google/android/gms/common/ui/SignInButtonImpl;

    invoke-direct {v2, v0}, Lcom/google/android/gms/common/ui/SignInButtonImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1, p2, p3}, Lcom/google/android/gms/common/ui/SignInButtonImpl;->configure(Landroid/content/res/Resources;II)V

    invoke-static {v2}, Lcom/google/android/gms/dynamic/ObjectWrapper;->wrap(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v3

    return-object v3
.end method
