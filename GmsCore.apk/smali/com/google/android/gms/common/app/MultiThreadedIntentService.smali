.class public abstract Lcom/google/android/gms/common/app/MultiThreadedIntentService;
.super Landroid/app/Service;
.source "MultiThreadedIntentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/app/MultiThreadedIntentService$IntentRunnable;
    }
.end annotation


# instance fields
.field private mFutureList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/concurrent/Future",
            "<*>;>;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mMaxThreads:I

.field private mRedelivery:Z

.field private mThreadPool:Ljava/util/concurrent/ExecutorService;

.field final mWorkDoneRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/app/MultiThreadedIntentService$1;-><init>(Lcom/google/android/gms/common/app/MultiThreadedIntentService;)V

    iput-object v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mWorkDoneRunnable:Ljava/lang/Runnable;

    iput p1, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mMaxThreads:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/common/app/MultiThreadedIntentService;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/app/MultiThreadedIntentService;

    iget-object v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mFutureList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/gms/common/app/MultiThreadedIntentService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/app/MultiThreadedIntentService;

    iget-object v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    iget v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mMaxThreads:I

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mHandler:Landroid/os/Handler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mFutureList:Ljava/util/ArrayList;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method protected abstract onHandleIntent(Landroid/content/Intent;)V
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$IntentRunnable;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/app/MultiThreadedIntentService$IntentRunnable;-><init>(Lcom/google/android/gms/common/app/MultiThreadedIntentService;Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mFutureList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0, p1, p3}, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->onStart(Landroid/content/Intent;I)V

    iget-boolean v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mRedelivery:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
