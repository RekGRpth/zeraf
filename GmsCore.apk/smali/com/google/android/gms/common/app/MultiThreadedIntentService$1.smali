.class Lcom/google/android/gms/common/app/MultiThreadedIntentService$1;
.super Ljava/lang/Object;
.source "MultiThreadedIntentService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/app/MultiThreadedIntentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/common/app/MultiThreadedIntentService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/app/MultiThreadedIntentService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$1;->this$0:Lcom/google/android/gms/common/app/MultiThreadedIntentService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const-string v2, "This runnable can only be called in the Main thread!"

    invoke-static {v2}, Lcom/google/android/gms/common/internal/Preconditions;->checkMainThread(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$1;->this$0:Lcom/google/android/gms/common/app/MultiThreadedIntentService;

    # getter for: Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mFutureList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->access$000(Lcom/google/android/gms/common/app/MultiThreadedIntentService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    invoke-interface {v2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$1;->this$0:Lcom/google/android/gms/common/app/MultiThreadedIntentService;

    invoke-virtual {v2}, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->stopSelf()V

    :cond_2
    return-void
.end method
