.class Lcom/google/android/gms/common/app/MultiThreadedIntentService$IntentRunnable;
.super Ljava/lang/Object;
.source "MultiThreadedIntentService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/app/MultiThreadedIntentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IntentRunnable"
.end annotation


# instance fields
.field private mIntent:Landroid/content/Intent;

.field final synthetic this$0:Lcom/google/android/gms/common/app/MultiThreadedIntentService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/app/MultiThreadedIntentService;Landroid/content/Intent;)V
    .locals 0
    .param p2    # Landroid/content/Intent;

    iput-object p1, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$IntentRunnable;->this$0:Lcom/google/android/gms/common/app/MultiThreadedIntentService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$IntentRunnable;->mIntent:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$IntentRunnable;->this$0:Lcom/google/android/gms/common/app/MultiThreadedIntentService;

    iget-object v1, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$IntentRunnable;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->onHandleIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$IntentRunnable;->this$0:Lcom/google/android/gms/common/app/MultiThreadedIntentService;

    # getter for: Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->access$100(Lcom/google/android/gms/common/app/MultiThreadedIntentService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/app/MultiThreadedIntentService$IntentRunnable;->this$0:Lcom/google/android/gms/common/app/MultiThreadedIntentService;

    iget-object v1, v1, Lcom/google/android/gms/common/app/MultiThreadedIntentService;->mWorkDoneRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
