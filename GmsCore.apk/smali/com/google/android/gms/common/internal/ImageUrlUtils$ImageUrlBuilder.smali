.class public abstract Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;
.super Ljava/lang/Object;
.source "ImageUrlUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/internal/ImageUrlUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ImageUrlBuilder"
.end annotation


# instance fields
.field protected mIsBlackAndWhite:Z

.field protected mSize:I

.field protected mUrl:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;->mSize:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;->mIsBlackAndWhite:Z

    iput-object p1, p0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;->mUrl:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract build()Ljava/lang/String;
.end method

.method public setSize(I)Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;->mSize:I

    return-object p0
.end method

.method public setSize(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;->mSize:I

    return-object p0
.end method
