.class public final Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;
.super Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;
.source "ImageUrlUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/internal/ImageUrlUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProfileImageUrlBuilder"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/ImageUrlUtils$ImageUrlBuilder;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public build()Ljava/lang/String;
    .locals 9

    const/4 v5, 0x0

    const/4 v8, 0x7

    iget-object v6, p0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;->mUrl:Ljava/lang/String;

    if-nez v6, :cond_0

    sget-object v6, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;->TAG:Ljava/lang/String;

    const-string v7, "converting null image URL!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v5

    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;->mUrl:Ljava/lang/String;

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    if-lt v4, v8, :cond_1

    const/16 v6, 0x9

    if-le v4, v6, :cond_2

    :cond_1
    sget-object v6, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;->TAG:Ljava/lang/String;

    const-string v7, "given URL doesn\'t have a valid format!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_4

    if-ge v1, v8, :cond_3

    aget-object v5, v3, v1

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    aget-object v5, v3, v1

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    aget-object v5, v3, v1

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :cond_4
    iget v5, p0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;->mSize:I

    const/4 v6, -0x1

    if-le v5, v6, :cond_5

    if-eqz v0, :cond_7

    const-string v5, ""

    :goto_2
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "s"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;->mSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :cond_5
    iget-boolean v5, p0, Lcom/google/android/gms/common/internal/ImageUrlUtils$ProfileImageUrlBuilder;->mIsBlackAndWhite:Z

    if-eqz v5, :cond_6

    if-eqz v0, :cond_8

    const-string v5, ""

    :goto_3
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "fbw=1"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :cond_6
    if-eqz v0, :cond_9

    const-string v5, ""

    :goto_4
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_7
    const-string v5, "-"

    goto :goto_2

    :cond_8
    const-string v5, "-"

    goto :goto_3

    :cond_9
    const-string v5, "/"

    goto :goto_4
.end method
