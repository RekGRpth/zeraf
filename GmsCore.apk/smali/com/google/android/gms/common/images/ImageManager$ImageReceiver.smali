.class public final Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;
.super Landroid/os/ResultReceiver;
.source "ImageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/images/ImageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "ImageReceiver"
.end annotation


# instance fields
.field private final mListenerHolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final mUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/google/android/gms/common/images/ImageManager;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/images/ImageManager;Landroid/net/Uri;)V
    .locals 2
    .param p2    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->this$0:Lcom/google/android/gms/common/images/ImageManager;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, Landroid/os/ResultReceiver;-><init>(Landroid/os/Handler;)V

    iput-object p2, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->mUri:Landroid/net/Uri;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->mListenerHolderList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public addOnImageLoadedListenerHolder(Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->mListenerHolderList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public onReceiveResult(ILandroid/os/Bundle;)V
    .locals 9
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v1, 0x0

    const-string v6, "com.google.android.gms.extra.fileDescriptor"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/os/ParcelFileDescriptor;

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    :try_start_0
    invoke-virtual {v5}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v6, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->this$0:Lcom/google/android/gms/common/images/ImageManager;

    # getter for: Lcom/google/android/gms/common/images/ImageManager;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/google/android/gms/common/images/ImageManager;->access$300(Lcom/google/android/gms/common/images/ImageManager;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v1, v6, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v6, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->this$0:Lcom/google/android/gms/common/images/ImageManager;

    # getter for: Lcom/google/android/gms/common/images/ImageManager;->mBitmapCache:Lcom/google/android/gms/common/internal/support/LruCache;
    invoke-static {v6}, Lcom/google/android/gms/common/images/ImageManager;->access$400(Lcom/google/android/gms/common/images/ImageManager;)Lcom/google/android/gms/common/internal/support/LruCache;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->mUri:Landroid/net/Uri;

    new-instance v8, Ljava/lang/ref/WeakReference;

    invoke-direct {v8, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v6, v7, v8}, Lcom/google/android/gms/common/internal/support/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->this$0:Lcom/google/android/gms/common/images/ImageManager;

    # getter for: Lcom/google/android/gms/common/images/ImageManager;->mUriMap:Ljava/util/Map;
    invoke-static {v6}, Lcom/google/android/gms/common/images/ImageManager;->access$500(Lcom/google/android/gms/common/images/ImageManager;)Ljava/util/Map;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->mUri:Landroid/net/Uri;

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->mListenerHolderList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_1
    if-ge v3, v4, :cond_1

    iget-object v6, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->mListenerHolderList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;

    iget-object v7, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->mUri:Landroid/net/Uri;

    invoke-virtual {v6, v7, v1}, Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;->onImageLoaded(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    # getter for: Lcom/google/android/gms/common/images/ImageManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/gms/common/images/ImageManager;->access$200()Ljava/lang/String;

    move-result-object v6

    const-string v7, "closed failed"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    return-void
.end method

.method public removeOnImageLoadedListenerHolder(Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/images/ImageManager$ListenerHolder;

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->mListenerHolderList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
