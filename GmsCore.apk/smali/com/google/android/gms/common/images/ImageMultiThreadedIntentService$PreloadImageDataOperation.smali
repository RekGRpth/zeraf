.class final Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;
.super Ljava/lang/Object;
.source "ImageMultiThreadedIntentService.java"

# interfaces
.implements Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$ImageOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PreloadImageDataOperation"
.end annotation


# instance fields
.field private final mContentUri:Landroid/net/Uri;

.field private final mImageUrls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPriority:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/util/ArrayList;I)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;->mContentUri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;->mImageUrls:Ljava/util/ArrayList;

    iput p3, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;->mPriority:I

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/common/images/ImageBroker;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/images/ImageBroker;

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;->mContentUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService$PreloadImageDataOperation;->mImageUrls:Ljava/util/ArrayList;

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/gms/common/images/ImageBroker;->fetchImageData(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;)V

    return-void
.end method
