.class public final Lcom/google/android/gms/common/acl/ScopeData;
.super Ljava/lang/Object;
.source "ScopeData.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/acl/ScopeData$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/common/acl/ScopeData;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMPTY_WARNING_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/login/LsoProto$Warning;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mActivityText:Ljava/lang/String;

.field private mAllCirclesVisible:Z

.field private final mDescription:Ljava/lang/String;

.field private final mDetail:Ljava/lang/String;

.field private final mHasFriendPickerData:Z

.field private final mIcon:Ljava/lang/String;

.field private mPaclPickerData:Ljava/lang/String;

.field private mShowSpeedbump:Z

.field private mVisibleEdges:Ljava/lang/String;

.field private mWarnings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/login/LsoProto$Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/acl/ScopeData;->EMPTY_WARNING_LIST:Ljava/util/List;

    new-instance v0, Lcom/google/android/gms/common/acl/ScopeData$1;

    invoke-direct {v0}, Lcom/google/android/gms/common/acl/ScopeData$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/acl/ScopeData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 7
    .param p1    # Landroid/os/Parcel;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mDescription:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mDetail:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mIcon:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mPaclPickerData:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mHasFriendPickerData:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mVisibleEdges:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mActivityText:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_1
    iput-boolean v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mShowSpeedbump:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2

    :goto_2
    iput-boolean v5, p0, Lcom/google/android/gms/common/acl/ScopeData;->mAllCirclesVisible:Z

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mWarnings:Ljava/util/List;

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_3

    new-instance v2, Lcom/google/android/gms/auth/login/LsoProto$Warning;

    invoke-direct {v2}, Lcom/google/android/gms/auth/login/LsoProto$Warning;-><init>()V

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/auth/login/LsoProto$Warning;->setWarningText(Ljava/lang/String;)Lcom/google/android/gms/auth/login/LsoProto$Warning;

    iget-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mWarnings:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_0
    move v4, v6

    goto :goto_0

    :cond_1
    move v4, v6

    goto :goto_1

    :cond_2
    move v5, v6

    goto :goto_2

    :cond_3
    iget-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mWarnings:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mWarnings:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/gms/common/acl/ScopeData$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/gms/common/acl/ScopeData$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/acl/ScopeData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Z
    .param p9    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/login/LsoProto$Warning;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/acl/ScopeData;->mDescription:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/common/acl/ScopeData;->mDetail:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mIcon:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/common/acl/ScopeData;->mPaclPickerData:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/gms/common/acl/ScopeData;->mHasFriendPickerData:Z

    iput-object p6, p0, Lcom/google/android/gms/common/acl/ScopeData;->mVisibleEdges:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/common/acl/ScopeData;->mActivityText:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/google/android/gms/common/acl/ScopeData;->mShowSpeedbump:Z

    iput-boolean p9, p0, Lcom/google/android/gms/common/acl/ScopeData;->mAllCirclesVisible:Z

    invoke-static {p10}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mWarnings:Ljava/util/List;

    return-void
.end method

.method static synthetic access$100()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/google/android/gms/common/acl/ScopeData;->EMPTY_WARNING_LIST:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getActivityText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mActivityText:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDetail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mDetail:Ljava/lang/String;

    return-object v0
.end method

.method public getPaclPickerData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mPaclPickerData:Ljava/lang/String;

    return-object v0
.end method

.method public getVisibleEdges()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mVisibleEdges:Ljava/lang/String;

    return-object v0
.end method

.method public getWarningText()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/common/acl/ScopeData;->hasWarnings()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mWarnings:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/LsoProto$Warning;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/LsoProto$Warning;->getWarningText()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public hasFriendPickerData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mHasFriendPickerData:Z

    return v0
.end method

.method public hasPaclPickerData()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mPaclPickerData:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWarnings()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mWarnings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAllCirclesVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mAllCirclesVisible:Z

    return v0
.end method

.method public isShowSpeedbump()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/acl/ScopeData;->mShowSpeedbump:Z

    return v0
.end method

.method public setAllCirclesVisible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/common/acl/ScopeData;->mAllCirclesVisible:Z

    return-void
.end method

.method public setPaclPickerData(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/common/acl/ScopeData;->mPaclPickerData:Ljava/lang/String;

    return-void
.end method

.method public setVisibleEdges(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/common/acl/ScopeData;->mVisibleEdges:Ljava/lang/String;

    return-void
.end method

.method public toDebugString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-class v1, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/acl/ScopeData;->mDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lcom/google/android/gms/common/acl/ScopeData;->hasPaclPickerData()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "\n   p_acl_picker_data: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/acl/ScopeData;->mPaclPickerData:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/acl/ScopeData;->hasFriendPickerData()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "\n   visible_edges: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/acl/ScopeData;->mVisibleEdges:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n   show_speedbump: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/common/acl/ScopeData;->mShowSpeedbump:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    const-string v1, "\n   all_circles_visible: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/common/acl/ScopeData;->mAllCirclesVisible:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/acl/ScopeData;->hasWarnings()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "\n   warnings: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/acl/ScopeData;->mWarnings:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mDetail:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mIcon:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mPaclPickerData:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mHasFriendPickerData:Z

    if-eqz v3, :cond_0

    move v3, v4

    :goto_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mVisibleEdges:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mActivityText:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mShowSpeedbump:Z

    if-eqz v3, :cond_1

    move v3, v4

    :goto_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mAllCirclesVisible:Z

    if-eqz v3, :cond_2

    :goto_2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/common/acl/ScopeData;->mWarnings:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/auth/login/LsoProto$Warning;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/LsoProto$Warning;->getWarningText()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_0
    move v3, v5

    goto :goto_0

    :cond_1
    move v3, v5

    goto :goto_1

    :cond_2
    move v4, v5

    goto :goto_2

    :cond_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    return-void
.end method
