.class public Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
.super Ljava/lang/Object;
.source "AclSelectionIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/acl/AclSelectionIntent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;


# direct methods
.method private constructor <init>(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;-><init>(Lcom/google/android/gms/common/acl/AclSelectionIntent$1;)V

    iput-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/ComponentName;Lcom/google/android/gms/common/acl/AclSelectionIntent$1;)V
    .locals 0
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Lcom/google/android/gms/common/acl/AclSelectionIntent$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;-><init>(Landroid/content/ComponentName;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/common/acl/AclSelectionIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;-><init>(Landroid/content/Intent;Lcom/google/android/gms/common/acl/AclSelectionIntent$1;)V

    iput-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/gms/common/acl/AclSelectionIntent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    return-object v0
.end method

.method public setAccountName(Ljava/lang/String;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "ACCOUNT_NAME"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public setAudience(Ljava/util/ArrayList;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "AUDIENCE"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "DESCRIPTION"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public setEveryoneChecked(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "EVERYONE"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setKnownAudienceMembers(Ljava/util/ArrayList;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "KNOWN_AUDIENCE"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public setLoadGroups(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "LOAD_GROUPS"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setLoadPeople(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "LOAD_PEOPLE"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setOkText(Ljava/lang/String;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "OK_TEXT"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public setShowCancel(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "CANCEL_VISIBLE"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setShowChips(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "CHIPS_VISIBLE"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->mIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const-string v1, "TITLE"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method
