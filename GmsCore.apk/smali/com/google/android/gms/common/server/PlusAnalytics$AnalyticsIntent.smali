.class public Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;
.super Ljava/lang/Object;
.source "PlusAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/server/PlusAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AnalyticsIntent"
.end annotation


# instance fields
.field private final mIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.service.default.INTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "isLoggingIntent"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.service.DefaultIntentService"

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;-><init>(Landroid/content/Intent;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    return-void
.end method

.method public static isLoggingIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p0    # Landroid/content/Intent;

    const-string v0, "isLoggingIntent"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getAccountName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAction()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "action"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-object v0
.end method

.method public getDuration()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "duration"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getEndView()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "endView"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getStartView()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getAction()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getEndView()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getEndView()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->getAction()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getLoggedCircles()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "circleTypes"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getStartView()Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "startView"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-object v0
.end method

.method public setAccountName(Ljava/lang/String;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public setAction(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;
    .locals 2
    .param p1    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object p0
.end method

.method public setEndView(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;
    .locals 2
    .param p1    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "endView"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object p0
.end method

.method public setLoggedCircle(Ljava/util/ArrayList;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;",
            ">;)",
            "Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "circleTypes"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public setStartView(Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;
    .locals 2
    .param p1    # Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    iget-object v0, p0, Lcom/google/android/gms/common/server/PlusAnalytics$AnalyticsIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "startView"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object p0
.end method
