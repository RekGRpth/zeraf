.class public final Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;
.super Ljava/lang/Object;
.source "AuthTokenTimeCache.java"


# static fields
.field private static sInstance:Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;


# instance fields
.field private final mTokenTimes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->mTokenTimes:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;
    .locals 2

    const-class v1, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->sInstance:Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->sInstance:Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->sInstance:Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public getTokenTime(Ljava/lang/String;)Ljava/lang/Long;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->mTokenTimes:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->mTokenTimes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updateTokenTime(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->mTokenTimes:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->mTokenTimes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->mTokenTimes:Ljava/util/HashMap;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
