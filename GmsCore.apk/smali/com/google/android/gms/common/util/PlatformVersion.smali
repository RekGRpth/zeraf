.class public final Lcom/google/android/gms/common/util/PlatformVersion;
.super Ljava/lang/Object;
.source "PlatformVersion.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkVersion(I)Z
    .locals 1
    .param p0    # I

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAtLeastHoneycomb()Z
    .locals 1

    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/PlatformVersion;->checkVersion(I)Z

    move-result v0

    return v0
.end method

.method public static isAtLeastHoneycombMR2()Z
    .locals 1

    const/16 v0, 0xd

    invoke-static {v0}, Lcom/google/android/gms/common/util/PlatformVersion;->checkVersion(I)Z

    move-result v0

    return v0
.end method

.method public static isAtLeastIceCreamSandwich()Z
    .locals 1

    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/PlatformVersion;->checkVersion(I)Z

    move-result v0

    return v0
.end method
