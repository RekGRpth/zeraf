.class public final Lcom/google/android/gms/common/analytics/Aspen;
.super Ljava/lang/Object;
.source "Aspen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/analytics/Aspen$1;,
        Lcom/google/android/gms/common/analytics/Aspen$LoggedCircle;,
        Lcom/google/android/gms/common/analytics/Aspen$Action;,
        Lcom/google/android/gms/common/analytics/Aspen$View;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/gms/common/analytics/Aspen;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    return-object v0
.end method

.method private static build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    .locals 2
    .param p0    # I

    new-instance v0, Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const-string v1, "asp"

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method
