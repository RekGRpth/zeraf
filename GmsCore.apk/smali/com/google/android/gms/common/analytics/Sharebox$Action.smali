.class public final Lcom/google/android/gms/common/analytics/Sharebox$Action;
.super Ljava/lang/Object;
.source "Sharebox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/analytics/Sharebox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Action"
.end annotation


# static fields
.field public static final ACL_VIEW_ABANDONED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final ACL_VIEW_ACCEPTED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x40

    # invokes: Lcom/google/android/gms/common/analytics/Sharebox;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Sharebox;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Sharebox$Action;->ACL_VIEW_ABANDONED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x41

    # invokes: Lcom/google/android/gms/common/analytics/Sharebox;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/common/analytics/Sharebox;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/analytics/Sharebox$Action;->ACL_VIEW_ACCEPTED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
