.class public final Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$CorpusConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$CorpusConfig;",
        "Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$CorpusConfig;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$3300()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->create()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$CorpusConfig;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    return-object v0
.end method


# virtual methods
.method public addAllUniversalSectionMappings(Ljava/lang/Iterable;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
            ">;)",
            "Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3602(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addSections(Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3502(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->build()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSections(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$SectionConfig;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3502(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addUniversalSectionMappings(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3602(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->build()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$CorpusConfig;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    iget-object v2, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3502(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    iget-object v2, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3602(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    return-object v0
.end method

.method public clearUniversalSectionMappings()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3602(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;

    return-object p0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->create()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->clone()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->clone()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->clone()Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getContentProviderUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getContentProviderUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSections(I)Lcom/google/android/gms/icing/Proto$SectionConfig;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSections(I)Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v0

    return-object v0
.end method

.method public getSectionsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getSectionsCount()I

    move-result v0

    return v0
.end method

.method public getUniversalSectionMappingsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasName()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasName()Z

    move-result v0

    return v0
.end method

.method public hasPackageName()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasPackageName()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusConfig;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$CorpusConfig;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setId(I)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasName()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasVersion()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getVersion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setVersion(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasPackageName()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setPackageName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasType()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getType()Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setType(Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasContentProviderUri()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getContentProviderUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setContentProviderUri(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasQueryLimit()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->getQueryLimit()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setQueryLimit(I)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    :cond_8
    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3502(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->sections_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3500(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_a
    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3602(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/util/List;)Ljava/util/List;

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->universalSectionMappings_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3600(Lcom/google/android/gms/icing/Proto$CorpusConfig;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v2}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setPackageName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;->valueOf(I)Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setType(Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setContentProviderUri(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setQueryLimit(I)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    goto :goto_0

    :sswitch_6
    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig;->newBuilder()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->addSections(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    goto :goto_0

    :sswitch_7
    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->addUniversalSectionMappings(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setId(I)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->setVersion(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setContentProviderUri(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasContentProviderUri:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$4702(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->contentProviderUri_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$4802(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setId(I)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3702(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->id_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3802(Lcom/google/android/gms/icing/Proto$CorpusConfig;I)I

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$3902(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$4002(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setPackageName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasPackageName:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$4302(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->packageName_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$4402(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setQueryLimit(I)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasQueryLimit:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$4902(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->queryLimit_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$5002(Lcom/google/android/gms/icing/Proto$CorpusConfig;I)I

    return-object p0
.end method

.method public setType(Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasType:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$4502(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->type_:Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$4602(Lcom/google/android/gms/icing/Proto$CorpusConfig;Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Type;

    return-object p0
.end method

.method public setVersion(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->hasVersion:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$4102(Lcom/google/android/gms/icing/Proto$CorpusConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$CorpusConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$CorpusConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$CorpusConfig;->version_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$CorpusConfig;->access$4202(Lcom/google/android/gms/icing/Proto$CorpusConfig;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
