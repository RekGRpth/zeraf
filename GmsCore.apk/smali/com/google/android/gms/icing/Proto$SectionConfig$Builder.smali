.class public final Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$SectionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$SectionConfig;",
        "Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$SectionConfig;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->create()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$SectionConfig;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$SectionConfig;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/gms/icing/Proto$SectionConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->build()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$SectionConfig;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->create()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->clone()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->clone()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->clone()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 1
    .param p1    # Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasIndexed()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getIndexed()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setIndexed(Z)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasTokenizer()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getTokenizer()Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setTokenizer(Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasWeight()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getWeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setWeight(I)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setIndexed(Z)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->valueOf(I)Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setTokenizer(Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->setWeight(I)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setIndexed(Z)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$SectionConfig;->hasIndexed:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->access$502(Lcom/google/android/gms/icing/Proto$SectionConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$SectionConfig;->indexed_:Z
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->access$602(Lcom/google/android/gms/icing/Proto$SectionConfig;Z)Z

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$SectionConfig;->hasName:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->access$302(Lcom/google/android/gms/icing/Proto$SectionConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$SectionConfig;->name_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->access$402(Lcom/google/android/gms/icing/Proto$SectionConfig;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setTokenizer(Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$SectionConfig;->hasTokenizer:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->access$702(Lcom/google/android/gms/icing/Proto$SectionConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$SectionConfig;->tokenizer_:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->access$802(Lcom/google/android/gms/icing/Proto$SectionConfig;Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;)Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    return-object p0
.end method

.method public setWeight(I)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$SectionConfig;->hasWeight:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->access$902(Lcom/google/android/gms/icing/Proto$SectionConfig;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->result:Lcom/google/android/gms/icing/Proto$SectionConfig;

    # setter for: Lcom/google/android/gms/icing/Proto$SectionConfig;->weight_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->access$1002(Lcom/google/android/gms/icing/Proto$SectionConfig;I)I

    return-object p0
.end method
