.class public final Lcom/google/android/gms/icing/Proto$Enums$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$Enums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$Enums;",
        "Lcom/google/android/gms/icing/Proto$Enums$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$Enums;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$10500()Lcom/google/android/gms/icing/Proto$Enums$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->create()Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$Enums$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$Enums$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$Enums;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$Enums;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$Enums$Builder;->result:Lcom/google/android/gms/icing/Proto$Enums;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/gms/icing/Proto$Enums;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Enums$Builder;->result:Lcom/google/android/gms/icing/Proto$Enums;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Enums$Builder;->result:Lcom/google/android/gms/icing/Proto$Enums;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$Enums;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->build()Lcom/google/android/gms/icing/Proto$Enums;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$Enums;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$Enums$Builder;->result:Lcom/google/android/gms/icing/Proto$Enums;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Enums$Builder;->result:Lcom/google/android/gms/icing/Proto$Enums;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$Enums$Builder;->result:Lcom/google/android/gms/icing/Proto$Enums;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$Enums$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->create()Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$Enums$Builder;->result:Lcom/google/android/gms/icing/Proto$Enums;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$Enums;)Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->clone()Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->clone()Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->clone()Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Enums$Builder;->result:Lcom/google/android/gms/icing/Proto$Enums;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$Enums;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$Enums;)Lcom/google/android/gms/icing/Proto$Enums$Builder;
    .locals 1
    .param p1    # Lcom/google/android/gms/icing/Proto$Enums;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Enums;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$Enums;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :cond_0
    return-object p0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$Enums$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    :pswitch_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    return-object v0
.end method
