.class public final Lcom/google/android/gms/icing/Proto$Document;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Document"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$Document$Builder;,
        Lcom/google/android/gms/icing/Proto$Document$Section;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$Document;


# instance fields
.field private corpusId_:I

.field private hasCorpusId:Z

.field private hasScore:Z

.field private hasUri:Z

.field private memoizedSerializedSize:I

.field private score_:I

.field private sections_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$Document$Section;",
            ">;"
        }
    .end annotation
.end field

.field private uri_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$Document;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$Document;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Document;->defaultInstance:Lcom/google/android/gms/icing/Proto$Document;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$Document;->defaultInstance:Lcom/google/android/gms/icing/Proto$Document;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$Document;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document;->corpusId_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$Document;->uri_:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document;->score_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$Document;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$Document;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document;->corpusId_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$Document;->uri_:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document;->score_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Document;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$12300(Lcom/google/android/gms/icing/Proto$Document;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$Document;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$12302(Lcom/google/android/gms/icing/Proto$Document;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$12402(Lcom/google/android/gms/icing/Proto$Document;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$Document;->hasCorpusId:Z

    return p1
.end method

.method static synthetic access$12502(Lcom/google/android/gms/icing/Proto$Document;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$Document;->corpusId_:I

    return p1
.end method

.method static synthetic access$12602(Lcom/google/android/gms/icing/Proto$Document;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$Document;->hasUri:Z

    return p1
.end method

.method static synthetic access$12702(Lcom/google/android/gms/icing/Proto$Document;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$Document;->uri_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$12802(Lcom/google/android/gms/icing/Proto$Document;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$Document;->hasScore:Z

    return p1
.end method

.method static synthetic access$12902(Lcom/google/android/gms/icing/Proto$Document;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$Document;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$Document;->score_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$Document;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$Document;->defaultInstance:Lcom/google/android/gms/icing/Proto$Document;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$Document$Builder;->create()Lcom/google/android/gms/icing/Proto$Document$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document$Builder;->access$12100()Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCorpusId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$Document;->corpusId_:I

    return v0
.end method

.method public getScore()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$Document;->score_:I

    return v0
.end method

.method public getSectionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$Document$Section;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document;->sections_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    iget v2, p0, Lcom/google/android/gms/icing/Proto$Document;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->hasCorpusId()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->getCorpusId()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->hasUri()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->getUri()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->hasScore()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->getScore()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->getSectionsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v4, 0x4

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_4
    iput v2, p0, Lcom/google/android/gms/icing/Proto$Document;->memoizedSerializedSize:I

    move v3, v2

    goto :goto_0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document;->uri_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCorpusId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$Document;->hasCorpusId:Z

    return v0
.end method

.method public hasScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$Document;->hasScore:Z

    return v0
.end method

.method public hasUri()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$Document;->hasUri:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$Document$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document;->newBuilder()Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->newBuilderForType()Lcom/google/android/gms/icing/Proto$Document$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->hasCorpusId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->getCorpusId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->hasUri()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->hasScore()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->getScore()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document;->getSectionsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    :cond_3
    return-void
.end method
