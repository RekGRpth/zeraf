.class public Lcom/google/android/gms/icing/proxy/ContactsProxy;
.super Landroid/app/Service;
.source "ContactsProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/proxy/ContactsProxy$LocalBinder;
    }
.end annotation


# static fields
.field private static final CONTACT_CP_AUTHORITY:Landroid/net/Uri;


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mConnection:Landroid/content/ServiceConnection;

.field mSearch:Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;

.field private mSearchIsBound:Z

.field private mTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://com.google.android.gms.icing.proxy.ContactsIndexingContentProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->CONTACT_CP_AUTHORITY:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mTimer:Ljava/util/Timer;

    new-instance v0, Lcom/google/android/gms/icing/proxy/ContactsProxy$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/proxy/ContactsProxy$1;-><init>(Lcom/google/android/gms/icing/proxy/ContactsProxy;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/google/android/gms/icing/proxy/ContactsProxy$LocalBinder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/proxy/ContactsProxy$LocalBinder;-><init>(Lcom/google/android/gms/icing/proxy/ContactsProxy;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mBinder:Landroid/os/IBinder;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/gms/icing/proxy/ContactsProxy;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/proxy/ContactsProxy;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearchIsBound:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/gms/icing/proxy/ContactsProxy;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/proxy/ContactsProxy;

    invoke-direct {p0}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->registerCorpus()V

    return-void
.end method

.method static synthetic access$200()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->CONTACT_CP_AUTHORITY:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/gms/icing/proxy/ContactsProxy;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/proxy/ContactsProxy;

    invoke-direct {p0}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->timerRun()V

    return-void
.end method

.method private bindToIcing()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.icing.INDEX_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method private registerCorpus()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/gms/icing/proxy/ContactsProxy$2;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/proxy/ContactsProxy$2;-><init>(Lcom/google/android/gms/icing/proxy/ContactsProxy;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private timerRun()V
    .locals 10

    iget-boolean v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearchIsBound:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearch:Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;

    if-eqz v0, :cond_0

    const-string v0, "Contacts proxy requesting index"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearch:Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;

    invoke-virtual {p0}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "contacts"

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;->getCorpusStatus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;

    move-result-object v6

    iget-wide v8, v6, Lcom/google/android/gms/appdatasearch/CorpusStatus;->lastIndexedSeqno:J

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearch:Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;

    invoke-virtual {p0}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "contacts"

    const-wide/16 v3, 0x1

    add-long/2addr v3, v8

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/appdatasearch/internal/IAppDataSearch;->requestIndexing(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/appdatasearch/RequestIndexingSpecification;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v7

    const-string v0, "Contacts proxy error contacting icing"

    invoke-static {v0, v7}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    :cond_0
    const-string v0, "Indexer is not bound to contacts proxy"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    const-string v0, "Stopping contacts proxy"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-boolean v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearchIsBound:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mSearchIsBound:Z

    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v0, "Starting contacts proxy"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/icing/proxy/ContactsProxy;->bindToIcing()V

    iget-object v0, p0, Lcom/google/android/gms/icing/proxy/ContactsProxy;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/google/android/gms/icing/proxy/ContactsProxy$3;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/proxy/ContactsProxy$3;-><init>(Lcom/google/android/gms/icing/proxy/ContactsProxy;)V

    const-wide/16 v2, 0x2710

    const-wide/32 v4, 0x493e0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    const/4 v0, 0x1

    return v0
.end method
