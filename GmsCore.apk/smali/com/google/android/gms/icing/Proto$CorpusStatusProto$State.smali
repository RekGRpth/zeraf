.class public final enum Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;
.super Ljava/lang/Enum;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$CorpusStatusProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

.field public static final enum ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

.field public static final enum INACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

.field public static final enum LIMBO:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    const-string v1, "LIMBO"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->LIMBO:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->INACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    sget-object v1, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->LIMBO:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->INACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->$VALUES:[Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    new-instance v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State$1;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->index:I

    iput p4, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->ACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->LIMBO:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->INACTIVE:Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;
    .locals 1

    const-class v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->$VALUES:[Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    invoke-virtual {v0}, [Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$CorpusStatusProto$State;->value:I

    return v0
.end method
