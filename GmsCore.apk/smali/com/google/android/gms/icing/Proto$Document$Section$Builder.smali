.class public final Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$Document$Section;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$Document$Section;",
        "Lcom/google/android/gms/icing/Proto$Document$Section$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$Document$Section;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$10800()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->create()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$Document$Section;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/gms/icing/Proto$Document$Section;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$Document$Section;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->build()Lcom/google/android/gms/icing/Proto$Document$Section;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$Document$Section;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->create()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$Document$Section;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->clone()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->clone()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->clone()Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getConfig()Lcom/google/android/gms/icing/Proto$SectionConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$Document$Section;->getConfig()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v0

    return-object v0
.end method

.method public hasConfig()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasConfig()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$Document$Section;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeConfig(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$SectionConfig;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasConfig()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    # getter for: Lcom/google/android/gms/icing/Proto$Document$Section;->config_:Lcom/google/android/gms/icing/Proto$SectionConfig;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11900(Lcom/google/android/gms/icing/Proto$Document$Section;)Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    # getter for: Lcom/google/android/gms/icing/Proto$Document$Section;->config_:Lcom/google/android/gms/icing/Proto$SectionConfig;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11900(Lcom/google/android/gms/icing/Proto$Document$Section;)Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$SectionConfig;->newBuilder(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v1

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->config_:Lcom/google/android/gms/icing/Proto$SectionConfig;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11902(Lcom/google/android/gms/icing/Proto$Document$Section;Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->hasConfig:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11802(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->config_:Lcom/google/android/gms/icing/Proto$SectionConfig;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11902(Lcom/google/android/gms/icing/Proto$Document$Section;Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$Document$Section;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 1
    .param p1    # Lcom/google/android/gms/icing/Proto$Document$Section;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Document$Section;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$Document$Section;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setId(I)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasIndexedLength()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->getIndexedLength()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setIndexedLength(I)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasContent()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->getContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setContent(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasSnippet()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->getSnippet()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setSnippet(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->hasConfig()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->getConfig()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->mergeConfig(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setId(I)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setIndexedLength(I)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setContent(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setSnippet(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig;->newBuilder()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->hasConfig()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->getConfig()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$SectionConfig;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->setConfig(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setConfig(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$SectionConfig;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->hasConfig:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11802(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->config_:Lcom/google/android/gms/icing/Proto$SectionConfig;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11902(Lcom/google/android/gms/icing/Proto$Document$Section;Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig;

    return-object p0
.end method

.method public setContent(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->hasContent:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11402(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->content_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11502(Lcom/google/android/gms/icing/Proto$Document$Section;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setId(I)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11002(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->id_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11102(Lcom/google/android/gms/icing/Proto$Document$Section;I)I

    return-object p0
.end method

.method public setIndexedLength(I)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->hasIndexedLength:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11202(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->indexedLength_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11302(Lcom/google/android/gms/icing/Proto$Document$Section;I)I

    return-object p0
.end method

.method public setSnippet(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Document$Section$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->hasSnippet:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11602(Lcom/google/android/gms/icing/Proto$Document$Section;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$Document$Section$Builder;->result:Lcom/google/android/gms/icing/Proto$Document$Section;

    # setter for: Lcom/google/android/gms/icing/Proto$Document$Section;->snippet_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$Document$Section;->access$11702(Lcom/google/android/gms/icing/Proto$Document$Section;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method
