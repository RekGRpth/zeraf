.class public final enum Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;
.super Ljava/lang/Enum;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$Enums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NativeIndexResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

.field public static final enum NATIVE_INDEX_RESULT_DOCUMENT_TRIMMED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

.field public static final enum NATIVE_INDEX_RESULT_ERROR_DOCSTORE:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

.field public static final enum NATIVE_INDEX_RESULT_ERROR_INDEX_LEXICON_FULL:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

.field public static final enum NATIVE_INDEX_RESULT_ERROR_URI_NOT_FOUND:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

.field public static final enum NATIVE_INDEX_RESULT_INDEX_NEED_FLUSH:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

.field public static final enum NATIVE_INDEX_RESULT_INDEX_TOKENS_CLIPPED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

.field public static final enum NATIVE_INDEX_RESULT_OK:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

.field public static final enum NATIVE_INDEX_RESULT_PROTO_PARSE_ERROR:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

.field public static final enum NATIVE_INDEX_RESULT_URI_REPLACED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    const-string v1, "NATIVE_INDEX_RESULT_OK"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_OK:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    const-string v1, "NATIVE_INDEX_RESULT_DOCUMENT_TRIMMED"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_DOCUMENT_TRIMMED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    const-string v1, "NATIVE_INDEX_RESULT_INDEX_TOKENS_CLIPPED"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_INDEX_TOKENS_CLIPPED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    const-string v1, "NATIVE_INDEX_RESULT_URI_REPLACED"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_URI_REPLACED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    const-string v1, "NATIVE_INDEX_RESULT_PROTO_PARSE_ERROR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_PROTO_PARSE_ERROR:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    const-string v1, "NATIVE_INDEX_RESULT_ERROR_URI_NOT_FOUND"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/16 v4, 0x14

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_ERROR_URI_NOT_FOUND:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    const-string v1, "NATIVE_INDEX_RESULT_ERROR_DOCSTORE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/16 v4, 0x15

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_ERROR_DOCSTORE:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    const-string v1, "NATIVE_INDEX_RESULT_INDEX_NEED_FLUSH"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/16 v4, 0x1e

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_INDEX_NEED_FLUSH:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    const-string v1, "NATIVE_INDEX_RESULT_ERROR_INDEX_LEXICON_FULL"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x1f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_ERROR_INDEX_LEXICON_FULL:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    sget-object v1, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_OK:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_DOCUMENT_TRIMMED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_INDEX_TOKENS_CLIPPED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_URI_REPLACED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_PROTO_PARSE_ERROR:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_ERROR_URI_NOT_FOUND:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_ERROR_DOCSTORE:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_INDEX_NEED_FLUSH:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_ERROR_INDEX_LEXICON_FULL:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->$VALUES:[Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult$1;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->index:I

    iput p4, p0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;
    .locals 1
    .param p0    # I

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_OK:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_DOCUMENT_TRIMMED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    goto :goto_0

    :sswitch_2
    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_INDEX_TOKENS_CLIPPED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    goto :goto_0

    :sswitch_3
    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_URI_REPLACED:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    goto :goto_0

    :sswitch_4
    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_PROTO_PARSE_ERROR:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    goto :goto_0

    :sswitch_5
    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_ERROR_URI_NOT_FOUND:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    goto :goto_0

    :sswitch_6
    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_ERROR_DOCSTORE:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    goto :goto_0

    :sswitch_7
    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_INDEX_NEED_FLUSH:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    goto :goto_0

    :sswitch_8
    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->NATIVE_INDEX_RESULT_ERROR_INDEX_LEXICON_FULL:Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0xa -> :sswitch_4
        0x14 -> :sswitch_5
        0x15 -> :sswitch_6
        0x1e -> :sswitch_7
        0x1f -> :sswitch_8
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;
    .locals 1

    const-class v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->$VALUES:[Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    invoke-virtual {v0}, [Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;

    return-object v0
.end method
