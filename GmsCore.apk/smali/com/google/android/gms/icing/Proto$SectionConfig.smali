.class public final Lcom/google/android/gms/icing/Proto$SectionConfig;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SectionConfig"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;,
        Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$SectionConfig;


# instance fields
.field private hasIndexed:Z

.field private hasName:Z

.field private hasTokenizer:Z

.field private hasWeight:Z

.field private indexed_:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private tokenizer_:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

.field private weight_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$SectionConfig;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$SectionConfig;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$SectionConfig;->defaultInstance:Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$SectionConfig;->defaultInstance:Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->name_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->indexed_:Z

    iput v1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->weight_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->name_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->indexed_:Z

    iput v1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->weight_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1002(Lcom/google/android/gms/icing/Proto$SectionConfig;I)I
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$SectionConfig;
    .param p1    # I

    iput p1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->weight_:I

    return p1
.end method

.method static synthetic access$302(Lcom/google/android/gms/icing/Proto$SectionConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$SectionConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasName:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/gms/icing/Proto$SectionConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$SectionConfig;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/gms/icing/Proto$SectionConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$SectionConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasIndexed:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/gms/icing/Proto$SectionConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$SectionConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->indexed_:Z

    return p1
.end method

.method static synthetic access$702(Lcom/google/android/gms/icing/Proto$SectionConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$SectionConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasTokenizer:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/android/gms/icing/Proto$SectionConfig;Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;)Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$SectionConfig;
    .param p1    # Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    iput-object p1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->tokenizer_:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/android/gms/icing/Proto$SectionConfig;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/Proto$SectionConfig;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasWeight:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$SectionConfig;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$SectionConfig;->defaultInstance:Lcom/google/android/gms/icing/Proto$SectionConfig;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->TOKENIZER_TEXT:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    iput-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->tokenizer_:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->create()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->access$100()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/Proto$SectionConfig;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig;->newBuilder()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$SectionConfig;)Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getIndexed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->indexed_:Z

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasIndexed()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getIndexed()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasTokenizer()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getTokenizer()Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasWeight()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getWeight()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iput v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getTokenizer()Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->tokenizer_:Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    return-object v0
.end method

.method public getWeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->weight_:I

    return v0
.end method

.method public hasIndexed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasIndexed:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasName:Z

    return v0
.end method

.method public hasTokenizer()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasTokenizer:Z

    return v0
.end method

.method public hasWeight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasWeight:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$SectionConfig;->newBuilder()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->newBuilderForType()Lcom/google/android/gms/icing/Proto$SectionConfig$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasIndexed()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getIndexed()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasTokenizer()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getTokenizer()Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/Proto$SectionConfig$Tokenizer;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->hasWeight()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$SectionConfig;->getWeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_3
    return-void
.end method
