.class public final Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$QueryRequestSpec;",
        "Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$16600()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    return-object v0
.end method


# virtual methods
.method public addCorpusSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addCorpusSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSectionMapping(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16902(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSectionMapping(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16902(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSectionWeight(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17002(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSectionWeight(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17002(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    iget-object v2, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    iget-object v2, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16902(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    iget-object v2, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17002(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->create()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->clone()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->corpusSpec_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16800(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasNoCorpusFilter()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getNoCorpusFilter()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setNoCorpusFilter(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    :cond_4
    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16902(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionMapping_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$16900(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasWantUris()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getWantUris()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setWantUris(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    :cond_7
    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17002(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Ljava/util/List;)Ljava/util/List;

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->sectionWeight_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17000(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_9
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasVerboseScoring()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getVerboseScoring()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setVerboseScoring(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    :cond_a
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasUniversalSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->getUniversalSearch()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setUniversalSearch(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto/16 :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->addCorpusSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$CorpusSpec;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setNoCorpusFilter(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->addSectionMapping(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionMapping;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setWantUris(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->addSectionWeight(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$SectionWeight;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setVerboseScoring(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setUniversalSearch(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setNoCorpusFilter(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasNoCorpusFilter:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17102(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->noCorpusFilter_:Z
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17202(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z

    return-object p0
.end method

.method public setUniversalSearch(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasUniversalSearch:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17702(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->universalSearch_:Z
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17802(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z

    return-object p0
.end method

.method public setVerboseScoring(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasVerboseScoring:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17502(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->verboseScoring_:Z
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17602(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z

    return-object p0
.end method

.method public setWantUris(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->hasWantUris:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17302(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->result:Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    # setter for: Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->wantUris_:Z
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->access$17402(Lcom/google/android/gms/icing/Proto$QueryRequestSpec;Z)Z

    return-object p0
.end method
