.class public final Lcom/google/android/gms/icing/Proto$Enums;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Enums"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/Proto$Enums$Builder;,
        Lcom/google/android/gms/icing/Proto$Enums$NativeIndexResult;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/gms/icing/Proto$Enums;


# instance fields
.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/icing/Proto$Enums;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/Proto$Enums;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/icing/Proto$Enums;->defaultInstance:Lcom/google/android/gms/icing/Proto$Enums;

    invoke-static {}, Lcom/google/android/gms/icing/Proto;->internalForceInit()V

    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums;->defaultInstance:Lcom/google/android/gms/icing/Proto$Enums;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$Enums;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Enums;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$Enums;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/icing/Proto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/icing/Proto$1;

    invoke-direct {p0}, Lcom/google/android/gms/icing/Proto$Enums;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Enums;->memoizedSerializedSize:I

    return-void
.end method

.method public static getDefaultInstance()Lcom/google/android/gms/icing/Proto$Enums;
    .locals 1

    sget-object v0, Lcom/google/android/gms/icing/Proto$Enums;->defaultInstance:Lcom/google/android/gms/icing/Proto$Enums;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/icing/Proto$Enums$Builder;
    .locals 1

    # invokes: Lcom/google/android/gms/icing/Proto$Enums$Builder;->create()Lcom/google/android/gms/icing/Proto$Enums$Builder;
    invoke-static {}, Lcom/google/android/gms/icing/Proto$Enums$Builder;->access$10500()Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    iget v0, p0, Lcom/google/android/gms/icing/Proto$Enums;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/Proto$Enums;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/android/gms/icing/Proto$Enums$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$Enums;->newBuilder()Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Enums;->newBuilderForType()Lcom/google/android/gms/icing/Proto$Enums$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 0
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$Enums;->getSerializedSize()I

    return-void
.end method
