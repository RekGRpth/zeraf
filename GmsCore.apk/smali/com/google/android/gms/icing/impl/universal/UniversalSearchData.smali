.class public Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;
.super Ljava/lang/Object;
.source "UniversalSearchData.java"


# instance fields
.field private final mActiveCorporaData:[Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$CorpusConfig;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    iput-object v4, p0, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->mActiveCorporaData:[Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/Proto$CorpusConfig;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->mActiveCorporaData:[Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    add-int/lit8 v3, v2, 0x1

    new-instance v5, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    invoke-direct {v5, v0}, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;-><init>(Lcom/google/android/gms/icing/Proto$CorpusConfig;)V

    aput-object v5, v4, v2

    move v2, v3

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static createSectionMap(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/util/Map;
    .locals 4
    .param p0    # Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->sections:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->sections:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->sections:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->name:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static getUniversalSectionMappings(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/util/List;
    .locals 6
    .param p0    # Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->universalSearchConfig:Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->universalSearchConfig:Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;

    invoke-virtual {v5}, Lcom/google/android/gms/appdatasearch/UniversalSearchCorpusConfig;->getUniversalSearchMappings()[Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->createSectionMap(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/util/Map;

    move-result-object v1

    const/4 v4, 0x0

    :goto_0
    invoke-static {}, Lcom/google/android/gms/appdatasearch/UniversalSearchSections;->getSectionsCount()I

    move-result v5

    if-ge v4, v5, :cond_1

    aget-object v5, v2, v4

    if-eqz v5, :cond_0

    aget-object v5, v2, v4

    invoke-static {v1, v5}, Lcom/google/android/gms/icing/impl/universal/Template;->parse(Ljava/util/Map;Ljava/lang/String;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;->newBuilder()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->setUniversalSectionId(I)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->setValueTemplate(Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Template;)Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/Proto$UniversalSectionMapping$Builder;->build()Lcom/google/android/gms/icing/Proto$UniversalSectionMapping;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static validateUniversalSectionMappings([Ljava/lang/String;)V
    .locals 6
    .param p0    # [Ljava/lang/String;

    if-eqz p0, :cond_1

    move-object v0, p0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    if-eqz v4, :cond_0

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/universal/Template;->validateTemplate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-direct {v5, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public createResults(Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 10
    .param p1    # Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->mActiveCorporaData:[Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    array-length v8, v8

    new-array v3, v8, [Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->mActiveCorporaData:[Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    array-length v8, v8

    new-array v6, v8, [Landroid/os/Bundle;

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->mActiveCorporaData:[Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    array-length v8, v8

    new-array v5, v8, [Landroid/os/Bundle;

    new-instance v1, Landroid/util/SparseIntArray;

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->mActiveCorporaData:[Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    array-length v8, v8

    invoke-direct {v1, v8}, Landroid/util/SparseIntArray;-><init>(I)V

    const/4 v4, 0x0

    :goto_0
    iget-object v8, p0, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->mActiveCorporaData:[Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    array-length v8, v8

    if-ge v4, v8, :cond_0

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->mActiveCorporaData:[Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    aget-object v0, v8, v4

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->getCorpusName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v4

    iget-object v8, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpora:[Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;

    aget-object v8, v8, v4

    iget v9, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    invoke-virtual {v0, v8, v9}, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->getSections(Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse$Corpus;I)Landroid/util/Pair;

    move-result-object v7

    iget-object v8, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Landroid/os/Bundle;

    aput-object v8, v6, v4

    iget-object v8, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Landroid/os/Bundle;

    aput-object v8, v5, v4

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->getCorpusId()I

    move-result v8

    invoke-virtual {v1, v8, v4}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mCorpusIds:[I

    const/4 v4, 0x0

    :goto_1
    iget v8, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    if-ge v4, v8, :cond_1

    aget v8, v2, v4

    invoke-virtual {v1, v8}, Landroid/util/SparseIntArray;->get(I)I

    move-result v8

    aput v8, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    iget v8, p1, Lcom/google/android/gms/icing/impl/NativeIndex$QueryResponse;->mNumResults:I

    invoke-static {v8, v2, v3, v6, v5}, Lcom/google/android/gms/appdatasearch/SafeParcelablesCreator;->createSearchResultsForUniversalSearch(I[I[Ljava/lang/String;[Landroid/os/Bundle;[Landroid/os/Bundle;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v8

    return-object v8
.end method

.method public getRequestSpec()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;
    .locals 6

    const/4 v5, 0x1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec;->newBuilder()Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setUniversalSearch(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->setNoCorpusFilter(Z)Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/universal/UniversalSearchData;->mActiveCorporaData:[Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    invoke-virtual {v1, v4}, Lcom/google/android/gms/icing/impl/universal/ActiveCorpusUniversalQueryData;->addCorpusSpec(Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Lcom/google/android/gms/icing/Proto$QueryRequestSpec$Builder;->build()Lcom/google/android/gms/icing/Proto$QueryRequestSpec;

    move-result-object v5

    return-object v5
.end method
