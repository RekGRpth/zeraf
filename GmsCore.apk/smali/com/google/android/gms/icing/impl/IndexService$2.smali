.class Lcom/google/android/gms/icing/impl/IndexService$2;
.super Ljava/lang/Object;
.source "IndexService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/impl/IndexService;->startMaintenanceAlarm()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/IndexService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/IndexService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "gms_icing_maintenance_frequency_days"

    const-wide/16 v4, 0x1

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    # setter for: Lcom/google/android/gms/icing/impl/IndexService;->mMaintenanceFrequencyDays:J
    invoke-static {v1, v2, v3}, Lcom/google/android/gms/icing/impl/IndexService;->access$002(Lcom/google/android/gms/icing/impl/IndexService;J)J

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/IndexService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "gms_icing_maintenance_hour_of_day"

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/google/android/gms/icing/impl/IndexService;->mMaintenanceHourOfDay:I
    invoke-static {v1, v2}, Lcom/google/android/gms/icing/impl/IndexService;->access$102(Lcom/google/android/gms/icing/impl/IndexService;I)I

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    const-string v2, "alarm"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/IndexService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/IndexService;->getMaintenanceTriggerMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/IndexService;->getMaintenanceIntervalMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    const-string v7, "com.google.android.gms.icing.INDEX_RECURRING_MAINTENANCE"

    # invokes: Lcom/google/android/gms/icing/impl/IndexService;->makeMaintenanceIntent(Ljava/lang/String;)Landroid/app/PendingIntent;
    invoke-static {v6, v7}, Lcom/google/android/gms/icing/impl/IndexService;->access$200(Lcom/google/android/gms/icing/impl/IndexService;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/IndexService;->mKeepAwake:Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->release()V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/IndexService$2;->this$0:Lcom/google/android/gms/icing/impl/IndexService;

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/IndexService;->mKeepAwake:Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/IndexService$ServiceKeepAwake;->release()V

    throw v1
.end method
