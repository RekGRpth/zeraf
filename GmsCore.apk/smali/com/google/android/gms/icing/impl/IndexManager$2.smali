.class Lcom/google/android/gms/icing/impl/IndexManager$2;
.super Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;
.source "IndexManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/icing/impl/IndexManager;->init(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/icing/impl/IndexManager;

.field final synthetic val$clear:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/IndexManager;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/IndexManager$2;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iput-boolean p2, p0, Lcom/google/android/gms/icing/impl/IndexManager$2;->val$clear:Z

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/IndexManager$KeepAwakeRunnable;-><init>(Lcom/google/android/gms/icing/impl/IndexManager;)V

    return-void
.end method


# virtual methods
.method public runAwake()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$2;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    iget-boolean v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$2;->val$clear:Z

    # invokes: Lcom/google/android/gms/icing/impl/IndexManager;->initIndexInternal(Z)V
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/IndexManager;->access$400(Lcom/google/android/gms/icing/impl/IndexManager;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/IndexManager$2;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mInitSem:Ljava/util/concurrent/Semaphore;
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/IndexManager;->access$500(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/IndexManager$2;->this$0:Lcom/google/android/gms/icing/impl/IndexManager;

    # getter for: Lcom/google/android/gms/icing/impl/IndexManager;->mInitSem:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/google/android/gms/icing/impl/IndexManager;->access$500(Lcom/google/android/gms/icing/impl/IndexManager;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method
