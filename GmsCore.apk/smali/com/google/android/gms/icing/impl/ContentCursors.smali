.class public Lcom/google/android/gms/icing/impl/ContentCursors;
.super Ljava/lang/Object;
.source "ContentCursors.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/icing/impl/ContentCursors$1;,
        Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;
    }
.end annotation


# static fields
.field private static final CORPUS_DOCUMENT_COLUMNS:[Ljava/lang/String;

.field private static final CORPUS_TAG_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private final mDocs:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

.field private final mDocumentColumns:[Ljava/lang/String;

.field private final mTags:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "seqno"

    aput-object v1, v0, v2

    const-string v1, "action"

    aput-object v1, v0, v3

    const-string v1, "uri"

    aput-object v1, v0, v4

    const-string v1, "doc_score"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/icing/impl/ContentCursors;->CORPUS_DOCUMENT_COLUMNS:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "seqno"

    aput-object v1, v0, v2

    const-string v1, "action"

    aput-object v1, v0, v3

    const-string v1, "uri"

    aput-object v1, v0, v4

    const-string v1, "tag"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/icing/impl/ContentCursors;->CORPUS_TAG_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;[Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;-><init>(Lcom/google/android/gms/icing/impl/ContentCursors$1;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocs:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    new-instance v0, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;-><init>(Lcom/google/android/gms/icing/impl/ContentCursors$1;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mTags:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v0, Lcom/google/android/gms/icing/impl/ContentCursors;->CORPUS_DOCUMENT_COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    array-length v1, p2

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocumentColumns:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/icing/impl/ContentCursors;->CORPUS_DOCUMENT_COLUMNS:[Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocumentColumns:[Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/icing/impl/ContentCursors;->CORPUS_DOCUMENT_COLUMNS:[Ljava/lang/String;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocumentColumns:[Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/icing/impl/ContentCursors;->CORPUS_DOCUMENT_COLUMNS:[Ljava/lang/String;

    array-length v1, v1

    array-length v2, p2

    invoke-static {p2, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method private contactContentProvider(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;Ljava/util/Map;)Landroid/database/Cursor;
    .locals 8
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # J
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x0

    invoke-interface {p7}, Ljava/util/Map;->clear()V

    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->acquireUnstableContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_2

    const-string v2, "Could not connect to content provider %s"

    invoke-static {v2, p1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_0
    :goto_1
    return-object v1

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v4, v1

    const/4 v1, 0x1

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x2

    aput-object p6, v4, v1

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_5

    const-string v1, "Cursor for %s is null, %s"

    invoke-static {v1, p2, p1}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_4
    :goto_3
    move-object v1, v6

    goto :goto_1

    :cond_5
    :try_start_2
    invoke-direct {p0, v6, p3, p7}, Lcom/google/android/gms/icing/impl/ContentCursors;->mapAndValidateColumnNames(Landroid/database/Cursor;[Ljava/lang/String;Ljava/util/Map;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    invoke-interface {p7}, Ljava/util/Map;->clear()V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v7

    :try_start_3
    const-string v1, "No permission to contact provider %s"

    invoke-static {v1, p1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v6, 0x0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_3

    :catch_1
    move-exception v7

    :try_start_4
    const-string v1, "Contact provider %s failed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v7, v1, v2}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v6, 0x0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_3

    :catch_2
    move-exception v7

    :try_start_5
    const-string v1, "Contacting provider %s failed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v7, v1, v2}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const/4 v6, 0x0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_3

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_6
    throw v1
.end method

.method private mapAndValidateColumnNames(Landroid/database/Cursor;[Ljava/lang/String;Ljava/util/Map;)Z
    .locals 6
    .param p1    # Landroid/database/Cursor;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    move-object v0, p2

    array-length v4, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v1, v0, v2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    const/4 v5, -0x1

    if-ne v3, v5, :cond_0

    const-string v5, "Column %s was not returned by client, refusing to index"

    invoke-static {v5, v1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v5, 0x0

    :goto_1
    return v5

    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p3, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x1

    goto :goto_1
.end method

.method private refillDocumentCursor(Landroid/net/Uri;J)V
    .locals 9
    .param p1    # Landroid/net/Uri;
    .param p2    # J

    const-string v2, "documents"

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocumentColumns:[Ljava/lang/String;

    const-string v6, "20"

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocs:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    # invokes: Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getWritableColumnMap()Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->access$400(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)Ljava/util/Map;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/icing/impl/ContentCursors;->contactContentProvider(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;Ljava/util/Map;)Landroid/database/Cursor;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocs:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    # invokes: Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->set(Landroid/database/Cursor;)V
    invoke-static {v0, v8}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->access$500(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;Landroid/database/Cursor;)V

    return-void
.end method

.method private refillTagsCursor(Landroid/net/Uri;J)V
    .locals 9
    .param p1    # Landroid/net/Uri;
    .param p2    # J

    const-string v2, "tags"

    sget-object v3, Lcom/google/android/gms/icing/impl/ContentCursors;->CORPUS_TAG_COLUMNS:[Ljava/lang/String;

    const-string v6, "100"

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mTags:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    # invokes: Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->getWritableColumnMap()Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->access$400(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)Ljava/util/Map;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/icing/impl/ContentCursors;->contactContentProvider(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;Ljava/util/Map;)Landroid/database/Cursor;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mTags:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    # invokes: Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->set(Landroid/database/Cursor;)V
    invoke-static {v0, v8}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->access$500(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public anyHasData()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocs:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    # getter for: Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mHasData:Z
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->access$100(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mTags:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    # getter for: Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->mHasData:Z
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->access$100(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocs:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    # invokes: Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->close()V
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->access$200(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mTags:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    # invokes: Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->close()V
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->access$200(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mContentResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method public docs()Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocs:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    return-object v0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    const-string v0, "Content cursor disposed without a closing"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/ContentCursors;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public refillIfNecessary(Landroid/net/Uri;J)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mDocs:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    # invokes: Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->needsRefill()Z
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->access$300(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/ContentCursors;->refillDocumentCursor(Landroid/net/Uri;J)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mTags:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    # invokes: Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->needsRefill()Z
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;->access$300(Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/ContentCursors;->refillTagsCursor(Landroid/net/Uri;J)V

    :cond_1
    return-void
.end method

.method public tags()Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ContentCursors;->mTags:Lcom/google/android/gms/icing/impl/ContentCursors$ContentCursor;

    return-object v0
.end method
