.class public Lcom/google/android/gms/icing/impl/AsyncScheduler;
.super Ljava/lang/Object;
.source "AsyncScheduler.java"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Ljava/lang/Thread;

.field private mStarted:Landroid/os/ConditionVariable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mStarted:Landroid/os/ConditionVariable;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/gms/icing/impl/AsyncScheduler;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0    # Lcom/google/android/gms/icing/impl/AsyncScheduler;
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/gms/icing/impl/AsyncScheduler;)Landroid/os/ConditionVariable;
    .locals 1
    .param p0    # Lcom/google/android/gms/icing/impl/AsyncScheduler;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mStarted:Landroid/os/ConditionVariable;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->quit()V

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/AsyncScheduler;->start()V

    return-void
.end method

.method public isInThread()Z
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandlerThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public quit()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandlerThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "Scheduler calling quit"

    invoke-static {v0}, Lcom/google/android/gms/icing/LogUtil;->d(Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/icing/impl/AsyncScheduler$2;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/AsyncScheduler$2;-><init>(Lcom/google/android/gms/icing/impl/AsyncScheduler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandlerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandlerThread:Ljava/lang/Thread;

    goto :goto_0
.end method

.method public schedule(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Couldn\'t schedule operation"

    invoke-static {v1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public schedule(Ljava/lang/Runnable;J)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;
    .param p2    # J

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Couldn\'t schedule operation"

    invoke-static {v1}, Lcom/google/android/gms/icing/LogUtil;->e(Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public start()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandlerThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/gms/icing/impl/AsyncScheduler$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/AsyncScheduler$1;-><init>(Lcom/google/android/gms/icing/impl/AsyncScheduler;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandlerThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mHandlerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/AsyncScheduler;->mStarted:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    goto :goto_0
.end method
