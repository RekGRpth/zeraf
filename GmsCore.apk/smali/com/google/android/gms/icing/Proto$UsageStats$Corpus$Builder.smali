.class public final Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Proto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;",
        "Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$21000()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 1

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->create()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;-><init>()V

    new-instance v1, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;-><init>(Lcom/google/android/gms/icing/Proto$1;)V

    iput-object v1, v0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    invoke-static {v0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->buildPartial()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->build()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    return-object v0
.end method

.method public clone()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->create()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->mergeFrom(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->clone()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->clone()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->clone()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 2
    .param p1    # Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    invoke-static {}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDefaultInstance()Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->setId(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocuments()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDocuments()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->setDocuments(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocuments()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDeletedDocuments()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->setDeletedDocuments(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocumentsSize()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDocumentsSize()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->setDocumentsSize(J)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocumentsSize()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->getDeletedDocumentsSize()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->setDeletedDocumentsSize(J)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->setId(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->setDocuments(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->setDeletedDocuments(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->setDocumentsSize(J)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->setDeletedDocumentsSize(J)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setDeletedDocuments(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocuments:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->access$21602(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    # setter for: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->deletedDocuments_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->access$21702(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;I)I

    return-object p0
.end method

.method public setDeletedDocumentsSize(J)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDeletedDocumentsSize:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->access$22002(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    # setter for: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->deletedDocumentsSize_:J
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->access$22102(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;J)J

    return-object p0
.end method

.method public setDocuments(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocuments:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->access$21402(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    # setter for: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->documents_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->access$21502(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;I)I

    return-object p0
.end method

.method public setDocumentsSize(J)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasDocumentsSize:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->access$21802(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    # setter for: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->documentsSize_:J
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->access$21902(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;J)J

    return-object p0
.end method

.method public setId(I)Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->hasId:Z
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->access$21202(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus$Builder;->result:Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;

    # setter for: Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->id_:I
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;->access$21302(Lcom/google/android/gms/icing/Proto$UsageStats$Corpus;I)I

    return-object p0
.end method
