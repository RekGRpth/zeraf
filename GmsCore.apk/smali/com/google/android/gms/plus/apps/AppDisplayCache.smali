.class public final Lcom/google/android/gms/plus/apps/AppDisplayCache;
.super Ljava/lang/Object;
.source "AppDisplayCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/apps/AppDisplayCache$1;,
        Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/google/android/gms/plus/apps/AppDisplayCache;


# instance fields
.field private final mCache:Lvedroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;",
            ">;"
        }
    .end annotation
.end field

.field private mDefaultDrawable:Landroid/graphics/drawable/Drawable;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method private constructor <init>(Landroid/content/pm/PackageManager;I)V
    .locals 1
    .param p1    # Landroid/content/pm/PackageManager;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance v0, Lvedroid/support/v4/util/LruCache;

    invoke-direct {v0, p2}, Lvedroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->mCache:Lvedroid/support/v4/util/LruCache;

    return-void
.end method

.method private createData(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;
    .locals 6
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/apps/Application;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->getDefaultDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_0
    new-instance v4, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/apps/Application;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->getDefaultDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-ne v1, v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-direct {v4, v5, v1, v2, v3}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;ZLcom/google/android/gms/plus/apps/AppDisplayCache$1;)V

    return-object v4

    :cond_1
    move-object v1, v3

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private getDefaultDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->mDefaultDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->mDefaultDrawable:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->mDefaultDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/AppDisplayCache;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->sInstance:Lcom/google/android/gms/plus/apps/AppDisplayCache;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/apps/AppDisplayCache;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/plus/apps/AppDisplayCache;-><init>(Landroid/content/pm/PackageManager;I)V

    sput-object v0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->sInstance:Lcom/google/android/gms/plus/apps/AppDisplayCache;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->sInstance:Lcom/google/android/gms/plus/apps/AppDisplayCache;

    return-object v0
.end method


# virtual methods
.method public getData(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/apps/Application;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->mCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v3, v0}, Lvedroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

    if-eqz v1, :cond_0

    move-object v2, v1

    :goto_0
    return-object v2

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->createData(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache;->mCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v3, v0, v1}, Lvedroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v1

    goto :goto_0
.end method

.method public updateDrawable(Lcom/google/android/gms/plus/model/apps/Application;Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/model/apps/Application;
    .param p2    # Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/apps/AppDisplayCache;->getData(Lcom/google/android/gms/plus/model/apps/Application;)Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;

    move-result-object v0

    if-eqz p2, :cond_0

    # invokes: Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->setDrawable(Landroid/graphics/drawable/Drawable;)V
    invoke-static {v0, p2}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->access$000(Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    const/4 v1, 0x0

    # invokes: Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->setDefaultImage(Z)V
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->access$100(Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;Z)V

    return-object v0
.end method
