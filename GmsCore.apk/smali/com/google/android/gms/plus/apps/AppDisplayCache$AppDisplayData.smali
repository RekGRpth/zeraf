.class public final Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;
.super Ljava/lang/Object;
.source "AppDisplayCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/apps/AppDisplayCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppDisplayData"
.end annotation


# instance fields
.field private final mDisplayName:Ljava/lang/CharSequence;

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mIsDefaultImage:Z


# direct methods
.method private constructor <init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Z)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/graphics/drawable/Drawable;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->mDisplayName:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->mDrawable:Landroid/graphics/drawable/Drawable;

    iput-boolean p3, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->mIsDefaultImage:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;ZLcom/google/android/gms/plus/apps/AppDisplayCache$1;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/graphics/drawable/Drawable;
    .param p3    # Z
    .param p4    # Lcom/google/android/gms/plus/apps/AppDisplayCache$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;Z)V
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->setDefaultImage(Z)V

    return-void
.end method

.method private setDefaultImage(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->mIsDefaultImage:Z

    return-void
.end method

.method private setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->mDisplayName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public isDefaultImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/AppDisplayCache$AppDisplayData;->mIsDefaultImage:Z

    return v0
.end method
