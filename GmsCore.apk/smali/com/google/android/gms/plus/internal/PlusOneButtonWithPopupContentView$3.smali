.class Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;
.super Ljava/lang/Object;
.source "PlusOneButtonWithPopupContentView.java"

# interfaces
.implements Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSignUpStateLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/plus/data/plusone/SignUpState;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/gms/plus/data/plusone/SignUpState;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    # setter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mSignUpState:Lcom/google/android/gms/plus/data/plusone/SignUpState;
    invoke-static {v0, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$302(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Lcom/google/android/gms/plus/data/plusone/SignUpState;)Lcom/google/android/gms/plus/data/plusone/SignUpState;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    # getter for: Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mPlusOneNoOpListener:Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;
    invoke-static {v1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->access$400(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v2, v2, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/PlusClient;->loadPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-boolean v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mClickPending:Z

    if-eqz v0, :cond_1

    const-string v0, "PlusOneButtonWithPopup"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onSignUpStateLoaded: performing pending click"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iput-boolean v3, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mTogglePending:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->performClick()Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView$3;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iput-boolean v3, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->mClickPending:Z

    :cond_1
    return-void
.end method
