.class Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneCompoundButton;
.super Landroid/widget/CompoundButton;
.source "PlusOneButtonContentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlusOneCompoundButton"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneCompoundButton;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-direct {p0, p2}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public toggle()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneCompoundButton;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-boolean v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTogglePending:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/CompoundButton;->toggle()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneCompoundButton;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->mTogglePending:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView$PlusOneCompoundButton;->this$0:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->showProgress()V

    goto :goto_0
.end method
