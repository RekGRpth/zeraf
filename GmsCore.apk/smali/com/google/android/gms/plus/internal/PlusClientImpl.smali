.class public final Lcom/google/android/gms/plus/internal/PlusClientImpl;
.super Lcom/google/android/gms/common/internal/GmsClient;
.source "PlusClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusGmsCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$SourceDisconnectedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$AppsLoadedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedCallback;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedCallback;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$TokenRevokedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$PeopleLoadedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$SourceDisconnectedCallback;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$AppsLoadedCallback;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$TokenRevokedCallback;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedCallback;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$PeopleLoadedCallback;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedCallback;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedCallback;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$SignUpStateLoadedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$SignUpStateLoadedCallback;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedBinderCallbacks;,
        Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/gms/plus/internal/IPlusService;",
        ">;"
    }
.end annotation


# static fields
.field public static final ARG_BOUNDING_BOX:Ljava/lang/String; = "bounding_box"

.field public static final ARG_SKIP_OOB:Ljava/lang/String; = "skip_oob"

.field public static final KEY_LOADED_PERSON:Ljava/lang/String; = "loaded_person"

.field public static final SERVICE_ACTION:Ljava/lang/String; = "com.google.android.gms.plus.service.START"


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field private final mAuthPackage:Ljava/lang/String;

.field private final mCallingPackage:Ljava/lang/String;

.field private mCurrentPerson:Lcom/google/android/gms/plus/model/people/Person;

.field private final mRequiredFeatures:[Ljava/lang/String;

.field private final mVisibleActions:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p3    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p3    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p4    # [Ljava/lang/String;

    const-string v2, "<<default account>>"

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/PlusClientImpl;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p4    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p5    # [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/internal/PlusClientImpl;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p5    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p6    # [Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/internal/PlusClientImpl;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p6    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p7    # [Ljava/lang/String;
    .param p8    # [Ljava/lang/String;
    .param p9    # [Ljava/lang/String;

    invoke-direct {p0, p1, p5, p6, p9}, Lcom/google/android/gms/common/internal/GmsClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mCallingPackage:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mAuthPackage:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mAccountName:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mVisibleActions:[Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mRequiredFeatures:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/model/people/Person;)Lcom/google/android/gms/plus/model/people/Person;
    .locals 0
    .param p0    # Lcom/google/android/gms/plus/internal/PlusClientImpl;
    .param p1    # Lcom/google/android/gms/plus/model/people/Person;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mCurrentPerson:Lcom/google/android/gms/plus/model/people/Person;

    return-object p1
.end method


# virtual methods
.method public clearDefaultAccount()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mCurrentPerson:Lcom/google/android/gms/plus/model/people/Person;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v1}, Lcom/google/android/gms/plus/internal/IPlusService;->clearDefaultAccount()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected bridge synthetic createServiceInterface(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1
    .param p1    # Landroid/os/IBinder;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->createServiceInterface(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/IPlusService;

    move-result-object v0

    return-object v0
.end method

.method protected createServiceInterface(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/IPlusService;
    .locals 1
    .param p1    # Landroid/os/IBinder;

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/IPlusService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/IPlusService;

    move-result-object v0

    return-object v0
.end method

.method public deletePlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v0, p2}, Lcom/google/android/gms/plus/internal/IPlusService;->deletePlusOne(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v3, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedBinderCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public disconnectSource(Lcom/google/android/gms/plus/PlusClient$OnSourceDisconnectedListener;Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnSourceDisconnectedListener;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$SourceDisconnectedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$SourceDisconnectedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnSourceDisconnectedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v0, p2, p3}, Lcom/google/android/gms/plus/internal/IPlusService;->disconnectSource(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v3, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl$SourceDisconnectedBinderCallbacks;->onSourceDisconnected(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v1}, Lcom/google/android/gms/plus/internal/IPlusService;->getAccountName()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getCurrentPerson()Lcom/google/android/gms/plus/model/people/Person;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mCurrentPerson:Lcom/google/android/gms/plus/model/people/Person;

    return-object v0
.end method

.method protected getServiceDescriptor()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.internal.IPlusService"

    return-object v0
.end method

.method protected getServiceFromBroker(Lcom/google/android/gms/common/internal/IGmsServiceBroker;Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;)V
    .locals 8
    .param p1    # Lcom/google/android/gms/common/internal/IGmsServiceBroker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/internal/IGmsServiceBroker;",
            "Lcom/google/android/gms/common/internal/GmsClient",
            "<",
            "Lcom/google/android/gms/plus/internal/IPlusService;",
            ">.GmsCallbacks;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v0, "skip_oob"

    const/4 v2, 0x0

    invoke-virtual {v7, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "request_visible_actions"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mVisibleActions:[Ljava/lang/String;

    invoke-virtual {v7, v0, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mRequiredFeatures:[Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "required_features"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mRequiredFeatures:[Ljava/lang/String;

    invoke-virtual {v7, v0, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusGmsCallbacks;

    invoke-direct {v1, p0, p2}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusGmsCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;)V

    const v2, 0x2e309c

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mCallingPackage:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mAuthPackage:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getScopes()[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mAccountName:Ljava/lang/String;

    move-object v0, p1

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/common/internal/IGmsServiceBroker;->getPlusService(Lcom/google/android/gms/common/internal/IGmsCallbacks;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public getSignUpState(Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$SignUpStateLoadedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$SignUpStateLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnSignUpStateLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v0}, Lcom/google/android/gms/plus/internal/IPlusService;->getSignUpState(Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v3, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl$SignUpStateLoadedBinderCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected getStartServiceAction()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.service.START"

    return-object v0
.end method

.method public final getVisibleActions()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusClientImpl;->mVisibleActions:[Ljava/lang/String;

    return-object v0
.end method

.method public insertPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v0, p2, p3}, Lcom/google/android/gms/plus/internal/IPlusService;->insertPlusOne(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v3, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedBinderCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public loadAudienceInternal(Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v0}, Lcom/google/android/gms/plus/internal/IPlusService;->loadAudienceInternal(Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v3, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl$AudienceLoadedBinderCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public loadConnectedApps(Lcom/google/android/gms/plus/PlusClient$OnAppsLoadedListener;ILjava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnAppsLoadedListener;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$AppsLoadedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$AppsLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnAppsLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v0, p2, p3}, Lcom/google/android/gms/plus/internal/IPlusService;->loadConnectedApps(Lcom/google/android/gms/plus/internal/IPlusCallbacks;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v2, 0x8

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->empty(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl$AppsLoadedBinderCallbacks;->onAppsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public loadImage(Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;Landroid/net/Uri;I)V
    .locals 5
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;
    .param p2    # Landroid/net/Uri;
    .param p3    # I

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "bounding_box"

    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnParcelFileDescriptorLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v3, v0, p2, v2}, Lcom/google/android/gms/plus/internal/IPlusService;->loadImage(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Landroid/net/Uri;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v3, 0x8

    invoke-virtual {v0, v3, v4, v4}, Lcom/google/android/gms/plus/internal/PlusClientImpl$ParcelFileDescriptorLoadedBinderCallbacks;->onImageFileDescriptorLoaded(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method public loadLinkPreviewInternal(Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnLinkPreviewLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v0, p2}, Lcom/google/android/gms/plus/internal/IPlusService;->loadLinkPreviewInternal(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v3, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl$LinkPreviewLoadedBinderCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public loadMoments(Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/net/Uri;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v1, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedBinderCallbacks;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnMomentsLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/IPlusService;

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/plus/internal/IPlusService;->loadMoments(Lcom/google/android/gms/plus/internal/IPlusCallbacks;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v7

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->empty(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v1, v0, v8, v8}, Lcom/google/android/gms/plus/internal/PlusClientImpl$MomentsLoadedBinderCallbacks;->onMomentsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public loadPeople(Lcom/google/android/gms/plus/PlusClient$OnPeopleLoadedListener;IIILjava/lang/String;)V
    .locals 7
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnPeopleLoadedListener;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v1, Lcom/google/android/gms/plus/internal/PlusClientImpl$PeopleLoadedBinderCallbacks;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PeopleLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnPeopleLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/IPlusService;

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/IPlusService;->loadPeople(Lcom/google/android/gms/plus/internal/IPlusCallbacks;IIILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v6

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->empty(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PeopleLoadedBinderCallbacks;->onPeopleLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public loadPerson(Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnPersonLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v0, p2}, Lcom/google/android/gms/plus/internal/IPlusService;->loadPerson(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v3, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PersonLoadedBinderCallbacks;->onPersonLoaded(ILandroid/os/Bundle;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V

    goto :goto_0
.end method

.method public loadPlusOne(Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnPlusOneLoadedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v0, p2}, Lcom/google/android/gms/plus/internal/IPlusService;->loadPlusOne(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v3, v3}, Lcom/google/android/gms/plus/internal/PlusClientImpl$PlusOneLoadedBinderCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public removeMoment(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v1, p1}, Lcom/google/android/gms/plus/internal/IPlusService;->removeMoment(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public revokeAccessAndDisconnect(Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->clearDefaultAccount()V

    new-instance v0, Lcom/google/android/gms/plus/internal/PlusClientImpl$TokenRevokedBinderCallbacks;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/internal/PlusClientImpl$TokenRevokedBinderCallbacks;-><init>(Lcom/google/android/gms/plus/internal/PlusClientImpl;Lcom/google/android/gms/plus/PlusClient$OnAccessRevokedListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v0}, Lcom/google/android/gms/plus/internal/IPlusService;->revokeAccessAndDisconnect(Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public writeMoment(Lcom/google/android/gms/plus/model/moments/Moment;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/model/moments/Moment;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->checkConnected()V

    :try_start_0
    check-cast p1, Lcom/google/android/gms/plus/internal/model/moments/MomentEntity;

    invoke-static {p1}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->from(Lcom/google/android/gms/common/server/response/FastJsonResponse;)Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusClientImpl;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/internal/IPlusService;

    invoke-interface {v2, v1}, Lcom/google/android/gms/plus/internal/IPlusService;->writeMoment(Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method
