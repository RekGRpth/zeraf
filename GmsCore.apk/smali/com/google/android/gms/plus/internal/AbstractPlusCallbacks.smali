.class public abstract Lcom/google/android/gms/plus/internal/AbstractPlusCallbacks;
.super Lcom/google/android/gms/plus/internal/IPlusCallbacks$Stub;
.source "AbstractPlusCallbacks.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/IPlusCallbacks$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccessRevoked(ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    return-void
.end method

.method public onAppsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/data/DataHolder;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onDefaultAccountResolvedInternal(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onImageFileDescriptorLoaded(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onMomentsLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/data/DataHolder;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onPeopleLoaded(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/data/DataHolder;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onPersonLoaded(ILandroid/os/Bundle;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    return-void
.end method

.method public onSourceDisconnected(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/os/Bundle;

    return-void
.end method
