.class public interface abstract Lcom/google/android/gms/plus/internal/PlusContract$PersonColumns;
.super Ljava/lang/Object;
.source "PlusContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/internal/PlusContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PersonColumns"
.end annotation


# static fields
.field public static final ALL_COLUMNS:[Ljava/lang/String;

.field public static final DISPLAY_NAME:Ljava/lang/String; = "displayName"

.field public static final IMAGE_URL:Ljava/lang/String; = "image"

.field public static final OBJECT_TYPE:Ljava/lang/String; = "objectType"

.field public static final PERSON_ID:Ljava/lang/String; = "personId"

.field public static final URL:Ljava/lang/String; = "url"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "personId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "image"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "objectType"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "url"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/plus/internal/PlusContract$PersonColumns;->ALL_COLUMNS:[Ljava/lang/String;

    return-void
.end method
