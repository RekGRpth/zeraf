.class Lcom/google/android/gms/plus/provider/PlusOpenHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PlusOpenHelper.java"


# static fields
.field private static final TABLE_PENDING_ACTIONS_CREATE:Ljava/lang/String;

.field private static final TABLE_PENDING_LOGS_CREATE:Ljava/lang/String;

.field private static final TABLE_PLUS_ACCOUNTS_CREATE:Ljava/lang/String;

.field private static final TABLE_PLUS_PROFILES_CREATE:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-class v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TAG:Ljava/lang/String;

    const-string v0, "CREATE TABLE %s (%s, %s, %s, %s, %s);"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "offline_frames"

    aput-object v2, v1, v4

    const-string v2, "_id INTEGER PRIMARY KEY AUTOINCREMENT"

    aput-object v2, v1, v5

    const-string v2, "packageName STRING"

    aput-object v2, v1, v6

    const-string v2, "accountName STRING"

    aput-object v2, v1, v7

    const-string v2, "payload STRING"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "url STRING"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TABLE_PENDING_ACTIONS_CREATE:Ljava/lang/String;

    const-string v0, "CREATE TABLE %s (%s, %s, %s, %s, %s);"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "offline_logs"

    aput-object v2, v1, v4

    const-string v2, "_id INTEGER PRIMARY KEY AUTOINCREMENT"

    aput-object v2, v1, v5

    const-string v2, "accountName STRING NOT NULL"

    aput-object v2, v1, v6

    const-string v2, "type INTEGER NOT NULL"

    aput-object v2, v1, v7

    const-string v2, "payload STRING NOT NULL"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "timestamp INTEGER NOT NULL"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TABLE_PENDING_LOGS_CREATE:Ljava/lang/String;

    const-string v0, "CREATE TABLE %s (%s, %s, %s, %s, %s, %s);"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "plus_accounts"

    aput-object v2, v1, v4

    const-string v2, "_id INTEGER PRIMARY KEY AUTOINCREMENT"

    aput-object v2, v1, v5

    const-string v2, "updated INTEGER NOT NULL"

    aput-object v2, v1, v6

    const-string v2, "display_name STRING"

    aput-object v2, v1, v7

    const-string v2, "account_name STRING"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "profile_image_url STRING"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "signedUp BOOLEAN"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TABLE_PLUS_ACCOUNTS_CREATE:Ljava/lang/String;

    const-string v0, "CREATE TABLE %s (%s, %s, %s, %s, %s);"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "plus_profiles"

    aput-object v2, v1, v4

    const-string v2, "_id INTEGER PRIMARY KEY AUTOINCREMENT"

    aput-object v2, v1, v5

    const-string v2, "updated INTEGER NOT NULL"

    aput-object v2, v1, v6

    const-string v2, "accountName STRING"

    aput-object v2, v1, v7

    const-string v2, "packageName STRING"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "profileJson STRING"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TABLE_PLUS_PROFILES_CREATE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    const/16 v1, 0x8

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object p1, p0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->mContext:Landroid/content/Context;

    return-void
.end method

.method private migrateDefaultAccounts(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 11
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "packageName"

    aput-object v0, v2, v1

    const-string v0, "accountName"

    aput-object v0, v2, v4

    const-string v1, "default_account"

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->mContext:Landroid/content/Context;

    invoke-static {v0, v10, v8}, Lcom/google/android/gms/common/account/AccountUtils;->setSelectedAccount(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    const-string v0, "DROP TABLE IF EXISTS default_account"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    sget-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TABLE_PENDING_ACTIONS_CREATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TABLE_PENDING_LOGS_CREATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TABLE_PLUS_ACCOUNTS_CREATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TABLE_PLUS_PROFILES_CREATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x5

    if-gt p2, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->migrateDefaultAccounts(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    const/4 v0, 0x6

    if-gt p2, v0, :cond_1

    sget-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TABLE_PLUS_PROFILES_CREATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x7

    if-ne p2, v0, :cond_2

    const-string v0, "DROP TABLE IF EXISTS %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "plus_profiles"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->TABLE_PLUS_PROFILES_CREATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public wipeData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "DROP TABLE IF EXISTS %s"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "offline_frames"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS %s"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "offline_logs"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS %s"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "plus_accounts"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS %s"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "plus_profiles"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/provider/PlusOpenHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method
