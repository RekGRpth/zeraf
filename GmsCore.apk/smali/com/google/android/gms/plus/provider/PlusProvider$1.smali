.class Lcom/google/android/gms/plus/provider/PlusProvider$1;
.super Landroid/os/AsyncTask;
.source "PlusProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/plus/provider/PlusProvider;->onAccountsUpdated([Landroid/accounts/Account;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/accounts/Account;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/provider/PlusProvider;

.field final synthetic val$contentResolver:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/provider/PlusProvider;Landroid/content/ContentResolver;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/provider/PlusProvider$1;->this$0:Lcom/google/android/gms/plus/provider/PlusProvider;

    iput-object p2, p0, Lcom/google/android/gms/plus/provider/PlusProvider$1;->val$contentResolver:Landroid/content/ContentResolver;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Landroid/accounts/Account;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/provider/PlusProvider$1;->doInBackground([Landroid/accounts/Account;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/accounts/Account;)Ljava/lang/Void;
    .locals 1
    .param p1    # [Landroid/accounts/Account;

    iget-object v0, p0, Lcom/google/android/gms/plus/provider/PlusProvider$1;->val$contentResolver:Landroid/content/ContentResolver;

    invoke-static {v0, p1}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->clearDeletedAccounts(Landroid/content/ContentResolver;[Landroid/accounts/Account;)V

    const/4 v0, 0x0

    return-object v0
.end method
