.class public Lcom/google/android/gms/plus/service/whitelisted/Attachments;
.super Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;
.source "Attachments.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypeArrays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    const-string v1, "previewThumbnails"

    const-string v2, "previewThumbnails"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/PreviewThumbnails;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    const-string v1, "displayName"

    const-string v2, "title"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    const-string v1, "thumbnails"

    const-string v2, "thumbnails"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/Thumbnails;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    const-string v1, "fullImage"

    const-string v2, "fullImage"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/FullImage;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    const-string v1, "url"

    const-string v2, "url"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    const-string v1, "image"

    const-string v2, "image"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/Image;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    const-string v1, "content"

    const-string v2, "description"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    const-string v1, "embed"

    const-string v2, "embed"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/Embed;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    const-string v1, "categories"

    const-string v2, "categories"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/Categories;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    const-string v1, "objectType"

    const-string v2, "type"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentValues;)V
    .locals 1
    .param p1    # Landroid/content/ContentValues;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;-><init>(Landroid/content/ContentValues;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/gms/plus/service/whitelisted/FullImage;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/Image;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/Embed;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p4    # Lcom/google/android/gms/plus/service/whitelisted/FullImage;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/gms/plus/service/whitelisted/Image;
    .param p7    # Ljava/lang/String;
    .param p8    # Lcom/google/android/gms/plus/service/whitelisted/Embed;
    .param p10    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/PreviewThumbnails;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/Thumbnails;",
            ">;",
            "Lcom/google/android/gms/plus/service/whitelisted/FullImage;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/whitelisted/Image;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/whitelisted/Embed;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/Categories;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastContentValuesJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v0, "previewThumbnails"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "title"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "thumbnails"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "fullImage"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "url"

    invoke-virtual {p0, v0, p5}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "image"

    invoke-virtual {p0, v0, p6}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "description"

    invoke-virtual {p0, v0, p7}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "embed"

    invoke-virtual {p0, v0, p8}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "categories"

    invoke-virtual {p0, v0, p9}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "type"

    invoke-virtual {p0, v0, p10}, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getCategories()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/Categories;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "categories"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getEmbed()Lcom/google/android/gms/plus/service/whitelisted/Embed;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "embed"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/Embed;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getFullImage()Lcom/google/android/gms/plus/service/whitelisted/FullImage;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "fullImage"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/FullImage;

    return-object v0
.end method

.method public getImage()Lcom/google/android/gms/plus/service/whitelisted/Image;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/Image;

    return-object v0
.end method

.method public getPreviewThumbnails()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/PreviewThumbnails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "previewThumbnails"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getThumbnails()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/Thumbnails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "thumbnails"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method protected isConcreteTypeArrayFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isConcreteTypeFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/Attachments;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
