.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "LoadSocialNetworkRequestPersonListOptions.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private mSyncStateToken:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSyncStateToken;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->sFields:Ljava/util/HashMap;

    const-string v1, "includeExtendedProfileInfo"

    const-string v2, "includeExtendedProfileInfo"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->sFields:Ljava/util/HashMap;

    const-string v1, "includePeople"

    const-string v2, "includePeople"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->sFields:Ljava/util/HashMap;

    const-string v1, "syncStateToken"

    const-string v2, "syncStateToken"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSyncStateToken;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->sFields:Ljava/util/HashMap;

    const-string v1, "maxPeople"

    const-string v2, "maxPeople"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->sFields:Ljava/util/HashMap;

    const-string v1, "profileTypesToFilter"

    const-string v2, "profileTypesToFilter"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forStrings(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSyncStateToken;Ljava/lang/Integer;Ljava/util/ArrayList;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;
    .param p2    # Ljava/lang/Boolean;
    .param p3    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSyncStateToken;
    .param p4    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSyncStateToken;",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    if-eqz p1, :cond_0

    const-string v0, "includeExtendedProfileInfo"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->setBoolean(Ljava/lang/String;Z)V

    :cond_0
    if-eqz p2, :cond_1

    const-string v0, "includePeople"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->setBoolean(Ljava/lang/String;Z)V

    :cond_1
    const-string v0, "syncStateToken"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p4, :cond_2

    const-string v0, "maxPeople"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->setInteger(Ljava/lang/String;I)V

    :cond_2
    const-string v0, "profileTypesToFilter"

    invoke-virtual {p0, v0, p5}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->setStrings(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    check-cast p2, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSyncStateToken;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->mSyncStateToken:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSyncStateToken;

    return-void
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getMaxPeople()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "maxPeople"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getProfileTypesToFilter()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "profileTypesToFilter"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSyncStateToken()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSyncStateToken;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->mSyncStateToken:Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataSyncStateToken;

    return-object v0
.end method

.method public isIncludeExtendedProfileInfo()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "includeExtendedProfileInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isIncludePeople()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/LoadSocialNetworkRequestPersonListOptions;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "includePeople"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method
