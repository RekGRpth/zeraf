.class public Lcom/google/android/gms/plus/service/PlusOperations$RemoveMomentOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RemoveMomentOperation"
.end annotation


# instance fields
.field private final mClientContext:Lcom/google/android/gms/common/server/ClientContext;

.field private final mMomentId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$RemoveMomentOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$RemoveMomentOperation;->mMomentId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$RemoveMomentOperation;->mClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$RemoveMomentOperation;->mMomentId:Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/gms/plus/broker/DataBroker;->removeMoment(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    return-void
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Exception;

    return-void
.end method
