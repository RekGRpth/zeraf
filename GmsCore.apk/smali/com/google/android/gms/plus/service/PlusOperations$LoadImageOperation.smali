.class public final Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoadImageOperation"
.end annotation


# instance fields
.field private final mBoundingBox:I

.field private final mImageBroker:Lcom/google/android/gms/common/images/ImageBroker;

.field private final mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

.field private final mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/images/ImageBroker;Landroid/net/Uri;ILcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/images/ImageBroker;
    .param p2    # Landroid/net/Uri;
    .param p3    # I
    .param p4    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mImageBroker:Lcom/google/android/gms/common/images/ImageBroker;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mUri:Landroid/net/Uri;

    iput p3, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mBoundingBox:I

    iput-object p4, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    return-void
.end method

.method private sendCallback(ILandroid/os/ParcelFileDescriptor;)V
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2, p2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onImageFileDescriptorLoaded(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    :try_start_1
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/plus/service/PlusOperations;->TAG:Ljava/lang/String;

    const-string v2, "Failed close"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz p2, :cond_1

    :try_start_2
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v1

    :catch_1
    move-exception v0

    sget-object v2, Lcom/google/android/gms/plus/service/PlusOperations;->TAG:Ljava/lang/String;

    const-string v3, "Failed close"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mUri:Landroid/net/Uri;

    iget v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mBoundingBox:I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mUri:Landroid/net/Uri;

    iget v3, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mBoundingBox:I

    invoke-static {v2, v3}, Lcom/google/android/gms/plus/internal/PlusContent$Image;->appendImageSize(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mImageBroker:Lcom/google/android/gms/common/images/ImageBroker;

    invoke-virtual {v2, p1, v1}, Lcom/google/android/gms/common/images/ImageBroker;->loadImageFile(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->sendCallback(ILandroid/os/ParcelFileDescriptor;)V

    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->sendCallback(ILandroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onImageFileDescriptorLoaded(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    :cond_0
    return-void
.end method
