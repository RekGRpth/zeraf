.class Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;
.super Lcom/google/android/gms/plus/internal/IPlusService$Stub;
.source "PlusService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PlusServiceEntity"
.end annotation


# instance fields
.field private final mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

.field private final mContext:Landroid/content/Context;

.field private final mGmsClientContext:Lcom/google/android/gms/common/server/ClientContext;

.field private final mImageBroker:Lcom/google/android/gms/common/images/ImageBroker;

.field private final mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

.field private final mVisibleActions:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/PlusService$OperationStarter;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Lcom/google/android/gms/common/server/ClientContext;
    .param p4    # Lcom/google/android/gms/plus/service/PlusService$OperationStarter;
    .param p5    # [Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/IPlusService$Stub;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mGmsClientContext:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1}, Lcom/google/android/gms/common/images/ImageBroker;->acquireBroker(Landroid/content/Context;)Lcom/google/android/gms/common/images/ImageBroker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mImageBroker:Lcom/google/android/gms/common/images/ImageBroker;

    iput-object p4, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iput-object p5, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mVisibleActions:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public clearDefaultAccount()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/plus/service/PlusOperations$ClearDefaultAccountOperation;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v2, v3}, Lcom/google/android/gms/plus/service/PlusOperations$ClearDefaultAccountOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public deletePlusOne(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperations$DeletePlusOneOperation;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/PlusOperations$DeletePlusOneOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public disconnectSource(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/plus/service/PlusOperations$DisconnectSourceOperation;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v2, v3, p2, p3, p1}, Lcom/google/android/gms/plus/service/PlusOperations$DisconnectSourceOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 4

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.permission.GET_ACCOUNTS"

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Missing android.permission.GET_ACCOUNTS"

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getAuthIntentInternal(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 9
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->verifyUid()V

    iget-object v7, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v8, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mVisibleActions:[Ljava/lang/String;

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;[Ljava/lang/String;)V

    invoke-virtual {v7, v8, v0}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public getDefaultAccountNameInternal(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->verifyUid()V

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/plus/service/PlusOperations$ResolveDefaultAccountOperation;

    invoke-direct {v2, p2, p1}, Lcom/google/android/gms/plus/service/PlusOperations$ResolveDefaultAccountOperation;-><init>(Ljava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public getSignUpState(Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v4, 0x1

    invoke-direct {v2, v3, p1, v4}, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/IPlusCallbacks;I)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public insertPlusOne(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperations$InsertPlusOneOperation;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p3, p1}, Lcom/google/android/gms/plus/service/PlusOperations$InsertPlusOneOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public loadAudienceInternal(Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/plus/service/PlusOperations$LoadAudienceOperation;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v2, v3, p1}, Lcom/google/android/gms/plus/service/PlusOperations$LoadAudienceOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public loadConnectedApps(Lcom/google/android/gms/plus/internal/IPlusCallbacks;ILjava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v2, v3, p2, p3, p1}, Lcom/google/android/gms/plus/service/PlusOperations$LoadAppsOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public loadImage(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # Landroid/net/Uri;
    .param p3    # Landroid/os/Bundle;

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    const-string v1, "bounding_box"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mImageBroker:Lcom/google/android/gms/common/images/ImageBroker;

    invoke-direct {v3, v4, p2, v0, p1}, Lcom/google/android/gms/plus/service/PlusOperations$LoadImageOperation;-><init>(Lcom/google/android/gms/common/images/ImageBroker;Landroid/net/Uri;ILcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startImage(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public loadLinkPreviewInternal(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->verifyUid()V

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperations$LoadLinkPreviewOperation;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/PlusOperations$LoadLinkPreviewOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public loadMoments(Lcom/google/android/gms/plus/internal/IPlusCallbacks;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/net/Uri;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "The userId parameter is required."

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/service/PlusOperations$ListMomentOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public loadPeople(Lcom/google/android/gms/plus/internal/IPlusCallbacks;IIILjava/lang/String;)V
    .locals 7
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/service/PlusOperations$ListPeopleOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public loadPerson(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "The userId parameter is required."

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperations$GetPersonOperation;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/PlusOperations$GetPersonOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public loadPlusOne(Lcom/google/android/gms/plus/internal/IPlusCallbacks;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p2    # Ljava/lang/String;

    const-string v1, "URL must not be null."

    invoke-static {p2, v1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperations$LoadPlusOneOperation;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mGmsClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/PlusOperations$LoadPlusOneOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public removeMoment(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/plus/service/PlusOperations$RemoveMomentOperation;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/plus/service/PlusOperations$RemoveMomentOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public revokeAccessAndDisconnect(Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v2, v3, p1}, Lcom/google/android/gms/plus/service/PlusOperations$RevokeAccessOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public setDefaultAccountNameInternal(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->verifyUid()V

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/plus/service/PlusOperations$SetDefaultAccountOperation;

    invoke-direct {v2, p1, p2}, Lcom/google/android/gms/plus/service/PlusOperations$SetDefaultAccountOperation;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void
.end method

.method public writeMoment(Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    if-nez p1, :cond_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "momentJson must not be empty"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->toString()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v2, Lcom/google/android/gms/plus/service/PlusOperations$WriteMomentOperation;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mCallerClientContext:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v2, v3, v1}, Lcom/google/android/gms/plus/service/PlusOperations$WriteMomentOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mOperationStarter:Lcom/google/android/gms/plus/service/PlusService$OperationStarter;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusService$PlusServiceEntity;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4, v2}, Lcom/google/android/gms/plus/service/PlusService$OperationStarter;->startDefault(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    return-void

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "momentJson must be valid JSON"

    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method
