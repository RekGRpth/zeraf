.class public final Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GetAuthIntentOperation"
.end annotation


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field private final mCallingUid:I

.field private final mPackageName:Ljava/lang/String;

.field private final mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

.field private final mScopesString:Ljava/lang/String;

.field private final mVisibleActions:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;
    .param p6    # [Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mAccountName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mPackageName:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mCallingUid:I

    iput-object p4, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mScopesString:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    iput-object p6, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mVisibleActions:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v11, 0x4

    const/4 v10, 0x0

    const/4 v9, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mPackageName:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mCallingUid:I

    iget-object v3, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mScopesString:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mAccountName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mVisibleActions:[Ljava/lang/String;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/service/PlusOperationsUtils;->checkAuth(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/plus/service/PlusOperationsUtils$StatusCodeResolution;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v6

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v6}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v10, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    const-string v0, "pendingIntent"

    invoke-virtual {v8, v0, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-interface {v0, v11, v8, v9}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :catch_1
    move-exception v6

    const-string v0, "NetworkError"

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v9, v9}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v9, v9}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :catch_2
    move-exception v6

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-interface {v0, v11, v9, v9}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$GetAuthIntentOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method
