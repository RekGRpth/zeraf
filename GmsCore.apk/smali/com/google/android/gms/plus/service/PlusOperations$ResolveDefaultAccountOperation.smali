.class public final Lcom/google/android/gms/plus/service/PlusOperations$ResolveDefaultAccountOperation;
.super Ljava/lang/Object;
.source "PlusOperations.java"

# interfaces
.implements Lcom/google/android/gms/plus/service/OperationIntentService$Operation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/PlusOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResolveDefaultAccountOperation"
.end annotation


# instance fields
.field private final mCallingPackage:Ljava/lang/String;

.field private final mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/plus/internal/IPlusCallbacks;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/PlusOperations$ResolveDefaultAccountOperation;->mCallingPackage:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ResolveDefaultAccountOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/plus/broker/DataBroker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ResolveDefaultAccountOperation;->mCallingPackage:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/google/android/gms/common/account/AccountUtils;->getSelectedAccount(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v2, 0x0

    aget-object v2, v1, v2

    iget-object v0, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/plus/service/PlusOperations$ResolveDefaultAccountOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    invoke-interface {v2, v0}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onDefaultAccountResolvedInternal(Ljava/lang/String;)V

    return-void
.end method

.method public onFatalException(Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/PlusOperations$ResolveDefaultAccountOperation;->mPlusCallbacks:Lcom/google/android/gms/plus/internal/IPlusCallbacks;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/IPlusCallbacks;->onDefaultAccountResolvedInternal(Ljava/lang/String;)V

    return-void
.end method
