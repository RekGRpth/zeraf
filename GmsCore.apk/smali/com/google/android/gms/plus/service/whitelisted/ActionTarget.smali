.class public Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "ActionTarget.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypeArrays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "settingsNotificationType"

    const-string v2, "settingsNotificationType"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/SettingsNotificationType;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "labelId"

    const-string v2, "labelId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "iphFlowId"

    const-string v2, "iphFlowId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLong(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "frame"

    const-string v2, "frame"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedFrame;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "notificationWidgetPreReloadBuildLabel"

    const-string v2, "notificationWidgetPreReloadBuildLabel"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "gadgetPlayId"

    const-string v2, "gadgetPlayId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "volumeChange"

    const-string v2, "volumeChange"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/VolumeChange;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "activityDetails"

    const-string v2, "activityDetails"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/ActivityDetails;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "tab"

    const-string v2, "tab"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "notificationWidgetUpTimeBeforeReload"

    const-string v2, "notificationWidgetUpTimeBeforeReload"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "promoType"

    const-string v2, "promoType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "updateStreamPosition"

    const-string v2, "updateStreamPosition"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "photoAlbumIdDeprecated"

    const-string v2, "photoAlbumIdDeprecated"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "isUnreadNotification"

    const-string v2, "isUnreadNotification"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "plusEventId"

    const-string v2, "plusEventId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "gadgetId"

    const-string v2, "gadgetId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "numUnreadNotifications"

    const-string v2, "numUnreadNotifications"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "activityId"

    const-string v2, "activityId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "commentId"

    const-string v2, "commentId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "circle"

    const-string v2, "circle"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "externalUrl"

    const-string v2, "externalUrl"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "iphStepId"

    const-string v2, "iphStepId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "questionsOneboxQuery"

    const-string v2, "questionsOneboxQuery"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "deprecatedCircleId"

    const-string v2, "deprecatedCircleId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forLongs(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "connectSiteId"

    const-string v2, "connectSiteId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "rhsComponent"

    const-string v2, "rhsComponent"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponent;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "notificationSlot"

    const-string v2, "notificationSlot"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "photoCount"

    const-string v2, "photoCount"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "shareboxInfo"

    const-string v2, "shareboxInfo"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedShareboxInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "notificationId"

    const-string v2, "notificationId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "billboardImpression"

    const-string v2, "billboardImpression"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardImpression;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "gaiaId"

    const-string v2, "gaiaId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forStrings(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "notificationWidgetPostReloadBuildLabel"

    const-string v2, "notificationWidgetPostReloadBuildLabel"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "autoComplete"

    const-string v2, "autoComplete"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedAutoComplete;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "suggestionSummary"

    const-string v2, "suggestionSummary"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionSummaryInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "actionSource"

    const-string v2, "actionSource"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "intrCelebsClick"

    const-string v2, "intrCelebsClick"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedIntrCelebsClick;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "deprecatedSettingsNotificationType"

    const-string v2, "deprecatedSettingsNotificationType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forStrings(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "photoId"

    const-string v2, "photoId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "suggestionInfo"

    const-string v2, "suggestionInfo"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "photoAlbumId"

    const-string v2, "photoAlbumId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "billboardPromoAction"

    const-string v2, "billboardPromoAction"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardPromoAction;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "region"

    const-string v2, "region"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "profileData"

    const-string v2, "profileData"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "categoryId"

    const-string v2, "categoryId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "notificationTypes"

    const-string v2, "notificationTypes"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/NotificationTypes;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "entityTypeId"

    const-string v2, "entityTypeId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "photoAlbumType"

    const-string v2, "photoAlbumType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "shortcutTask"

    const-string v2, "shortcutTask"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forString(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "previousNumUnreadNotifications"

    const-string v2, "previousNumUnreadNotifications"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "ribbonClick"

    const-string v2, "ribbonClick"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonClick;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "page"

    const-string v2, "page"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forInteger(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "circleMember"

    const-string v2, "circleMember"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedCircleMember;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteTypeArray(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    const-string v1, "ribbonOrder"

    const-string v2, "ribbonOrder"

    const-class v3, Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonOrder;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypeArrays:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/Long;Lcom/google/android/gms/plus/service/whitelisted/LoggedFrame;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/VolumeChange;Lcom/google/android/gms/plus/service/whitelisted/ActivityDetails;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponent;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/gms/plus/service/whitelisted/LoggedShareboxInfo;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardImpression;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/LoggedAutoComplete;Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionSummaryInfo;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/LoggedIntrCelebsClick;Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardPromoAction;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/ArrayList;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonClick;Ljava/lang/Integer;Ljava/util/ArrayList;Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonOrder;)V
    .locals 4
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Long;
    .param p4    # Lcom/google/android/gms/plus/service/whitelisted/LoggedFrame;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/gms/plus/service/whitelisted/VolumeChange;
    .param p8    # Lcom/google/android/gms/plus/service/whitelisted/ActivityDetails;
    .param p9    # Ljava/lang/Integer;
    .param p10    # Ljava/lang/Integer;
    .param p11    # Ljava/lang/Integer;
    .param p12    # Ljava/lang/Integer;
    .param p13    # Ljava/lang/String;
    .param p14    # Ljava/lang/Boolean;
    .param p15    # Ljava/lang/String;
    .param p16    # Ljava/lang/String;
    .param p17    # Ljava/lang/Integer;
    .param p18    # Ljava/lang/String;
    .param p19    # Ljava/lang/String;
    .param p21    # Ljava/lang/String;
    .param p22    # Ljava/lang/String;
    .param p23    # Ljava/lang/String;
    .param p25    # Ljava/lang/Integer;
    .param p26    # Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponent;
    .param p27    # Ljava/lang/Integer;
    .param p28    # Ljava/lang/Integer;
    .param p29    # Lcom/google/android/gms/plus/service/whitelisted/LoggedShareboxInfo;
    .param p30    # Ljava/lang/String;
    .param p31    # Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardImpression;
    .param p33    # Ljava/lang/String;
    .param p34    # Lcom/google/android/gms/plus/service/whitelisted/LoggedAutoComplete;
    .param p35    # Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionSummaryInfo;
    .param p36    # Ljava/lang/String;
    .param p37    # Lcom/google/android/gms/plus/service/whitelisted/LoggedIntrCelebsClick;
    .param p39    # Ljava/lang/String;
    .param p41    # Ljava/lang/String;
    .param p42    # Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardPromoAction;
    .param p43    # Ljava/lang/String;
    .param p44    # Ljava/lang/String;
    .param p45    # Ljava/lang/Integer;
    .param p47    # Ljava/lang/Integer;
    .param p48    # Ljava/lang/Integer;
    .param p49    # Ljava/lang/String;
    .param p50    # Ljava/lang/Integer;
    .param p51    # Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonClick;
    .param p52    # Ljava/lang/Integer;
    .param p54    # Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonOrder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/SettingsNotificationType;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedFrame;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/whitelisted/VolumeChange;",
            "Lcom/google/android/gms/plus/service/whitelisted/ActivityDetails;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponent;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedShareboxInfo;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardImpression;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedAutoComplete;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionSummaryInfo;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedIntrCelebsClick;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardPromoAction;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/NotificationTypes;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonClick;",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircleMember;",
            ">;",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonOrder;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "settingsNotificationType"

    invoke-virtual {p0, v1, p1}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "labelId"

    invoke-virtual {p0, v1, p2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    const-string v1, "iphFlowId"

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setLong(Ljava/lang/String;J)V

    :cond_0
    const-string v1, "frame"

    invoke-virtual {p0, v1, p4}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "notificationWidgetPreReloadBuildLabel"

    invoke-virtual {p0, v1, p5}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "gadgetPlayId"

    invoke-virtual {p0, v1, p6}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "volumeChange"

    invoke-virtual {p0, v1, p7}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "activityDetails"

    invoke-virtual {p0, v1, p8}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p9, :cond_1

    const-string v1, "tab"

    invoke-virtual {p9}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_1
    if-eqz p10, :cond_2

    const-string v1, "notificationWidgetUpTimeBeforeReload"

    invoke-virtual {p10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_2
    if-eqz p11, :cond_3

    const-string v1, "promoType"

    invoke-virtual {p11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_3
    if-eqz p12, :cond_4

    const-string v1, "updateStreamPosition"

    invoke-virtual/range {p12 .. p12}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_4
    const-string v1, "photoAlbumIdDeprecated"

    move-object/from16 v0, p13

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p14, :cond_5

    const-string v1, "isUnreadNotification"

    invoke-virtual/range {p14 .. p14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setBoolean(Ljava/lang/String;Z)V

    :cond_5
    const-string v1, "plusEventId"

    move-object/from16 v0, p15

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "gadgetId"

    move-object/from16 v0, p16

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p17, :cond_6

    const-string v1, "numUnreadNotifications"

    invoke-virtual/range {p17 .. p17}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_6
    const-string v1, "activityId"

    move-object/from16 v0, p18

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "commentId"

    move-object/from16 v0, p19

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "circle"

    move-object/from16 v0, p20

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "externalUrl"

    move-object/from16 v0, p21

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "iphStepId"

    move-object/from16 v0, p22

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "questionsOneboxQuery"

    move-object/from16 v0, p23

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "deprecatedCircleId"

    move-object/from16 v0, p24

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setLongs(Ljava/lang/String;Ljava/util/ArrayList;)V

    if-eqz p25, :cond_7

    const-string v1, "connectSiteId"

    invoke-virtual/range {p25 .. p25}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_7
    const-string v1, "rhsComponent"

    move-object/from16 v0, p26

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p27, :cond_8

    const-string v1, "notificationSlot"

    invoke-virtual/range {p27 .. p27}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_8
    if-eqz p28, :cond_9

    const-string v1, "photoCount"

    invoke-virtual/range {p28 .. p28}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_9
    const-string v1, "shareboxInfo"

    move-object/from16 v0, p29

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "notificationId"

    move-object/from16 v0, p30

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "billboardImpression"

    move-object/from16 v0, p31

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "gaiaId"

    move-object/from16 v0, p32

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setStrings(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "notificationWidgetPostReloadBuildLabel"

    move-object/from16 v0, p33

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "autoComplete"

    move-object/from16 v0, p34

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "suggestionSummary"

    move-object/from16 v0, p35

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "actionSource"

    move-object/from16 v0, p36

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "intrCelebsClick"

    move-object/from16 v0, p37

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "deprecatedSettingsNotificationType"

    move-object/from16 v0, p38

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setStrings(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "photoId"

    move-object/from16 v0, p39

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "suggestionInfo"

    move-object/from16 v0, p40

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "photoAlbumId"

    move-object/from16 v0, p41

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "billboardPromoAction"

    move-object/from16 v0, p42

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v1, "region"

    move-object/from16 v0, p43

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "profileData"

    move-object/from16 v0, p44

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p45, :cond_a

    const-string v1, "categoryId"

    invoke-virtual/range {p45 .. p45}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_a
    const-string v1, "notificationTypes"

    move-object/from16 v0, p46

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    if-eqz p47, :cond_b

    const-string v1, "entityTypeId"

    invoke-virtual/range {p47 .. p47}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_b
    if-eqz p48, :cond_c

    const-string v1, "photoAlbumType"

    invoke-virtual/range {p48 .. p48}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_c
    const-string v1, "shortcutTask"

    move-object/from16 v0, p49

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p50, :cond_d

    const-string v1, "previousNumUnreadNotifications"

    invoke-virtual/range {p50 .. p50}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_d
    const-string v1, "ribbonClick"

    move-object/from16 v0, p51

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    if-eqz p52, :cond_e

    const-string v1, "page"

    invoke-virtual/range {p52 .. p52}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->setInteger(Ljava/lang/String;I)V

    :cond_e
    const-string v1, "circleMember"

    move-object/from16 v0, p53

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "ribbonOrder"

    move-object/from16 v0, p54

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addConcreteTypeArray(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getActionSource()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "actionSource"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getActivityDetails()Lcom/google/android/gms/plus/service/whitelisted/ActivityDetails;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "activityDetails"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/ActivityDetails;

    return-object v0
.end method

.method public getActivityId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "activityId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAutoComplete()Lcom/google/android/gms/plus/service/whitelisted/LoggedAutoComplete;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "autoComplete"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedAutoComplete;

    return-object v0
.end method

.method public getBillboardImpression()Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardImpression;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "billboardImpression"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardImpression;

    return-object v0
.end method

.method public getBillboardPromoAction()Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardPromoAction;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "billboardPromoAction"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedBillboardPromoAction;

    return-object v0
.end method

.method public getCategoryId()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "categoryId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getCircle()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircle;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "circle"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCircleMember()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedCircleMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "circleMember"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCommentId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "commentId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getConnectSiteId()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "connectSiteId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getDeprecatedCircleId()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "deprecatedCircleId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDeprecatedSettingsNotificationType()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "deprecatedSettingsNotificationType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getEntityTypeId()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "entityTypeId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getExternalUrl()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "externalUrl"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public getFrame()Lcom/google/android/gms/plus/service/whitelisted/LoggedFrame;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "frame"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedFrame;

    return-object v0
.end method

.method public getGadgetId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "gadgetId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getGadgetPlayId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "gadgetPlayId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getGaiaId()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "gaiaId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getIntrCelebsClick()Lcom/google/android/gms/plus/service/whitelisted/LoggedIntrCelebsClick;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "intrCelebsClick"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedIntrCelebsClick;

    return-object v0
.end method

.method public getIphFlowId()Ljava/lang/Long;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "iphFlowId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public getIphStepId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "iphStepId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLabelId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "labelId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "notificationId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationSlot()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "notificationSlot"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getNotificationTypes()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/NotificationTypes;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "notificationTypes"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNotificationWidgetPostReloadBuildLabel()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "notificationWidgetPostReloadBuildLabel"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationWidgetPreReloadBuildLabel()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "notificationWidgetPreReloadBuildLabel"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationWidgetUpTimeBeforeReload()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "notificationWidgetUpTimeBeforeReload"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getNumUnreadNotifications()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "numUnreadNotifications"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getPage()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "page"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getPhotoAlbumId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoAlbumId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoAlbumIdDeprecated()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoAlbumIdDeprecated"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoAlbumType()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoAlbumType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getPhotoCount()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoCount"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getPhotoId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "photoId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPlusEventId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "plusEventId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPreviousNumUnreadNotifications()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "previousNumUnreadNotifications"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getProfileData()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "profileData"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPromoType()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "promoType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getQuestionsOneboxQuery()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "questionsOneboxQuery"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRegion()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "region"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRhsComponent()Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponent;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "rhsComponent"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRhsComponent;

    return-object v0
.end method

.method public getRibbonClick()Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonClick;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "ribbonClick"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonClick;

    return-object v0
.end method

.method public getRibbonOrder()Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonOrder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "ribbonOrder"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedRibbonOrder;

    return-object v0
.end method

.method public getSettingsNotificationType()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/SettingsNotificationType;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "settingsNotificationType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getShareboxInfo()Lcom/google/android/gms/plus/service/whitelisted/LoggedShareboxInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "shareboxInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedShareboxInfo;

    return-object v0
.end method

.method public getShortcutTask()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "shortcutTask"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionInfo()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypeArrays:Ljava/util/HashMap;

    const-string v1, "suggestionInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSuggestionSummary()Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionSummaryInfo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "suggestionSummary"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/LoggedSuggestionSummaryInfo;

    return-object v0
.end method

.method public getTab()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "tab"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getUpdateStreamPosition()Ljava/lang/Integer;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "updateStreamPosition"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getVolumeChange()Lcom/google/android/gms/plus/service/whitelisted/VolumeChange;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "volumeChange"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/VolumeChange;

    return-object v0
.end method

.method protected isConcreteTypeArrayFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypeArrays:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isConcreteTypeFieldSet(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isIsUnreadNotification()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/ActionTarget;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "isUnreadNotification"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method
