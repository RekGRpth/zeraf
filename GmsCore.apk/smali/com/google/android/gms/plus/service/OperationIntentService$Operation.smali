.class public interface abstract Lcom/google/android/gms/plus/service/OperationIntentService$Operation;
.super Ljava/lang/Object;
.source "OperationIntentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/service/OperationIntentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Operation"
.end annotation


# virtual methods
.method public abstract execute(Landroid/content/Context;Lcom/google/android/gms/plus/broker/DataBroker;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;,
            Lcom/google/android/gms/auth/GoogleAuthException;
        }
    .end annotation
.end method

.method public abstract onFatalException(Ljava/lang/Exception;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
