.class public Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;
.super Lcom/google/android/gms/common/server/response/FastMapJsonResponse;
.source "DataCircleData.java"


# static fields
.field private static final sFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mConcreteTypes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->sFields:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->sFields:Ljava/util/HashMap;

    const-string v1, "deleted"

    const-string v2, "deleted"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forBoolean(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->sFields:Ljava/util/HashMap;

    const-string v1, "circleProperties"

    const-string v2, "circleProperties"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->sFields:Ljava/util/HashMap;

    const-string v1, "circleId"

    const-string v2, "circleId"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleId;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->sFields:Ljava/util/HashMap;

    const-string v1, "continuationToken"

    const-string v2, "continuationToken"

    const-class v3, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContinuationToken;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->forConcreteType(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->mConcreteTypes:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleId;Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContinuationToken;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;
    .param p2    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;
    .param p3    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleId;
    .param p4    # Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContinuationToken;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastMapJsonResponse;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->mConcreteTypes:Ljava/util/HashMap;

    if-eqz p1, :cond_0

    const-string v0, "deleted"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->setBoolean(Ljava/lang/String;Z)V

    :cond_0
    const-string v0, "circleProperties"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "circleId"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    const-string v0, "continuationToken"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method


# virtual methods
.method public addConcreteType(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->mConcreteTypes:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getCircleId()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleId;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "circleId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleId;

    return-object v0
.end method

.method public getCircleProperties()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "circleProperties"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleProperties;

    return-object v0
.end method

.method public getContinuationToken()Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContinuationToken;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->mConcreteTypes:Ljava/util/HashMap;

    const-string v1, "continuationToken"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataContinuationToken;

    return-object v0
.end method

.method public getFieldMappings()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse$Field",
            "<**>;>;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->sFields:Ljava/util/HashMap;

    return-object v0
.end method

.method public isDeleted()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/plusi/loadsocialnetwork/DataCircleData;->getValues()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "deleted"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method
