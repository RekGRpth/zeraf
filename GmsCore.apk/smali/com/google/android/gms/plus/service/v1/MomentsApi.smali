.class public final Lcom/google/android/gms/plus/service/v1/MomentsApi;
.super Lcom/google/android/gms/common/server/BaseApi;
.source "MomentsApi.java"


# instance fields
.field private final mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/BaseApiaryServer;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-direct {p0}, Lcom/google/android/gms/common/server/BaseApi;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1/MomentsApi;->mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    return-void
.end method

.method public static urlForList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Integer;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const-string v1, "people/%1$s/moments/%2$s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_0

    const-string v1, "maxResults"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz p3, :cond_1

    const-string v1, "pageToken"

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-eqz p4, :cond_2

    const-string v1, "targetUrl"

    invoke-static {p4}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    if-eqz p5, :cond_3

    const-string v1, "type"

    invoke-static {p5}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    return-object v0
.end method

.method public static urlForRemove(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;

    const-string v1, "moments/%1$s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public listBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 7
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Integer;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p9    # Lcom/google/android/gms/common/server/response/FieldMappingDictionary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/common/server/response/FastJsonResponse;",
            ">(",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/google/android/gms/common/server/response/FieldMappingDictionary;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-static/range {p2 .. p7}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->urlForList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsApi;->mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v1, p1

    move-object v5, p8

    move-object/from16 v6, p9

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/common/server/BaseApiaryServer;->getResponseBlocking(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    return-object v0
.end method

.method public removeBlocking(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1/MomentsApi;->urlForRemove(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1/MomentsApi;->mServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v0, v3}, Lcom/google/android/gms/common/server/BaseApiaryServer;->performNoResponseRequestBlocking(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method
