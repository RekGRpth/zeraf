.class public Lcom/google/android/gms/plus/audience/CircleSelectionActivity;
.super Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;
.source "CircleSelectionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/audience/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;
    }
.end annotation


# static fields
.field private static final CIRCLE_COMPARER:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private static final COLLATOR:Ljava/text/Collator;


# instance fields
.field private mEveryoneCheckbox:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->COLLATOR:Ljava/text/Collator;

    new-instance v0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity$1;

    invoke-direct {v0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->CIRCLE_COMPARER:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/text/Collator;
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->COLLATOR:Ljava/text/Collator;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->CIRCLE_COMPARER:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method public audienceMemberRemoved(Lcom/google/android/gms/common/people/views/AudienceView;Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/people/views/AudienceView;
    .param p2    # Lcom/google/android/gms/common/people/data/AudienceMember;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->audienceMemberRemoved(Lcom/google/android/gms/common/people/views/AudienceView;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    return-void
.end method

.method public audienceMemberRemoved(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
    .param p2    # Lcom/google/android/gms/common/people/data/AudienceMember;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->audienceMemberRemoved(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    return-void
.end method

.method protected createAdapter(Z)Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
    .locals 1
    .param p1    # Z

    new-instance v0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method

.method protected getResultBuilder()Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getResultBuilder()Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setEveryoneChecked(Z)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setClickable(Z)V

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getAudienceView()Lcom/google/android/gms/common/people/views/AudienceView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getAdapter()Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getCircles()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/people/views/AudienceView;->set(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getAdapter()Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getAdapter()Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getCircles()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->setAudienceSelections(Ljava/util/Collection;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->selectionChanged()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0073

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onCreateComplete(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getEveryoneCheckboxText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const v1, 0x7f0a0074

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getEveryoneCheckboxText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const v1, 0x7f0a0073

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0a0076

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0a0075

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->isEveryoneChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->onCreateComplete(Landroid/os/Bundle;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    const-string v2, "everyone.checkbox"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "everyone.checkbox"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected selectionChanged()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->selectionChanged()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0067

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method protected swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getAdapter()Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getAdapter()Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getCircles()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->setAudienceSelections(Ljava/util/Collection;)V

    :cond_0
    return-void
.end method
