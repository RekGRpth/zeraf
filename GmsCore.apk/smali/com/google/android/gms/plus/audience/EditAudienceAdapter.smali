.class Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
.super Landroid/widget/BaseAdapter;
.source "EditAudienceAdapter.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;,
        Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;,
        Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;
    }
.end annotation


# static fields
.field private static final AUDIENCE_COMPARER:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCircles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mListener:Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;

.field private final mPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private final mSearchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mSectionTitles:Z

.field private mSections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;",
            ">;"
        }
    .end annotation
.end field

.field private final mSelectedAudience:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private final mSystemGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$1;

    invoke-direct {v0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->AUDIENCE_COMPARER:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSearchResults:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mCircles:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mPeople:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSystemGroups:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mContext:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSectionTitles:Z

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->rebuildSections()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mListener:Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)Z
    .locals 1
    .param p0    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSectionTitles:Z

    return v0
.end method


# virtual methods
.method protected castOrInflate(Landroid/view/View;I)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "I)TT;"
        }
    .end annotation

    if-eqz p1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    if-lez p2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object p1, v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "No usable view found and no layout provided"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getAudienceSelections()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_2

    iget-object v7, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->getLength()I

    move-result v6

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v6, :cond_1

    invoke-virtual {v4, v2}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v7, v1, Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    invoke-virtual {v7, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    check-cast v1, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v3
.end method

.method protected getCircleView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # Lcom/google/android/gms/common/people/data/AudienceMember;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v2, 0x7f0200c9

    const v1, 0x7f040021

    invoke-virtual {p0, p2, v1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->castOrInflate(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->hideDescription()V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setIcon(I)V

    sget-object v1, Lcom/google/android/gms/common/people/data/AudienceMember;->PUBLIC:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0b0062

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setName(I)V

    const v1, 0x7f0200cc

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setIcon(I)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0031

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setDescription(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setName(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->getMemberCount()I

    move-result v1

    if-ltz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0032

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->getMemberCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setCountText(Ljava/lang/String;)V

    :goto_1
    new-instance v1, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;-><init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->isSelected(Lcom/google/android/gms/common/people/data/AudienceMember;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setChecked(Z)V

    return-object v0

    :cond_2
    sget-object v1, Lcom/google/android/gms/common/people/data/AudienceMember;->YOUR_CIRCLES:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f0b0063

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setName(I)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setIcon(I)V

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/google/android/gms/common/people/data/AudienceMember;->EXTENDED_CIRCLES:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f0b0064

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setName(I)V

    const v1, 0x7f0200cb

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setIcon(I)V

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/google/android/gms/common/people/data/AudienceMember;->DOMAIN:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0200ca

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setIcon(I)V

    goto :goto_0

    :cond_5
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setCountText(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected getCircles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mCircles:Ljava/util/List;

    return-object v0
.end method

.method public getCount()I
    .locals 4

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->getLength()I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method protected getGroupView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Lcom/google/android/gms/common/people/data/AudienceMember;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getCircleView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getHeaderView(Ljava/lang/String;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f040022

    invoke-virtual {p0, p2, v1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->castOrInflate(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AudienceSelectionHeaderView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionHeaderView;->setText(Ljava/lang/String;)V

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1    # I

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->getLength()I

    move-result v2

    if-ge p1, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    invoke-virtual {v3, p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    sub-int/2addr p1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    instance-of v2, v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v2, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Unknown item in adapter"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_0
    const/4 v2, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v2, 0x3

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected getPeople()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mPeople:Ljava/util/List;

    return-object v0
.end method

.method protected getPersonView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Lcom/google/android/gms/common/people/data/AudienceMember;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f040023

    invoke-virtual {p0, p2, v1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->castOrInflate(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AudienceSelectionPersonView;

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionPersonView;->setName(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$CheckedListener;-><init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionPersonView;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionPersonView;->setChecked(Z)V

    return-object v0
.end method

.method public getPositionForSection(I)I
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    iget-object v2, v2, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->getLength()I

    move-result v2

    add-int/2addr v1, v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method protected getSearchResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSearchResults:Ljava/util/List;

    return-object v0
.end method

.method public getSectionForPosition(I)I
    .locals 3
    .param p1    # I

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->getLength()I

    move-result v2

    sub-int/2addr p1, v2

    if-gez p1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected getSystemGroups()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSystemGroups:Ljava/util/List;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getItemViewType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected item type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getHeaderView(Ljava/lang/String;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_1
    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getGroupView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getCircleView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getPersonView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSelected(Lcom/google/android/gms/common/people/data/AudienceMember;)Z
    .locals 1
    .param p1    # Lcom/google/android/gms/common/people/data/AudienceMember;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected rebuildSections(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 19
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;",
            ">;"
        }
    .end annotation

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    if-nez p4, :cond_3

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    new-instance v1, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b002b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\u2620"

    const/4 v6, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;-><init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/audience/EditAudienceAdapter$1;)V

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    if-eqz p3, :cond_1

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    new-instance v4, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "\u25ef"

    const/4 v9, 0x0

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;-><init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/audience/EditAudienceAdapter$1;)V

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v16, Ljava/util/TreeMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/TreeMap;-><init>()V

    if-eqz p5, :cond_5

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v15

    const/4 v10, 0x0

    :goto_1
    if-ge v10, v15, :cond_4

    move-object/from16 v0, p5

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v12, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    move-object/from16 v0, p5

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    move-object/from16 v0, p4

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sget-object v1, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->AUDIENCE_COMPARER:Ljava/util/Comparator;

    invoke-static {v3, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual/range {v16 .. v16}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    new-instance v4, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v1, v2, v5}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;-><init>(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/audience/EditAudienceAdapter$1;)V

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    const-string v1, "Audience"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v18, 0x0

    const/16 v17, 0x0

    const/4 v10, 0x0

    :goto_3
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v10, v1, :cond_6

    invoke-virtual {v14, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->getLength()I

    move-result v1

    add-int v18, v18, v1

    invoke-virtual {v14, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;

    iget-object v1, v1, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int v17, v17, v1

    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_6
    const-string v1, "Audience"

    const-string v2, "Organized %d audience members (%d total items) into %d sections"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    return-object v14
.end method

.method rebuildSections()V
    .locals 6

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getSystemGroups()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getCircles()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getSearchResults()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getPeople()Ljava/util/List;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->rebuildSections(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setAudienceChangedListener(Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mListener:Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;

    return-void
.end method

.method public setAudienceSelections(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mListener:Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;->audienceChanged(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)V

    return-void
.end method

.method protected swapCircles(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mCircles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mCircles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method public swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->swapGroups(Ljava/util/List;)V

    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->swapCircles(Ljava/util/List;)V

    invoke-virtual {p0, p3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->swapPeople(Ljava/util/List;)V

    invoke-virtual {p0, p4}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->swapSearchResults(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->rebuildSections()V

    return-void
.end method

.method protected swapGroups(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSystemGroups:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSystemGroups:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method protected swapPeople(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mPeople:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mPeople:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method protected swapSearchResults(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method
