.class Lcom/google/android/gms/plus/audience/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;
.super Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
.source "CircleSelectionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/audience/CircleSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HiddenCircleAwareEditAudienceAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected bridge synthetic getCircleView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Lcom/google/android/gms/common/people/data/AudienceMember;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;->getCircleView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;

    move-result-object v0

    return-object v0
.end method

.method protected getCircleView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;
    .locals 3
    .param p1    # Lcom/google/android/gms/common/people/data/AudienceMember;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getCircleView(Lcom/google/android/gms/common/people/data/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->isHidden()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0030

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->setDescription(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionCircleView;->hideDescription()V

    goto :goto_0
.end method

.method protected swapCircles(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    # getter for: Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->CIRCLE_COMPARER:Ljava/util/Comparator;
    invoke-static {}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->access$100()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->swapCircles(Ljava/util/List;)V

    return-void
.end method
