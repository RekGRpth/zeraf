.class public Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "BaseAudienceSelectionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/people/views/AudienceView$AudienceChangedListener;
.implements Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;


# instance fields
.field private mAdapter:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

.field private mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

.field private mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

.field private mCircles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mCirclesLoaded:Z

.field private mGroups:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mGroupsLoaded:Z

.field private mPeople:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mPeopleLoaded:Z

.field protected mProgressBar:Landroid/view/View;

.field private mSearchResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field protected mViewList:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroupsLoaded:Z

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCirclesLoaded:Z

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mPeopleLoaded:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mPeople:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mSearchResults:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public audienceChanged(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->getAudienceSelections()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;->addAll(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->selectionChanged()V

    return-void
.end method

.method public audienceMemberAdded(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
    .param p2    # Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->audienceChanged(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;)V

    return-void
.end method

.method public audienceMemberRemoved(Lcom/google/android/gms/common/people/views/AudienceView;Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/people/views/AudienceView;
    .param p2    # Lcom/google/android/gms/common/people/data/AudienceMember;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;->getAudience()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->setAudienceSelections(Ljava/util/Collection;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->selectionChanged()V

    return-void
.end method

.method public audienceMemberRemoved(Lcom/google/android/gms/plus/audience/EditAudienceAdapter;Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
    .param p2    # Lcom/google/android/gms/common/people/data/AudienceMember;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;->remove(Lcom/google/android/gms/common/people/data/AudienceMember;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->selectionChanged()V

    return-void
.end method

.method protected cancel()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->finish()V

    return-void
.end method

.method protected createAdapter(Z)Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
    .locals 1
    .param p1    # Z

    new-instance v0, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method

.method protected getAdapter()Lcom/google/android/gms/plus/audience/EditAudienceAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    return-object v0
.end method

.method protected final getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    return-object v0
.end method

.method protected getAudienceView()Lcom/google/android/gms/common/people/views/AudienceView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    return-object v0
.end method

.method protected final getCirclesLoaded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCirclesLoaded:Z

    return v0
.end method

.method protected final getGroupsLoaded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroupsLoaded:Z

    return v0
.end method

.method protected getListView()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    return-object v0
.end method

.method protected final getPeopleLoaded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mPeopleLoaded:Z

    return v0
.end method

.method protected getResultBuilder()Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;
    .locals 2

    new-instance v0, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    const-class v1, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;->getAudience()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->setAudience(Ljava/util/ArrayList;)Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected ok()V
    .locals 2

    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->getResultBuilder()Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->build()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->finish()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0067

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->ok()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0068

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->cancel()V

    goto :goto_0
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const v12, 0x7f0a0017

    const/4 v11, 0x1

    const/4 v6, 0x0

    const v10, 0x7f0a0068

    const/16 v7, 0x8

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v5, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-direct {v5, v8}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent$Builder;->build()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    const v5, 0x7f04001f

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->setContentView(I)V

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getTitle()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getShowCancel()Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {p0, v10}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getOkText()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    const v5, 0x7f0a0067

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iget-object v8, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v8}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getOkText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v8, 0x7f040024

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getDescription()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v4, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v8}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getDescription()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    const v5, 0x7f0a0065

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    invoke-virtual {v5, v11}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    invoke-virtual {v5, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getSectionTitles()Z

    move-result v5

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->createAdapter(Z)Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    invoke-virtual {v5, p0}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->setAudienceChangedListener(Lcom/google/android/gms/plus/audience/EditAudienceAdapter$AudienceChangedListener;)V

    const v5, 0x7f0a0072

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v5, p0}, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;->setAudienceChangedListener(Lcom/google/android/gms/common/people/views/AudienceView$AudienceChangedListener;)V

    iget-object v8, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getShowChips()Z

    move-result v5

    if-eqz v5, :cond_6

    move v5, v6

    :goto_3
    invoke-virtual {v8, v5}, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;->setVisibility(I)V

    const v5, 0x7f0a0066

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mProgressBar:Landroid/view/View;

    const v5, 0x7f0a0067

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v10}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p1, :cond_9

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getAudience()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getKnownAudienceMembers()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    const/4 v1, 0x0

    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_a

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->isPerson()Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mPeople:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    const v5, 0x7f0b002a

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->setTitle(I)V

    goto/16 :goto_0

    :cond_4
    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getCancelText()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p0, v10}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iget-object v8, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v8}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getCancelText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_5
    invoke-virtual {v4, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_6
    move v5, v7

    goto :goto_3

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->isCircle()Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->isSystemGroup()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    const-string v5, "groups"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    const-string v5, "circles"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    const-string v5, "people"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mPeople:Ljava/util/ArrayList;

    const-string v5, "searchresults"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mSearchResults:Ljava/util/ArrayList;

    const-string v5, "groups.loaded"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroupsLoaded:Z

    const-string v5, "circles.loaded"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCirclesLoaded:Z

    const-string v5, "people.loaded"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mPeopleLoaded:Z

    const-string v5, "selections"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    :cond_a
    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    invoke-virtual {v5, v3}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->setAudienceSelections(Ljava/util/Collection;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v5, v3}, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;->set(Ljava/util/List;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    invoke-virtual {v5, v11}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->swapContents()V

    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->onCreateComplete(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->selectionChanged()V

    return-void
.end method

.method protected onCreateComplete(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "groups.loaded"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroupsLoaded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "circles.loaded"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCirclesLoaded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "people.loaded"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mPeopleLoaded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "groups"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "circles"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "people"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mPeople:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "searchresults"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mSearchResults:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "selections"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;->getAudience()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method protected selectionChanged()V
    .locals 2

    const v1, 0x7f0a0067

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceIntent:Lcom/google/android/gms/common/acl/AclSelectionIntent;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getAllowEmptySelection()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAudienceView:Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/views/AudienceView$ClickToRemove;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected setCircleLoadResults(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCirclesLoaded:Z

    return-void
.end method

.method protected setGroupLoadResults(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroupsLoaded:Z

    return-void
.end method

.method protected final swapContents()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mGroups:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mCircles:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mPeople:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mSearchResults:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->mAdapter:Lcom/google/android/gms/plus/audience/EditAudienceAdapter;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/audience/EditAudienceAdapter;->swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method
