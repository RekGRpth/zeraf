.class public final Lcom/google/android/gms/plus/audience/AudienceMemberConversions;
.super Ljava/lang/Object;
.source "AudienceMemberConversions.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toAudienceMembers(Lcom/google/android/gms/plus/service/whitelisted/AudiencesFeed;)Ljava/util/ArrayList;
    .locals 10
    .param p0    # Lcom/google/android/gms/plus/service/whitelisted/AudiencesFeed;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/plus/service/whitelisted/AudiencesFeed;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/AudiencesFeed;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v6, :cond_7

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/whitelisted/AudiencesFeed;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/whitelisted/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/whitelisted/Audience;->getItem()Lcom/google/android/gms/plus/service/whitelisted/PlusAclentryResource;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/plus/service/whitelisted/PlusAclentryResource;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/whitelisted/Audience;->getItem()Lcom/google/android/gms/plus/service/whitelisted/PlusAclentryResource;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/plus/service/whitelisted/PlusAclentryResource;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/whitelisted/Audience;->getItem()Lcom/google/android/gms/plus/service/whitelisted/PlusAclentryResource;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/plus/service/whitelisted/PlusAclentryResource;->getType()Ljava/lang/String;

    move-result-object v7

    const-string v8, "private"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/whitelisted/Audience;->getVisibility()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "limited"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/whitelisted/Audience;->getVisibility()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_1
    const-string v8, "circle"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v8, -0x1

    invoke-static {v3, v4, v8, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->forCircle(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    const-string v8, "myCircles"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "YOUR_CIRCLES"

    invoke-static {v8, v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    const-string v8, "extendedCircles"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    const-string v8, "EXTENDED_CIRCLES"

    invoke-static {v8, v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    const-string v8, "public"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    const-string v8, "PUBLIC"

    invoke-static {v8, v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->forGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    const-string v8, "domain"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-static {v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->forDomain(Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    return-object v5
.end method
