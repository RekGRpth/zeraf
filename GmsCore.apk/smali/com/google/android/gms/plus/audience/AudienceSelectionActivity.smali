.class public Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;
.super Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;
.source "AudienceSelectionActivity.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
.implements Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;


# instance fields
.field private mPlusClient:Lcom/google/android/gms/plus/PlusClient;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected cancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->cancel()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v0, 0x2328

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->cancel()V

    goto :goto_0
.end method

.method public onAudienceLoaded(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadGroups()Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->isSystemGroup()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->setGroupLoadResults(Ljava/util/ArrayList;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadCircles()Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->isCircle()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->setCircleLoadResults(Ljava/util/ArrayList;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->swapContents()V

    :cond_6
    return-void
.end method

.method public onConnected()V
    .locals 6

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadGroups()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getGroupsLoaded()Z

    move-result v5

    if-nez v5, :cond_3

    move v1, v3

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadCircles()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getCirclesLoaded()Z

    move-result v5

    if-nez v5, :cond_4

    move v0, v3

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadPeople()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getPeopleLoaded()Z

    move-result v5

    if-nez v5, :cond_5

    move v2, v3

    :goto_2
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v3, p0}, Lcom/google/android/gms/plus/PlusClient;->loadCirclesInternal(Lcom/google/android/gms/plus/PlusClient$OnAudienceLoadedListener;)V

    :cond_1
    if-eqz v2, :cond_2

    :cond_2
    return-void

    :cond_3
    move v1, v4

    goto :goto_0

    :cond_4
    move v0, v4

    goto :goto_1

    :cond_5
    move v2, v4

    goto :goto_2
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x2328

    :try_start_0
    invoke-virtual {p1, p0, v1}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    goto :goto_0
.end method

.method protected onCreateComplete(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadGroups()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getGroupsLoaded()Z

    move-result v6

    if-nez v6, :cond_2

    move v2, v4

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadCircles()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getCirclesLoaded()Z

    move-result v6

    if-nez v6, :cond_3

    move v1, v4

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadPeople()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getPeopleLoaded()Z

    move-result v6

    if-nez v6, :cond_4

    move v3, v4

    :goto_2
    if-nez v2, :cond_0

    if-nez v1, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mProgressBar:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setVisibility(I)V

    :cond_1
    new-instance v6, Lcom/google/android/gms/plus/PlusClient$Builder;

    invoke-direct {v6, p0, p0, p0}, Lcom/google/android/gms/plus/PlusClient$Builder;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/plus/PlusClient$Builder;->setAccountName(Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v6

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const-string v8, "https://www.googleapis.com/auth/plus.me"

    aput-object v8, v7, v5

    const-string v5, "https://www.googleapis.com/auth/emeraldsea.circles.read"

    aput-object v5, v7, v4

    invoke-virtual {v6, v7}, Lcom/google/android/gms/plus/PlusClient$Builder;->setScopes([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/plus/PlusClient$Builder;->build()Lcom/google/android/gms/plus/PlusClient;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    return-void

    :cond_2
    move v2, v5

    goto :goto_0

    :cond_3
    move v1, v5

    goto :goto_1

    :cond_4
    move v3, v5

    goto :goto_2
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mPlusClient:Lcom/google/android/gms/plus/PlusClient;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->onStop()V

    return-void
.end method

.method protected swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/audience/BaseAudienceSelectionActivity;->swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadGroups()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getGroupsLoaded()Z

    move-result v5

    if-nez v5, :cond_1

    move v1, v3

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadCircles()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getCirclesLoaded()Z

    move-result v5

    if-nez v5, :cond_2

    move v0, v3

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/gms/common/acl/AclSelectionIntent;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/acl/AclSelectionIntent;->getLoadPeople()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->getPeopleLoaded()Z

    move-result v5

    if-nez v5, :cond_3

    move v2, v3

    :goto_2
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    if-nez v2, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mProgressBar:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionActivity;->mViewList:Landroid/widget/ListView;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v1, v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    :cond_3
    move v2, v4

    goto :goto_2
.end method
