.class public Lcom/google/android/gms/plus/GooglePlusUtil;
.super Ljava/lang/Object;
.source "GooglePlusUtil.java"


# static fields
.field static final ENABLE_GOOGLE_PLUS_BUTTON:Ljava/lang/String; = "enable_google_plus_button"

.field static final ENABLE_GOOGLE_PLUS_TEXT:Ljava/lang/String; = "enable_google_plus_text"

.field static final ENABLE_GOOGLE_PLUS_TITLE:Ljava/lang/String; = "enable_google_plus_title"

.field static final GOOGLE_PLUS_MIN_VERSION:I = 0x13ab6680

.field static final INSTALL_GOOGLE_PLUS_BUTTON:Ljava/lang/String; = "install_google_plus_button"

.field static final INSTALL_GOOGLE_PLUS_TEXT:Ljava/lang/String; = "install_google_plus_text"

.field static final INSTALL_GOOGLE_PLUS_TITLE:Ljava/lang/String; = "install_google_plus_title"

.field private static final TAG:Ljava/lang/String;

.field static final UPDATE_GOOGLE_PLUS_BUTTON:Ljava/lang/String; = "update_google_plus_button"

.field static final UPDATE_GOOGLE_PLUS_TEXT:Ljava/lang/String; = "update_google_plus_text"

.field static final UPDATE_GOOGLE_PLUS_TITLE:Ljava/lang/String; = "update_google_plus_title"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/plus/GooglePlusUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/GooglePlusUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Cannot instantiate GooglePlusUtil"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static checkGooglePlusApp(Landroid/content/Context;)I
    .locals 1
    .param p0    # Landroid/content/Context;

    const v0, 0x13ab6680

    invoke-static {p0, v0}, Lcom/google/android/gms/plus/GooglePlusUtil;->checkGooglePlusApp(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method private static checkGooglePlusApp(Landroid/content/Context;I)I
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    :try_start_0
    const-string v5, "com.google.android.apps.plus"

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    const-string v5, "com.google.android.apps.plus"

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v5, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-ge v5, p1, :cond_1

    const/4 v4, 0x2

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-boolean v5, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v5, :cond_0

    const/4 v4, 0x3

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public static getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;
    .locals 4
    .param p0    # I
    .param p1    # Landroid/app/Activity;
    .param p2    # I

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected errorCode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "install_google_plus_title"

    const-string v2, "Get Google+"

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/GooglePlusUtil;->getStringFromPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "install_google_plus_text"

    const-string v2, "Download Google+ from Google Play so you can share this?"

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/GooglePlusUtil;->getStringFromPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "install_google_plus_button"

    const-string v2, "Get Google+"

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/GooglePlusUtil;->getStringFromPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/internal/DialogRedirect;

    const-string v3, "com.google.android.apps.plus"

    invoke-static {v3}, Lcom/google/android/gms/common/internal/GmsIntents;->createPlayStoreIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v2, p1, v3, p2}, Lcom/google/android/gms/common/internal/DialogRedirect;-><init>(Landroid/app/Activity;Landroid/content/Intent;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "enable_google_plus_title"

    const-string v2, "Enable Google+"

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/GooglePlusUtil;->getStringFromPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "enable_google_plus_text"

    const-string v2, "Enable Google+ from Google Play so you can share this?"

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/GooglePlusUtil;->getStringFromPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "enable_google_plus_button"

    const-string v2, "Enable Google+"

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/GooglePlusUtil;->getStringFromPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/internal/DialogRedirect;

    const-string v3, "com.google.android.apps.plus"

    invoke-static {v3}, Lcom/google/android/gms/common/internal/GmsIntents;->createSettingsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v2, p1, v3, p2}, Lcom/google/android/gms/common/internal/DialogRedirect;-><init>(Landroid/app/Activity;Landroid/content/Intent;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "update_google_plus_title"

    const-string v2, "Update Google+"

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/GooglePlusUtil;->getStringFromPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "update_google_plus_text"

    const-string v2, "Update Google+ from Google Play so you can share this?"

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/GooglePlusUtil;->getStringFromPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "update_google_plus_button"

    const-string v2, "Update"

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/plus/GooglePlusUtil;->getStringFromPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/internal/DialogRedirect;

    const-string v3, "com.google.android.apps.plus"

    invoke-static {v3}, Lcom/google/android/gms/common/internal/GmsIntents;->createPlayStoreIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v2, p1, v3, p2}, Lcom/google/android/gms/common/internal/DialogRedirect;-><init>(Landroid/app/Activity;Landroid/content/Intent;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method static getStringFromPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    :try_start_0
    const-string v3, "com.google.android.gms"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "string"

    const-string v4, "com.google.android.gms"

    invoke-virtual {v2, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object p2

    :goto_0
    return-object p2

    :catch_0
    move-exception v0

    sget-object v3, Lcom/google/android/gms/plus/GooglePlusUtil;->TAG:Ljava/lang/String;

    const-string v4, "Unable to load resources from GMS: GMS not installed."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v3, Lcom/google/android/gms/plus/GooglePlusUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to load resources from GMS: Resource \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\" not found."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception v0

    sget-object v3, Lcom/google/android/gms/plus/GooglePlusUtil;->TAG:Ljava/lang/String;

    const-string v4, "Unable to load resources from GMS."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
