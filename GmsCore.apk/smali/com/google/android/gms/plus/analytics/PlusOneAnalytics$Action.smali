.class public final Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;
.super Ljava/lang/Object;
.source "PlusOneAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Action"
.end annotation


# static fields
.field public static final CLICKED_PLUS_ONE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final CLICKED_SHARE_FROM_PLUS_ONE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final CLICKED_SHARE_FROM_PLUS_ONE_NO_PLUS_APP:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final GET_PLUS_ONES:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PLUS_ONE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PLUS_ONE_ABUSED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PLUS_ONE_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PLUS_ONE_CONFIRMATION_ERROR:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PLUS_ONE_CONFIRMED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PLUS_ONE_FRIENDS_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PLUS_ONE_PREVIEW_ERROR:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final PLUS_ONE_PREVIEW_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final READ_PLUS_ONE_ERROR:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final UNDO_PLUS_ONE_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final UNDO_PLUS_ONE_CONFIRMED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final WRITE_PLUS_ONE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

.field public static final WRITE_PLUS_ONE_ERROR:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->GET_PLUS_ONES:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x9

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xa

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_CONFIRMED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xb

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xc

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->UNDO_PLUS_ONE_CONFIRMED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xd

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->UNDO_PLUS_ONE_CANCELED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xe

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_ABUSED:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0xf

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->READ_PLUS_ONE_ERROR:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x10

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->WRITE_PLUS_ONE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x11

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->WRITE_PLUS_ONE_ERROR:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x12

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_CONFIRMATION_ERROR:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x13

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_FRIENDS_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x14

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_PREVIEW_SHOWN:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x15

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->CLICKED_PLUS_ONE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x16

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->CLICKED_SHARE_FROM_PLUS_ONE:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x17

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->PLUS_ONE_PREVIEW_ERROR:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    const/16 v0, 0x18

    # invokes: Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->build(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;
    invoke-static {v0}, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics;->access$000(I)Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/analytics/PlusOneAnalytics$Action;->CLICKED_SHARE_FROM_PLUS_ONE_NO_PLUS_APP:Lcom/google/android/gms/common/server/PlusAnalytics$FavaDiagnosticsEntity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
