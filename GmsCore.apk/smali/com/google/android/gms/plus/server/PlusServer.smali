.class public Lcom/google/android/gms/plus/server/PlusServer;
.super Lcom/google/android/gms/common/server/BaseApiaryServer;
.source "PlusServer.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/plus/server/PlusServer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/server/PlusServer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/common/server/BaseApiaryServer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected createHeaders(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/server/BaseApiaryServer;->createHeaders(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;->getInstance()Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/server/PlusServer;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/plus/server/PlusServer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/plus/broker/ContainerParam;->get(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->getInstance()Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getAuthPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/common/server/auth/AuthTokenTimeCache;->getTokenTime(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/common/server/SpamAndAbuseHeaders;->addHeaders(Landroid/content/Context;Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "User-Agent"

    invoke-static {p1}, Lcom/google/android/gms/plus/server/UserAgent;->get(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method
