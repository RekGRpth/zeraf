.class abstract Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;
.super Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;
.source "AccountSignUpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "AccountSignUpClientState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p2    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public requiresRestartOnDisconnect()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public start()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->showSpinner()V
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$100(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$AccountSignUpClientState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountSignUpClient:Lcom/google/android/gms/plus/activity/AccountSignUpClient;
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$200(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lcom/google/android/gms/plus/activity/AccountSignUpClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$State;->start()V

    goto :goto_0
.end method
