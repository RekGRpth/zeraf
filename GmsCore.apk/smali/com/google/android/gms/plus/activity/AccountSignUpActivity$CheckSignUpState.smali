.class Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;
.super Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SubActivityState;
.source "AccountSignUpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckSignUpState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$SubActivityState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
    .param p2    # Lcom/google/android/gms/plus/activity/AccountSignUpActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public isFinished()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mIsSignedUp:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$1000(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public onStart()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # invokes: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->hideSpinner()V
    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$800(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const-class v2, Lcom/google/android/gms/plus/PlusActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "authAccount"

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    # getter for: Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->access$400(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity$CheckSignUpState;->this$0:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
