.class Lcom/google/android/gms/plus/UnifiedSetupActivity$2;
.super Lcom/google/android/gms/auth/login/CancelableCallbackThread;
.source "UnifiedSetupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/plus/UnifiedSetupActivity;->startAuthTokenTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/UnifiedSetupActivity;Landroid/os/Message;)V
    .locals 0
    .param p2    # Landroid/os/Message;

    iput-object p1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$2;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    invoke-direct {p0, p2}, Lcom/google/android/gms/auth/login/CancelableCallbackThread;-><init>(Landroid/os/Message;)V

    return-void
.end method


# virtual methods
.method protected runInBackground()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$2;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    # getter for: Lcom/google/android/gms/plus/UnifiedSetupActivity;->mGlsUser:Lcom/google/android/gms/auth/login/GLSUser;
    invoke-static {v1}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->access$400(Lcom/google/android/gms/plus/UnifiedSetupActivity;)Lcom/google/android/gms/auth/login/GLSUser;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$2;->this$0:Lcom/google/android/gms/plus/UnifiedSetupActivity;

    # getter for: Lcom/google/android/gms/plus/UnifiedSetupActivity;->mSession:Lcom/google/android/gms/auth/login/GLSSession;
    invoke-static {v2}, Lcom/google/android/gms/plus/UnifiedSetupActivity;->access$300(Lcom/google/android/gms/plus/UnifiedSetupActivity;)Lcom/google/android/gms/auth/login/GLSSession;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/login/GLSUser;->checkGplus(Lcom/google/android/gms/auth/login/GLSSession;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$2;->mIsCanceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/gms/auth/login/Status;->USER_CANCEL:Lcom/google/android/gms/auth/login/Status;

    iget-object v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$2;->mCallbackMessage:Landroid/os/Message;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/login/Status;->toMessage(Landroid/os/Message;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "authtoken"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$2;->mCallbackMessage:Landroid/os/Message;

    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "intent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/gms/auth/login/Status;->SUCCESS:Lcom/google/android/gms/auth/login/Status;

    iget-object v2, p0, Lcom/google/android/gms/plus/UnifiedSetupActivity$2;->mCallbackMessage:Landroid/os/Message;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/login/Status;->toMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
