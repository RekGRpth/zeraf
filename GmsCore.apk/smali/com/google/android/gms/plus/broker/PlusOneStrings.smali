.class final Lcom/google/android/gms/plus/broker/PlusOneStrings;
.super Ljava/lang/Object;
.source "PlusOneStrings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/plus/broker/PlusOneStrings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/broker/PlusOneStrings;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static makeAnnotations(Landroid/content/res/Resources;Ljava/util/Locale;ZILjava/util/ArrayList;)Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;
    .locals 10
    .param p0    # Landroid/content/res/Resources;
    .param p1    # Ljava/util/Locale;
    .param p2    # Z
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/Locale;",
            "ZI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;"
        }
    .end annotation

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz p2, :cond_0

    const/4 v6, 0x1

    :goto_0
    rsub-int/lit8 v6, v6, 0x4

    invoke-static {v7, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    new-instance v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;

    invoke-direct {v1}, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;-><init>()V

    if-nez p2, :cond_2

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {p0, p1, p3}, Lcom/google/android/gms/plus/broker/PlusOneStrings;->makeBubbleAnnotation(Landroid/content/res/Resources;Ljava/util/Locale;I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    if-nez p3, :cond_1

    const v6, 0x7f0b000b

    invoke-virtual {p0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    const v6, 0x7f0b000c

    invoke-virtual {p0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    :goto_1
    return-object v1

    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    :cond_1
    const v6, 0x7f0b000d

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const v9, 0x7f0b000b

    invoke-virtual {p0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    const v6, 0x7f0b000d

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const v9, 0x7f0b000c

    invoke-virtual {p0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    const v6, 0x7f0b000e

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->shortText:Ljava/lang/String;

    goto :goto_1

    :cond_2
    if-eqz p2, :cond_5

    const/4 v6, 0x1

    :goto_2
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int v5, v6, v7

    if-ge p3, v5, :cond_4

    sget-object v6, Lcom/google/android/gms/plus/broker/PlusOneStrings;->TAG:Ljava/lang/String;

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    sget-object v7, Lcom/google/android/gms/plus/broker/PlusOneStrings;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Global count is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " but +1 has been "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p2, :cond_6

    const-string v6, "set by viewer"

    :goto_3
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p2, :cond_7

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_7

    const-string v6, " and "

    :goto_4
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "+1\'d by "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " friend(s)"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_5
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move p3, v5

    :cond_4
    invoke-static {p0, p1, p3}, Lcom/google/android/gms/plus/broker/PlusOneStrings;->makeBubbleAnnotation(Landroid/content/res/Resources;Ljava/util/Locale;I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    const/4 v0, 0x0

    if-eqz p2, :cond_a

    if-eqz p2, :cond_9

    const/4 v6, 0x1

    if-ne p3, v6, :cond_9

    const v6, 0x7f0b000f

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const v9, 0x7f0b0010

    invoke-virtual {p0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    const v6, 0x7f0b0010

    invoke-virtual {p0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    goto/16 :goto_1

    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_2

    :cond_6
    const-string v6, ""

    goto :goto_3

    :cond_7
    const-string v6, ""

    goto :goto_4

    :cond_8
    const-string v6, ""

    goto :goto_5

    :cond_9
    const v6, 0x7f0b0012

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const v9, 0x7f0b0010

    invoke-virtual {p0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_a
    const/4 v4, 0x0

    :goto_6
    if-ge v4, v3, :cond_c

    invoke-virtual {p4, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v0, :cond_b

    const v6, 0x7f0b0012

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_b
    const v6, 0x7f0b0011

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    const/4 v8, 0x1

    aput-object v2, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_c
    const v6, 0x7f0b000d

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v0, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    const v6, 0x7f0b000e

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method static makeBubbleAnnotation(Landroid/content/res/Resources;Ljava/util/Locale;I)Ljava/lang/String;
    .locals 10
    .param p0    # Landroid/content/res/Resources;
    .param p1    # Ljava/util/Locale;
    .param p2    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-ltz p2, :cond_0

    move v3, v4

    :goto_0
    const-string v6, "Count must be non-negative."

    invoke-static {v3, v6}, Lcom/google/android/gms/common/internal/Asserts;->checkState(ZLjava/lang/Object;)V

    const/4 v3, 0x2

    invoke-static {p2, v3}, Lcom/google/android/gms/plus/broker/PlusOneStrings;->toPrecision(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {p1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    const/16 v3, 0x3e8

    if-ge p2, v3, :cond_1

    int-to-long v3, p2

    invoke-virtual {v1, v3, v4}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    move v3, v5

    goto :goto_0

    :cond_1
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x6

    if-gt v0, v3, :cond_2

    const v3, 0x7f0b0007

    new-array v4, v4, [Ljava/lang/Object;

    int-to-double v6, v2

    const-wide v8, 0x408f400000000000L

    div-double/2addr v6, v8

    invoke-virtual {v1, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    const/16 v3, 0x9

    if-gt v0, v3, :cond_3

    const v3, 0x7f0b0008

    new-array v4, v4, [Ljava/lang/Object;

    int-to-double v6, v2

    const-wide v8, 0x412e848000000000L

    div-double/2addr v6, v8

    invoke-virtual {v1, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    const v3, 0x7f0b0009

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_4
    const/16 v3, 0x2710

    if-ge p2, v3, :cond_5

    int-to-long v3, p2

    invoke-virtual {v1, v3, v4}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_5
    const v3, 0x7f0b000a

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method static toPrecision(II)I
    .locals 2
    .param p0    # I
    .param p1    # I

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v1, Ljava/math/MathContext;

    invoke-direct {v1, p1}, Ljava/math/MathContext;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->round(Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    return v0
.end method
