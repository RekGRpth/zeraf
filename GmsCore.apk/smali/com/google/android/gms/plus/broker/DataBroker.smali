.class public Lcom/google/android/gms/plus/broker/DataBroker;
.super Ljava/lang/Object;
.source "DataBroker.java"


# static fields
.field private static sInstance:Lcom/google/android/gms/plus/broker/DataBroker;


# instance fields
.field private final mFramesAgent:Lcom/google/android/gms/plus/broker/FramesAgent;

.field private final mOAuthServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

.field private final mPlusSandboxAgent:Lcom/google/android/gms/plus/broker/PlusSandboxAgent;

.field private final mPlusV1Server:Lcom/google/android/gms/common/server/BaseApiaryServer;

.field private final mPlusWhitelistedAgent:Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;

.field private final mPlusWhitelistedServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

.field private final mPosAgent:Lcom/google/android/gms/plus/broker/PosAgent;

.field private final mPosRpcServer:Lcom/google/android/gms/plus/server/ApiaryRpcServer;

.field private final mPosServer:Lcom/google/android/gms/common/server/BaseApiaryServer;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Lcom/google/android/gms/plus/server/PlusServer;

    sget-object v2, Lcom/google/android/gms/plus/config/G;->posServerUrl:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/config/G;->posServerApiPath:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/plus/config/G;->cacheEnabled:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v5, Lcom/google/android/gms/plus/config/G;->verboseLogging:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/plus/config/G;->apiaryTrace:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/config/G;->posBackendOverride:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v7}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/server/PlusServer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPosServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    new-instance v0, Lcom/google/android/gms/plus/server/ApiaryRpcServer;

    sget-object v2, Lcom/google/android/gms/plus/config/G;->posServerUrl:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, ""

    sget-object v4, Lcom/google/android/gms/plus/config/G;->cacheEnabled:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v5, Lcom/google/android/gms/plus/config/G;->verboseLogging:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/plus/config/G;->apiaryTrace:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/config/G;->posBackendOverride:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v7}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/server/ApiaryRpcServer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPosRpcServer:Lcom/google/android/gms/plus/server/ApiaryRpcServer;

    new-instance v0, Lcom/google/android/gms/plus/broker/PosAgent;

    new-instance v2, Lcom/google/android/gms/plus/service/pos/PlusonesApi;

    iget-object v3, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPosServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-direct {v2, v3}, Lcom/google/android/gms/plus/service/pos/PlusonesApi;-><init>(Lcom/google/android/gms/common/server/BaseApiaryServer;)V

    new-instance v3, Lcom/google/android/gms/plus/service/rpc/PlusonesRpcApi;

    iget-object v4, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPosRpcServer:Lcom/google/android/gms/plus/server/ApiaryRpcServer;

    invoke-direct {v3, v4}, Lcom/google/android/gms/plus/service/rpc/PlusonesRpcApi;-><init>(Lcom/google/android/gms/plus/server/ApiaryRpcServer;)V

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/plus/broker/PosAgent;-><init>(Lcom/google/android/gms/plus/service/pos/PlusonesApi;Lcom/google/android/gms/plus/service/rpc/PlusonesRpcApi;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPosAgent:Lcom/google/android/gms/plus/broker/PosAgent;

    new-instance v0, Lcom/google/android/gms/plus/server/PlusServer;

    sget-object v2, Lcom/google/android/gms/plus/config/G;->plusWhitelistedServerUrl:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/config/G;->plusWhitelistedServerApiPath:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/plus/config/G;->cacheEnabled:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v5, Lcom/google/android/gms/plus/config/G;->verboseLogging:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/plus/config/G;->apiaryTrace:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/config/G;->plusWhitelistedBackendOverride:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v7}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/server/PlusServer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    new-instance v2, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;

    new-instance v3, Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-direct {v3, v0}, Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;-><init>(Lcom/google/android/gms/common/server/BaseApiaryServer;)V

    new-instance v4, Lcom/google/android/gms/plus/service/whitelisted/AudiencesApi;

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-direct {v4, v0}, Lcom/google/android/gms/plus/service/whitelisted/AudiencesApi;-><init>(Lcom/google/android/gms/common/server/BaseApiaryServer;)V

    new-instance v5, Lcom/google/android/gms/plus/service/whitelisted/RpcApi;

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-direct {v5, v0}, Lcom/google/android/gms/plus/service/whitelisted/RpcApi;-><init>(Lcom/google/android/gms/common/server/BaseApiaryServer;)V

    new-instance v6, Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-direct {v6, v0}, Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;-><init>(Lcom/google/android/gms/common/server/BaseApiaryServer;)V

    new-instance v7, Lcom/google/android/gms/plus/service/whitelisted/MomentsApi;

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-direct {v7, v0}, Lcom/google/android/gms/plus/service/whitelisted/MomentsApi;-><init>(Lcom/google/android/gms/common/server/BaseApiaryServer;)V

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;-><init>(Lcom/google/android/gms/plus/service/whitelisted/ActivitiesApi;Lcom/google/android/gms/plus/service/whitelisted/AudiencesApi;Lcom/google/android/gms/plus/service/whitelisted/RpcApi;Lcom/google/android/gms/plus/service/whitelisted/ApplicationsApi;Lcom/google/android/gms/plus/service/whitelisted/MomentsApi;)V

    iput-object v2, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedAgent:Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;

    new-instance v0, Lcom/google/android/gms/plus/server/PlusServer;

    sget-object v2, Lcom/google/android/gms/plus/config/G;->plusV1ServerUrl:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/config/G;->plusV1ServerApiPath:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/plus/config/G;->cacheEnabled:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v5, Lcom/google/android/gms/plus/config/G;->verboseLogging:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/plus/config/G;->apiaryTrace:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/config/G;->plusV1BackendOverride:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v7}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/server/PlusServer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusV1Server:Lcom/google/android/gms/common/server/BaseApiaryServer;

    new-instance v0, Lcom/google/android/gms/plus/broker/FramesAgent;

    iget-object v2, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusV1Server:Lcom/google/android/gms/common/server/BaseApiaryServer;

    new-instance v3, Lcom/google/android/gms/plus/service/v1/MomentsApi;

    iget-object v4, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusV1Server:Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-direct {v3, v4}, Lcom/google/android/gms/plus/service/v1/MomentsApi;-><init>(Lcom/google/android/gms/common/server/BaseApiaryServer;)V

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/plus/broker/FramesAgent;-><init>(Lcom/google/android/gms/common/server/BaseApiaryServer;Lcom/google/android/gms/plus/service/v1/MomentsApi;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mFramesAgent:Lcom/google/android/gms/plus/broker/FramesAgent;

    new-instance v0, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;

    new-instance v2, Lcom/google/android/gms/plus/service/v1/PeopleApi;

    iget-object v3, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusV1Server:Lcom/google/android/gms/common/server/BaseApiaryServer;

    invoke-direct {v2, v3}, Lcom/google/android/gms/plus/service/v1/PeopleApi;-><init>(Lcom/google/android/gms/common/server/BaseApiaryServer;)V

    invoke-direct {v0, v2}, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;-><init>(Lcom/google/android/gms/plus/service/v1/PeopleApi;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusSandboxAgent:Lcom/google/android/gms/plus/broker/PlusSandboxAgent;

    new-instance v0, Lcom/google/android/gms/plus/server/PlusServer;

    sget-object v2, Lcom/google/android/gms/plus/config/G;->oauthServerUrl:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/config/G;->oauthServerApiPath:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/plus/config/G;->cacheEnabled:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v5, Lcom/google/android/gms/plus/config/G;->verboseLogging:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/plus/config/G;->apiaryTrace:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/config/G;->oauthBackendOverride:Lcom/google/android/gms/common/config/GservicesValue;

    invoke-virtual {v7}, Lcom/google/android/gms/common/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/server/PlusServer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mOAuthServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    return-void
.end method

.method private getDefaultAccountClientContext(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/gms/common/account/AccountUtils;->getSelectedAccount(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingUid()I

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getRequestedAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getCallingPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getAuthPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/server/ClientContext;->setGrantedScopes(Lcom/google/android/gms/common/server/ClientContext;)V

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/gms/plus/broker/DataBroker;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/gms/plus/broker/DataBroker;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/plus/broker/DataBroker;->sInstance:Lcom/google/android/gms/plus/broker/DataBroker;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/broker/DataBroker;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/broker/DataBroker;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/plus/broker/DataBroker;->sInstance:Lcom/google/android/gms/plus/broker/DataBroker;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/broker/DataBroker;->sInstance:Lcom/google/android/gms/plus/broker/DataBroker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static logErrorResponseExternal(Lcom/android/volley/VolleyError;Ljava/lang/String;)V
    .locals 9
    .param p0    # Lcom/android/volley/VolleyError;
    .param p1    # Ljava/lang/String;

    const-string v6, "GooglePlusPlatform"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    iget-object v6, v6, Lcom/android/volley/NetworkResponse;->data:[B

    if-eqz v6, :cond_0

    const-string v6, "GooglePlusPlatform"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unexpected response code ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    iget v8, v8, Lcom/android/volley/NetworkResponse;->statusCode:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") when requesting: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    iget-object v2, v6, Lcom/android/volley/NetworkResponse;->data:[B

    const/4 v4, 0x0

    :try_start_0
    new-instance v5, Ljava/io/InputStreamReader;

    new-instance v6, Ljava/util/zip/GZIPInputStream;

    new-instance v7, Ljava/io/ByteArrayInputStream;

    invoke-direct {v7, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v6, v7}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x100

    new-array v0, v6, [C

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v5, v0}, Ljava/io/InputStreamReader;->read([C)I

    move-result v3

    if-ltz v3, :cond_1

    const/4 v6, 0x0

    invoke-virtual {v1, v0, v6, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v6

    move-object v4, v5

    :goto_1
    if-eqz v4, :cond_0

    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_2
    return-void

    :cond_1
    :try_start_3
    const-string v6, "GooglePlusPlatform"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error response: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v5, :cond_0

    :try_start_4
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v6

    goto :goto_2

    :catchall_0
    move-exception v6

    :goto_3
    if-eqz v4, :cond_2

    :try_start_5
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_2
    :goto_4
    throw v6

    :catch_2
    move-exception v6

    goto :goto_2

    :catch_3
    move-exception v7

    goto :goto_4

    :catchall_1
    move-exception v6

    move-object v4, v5

    goto :goto_3

    :catch_4
    move-exception v6

    goto :goto_1
.end method


# virtual methods
.method public deletePlusOne(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/util/Pair;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPosAgent:Lcom/google/android/gms/plus/broker/PosAgent;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/broker/DataBroker;->getProfilePhotoUrl(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/gms/plus/broker/PosAgent;->deletePlusOne(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public disconnectSource(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedAgent:Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->disconnectSource(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V

    return-void
.end method

.method public getAudience(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/ArrayList;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedAgent:Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->getAudience(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPersonBytes(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)[B
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusSandboxAgent:Lcom/google/android/gms/plus/broker/PlusSandboxAgent;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->getCurrentPersonBytes(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)[B
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "getPerson"

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/broker/DataBroker;->logErrorResponseExternal(Lcom/android/volley/VolleyError;Ljava/lang/String;)V

    throw v0
.end method

.method public getLinkPreview(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/data/plusone/LinkPreview;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/plus/broker/PlusCache;->getInstance()Lcom/google/android/gms/plus/broker/PlusCache;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/gms/plus/broker/PlusCache;->queryPreview(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/data/plusone/LinkPreview;-><init>(Landroid/content/ContentValues;)V

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedAgent:Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->getLinkPreview(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/data/plusone/LinkPreview;

    move-result-object v1

    goto :goto_0
.end method

.method public getPerson(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/server/response/SafeParcelResponse;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusSandboxAgent:Lcom/google/android/gms/plus/broker/PlusSandboxAgent;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->getPerson(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/server/response/SafeParcelResponse;
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "getPerson"

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/broker/DataBroker;->logErrorResponseExternal(Lcom/android/volley/VolleyError;Ljava/lang/String;)V

    throw v0
.end method

.method public getProfilePhotoUrl(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/plus/broker/DataBroker;->getSignUpState(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;I)Lcom/google/android/gms/plus/data/plusone/SignUpState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/plusone/SignUpState;->getProfileImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSignUpState(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;I)Lcom/google/android/gms/plus/data/plusone/SignUpState;
    .locals 16
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    const/4 v12, 0x1

    move/from16 v0, p3

    if-eq v0, v12, :cond_6

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/server/ClientContext;->getResolvedAccountName()Ljava/lang/String;

    move-result-object v12

    invoke-static {v6, v12}, Lcom/google/android/gms/plus/provider/PlusProviderUtils;->queryPlusAccount(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v2

    if-eqz v2, :cond_5

    :try_start_0
    invoke-virtual {v2}, Landroid/database/AbstractWindowedCursor;->moveToFirst()Z

    move-result v12

    if-eqz v12, :cond_5

    const-string v12, "signedUp"

    invoke-virtual {v2, v12}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/database/AbstractWindowedCursor;->getInt(I)I

    move-result v12

    if-nez v12, :cond_1

    const/4 v7, 0x0

    :goto_0
    if-nez v7, :cond_2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/plus/broker/DataBroker;->mPosAgent:Lcom/google/android/gms/plus/broker/PosAgent;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v12, v0, v1}, Lcom/google/android/gms/plus/broker/PosAgent;->getSignUpState(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/plus/data/plusone/SignUpState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_0
    :goto_1
    return-object v12

    :cond_1
    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v12, "updated"

    invoke-virtual {v2, v12}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-static {}, Lcom/google/android/gms/common/util/DefaultClock;->getInstance()Lcom/google/android/gms/common/util/Clock;

    move-result-object v12

    invoke-interface {v12}, Lcom/google/android/gms/common/util/Clock;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v2, v11}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v14

    sub-long v9, v12, v14

    const-wide/32 v12, 0x36ee80

    cmp-long v12, v9, v12

    if-lez v12, :cond_3

    new-instance v12, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;

    const/4 v13, 0x0

    const/4 v14, 0x1

    move-object/from16 v0, p2

    invoke-direct {v12, v0, v13, v14}, Lcom/google/android/gms/plus/service/PlusOperations$GetSignUpStateOperation;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/IPlusCallbacks;I)V

    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lcom/google/android/gms/plus/service/DefaultIntentService;->startOperation(Landroid/content/Context;Lcom/google/android/gms/plus/service/OperationIntentService$Operation;)V

    :cond_3
    const-string v12, "display_name"

    invoke-virtual {v2, v12}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v2, v12}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v12, "profile_image_url"

    invoke-virtual {v2, v12}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v2, v12}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v12, "signedUp"

    invoke-virtual {v2, v12}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v2, v12}, Landroid/database/AbstractWindowedCursor;->getInt(I)I

    move-result v12

    if-eqz v12, :cond_4

    const/4 v4, 0x1

    :goto_2
    new-instance v12, Lcom/google/android/gms/plus/data/plusone/SignUpState;

    invoke-direct {v12, v3, v5, v4}, Lcom/google/android/gms/plus/data/plusone/SignUpState;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/database/AbstractWindowedCursor;->close()V

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/plus/broker/DataBroker;->mPosAgent:Lcom/google/android/gms/plus/broker/PosAgent;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v12, v0, v1}, Lcom/google/android/gms/plus/broker/PosAgent;->getSignUpState(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/plus/data/plusone/SignUpState;

    move-result-object v12

    goto :goto_1

    :catchall_0
    move-exception v12

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_7
    throw v12
.end method

.method public insertFrame(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mFramesAgent:Lcom/google/android/gms/plus/broker/FramesAgent;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/plus/broker/FramesAgent;->insertFrame(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    return-void
.end method

.method public insertPlusOne(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPosAgent:Lcom/google/android/gms/plus/broker/PosAgent;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/broker/DataBroker;->getProfilePhotoUrl(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/broker/PosAgent;->insertPlusOne(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public listApplications(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;)Landroid/util/Pair;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/data/DataHolder;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedAgent:Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->listApplications(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public listMoments(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/MomentsFeed;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/net/Uri;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mFramesAgent:Lcom/google/android/gms/plus/broker/FramesAgent;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/plus/broker/FramesAgent;->listMoments(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/MomentsFeed;
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v8

    const-string v0, "listMoments"

    invoke-static {v8, v0}, Lcom/google/android/gms/plus/broker/DataBroker;->logErrorResponseExternal(Lcom/android/volley/VolleyError;Ljava/lang/String;)V

    throw v8
.end method

.method public listPeople(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;)Landroid/util/Pair;
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "III",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/data/DataHolder;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusSandboxAgent:Lcom/google/android/gms/plus/broker/PlusSandboxAgent;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/broker/PlusSandboxAgent;->listPeople(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;)Landroid/util/Pair;
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v7

    const-string v0, "listPeople"

    invoke-static {v7, v0}, Lcom/google/android/gms/plus/broker/DataBroker;->logErrorResponseExternal(Lcom/android/volley/VolleyError;Ljava/lang/String;)V

    throw v7
.end method

.method public loadPlusOne(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Landroid/util/Pair;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/lang/String;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/broker/DataBroker;->getDefaultAccountClientContext(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPosAgent:Lcom/google/android/gms/plus/broker/PosAgent;

    invoke-virtual {p0, p1, v2}, Lcom/google/android/gms/plus/broker/DataBroker;->getProfilePhotoUrl(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/broker/PosAgent;->getPlusOne(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public removeMoment(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mFramesAgent:Lcom/google/android/gms/plus/broker/FramesAgent;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/plus/broker/FramesAgent;->removeMoment(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    return-void
.end method

.method public revokeAccess(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/server/ClientContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    new-instance v2, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;

    invoke-direct {v2, p2}, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-virtual {v2, p1}, Lcom/google/android/gms/common/server/auth/AuthSessionAuthenticator;->get(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/revoke?token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mOAuthServer:Lcom/google/android/gms/common/server/BaseApiaryServer;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, p2, v3, v1, v4}, Lcom/google/android/gms/common/server/BaseApiaryServer;->performNoResponseRequestBlocking(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method public sendLogEvents(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;IZ)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p3    # I
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/server/ClientContext;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/plus/service/whitelisted/ClientOzEvent;",
            ">;IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mPlusWhitelistedAgent:Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/broker/PlusWhitelistedAgent;->sendLogEvents(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;IZ)V

    return-void
.end method

.method public uploadFrame(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/server/ClientContext;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/auth/GoogleAuthException;,
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/broker/DataBroker;->mFramesAgent:Lcom/google/android/gms/plus/broker/FramesAgent;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/gms/plus/broker/FramesAgent;->uploadFrame(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "writeMoment"

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/broker/DataBroker;->logErrorResponseExternal(Lcom/android/volley/VolleyError;Ljava/lang/String;)V

    throw v0
.end method
