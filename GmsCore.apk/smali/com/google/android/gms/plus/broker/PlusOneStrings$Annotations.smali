.class final Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;
.super Ljava/lang/Object;
.source "PlusOneStrings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/broker/PlusOneStrings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Annotations"
.end annotation


# instance fields
.field bubbleText:Ljava/lang/String;

.field longText:Ljava/lang/String;

.field shortText:Ljava/lang/String;

.field text:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->shortText:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->shortText:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->shortText:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    instance-of v1, p1, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;

    if-eqz v1, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->shortText:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->shortText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->shortText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/common/internal/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "Annotations[longText=%s text=%s, shortText=%s, bubbleText=%s]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->longText:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->text:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->shortText:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/plus/broker/PlusOneStrings$Annotations;->bubbleText:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
