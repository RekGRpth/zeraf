.class public final Lcom/google/android/gms/appdatasearch/SearchResults;
.super Ljava/lang/Object;
.source "SearchResults.java"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;,
        Lcom/google/android/gms/appdatasearch/SearchResults$Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/google/android/gms/appdatasearch/SearchResults$Result;",
        ">;",
        "Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/SearchResultsCreator;


# instance fields
.field mCorpusIds:[I

.field mCorpusNames:[Ljava/lang/String;

.field mErrorMessage:Ljava/lang/String;

.field mNumResults:I

.field mSectionBuffers:[Landroid/os/Bundle;

.field mSectionLengths:[Landroid/os/Bundle;

.field mTags:[Landroid/os/Bundle;

.field mUriBuffer:[B

.field mUriLengths:[I

.field mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/SearchResultsCreator;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/SearchResultsCreator;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/SearchResultsCreator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mVersionCode:I

    return-void
.end method

.method constructor <init>(I[I[Ljava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;)V
    .locals 0
    .param p1    # I
    .param p2    # [I
    .param p3    # [Ljava/lang/String;
    .param p4    # [I
    .param p5    # [B
    .param p6    # [Landroid/os/Bundle;
    .param p7    # [Landroid/os/Bundle;
    .param p8    # [Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mNumResults:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mCorpusIds:[I

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mCorpusNames:[Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mUriLengths:[I

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mUriBuffer:[B

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mTags:[Landroid/os/Bundle;

    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mSectionLengths:[Landroid/os/Bundle;

    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mSectionBuffers:[Landroid/os/Bundle;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mNumResults:I

    iput-object v1, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mCorpusIds:[I

    iput-object v1, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mCorpusNames:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/SearchResultsCreator;

    const/4 v0, 0x0

    return v0
.end method

.method public getNumResults()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mNumResults:I

    return v0
.end method

.method public hasError()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/gms/appdatasearch/SearchResults$Result;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/SearchResults;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;)V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/SearchResultsCreator;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/SearchResultsCreator;->writeToParcel(Lcom/google/android/gms/appdatasearch/SearchResults;Landroid/os/Parcel;I)V

    return-void
.end method
