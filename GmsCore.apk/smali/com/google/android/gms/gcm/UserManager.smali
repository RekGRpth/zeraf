.class public Lcom/google/android/gms/gcm/UserManager;
.super Ljava/lang/Object;
.source "UserManager.java"


# instance fields
.field private mEndpoint:Lcom/google/buzz/mobile/GcmClient;

.field private mForeground:I

.field private mSendFgStatus:Z

.field private mStopLock:Ljava/util/concurrent/Semaphore;

.field private mStopped:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/UserManager;->mStopped:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/UserManager;->mSendFgStatus:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/gcm/UserManager;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0    # Lcom/google/android/gms/gcm/UserManager;

    iget-object v0, p0, Lcom/google/android/gms/gcm/UserManager;->mStopLock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method private appendSerial(Ljava/lang/StringBuffer;I)V
    .locals 1
    .param p1    # Ljava/lang/StringBuffer;
    .param p2    # I

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    return-void
.end method


# virtual methods
.method public sendUserStatus()V
    .locals 7

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const/4 v5, -0x1

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gms/gcm/UserManager;->updateUserStatus(Ljava/util/Map;I)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v3, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-direct {v3}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;-><init>()V

    const-string v5, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v3, v5}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setCategory(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-direct {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setKey(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setValue(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-virtual {v3, v0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->addAppData(Lcom/google/buzz/mobile/proto/GCM$AppData;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/google/android/gms/gcm/UserManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Lcom/google/buzz/mobile/GcmClient;->sendPacket(Lcom/google/protobuf/micro/MessageMicro;Z)V

    :cond_1
    return-void
.end method

.method public sendUserStatus(Landroid/content/BroadcastReceiver;Landroid/content/Intent;)V
    .locals 13
    .param p1    # Landroid/content/BroadcastReceiver;
    .param p2    # Landroid/content/Intent;

    const/4 v12, 0x0

    invoke-virtual {p1}, Landroid/content/BroadcastReceiver;->goAsync()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v1

    const-string v9, "android.intent.extra.user_handle"

    const/4 v10, -0x1

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    const/4 v7, -0x1

    const-string v9, "android.intent.action.USER_STOPPING"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    move v7, v8

    new-instance v9, Ljava/util/concurrent/Semaphore;

    invoke-direct {v9, v12}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v9, p0, Lcom/google/android/gms/gcm/UserManager;->mStopLock:Ljava/util/concurrent/Semaphore;

    :cond_0
    const-string v9, "GCM/UserManager"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v9, "GCM/UserManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Send user status "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v4, 0x1

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, v6, v7}, Lcom/google/android/gms/gcm/UserManager;->updateUserStatus(Ljava/util/Map;I)Z

    move-result v9

    if-eqz v9, :cond_3

    new-instance v5, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-direct {v5}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;-><init>()V

    const-string v9, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v5, v9}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->setCategory(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    new-instance v0, Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-direct {v0}, Lcom/google/buzz/mobile/proto/GCM$AppData;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setKey(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v0, v9}, Lcom/google/buzz/mobile/proto/GCM$AppData;->setValue(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$AppData;

    invoke-virtual {v5, v0}, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;->addAppData(Lcom/google/buzz/mobile/proto/GCM$AppData;)Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    goto :goto_0

    :cond_2
    iget-object v9, p0, Lcom/google/android/gms/gcm/UserManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v9, v5, v12}, Lcom/google/buzz/mobile/GcmClient;->sendPacket(Lcom/google/protobuf/micro/MessageMicro;Z)V

    iget-object v9, p0, Lcom/google/android/gms/gcm/UserManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v9}, Lcom/google/buzz/mobile/GcmClient;->isActive()Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "android.intent.action.USER_STOPPING"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    const/4 v4, 0x0

    new-instance v9, Ljava/lang/Thread;

    new-instance v10, Lcom/google/android/gms/gcm/UserManager$1;

    invoke-direct {v10, p0, v1}, Lcom/google/android/gms/gcm/UserManager$1;-><init>(Lcom/google/android/gms/gcm/UserManager;Landroid/content/BroadcastReceiver$PendingResult;)V

    invoke-direct {v9, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    :cond_3
    if-eqz v4, :cond_4

    invoke-virtual {v1}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    :cond_4
    return-void
.end method

.method public setClient(Lcom/google/buzz/mobile/GcmClient;)V
    .locals 0
    .param p1    # Lcom/google/buzz/mobile/GcmClient;

    iput-object p1, p0, Lcom/google/android/gms/gcm/UserManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    return-void
.end method

.method public updateUserStatus(Ljava/util/Map;I)Z
    .locals 13
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)Z"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/gcm/Compat;->getUsers()Ljava/util/List;

    move-result-object v9

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Lcom/google/android/gms/gcm/Compat;->getCurrentUser()I

    move-result v10

    invoke-static {v10}, Lcom/google/android/gms/gcm/Compat;->getUserSerialNumber(I)I

    move-result v1

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/gcm/Compat;->getSerialNumber(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v3}, Lcom/google/android/gms/gcm/Compat;->isUserRunning(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v8, v6}, Lcom/google/android/gms/gcm/UserManager;->appendSerial(Ljava/lang/StringBuffer;I)V

    goto :goto_0

    :cond_1
    if-eq v6, v1, :cond_0

    invoke-direct {p0, v0, v6}, Lcom/google/android/gms/gcm/UserManager;->appendSerial(Ljava/lang/StringBuffer;I)V

    goto :goto_0

    :cond_2
    const/4 v10, -0x1

    if-eq p2, v10, :cond_3

    invoke-static {p2}, Lcom/google/android/gms/gcm/Compat;->getUserSerialNumber(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v8, v7}, Lcom/google/android/gms/gcm/UserManager;->appendSerial(Ljava/lang/StringBuffer;I)V

    :cond_3
    const/4 v4, 0x0

    iget-object v10, p0, Lcom/google/android/gms/gcm/UserManager;->mStopped:Ljava/util/List;

    invoke-interface {v5, v10}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v10

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/google/android/gms/gcm/UserManager;->mStopped:Ljava/util/List;

    invoke-interface {v10, v5}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v10

    if-nez v10, :cond_5

    :cond_4
    const/4 v4, 0x1

    :cond_5
    iget v10, p0, Lcom/google/android/gms/gcm/UserManager;->mForeground:I

    if-eq v10, v1, :cond_6

    iget-boolean v10, p0, Lcom/google/android/gms/gcm/UserManager;->mSendFgStatus:Z

    if-eqz v10, :cond_6

    const/4 v4, 0x1

    :cond_6
    iput-object v5, p0, Lcom/google/android/gms/gcm/UserManager;->mStopped:Ljava/util/List;

    iput v1, p0, Lcom/google/android/gms/gcm/UserManager;->mForeground:I

    const-string v10, "u:f"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {p1, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v10

    if-lez v10, :cond_7

    const-string v10, "u:b"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {p1, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v10

    if-lez v10, :cond_8

    const-string v10, "u:s"

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {p1, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    const-string v10, "GCM/UserManager"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_9

    const-string v10, "GCM/UserManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "UserStatus: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    return v4
.end method
