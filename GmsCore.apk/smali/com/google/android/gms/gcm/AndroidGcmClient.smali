.class final Lcom/google/android/gms/gcm/AndroidGcmClient;
.super Lcom/google/buzz/mobile/GcmClient;
.source "AndroidGcmClient.java"


# static fields
.field static sTestHook:Ljava/util/concurrent/BlockingQueue;


# instance fields
.field private mGcmManager:Lcom/google/android/gms/gcm/DataMessageManager;

.field private mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

.field private mProvisioning:Lcom/google/android/gms/gcm/GcmProvisioning;

.field private mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

.field private mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

.field private mService:Lcom/google/android/gms/gcm/GcmService;

.field private mUserManager:Lcom/google/android/gms/gcm/UserManager;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/gcm/GcmService;Lcom/google/android/gms/gcm/Rmq2Manager;Lcom/google/android/gms/gcm/HeartbeatAlarm;Lcom/google/android/gms/gcm/ReconnectManager;Lcom/google/android/gms/gcm/DataMessageManager;Lcom/google/android/gms/gcm/UserManager;Lcom/google/android/gms/gcm/GcmProvisioning;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/gcm/GcmService;
    .param p2    # Lcom/google/android/gms/gcm/Rmq2Manager;
    .param p3    # Lcom/google/android/gms/gcm/HeartbeatAlarm;
    .param p4    # Lcom/google/android/gms/gcm/ReconnectManager;
    .param p5    # Lcom/google/android/gms/gcm/DataMessageManager;
    .param p6    # Lcom/google/android/gms/gcm/UserManager;
    .param p7    # Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-direct {p0}, Lcom/google/buzz/mobile/GcmClient;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mService:Lcom/google/android/gms/gcm/GcmService;

    iput-object p2, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mPort:I

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Rmq2Manager;->getInitialOutgoinPersistentId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mRmqId:J

    iput-object p3, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->setClient(Lcom/google/buzz/mobile/GcmClient;)V

    iput-object p4, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/gcm/ReconnectManager;->setClient(Lcom/google/buzz/mobile/GcmClient;)V

    iput-object p5, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mGcmManager:Lcom/google/android/gms/gcm/DataMessageManager;

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mGcmManager:Lcom/google/android/gms/gcm/DataMessageManager;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/gcm/DataMessageManager;->setClient(Lcom/google/buzz/mobile/GcmClient;)V

    iput-object p6, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mUnackedS2dIds:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

    invoke-virtual {v1}, Lcom/google/android/gms/gcm/Rmq2Manager;->getS2dIds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-object p7, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mProvisioning:Lcom/google/android/gms/gcm/GcmProvisioning;

    return-void
.end method

.method public static logConnectionClosed(III)V
    .locals 5
    .param p0    # I
    .param p1    # I
    .param p2    # I

    shl-int/lit8 v1, p1, 0x8

    add-int v0, v1, p0

    const v1, 0x31ce3

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    return-void
.end method

.method public static logConnectionEvent(IIII)V
    .locals 3
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    shl-int/lit8 v1, p0, 0x18

    shl-int/lit8 v2, p1, 0x10

    add-int/2addr v1, v2

    shl-int/lit8 v2, p2, 0x8

    add-int/2addr v1, v2

    add-int v0, v1, p3

    const v1, 0x31ce2

    invoke-static {v1, v0}, Landroid/util/EventLog;->writeEvent(II)I

    return-void
.end method


# virtual methods
.method protected beforeLogin(Lcom/google/buzz/mobile/proto/GCM$LoginRequest;)V
    .locals 7
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    iget-object v5, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    if-eqz v5, :cond_0

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iget-object v5, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mUserManager:Lcom/google/android/gms/gcm/UserManager;

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Lcom/google/android/gms/gcm/UserManager;->updateUserStatus(Ljava/util/Map;I)Z

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    new-instance v2, Lcom/google/buzz/mobile/proto/GCM$Setting;

    invoke-direct {v2}, Lcom/google/buzz/mobile/proto/GCM$Setting;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/buzz/mobile/proto/GCM$Setting;->setName(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$Setting;

    invoke-virtual {v2, v4}, Lcom/google/buzz/mobile/proto/GCM$Setting;->setValue(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$Setting;

    invoke-virtual {p1, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->addSetting(Lcom/google/buzz/mobile/proto/GCM$Setting;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected log(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;

    const-string v0, "GCM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    const-string v0, "GCM"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "GCM"

    invoke-static {v0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected onAckReceived(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

    iget-wide v1, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mRmqId:J

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/gcm/Rmq2Manager;->removeMessagesByRmq2Ids(Ljava/util/List;J)I

    return-void
.end method

.method protected onDisconnected(ILjava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v4, "GCM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Disconnected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    invoke-virtual {v4}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->releaseWakelock()V

    iget-object v4, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    invoke-virtual {v4}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->clearAlarm()V

    iget-object v4, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {v4, p2}, Lcom/google/android/gms/gcm/ReconnectManager;->onDisconnected(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {v4}, Lcom/google/android/gms/gcm/ReconnectManager;->getNetworkState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {v4}, Lcom/google/android/gms/gcm/ReconnectManager;->getNetworkType()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/AndroidGcmClient;->getLastConnectionTime()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    long-to-int v4, v4

    invoke-static {p1, v3, v4}, Lcom/google/android/gms/gcm/AndroidGcmClient;->logConnectionClosed(III)V

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/AndroidGcmClient;->getState()I

    move-result v5

    invoke-virtual {v2}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v6

    invoke-static {v4, v5, p1, v6}, Lcom/google/android/gms/gcm/AndroidGcmClient;->logConnectionEvent(IIII)V

    goto :goto_0
.end method

.method protected onHearbeatSent()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->releaseWakelock()V

    return-void
.end method

.method protected onLoginResponse(Lcom/google/buzz/mobile/proto/GCM$LoginResponse;)V
    .locals 2
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$LoginResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "GCM"

    const-string v1, "Connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->onLoginResponse(Lcom/google/buzz/mobile/proto/GCM$LoginResponse;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mReconnectManager:Lcom/google/android/gms/gcm/ReconnectManager;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/ReconnectManager;->onLoginResponse(Lcom/google/buzz/mobile/proto/GCM$LoginResponse;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mProvisioning:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/GcmProvisioning;->disableGSFConnection()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/gcm/Rmq2Manager;->resendPackets(Lcom/google/buzz/mobile/GcmClient;)V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/common/GoogleLogTags;->writeGtalkConnection(I)V

    sget-object v0, Lcom/google/android/gms/gcm/AndroidGcmClient;->sTestHook:Ljava/util/concurrent/BlockingQueue;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/gcm/AndroidGcmClient;->sTestHook:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected onMessage(Lcom/google/protobuf/micro/MessageMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "GCM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->markPacketReception()V

    instance-of v0, p1, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/gcm/AndroidGcmClient;->sTestHook:Ljava/util/concurrent/BlockingQueue;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/gcm/AndroidGcmClient;->sTestHook:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mService:Lcom/google/android/gms/gcm/GcmService;

    iget-object v0, v0, Lcom/google/android/gms/gcm/GcmService;->mGCMManager:Lcom/google/android/gms/gcm/DataMessageManager;

    check-cast p1, Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/DataMessageManager;->processPacket(Lcom/google/buzz/mobile/proto/GCM$DataMessageStanza;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    instance-of v0, p1, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mHeartbeat:Lcom/google/android/gms/gcm/HeartbeatAlarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/HeartbeatAlarm;->markPacketReception()V

    goto :goto_0
.end method

.method protected onMessageOut(Lcom/google/protobuf/micro/MessageMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;

    sget-object v0, Lcom/google/android/gms/gcm/AndroidGcmClient;->sTestHook:Ljava/util/concurrent/BlockingQueue;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/gcm/AndroidGcmClient;->sTestHook:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Message written:  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/AndroidGcmClient;->log(Ljava/lang/String;)V

    return-void
.end method

.method protected onS2DConfirmed(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/Rmq2Manager;->deleteS2dIds(Ljava/util/List;)V

    return-void
.end method

.method protected onS2DReceived(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/Rmq2Manager;->addS2dId(Ljava/lang/String;)V

    return-void
.end method

.method protected onSaveMessage(Ljava/lang/String;BLcom/google/protobuf/micro/MessageMicro;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .param p3    # Lcom/google/protobuf/micro/MessageMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/gcm/AndroidGcmClient;->mRmq2:Lcom/google/android/gms/gcm/Rmq2Manager;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/gms/gcm/Rmq2Manager;->enqueueMessage(IBLcom/google/protobuf/micro/MessageMicro;)V

    return-void
.end method
