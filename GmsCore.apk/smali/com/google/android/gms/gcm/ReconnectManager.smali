.class public Lcom/google/android/gms/gcm/ReconnectManager;
.super Landroid/content/BroadcastReceiver;
.source "ReconnectManager.java"


# instance fields
.field private mAirplaneModeTurnedOffTimeStamp:J

.field private mAirplaneModeTurnedOnTimeStamp:J

.field mAlarm:Lcom/google/android/gms/gcm/Alarm;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mDeviceStorageLow:Z

.field private mEndpoint:Lcom/google/buzz/mobile/GcmClient;

.field private mInMobileHipriorityMode:Z

.field private mInitialReconnectDelay:J

.field mLastInetReportSuccessful:Z

.field private mLastMobileNetworkOutageTs:J

.field private mLastNetworkAvailable:Z

.field private mLastNetworkBroadcastTs:J

.field private mLock:Ljava/lang/Object;

.field private mMaxReconnectDelay:J

.field private mMinReconnectDelayLong:I

.field private mMinReconnectDelayShort:I

.field mNetworkState:Landroid/net/NetworkInfo$State;

.field private mNetworkSuspended:Z

.field mNetworkType:I

.field mNotifyNetworkState:Landroid/net/NetworkInfo$State;

.field mNotifyNetworkType:I

.field private mRandomGenerator:Ljava/util/Random;

.field private mReconnectAlarmSet:Z

.field private mReconnectBackoffRateMultiplier:D

.field private mReconnectDelay:J

.field private mReconnectVariantLong:I

.field private mReconnectVariantShort:I

.field private mWifiDisconnectedTimeStamp:J

.field private shortNetworkDowntime:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mRandomGenerator:Ljava/util/Random;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkSuspended:Z

    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    iput-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    iput-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mNotifyNetworkState:Landroid/net/NetworkInfo$State;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    new-instance v0, Lcom/google/android/gms/gcm/Alarm;

    iget-object v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mContext:Landroid/content/Context;

    const-string v2, "GTALK_CONN_ALARM"

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/gms/gcm/Alarm;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    const-string v1, "com.google.android.intent.action.GTALK_RECONNECT"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/Alarm;->setAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->init()V

    return-void
.end method

.method private checkThrottleReconnect(JJ)Z
    .locals 7
    .param p1    # J
    .param p3    # J

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v3, v0, p1

    const-wide/16 v5, 0x4e20

    cmp-long v3, v3, v5

    if-gtz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    sub-long v3, v0, p3

    const-wide/32 v5, 0xea60

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    const-string v2, "checkThrottleReconnect = true"

    invoke-direct {p0, v2}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private clearNetworkOutageTimestamp()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mLastMobileNetworkOutageTs:J

    return-void
.end method

.method private handleAirplaneModeChanged(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const-wide/16 v3, 0x0

    const-string v1, "state"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleAirplaneModeChanged: airplaneModeOn="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAirplaneModeTurnedOnTimeStamp:J

    iput-wide v3, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAirplaneModeTurnedOffTimeStamp:J

    :goto_0
    return-void

    :cond_0
    iput-wide v3, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAirplaneModeTurnedOnTimeStamp:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAirplaneModeTurnedOffTimeStamp:J

    goto :goto_0
.end method

.method private handleBackgroundDataSettingChange()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->isBackgroundDataEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->retryConnection(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/buzz/mobile/GcmClient;->disconnect(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private handleConnectivityChanged(Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Intent;

    const/4 v3, 0x0

    const-string v0, "noConnectivity"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    const-string v0, "networkInfo"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    const-string v0, "GCM/Reconnect"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "reason"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v0, "isFailover"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connectivity status: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v8, :cond_1

    const-string v0, "NO CONNECTIVITY"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " state="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo$State;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v9, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " reason="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v6, :cond_5

    const-string v0, " failover"

    :goto_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    const-string v0, "otherNetwork"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/net/NetworkInfo;

    if-eqz v7, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "    net2.type= "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " net2.state="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo$State;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_0
    :goto_5
    if-nez v1, :cond_a

    :goto_6
    return-void

    :cond_1
    const-string v0, ""

    goto/16 :goto_0

    :cond_2
    const-string v0, "unknown"

    goto :goto_1

    :cond_3
    const-string v0, "unknown"

    goto :goto_2

    :cond_4
    const-string v0, ""

    goto :goto_3

    :cond_5
    const-string v0, ""

    goto :goto_4

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connectivity status: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v8, :cond_7

    const-string v0, "NO CONNECTIVITY"

    :goto_7
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", state="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    :goto_9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    goto :goto_5

    :cond_7
    const-string v0, ""

    goto :goto_7

    :cond_8
    const-string v0, ""

    goto :goto_8

    :cond_9
    const-string v0, ""

    goto :goto_9

    :cond_a
    iget-wide v2, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAirplaneModeTurnedOnTimeStamp:J

    iget-wide v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAirplaneModeTurnedOffTimeStamp:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/gcm/ReconnectManager;->networkStateChanged(Landroid/net/NetworkInfo;JJ)V

    goto :goto_6
.end method

.method private isBackgroundDataEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    return v0
.end method

.method private isWanMobileNetwork(I)Z
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x7

    if-eq p1, v1, :cond_0

    const/16 v1, 0x9

    if-eq p1, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const-string v0, "GCM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "GCM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/GcmClient;->isActive()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "A "

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->getAlarmTimer()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/GcmClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "a "

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method private pollNetworkAvailable()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mLastNetworkAvailable:Z

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/gcm/ReconnectManager;->setInternalNetworkState(Landroid/net/NetworkInfo$State;I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mLastNetworkAvailable:Z

    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    const/4 v2, -0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/gcm/ReconnectManager;->setInternalNetworkState(Landroid/net/NetworkInfo$State;I)V

    goto :goto_0
.end method

.method private reportInetCondition(ZI)V
    .locals 9
    .param p1    # Z
    .param p2    # I

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mLock:Ljava/lang/Object;

    monitor-enter v4

    if-eqz p1, :cond_0

    const/16 v1, 0x64

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v5, "reportInetCondition"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mLastInetReportSuccessful:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    monitor-exit v4

    return-void

    :catch_0
    move-exception v2

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mLastInetReportSuccessful:Z

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private setDeviceStorageLow(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mDeviceStorageLow:Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private setInternalNetworkState(Landroid/net/NetworkInfo$State;I)V
    .locals 2
    .param p1    # Landroid/net/NetworkInfo$State;
    .param p2    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setInternalNetworkState: type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    sget-object v0, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkSuspended:Z

    iput-object p1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    iput p2, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkType:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldResetReconnectTimer()Z
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v4}, Lcom/google/buzz/mobile/GcmClient;->getLastConnectionAttemptSuccessful()Z

    move-result v0

    iget-object v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v4}, Lcom/google/buzz/mobile/GcmClient;->getLastConnectionTime()J

    move-result-wide v4

    const-wide/16 v6, 0xa

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    move v1, v2

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    :goto_1
    if-nez v2, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "shouldResetReconnectTimer: lastConnectionWasOfMininumDuration="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", lastAttemptSuccessful="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_0
    return v2

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method


# virtual methods
.method public cancelReconnectAlarm()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectAlarmSet:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectAlarmSet:Z

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->stop()V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ensureRouteOverMobileHipriNetworkInterface()Z
    .locals 7

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v5}, Lcom/google/buzz/mobile/GcmClient;->isConnected()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    iget-object v5, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v5}, Lcom/google/buzz/mobile/GcmClient;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    const/4 v5, 0x3

    aget-byte v5, v0, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x18

    const/4 v6, 0x2

    aget-byte v6, v0, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    or-int/2addr v5, v6

    aget-byte v6, v0, v4

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    or-int v2, v5, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "--- requestRouteToHost for TYPE_MOBILE_HIPRI, host_addr="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/4 v5, 0x5

    invoke-virtual {v3, v5, v2}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "GCM/Reconnect"

    const-string v5, "requestRouteToHost: failed!"

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move v3, v4

    goto :goto_0
.end method

.method public getNetworkState()Landroid/net/NetworkInfo$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    return-object v0
.end method

.method public getNetworkType()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkType:I

    return v0
.end method

.method public getReconnectTime()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->getNextAlarmTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public handleDeviceStorageLow()V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->setDeviceStorageLow(Z)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/buzz/mobile/GcmClient;->disconnect(ILjava/lang/String;)V

    return-void
.end method

.method public handleDeviceStorageOk()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->setDeviceStorageLow(Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->retryConnection(Z)V

    return-void
.end method

.method init()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->clearNetworkOutageTimestamp()V

    invoke-direct {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->pollNetworkAvailable()V

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->initFromSettings()V

    return-void
.end method

.method initFromSettings()V
    .locals 7

    const/16 v6, 0x2710

    iget-object v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "gms_max_reconnect_delay"

    const v5, 0x493e0

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mMaxReconnectDelay:J

    const-string v4, "gms_min_reconnect_delay_short"

    const/16 v5, 0x1388

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mMinReconnectDelayShort:I

    const-string v4, "gtalk_reconnect_variant_short"

    invoke-static {v3, v4, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectVariantShort:I

    const-string v4, "gms_min_reconnect_delay_long"

    invoke-static {v3, v4, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mMinReconnectDelayLong:I

    const-string v4, "gtalk_reconnect_variant_long"

    const/16 v5, 0x7530

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectVariantLong:I

    const-string v4, "gtalk_short_network_downtime"

    const v5, 0x2932e0

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/google/android/gms/gcm/ReconnectManager;->shortNetworkDowntime:I

    const-string v4, "gtalk_reconnect_backoff_multiplier"

    invoke-static {v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-wide/high16 v0, 0x4000000000000000L

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :cond_0
    :goto_0
    iput-wide v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectBackoffRateMultiplier:D

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/google/android/gms/gcm/ReconnectManager;->resetReconnectionTimer(Z)V

    return-void

    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public networkStateChanged(Landroid/net/NetworkInfo;JJ)V
    .locals 22
    .param p1    # Landroid/net/NetworkInfo;
    .param p2    # J
    .param p4    # J

    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v5

    const/4 v6, -0x1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/gms/gcm/ReconnectManager;->mLastNetworkBroadcastTs:J

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mNotifyNetworkState:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mNotifyNetworkType:I

    if-eqz v5, :cond_3

    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gms/gcm/ReconnectManager;->mLastNetworkAvailable:Z

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    :goto_0
    const/16 v18, 0x5

    move/from16 v0, v18

    if-ne v15, v0, :cond_6

    sget-object v18, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, v18

    if-ne v14, v0, :cond_5

    if-eqz v6, :cond_0

    const/16 v18, 0x6

    move/from16 v0, v18

    if-ne v6, v0, :cond_1

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/gcm/ReconnectManager;->ensureRouteOverMobileHipriNetworkInterface()Z

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gms/gcm/ReconnectManager;->mInMobileHipriorityMode:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mInMobileHipriorityMode:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    const-string v18, "networkStateChanged for MOBILE_HIPRI: set MOBILE_HIPRI=true"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mLastNetworkAvailable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_13

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mInMobileHipriorityMode:Z

    move/from16 v18, v0

    if-eqz v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/buzz/mobile/GcmClient;->isConnected()Z

    move-result v18

    if-eqz v18, :cond_8

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "networkStateChanged: active_net_type="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", current_net_type="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkType:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", in MOBILE_HIPRI, ignore"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gms/gcm/ReconnectManager;->mLastNetworkAvailable:Z

    goto/16 :goto_0

    :cond_4
    const-string v18, "networkStateChanged for MOBILE_HIPRI: MOBILE_HIPRI=false, ensureRouteOverMobileHipriNetworkInterface() failed"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    sget-object v18, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, v18

    if-ne v14, v0, :cond_1

    const-string v18, "networkStateChanged: MOBILE_HIPRI disconnected"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gms/gcm/ReconnectManager;->mInMobileHipriorityMode:Z

    goto :goto_1

    :cond_6
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v15, v0, :cond_1

    sget-object v18, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, v18

    if-ne v14, v0, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/gms/gcm/ReconnectManager;->mWifiDisconnectedTimeStamp:J

    goto/16 :goto_1

    :cond_7
    sget-object v18, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, v18

    if-ne v14, v0, :cond_1

    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/gms/gcm/ReconnectManager;->mWifiDisconnectedTimeStamp:J

    goto/16 :goto_1

    :cond_8
    const-string v18, "networkStateChanged: reset MOBILE_HIPRI to false"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/gms/gcm/ReconnectManager;->mInMobileHipriorityMode:Z

    :cond_9
    const-string v18, "GCM/Reconnect"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_a

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "networkStateChanged (has active network): active_network_type="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", curr_network_type="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkType:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", curr_network_state="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/buzz/mobile/GcmClient;->isActive()Z

    move-result v18

    if-nez v18, :cond_c

    const/4 v12, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    move-object/from16 v18, v0

    if-eqz v18, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/buzz/mobile/GcmClient;->isConnected()Z

    move-result v11

    :goto_4
    const/16 v16, 0x0

    if-nez v12, :cond_b

    if-eqz v11, :cond_e

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkState:Landroid/net/NetworkInfo$State;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    if-ne v7, v0, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkType:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ne v6, v0, :cond_e

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "### networkStateChanged: active and curr network type/state are the same("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "), ignore"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    if-eqz v11, :cond_2

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->reportInetCondition(Z)V

    goto/16 :goto_2

    :cond_c
    const/4 v12, 0x0

    goto :goto_3

    :cond_d
    const/4 v11, 0x0

    goto :goto_4

    :cond_e
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v18

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getType()I

    move-result v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/gcm/ReconnectManager;->setInternalNetworkState(Landroid/net/NetworkInfo$State;I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/gms/gcm/ReconnectManager;->isWanMobileNetwork(I)Z

    move-result v18

    if-eqz v18, :cond_f

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mWifiDisconnectedTimeStamp:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-wide/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/gcm/ReconnectManager;->checkThrottleReconnect(JJ)Z

    move-result v16

    :cond_f
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mNetworkSuspended:Z

    if-nez v11, :cond_10

    if-eqz v16, :cond_10

    if-eqz v13, :cond_11

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Lcom/google/buzz/mobile/GcmClient;->disconnect(ILjava/lang/String;)V

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->retryConnection(Z)V

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->resetReconnectionTimer(Z)V

    :goto_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/gcm/ReconnectManager;->clearNetworkOutageTimestamp()V

    goto/16 :goto_2

    :cond_11
    const/16 v17, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/gms/gcm/ReconnectManager;->isWanMobileNetwork(I)Z

    move-result v18

    if-eqz v18, :cond_12

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mLastMobileNetworkOutageTs:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-eqz v18, :cond_12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mLastMobileNetworkOutageTs:J

    move-wide/from16 v20, v0

    sub-long v9, v18, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->shortNetworkDowntime:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    cmp-long v18, v9, v18

    if-lez v18, :cond_12

    const/16 v17, 0x1

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "networkStateChanged: mLastMobileNetworkOutageTs="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mLastMobileNetworkOutageTs:J

    move-wide/from16 v19, v0

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", diff="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", use long delay"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_12
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->resetReconnectionTimer(Z)V

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->retryConnection(Z)V

    goto :goto_5

    :cond_13
    const-string v18, "GCM/Reconnect"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_14

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "networkStateChanged (no active network): , notify_network_type="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", notify_network_state="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_14
    sget-object v18, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    const/16 v19, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/gcm/ReconnectManager;->setInternalNetworkState(Landroid/net/NetworkInfo$State;I)V

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mLastMobileNetworkOutageTs:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-nez v18, :cond_15

    sget-object v18, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    move-object/from16 v0, v18

    if-ne v14, v0, :cond_17

    const-wide/16 v18, 0x0

    cmp-long v18, p2, v18

    if-lez v18, :cond_17

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    sub-long v18, v18, p2

    const-wide/16 v20, 0x4e20

    cmp-long v18, v18, v20

    if-gez v18, :cond_17

    const/4 v8, 0x1

    :goto_6
    if-nez v8, :cond_15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/gms/gcm/ReconnectManager;->mLastMobileNetworkOutageTs:J

    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/buzz/mobile/GcmClient;->isConnected()Z

    move-result v18

    if-eqz v18, :cond_2

    const-string v18, "GCM/Reconnect"

    const/16 v19, 0x3

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_16

    const-string v18, "========== network down, force close conn ========="

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Lcom/google/buzz/mobile/GcmClient;->disconnect(ILjava/lang/String;)V

    goto/16 :goto_2

    :cond_17
    const/4 v8, 0x0

    goto :goto_6
.end method

.method onDisconnected(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->releaseWakelock()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->reportInetCondition(Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->retryConnection(Z)V

    return-void
.end method

.method onLoginResponse(Lcom/google/buzz/mobile/proto/GCM$LoginResponse;)V
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$LoginResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->cancelReconnectAlarm()V

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->releaseWakelock()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->reportInetCondition(Z)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GCM"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive intent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_0
    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->handleDeviceStorageLow()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->handleDeviceStorageOk()V

    goto :goto_0

    :cond_3
    const-string v1, "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->handleBackgroundDataSettingChange()V

    goto :goto_0

    :cond_4
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0, p2}, Lcom/google/android/gms/gcm/ReconnectManager;->handleConnectivityChanged(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-direct {p0, p2}, Lcom/google/android/gms/gcm/ReconnectManager;->handleAirplaneModeChanged(Landroid/content/Intent;)V

    goto :goto_0

    :cond_6
    const-string v1, "com.google.android.intent.action.GTALK_RECONNECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v1}, Lcom/google/buzz/mobile/GcmClient;->isActive()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->cancelReconnectAlarm()V

    goto :goto_0

    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectAlarmSet:Z

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/gcm/ReconnectManager;->retryConnection(Z)V

    goto :goto_0
.end method

.method releaseWakelock()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->getWakeLock()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method

.method reportInetCondition(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->getNetworkType()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->reportInetCondition(ZI)V

    return-void
.end method

.method public resetReconnectionTimer(Z)V
    .locals 5
    .param p1    # Z

    iget-wide v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectDelay:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectDelay:J

    iget-wide v2, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mInitialReconnectDelay:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mMinReconnectDelayLong:I

    iget-object v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mRandomGenerator:Ljava/util/Random;

    iget v2, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectVariantLong:I

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mInitialReconnectDelay:J

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "resetReconnectionTimer "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mInitialReconnectDelay:J

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mInitialReconnectDelay:J

    iput-wide v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectDelay:J

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mMinReconnectDelayShort:I

    iget-object v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mRandomGenerator:Ljava/util/Random;

    iget v2, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectVariantShort:I

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mInitialReconnectDelay:J

    goto :goto_1
.end method

.method public retryConnection(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/GcmClient;->getPort()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/buzz/mobile/GcmClient;->disconnect(ILjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/GcmClient;->isActive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/GcmClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_2

    const-string v0, "ReconnectManager: connect()"

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/Alarm;->getWakeLock()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v1}, Lcom/google/buzz/mobile/GcmClient;->getTimeout()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    invoke-virtual {v0}, Lcom/google/buzz/mobile/GcmClient;->connect()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->setReconnectAlarm()V

    goto :goto_0
.end method

.method public setClient(Lcom/google/buzz/mobile/GcmClient;)V
    .locals 0
    .param p1    # Lcom/google/buzz/mobile/GcmClient;

    iput-object p1, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mEndpoint:Lcom/google/buzz/mobile/GcmClient;

    return-void
.end method

.method public setReconnectAlarm()V
    .locals 14

    monitor-enter p0

    :try_start_0
    iget-boolean v9, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectAlarmSet:Z

    if-eqz v9, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->getReconnectTime()J

    move-result-wide v1

    cmp-long v9, v1, v5

    if-gez v9, :cond_0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "alarm failed to fire: alarmTime="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", now="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_0
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectAlarmSet:Z

    invoke-direct {p0}, Lcom/google/android/gms/gcm/ReconnectManager;->shouldResetReconnectTimer()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/google/android/gms/gcm/ReconnectManager;->resetReconnectionTimer(Z)V

    :cond_1
    iget-object v9, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mAlarm:Lcom/google/android/gms/gcm/Alarm;

    iget-wide v10, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectDelay:J

    invoke-virtual {v9, v10, v11}, Lcom/google/android/gms/gcm/Alarm;->start(J)V

    iget-wide v9, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectDelay:J

    long-to-double v9, v9

    iget-wide v11, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectBackoffRateMultiplier:D

    mul-double/2addr v9, v11

    double-to-long v3, v9

    iget-wide v7, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectDelay:J

    iget-wide v9, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mMaxReconnectDelay:J

    cmp-long v9, v3, v9

    if-gez v9, :cond_3

    :goto_0
    iput-wide v3, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectDelay:J

    const-string v9, "GCM/Reconnect"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setReconAlarm: set delay to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-wide/16 v10, 0x3e8

    div-long v10, v7, v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "s"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " retry in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mReconnectDelay:J

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "s"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-eqz v0, :cond_4

    const-string v9, " no backoff "

    :goto_1
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/google/android/gms/gcm/ReconnectManager;->log(Ljava/lang/String;)V

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    iget-wide v3, p0, Lcom/google/android/gms/gcm/ReconnectManager;->mMaxReconnectDelay:J

    goto :goto_0

    :cond_4
    const-string v9, " backoff"

    goto :goto_1

    :catchall_0
    move-exception v9

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9
.end method
