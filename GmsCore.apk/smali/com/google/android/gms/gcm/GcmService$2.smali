.class Lcom/google/android/gms/gcm/GcmService$2;
.super Ljava/lang/Object;
.source "GcmService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/gcm/GcmService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/gcm/GcmService;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$startId:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/gcm/GcmService;Landroid/content/Intent;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/gcm/GcmService$2;->this$0:Lcom/google/android/gms/gcm/GcmService;

    iput-object p2, p0, Lcom/google/android/gms/gcm/GcmService$2;->val$intent:Landroid/content/Intent;

    iput p3, p0, Lcom/google/android/gms/gcm/GcmService$2;->val$startId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const-string v0, "com.google.android.c2dm.intent.REGISTER"

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService$2;->val$intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService$2;->this$0:Lcom/google/android/gms/gcm/GcmService;

    iget-object v0, v0, Lcom/google/android/gms/gcm/GcmService;->mRegistrar:Lcom/google/android/gms/gcm/PushMessagingRegistrar;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService$2;->val$intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/PushMessagingRegistrar;->register(Landroid/content/Intent;)Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService$2;->this$0:Lcom/google/android/gms/gcm/GcmService;

    iget v1, p0, Lcom/google/android/gms/gcm/GcmService$2;->val$startId:I

    # invokes: Lcom/google/android/gms/gcm/GcmService;->stopServiceIfConnectionDisabled(I)V
    invoke-static {v0, v1}, Lcom/google/android/gms/gcm/GcmService;->access$000(Lcom/google/android/gms/gcm/GcmService;I)V

    return-void

    :cond_1
    const-string v0, "com.google.android.c2dm.intent.UNREGISTER"

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService$2;->val$intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService$2;->this$0:Lcom/google/android/gms/gcm/GcmService;

    iget-object v0, v0, Lcom/google/android/gms/gcm/GcmService;->mRegistrar:Lcom/google/android/gms/gcm/PushMessagingRegistrar;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService$2;->val$intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/PushMessagingRegistrar;->unregister(Landroid/content/Intent;)V

    goto :goto_0
.end method
