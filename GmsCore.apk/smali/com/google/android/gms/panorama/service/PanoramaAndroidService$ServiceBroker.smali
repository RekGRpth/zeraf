.class final Lcom/google/android/gms/panorama/service/PanoramaAndroidService$ServiceBroker;
.super Lcom/google/android/gms/common/internal/AbstractServiceBroker;
.source "PanoramaAndroidService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/panorama/service/PanoramaAndroidService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ServiceBroker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/panorama/service/PanoramaAndroidService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/panorama/service/PanoramaAndroidService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$ServiceBroker;->this$0:Lcom/google/android/gms/panorama/service/PanoramaAndroidService;

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/AbstractServiceBroker;-><init>()V

    return-void
.end method


# virtual methods
.method public getPanoramaService(Lcom/google/android/gms/common/internal/IGmsCallbacks;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/internal/IGmsCallbacks;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/Bundle;

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$PanoramaService;

    iget-object v3, p0, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$ServiceBroker;->this$0:Lcom/google/android/gms/panorama/service/PanoramaAndroidService;

    invoke-direct {v2, v3}, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$PanoramaService;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$PanoramaService;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {p1, v1, v2, v3}, Lcom/google/android/gms/common/internal/IGmsCallbacks;->onPostInitComplete(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/panorama/service/PanoramaAndroidService;->TAG:Ljava/lang/String;

    const-string v2, "client died while brokering service"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
