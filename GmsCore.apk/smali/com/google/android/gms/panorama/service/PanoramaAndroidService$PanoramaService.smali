.class Lcom/google/android/gms/panorama/service/PanoramaAndroidService$PanoramaService;
.super Lcom/google/android/gms/panorama/internal/IPanoramaService$Stub;
.source "PanoramaAndroidService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/panorama/service/PanoramaAndroidService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PanoramaService"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/gms/panorama/internal/IPanoramaService$Stub;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$PanoramaService;->mContext:Landroid/content/Context;

    return-void
.end method

.method private static getPanoramaType(Lcom/google/android/gms/panorama/util/PanoMetadata;)I
    .locals 2
    .param p0    # Lcom/google/android/gms/panorama/util/PanoMetadata;

    iget v0, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoWidth:I

    iget v1, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaWidth:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->fullPanoHeight:I

    iget v1, p0, Lcom/google/android/gms/panorama/util/PanoMetadata;->croppedAreaHeight:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public loadPanoramaInfo(Lcom/google/android/gms/panorama/internal/IPanoramaCallbacks;Landroid/net/Uri;Landroid/os/Bundle;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/panorama/ContentResolverInputStreamFactory;

    iget-object v1, p0, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$PanoramaService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/google/android/gms/panorama/ContentResolverInputStreamFactory;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/google/android/gms/panorama/util/PanoMetadata;->parse(Lcom/google/android/gms/panorama/util/InputStreamFactory;)Lcom/google/android/gms/panorama/util/PanoMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/android/gms/panorama/util/PanoMetadata;->synthetic:Z

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {p1, v4, v5, v4, v5}, Lcom/google/android/gms/panorama/internal/IPanoramaCallbacks;->onPanoramaInfoLoaded(ILandroid/os/Bundle;ILandroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$PanoramaService;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/panorama/PanoramaViewActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz p3, :cond_2

    invoke-virtual {v1, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_2
    invoke-virtual {v1, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    if-eqz p4, :cond_3

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_3
    invoke-static {v0}, Lcom/google/android/gms/panorama/service/PanoramaAndroidService$PanoramaService;->getPanoramaType(Lcom/google/android/gms/panorama/util/PanoMetadata;)I

    move-result v0

    invoke-interface {p1, v4, v5, v0, v1}, Lcom/google/android/gms/panorama/internal/IPanoramaCallbacks;->onPanoramaInfoLoaded(ILandroid/os/Bundle;ILandroid/content/Intent;)V

    goto :goto_0
.end method
