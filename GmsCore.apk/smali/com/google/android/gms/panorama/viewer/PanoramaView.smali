.class public Lcom/google/android/gms/panorama/viewer/PanoramaView;
.super Landroid/opengl/GLSurfaceView;
.source "PanoramaView.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDownPitchYawPointer:Lcom/google/android/gms/panorama/math/PitchYaw;

.field private mDownPos:Landroid/graphics/PointF;

.field private mIgnoreNextActionUpForThrowing:Z

.field private mLastUpPitchYawPointer:Lcom/google/android/gms/panorama/math/PitchYaw;

.field private mLastZoom:Z

.field private mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

.field private mThrowController:Lcom/google/android/gms/panorama/viewer/ThrowController;

.field private mThrowPos:Landroid/graphics/PointF;

.field private mTimeTouchDown:J

.field private mTouchReleaseCallback:Lcom/google/android/gms/panorama/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mZoomCurrentDistance:F

.field private mZoomStartingDistance:F

.field private mZooming:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/panorama/viewer/PanoramaView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/gms/panorama/viewer/ThrowController;

    invoke-direct {v1}, Lcom/google/android/gms/panorama/viewer/ThrowController;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mThrowController:Lcom/google/android/gms/panorama/viewer/ThrowController;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mIgnoreNextActionUpForThrowing:Z

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mThrowPos:Landroid/graphics/PointF;

    :try_start_0
    new-instance v1, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;-><init>(Lcom/google/android/gms/panorama/viewer/PanoramaView;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    new-instance v2, Lcom/google/android/gms/panorama/viewer/PanoramaView$1;

    invoke-direct {v2, p0}, Lcom/google/android/gms/panorama/viewer/PanoramaView$1;-><init>(Lcom/google/android/gms/panorama/viewer/PanoramaView;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->setOnInitializedCallback(Lcom/google/android/gms/panorama/util/Callback;)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->setEGLContextClientVersion(I)V

    iget-object v1, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->setRenderMode(I)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v1, Lcom/google/android/gms/panorama/viewer/PanoramaView;->TAG:Ljava/lang/String;

    const-string v2, "Error creating Panorama view renderer."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/gms/panorama/viewer/PanoramaView;)Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;
    .locals 1
    .param p0    # Lcom/google/android/gms/panorama/viewer/PanoramaView;

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    return-object v0
.end method

.method private static getDistance(Landroid/view/MotionEvent;Landroid/graphics/PointF;)D
    .locals 6
    .param p0    # Landroid/view/MotionEvent;
    .param p1    # Landroid/graphics/PointF;

    iget v2, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float v0, v2, v3

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float v1, v2, v3

    float-to-double v2, v0

    float-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    return-wide v2
.end method

.method private getPinchDistance(Landroid/view/MotionEvent;)F
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v4, :cond_0

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float v0, v2, v3

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float v1, v2, v3

    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private stopThrowInProgress()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mThrowController:Lcom/google/android/gms/panorama/viewer/ThrowController;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/viewer/ThrowController;->stopThrow()V

    return-void
.end method


# virtual methods
.method public onDrawFrame()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mThrowController:Lcom/google/android/gms/panorama/viewer/ThrowController;

    iget-object v4, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mThrowPos:Landroid/graphics/PointF;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/panorama/viewer/ThrowController;->getThrowDelta(Landroid/graphics/PointF;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    iget-object v4, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mThrowPos:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mThrowPos:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->screenToPitchYaw(FF)Lcom/google/android/gms/panorama/math/PitchYaw;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mLastUpPitchYawPointer:Lcom/google/android/gms/panorama/math/PitchYaw;

    iget v3, v3, Lcom/google/android/gms/panorama/math/PitchYaw;->pitch:F

    iget v4, v2, Lcom/google/android/gms/panorama/math/PitchYaw;->pitch:F

    sub-float v0, v3, v4

    iget-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mLastUpPitchYawPointer:Lcom/google/android/gms/panorama/math/PitchYaw;

    iget v3, v3, Lcom/google/android/gms/panorama/math/PitchYaw;->yaw:F

    iget v4, v2, Lcom/google/android/gms/panorama/math/PitchYaw;->yaw:F

    sub-float v1, v3, v4

    iget-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    iget-object v4, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v4}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->getPitchDegrees()F

    move-result v4

    sub-float/2addr v4, v0

    invoke-virtual {v3, v4}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->setPitchAngleDegrees(F)V

    iget-object v3, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    iget-object v4, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v4}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->getYawDegrees()F

    move-result v4

    sub-float/2addr v4, v1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->setYawAngleDegrees(F)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    and-int/lit16 v8, v8, 0xff

    packed-switch v8, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v8, 0x1

    return v8

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->stopThrowInProgress()V

    new-instance v8, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    invoke-direct {v8, v9, v10}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mDownPos:Landroid/graphics/PointF;

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->screenToPitchYaw(FF)Lcom/google/android/gms/panorama/math/PitchYaw;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mDownPitchYawPointer:Lcom/google/android/gms/panorama/math/PitchYaw;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mTimeTouchDown:J

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mThrowController:Lcom/google/android/gms/panorama/viewer/ThrowController;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v11

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/google/android/gms/panorama/viewer/ThrowController;->onPointerDown(FFJ)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->getPinchDistance(Landroid/view/MotionEvent;)F

    move-result v8

    iput v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mZoomStartingDistance:F

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mZooming:Z

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mThrowController:Lcom/google/android/gms/panorama/viewer/ThrowController;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v11

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/google/android/gms/panorama/viewer/ThrowController;->onPointerMove(FFJ)V

    :cond_1
    iget-boolean v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mLastZoom:Z

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->screenToPitchYaw(FF)Lcom/google/android/gms/panorama/math/PitchYaw;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mDownPitchYawPointer:Lcom/google/android/gms/panorama/math/PitchYaw;

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mLastZoom:Z

    :cond_2
    iget-boolean v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mZooming:Z

    if-eqz v8, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    const/4 v9, 0x1

    if-le v8, v9, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->getPinchDistance(Landroid/view/MotionEvent;)F

    move-result v8

    iput v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mZoomCurrentDistance:F

    iget v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mZoomCurrentDistance:F

    iget v9, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mZoomStartingDistance:F

    div-float v7, v8, v9

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v8, v7}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->pinchZoom(F)V

    goto/16 :goto_0

    :cond_3
    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->screenToPitchYaw(FF)Lcom/google/android/gms/panorama/math/PitchYaw;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mDownPitchYawPointer:Lcom/google/android/gms/panorama/math/PitchYaw;

    iget v8, v8, Lcom/google/android/gms/panorama/math/PitchYaw;->pitch:F

    iget v9, v6, Lcom/google/android/gms/panorama/math/PitchYaw;->pitch:F

    sub-float v0, v8, v9

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mDownPitchYawPointer:Lcom/google/android/gms/panorama/math/PitchYaw;

    iget v8, v8, Lcom/google/android/gms/panorama/math/PitchYaw;->yaw:F

    iget v9, v6, Lcom/google/android/gms/panorama/math/PitchYaw;->yaw:F

    sub-float v1, v8, v9

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mDownPos:Landroid/graphics/PointF;

    invoke-static {p1, v8}, Lcom/google/android/gms/panorama/viewer/PanoramaView;->getDistance(Landroid/view/MotionEvent;Landroid/graphics/PointF;)D

    move-result-wide v8

    const-wide/high16 v10, 0x4010000000000000L

    cmpl-double v8, v8, v10

    if-ltz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    iget-object v9, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v9}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->getPitchDegrees()F

    move-result v9

    sub-float/2addr v9, v0

    invoke-virtual {v8, v9}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->setPitchAngleDegrees(F)V

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    iget-object v9, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v9}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->getYawDegrees()F

    move-result v9

    sub-float/2addr v9, v1

    invoke-virtual {v8, v9}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->setYawAngleDegrees(F)V

    goto/16 :goto_0

    :pswitch_4
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mZooming:Z

    iget v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mZoomCurrentDistance:F

    iget v9, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mZoomStartingDistance:F

    div-float v7, v8, v9

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v8, v7}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->endPinchZoom(F)V

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mLastZoom:Z

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mIgnoreNextActionUpForThrowing:Z

    goto/16 :goto_0

    :pswitch_5
    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mDownPos:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    sub-float v4, v8, v9

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mDownPos:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    sub-float v5, v8, v9

    float-to-double v8, v4

    float-to-double v10, v5

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mTimeTouchDown:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x190

    cmp-long v8, v8, v10

    if-gez v8, :cond_4

    const-wide/high16 v8, 0x4024000000000000L

    cmpg-double v8, v2, v8

    if-gez v8, :cond_4

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v8}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->toggleAutoSpin()V

    :cond_4
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mLastZoom:Z

    iget-boolean v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mIgnoreNextActionUpForThrowing:Z

    if-nez v8, :cond_5

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->screenToPitchYaw(FF)Lcom/google/android/gms/panorama/math/PitchYaw;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mLastUpPitchYawPointer:Lcom/google/android/gms/panorama/math/PitchYaw;

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mThrowController:Lcom/google/android/gms/panorama/viewer/ThrowController;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v11

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/google/android/gms/panorama/viewer/ThrowController;->onPointerUp(FFJ)V

    :goto_1
    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mTouchReleaseCallback:Lcom/google/android/gms/panorama/util/Callback;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mTouchReleaseCallback:Lcom/google/android/gms/panorama/util/Callback;

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Lcom/google/android/gms/panorama/util/Callback;->onCallback(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_5
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mIgnoreNextActionUpForThrowing:Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public setAutoRotationCallback(Lcom/google/android/gms/panorama/util/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->setAutoRotationCallback(Lcom/google/android/gms/panorama/util/Callback;)V

    return-void
.end method

.method public setPanoramaImage(Lcom/google/android/gms/panorama/viewer/PanoramaImage;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/panorama/viewer/PanoramaImage;

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->setPanoramaImage(Lcom/google/android/gms/panorama/viewer/PanoramaImage;)V

    return-void
.end method

.method public setSensorReader(Landroid/view/Display;Lcom/google/android/gms/panorama/sensor/SensorReader;)V
    .locals 1
    .param p1    # Landroid/view/Display;
    .param p2    # Lcom/google/android/gms/panorama/sensor/SensorReader;

    iget-object v0, p0, Lcom/google/android/gms/panorama/viewer/PanoramaView;->mRenderer:Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/panorama/viewer/PanoramaViewRenderer;->setSensorReader(Landroid/view/Display;Lcom/google/android/gms/panorama/sensor/SensorReader;)V

    return-void
.end method
