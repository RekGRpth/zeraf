.class public Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;
.super Lcom/google/android/gms/panorama/util/HttpRequestTask;
.source "ExpandShortenedUrlTask.java"


# static fields
.field private static final SHORTENER_API_URL:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final callback:Lcom/google/android/gms/panorama/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/panorama/util/UrlShortener;->getApiBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&shortUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;->SHORTENER_API_URL:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/panorama/util/HttpRequestTask;-><init>(Landroid/content/Context;Z)V

    iput-object p2, p0, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;->callback:Lcom/google/android/gms/panorama/util/Callback;

    return-void
.end method

.method public static expandAsync(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/panorama/util/Callback;)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/panorama/util/Callback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;->SHORTENER_API_URL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;

    invoke-direct {v2, p0, p2}, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;-><init>(Landroid/content/Context;Lcom/google/android/gms/panorama/util/Callback;)V

    const/4 v3, 0x1

    new-array v3, v3, [Lorg/apache/http/client/methods/HttpUriRequest;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public processUploadResponse(Lorg/apache/http/HttpResponse;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lorg/apache/http/HttpResponse;
    .param p2    # Ljava/lang/String;

    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;->callback:Lcom/google/android/gms/panorama/util/Callback;

    const-string v3, "longUrl"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/gms/panorama/util/Callback;->onCallback(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v2, p0, Lcom/google/android/gms/panorama/util/ExpandShortenedUrlTask;->callback:Lcom/google/android/gms/panorama/util/Callback;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/gms/panorama/util/Callback;->onCallback(Ljava/lang/Object;)V

    goto :goto_0
.end method
