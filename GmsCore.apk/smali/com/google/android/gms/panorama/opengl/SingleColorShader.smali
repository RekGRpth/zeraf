.class public Lcom/google/android/gms/panorama/opengl/SingleColorShader;
.super Lcom/google/android/gms/panorama/opengl/Shader;
.source "SingleColorShader.java"


# instance fields
.field private mColorIndex:I

.field private mFragmentShader:Ljava/lang/String;

.field private final mVertexShader:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/panorama/opengl/Shader;-><init>()V

    const-string v0, "uniform mat4 uMvpMatrix;                   \nattribute vec4 aPosition;                   \nvoid main()                                 \n{                                           \n   gl_Position = uMvpMatrix * aPosition;    \n}                                           \n"

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mVertexShader:Ljava/lang/String;

    const-string v0, "precision mediump float;                       \nuniform vec4 uDrawColor;                       \nvoid main()                                    \n{                                              \n  gl_FragColor = uDrawColor;                   \n}                                              \n"

    iput-object v0, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mFragmentShader:Ljava/lang/String;

    const-string v0, "uniform mat4 uMvpMatrix;                   \nattribute vec4 aPosition;                   \nvoid main()                                 \n{                                           \n   gl_Position = uMvpMatrix * aPosition;    \n}                                           \n"

    iget-object v1, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mFragmentShader:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->createProgram(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mProgram:I

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mProgram:I

    const-string v1, "aPosition"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->getAttribute(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mVertexIndex:I

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mProgram:I

    const-string v1, "uMvpMatrix"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->getUniform(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mTransformIndex:I

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mProgram:I

    const-string v1, "uDrawColor"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->getUniform(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mColorIndex:I

    return-void
.end method


# virtual methods
.method public setColor([F)V
    .locals 5
    .param p1    # [F

    invoke-virtual {p0}, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->bind()V

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/SingleColorShader;->mColorIndex:I

    const/4 v1, 0x0

    aget v1, p1, v1

    const/4 v2, 0x1

    aget v2, p1, v2

    const/4 v3, 0x2

    aget v3, p1, v3

    const/4 v4, 0x3

    aget v4, p1, v4

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    return-void
.end method
