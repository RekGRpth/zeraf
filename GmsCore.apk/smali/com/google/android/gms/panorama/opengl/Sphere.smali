.class public Lcom/google/android/gms/panorama/opengl/Sphere;
.super Lcom/google/android/gms/panorama/opengl/DrawableGL;
.source "Sphere.java"


# instance fields
.field private mLineDrawing:Z

.field private mLineIndices:Ljava/nio/ShortBuffer;

.field private mNumLineIndices:I

.field private mNumTriangleIndices:I


# direct methods
.method public constructor <init>(IIF)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # F

    invoke-direct {p0}, Lcom/google/android/gms/panorama/opengl/DrawableGL;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mLineDrawing:Z

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/panorama/opengl/Sphere;->generateGeometry(IIF)V

    return-void
.end method

.method private generateGeometry(IIF)V
    .locals 31
    .param p1    # I
    .param p2    # I
    .param p3    # F

    mul-int v14, p1, p2

    add-int/lit8 v28, p1, -0x1

    add-int/lit8 v29, p2, -0x1

    mul-int v28, v28, v29

    mul-int/lit8 v13, v28, 0x6

    const/16 v28, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v14, v13, v1}, Lcom/google/android/gms/panorama/opengl/Sphere;->initGeometry(IIZ)V

    mul-int/lit8 v28, v14, 0x2

    mul-int/lit8 v28, v28, 0x2

    invoke-static/range {v28 .. v28}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/panorama/opengl/Sphere;->mLineIndices:Ljava/nio/ShortBuffer;

    const v28, 0x40490fdb

    add-int/lit8 v29, p1, -0x1

    move/from16 v0, v29

    int-to-float v0, v0

    move/from16 v29, v0

    div-float v3, v28, v29

    const v28, 0x40c90fdb

    add-int/lit8 v29, p2, -0x1

    move/from16 v0, v29

    int-to-float v0, v0

    move/from16 v29, v0

    div-float v4, v28, v29

    const/16 v23, 0x0

    const/16 v20, 0x0

    const v15, 0x3fc90fdb

    const/16 v16, 0x0

    :goto_0
    move/from16 v0, v16

    move/from16 v1, p1

    if-ge v0, v1, :cond_1

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v28, v0

    mul-float v28, v28, v3

    const v29, 0x3fc90fdb

    sub-float v10, v28, v29

    const/high16 v28, 0x3f800000

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v29, v0

    add-int/lit8 v30, p1, -0x1

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    div-float v29, v29, v30

    sub-float v22, v28, v29

    const/4 v5, 0x0

    :goto_1
    move/from16 v0, p2

    if-ge v5, v0, :cond_0

    int-to-float v0, v5

    move/from16 v28, v0

    mul-float v28, v28, v4

    const v29, 0x3fc90fdb

    add-float v11, v28, v29

    invoke-static {v10}, Landroid/util/FloatMath;->sin(F)F

    move-result v19

    invoke-static {v10}, Landroid/util/FloatMath;->cos(F)F

    move-result v6

    invoke-static {v11}, Landroid/util/FloatMath;->cos(F)F

    move-result v28

    mul-float v28, v28, v6

    mul-float v25, v28, p3

    mul-float v26, v19, p3

    invoke-static {v11}, Landroid/util/FloatMath;->sin(F)F

    move-result v28

    mul-float v28, v28, v6

    mul-float v27, v28, p3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mVertices:Ljava/nio/FloatBuffer;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v23

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mVertices:Ljava/nio/FloatBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v29, v23, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v29

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mVertices:Ljava/nio/FloatBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v29, v23, 0x2

    move-object/from16 v0, v28

    move/from16 v1, v29

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    add-int/lit8 v23, v23, 0x3

    int-to-float v0, v5

    move/from16 v28, v0

    add-int/lit8 v29, p2, -0x1

    move/from16 v0, v29

    int-to-float v0, v0

    move/from16 v29, v0

    div-float v21, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mTexCoords:Ljava/nio/FloatBuffer;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mTexCoords:Ljava/nio/FloatBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v29, v20, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v29

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    add-int/lit8 v20, v20, 0x2

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    :cond_0
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    :cond_1
    move/from16 v18, p2

    const/4 v8, 0x0

    const/16 v16, 0x0

    :goto_2
    add-int/lit8 v28, p1, -0x1

    move/from16 v0, v16

    move/from16 v1, v28

    if-ge v0, v1, :cond_3

    mul-int v17, v16, v18

    add-int/lit8 v28, v16, 0x1

    mul-int v12, v28, v18

    const/4 v5, 0x0

    move v9, v8

    :goto_3
    add-int/lit8 v28, p2, -0x1

    move/from16 v0, v28

    if-ge v5, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mIndices:Ljava/nio/ShortBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v8, v9, 0x1

    move/from16 v0, v17

    int-to-short v0, v0

    move/from16 v29, v0

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v9, v1}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mIndices:Ljava/nio/ShortBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v9, v8, 0x1

    add-int/lit8 v29, v12, 0x1

    move/from16 v0, v29

    int-to-short v0, v0

    move/from16 v29, v0

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v8, v1}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mIndices:Ljava/nio/ShortBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v8, v9, 0x1

    int-to-short v0, v12

    move/from16 v29, v0

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v9, v1}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mIndices:Ljava/nio/ShortBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v9, v8, 0x1

    move/from16 v0, v17

    int-to-short v0, v0

    move/from16 v29, v0

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v8, v1}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mIndices:Ljava/nio/ShortBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v8, v9, 0x1

    add-int/lit8 v29, v17, 0x1

    move/from16 v0, v29

    int-to-short v0, v0

    move/from16 v29, v0

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v9, v1}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mIndices:Ljava/nio/ShortBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v9, v8, 0x1

    add-int/lit8 v29, v12, 0x1

    move/from16 v0, v29

    int-to-short v0, v0

    move/from16 v29, v0

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v8, v1}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    add-int/lit8 v17, v17, 0x1

    add-int/lit8 v12, v12, 0x1

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    :cond_2
    add-int/lit8 v16, v16, 0x1

    move v8, v9

    goto/16 :goto_2

    :cond_3
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mNumTriangleIndices:I

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/16 v24, 0x0

    const/16 v16, 0x0

    :goto_4
    move/from16 v0, v16

    move/from16 v1, p1

    if-ge v0, v1, :cond_5

    const/4 v5, 0x0

    move v9, v8

    :goto_5
    move/from16 v0, p2

    if-ge v5, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mLineIndices:Ljava/nio/ShortBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v8, v9, 0x1

    move/from16 v0, v24

    int-to-short v0, v0

    move/from16 v29, v0

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v9, v1}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    add-int v24, v24, v7

    add-int/lit8 v5, v5, 0x1

    move v9, v8

    goto :goto_5

    :cond_4
    sub-int v24, v24, v7

    neg-int v7, v7

    add-int v24, v24, p2

    add-int/lit8 v16, v16, 0x1

    move v8, v9

    goto :goto_4

    :cond_5
    const/4 v7, -0x1

    sub-int v24, v24, p2

    const/4 v5, 0x0

    :goto_6
    move/from16 v0, p2

    if-ge v5, v0, :cond_7

    const/16 v16, 0x0

    move v9, v8

    :goto_7
    move/from16 v0, v16

    move/from16 v1, p1

    if-ge v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/panorama/opengl/Sphere;->mLineIndices:Ljava/nio/ShortBuffer;

    move-object/from16 v28, v0

    add-int/lit8 v8, v9, 0x1

    move/from16 v0, v24

    int-to-short v0, v0

    move/from16 v29, v0

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v9, v1}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    mul-int v28, v7, p2

    add-int v24, v24, v28

    add-int/lit8 v16, v16, 0x1

    move v9, v8

    goto :goto_7

    :cond_6
    mul-int v28, v7, p2

    sub-int v24, v24, v28

    neg-int v7, v7

    add-int/lit8 v24, v24, 0x1

    add-int/lit8 v5, v5, 0x1

    move v8, v9

    goto :goto_6

    :cond_7
    add-int/lit8 v28, v8, -0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gms/panorama/opengl/Sphere;->mNumLineIndices:I

    return-void
.end method


# virtual methods
.method public drawObject([F)V
    .locals 4
    .param p1    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    const/16 v3, 0x1403

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/opengl/Shader;->bind()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mVertices:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    iget-object v1, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mVertices:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/opengl/Shader;->setVertices(Ljava/nio/FloatBuffer;)V

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mLineDrawing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mTexCoords:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    iget-object v1, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mTexCoords:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/opengl/Shader;->setTexCoords(Ljava/nio/FloatBuffer;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mTextures:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mTextures:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/panorama/opengl/GLTexture;

    iget-object v1, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/opengl/GLTexture;->bind(Lcom/google/android/gms/panorama/opengl/Shader;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mShader:Lcom/google/android/gms/panorama/opengl/Shader;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/panorama/opengl/Shader;->setTransform([F)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mIndices:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mLineIndices:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mLineDrawing:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mNumLineIndices:I

    iget-object v2, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mLineIndices:Ljava/nio/ShortBuffer;

    invoke-static {v0, v1, v3, v2}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mNumTriangleIndices:I

    iget-object v2, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mIndices:Ljava/nio/ShortBuffer;

    invoke-static {v0, v1, v3, v2}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public setLineDrawing(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gms/panorama/opengl/Sphere;->mLineDrawing:Z

    return-void
.end method
