.class public Lcom/google/android/gms/panorama/opengl/Vertex;
.super Ljava/lang/Object;
.source "Vertex.java"


# instance fields
.field public final x:F

.field public final y:F

.field public final z:F


# direct methods
.method public constructor <init>(FFF)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/panorama/opengl/Vertex;->x:F

    iput p2, p0, Lcom/google/android/gms/panorama/opengl/Vertex;->y:F

    iput p3, p0, Lcom/google/android/gms/panorama/opengl/Vertex;->z:F

    return-void
.end method


# virtual methods
.method public addToBuffer(Ljava/nio/FloatBuffer;I)V
    .locals 2
    .param p1    # Ljava/nio/FloatBuffer;
    .param p2    # I

    iget v0, p0, Lcom/google/android/gms/panorama/opengl/Vertex;->x:F

    invoke-virtual {p1, p2, v0}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    add-int/lit8 v0, p2, 0x1

    iget v1, p0, Lcom/google/android/gms/panorama/opengl/Vertex;->y:F

    invoke-virtual {p1, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    add-int/lit8 v0, p2, 0x2

    iget v1, p0, Lcom/google/android/gms/panorama/opengl/Vertex;->z:F

    invoke-virtual {p1, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    return-void
.end method
