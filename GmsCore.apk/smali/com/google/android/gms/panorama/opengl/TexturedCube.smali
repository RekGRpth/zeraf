.class public Lcom/google/android/gms/panorama/opengl/TexturedCube;
.super Lcom/google/android/gms/panorama/opengl/DrawableGL;
.source "TexturedCube.java"


# instance fields
.field private mNumIndices:I

.field private shader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

.field private texture:Lcom/google/android/gms/panorama/opengl/GLTexture;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;F)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F

    invoke-direct {p0}, Lcom/google/android/gms/panorama/opengl/DrawableGL;-><init>()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mNumIndices:I

    :try_start_0
    new-instance v1, Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    invoke-direct {v1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->shader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;
    :try_end_0
    .catch Lcom/google/android/gms/panorama/opengl/OpenGLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v1, Lcom/google/android/gms/panorama/opengl/GLTexture;

    invoke-direct {v1}, Lcom/google/android/gms/panorama/opengl/GLTexture;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->texture:Lcom/google/android/gms/panorama/opengl/GLTexture;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->generateGeometry(Landroid/graphics/Bitmap;F)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/opengl/OpenGLException;->printStackTrace()V

    goto :goto_0
.end method

.method private generateGeometry(Landroid/graphics/Bitmap;F)V
    .locals 27
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F

    const/16 v22, 0x24

    const/16 v2, 0x24

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mNumIndices:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mNumIndices:I

    const/4 v8, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1, v2, v8}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->initGeometry(IIZ)V

    const/16 v2, 0x18

    new-array v0, v2, [F

    move-object/from16 v26, v0

    fill-array-data v26, :array_0

    const/16 v2, 0x24

    new-array v0, v2, [S

    move-object/from16 v25, v0

    fill-array-data v25, :array_1

    const/16 v19, 0x0

    :goto_0
    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_0

    aget-short v24, v25, v19

    mul-int/lit8 v20, v24, 0x3

    aget v2, v26, v20

    mul-float v2, v2, p2

    add-int/lit8 v8, v20, 0x1

    aget v8, v26, v8

    mul-float v8, v8, p2

    add-int/lit8 v9, v20, 0x2

    aget v9, v26, v9

    mul-float v9, v9, p2

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2, v8, v9}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putVertex(IFFF)V

    move/from16 v0, v19

    int-to-short v2, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putIndex(IS)V

    add-int/lit8 v19, v19, 0x1

    goto :goto_0

    :cond_0
    const/high16 v10, 0x3e800000

    const v11, 0x3eaaaaab

    const/16 v21, 0x0

    move/from16 v23, v10

    move v5, v11

    add-float v7, v5, v11

    const/4 v3, 0x0

    const/16 v19, 0x0

    :goto_1
    const/4 v2, 0x4

    move/from16 v0, v19

    if-ge v0, v2, :cond_1

    move/from16 v0, v19

    int-to-float v2, v0

    mul-float v4, v2, v10

    add-float v6, v4, v10

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putFaceTexCooords(IFFFF)I

    move-result v3

    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    :cond_1
    const/high16 v2, 0x40000000

    mul-float v12, v10, v2

    const/4 v13, 0x0

    move-object/from16 v8, p0

    move v9, v3

    invoke-direct/range {v8 .. v13}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putFaceTexCooords(IFFFF)I

    move-result v3

    const/high16 v2, 0x40400000

    mul-float v15, v2, v11

    const/high16 v2, 0x40000000

    mul-float v16, v10, v2

    const/high16 v2, 0x40000000

    mul-float v17, v2, v11

    move-object/from16 v12, p0

    move v13, v3

    move v14, v10

    invoke-direct/range {v12 .. v17}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putFaceTexCooords(IFFFF)I

    move-result v3

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->texture:Lcom/google/android/gms/panorama/opengl/GLTexture;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/gms/panorama/opengl/GLTexture;->loadBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Lcom/google/android/gms/panorama/opengl/OpenGLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :catch_0
    move-exception v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/gms/panorama/opengl/OpenGLException;->printStackTrace()V

    goto :goto_2

    nop

    :array_0
    .array-data 4
        -0x40800000
        -0x40800000
        0x3f800000
        0x3f800000
        -0x40800000
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
        -0x40800000
        0x3f800000
        0x3f800000
        -0x40800000
        -0x40800000
        -0x40800000
        0x3f800000
        -0x40800000
        -0x40800000
        0x3f800000
        0x3f800000
        -0x40800000
        -0x40800000
        0x3f800000
        -0x40800000
    .end array-data

    :array_1
    .array-data 2
        0x0s
        0x7s
        0x3s
        0x0s
        0x4s
        0x7s
        0x4s
        0x6s
        0x7s
        0x4s
        0x5s
        0x6s
        0x5s
        0x2s
        0x6s
        0x5s
        0x1s
        0x2s
        0x1s
        0x3s
        0x2s
        0x1s
        0x0s
        0x3s
        0x3s
        0x6s
        0x7s
        0x3s
        0x2s
        0x6s
        0x4s
        0x1s
        0x0s
        0x4s
        0x5s
        0x1s
    .end array-data
.end method

.method private putFaceTexCooords(IFFFF)I
    .locals 0
    .param p1    # I
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F

    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putTexCoord(IFF)V

    add-int/lit8 p1, p1, 0x2

    invoke-direct {p0, p1, p4, p3}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putTexCoord(IFF)V

    add-int/lit8 p1, p1, 0x2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putTexCoord(IFF)V

    add-int/lit8 p1, p1, 0x2

    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putTexCoord(IFF)V

    add-int/lit8 p1, p1, 0x2

    invoke-direct {p0, p1, p4, p5}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putTexCoord(IFF)V

    add-int/lit8 p1, p1, 0x2

    invoke-direct {p0, p1, p4, p3}, Lcom/google/android/gms/panorama/opengl/TexturedCube;->putTexCoord(IFF)V

    add-int/lit8 p1, p1, 0x2

    return p1
.end method

.method private putTexCoord(IFF)V
    .locals 2
    .param p1    # I
    .param p2    # F
    .param p3    # F

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mTexCoords:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mTexCoords:Ljava/nio/FloatBuffer;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1, p3}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    return-void
.end method


# virtual methods
.method public drawObject([F)V
    .locals 4
    .param p1    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/panorama/opengl/OpenGLException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->shader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->bind()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->shader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mVertices:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->shader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    iget-object v1, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mVertices:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->setVertices(Ljava/nio/FloatBuffer;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mTexCoords:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->shader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    iget-object v1, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mTexCoords:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->setTexCoords(Ljava/nio/FloatBuffer;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->texture:Lcom/google/android/gms/panorama/opengl/GLTexture;

    iget-object v1, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->shader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/panorama/opengl/GLTexture;->bind(Lcom/google/android/gms/panorama/opengl/Shader;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->shader:Lcom/google/android/gms/panorama/shaders/TransparencyShader;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/panorama/shaders/TransparencyShader;->setTransform([F)V

    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mNumIndices:I

    const/16 v2, 0x1403

    iget-object v3, p0, Lcom/google/android/gms/panorama/opengl/TexturedCube;->mIndices:Ljava/nio/ShortBuffer;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    return-void
.end method
