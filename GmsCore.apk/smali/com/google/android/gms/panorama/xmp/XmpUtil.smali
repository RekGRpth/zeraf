.class public Lcom/google/android/gms/panorama/xmp/XmpUtil;
.super Ljava/lang/Object;
.source "XmpUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/panorama/xmp/XmpUtil$1;,
        Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;
    }
.end annotation


# direct methods
.method static constructor <clinit>()V
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/adobe/xmp/XMPMetaFactory;->getSchemaRegistry()Lcom/adobe/xmp/XMPSchemaRegistry;

    move-result-object v1

    const-string v2, "http://ns.google.com/photos/1.0/panorama/"

    const-string v3, "GPano"

    invoke-interface {v1, v2, v3}, Lcom/adobe/xmp/XMPSchemaRegistry;->registerNamespace(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/adobe/xmp/XMPException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/adobe/xmp/XMPException;->printStackTrace()V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static extractXMPMeta(Ljava/io/InputStream;)Lcom/adobe/xmp/XMPMeta;
    .locals 11
    .param p0    # Ljava/io/InputStream;

    const/4 v4, 0x0

    const/4 v7, 0x1

    invoke-static {p0, v7}, Lcom/google/android/gms/panorama/xmp/XmpUtil;->parse(Ljava/io/InputStream;Z)Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;

    iget-object v7, v5, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->data:[B

    invoke-static {v7}, Lcom/google/android/gms/panorama/xmp/XmpUtil;->hasXMPHeader([B)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, v5, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->data:[B

    invoke-static {v7}, Lcom/google/android/gms/panorama/xmp/XmpUtil;->getXMPContentEnd([B)I

    move-result v2

    add-int/lit8 v7, v2, -0x1d

    new-array v0, v7, [B

    iget-object v7, v5, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->data:[B

    const/16 v8, 0x1d

    const/4 v9, 0x0

    array-length v10, v0

    invoke-static {v7, v8, v0, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :try_start_0
    invoke-static {v0}, Lcom/adobe/xmp/XMPMetaFactory;->parseFromBuffer([B)Lcom/adobe/xmp/XMPMeta;
    :try_end_0
    .catch Lcom/adobe/xmp/XMPException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v7, "XmpUtil"

    const-string v8, "XMP parse error."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getXMPContentEnd([B)I
    .locals 3
    .param p0    # [B

    array-length v1, p0

    add-int/lit8 v0, v1, -0x1

    :goto_0
    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    aget-byte v1, p0, v0

    const/16 v2, 0x3e

    if-ne v1, v2, :cond_0

    add-int/lit8 v1, v0, -0x1

    aget-byte v1, p0, v1

    const/16 v2, 0x3f

    if-eq v1, v2, :cond_0

    add-int/lit8 v1, v0, 0x1

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    array-length v1, p0

    goto :goto_1
.end method

.method private static hasXMPHeader([B)Z
    .locals 6
    .param p0    # [B

    const/16 v4, 0x1d

    const/4 v2, 0x0

    array-length v3, p0

    if-ge v3, v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/16 v3, 0x1d

    :try_start_0
    new-array v1, v3, [B

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x1d

    invoke-static {p0, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v3, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v3, v1, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const-string v4, "http://ns.adobe.com/xap/1.0/\u0000"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static parse(Ljava/io/InputStream;Z)Ljava/util/List;
    .locals 14
    .param p0    # Ljava/io/InputStream;
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;",
            ">;"
        }
    .end annotation

    const/16 v13, 0xff

    const/4 v12, -0x1

    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v9

    if-ne v9, v13, :cond_0

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    const/16 v10, 0xd8

    if-eq v9, v10, :cond_3

    :cond_0
    if-eqz p0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_1
    :goto_0
    move-object v7, v8

    :cond_2
    :goto_1
    return-object v7

    :cond_3
    :try_start_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eq v0, v12, :cond_11

    if-eq v0, v13, :cond_5

    if-eqz p0, :cond_4

    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_4
    :goto_3
    move-object v7, v8

    goto :goto_1

    :cond_5
    :try_start_4
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v0

    if-eq v0, v13, :cond_5

    if-ne v0, v12, :cond_7

    if-eqz p0, :cond_6

    :try_start_5
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    :cond_6
    :goto_4
    move-object v7, v8

    goto :goto_1

    :cond_7
    move v5, v0

    const/16 v9, 0xda

    if-ne v5, v9, :cond_9

    if-nez p1, :cond_8

    :try_start_6
    new-instance v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;

    const/4 v9, 0x0

    invoke-direct {v6, v9}, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;-><init>(Lcom/google/android/gms/panorama/xmp/XmpUtil$1;)V

    iput v5, v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->marker:I

    const/4 v9, -0x1

    iput v9, v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->length:I

    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v9

    new-array v9, v9, [B

    iput-object v9, v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->data:[B

    iget-object v9, v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->data:[B

    const/4 v10, 0x0

    iget-object v11, v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->data:[B

    array-length v11, v11

    invoke-virtual {p0, v9, v10, v11}, Ljava/io/InputStream;->read([BII)I

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_8
    if-eqz p0, :cond_2

    :try_start_7
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_1

    :catch_0
    move-exception v8

    goto :goto_1

    :cond_9
    :try_start_8
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v4

    if-eq v3, v12, :cond_a

    if-ne v4, v12, :cond_c

    :cond_a
    if-eqz p0, :cond_b

    :try_start_9
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    :cond_b
    :goto_5
    move-object v7, v8

    goto :goto_1

    :cond_c
    shl-int/lit8 v9, v3, 0x8

    or-int v2, v9, v4

    if-eqz p1, :cond_d

    const/16 v9, 0xe1

    if-ne v0, v9, :cond_f

    :cond_d
    :try_start_a
    new-instance v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;

    const/4 v9, 0x0

    invoke-direct {v6, v9}, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;-><init>(Lcom/google/android/gms/panorama/xmp/XmpUtil$1;)V

    iput v5, v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->marker:I

    iput v2, v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->length:I

    add-int/lit8 v9, v2, -0x2

    new-array v9, v9, [B

    iput-object v9, v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->data:[B

    iget-object v9, v6, Lcom/google/android/gms/panorama/xmp/XmpUtil$Section;->data:[B

    const/4 v10, 0x0

    add-int/lit8 v11, v2, -0x2

    invoke-virtual {p0, v9, v10, v11}, Ljava/io/InputStream;->read([BII)I

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_2

    :catch_1
    move-exception v1

    :try_start_b
    const-string v9, "XmpUtil"

    const-string v10, "Could not parse file."

    invoke-static {v9, v10, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-eqz p0, :cond_e

    :try_start_c
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    :cond_e
    :goto_6
    move-object v7, v8

    goto/16 :goto_1

    :cond_f
    add-int/lit8 v9, v2, -0x2

    int-to-long v9, v9

    :try_start_d
    invoke-virtual {p0, v9, v10}, Ljava/io/InputStream;->skip(J)J
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_2

    :catchall_0
    move-exception v8

    if-eqz p0, :cond_10

    :try_start_e
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8

    :cond_10
    :goto_7
    throw v8

    :cond_11
    if-eqz p0, :cond_2

    :try_start_f
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v8

    goto/16 :goto_1

    :catch_3
    move-exception v9

    goto/16 :goto_0

    :catch_4
    move-exception v9

    goto/16 :goto_3

    :catch_5
    move-exception v9

    goto/16 :goto_4

    :catch_6
    move-exception v9

    goto :goto_5

    :catch_7
    move-exception v9

    goto :goto_6

    :catch_8
    move-exception v9

    goto :goto_7
.end method
