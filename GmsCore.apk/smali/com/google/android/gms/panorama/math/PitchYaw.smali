.class public Lcom/google/android/gms/panorama/math/PitchYaw;
.super Ljava/lang/Object;
.source "PitchYaw.java"


# instance fields
.field public final pitch:F

.field public final yaw:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0
    .param p1    # F
    .param p2    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/panorama/math/PitchYaw;->pitch:F

    iput p2, p0, Lcom/google/android/gms/panorama/math/PitchYaw;->yaw:F

    return-void
.end method
