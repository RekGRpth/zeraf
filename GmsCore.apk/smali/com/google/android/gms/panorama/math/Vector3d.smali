.class public Lcom/google/android/gms/panorama/math/Vector3d;
.super Ljava/lang/Object;
.source "Vector3d.java"


# instance fields
.field public x:D

.field public y:D

.field public z:D


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cross(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)V
    .locals 11
    .param p0    # Lcom/google/android/gms/panorama/math/Vector3d;
    .param p1    # Lcom/google/android/gms/panorama/math/Vector3d;
    .param p2    # Lcom/google/android/gms/panorama/math/Vector3d;

    iget-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iget-wide v2, p1, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    iget-wide v4, p1, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double/2addr v2, v4

    sub-double v1, v0, v2

    iget-wide v3, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    iget-wide v5, p1, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    mul-double/2addr v3, v5

    iget-wide v5, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    iget-wide v7, p1, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double/2addr v5, v7

    sub-double/2addr v3, v5

    iget-wide v5, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    iget-wide v7, p1, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double/2addr v5, v7

    iget-wide v7, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iget-wide v9, p1, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    mul-double/2addr v7, v9

    sub-double/2addr v5, v7

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/panorama/math/Vector3d;->set(DDD)V

    return-void
.end method

.method public static dot(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)D
    .locals 6
    .param p0    # Lcom/google/android/gms/panorama/math/Vector3d;
    .param p1    # Lcom/google/android/gms/panorama/math/Vector3d;

    iget-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    iget-wide v2, p1, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iget-wide v4, p1, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    iget-wide v4, p1, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public static sub(Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;Lcom/google/android/gms/panorama/math/Vector3d;)V
    .locals 9
    .param p0    # Lcom/google/android/gms/panorama/math/Vector3d;
    .param p1    # Lcom/google/android/gms/panorama/math/Vector3d;
    .param p2    # Lcom/google/android/gms/panorama/math/Vector3d;

    iget-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    iget-wide v2, p1, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    sub-double v1, v0, v2

    iget-wide v3, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iget-wide v5, p1, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    sub-double/2addr v3, v5

    iget-wide v5, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    iget-wide v7, p1, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    sub-double/2addr v5, v7

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/panorama/math/Vector3d;->set(DDD)V

    return-void
.end method


# virtual methods
.method public length()D
    .locals 6

    iget-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    iget-wide v2, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iget-wide v4, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    iget-wide v4, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public normalize()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/panorama/math/Vector3d;->length()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/high16 v2, 0x3ff0000000000000L

    div-double/2addr v2, v0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/panorama/math/Vector3d;->scale(D)V

    :cond_0
    return-void
.end method

.method public scale(D)V
    .locals 2
    .param p1    # D

    iget-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    iget-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iget-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    return-void
.end method

.method public set(DDD)V
    .locals 0
    .param p1    # D
    .param p3    # D
    .param p5    # D

    iput-wide p1, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    iput-wide p3, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iput-wide p5, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    return-void
.end method

.method public set(Lcom/google/android/gms/panorama/math/Vector3d;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/panorama/math/Vector3d;

    iget-wide v0, p1, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    iput-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    iget-wide v0, p1, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iput-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iget-wide v0, p1, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    iput-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    return-void
.end method

.method public setComponent(ID)V
    .locals 1
    .param p1    # I
    .param p2    # D

    if-nez p1, :cond_0

    iput-wide p2, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iput-wide p2, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    goto :goto_0

    :cond_1
    iput-wide p2, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    goto :goto_0
.end method

.method public setZero()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->z:D

    iput-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->y:D

    iput-wide v0, p0, Lcom/google/android/gms/panorama/math/Vector3d;->x:D

    return-void
.end method
