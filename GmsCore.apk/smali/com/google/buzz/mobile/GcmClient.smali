.class public abstract Lcom/google/buzz/mobile/GcmClient;
.super Ljava/lang/Object;
.source "GcmClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/buzz/mobile/GcmClient$D2sInfo;
    }
.end annotation


# instance fields
.field protected lastConnectionOk:Z

.field protected final mAckedS2dMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mAckingInterval:I

.field protected mActive:Z

.field protected final mD2sIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/buzz/mobile/GcmClient$D2sInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected mHost:Ljava/lang/String;

.field protected mId:Ljava/lang/String;

.field protected mIn:Lcom/google/protobuf/micro/CodedInputStreamMicro;

.field protected mLastConnect:J

.field protected mLastDisconnect:J

.field protected mLastStreamIdAcked:I

.field protected mOut:Lcom/google/protobuf/micro/CodedOutputStreamMicro;

.field mPing:Z

.field protected mPort:I

.field protected mReaderThread:Ljava/lang/Thread;

.field protected mResendQueue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;"
        }
    .end annotation
.end field

.field protected mRmqId:J

.field protected mRunning:Z

.field protected mSendQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;"
        }
    .end annotation
.end field

.field protected mServerVersion:I

.field protected mSoTimeout:I

.field protected mSocket:Ljava/net/Socket;

.field protected mSocketFactory:Ljavax/net/SocketFactory;

.field protected mStreamIdIn:I

.field protected mStreamIdOut:I

.field protected mTimeout:I

.field protected mToken:Ljava/lang/String;

.field protected mUnackedS2dIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mWriterThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "mtalk.google.com"

    iput-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mHost:Ljava/lang/String;

    const/16 v0, 0x146c

    iput v0, p0, Lcom/google/buzz/mobile/GcmClient;->mPort:I

    iput-boolean v1, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    iput-boolean v1, p0, Lcom/google/buzz/mobile/GcmClient;->mActive:Z

    const/16 v0, 0xa

    iput v0, p0, Lcom/google/buzz/mobile/GcmClient;->mAckingInterval:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdIn:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    iput-boolean v1, p0, Lcom/google/buzz/mobile/GcmClient;->lastConnectionOk:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mResendQueue:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mSendQueue:Ljava/util/concurrent/BlockingQueue;

    const/16 v0, 0x4e20

    iput v0, p0, Lcom/google/buzz/mobile/GcmClient;->mTimeout:I

    const v0, 0xdbba0

    iput v0, p0, Lcom/google/buzz/mobile/GcmClient;->mSoTimeout:I

    iput-boolean v1, p0, Lcom/google/buzz/mobile/GcmClient;->mPing:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/buzz/mobile/GcmClient;)V
    .locals 0
    .param p0    # Lcom/google/buzz/mobile/GcmClient;

    invoke-direct {p0}, Lcom/google/buzz/mobile/GcmClient;->readerThread()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/buzz/mobile/GcmClient;)V
    .locals 0
    .param p0    # Lcom/google/buzz/mobile/GcmClient;

    invoke-direct {p0}, Lcom/google/buzz/mobile/GcmClient;->writerThread()V

    return-void
.end method

.method private ackD2sMessagesInternal(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/GcmClient$D2sInfo;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v5, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    monitor-enter v5

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/GcmClient$D2sInfo;

    iget-object v4, v0, Lcom/google/buzz/mobile/GcmClient$D2sInfo;->d2sId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_0
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/GcmClient;->onAckReceived(Ljava/util/ArrayList;)V

    return-void
.end method

.method private close(ILjava/lang/String;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    iget v1, p0, Lcom/google/buzz/mobile/GcmClient;->mPort:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-nez v1, :cond_1

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Closing connection "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/google/buzz/mobile/GcmClient;->mActive:Z

    invoke-virtual {p0}, Lcom/google/buzz/mobile/GcmClient;->closeSocket()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/buzz/mobile/GcmClient;->mLastDisconnect:J

    iget-object v1, p0, Lcom/google/buzz/mobile/GcmClient;->mSendQueue:Ljava/util/concurrent/BlockingQueue;

    new-instance v2, Lcom/google/buzz/mobile/proto/GCM$Close;

    invoke-direct {v2}, Lcom/google/buzz/mobile/proto/GCM$Close;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    :try_start_2
    invoke-virtual {p0, p1, p2}, Lcom/google/buzz/mobile/GcmClient;->onDisconnected(ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Error closing"

    invoke-virtual {p0, v1, v0}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private declared-synchronized getNextRmqId()J
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/buzz/mobile/GcmClient;->mRmqId:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/buzz/mobile/GcmClient;->mRmqId:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private internalProcessS2dMessage(Ljava/lang/String;I)V
    .locals 15
    .param p1    # Ljava/lang/String;
    .param p2    # I

    if-eqz p1, :cond_0

    iget-object v13, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    monitor-enter v13

    :try_start_0
    iget-object v12, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v5, 0x0

    const/4 v10, 0x0

    const/4 v12, -0x1

    move/from16 v0, p2

    if-eq v0, v12, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v13, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    monitor-enter v13

    :try_start_1
    iget-object v12, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/buzz/mobile/GcmClient$D2sInfo;

    iget v12, v3, Lcom/google/buzz/mobile/GcmClient$D2sInfo;->streamId:I

    move/from16 v0, p2

    if-ge v0, v12, :cond_5

    :cond_1
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_2

    invoke-direct {p0, v2}, Lcom/google/buzz/mobile/GcmClient;->ackD2sMessagesInternal(Ljava/util/List;)V

    :cond_2
    iget-object v13, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    monitor-enter v13

    :try_start_2
    iget-object v12, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v7

    move-object v11, v10

    move-object v6, v5

    :goto_1
    :try_start_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move/from16 v0, p2

    if-ge v0, v4, :cond_6

    :cond_3
    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-object v10, v11

    move-object v5, v6

    :cond_4
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v10, v5}, Lcom/google/buzz/mobile/GcmClient;->updateS2dIds(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-void

    :catchall_0
    move-exception v12

    :try_start_4
    monitor-exit v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v12

    :cond_5
    :try_start_5
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_1
    move-exception v12

    monitor-exit v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v12

    :cond_6
    if-nez v6, :cond_8

    :try_start_6
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :try_start_7
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    :goto_2
    :try_start_8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v5, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v12, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v12, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :catchall_2
    move-exception v12

    :goto_4
    monitor-exit v13
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v12

    :cond_7
    move-object v11, v10

    move-object v6, v5

    goto :goto_1

    :catchall_3
    move-exception v12

    move-object v10, v11

    move-object v5, v6

    goto :goto_4

    :catchall_4
    move-exception v12

    move-object v10, v11

    goto :goto_4

    :cond_8
    move-object v10, v11

    move-object v5, v6

    goto :goto_2
.end method

.method private processD2sMessage(Ljava/lang/String;)I
    .locals 5
    .param p1    # Ljava/lang/String;

    iget v2, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdOut:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdOut:I

    if-eqz p1, :cond_0

    new-instance v1, Lcom/google/buzz/mobile/GcmClient$D2sInfo;

    iget v2, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdOut:I

    invoke-direct {v1, v2, p1}, Lcom/google/buzz/mobile/GcmClient$D2sInfo;-><init>(ILjava/lang/String;)V

    iget-object v3, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    monitor-enter v3

    :try_start_1
    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v3, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    monitor-enter v3

    if-eqz v0, :cond_2

    :try_start_2
    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    iget v4, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdOut:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    iget v2, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdOut:I

    return v2

    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    :catchall_2
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v2
.end method

.method private processSelectiveAck(Lcom/google/buzz/mobile/proto/GCM$SelectiveAck;)V
    .locals 0
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$SelectiveAck;

    return-void
.end method

.method private readTag()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/google/buzz/mobile/GcmClient;->mIn:Lcom/google/protobuf/micro/CodedInputStreamMicro;

    invoke-virtual {v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readRawByte()B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, -0x1

    goto :goto_0
.end method

.method private readerThread()V
    .locals 10

    const/16 v9, 0x10

    const/4 v8, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/google/buzz/mobile/GcmClient;->mIn:Lcom/google/protobuf/micro/CodedInputStreamMicro;

    invoke-virtual {v6}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readRawByte()B

    move-result v6

    iput v6, p0, Lcom/google/buzz/mobile/GcmClient;->mServerVersion:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget v6, p0, Lcom/google/buzz/mobile/GcmClient;->mServerVersion:I

    const/16 v7, 0x8

    if-ge v6, v7, :cond_1

    const/16 v6, 0xc

    invoke-direct {p0, v6, v8}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v9, v6}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    const-string v6, "Error connectin "

    invoke-virtual {p0, v6, v2}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_1
    iget-boolean v6, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-eqz v6, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/google/buzz/mobile/GcmClient;->readTag()I

    move-result v5

    iget-boolean v6, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-eqz v6, :cond_0

    if-gez v5, :cond_2

    new-instance v6, Ljava/io/IOException;

    const-string v7, "Closed reading tag"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v2

    const-string v6, "Error reading messages"

    invoke-virtual {p0, v6, v2}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v9, v6}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_2
    iget-object v6, p0, Lcom/google/buzz/mobile/GcmClient;->mIn:Lcom/google/protobuf/micro/CodedInputStreamMicro;

    invoke-virtual {p0, v6, v5}, Lcom/google/buzz/mobile/GcmClient;->parsePacket(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v4

    instance-of v6, v4, Lcom/google/buzz/mobile/proto/GCM$LoginResponse;

    if-eqz v6, :cond_4

    check-cast v4, Lcom/google/buzz/mobile/proto/GCM$LoginResponse;

    invoke-virtual {p0, v4}, Lcom/google/buzz/mobile/GcmClient;->handleLoginResponse(Lcom/google/buzz/mobile/proto/GCM$LoginResponse;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-direct {p0, v6, v7}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    goto :goto_0

    :cond_4
    if-nez v3, :cond_5

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-direct {p0, v6, v7}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    :cond_5
    instance-of v6, v4, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;

    if-eqz v6, :cond_6

    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatAck;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {p0, v1, v6}, Lcom/google/buzz/mobile/GcmClient;->sendPacket(Lcom/google/protobuf/micro/MessageMicro;Z)V

    :cond_6
    iget-object v7, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    monitor-enter v7
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    iget v6, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdIn:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdIn:I

    iget v6, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdIn:I

    iget v8, p0, Lcom/google/buzz/mobile/GcmClient;->mLastStreamIdAcked:I

    sub-int/2addr v6, v8

    iget v8, p0, Lcom/google/buzz/mobile/GcmClient;->mAckingInterval:I

    if-lt v6, v8, :cond_7

    invoke-direct {p0}, Lcom/google/buzz/mobile/GcmClient;->sendAck()V

    :cond_7
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-static {v4}, Lcom/google/buzz/mobile/GcmProtoUtils;->getRmq2Id(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4}, Lcom/google/buzz/mobile/GcmProtoUtils;->getLastStreamId(Lcom/google/protobuf/micro/MessageMicro;)I

    move-result v7

    invoke-direct {p0, v6, v7}, Lcom/google/buzz/mobile/GcmClient;->internalProcessS2dMessage(Ljava/lang/String;I)V

    instance-of v6, v4, Lcom/google/buzz/mobile/proto/GCM$SelectiveAck;

    if-eqz v6, :cond_8

    move-object v0, v4

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$SelectiveAck;

    move-object v6, v0

    invoke-direct {p0, v6}, Lcom/google/buzz/mobile/GcmClient;->processSelectiveAck(Lcom/google/buzz/mobile/proto/GCM$SelectiveAck;)V

    :cond_8
    invoke-virtual {p0, v4}, Lcom/google/buzz/mobile/GcmClient;->onMessage(Lcom/google/protobuf/micro/MessageMicro;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1

    :catchall_0
    move-exception v6

    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v6
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
.end method

.method private sendAck()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/buzz/mobile/GcmProtoUtils;->newStreamAck()Lcom/google/buzz/mobile/proto/GCM$IqStanza;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/buzz/mobile/GcmClient;->sendPacket(Lcom/google/protobuf/micro/MessageMicro;Z)V

    return-void
.end method

.method private declared-synchronized sendPacketToWire(Lcom/google/protobuf/micro/MessageMicro;)Z
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/buzz/mobile/GcmProtoUtils;->getRmq2Id(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/buzz/mobile/GcmClient;->processD2sMessage(Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v1, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdIn:I

    iget v3, p0, Lcom/google/buzz/mobile/GcmClient;->mLastStreamIdAcked:I

    if-le v1, v3, :cond_0

    iget v1, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdIn:I

    invoke-static {p1, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->setLastStreamId(Lcom/google/protobuf/micro/MessageMicro;I)V

    iget v1, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdIn:I

    iput v1, p0, Lcom/google/buzz/mobile/GcmClient;->mLastStreamIdAcked:I

    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/GcmClient;->onMessageOut(Lcom/google/protobuf/micro/MessageMicro;)V

    iget-object v1, p0, Lcom/google/buzz/mobile/GcmClient;->mOut:Lcom/google/protobuf/micro/CodedOutputStreamMicro;

    invoke-static {p1, v1}, Lcom/google/buzz/mobile/GcmClient;->serializePacket(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/CodedOutputStreamMicro;)I

    const-string v1, "SENT:"

    invoke-static {v1, p1}, Lcom/google/buzz/mobile/GcmProtoUtils;->toString(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x1

    monitor-exit p0

    return v1

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static serializePacket(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/CodedOutputStreamMicro;)I
    .locals 2
    .param p0    # Lcom/google/protobuf/micro/MessageMicro;
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/google/buzz/mobile/GcmProtoUtils;->getTag(Lcom/google/protobuf/micro/MessageMicro;)B

    move-result v1

    invoke-virtual {p0}, Lcom/google/protobuf/micro/MessageMicro;->getSerializedSize()I

    move-result v0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeRawByte(B)V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeRawVarint32(I)V

    invoke-virtual {p0, p1}, Lcom/google/protobuf/micro/MessageMicro;->writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V

    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->flush()V

    return v0
.end method

.method private updateS2dIds(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/GcmClient;->onS2DReceived(Ljava/lang/String;)V

    :cond_0
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    if-lez v1, :cond_1

    invoke-virtual {p0, p2}, Lcom/google/buzz/mobile/GcmClient;->onS2DConfirmed(Ljava/util/List;)V

    :cond_1
    if-eqz p3, :cond_3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v4

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    return-void
.end method

.method private writerThread()V
    .locals 8

    const/4 v5, 0x0

    const/16 v7, 0xa

    const/16 v6, 0x10

    const-string v3, "Starting a new connection"

    invoke-virtual {p0, v3}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/GcmClient;->connectBlocking()V

    iget-object v3, p0, Lcom/google/buzz/mobile/GcmClient;->mOut:Lcom/google/protobuf/micro/CodedOutputStreamMicro;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeRawByte(I)V

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/buzz/mobile/GcmClient;->lastConnectionOk:Z

    const/4 v3, 0x0

    iput v3, p0, Lcom/google/buzz/mobile/GcmClient;->mLastStreamIdAcked:I

    const/4 v3, 0x1

    iput v3, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdIn:I

    const/4 v3, 0x0

    iput v3, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdOut:I

    invoke-virtual {p0}, Lcom/google/buzz/mobile/GcmClient;->sendLogin()V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_4

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/google/buzz/mobile/GcmClient$1;

    invoke-direct {v4, p0}, Lcom/google/buzz/mobile/GcmClient$1;-><init>(Lcom/google/buzz/mobile/GcmClient;)V

    const-string v5, "GCMReader"

    invoke-direct {v3, v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/buzz/mobile/GcmClient;->mReaderThread:Ljava/lang/Thread;

    iget-object v3, p0, Lcom/google/buzz/mobile/GcmClient;->mReaderThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-eqz v3, :cond_2

    :try_start_1
    iget-object v3, p0, Lcom/google/buzz/mobile/GcmClient;->mSendQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/micro/MessageMicro;

    iget-boolean v3, p0, Lcom/google/buzz/mobile/GcmClient;->mPing:Z

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/buzz/mobile/GcmClient;->mPing:Z

    new-instance v3, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;

    invoke-direct {v3}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;-><init>()V

    invoke-direct {p0, v3}, Lcom/google/buzz/mobile/GcmClient;->sendPacketToWire(Lcom/google/protobuf/micro/MessageMicro;)Z

    invoke-virtual {p0}, Lcom/google/buzz/mobile/GcmClient;->onHearbeatSent()V

    :cond_1
    instance-of v3, v2, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;

    if-nez v3, :cond_0

    instance-of v3, v2, Lcom/google/buzz/mobile/proto/GCM$Close;

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-eqz v3, :cond_0

    invoke-direct {p0, v2}, Lcom/google/buzz/mobile/GcmClient;->sendPacketToWire(Lcom/google/protobuf/micro/MessageMicro;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_6

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-direct {p0, v6, v5}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Timeout error connecting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void

    :catch_2
    move-exception v1

    invoke-direct {p0, v6, v5}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Socket error connecting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;)V

    goto :goto_1

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v6, v3}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IO error connecting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;)V

    goto :goto_1

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v7, v3}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    const-string v3, "Unexpected error connecting "

    invoke-virtual {p0, v3, v1}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_5
    move-exception v0

    iget-boolean v3, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v6, v3}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    const-string v3, "Error writing "

    invoke-virtual {p0, v3, v0}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v7, v3}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    const-string v3, "Unexpected error "

    invoke-virtual {p0, v3, v0}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method protected beforeLogin(Lcom/google/buzz/mobile/proto/GCM$LoginRequest;)V
    .locals 0
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    return-void
.end method

.method protected closeSocket()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mSocket:Ljava/net/Socket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public connect()V
    .locals 2

    const-class v1, Lcom/google/buzz/mobile/GcmClient;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mSocketFactory:Ljavax/net/SocketFactory;

    if-nez v0, :cond_1

    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mSocketFactory:Ljavax/net/SocketFactory;

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/GcmClient;->initWriter()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected connectBlocking()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/buzz/mobile/GcmClient;->mSocketFactory:Ljavax/net/SocketFactory;

    invoke-virtual {v1}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    iget v1, p0, Lcom/google/buzz/mobile/GcmClient;->mTimeout:I

    invoke-virtual {v0, v1}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    new-instance v1, Ljava/net/InetSocketAddress;

    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mHost:Ljava/lang/String;

    iget v3, p0, Lcom/google/buzz/mobile/GcmClient;->mPort:I

    invoke-direct {v1, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iget v2, p0, Lcom/google/buzz/mobile/GcmClient;->mTimeout:I

    invoke-virtual {v0, v1, v2}, Ljavax/net/ssl/SSLSocket;->connect(Ljava/net/SocketAddress;I)V

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    iput-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->newInstance(Ljava/io/OutputStream;)Lcom/google/protobuf/micro/CodedOutputStreamMicro;

    move-result-object v1

    iput-object v1, p0, Lcom/google/buzz/mobile/GcmClient;->mOut:Lcom/google/protobuf/micro/CodedOutputStreamMicro;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v1

    iput-object v1, p0, Lcom/google/buzz/mobile/GcmClient;->mIn:Lcom/google/protobuf/micro/CodedInputStreamMicro;

    return-void
.end method

.method public disconnect(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/buzz/mobile/GcmClient;->close(ILjava/lang/String;)V

    return-void
.end method

.method public getInetAddress()Ljava/net/InetAddress;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mSocket:Ljava/net/Socket;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v0

    goto :goto_0
.end method

.method public getLastConnectionAttemptSuccessful()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->lastConnectionOk:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLastConnectionTime()J
    .locals 4

    iget-wide v0, p0, Lcom/google/buzz/mobile/GcmClient;->mLastConnect:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/buzz/mobile/GcmClient;->mLastConnect:J

    iget-wide v2, p0, Lcom/google/buzz/mobile/GcmClient;->mLastDisconnect:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_1
    iget-wide v0, p0, Lcom/google/buzz/mobile/GcmClient;->mLastDisconnect:J

    iget-wide v2, p0, Lcom/google/buzz/mobile/GcmClient;->mLastConnect:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public getPort()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/GcmClient;->mPort:I

    return v0
.end method

.method public getRemoteAddress()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/GcmClient;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/buzz/mobile/GcmClient;->mPort:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getState()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->mActive:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public getTimeout()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/GcmClient;->mTimeout:I

    return v0
.end method

.method protected handleLoginResponse(Lcom/google/buzz/mobile/proto/GCM$LoginResponse;)Z
    .locals 8
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$LoginResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/google/buzz/mobile/proto/GCM$LoginResponse;->getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;->getCode()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;->getCode()I

    move-result v3

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/GcmClient;->onLoginResponse(Lcom/google/buzz/mobile/proto/GCM$LoginResponse;)V

    :goto_0
    return v4

    :cond_0
    iput-boolean v5, p0, Lcom/google/buzz/mobile/GcmClient;->lastConnectionOk:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/buzz/mobile/GcmClient;->mLastConnect:J

    iget-object v6, p0, Lcom/google/buzz/mobile/GcmClient;->mSocket:Ljava/net/Socket;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/buzz/mobile/GcmClient;->mSocket:Ljava/net/Socket;

    iget v7, p0, Lcom/google/buzz/mobile/GcmClient;->mSoTimeout:I

    invoke-virtual {v6, v7}, Ljava/net/Socket;->setSoTimeout(I)V

    :cond_1
    iput-boolean v5, p0, Lcom/google/buzz/mobile/GcmClient;->mActive:Z

    iget-object v6, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    monitor-enter v6

    :try_start_0
    iget-object v7, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/GcmClient;->onLoginResponse(Lcom/google/buzz/mobile/proto/GCM$LoginResponse;)V

    iget-object v6, p0, Lcom/google/buzz/mobile/GcmClient;->mResendQueue:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {p0, v2, v4}, Lcom/google/buzz/mobile/GcmClient;->sendPacket(Lcom/google/protobuf/micro/MessageMicro;Z)V

    goto :goto_1

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :cond_2
    move v4, v5

    goto :goto_0
.end method

.method protected initWriter()V
    .locals 3

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/buzz/mobile/GcmClient$2;

    invoke-direct {v1, p0}, Lcom/google/buzz/mobile/GcmClient$2;-><init>(Lcom/google/buzz/mobile/GcmClient;)V

    const-string v2, "GCMWriter"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mWriterThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mWriterThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public isActive()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->mActive:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    return v0
.end method

.method protected log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method protected abstract log(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method protected onAckReceived(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mResendQueue:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onDisconnected(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Disconnected: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;)V

    return-void
.end method

.method protected onHearbeatSent()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method protected abstract onLoginResponse(Lcom/google/buzz/mobile/proto/GCM$LoginResponse;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract onMessage(Lcom/google/protobuf/micro/MessageMicro;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected onMessageOut(Lcom/google/protobuf/micro/MessageMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Send message  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;)V

    return-void
.end method

.method protected onS2DConfirmed(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected onS2DReceived(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method protected onSaveMessage(Ljava/lang/String;BLcom/google/protobuf/micro/MessageMicro;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .param p3    # Lcom/google/protobuf/micro/MessageMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/buzz/mobile/GcmClient;->mResendQueue:Ljava/util/Map;

    invoke-interface {v0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method parsePacket(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Lcom/google/protobuf/micro/MessageMicro;
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readRawVarint32()I

    move-result v1

    const/4 v3, 0x0

    new-array v0, v3, [B

    if-lez v1, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readRawBytes(I)[B

    move-result-object v0

    :cond_0
    int-to-byte v3, p2

    invoke-static {v3, v0}, Lcom/google/buzz/mobile/GcmProtoUtils;->parse(B[B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CH-IN: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/buzz/mobile/GcmProtoUtils;->toString(Ljava/lang/String;Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;)V

    return-object v2
.end method

.method public resend(JI[B)V
    .locals 4
    .param p1    # J
    .param p3    # I
    .param p4    # [B

    int-to-byte v2, p3

    :try_start_0
    invoke-static {v2, p4}, Lcom/google/buzz/mobile/GcmProtoUtils;->parse(B[B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    iget-object v3, p0, Lcom/google/buzz/mobile/GcmClient;->mSendQueue:Ljava/util/concurrent/BlockingQueue;

    monitor-enter v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mSendQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2, v0}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    monitor-exit v3

    :goto_0
    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid tag for resend "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendHeartbeat()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/GcmClient;->mPing:Z

    new-instance v0, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;

    invoke-direct {v0}, Lcom/google/buzz/mobile/proto/GCM$HeartbeatPing;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/buzz/mobile/GcmClient;->sendPacket(Lcom/google/protobuf/micro/MessageMicro;Z)V

    goto :goto_0
.end method

.method protected sendLogin()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v12, 0x1

    const/4 v11, 0x0

    iget-object v10, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    monitor-enter v10

    :try_start_0
    iget-object v9, p0, Lcom/google/buzz/mobile/GcmClient;->mD2sIds:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v10, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    monitor-enter v10

    :try_start_1
    iget-object v9, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    iget-object v9, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    invoke-interface {v9, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v9

    :catchall_1
    move-exception v9

    :try_start_2
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v9

    :cond_0
    :try_start_3
    iget-object v9, p0, Lcom/google/buzz/mobile/GcmClient;->mAckedS2dMap:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->clear()V

    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v3, p0, Lcom/google/buzz/mobile/GcmClient;->mId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mToken:Ljava/lang/String;

    new-instance v7, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    invoke-direct {v7}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;-><init>()V

    const-string v9, "login-1"

    invoke-virtual {v7, v9}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    const-string v9, "mcs.android.com"

    invoke-virtual {v7, v9}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setDomain(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    invoke-virtual {v7, v3}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setResource(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "android-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setDeviceId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    invoke-virtual {v7, v11}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setAdaptiveHeartbeat(Z)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    const-wide/32 v9, 0xf4240

    invoke-virtual {v7, v9, v10}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    invoke-virtual {v7, v12}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setNetworkType(I)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    invoke-virtual {v7, v3}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setUser(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    invoke-virtual {v7, v2}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setAuthToken(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    const/4 v9, 0x2

    invoke-virtual {v7, v9}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setAuthService(I)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    invoke-virtual {v7, v12}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->setUseRmq2(Z)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    iget-object v9, p0, Lcom/google/buzz/mobile/GcmClient;->mUnackedS2dIds:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v7, v5}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->addReceivedPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    goto :goto_1

    :cond_1
    new-instance v8, Lcom/google/buzz/mobile/proto/GCM$Setting;

    invoke-direct {v8}, Lcom/google/buzz/mobile/proto/GCM$Setting;-><init>()V

    const-string v9, "new_vc"

    invoke-virtual {v8, v9}, Lcom/google/buzz/mobile/proto/GCM$Setting;->setName(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$Setting;

    const-string v9, "1"

    invoke-virtual {v8, v9}, Lcom/google/buzz/mobile/proto/GCM$Setting;->setValue(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$Setting;

    invoke-virtual {v7, v8}, Lcom/google/buzz/mobile/proto/GCM$LoginRequest;->addSetting(Lcom/google/buzz/mobile/proto/GCM$Setting;)Lcom/google/buzz/mobile/proto/GCM$LoginRequest;

    invoke-virtual {p0, v7}, Lcom/google/buzz/mobile/GcmClient;->beforeLogin(Lcom/google/buzz/mobile/proto/GCM$LoginRequest;)V

    invoke-virtual {p0, v7, v11}, Lcom/google/buzz/mobile/GcmClient;->sendPacket(Lcom/google/protobuf/micro/MessageMicro;Z)V

    return-void
.end method

.method public sendPacket(Lcom/google/protobuf/micro/MessageMicro;Z)V
    .locals 7
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;
    .param p2    # Z

    if-eqz p2, :cond_2

    :try_start_0
    invoke-static {p1}, Lcom/google/buzz/mobile/GcmProtoUtils;->getRmq2Id(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v5, ""

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/buzz/mobile/GcmClient;->getNextRmqId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/buzz/mobile/GcmProtoUtils;->setRmq2Id(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)V

    :cond_1
    invoke-static {p1}, Lcom/google/buzz/mobile/GcmProtoUtils;->getTag(Lcom/google/protobuf/micro/MessageMicro;)B

    move-result v4

    invoke-virtual {p0, v1, v4, p1}, Lcom/google/buzz/mobile/GcmClient;->onSaveMessage(Ljava/lang/String;BLcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    iget-object v6, p0, Lcom/google/buzz/mobile/GcmClient;->mSendQueue:Ljava/util/concurrent/BlockingQueue;

    monitor-enter v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v5, p0, Lcom/google/buzz/mobile/GcmClient;->mSendQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v5, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    monitor-exit v6

    :goto_0
    return-void

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    const-string v5, "Error sending packet"

    invoke-virtual {p0, v5, v0}, Lcom/google/buzz/mobile/GcmClient;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setAckingInterval(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/buzz/mobile/GcmClient;->mAckingInterval:I

    return-void
.end method

.method public setAndroidIdAuth(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/buzz/mobile/GcmClient;->mId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/buzz/mobile/GcmClient;->mToken:Ljava/lang/String;

    return-void
.end method

.method public setServer(Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iput-object p1, p0, Lcom/google/buzz/mobile/GcmClient;->mHost:Ljava/lang/String;

    iput p2, p0, Lcom/google/buzz/mobile/GcmClient;->mPort:I

    return-void
.end method

.method public setSocketFactory(Ljavax/net/SocketFactory;)V
    .locals 0
    .param p1    # Ljavax/net/SocketFactory;

    iput-object p1, p0, Lcom/google/buzz/mobile/GcmClient;->mSocketFactory:Ljavax/net/SocketFactory;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v1, p0, Lcom/google/buzz/mobile/GcmClient;->mRunning:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/buzz/mobile/GcmClient;->mSocket:Ljava/net/Socket;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",host="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/buzz/mobile/GcmClient;->mHost:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/buzz/mobile/GcmClient;->mPort:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",last= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Lcom/google/buzz/mobile/GcmClient;->mLastConnect:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",streamIdIn= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdIn:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",streamIdOut= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/buzz/mobile/GcmClient;->mStreamIdOut:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    iget-boolean v1, p0, Lcom/google/buzz/mobile/GcmClient;->mActive:Z

    if-nez v1, :cond_1

    const-string v1, "Connecting"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lastDisconnect="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Lcom/google/buzz/mobile/GcmClient;->mLastDisconnect:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method
