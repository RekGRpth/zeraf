.class public final Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GCM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/buzz/mobile/proto/GCM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BindAccountResponse"
.end annotation


# instance fields
.field private cachedSize:I

.field private error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

.field private hasError:Z

.field private hasId:Z

.field private hasJid:Z

.field private hasLastStreamIdReceived:Z

.field private hasStreamId:Z

.field private id_:Ljava/lang/String;

.field private jid_:Ljava/lang/String;

.field private lastStreamIdReceived_:I

.field private streamId_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->jid_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->streamId_:I

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->lastStreamIdReceived_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->cachedSize:I

    return v0
.end method

.method public getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getJid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->jid_:Ljava/lang/String;

    return-object v0
.end method

.method public getLastStreamIdReceived()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->lastStreamIdReceived_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasJid()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getJid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasError()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasStreamId()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getStreamId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasLastStreamIdReceived()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getLastStreamIdReceived()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->cachedSize:I

    return v0
.end method

.method public getStreamId()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->streamId_:I

    return v0
.end method

.method public hasError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasError:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasId:Z

    return v0
.end method

.method public hasJid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasJid:Z

    return v0
.end method

.method public hasLastStreamIdReceived()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasLastStreamIdReceived:Z

    return v0
.end method

.method public hasStreamId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasStreamId:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->setJid(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->setError(Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;

    move-result-object v0

    return-object v0
.end method

.method public setError(Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasError:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->id_:Ljava/lang/String;

    return-object p0
.end method

.method public setJid(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasJid:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->jid_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasLastStreamIdReceived:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->lastStreamIdReceived_:I

    return-object p0
.end method

.method public setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasStreamId:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->streamId_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasJid()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getJid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasStreamId()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getStreamId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->hasLastStreamIdReceived()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountResponse;->getLastStreamIdReceived()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    return-void
.end method
