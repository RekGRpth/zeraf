.class public final Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GCM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/buzz/mobile/proto/GCM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BindAccountRequest"
.end annotation


# instance fields
.field private accountId_:J

.field private authToken_:Ljava/lang/String;

.field private cachedSize:I

.field private domain_:Ljava/lang/String;

.field private hasAccountId:Z

.field private hasAuthToken:Z

.field private hasDomain:Z

.field private hasId:Z

.field private hasLastStreamIdReceived:Z

.field private hasPersistentId:Z

.field private hasResource:Z

.field private hasStreamId:Z

.field private hasUser:Z

.field private id_:Ljava/lang/String;

.field private lastStreamIdReceived_:I

.field private persistentId_:Ljava/lang/String;

.field private resource_:Ljava/lang/String;

.field private streamId_:I

.field private user_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->domain_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->user_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->resource_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->authToken_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->persistentId_:Ljava/lang/String;

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->streamId_:I

    iput v1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->lastStreamIdReceived_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->accountId_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAccountId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->accountId_:J

    return-wide v0
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->authToken_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->cachedSize:I

    return v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->domain_:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getLastStreamIdReceived()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->lastStreamIdReceived_:I

    return v0
.end method

.method public getPersistentId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->persistentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getResource()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->resource_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasDomain()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getDomain()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasUser()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getUser()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasResource()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getResource()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasAuthToken()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getAuthToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasPersistentId()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getPersistentId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasStreamId()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getStreamId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasLastStreamIdReceived()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getLastStreamIdReceived()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasAccountId()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getAccountId()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->cachedSize:I

    return v0
.end method

.method public getStreamId()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->streamId_:I

    return v0
.end method

.method public getUser()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->user_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAccountId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasAccountId:Z

    return v0
.end method

.method public hasAuthToken()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasAuthToken:Z

    return v0
.end method

.method public hasDomain()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasDomain:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasId:Z

    return v0
.end method

.method public hasLastStreamIdReceived()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasLastStreamIdReceived:Z

    return v0
.end method

.method public hasPersistentId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasPersistentId:Z

    return v0
.end method

.method public hasResource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasResource:Z

    return v0
.end method

.method public hasStreamId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasStreamId:Z

    return v0
.end method

.method public hasUser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasUser:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setDomain(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setUser(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setResource(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setAuthToken(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;

    move-result-object v0

    return-object v0
.end method

.method public setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasAccountId:Z

    iput-wide p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->accountId_:J

    return-object p0
.end method

.method public setAuthToken(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasAuthToken:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->authToken_:Ljava/lang/String;

    return-object p0
.end method

.method public setDomain(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasDomain:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->domain_:Ljava/lang/String;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->id_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasLastStreamIdReceived:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->lastStreamIdReceived_:I

    return-object p0
.end method

.method public setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasPersistentId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->persistentId_:Ljava/lang/String;

    return-object p0
.end method

.method public setResource(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasResource:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->resource_:Ljava/lang/String;

    return-object p0
.end method

.method public setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasStreamId:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->streamId_:I

    return-object p0
.end method

.method public setUser(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasUser:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->user_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasDomain()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasUser()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getUser()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasResource()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getResource()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasAuthToken()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasPersistentId()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getPersistentId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasStreamId()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getStreamId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasLastStreamIdReceived()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getLastStreamIdReceived()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->hasAccountId()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BindAccountRequest;->getAccountId()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_8
    return-void
.end method
