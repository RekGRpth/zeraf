.class public final Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GCM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/buzz/mobile/proto/GCM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BatchPresenceStanza"
.end annotation


# instance fields
.field private accountId_:J

.field private cachedSize:I

.field private error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

.field private hasAccountId:Z

.field private hasError:Z

.field private hasId:Z

.field private hasLastStreamIdReceived:Z

.field private hasPersistentId:Z

.field private hasStreamId:Z

.field private hasTo:Z

.field private hasType:Z

.field private id_:Ljava/lang/String;

.field private lastStreamIdReceived_:I

.field private persistentId_:Ljava/lang/String;

.field private presence_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;",
            ">;"
        }
    .end annotation
.end field

.field private streamId_:I

.field private to_:Ljava/lang/String;

.field private type_:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->to_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->presence_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->persistentId_:Ljava/lang/String;

    iput v2, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->streamId_:I

    iput v2, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->lastStreamIdReceived_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->accountId_:J

    iput v2, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->type_:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addPresence(Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->presence_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->presence_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->presence_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAccountId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->accountId_:J

    return-wide v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->cachedSize:I

    return v0
.end method

.method public getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getLastStreamIdReceived()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->lastStreamIdReceived_:I

    return v0
.end method

.method public getPersistentId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->persistentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPresenceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->presence_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasId()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasTo()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getTo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getPresenceList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasPersistentId()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasStreamId()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getStreamId()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasLastStreamIdReceived()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getLastStreamIdReceived()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasAccountId()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getAccountId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasType()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasError()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    iput v2, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->cachedSize:I

    return v2
.end method

.method public getStreamId()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->streamId_:I

    return v0
.end method

.method public getTo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->to_:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->type_:I

    return v0
.end method

.method public hasAccountId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasAccountId:Z

    return v0
.end method

.method public hasError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasError:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasId:Z

    return v0
.end method

.method public hasLastStreamIdReceived()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasLastStreamIdReceived:Z

    return v0
.end method

.method public hasPersistentId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasPersistentId:Z

    return v0
.end method

.method public hasStreamId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasStreamId:Z

    return v0
.end method

.method public hasTo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasTo:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->setTo(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->addPresence(Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->setType(I)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    invoke-direct {v1}, Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->setError(Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;

    move-result-object v0

    return-object v0
.end method

.method public setAccountId(J)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasAccountId:Z

    iput-wide p1, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->accountId_:J

    return-object p0
.end method

.method public setError(Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
    .locals 1
    .param p1    # Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasError:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->error_:Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->id_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastStreamIdReceived(I)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasLastStreamIdReceived:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->lastStreamIdReceived_:I

    return-object p0
.end method

.method public setPersistentId(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasPersistentId:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->persistentId_:Ljava/lang/String;

    return-object p0
.end method

.method public setStreamId(I)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasStreamId:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->streamId_:I

    return-object p0
.end method

.method public setTo(Ljava/lang/String;)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasTo:Z

    iput-object p1, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->to_:Ljava/lang/String;

    return-object p0
.end method

.method public setType(I)Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasType:Z

    iput p1, p0, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->type_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasTo()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getTo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getPresenceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/buzz/mobile/proto/GCM$PresenceStanza;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasPersistentId()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getPersistentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasStreamId()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getStreamId()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasLastStreamIdReceived()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getLastStreamIdReceived()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasAccountId()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getAccountId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasType()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->hasError()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/buzz/mobile/proto/GCM$BatchPresenceStanza;->getError()Lcom/google/buzz/mobile/proto/GCM$ErrorInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    return-void
.end method
