.class public Lcom/android/settings/autostarts/LewaManageAutostarts;
.super Lcom/android/settings/SettingsPreferenceFragment;
.source "LewaManageAutostarts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;
    }
.end annotation


# static fields
.field static final DEBUG_SETTING:Z = false

.field public static final MSG_NOTIFY_DATA_CHANGED:I = 0x1

.field public static final TAG:Ljava/lang/String; = "LewaManageAutostarts"


# instance fields
.field private am:Landroid/app/ActivityManager;

.field public handler:Landroid/os/Handler;

.field private mAppAdapter:Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;

.field private mAppList:Landroid/widget/ListView;

.field private mEmptyView:Landroid/widget/TextView;

.field private mInstallPkgNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIntents:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/settings/autostarts/AppItem;",
            ">;"
        }
    .end annotation
.end field

.field private mOptimizedSize:I

.field private mPkgNames:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRootView:Landroid/view/View;

.field private menuItem:Landroid/view/MenuItem;

.field private pm:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mPkgNames:Ljava/util/Vector;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mList:Ljava/util/List;

    new-instance v0, Lcom/android/settings/autostarts/LewaManageAutostarts$5;

    invoke-direct {v0, p0}, Lcom/android/settings/autostarts/LewaManageAutostarts$5;-><init>(Lcom/android/settings/autostarts/LewaManageAutostarts;)V

    iput-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/autostarts/LewaManageAutostarts;)Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;
    .locals 1
    .param p0    # Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppAdapter:Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/settings/autostarts/LewaManageAutostarts;)I
    .locals 1
    .param p0    # Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I

    return v0
.end method

.method static synthetic access$108(Lcom/android/settings/autostarts/LewaManageAutostarts;)I
    .locals 2
    .param p0    # Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I

    return v0
.end method

.method static synthetic access$110(Lcom/android/settings/autostarts/LewaManageAutostarts;)I
    .locals 2
    .param p0    # Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/settings/autostarts/LewaManageAutostarts;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/settings/autostarts/LewaManageAutostarts;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mIntents:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/settings/autostarts/LewaManageAutostarts;)Landroid/content/pm/PackageManager;
    .locals 1
    .param p0    # Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->pm:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/settings/autostarts/LewaManageAutostarts;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0    # Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/settings/autostarts/LewaManageAutostarts;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mList:Ljava/util/List;

    return-object v0
.end method

.method private initDatas()V
    .locals 7

    const/4 v5, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mInstallPkgNames:Ljava/util/List;

    iget-object v4, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    :try_start_0
#    iget-object v4, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->pm:Landroid/content/pm/PackageManager;

#    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

#    const/4 v6, 0x0

#    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

#    move-result-object v4

#    iget v0, v4, Landroid/content/pm/ApplicationInfo;->flags:I

#    and-int/lit8 v4, v0, 0x1

#    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mInstallPkgNames:Ljava/util/List;

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Landroid/content/AutoStarts;->getInstance(Landroid/content/Context;)Landroid/content/AutoStarts;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/AutoStarts;->getIntents()Ljava/util/Set;

    move-result-object v4

    iput-object v4, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mIntents:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public closeAutoStartApp(Lcom/android/settings/autostarts/AppItem;)V
    .locals 1
    .param p1    # Lcom/android/settings/autostarts/AppItem;

    new-instance v0, Lcom/android/settings/autostarts/LewaManageAutostarts$3;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/autostarts/LewaManageAutostarts$3;-><init>(Lcom/android/settings/autostarts/LewaManageAutostarts;Lcom/android/settings/autostarts/AppItem;)V

    invoke-virtual {v0}, Lcom/android/settings/autostarts/LewaManageAutostarts$3;->start()V

    return-void
.end method

.method protected getCurrentLists()V
    .locals 25

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mPkgNames:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    new-instance v10, Landroid/content/Intent;

    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-direct {v10, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->pm:Landroid/content/pm/PackageManager;

    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x200

    invoke-virtual {v2, v3, v5}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v24

    new-instance v23, Ljava/util/HashSet;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashSet;-><init>()V

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v21

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mIntents:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/content/Intent;

    invoke-virtual/range {v17 .. v17}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->pm:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_2
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v22

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, v22

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->pm:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_4
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v22

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move-object/from16 v0, v22

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    const/16 v18, 0x0

    :goto_3
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v18

    if-ge v0, v2, :cond_7

    move/from16 v0, v18

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v16

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mPkgNames:Ljava/util/Vector;

    invoke-virtual {v2, v4}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mInstallPkgNames:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v1, Lcom/android/settings/autostarts/AppItem;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, v16

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const v6, 0x7f0b0972

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-direct/range {v1 .. v7}, Lcom/android/settings/autostarts/AppItem;-><init>(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mPkgNames:Ljava/util/Vector;

    invoke-virtual {v2, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I

    :cond_6
    add-int/lit8 v18, v18, 0x1

    goto :goto_3

    :cond_7
    const/4 v13, 0x0

    :goto_4
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    if-ge v13, v2, :cond_9

    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v16

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mPkgNames:Ljava/util/Vector;

    invoke-virtual {v2, v4}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mInstallPkgNames:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    new-instance v1, Lcom/android/settings/autostarts/AppItem;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, v16

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const v6, 0x7f0b0972

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/android/settings/autostarts/AppItem;-><init>(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mPkgNames:Ljava/util/Vector;

    invoke-virtual {v2, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I

    :cond_8
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    :cond_9
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mIntents:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_a
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/content/Intent;

    invoke-virtual/range {v17 .. v17}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->pm:Landroid/content/pm/PackageManager;

    const/16 v3, 0x200

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_b
    :goto_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v22

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move-object/from16 v0, v22

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->pm:Landroid/content/pm/PackageManager;

    const/16 v3, 0x200

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_d
    :goto_6
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v22

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move-object/from16 v0, v22

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_e
    const/16 v18, 0x0

    :goto_7
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v18

    if-ge v0, v2, :cond_10

    move/from16 v0, v18

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v16

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mPkgNames:Ljava/util/Vector;

    invoke-virtual {v2, v4}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mInstallPkgNames:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    new-instance v1, Lcom/android/settings/autostarts/AppItem;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v16

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const v6, 0x7f0b0972

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-direct/range {v1 .. v7}, Lcom/android/settings/autostarts/AppItem;-><init>(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mPkgNames:Ljava/util/Vector;

    invoke-virtual {v2, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    add-int/lit8 v18, v18, 0x1

    goto :goto_7

    :cond_10
    const/4 v13, 0x0

    :goto_8
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    if-ge v13, v2, :cond_12

    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v16

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mPkgNames:Ljava/util/Vector;

    invoke-virtual {v2, v4}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mInstallPkgNames:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    new-instance v1, Lcom/android/settings/autostarts/AppItem;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v16

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const v6, 0x7f0b0973

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/android/settings/autostarts/AppItem;-><init>(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mPkgNames:Ljava/util/Vector;

    invoke-virtual {v2, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_11
    add-int/lit8 v13, v13, 0x1

    goto :goto_8

    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settings/autostarts/LewaManageAutostarts;->handler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getActivity()Landroid/app/Activity;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->am:Landroid/app/ActivityManager;

    invoke-virtual {p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->pm:Landroid/content/pm/PackageManager;

    invoke-direct {p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->initDatas()V

    invoke-virtual {p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getCurrentLists()V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/16 v6, 0x18

    const/4 v5, 0x1

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0400b3

    invoke-virtual {v1, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mRootView:Landroid/view/View;

    new-instance v1, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mList:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;-><init>(Lcom/android/settings/autostarts/LewaManageAutostarts;Landroid/view/LayoutInflater;Ljava/util/List;)V

    iput-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppAdapter:Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mRootView:Landroid/view/View;

    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mRootView:Landroid/view/View;

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mEmptyView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mEmptyView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mEmptyView:Landroid/widget/TextView;

    const v2, 0x7f0b0976

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    :cond_0
    instance-of v1, p2, Landroid/preference/PreferenceFrameLayout;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mRootView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    if-eqz v0, :cond_1

    iput-boolean v5, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    :cond_1
    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppList:Landroid/widget/ListView;

    invoke-virtual {v1, v6, v4, v6, v4}, Landroid/widget/ListView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppList:Landroid/widget/ListView;

    const v2, 0x7f0200da

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelector(I)V

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppList:Landroid/widget/ListView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setScrollbarFadingEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppList:Landroid/widget/ListView;

    const/high16 v2, 0x2000000

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppList:Landroid/widget/ListView;

    new-instance v2, Lcom/android/settings/autostarts/LewaManageAutostarts$1;

    invoke-direct {v2, p0}, Lcom/android/settings/autostarts/LewaManageAutostarts$1;-><init>(Lcom/android/settings/autostarts/LewaManageAutostarts;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/android/settings/autostarts/LewaManageAutostarts$2;

    invoke-direct {v2, p0}, Lcom/android/settings/autostarts/LewaManageAutostarts$2;-><init>(Lcom/android/settings/autostarts/LewaManageAutostarts;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts;->mRootView:Landroid/view/View;

    return-object v1
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    return-void
.end method

.method public startAutoStartApp(Lcom/android/settings/autostarts/AppItem;)V
    .locals 1
    .param p1    # Lcom/android/settings/autostarts/AppItem;

    new-instance v0, Lcom/android/settings/autostarts/LewaManageAutostarts$4;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/autostarts/LewaManageAutostarts$4;-><init>(Lcom/android/settings/autostarts/LewaManageAutostarts;Lcom/android/settings/autostarts/AppItem;)V

    invoke-virtual {v0}, Lcom/android/settings/autostarts/LewaManageAutostarts$4;->start()V

    return-void
.end method
