.class Ljava/util/zip/ZipFile$RAFStream;
.super Ljava/io/InputStream;
.source "ZipFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljava/util/zip/ZipFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RAFStream"
.end annotation


# instance fields
.field private length:J

.field private offset:J

.field private final sharedRaf:Ljava/io/RandomAccessFile;


# direct methods
.method public constructor <init>(Ljava/io/RandomAccessFile;J)V
    .locals 2
    .param p1    # Ljava/io/RandomAccessFile;
    .param p2    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Ljava/util/zip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    iput-wide p2, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    iput-wide v0, p0, Ljava/util/zip/ZipFile$RAFStream;->length:J

    return-void
.end method

.method static synthetic access$102(Ljava/util/zip/ZipFile$RAFStream;J)J
    .locals 0
    .param p0    # Ljava/util/zip/ZipFile$RAFStream;
    .param p1    # J

    iput-wide p1, p0, Ljava/util/zip/ZipFile$RAFStream;->length:J

    return-wide p1
.end method

.method static synthetic access$200(Ljava/util/zip/ZipFile$RAFStream;)J
    .locals 2
    .param p0    # Ljava/util/zip/ZipFile$RAFStream;

    iget-wide v0, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    return-wide v0
.end method


# virtual methods
.method public available()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-wide v0, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    iget-wide v2, p0, Ljava/util/zip/ZipFile$RAFStream;->length:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fill(Ljava/util/zip/Inflater;I)I
    .locals 8
    .param p1    # Ljava/util/zip/Inflater;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Ljava/util/zip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    monitor-enter v3

    :try_start_0
    iget-wide v4, p0, Ljava/util/zip/ZipFile$RAFStream;->length:J

    iget-wide v6, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    sub-long/2addr v4, v6

    long-to-int v2, v4

    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Ljava/util/zip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    iget-wide v4, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    invoke-virtual {p1, v2, v4, v5, p2}, Ljava/util/zip/Inflater;->setFileInput(Ljava/io/FileDescriptor;JI)I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {p0, v4, v5}, Ljava/util/zip/ZipFile$RAFStream;->skip(J)J

    monitor-exit v3

    return v1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Llibcore/io/Streams;->readSingleByte(Ljava/io/InputStream;)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 9
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Ljava/util/zip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Ljava/util/zip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    iget-wide v3, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    invoke-virtual {v1, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    int-to-long v3, p3

    iget-wide v5, p0, Ljava/util/zip/ZipFile$RAFStream;->length:J

    iget-wide v7, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    sub-long/2addr v5, v7

    cmp-long v1, v3, v5

    if-lez v1, :cond_0

    iget-wide v3, p0, Ljava/util/zip/ZipFile$RAFStream;->length:J

    iget-wide v5, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    sub-long/2addr v3, v5

    long-to-int p3, v3

    :cond_0
    iget-object v1, p0, Ljava/util/zip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v0

    if-lez v0, :cond_1

    iget-wide v3, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    int-to-long v5, v0

    add-long/2addr v3, v5

    iput-wide v3, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    monitor-exit v2

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public skip(J)J
    .locals 4
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-wide v0, p0, Ljava/util/zip/ZipFile$RAFStream;->length:J

    iget-wide v2, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    iget-wide v0, p0, Ljava/util/zip/ZipFile$RAFStream;->length:J

    iget-wide v2, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    sub-long p1, v0, v2

    :cond_0
    iget-wide v0, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Ljava/util/zip/ZipFile$RAFStream;->offset:J

    return-wide p1
.end method
