.class public Ljava/util/concurrent/ConcurrentHashMap;
.super Ljava/util/AbstractMap;
.source "ConcurrentHashMap.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljava/util/concurrent/ConcurrentHashMap$EntrySet;,
        Ljava/util/concurrent/ConcurrentHashMap$Values;,
        Ljava/util/concurrent/ConcurrentHashMap$KeySet;,
        Ljava/util/concurrent/ConcurrentHashMap$EntryIterator;,
        Ljava/util/concurrent/ConcurrentHashMap$WriteThroughEntry;,
        Ljava/util/concurrent/ConcurrentHashMap$ValueIterator;,
        Ljava/util/concurrent/ConcurrentHashMap$KeyIterator;,
        Ljava/util/concurrent/ConcurrentHashMap$HashIterator;,
        Ljava/util/concurrent/ConcurrentHashMap$Segment;,
        Ljava/util/concurrent/ConcurrentHashMap$HashEntry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final DEFAULT_CONCURRENCY_LEVEL:I = 0x10

.field static final DEFAULT_INITIAL_CAPACITY:I = 0x10

.field static final DEFAULT_LOAD_FACTOR:F = 0.75f

.field static final MAXIMUM_CAPACITY:I = 0x40000000

.field static final MAX_SEGMENTS:I = 0x10000

.field static final MIN_SEGMENT_TABLE_CAPACITY:I = 0x2

.field static final RETRIES_BEFORE_LOCK:I = 0x2

.field private static final SBASE:J

.field private static final SSHIFT:I

.field private static final TBASE:J

.field private static final TSHIFT:I

.field private static final UNSAFE:Lsun/misc/Unsafe;

.field private static final serialVersionUID:J = 0x6499de129d87293dL


# instance fields
.field transient entrySet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field transient keySet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field final segmentMask:I

.field final segmentShift:I

.field final segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/concurrent/ConcurrentHashMap$Segment",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field transient values:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    :try_start_0
    invoke-static {}, Lsun/misc/Unsafe;->getUnsafe()Lsun/misc/Unsafe;

    move-result-object v5

    sput-object v5, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    const-class v3, [Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    const-class v1, [Ljava/util/concurrent/ConcurrentHashMap$Segment;

    sget-object v5, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v5, v3}, Lsun/misc/Unsafe;->arrayBaseOffset(Ljava/lang/Class;)I

    move-result v5

    int-to-long v5, v5

    sput-wide v5, Ljava/util/concurrent/ConcurrentHashMap;->TBASE:J

    sget-object v5, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v5, v1}, Lsun/misc/Unsafe;->arrayBaseOffset(Ljava/lang/Class;)I

    move-result v5

    int-to-long v5, v5

    sput-wide v5, Ljava/util/concurrent/ConcurrentHashMap;->SBASE:J

    sget-object v5, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v5, v3}, Lsun/misc/Unsafe;->arrayIndexScale(Ljava/lang/Class;)I

    move-result v4

    sget-object v5, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v5, v1}, Lsun/misc/Unsafe;->arrayIndexScale(Ljava/lang/Class;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    add-int/lit8 v5, v2, -0x1

    and-int/2addr v5, v2

    if-nez v5, :cond_0

    add-int/lit8 v5, v4, -0x1

    and-int/2addr v5, v4

    if-eqz v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/Error;

    const-string v6, "data type scale not a power of two"

    invoke-direct {v5, v6}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v5

    :catch_0
    move-exception v0

    new-instance v5, Ljava/lang/Error;

    invoke-direct {v5, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v5

    rsub-int/lit8 v5, v5, 0x1f

    sput v5, Ljava/util/concurrent/ConcurrentHashMap;->SSHIFT:I

    invoke-static {v4}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v5

    rsub-int/lit8 v5, v5, 0x1f

    sput v5, Ljava/util/concurrent/ConcurrentHashMap;->TSHIFT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/16 v1, 0x10

    const/high16 v0, 0x3f400000

    invoke-direct {p0, v1, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1    # I

    const/high16 v0, 0x3f400000

    const/16 v1, 0x10

    invoke-direct {p0, p1, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    return-void
.end method

.method public constructor <init>(IF)V
    .locals 1
    .param p1    # I
    .param p2    # F

    const/16 v0, 0x10

    invoke-direct {p0, p1, p2, v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    return-void
.end method

.method public constructor <init>(IFI)V
    .locals 9
    .param p1    # I
    .param p2    # F
    .param p3    # I

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    const/4 v6, 0x0

    cmpl-float v6, p2, v6

    if-lez v6, :cond_0

    if-ltz p1, :cond_0

    if-gtz p3, :cond_1

    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v6

    :cond_1
    const/high16 v6, 0x10000

    if-le p3, v6, :cond_2

    const/high16 p3, 0x10000

    :cond_2
    const/4 v4, 0x0

    const/4 v5, 0x1

    :goto_0
    if-ge v5, p3, :cond_3

    add-int/lit8 v4, v4, 0x1

    shl-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    rsub-int/lit8 v6, v4, 0x20

    iput v6, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentShift:I

    add-int/lit8 v6, v5, -0x1

    iput v6, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentMask:I

    const/high16 v6, 0x40000000

    if-le p1, v6, :cond_4

    const/high16 p1, 0x40000000

    :cond_4
    div-int v0, p1, v5

    mul-int v6, v0, v5

    if-ge v6, p1, :cond_5

    add-int/lit8 v0, v0, 0x1

    :cond_5
    const/4 v1, 0x2

    :goto_1
    if-ge v1, v0, :cond_6

    shl-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    new-instance v2, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    int-to-float v6, v1

    mul-float/2addr v6, p2

    float-to-int v7, v6

    new-array v6, v1, [Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    check-cast v6, [Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    invoke-direct {v2, p2, v7, v6}, Ljava/util/concurrent/ConcurrentHashMap$Segment;-><init>(FI[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V

    new-array v3, v5, [Ljava/util/concurrent/ConcurrentHashMap$Segment;

    check-cast v3, [Ljava/util/concurrent/ConcurrentHashMap$Segment;

    sget-object v6, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v7, Ljava/util/concurrent/ConcurrentHashMap;->SBASE:J

    invoke-virtual {v6, v3, v7, v8, v2}, Lsun/misc/Unsafe;->putOrderedObject(Ljava/lang/Object;JLjava/lang/Object;)V

    iput-object v3, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    const/16 v2, 0x10

    const/high16 v1, 0x3f400000

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-virtual {p0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method private ensureSegment(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;
    .locals 17
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/concurrent/ConcurrentHashMap$Segment",
            "<TK;TV;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v2, v0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    sget v1, Ljava/util/concurrent/ConcurrentHashMap;->SSHIFT:I

    shl-int v1, p1, v1

    int-to-long v13, v1

    sget-wide v15, Ljava/util/concurrent/ConcurrentHashMap;->SBASE:J

    add-long v3, v13, v15

    sget-object v1, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v1, v2, v3, v4}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    if-nez v10, :cond_1

    const/4 v1, 0x0

    aget-object v9, v2, v1

    iget-object v1, v9, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    array-length v7, v1

    iget v8, v9, Ljava/util/concurrent/ConcurrentHashMap$Segment;->loadFactor:F

    int-to-float v1, v7

    mul-float/2addr v1, v8

    float-to-int v12, v1

    new-array v11, v7, [Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    check-cast v11, [Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    sget-object v1, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v1, v2, v3, v4}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    if-nez v10, :cond_1

    new-instance v6, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    invoke-direct {v6, v8, v12, v11}, Ljava/util/concurrent/ConcurrentHashMap$Segment;-><init>(FI[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;)V

    :cond_0
    sget-object v1, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v1, v2, v3, v4}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    if-nez v10, :cond_1

    sget-object v1, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    const/4 v5, 0x0

    move-object v10, v6

    invoke-virtual/range {v1 .. v6}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_1
    return-object v10
.end method

.method static final entryAt([Ljava/util/concurrent/ConcurrentHashMap$HashEntry;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/util/concurrent/ConcurrentHashMap$HashEntry",
            "<TK;TV;>;I)",
            "Ljava/util/concurrent/ConcurrentHashMap$HashEntry",
            "<TK;TV;>;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    int-to-long v1, p1

    sget v3, Ljava/util/concurrent/ConcurrentHashMap;->TSHIFT:I

    shl-long/2addr v1, v3

    sget-wide v3, Ljava/util/concurrent/ConcurrentHashMap;->TBASE:J

    add-long/2addr v1, v3

    invoke-virtual {v0, p0, v1, v2}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    goto :goto_0
.end method

.method static final entryForHash(Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/ConcurrentHashMap$Segment",
            "<TK;TV;>;I)",
            "Ljava/util/concurrent/ConcurrentHashMap$HashEntry",
            "<TK;TV;>;"
        }
    .end annotation

    if-eqz p0, :cond_0

    iget-object v0, p0, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    if-nez v0, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    and-int/2addr v2, p1

    int-to-long v2, v2

    sget v4, Ljava/util/concurrent/ConcurrentHashMap;->TSHIFT:I

    shl-long/2addr v2, v4

    sget-wide v4, Ljava/util/concurrent/ConcurrentHashMap;->TBASE:J

    add-long/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    goto :goto_0
.end method

.method private static hash(I)I
    .locals 2
    .param p0    # I

    shl-int/lit8 v0, p0, 0xf

    xor-int/lit16 v0, v0, -0x3283

    add-int/2addr p0, v0

    ushr-int/lit8 v0, p0, 0xa

    xor-int/2addr p0, v0

    shl-int/lit8 v0, p0, 0x3

    add-int/2addr p0, v0

    ushr-int/lit8 v0, p0, 0x6

    xor-int/2addr p0, v0

    shl-int/lit8 v0, p0, 0x2

    shl-int/lit8 v1, p0, 0xe

    add-int/2addr v0, v1

    add-int/2addr p0, v0

    ushr-int/lit8 v0, p0, 0x10

    xor-int/2addr v0, p0

    return v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 8
    .param p1    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    const/4 v0, 0x2

    iget-object v4, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    const/4 v1, 0x0

    :goto_0
    array-length v6, v4

    if-ge v1, v6, :cond_2

    aget-object v3, v4, v1

    if-eqz v3, :cond_0

    int-to-float v6, v0

    iget v7, v3, Ljava/util/concurrent/ConcurrentHashMap$Segment;->loadFactor:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v3, Ljava/util/concurrent/ConcurrentHashMap$Segment;->threshold:I

    new-array v6, v0, [Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    check-cast v6, [Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    iput-object v6, v3, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v2, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v5

    if-nez v2, :cond_1

    return-void
.end method

.method static final segmentAt([Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$Segment;
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/util/concurrent/ConcurrentHashMap$Segment",
            "<TK;TV;>;I)",
            "Ljava/util/concurrent/ConcurrentHashMap$Segment",
            "<TK;TV;>;"
        }
    .end annotation

    sget v2, Ljava/util/concurrent/ConcurrentHashMap;->SSHIFT:I

    shl-int v2, p1, v2

    int-to-long v2, v2

    sget-wide v4, Ljava/util/concurrent/ConcurrentHashMap;->SBASE:J

    add-long v0, v2, v4

    if-nez p0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v2, p0, v0, v1}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    goto :goto_0
.end method

.method private segmentForHash(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/concurrent/ConcurrentHashMap$Segment",
            "<TK;TV;>;"
        }
    .end annotation

    iget v2, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentShift:I

    ushr-int v2, p1, v2

    iget v3, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentMask:I

    and-int/2addr v2, v3

    sget v3, Ljava/util/concurrent/ConcurrentHashMap;->SSHIFT:I

    shl-int/2addr v2, v3

    int-to-long v2, v2

    sget-wide v4, Ljava/util/concurrent/ConcurrentHashMap;->SBASE:J

    add-long v0, v2, v4

    sget-object v2, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    iget-object v3, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    invoke-virtual {v2, v3, v0, v1}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    return-object v2
.end method

.method static final setEntryAt([Ljava/util/concurrent/ConcurrentHashMap$HashEntry;ILjava/util/concurrent/ConcurrentHashMap$HashEntry;)V
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/util/concurrent/ConcurrentHashMap$HashEntry",
            "<TK;TV;>;I",
            "Ljava/util/concurrent/ConcurrentHashMap$HashEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    sget-object v0, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    int-to-long v1, p1

    sget v3, Ljava/util/concurrent/ConcurrentHashMap;->TSHIFT:I

    shl-long/2addr v1, v3

    sget-wide v3, Ljava/util/concurrent/ConcurrentHashMap;->TBASE:J

    add-long/2addr v1, v3

    invoke-virtual {v0, p0, v1, v2, p2}, Lsun/misc/Unsafe;->putOrderedObject(Ljava/lang/Object;JLjava/lang/Object;)V

    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 8
    .param p1    # Ljava/io/ObjectOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x0

    const/4 v2, 0x0

    :goto_0
    iget-object v6, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    array-length v6, v6

    if-ge v2, v6, :cond_0

    invoke-direct {p0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->ensureSegment(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    iget-object v4, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    const/4 v2, 0x0

    :goto_1
    array-length v6, v4

    if-ge v2, v6, :cond_3

    invoke-static {v4, v2}, Ljava/util/concurrent/ConcurrentHashMap;->segmentAt([Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v5, v3, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    const/4 v1, 0x0

    :goto_2
    array-length v6, v5

    if-ge v1, v6, :cond_2

    invoke-static {v5, v1}, Ljava/util/concurrent/ConcurrentHashMap;->entryAt([Ljava/util/concurrent/ConcurrentHashMap$HashEntry;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_1

    iget-object v6, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->key:Ljava/lang/Object;

    invoke-virtual {p1, v6}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v6, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    invoke-virtual {p1, v6}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catchall_0
    move-exception v6

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v6

    :cond_2
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p1, v7}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {p1, v7}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 4

    iget-object v2, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    invoke-static {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->segmentAt([Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->clear()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-virtual {p0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 12
    .param p1    # Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v7

    invoke-static {v7}, Ljava/util/concurrent/ConcurrentHashMap;->hash(I)I

    move-result v1

    iget v7, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentShift:I

    ushr-int v7, v1, v7

    iget v8, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentMask:I

    and-int/2addr v7, v8

    sget v8, Ljava/util/concurrent/ConcurrentHashMap;->SSHIFT:I

    shl-int/2addr v7, v8

    int-to-long v7, v7

    sget-wide v9, Ljava/util/concurrent/ConcurrentHashMap;->SBASE:J

    add-long v5, v7, v9

    sget-object v7, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    iget-object v8, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    invoke-virtual {v7, v8, v5, v6}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    if-eqz v3, :cond_2

    iget-object v4, v3, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    if-eqz v4, :cond_2

    sget-object v7, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    and-int/2addr v8, v1

    int-to-long v8, v8

    sget v10, Ljava/util/concurrent/ConcurrentHashMap;->TSHIFT:I

    shl-long/2addr v8, v10

    sget-wide v10, Ljava/util/concurrent/ConcurrentHashMap;->TBASE:J

    add-long/2addr v8, v10

    invoke-virtual {v7, v4, v8, v9}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    :goto_0
    if-eqz v0, :cond_2

    iget-object v2, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->key:Ljava/lang/Object;

    if-eq v2, p1, :cond_0

    iget v7, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->hash:I

    if-ne v7, v1, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    const/4 v7, 0x1

    :goto_1
    return v7

    :cond_1
    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 16
    .param p1    # Ljava/lang/Object;

    if-nez p1, :cond_0

    new-instance v14, Ljava/lang/NullPointerException;

    invoke-direct {v14}, Ljava/lang/NullPointerException;-><init>()V

    throw v14

    :cond_0
    move-object/from16 v0, p0

    iget-object v9, v0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    const/4 v7, -0x1

    :goto_0
    const-wide/16 v10, 0x0

    const/4 v3, 0x0

    :goto_1
    :try_start_0
    array-length v14, v9

    if-ge v3, v14, :cond_7

    const/4 v14, 0x2

    if-ne v7, v14, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->ensureSegment(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    add-int/lit8 v4, v4, 0x1

    :cond_1
    iget-object v12, v8, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    if-eqz v12, :cond_3

    const/4 v2, 0x0

    :goto_2
    array-length v14, v12

    if-ge v2, v14, :cond_6

    invoke-static {v12, v2}, Ljava/util/concurrent/ConcurrentHashMap;->entryAt([Ljava/util/concurrent/ConcurrentHashMap$HashEntry;I)Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    move-result-object v1

    :goto_3
    if-eqz v1, :cond_5

    iget-object v13, v1, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    if-eqz v13, :cond_4

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v14

    if-eqz v14, :cond_4

    const/4 v14, 0x1

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v4, :cond_c

    aget-object v15, v9, v3

    invoke-virtual {v15}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_2
    :try_start_1
    invoke-static {v9, v3}, Ljava/util/concurrent/ConcurrentHashMap;->segmentAt([Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v8

    if-nez v8, :cond_1

    :cond_3
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, v1, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    goto :goto_3

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    iget v14, v8, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long v14, v14

    add-long/2addr v10, v14

    goto :goto_5

    :cond_7
    if-ltz v7, :cond_8

    cmp-long v14, v10, v5

    if-eqz v14, :cond_9

    :cond_8
    if-lez v4, :cond_a

    :cond_9
    const/4 v14, 0x0

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v4, :cond_c

    aget-object v15, v9, v3

    invoke-virtual {v15}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_a
    move-wide v5, v10

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_b
    throw v14

    :cond_c
    return v14

    :catchall_0
    move-exception v14

    const/4 v3, 0x0

    :goto_7
    if-ge v3, v4, :cond_b

    aget-object v15, v9, v3

    invoke-virtual {v15}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_7
.end method

.method public elements()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<TV;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap$ValueIterator;

    invoke-direct {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap$ValueIterator;-><init>(Ljava/util/concurrent/ConcurrentHashMap;)V

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    iget-object v0, p0, Ljava/util/concurrent/ConcurrentHashMap;->entrySet:Ljava/util/Set;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap$EntrySet;

    invoke-direct {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap$EntrySet;-><init>(Ljava/util/concurrent/ConcurrentHashMap;)V

    iput-object v0, p0, Ljava/util/concurrent/ConcurrentHashMap;->entrySet:Ljava/util/Set;

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v7

    invoke-static {v7}, Ljava/util/concurrent/ConcurrentHashMap;->hash(I)I

    move-result v1

    iget v7, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentShift:I

    ushr-int v7, v1, v7

    iget v8, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentMask:I

    and-int/2addr v7, v8

    sget v8, Ljava/util/concurrent/ConcurrentHashMap;->SSHIFT:I

    shl-int/2addr v7, v8

    int-to-long v7, v7

    sget-wide v9, Ljava/util/concurrent/ConcurrentHashMap;->SBASE:J

    add-long v5, v7, v9

    sget-object v7, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    iget-object v8, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    invoke-virtual {v7, v8, v5, v6}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    if-eqz v3, :cond_2

    iget-object v4, v3, Ljava/util/concurrent/ConcurrentHashMap$Segment;->table:[Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    if-eqz v4, :cond_2

    sget-object v7, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    and-int/2addr v8, v1

    int-to-long v8, v8

    sget v10, Ljava/util/concurrent/ConcurrentHashMap;->TSHIFT:I

    shl-long/2addr v8, v10

    sget-wide v10, Ljava/util/concurrent/ConcurrentHashMap;->TBASE:J

    add-long/2addr v8, v10

    invoke-virtual {v7, v4, v8, v9}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    :goto_0
    if-eqz v0, :cond_2

    iget-object v2, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->key:Ljava/lang/Object;

    if-eq v2, p1, :cond_0

    iget v7, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->hash:I

    if-ne v7, v1, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    iget-object v7, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->value:Ljava/lang/Object;

    :goto_1
    return-object v7

    :cond_1
    iget-object v0, v0, Ljava/util/concurrent/ConcurrentHashMap$HashEntry;->next:Ljava/util/concurrent/ConcurrentHashMap$HashEntry;

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public isEmpty()Z
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v5, 0x0

    const-wide/16 v3, 0x0

    iget-object v2, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    const/4 v0, 0x0

    :goto_0
    array-length v6, v2

    if-ge v0, v6, :cond_3

    invoke-static {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->segmentAt([Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v1

    if-eqz v1, :cond_2

    iget v6, v1, Ljava/util/concurrent/ConcurrentHashMap$Segment;->count:I

    if-eqz v6, :cond_1

    :cond_0
    :goto_1
    return v5

    :cond_1
    iget v6, v1, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    int-to-long v6, v6

    add-long/2addr v3, v6

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    cmp-long v6, v3, v8

    if-eqz v6, :cond_6

    const/4 v0, 0x0

    :goto_2
    array-length v6, v2

    if-ge v0, v6, :cond_5

    invoke-static {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->segmentAt([Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v1

    if-eqz v1, :cond_4

    iget v6, v1, Ljava/util/concurrent/ConcurrentHashMap$Segment;->count:I

    if-nez v6, :cond_0

    iget v6, v1, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    int-to-long v6, v6

    sub-long/2addr v3, v6

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    cmp-long v6, v3, v8

    if-nez v6, :cond_0

    :cond_6
    const/4 v5, 0x1

    goto :goto_1
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    iget-object v0, p0, Ljava/util/concurrent/ConcurrentHashMap;->keySet:Ljava/util/Set;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap$KeySet;

    invoke-direct {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap$KeySet;-><init>(Ljava/util/concurrent/ConcurrentHashMap;)V

    iput-object v0, p0, Ljava/util/concurrent/ConcurrentHashMap;->keySet:Ljava/util/Set;

    goto :goto_0
.end method

.method public keys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<TK;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap$KeyIterator;

    invoke-direct {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap$KeyIterator;-><init>(Ljava/util/concurrent/ConcurrentHashMap;)V

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    if-nez p2, :cond_0

    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/util/concurrent/ConcurrentHashMap;->hash(I)I

    move-result v0

    iget v3, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentShift:I

    ushr-int v3, v0, v3

    iget v4, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentMask:I

    and-int v1, v3, v4

    sget-object v3, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    iget-object v4, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    sget v5, Ljava/util/concurrent/ConcurrentHashMap;->SSHIFT:I

    shl-int v5, v1, v5

    int-to-long v5, v5

    sget-wide v7, Ljava/util/concurrent/ConcurrentHashMap;->SBASE:J

    add-long/2addr v5, v7

    invoke-virtual {v3, v4, v5, v6}, Lsun/misc/Unsafe;->getObject(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    if-nez v2, :cond_1

    invoke-direct {p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->ensureSegment(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v2

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v2, p1, v0, p2, v3}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->put(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v3

    return-object v3
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    if-nez p2, :cond_0

    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/util/concurrent/ConcurrentHashMap;->hash(I)I

    move-result v0

    iget v3, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentShift:I

    ushr-int v3, v0, v3

    iget v4, p0, Ljava/util/concurrent/ConcurrentHashMap;->segmentMask:I

    and-int v1, v3, v4

    sget-object v3, Ljava/util/concurrent/ConcurrentHashMap;->UNSAFE:Lsun/misc/Unsafe;

    iget-object v4, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    sget v5, Ljava/util/concurrent/ConcurrentHashMap;->SSHIFT:I

    shl-int v5, v1, v5

    int-to-long v5, v5

    sget-wide v7, Ljava/util/concurrent/ConcurrentHashMap;->SBASE:J

    add-long/2addr v5, v7

    invoke-virtual {v3, v4, v5, v6}, Lsun/misc/Unsafe;->getObject(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ConcurrentHashMap$Segment;

    if-nez v2, :cond_1

    invoke-direct {p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->ensureSegment(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v2

    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v2, p1, v0, p2, v3}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->put(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v3

    return-object v3
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/util/concurrent/ConcurrentHashMap;->hash(I)I

    move-result v0

    invoke-direct {p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->segmentForHash(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v1, p1, v0, v2}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->remove(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/util/concurrent/ConcurrentHashMap;->hash(I)I

    move-result v0

    if-eqz p2, :cond_0

    invoke-direct {p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->segmentForHash(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1, v0, p2}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->remove(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/util/concurrent/ConcurrentHashMap;->hash(I)I

    move-result v0

    if-nez p2, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_0
    invoke-direct {p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->segmentForHash(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {v1, p1, v0, p2}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->replace(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/util/concurrent/ConcurrentHashMap;->hash(I)I

    move-result v0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_1
    invoke-direct {p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->segmentForHash(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1, p1, v0, p2, p3}, Ljava/util/concurrent/ConcurrentHashMap$Segment;->replace(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 15

    iget-object v6, p0, Ljava/util/concurrent/ConcurrentHashMap;->segments:[Ljava/util/concurrent/ConcurrentHashMap$Segment;

    array-length v5, v6

    const-wide/16 v1, 0x0

    const/4 v3, -0x1

    :goto_0
    const/4 v11, 0x2

    if-ge v3, v11, :cond_4

    const-wide/16 v9, 0x0

    const-wide/16 v7, 0x0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_1

    invoke-static {v6, v0}, Ljava/util/concurrent/ConcurrentHashMap;->segmentAt([Ljava/util/concurrent/ConcurrentHashMap$Segment;I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v4

    if-eqz v4, :cond_0

    iget v11, v4, Ljava/util/concurrent/ConcurrentHashMap$Segment;->modCount:I

    int-to-long v11, v11

    add-long/2addr v9, v11

    iget v11, v4, Ljava/util/concurrent/ConcurrentHashMap$Segment;->count:I

    int-to-long v11, v11

    add-long/2addr v7, v11

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    cmp-long v11, v9, v1

    if-nez v11, :cond_3

    const/16 v11, 0x1f

    ushr-long v11, v7, v11

    const-wide/16 v13, 0x0

    cmp-long v11, v11, v13

    if-nez v11, :cond_2

    long-to-int v11, v7

    :goto_2
    return v11

    :cond_2
    const v11, 0x7fffffff

    goto :goto_2

    :cond_3
    move-wide v1, v9

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    const-wide/16 v7, 0x0

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v5, :cond_5

    invoke-direct {p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->ensureSegment(I)Ljava/util/concurrent/ConcurrentHashMap$Segment;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    iget v11, v4, Ljava/util/concurrent/ConcurrentHashMap$Segment;->count:I

    int-to-long v11, v11

    add-long/2addr v7, v11

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v5, :cond_6

    aget-object v11, v6, v0

    invoke-virtual {v11}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    const/16 v11, 0x1f

    ushr-long v11, v7, v11

    const-wide/16 v13, 0x0

    cmp-long v11, v11, v13

    if-nez v11, :cond_7

    long-to-int v11, v7

    goto :goto_2

    :cond_7
    const v11, 0x7fffffff

    goto :goto_2
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    iget-object v0, p0, Ljava/util/concurrent/ConcurrentHashMap;->values:Ljava/util/Collection;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap$Values;

    invoke-direct {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap$Values;-><init>(Ljava/util/concurrent/ConcurrentHashMap;)V

    iput-object v0, p0, Ljava/util/concurrent/ConcurrentHashMap;->values:Ljava/util/Collection;

    goto :goto_0
.end method
