.class Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;
.super Ljava/lang/Object;
.source "ChooseActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/appwidget/worldclock/ChooseActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const-string v5, "MTKWORLDCHOOSE"

    const-string v6, "on item click .... "

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const/4 v4, 0x0

    const v5, 0x7f090006

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    iget-object v6, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v6, v4}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$100(Lcom/mediatek/appwidget/worldclock/ChooseActivity;Ljava/lang/String;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$002(Lcom/mediatek/appwidget/worldclock/ChooseActivity;Lcom/mediatek/appwidget/worldclock/ClockCityInfo;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v5}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$000(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    const v6, 0x7f070008

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v1}, Lcom/mediatek/appwidget/worldclock/ClockCityUtils;->initPreference(Landroid/content/Context;)V

    invoke-static {}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$200()I

    move-result v5

    iget-object v6, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v6}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$000(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/appwidget/worldclock/ClockCityUtils;->savePreferences(ILcom/mediatek/appwidget/worldclock/ClockCityInfo;)V

    invoke-static {}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$200()I

    move-result v5

    iget-object v6, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v6}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$000(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v6

    invoke-static {v1, v5, v6}, Lcom/mediatek/appwidget/worldclock/WorldClockWidgetProvider;->updateCity(Landroid/content/Context;ILcom/mediatek/appwidget/worldclock/ClockCityInfo;)V

    const-string v5, "MTKWORLDCHOOSE"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "position = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p3}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$302(I)I

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
