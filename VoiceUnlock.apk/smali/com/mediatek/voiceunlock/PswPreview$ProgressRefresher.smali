.class Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;
.super Ljava/lang/Object;
.source "PswPreview.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/voiceunlock/PswPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProgressRefresher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/voiceunlock/PswPreview;


# direct methods
.method constructor <init>(Lcom/mediatek/voiceunlock/PswPreview;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$100(Lcom/mediatek/voiceunlock/PswPreview;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$800(Lcom/mediatek/voiceunlock/PswPreview;)I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ProgressRefresher Position:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/voiceunlock/PswPreview;->access$900(Lcom/mediatek/voiceunlock/PswPreview;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$300(Lcom/mediatek/voiceunlock/PswPreview;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$800(Lcom/mediatek/voiceunlock/PswPreview;)I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$1000(Lcom/mediatek/voiceunlock/PswPreview;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_1
    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$1100(Lcom/mediatek/voiceunlock/PswPreview;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$1200(Lcom/mediatek/voiceunlock/PswPreview;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$1100(Lcom/mediatek/voiceunlock/PswPreview;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;

    iget-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-direct {v2, v3}, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;-><init>(Lcom/mediatek/voiceunlock/PswPreview;)V

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    return-void
.end method
