.class Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;
.super Lcom/mediatek/common/voicecommand/VoiceCommandListener;
.source "VoiceUnlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;


# direct methods
.method constructor <init>(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;->this$0:Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    invoke-direct {p0, p2}, Lcom/mediatek/common/voicecommand/VoiceCommandListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onVoiceCommandNotified(IILandroid/os/Bundle;)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    const/4 v6, 0x1

    const-string v3, "Result"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;->this$0:Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onNotified result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->access$000(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;Ljava/lang/String;)V

    if-ne v2, v6, :cond_1

    const/4 v3, 0x4

    if-ne p2, v3, :cond_0

    const-string v3, "Result_Info"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;->this$0:Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onNotified TRAINING_PSWDFILE path = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->access$000(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;->this$0:Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    invoke-static {v3}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->access$100(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;->this$0:Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    invoke-static {v4}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->access$100(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v3, 0xa

    if-ne v2, v3, :cond_0

    const-string v3, "Reslut_INfo1"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;->this$0:Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onNotified RESULT_ERROR errorMsg = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->access$000(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;->this$0:Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    invoke-static {v3}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->access$100(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;->this$0:Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    invoke-static {v4}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->access$100(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v6, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
