.class Lcom/android/musicvis/vis1/Visualization1$CubeEngine;
.super Landroid/service/wallpaper/WallpaperService$Engine;
.source "Visualization1.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/musicvis/vis1/Visualization1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CubeEngine"
.end annotation


# instance fields
.field private mAudioCapture:Lcom/android/musicvis/AudioCapture;

.field private mCenterX:F

.field private mCenterY:F

.field private final mDrawCube:Ljava/lang/Runnable;

.field private mOffset:F

.field private final mPaint:Landroid/graphics/Paint;

.field private mStartTime:J

.field private mTouchX:F

.field private mTouchY:F

.field private mVisible:Z

.field private mVizData:[I

.field private mWidth:I

.field final synthetic this$0:Lcom/android/musicvis/vis1/Visualization1;


# direct methods
.method constructor <init>(Lcom/android/musicvis/vis1/Visualization1;)V
    .locals 2

    const/high16 v1, -0x40800000

    iput-object p1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->this$0:Lcom/android/musicvis/vis1/Visualization1;

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mPaint:Landroid/graphics/Paint;

    iput v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mTouchX:F

    iput v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mTouchY:F

    const/16 v0, 0x400

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mVizData:[I

    new-instance v0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine$1;

    invoke-direct {v0, p0}, Lcom/android/musicvis/vis1/Visualization1$CubeEngine$1;-><init>(Lcom/android/musicvis/vis1/Visualization1$CubeEngine;)V

    iput-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mDrawCube:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method drawCube(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    const/high16 v1, -0x1000000

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    iget-object v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v1, v2, v2}, Lcom/android/musicvis/AudioCapture;->getFormattedData(II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mVizData:[I

    :goto_0
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mWidth:I

    if-ge v0, v1, :cond_1

    int-to-float v1, v0

    iget v2, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mCenterY:F

    iget-object v3, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mVizData:[I

    aget v3, v3, v0

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mVizData:[I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method drawFrame()V
    .locals 9

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {v3}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->drawCube(Landroid/graphics/Canvas;)V

    invoke-virtual {p0, v0}, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->drawTouchPoint(Landroid/graphics/Canvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v3, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_1
    iget-object v5, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->this$0:Lcom/android/musicvis/vis1/Visualization1;

    invoke-static {v5}, Lcom/android/musicvis/vis1/Visualization1;->access$000(Lcom/android/musicvis/vis1/Visualization1;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mDrawCube:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v5, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mVisible:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->this$0:Lcom/android/musicvis/vis1/Visualization1;

    invoke-static {v5}, Lcom/android/musicvis/vis1/Visualization1;->access$000(Lcom/android/musicvis/vis1/Visualization1;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mDrawCube:Ljava/lang/Runnable;

    const-wide/16 v7, 0x28

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    return-void

    :catchall_0
    move-exception v5

    if-eqz v0, :cond_3

    invoke-interface {v3, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_3
    throw v5
.end method

.method drawTouchPoint(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mTouchX:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mTouchY:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mTouchX:F

    iget v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mTouchY:F

    const/high16 v2, 0x42480000

    iget-object v3, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/high16 v1, 0x40000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mStartTime:J

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->this$0:Lcom/android/musicvis/vis1/Visualization1;

    invoke-static {v0}, Lcom/android/musicvis/vis1/Visualization1;->access$000(Lcom/android/musicvis/vis1/Visualization1;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mDrawCube:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->stop()V

    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    :cond_0
    return-void
.end method

.method public onOffsetsChanged(FFFFII)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # I

    iput p1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mOffset:F

    invoke-virtual {p0}, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->drawFrame()V

    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/high16 v1, 0x40000000

    invoke-super {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    iput p3, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mWidth:I

    int-to-float v0, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mCenterX:F

    int-to-float v0, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mCenterY:F

    invoke-virtual {p0}, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->drawFrame()V

    return-void
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mVisible:Z

    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->this$0:Lcom/android/musicvis/vis1/Visualization1;

    invoke-static {v0}, Lcom/android/musicvis/vis1/Visualization1;->access$000(Lcom/android/musicvis/vis1/Visualization1;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mDrawCube:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/high16 v2, -0x40800000

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mTouchX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mTouchY:F

    :goto_0
    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onTouchEvent(Landroid/view/MotionEvent;)V

    return-void

    :cond_0
    iput v2, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mTouchX:F

    iput v2, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mTouchY:F

    goto :goto_0
.end method

.method public onVisibilityChanged(Z)V
    .locals 3
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mVisible:Z

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/musicvis/AudioCapture;

    const/4 v1, 0x0

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2}, Lcom/android/musicvis/AudioCapture;-><init>(II)V

    iput-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    :cond_0
    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->start()V

    invoke-virtual {p0}, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->drawFrame()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->this$0:Lcom/android/musicvis/vis1/Visualization1;

    invoke-static {v0}, Lcom/android/musicvis/vis1/Visualization1;->access$000(Lcom/android/musicvis/vis1/Visualization1;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mDrawCube:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->stop()V

    iget-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/musicvis/vis1/Visualization1$CubeEngine;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    goto :goto_0
.end method
