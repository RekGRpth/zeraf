.class Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;
.super Landroid/service/wallpaper/WallpaperService$Engine;
.source "RenderScriptWallpaper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/musicvis/RenderScriptWallpaper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RenderScriptEngine"
.end annotation


# instance fields
.field private mRenderer:Lcom/android/musicvis/RenderScriptScene;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mRs:Landroid/renderscript/RenderScriptGL;

.field final synthetic this$0:Lcom/android/musicvis/RenderScriptWallpaper;


# direct methods
.method private constructor <init>(Lcom/android/musicvis/RenderScriptWallpaper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->this$0:Lcom/android/musicvis/RenderScriptWallpaper;

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/musicvis/RenderScriptWallpaper;Lcom/android/musicvis/RenderScriptWallpaper$1;)V
    .locals 0
    .param p1    # Lcom/android/musicvis/RenderScriptWallpaper;
    .param p2    # Lcom/android/musicvis/RenderScriptWallpaper$1;

    invoke-direct {p0, p1}, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;-><init>(Lcom/android/musicvis/RenderScriptWallpaper;)V

    return-void
.end method

.method private destroyRenderer()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    invoke-virtual {v0}, Lcom/android/musicvis/RenderScriptScene;->stop()V

    iput-object v1, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    :cond_0
    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0, v1, v2, v2}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->destroy()V

    iput-object v1, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    :cond_1
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->setTouchEventsEnabled(Z)V

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    invoke-direct {p0}, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->destroyRenderer()V

    return-void
.end method

.method public onOffsetsChanged(FFFFII)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # I

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    invoke-virtual {v0, p1, p2, p5, p6}, Lcom/android/musicvis/RenderScriptScene;->setOffset(FFII)V

    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 4
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0, p1, p3, p4}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    :cond_0
    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->this$0:Lcom/android/musicvis/RenderScriptWallpaper;

    invoke-virtual {v0, p3, p4}, Lcom/android/musicvis/RenderScriptWallpaper;->createScene(II)Lcom/android/musicvis/RenderScriptScene;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    iget-object v1, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->this$0:Lcom/android/musicvis/RenderScriptWallpaper;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->isPreview()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/musicvis/RenderScriptScene;->init(Landroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;Z)V

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    invoke-virtual {v0}, Lcom/android/musicvis/RenderScriptScene;->start()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    invoke-virtual {v0, p3, p4}, Lcom/android/musicvis/RenderScriptScene;->resize(II)V

    goto :goto_0
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    new-instance v0, Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    invoke-direct {v0}, Landroid/renderscript/RenderScriptGL$SurfaceConfig;-><init>()V

    new-instance v1, Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->this$0:Lcom/android/musicvis/RenderScriptWallpaper;

    invoke-direct {v1, v2, v0}, Landroid/renderscript/RenderScriptGL;-><init>(Landroid/content/Context;Landroid/renderscript/RenderScriptGL$SurfaceConfig;)V

    iput-object v1, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    iget-object v1, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRs:Landroid/renderscript/RenderScriptGL;

    sget-object v2, Landroid/renderscript/RenderScript$Priority;->LOW:Landroid/renderscript/RenderScript$Priority;

    invoke-virtual {v1, v2}, Landroid/renderscript/RenderScript;->setPriority(Landroid/renderscript/RenderScript$Priority;)V

    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    invoke-direct {p0}, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->destroyRenderer()V

    return-void
.end method

.method public onVisibilityChanged(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    invoke-virtual {v0}, Lcom/android/musicvis/RenderScriptScene;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/musicvis/RenderScriptWallpaper$RenderScriptEngine;->mRenderer:Lcom/android/musicvis/RenderScriptScene;

    invoke-virtual {v0}, Lcom/android/musicvis/RenderScriptScene;->stop()V

    goto :goto_0
.end method
