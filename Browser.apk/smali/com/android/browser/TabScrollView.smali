.class public Lcom/android/browser/TabScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "TabScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/TabScrollView$TabLayout;
    }
.end annotation


# instance fields
.field private mAnimationDuration:I

.field private mContentView:Landroid/widget/LinearLayout;

.field private mSelected:I

.field private mTabOverlap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/android/browser/TabScrollView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/android/browser/TabScrollView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/android/browser/TabScrollView;->init(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/TabScrollView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0    # Lcom/android/browser/TabScrollView;

    iget-object v0, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/browser/TabScrollView;)I
    .locals 1
    .param p0    # Lcom/android/browser/TabScrollView;

    iget v0, p0, Lcom/android/browser/TabScrollView;->mTabOverlap:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/browser/TabScrollView;)I
    .locals 1
    .param p0    # Lcom/android/browser/TabScrollView;

    iget v0, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    return v0
.end method

.method private animateScroll(I)V
    .locals 5
    .param p1    # I

    const-string v1, "scroll"

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x1

    aput p1, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/TabScrollView;->mAnimationDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method private ensureChildVisible(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int v1, v0, v4

    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int v3, v2, v4

    if-ge v0, v2, :cond_1

    invoke-direct {p0, v0}, Lcom/android/browser/TabScrollView;->animateScroll(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-le v1, v3, :cond_0

    sub-int v4, v1, v3

    add-int/2addr v4, v2

    invoke-direct {p0, v4}, Lcom/android/browser/TabScrollView;->animateScroll(I)V

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, -0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/browser/TabScrollView;->mAnimationDuration:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/browser/TabScrollView;->mTabOverlap:I

    invoke-virtual {p0, v3}, Landroid/view/View;->setHorizontalScrollBarEnabled(Z)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/widget/HorizontalScrollView;->setOverScrollMode(I)V

    new-instance v0, Lcom/android/browser/TabScrollView$TabLayout;

    invoke-direct {v0, p0, p1}, Lcom/android/browser/TabScrollView$TabLayout;-><init>(Lcom/android/browser/TabScrollView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b001e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    iget-object v0, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;)V

    iput v4, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    invoke-virtual {p0}, Lcom/android/browser/TabScrollView;->getScroll()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/browser/TabScrollView;->setScroll(I)V

    return-void
.end method


# virtual methods
.method addTab(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setActivated(Z)V

    return-void
.end method

.method clearTabs()V
    .locals 1

    iget-object v0, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method getChildIndex(Landroid/view/View;)I
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public getScroll()I
    .locals 1

    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v0

    return v0
.end method

.method getSelectedTab()Landroid/view/View;
    .locals 2

    iget v0, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    iget-object v1, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/HorizontalScrollView;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/android/browser/TabScrollView;->getSelectedTab()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/TabScrollView;->ensureChildVisible(Landroid/view/View;)V

    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onScrollChanged(IIII)V

    invoke-virtual {p0}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method removeTab(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    if-ne v0, v1, :cond_1

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void

    :cond_1
    iget v1, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    goto :goto_0
.end method

.method public setScroll(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    return-void
.end method

.method setSelectedTab(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/browser/TabScrollView;->getSelectedTab()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    :cond_0
    iput p1, p0, Lcom/android/browser/TabScrollView;->mSelected:I

    invoke-virtual {p0}, Lcom/android/browser/TabScrollView;->getSelectedTab()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    :cond_1
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->requestLayout()V

    return-void
.end method

.method protected updateLayout()V
    .locals 4

    iget-object v3, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/android/browser/TabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/browser/TabBar$TabView;

    invoke-virtual {v2}, Lcom/android/browser/TabBar$TabView;->updateLayoutParams()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/browser/TabScrollView;->getSelectedTab()Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/browser/TabScrollView;->ensureChildVisible(Landroid/view/View;)V

    return-void
.end method
