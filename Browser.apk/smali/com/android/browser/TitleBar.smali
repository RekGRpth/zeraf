.class public Lcom/android/browser/TitleBar;
.super Landroid/widget/RelativeLayout;
.source "TitleBar.java"


# static fields
.field private static final ANIM_TITLEBAR_DECELERATE:F = 2.5f

.field private static final PROGRESS_MAX:I = 0x64


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAutoLogin:Lcom/android/browser/AutologinBar;

.field private mBaseUi:Lcom/android/browser/BaseUi;

.field private mContentView:Landroid/widget/FrameLayout;

.field private mHideTileBarAnimatorListener:Landroid/animation/Animator$AnimatorListener;

.field private mInLoad:Z

.field private mIsFixedTitleBar:Z

.field private mNavBar:Lcom/android/browser/NavigationBarBase;

.field private mProgress:Lcom/android/browser/PageProgressView;

.field private mShowing:Z

.field private mSkipTitleBarAnimations:Z

.field private mSlop:I

.field private mSnapshotBar:Lcom/android/browser/SnapshotBar;

.field private mTitleBarAnimator:Landroid/animation/Animator;

.field private mUiController:Lcom/android/browser/UiController;

.field private mUseQuickControls:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/browser/UiController;Lcom/android/browser/BaseUi;Landroid/widget/FrameLayout;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/browser/UiController;
    .param p3    # Lcom/android/browser/BaseUi;
    .param p4    # Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Lcom/android/browser/TitleBar$1;

    invoke-direct {v1, p0}, Lcom/android/browser/TitleBar$1;-><init>(Lcom/android/browser/TitleBar;)V

    iput-object v1, p0, Lcom/android/browser/TitleBar;->mHideTileBarAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    iput-object p2, p0, Lcom/android/browser/TitleBar;->mUiController:Lcom/android/browser/UiController;

    iput-object p3, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    iput-object p4, p0, Lcom/android/browser/TitleBar;->mContentView:Landroid/widget/FrameLayout;

    const-string v1, "accessibility"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    iput-object v1, p0, Lcom/android/browser/TitleBar;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v1}, Lcom/android/browser/UiController;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/android/browser/TitleBar;->mSlop:I

    invoke-direct {p0, p1}, Lcom/android/browser/TitleBar;->initLayout(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/browser/TitleBar;->setFixedTitleBar()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/TitleBar;)Lcom/android/browser/AutologinBar;
    .locals 1
    .param p0    # Lcom/android/browser/TitleBar;

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/browser/TitleBar;)Lcom/android/browser/BaseUi;
    .locals 1
    .param p0    # Lcom/android/browser/TitleBar;

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    return-object v0
.end method

.method private calculateEmbeddedHeight()I
    .locals 2

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method private inAutoLogin()Z
    .locals 1

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private inflateAutoLoginBar()V
    .locals 2

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f0d0081

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/browser/AutologinBar;

    iput-object v1, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    invoke-virtual {v1, p0}, Lcom/android/browser/AutologinBar;->setTitleBar(Lcom/android/browser/TitleBar;)V

    goto :goto_0
.end method

.method private inflateSnapshotBar()V
    .locals 2

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f0d0080

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/browser/SnapshotBar;

    iput-object v1, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    invoke-virtual {v1, p0}, Lcom/android/browser/SnapshotBar;->setTitleBar(Lcom/android/browser/TitleBar;)V

    goto :goto_0
.end method

.method private initLayout(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040036

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v1, 0x7f0d0082

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/browser/PageProgressView;

    iput-object v1, p0, Lcom/android/browser/TitleBar;->mProgress:Lcom/android/browser/PageProgressView;

    const v1, 0x7f0d007f

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/browser/NavigationBarBase;

    iput-object v1, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    invoke-virtual {v1, p0}, Lcom/android/browser/NavigationBarBase;->setTitleBar(Lcom/android/browser/TitleBar;)V

    return-void
.end method

.method private makeLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method private setFixedTitleBar()V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/android/browser/TitleBar;->mUseQuickControls:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090001

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    :goto_0
    iget-object v4, p0, Lcom/android/browser/TitleBar;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v4

    or-int/2addr v0, v4

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-boolean v4, p0, Lcom/android/browser/TitleBar;->mIsFixedTitleBar:Z

    if-ne v4, v0, :cond_1

    if-eqz v1, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    iput-boolean v0, p0, Lcom/android/browser/TitleBar;->mIsFixedTitleBar:Z

    invoke-virtual {p0, v2}, Lcom/android/browser/TitleBar;->setSkipTitleBarAnimations(Z)V

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->show()V

    invoke-virtual {p0, v3}, Lcom/android/browser/TitleBar;->setSkipTitleBarAnimations(Z)V

    if-eqz v1, :cond_2

    invoke-virtual {v1, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_2
    iget-boolean v2, p0, Lcom/android/browser/TitleBar;->mIsFixedTitleBar:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v2, p0}, Lcom/android/browser/BaseUi;->addFixedTitleBar(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/android/browser/TitleBar;->mContentView:Landroid/widget/FrameLayout;

    invoke-direct {p0}, Lcom/android/browser/TitleBar;->makeLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {v2, p0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v2, v3}, Lcom/android/browser/BaseUi;->setContentViewMarginTop(I)V

    goto :goto_1
.end method


# virtual methods
.method cancelTitleBarAnimation(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mTitleBarAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mTitleBarAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/TitleBar;->mTitleBarAnimator:Landroid/animation/Animator;

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationY(F)V

    :cond_1
    return-void
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->getCurrentWebView()Landroid/webkit/WebView;

    move-result-object v0

    const/16 v1, 0x82

    if-ne v1, p2, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->hasFocusable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentWebView()Landroid/webkit/WebView;
    .locals 2

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v1}, Lcom/android/browser/BaseUi;->getActiveTab()Lcom/android/browser/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/browser/Tab;->getWebView()Landroid/webkit/WebView;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getEmbeddedHeight()I
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/TitleBar;->mUseQuickControls:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/browser/TitleBar;->mIsFixedTitleBar:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/android/browser/TitleBar;->calculateEmbeddedHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getNavigationBar()Lcom/android/browser/NavigationBarBase;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    return-object v0
.end method

.method public getProgressView()Lcom/android/browser/PageProgressView;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mProgress:Lcom/android/browser/PageProgressView;

    return-object v0
.end method

.method public getUi()Lcom/android/browser/BaseUi;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    return-object v0
.end method

.method public getUiController()Lcom/android/browser/UiController;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mUiController:Lcom/android/browser/UiController;

    return-object v0
.end method

.method public getVisibleTitleHeight()I
    .locals 3

    iget-object v2, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v2}, Lcom/android/browser/BaseUi;->getActiveTab()Lcom/android/browser/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/browser/Tab;->getWebView()Landroid/webkit/WebView;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getVisibleTitleHeight()I

    move-result v2

    :goto_1
    return v2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method hide()V
    .locals 6

    const/4 v5, 0x0

    iget-boolean v1, p0, Lcom/android/browser/TitleBar;->mUseQuickControls:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iput-boolean v5, p0, Lcom/android/browser/TitleBar;->mShowing:Z

    :cond_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/android/browser/TitleBar;->mIsFixedTitleBar:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/browser/TitleBar;->mSkipTitleBarAnimations:Z

    if-nez v1, :cond_2

    invoke-virtual {p0, v5}, Lcom/android/browser/TitleBar;->cancelTitleBarAnimation(Z)V

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->getVisibleTitleHeight()I

    move-result v0

    const-string v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v3

    aput v3, v2, v5

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->getEmbeddedHeight()I

    move-result v4

    neg-int v4, v4

    add-int/2addr v4, v0

    int-to-float v4, v4

    aput v4, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/android/browser/TitleBar;->mTitleBarAnimator:Landroid/animation/Animator;

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mTitleBarAnimator:Landroid/animation/Animator;

    iget-object v2, p0, Lcom/android/browser/TitleBar;->mHideTileBarAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mTitleBarAnimator:Landroid/animation/Animator;

    invoke-virtual {p0, v1}, Lcom/android/browser/TitleBar;->setupTitleBarAnimator(Landroid/animation/Animator;)V

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mTitleBarAnimator:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->onScrollChanged()V

    goto :goto_0
.end method

.method public hideAutoLogin(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    iget-boolean v1, p0, Lcom/android/browser/TitleBar;->mUseQuickControls:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v1}, Lcom/android/browser/BaseUi;->hideTitleBar()V

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v1}, Lcom/android/browser/BaseUi;->refreshWebView()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f050001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/android/browser/TitleBar$2;

    invoke-direct {v1, p0}, Lcom/android/browser/TitleBar$2;-><init>(Lcom/android/browser/TitleBar;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    invoke-virtual {v1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v1}, Lcom/android/browser/BaseUi;->refreshWebView()V

    goto :goto_0
.end method

.method public isEditingUrl()Z
    .locals 1

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    invoke-virtual {v0}, Lcom/android/browser/NavigationBarBase;->isEditingUrl()Z

    move-result v0

    return v0
.end method

.method public isInLoad()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/TitleBar;->mInLoad:Z

    return v0
.end method

.method isShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/TitleBar;->mShowing:Z

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/view/View;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/android/browser/TitleBar;->setFixedTitleBar()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    iget-boolean v1, p0, Lcom/android/browser/TitleBar;->mIsFixedTitleBar:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-direct {p0}, Lcom/android/browser/TitleBar;->calculateEmbeddedHeight()I

    move-result v2

    sub-int v0, v1, v2

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    neg-int v2, v0

    invoke-virtual {v1, v2}, Lcom/android/browser/BaseUi;->setContentViewMarginTop(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/browser/BaseUi;->setContentViewMarginTop(I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    invoke-direct {p0}, Lcom/android/browser/TitleBar;->setFixedTitleBar()V

    return-void
.end method

.method public onScrollChanged()V
    .locals 4

    iget-boolean v1, p0, Lcom/android/browser/TitleBar;->mShowing:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/browser/TitleBar;->mIsFixedTitleBar:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->getVisibleTitleHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->getEmbeddedHeight()I

    move-result v2

    sub-int v0, v1, v2

    int-to-float v1, v0

    invoke-virtual {p0, v1}, Landroid/view/View;->setTranslationY(F)V

    iget v1, p0, Lcom/android/browser/TitleBar;->mSlop:I

    neg-int v1, v1

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->show()V

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v2, v3}, Lcom/android/browser/BaseUi;->showBottomBarForDuration(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/android/browser/TitleBar;->mSlop:I

    neg-int v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v1}, Lcom/android/browser/BaseUi;->hideBottomBar()V

    goto :goto_0
.end method

.method public onTabDataChanged(Lcom/android/browser/Tab;)V
    .locals 3
    .param p1    # Lcom/android/browser/Tab;

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    invoke-virtual {v0, p1}, Lcom/android/browser/SnapshotBar;->onTabDataChanged(Lcom/android/browser/Tab;)V

    :cond_0
    invoke-virtual {p1}, Lcom/android/browser/Tab;->isSnapshot()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/browser/TitleBar;->inflateSnapshotBar()V

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setProgress(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v0, 0x64

    if-lt p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mProgress:Lcom/android/browser/PageProgressView;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lcom/android/browser/PageProgressView;->setProgress(I)V

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mProgress:Lcom/android/browser/PageProgressView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/android/browser/TitleBar;->mInLoad:Z

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    invoke-virtual {v0}, Lcom/android/browser/NavigationBarBase;->onProgressStopped()V

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->isEditingUrl()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->wantsToBeVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/browser/TitleBar;->mUseQuickControls:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->hide()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v0}, Lcom/android/browser/BaseUi;->showTitleBarForDuration()V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/android/browser/TitleBar;->mInLoad:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mProgress:Lcom/android/browser/PageProgressView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iput-boolean v3, p0, Lcom/android/browser/TitleBar;->mInLoad:Z

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    invoke-virtual {v0}, Lcom/android/browser/NavigationBarBase;->onProgressStarted()V

    :cond_3
    iget-object v0, p0, Lcom/android/browser/TitleBar;->mProgress:Lcom/android/browser/PageProgressView;

    mul-int/lit16 v1, p1, 0x2710

    div-int/lit8 v1, v1, 0x64

    invoke-virtual {v0, v1}, Lcom/android/browser/PageProgressView;->setProgress(I)V

    iget-boolean v0, p0, Lcom/android/browser/TitleBar;->mUseQuickControls:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->isEditingUrl()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0, v3}, Lcom/android/browser/TitleBar;->setShowProgressOnly(Z)V

    :cond_4
    iget-boolean v0, p0, Lcom/android/browser/TitleBar;->mShowing:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->show()V

    goto :goto_0
.end method

.method setShowProgressOnly(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->wantsToBeVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/browser/TitleBar;->mNavBar:Lcom/android/browser/NavigationBarBase;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method setSkipTitleBarAnimations(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/browser/TitleBar;->mSkipTitleBarAnimations:Z

    return-void
.end method

.method public setUseQuickControls(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/browser/TitleBar;->mUseQuickControls:Z

    invoke-direct {p0}, Lcom/android/browser/TitleBar;->setFixedTitleBar()V

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method setupTitleBarAnimator(Landroid/animation/Animator;)V
    .locals 4
    .param p1    # Landroid/animation/Animator;

    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40200000

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {p1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    int-to-long v2, v0

    invoke-virtual {p1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    return-void
.end method

.method show()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, v5}, Lcom/android/browser/TitleBar;->cancelTitleBarAnimation(Z)V

    iget-boolean v2, p0, Lcom/android/browser/TitleBar;->mUseQuickControls:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/browser/TitleBar;->mSkipTitleBarAnimations:Z

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {p0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Landroid/view/View;->setTranslationY(F)V

    :goto_0
    iput-boolean v6, p0, Lcom/android/browser/TitleBar;->mShowing:Z

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->getVisibleTitleHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/browser/TitleBar;->getEmbeddedHeight()I

    move-result v2

    neg-int v2, v2

    add-int/2addr v2, v1

    int-to-float v0, v2

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v2

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :cond_2
    const-string v2, "translationY"

    const/4 v3, 0x2

    new-array v3, v3, [F

    aput v0, v3, v5

    aput v4, v3, v6

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/TitleBar;->mTitleBarAnimator:Landroid/animation/Animator;

    iget-object v2, p0, Lcom/android/browser/TitleBar;->mTitleBarAnimator:Landroid/animation/Animator;

    invoke-virtual {p0, v2}, Lcom/android/browser/TitleBar;->setupTitleBarAnimator(Landroid/animation/Animator;)V

    iget-object v2, p0, Lcom/android/browser/TitleBar;->mTitleBarAnimator:Landroid/animation/Animator;

    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method public showAutoLogin(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/browser/TitleBar;->mUseQuickControls:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mBaseUi:Lcom/android/browser/BaseUi;

    invoke-virtual {v0}, Lcom/android/browser/BaseUi;->showTitleBar()V

    :cond_0
    iget-object v0, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/browser/TitleBar;->inflateAutoLoginBar()V

    :cond_1
    iget-object v0, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x7f050000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_2
    return-void
.end method

.method public updateAutoLogin(Lcom/android/browser/Tab;Z)V
    .locals 1
    .param p1    # Lcom/android/browser/Tab;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/android/browser/Tab;->getDeviceAccountLogin()Lcom/android/browser/DeviceAccountLogin;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/browser/TitleBar;->inflateAutoLoginBar()V

    :cond_1
    iget-object v0, p0, Lcom/android/browser/TitleBar;->mAutoLogin:Lcom/android/browser/AutologinBar;

    invoke-virtual {v0, p1, p2}, Lcom/android/browser/AutologinBar;->updateAutoLogin(Lcom/android/browser/Tab;Z)V

    goto :goto_0
.end method

.method public useQuickControls()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/TitleBar;->mUseQuickControls:Z

    return v0
.end method

.method public wantsToBeVisible()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/browser/TitleBar;->inAutoLogin()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/browser/TitleBar;->mSnapshotBar:Lcom/android/browser/SnapshotBar;

    invoke-virtual {v0}, Lcom/android/browser/SnapshotBar;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
