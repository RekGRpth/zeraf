.class public Lcom/android/browser/BottomBar;
.super Landroid/widget/LinearLayout;
.source "BottomBar.java"


# static fields
.field private static final ANIM_TITLEBAR_DECELERATE:F = 2.5f

.field private static final TAG:Ljava/lang/String; = "BottomBar"


# instance fields
.field private mBaseUi:Lcom/android/browser/BaseUi;

.field protected mBottomBar:Landroid/widget/LinearLayout;

.field private mBottomBarAnimator:Landroid/animation/Animator;

.field protected mBottomBarBack:Landroid/widget/ImageView;

.field protected mBottomBarBookmarks:Landroid/widget/ImageView;

.field protected mBottomBarForward:Landroid/widget/ImageView;

.field protected mBottomBarTabCount:Landroid/widget/TextView;

.field protected mBottomBarTabs:Landroid/widget/ImageView;

.field private mContentView:Landroid/widget/FrameLayout;

.field private mContentViewHeight:I

.field private mContext:Landroid/content/Context;

.field private mHideBottomBarAnimatorListener:Landroid/animation/Animator$AnimatorListener;

.field private mShowing:Z

.field private mTabControl:Lcom/android/browser/TabControl;

.field private mUiController:Lcom/android/browser/UiController;

.field private mUseFullScreen:Z

.field private mUseQuickControls:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/browser/UiController;Lcom/android/browser/BaseUi;Lcom/android/browser/TabControl;Landroid/widget/FrameLayout;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/browser/UiController;
    .param p3    # Lcom/android/browser/BaseUi;
    .param p4    # Lcom/android/browser/TabControl;
    .param p5    # Landroid/widget/FrameLayout;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/browser/BottomBar$10;

    invoke-direct {v0, p0}, Lcom/android/browser/BottomBar$10;-><init>(Lcom/android/browser/BottomBar;)V

    iput-object v0, p0, Lcom/android/browser/BottomBar;->mHideBottomBarAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    iput-object p1, p0, Lcom/android/browser/BottomBar;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/browser/BottomBar;->mUiController:Lcom/android/browser/UiController;

    iput-object p3, p0, Lcom/android/browser/BottomBar;->mBaseUi:Lcom/android/browser/BaseUi;

    iput-object p4, p0, Lcom/android/browser/BottomBar;->mTabControl:Lcom/android/browser/TabControl;

    iput-object p5, p0, Lcom/android/browser/BottomBar;->mContentView:Landroid/widget/FrameLayout;

    invoke-direct {p0, p1}, Lcom/android/browser/BottomBar;->initLayout(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/browser/BottomBar;->setupBottomBar()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/BottomBar;)Lcom/android/browser/UiController;
    .locals 1
    .param p0    # Lcom/android/browser/BottomBar;

    iget-object v0, p0, Lcom/android/browser/BottomBar;->mUiController:Lcom/android/browser/UiController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/browser/BottomBar;)Lcom/android/browser/BaseUi;
    .locals 1
    .param p0    # Lcom/android/browser/BottomBar;

    iget-object v0, p0, Lcom/android/browser/BottomBar;->mBaseUi:Lcom/android/browser/BaseUi;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/browser/BottomBar;)Lcom/android/browser/TabControl;
    .locals 1
    .param p0    # Lcom/android/browser/BottomBar;

    iget-object v0, p0, Lcom/android/browser/BottomBar;->mTabControl:Lcom/android/browser/TabControl;

    return-object v0
.end method

.method private getVisibleBottomHeight()I
    .locals 1

    iget-object v0, p0, Lcom/android/browser/BottomBar;->mBottomBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    return v0
.end method

.method private initLayout(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040011

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v1, 0x7f0d0028

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBar:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0029

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarBack:Landroid/widget/ImageView;

    const v1, 0x7f0d002a

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarForward:Landroid/widget/ImageView;

    const v1, 0x7f0d002b

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarTabs:Landroid/widget/ImageView;

    const v1, 0x7f0d002d

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarBookmarks:Landroid/widget/ImageView;

    const v1, 0x7f0d002c

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarTabCount:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarBack:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/browser/BottomBar$1;

    invoke-direct {v2, p0}, Lcom/android/browser/BottomBar$1;-><init>(Lcom/android/browser/BottomBar;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarBack:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/browser/BottomBar$2;

    invoke-direct {v2, p0}, Lcom/android/browser/BottomBar$2;-><init>(Lcom/android/browser/BottomBar;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarForward:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/browser/BottomBar$3;

    invoke-direct {v2, p0}, Lcom/android/browser/BottomBar$3;-><init>(Lcom/android/browser/BottomBar;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarForward:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/browser/BottomBar$4;

    invoke-direct {v2, p0}, Lcom/android/browser/BottomBar$4;-><init>(Lcom/android/browser/BottomBar;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarTabs:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/browser/BottomBar$5;

    invoke-direct {v2, p0}, Lcom/android/browser/BottomBar$5;-><init>(Lcom/android/browser/BottomBar;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarTabs:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/browser/BottomBar$6;

    invoke-direct {v2, p0}, Lcom/android/browser/BottomBar$6;-><init>(Lcom/android/browser/BottomBar;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarBookmarks:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/browser/BottomBar$7;

    invoke-direct {v2, p0}, Lcom/android/browser/BottomBar$7;-><init>(Lcom/android/browser/BottomBar;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarBookmarks:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/browser/BottomBar$8;

    invoke-direct {v2, p0}, Lcom/android/browser/BottomBar$8;-><init>(Lcom/android/browser/BottomBar;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mBottomBarTabCount:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/browser/BottomBar;->mUiController:Lcom/android/browser/UiController;

    invoke-interface {v2}, Lcom/android/browser/UiController;->getTabControl()Lcom/android/browser/TabControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/browser/TabControl;->getTabCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mTabControl:Lcom/android/browser/TabControl;

    new-instance v2, Lcom/android/browser/BottomBar$9;

    invoke-direct {v2, p0}, Lcom/android/browser/BottomBar$9;-><init>(Lcom/android/browser/BottomBar;)V

    invoke-virtual {v1, v2}, Lcom/android/browser/TabControl;->setOnTabCountChangedListener(Lcom/android/browser/TabControl$OnTabCountChangedListener;)V

    return-void
.end method

.method private makeLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 3

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x50

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    return-object v0
.end method

.method private setupBottomBar()V
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/android/browser/BottomBar;->show()V

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v1, p0, Lcom/android/browser/BottomBar;->mContentView:Landroid/widget/FrameLayout;

    invoke-direct {p0}, Lcom/android/browser/BottomBar;->makeLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/browser/BottomBar;->mBaseUi:Lcom/android/browser/BaseUi;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/browser/BaseUi;->setContentViewMarginBottom(I)V

    return-void
.end method


# virtual methods
.method cancelBottomBarAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    :cond_0
    return-void
.end method

.method public changeBottomBarState(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z

    iget-object v0, p0, Lcom/android/browser/BottomBar;->mBottomBarBack:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/browser/BottomBar;->mBottomBarForward:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method hide()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v2, "BottomBar"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bottom bar hide(), showing: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/browser/BottomBar;->mShowing:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/android/browser/BottomBar;->mUseQuickControls:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/browser/BottomBar;->mUseFullScreen:Z

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/browser/BottomBar;->cancelBottomBarAnimation()V

    invoke-direct {p0}, Lcom/android/browser/BottomBar;->getVisibleBottomHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    const-string v2, "translationY"

    new-array v3, v7, [F

    aput v0, v3, v5

    int-to-float v4, v1

    add-float/2addr v4, v0

    aput v4, v3, v6

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    iget-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    iget-object v3, p0, Lcom/android/browser/BottomBar;->mHideBottomBarAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    invoke-virtual {p0, v2}, Lcom/android/browser/BottomBar;->setupBottomBarAnimator(Landroid/animation/Animator;)V

    iget-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v5, p0, Lcom/android/browser/BottomBar;->mShowing:Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/browser/BottomBar;->cancelBottomBarAnimation()V

    invoke-direct {p0}, Lcom/android/browser/BottomBar;->getVisibleBottomHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    const-string v2, "BottomBar"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hide(): visibleHeight: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "BottomBar"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hide(): startPos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "translationY"

    new-array v3, v7, [F

    aput v0, v3, v5

    int-to-float v4, v1

    add-float/2addr v4, v0

    aput v4, v3, v6

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    iget-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    iget-object v3, p0, Lcom/android/browser/BottomBar;->mHideBottomBarAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    invoke-virtual {p0, v2}, Lcom/android/browser/BottomBar;->setupBottomBarAnimator(Landroid/animation/Animator;)V

    iget-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    iput-boolean v5, p0, Lcom/android/browser/BottomBar;->mShowing:Z

    goto :goto_0
.end method

.method isShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/BottomBar;->mShowing:Z

    return v0
.end method

.method public onScrollChanged()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/BottomBar;->mShowing:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/browser/BottomBar;->getVisibleBottomHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationY(F)V

    :cond_0
    return-void
.end method

.method public setFullScreen(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/browser/BottomBar;->mUseFullScreen:Z

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setUseQuickControls(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/browser/BottomBar;->mUseQuickControls:Z

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method setupBottomBarAnimator(Landroid/animation/Animator;)V
    .locals 4
    .param p1    # Landroid/animation/Animator;

    iget-object v2, p0, Lcom/android/browser/BottomBar;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40200000

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {p1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    int-to-long v2, v0

    invoke-virtual {p1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    return-void
.end method

.method show()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/browser/BottomBar;->cancelBottomBarAnimation()V

    iget-boolean v2, p0, Lcom/android/browser/BottomBar;->mUseQuickControls:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/browser/BottomBar;->mUseFullScreen:Z

    if-eqz v2, :cond_2

    :cond_0
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v5, p0, Lcom/android/browser/BottomBar;->mShowing:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v2, p0, Lcom/android/browser/BottomBar;->mShowing:Z

    if-nez v2, :cond_1

    invoke-virtual {p0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/browser/BottomBar;->getVisibleBottomHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    const-string v2, "BottomBar"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bottombar show(): visibleHeight: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " show(): startPos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "translationY"

    const/4 v3, 0x2

    new-array v3, v3, [F

    aput v0, v3, v5

    int-to-float v4, v1

    sub-float v4, v0, v4

    aput v4, v3, v6

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    iget-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    invoke-virtual {p0, v2}, Lcom/android/browser/BottomBar;->setupBottomBarAnimator(Landroid/animation/Animator;)V

    iget-object v2, p0, Lcom/android/browser/BottomBar;->mBottomBarAnimator:Landroid/animation/Animator;

    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    iput-boolean v6, p0, Lcom/android/browser/BottomBar;->mShowing:Z

    goto :goto_0
.end method

.method public useQuickControls()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/BottomBar;->mUseQuickControls:Z

    return v0
.end method
