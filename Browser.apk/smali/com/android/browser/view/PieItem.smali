.class public Lcom/android/browser/view/PieItem;
.super Ljava/lang/Object;
.source "PieItem.java"


# instance fields
.field private animate:F

.field private inner:I

.field private level:I

.field private mEnabled:Z

.field private mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/browser/view/PieItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPieView:Lcom/android/browser/view/PieMenu$PieView;

.field private mSelected:Z

.field private mView:Landroid/view/View;

.field private outer:I

.field private start:F

.field private sweep:F


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/browser/view/PieItem;->mView:Landroid/view/View;

    iput p2, p0, Lcom/android/browser/view/PieItem;->level:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/browser/view/PieItem;->mEnabled:Z

    invoke-virtual {p0}, Lcom/android/browser/view/PieItem;->getAnimationAngle()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/browser/view/PieItem;->setAnimationAngle(F)V

    invoke-virtual {p0}, Lcom/android/browser/view/PieItem;->getAlpha()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/browser/view/PieItem;->setAlpha(F)V

    return-void
.end method

.method public constructor <init>(Landroid/view/View;ILcom/android/browser/view/PieMenu$PieView;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Lcom/android/browser/view/PieMenu$PieView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/browser/view/PieItem;->mView:Landroid/view/View;

    iput p2, p0, Lcom/android/browser/view/PieItem;->level:I

    iput-object p3, p0, Lcom/android/browser/view/PieItem;->mPieView:Lcom/android/browser/view/PieMenu$PieView;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/view/PieItem;->mEnabled:Z

    return-void
.end method


# virtual methods
.method public addItem(Lcom/android/browser/view/PieItem;)V
    .locals 1
    .param p1    # Lcom/android/browser/view/PieItem;

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mItems:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/browser/view/PieItem;->mItems:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getAlpha()F
    .locals 1

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000

    goto :goto_0
.end method

.method public getAnimationAngle()F
    .locals 1

    iget v0, p0, Lcom/android/browser/view/PieItem;->animate:F

    return v0
.end method

.method public getInnerRadius()I
    .locals 1

    iget v0, p0, Lcom/android/browser/view/PieItem;->inner:I

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/browser/view/PieItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mItems:Ljava/util/List;

    return-object v0
.end method

.method public getLevel()I
    .locals 1

    iget v0, p0, Lcom/android/browser/view/PieItem;->level:I

    return v0
.end method

.method public getOuterRadius()I
    .locals 1

    iget v0, p0, Lcom/android/browser/view/PieItem;->outer:I

    return v0
.end method

.method public getPieView()Lcom/android/browser/view/PieMenu$PieView;
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/view/PieItem;->mEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mPieView:Lcom/android/browser/view/PieMenu$PieView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStart()F
    .locals 1

    iget v0, p0, Lcom/android/browser/view/PieItem;->start:F

    return v0
.end method

.method public getStartAngle()F
    .locals 2

    iget v0, p0, Lcom/android/browser/view/PieItem;->start:F

    iget v1, p0, Lcom/android/browser/view/PieItem;->animate:F

    add-float/2addr v0, v1

    return v0
.end method

.method public getSweep()F
    .locals 1

    iget v0, p0, Lcom/android/browser/view/PieItem;->sweep:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mView:Landroid/view/View;

    return-object v0
.end method

.method public hasItems()Z
    .locals 1

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mItems:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPieView()Z
    .locals 1

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mPieView:Lcom/android/browser/view/PieMenu$PieView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/view/PieItem;->mSelected:Z

    return v0
.end method

.method public setAlpha(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    return-void
.end method

.method public setAnimationAngle(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/browser/view/PieItem;->animate:F

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/browser/view/PieItem;->mEnabled:Z

    return-void
.end method

.method public setGeometry(FFII)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # I

    iput p1, p0, Lcom/android/browser/view/PieItem;->start:F

    iput p2, p0, Lcom/android/browser/view/PieItem;->sweep:F

    iput p3, p0, Lcom/android/browser/view/PieItem;->inner:I

    iput p4, p0, Lcom/android/browser/view/PieItem;->outer:I

    return-void
.end method

.method public setPieView(Lcom/android/browser/view/PieMenu$PieView;)V
    .locals 0
    .param p1    # Lcom/android/browser/view/PieMenu$PieView;

    iput-object p1, p0, Lcom/android/browser/view/PieItem;->mPieView:Lcom/android/browser/view/PieMenu$PieView;

    return-void
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/browser/view/PieItem;->mSelected:Z

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/view/PieItem;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setSelected(Z)V

    :cond_0
    return-void
.end method
