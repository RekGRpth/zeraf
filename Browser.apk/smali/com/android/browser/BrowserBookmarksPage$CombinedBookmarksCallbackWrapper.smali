.class Lcom/android/browser/BrowserBookmarksPage$CombinedBookmarksCallbackWrapper;
.super Ljava/lang/Object;
.source "BrowserBookmarksPage.java"

# interfaces
.implements Lcom/android/browser/BookmarksPageCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/BrowserBookmarksPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CombinedBookmarksCallbackWrapper"
.end annotation


# instance fields
.field private mCombinedCallback:Lcom/android/browser/CombinedBookmarksCallbacks;


# direct methods
.method private constructor <init>(Lcom/android/browser/CombinedBookmarksCallbacks;)V
    .locals 0
    .param p1    # Lcom/android/browser/CombinedBookmarksCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/browser/BrowserBookmarksPage$CombinedBookmarksCallbackWrapper;->mCombinedCallback:Lcom/android/browser/CombinedBookmarksCallbacks;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/browser/CombinedBookmarksCallbacks;Lcom/android/browser/BrowserBookmarksPage$1;)V
    .locals 0
    .param p1    # Lcom/android/browser/CombinedBookmarksCallbacks;
    .param p2    # Lcom/android/browser/BrowserBookmarksPage$1;

    invoke-direct {p0, p1}, Lcom/android/browser/BrowserBookmarksPage$CombinedBookmarksCallbackWrapper;-><init>(Lcom/android/browser/CombinedBookmarksCallbacks;)V

    return-void
.end method


# virtual methods
.method public onBookmarkSelected(Landroid/database/Cursor;Z)Z
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/browser/BrowserBookmarksPage$CombinedBookmarksCallbackWrapper;->mCombinedCallback:Lcom/android/browser/CombinedBookmarksCallbacks;

    invoke-static {p1}, Lcom/android/browser/BrowserBookmarksPage;->getUrl(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/browser/CombinedBookmarksCallbacks;->openUrl(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public varargs onOpenInNewWindow([Ljava/lang/String;)Z
    .locals 1
    .param p1    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/browser/BrowserBookmarksPage$CombinedBookmarksCallbackWrapper;->mCombinedCallback:Lcom/android/browser/CombinedBookmarksCallbacks;

    invoke-interface {v0, p1}, Lcom/android/browser/CombinedBookmarksCallbacks;->openInNewTab([Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method
