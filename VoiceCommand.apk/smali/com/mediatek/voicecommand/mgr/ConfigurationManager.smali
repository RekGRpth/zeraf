.class public Lcom/mediatek/voicecommand/mgr/ConfigurationManager;
.super Ljava/lang/Object;
.source "ConfigurationManager.java"


# static fields
.field private static sCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

.field private static sInstanceLock:[B


# instance fields
.field private isCfgPrepared:Z

.field private mConfigurationXML:Lcom/mediatek/voicecommand/cfg/ConfigurationXML;

.field private final mContext:Landroid/content/Context;

.field private mCurLanguageKey:Ljava/lang/String;

.field private mCurrentLanguageIndex:I

.field private mFeatureFilePath:Ljava/lang/String;

.field private mLanguageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mModeFilePath:Ljava/lang/String;

.field private final mPaths:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPswdFilePath:Ljava/lang/String;

.field private mRecogPatternPath:Ljava/lang/String;

.field private mUBMFilePath:Ljava/lang/String;

.field private mUIPatternPath:Ljava/lang/String;

.field private final mVoiceKeyWordInfos:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVoiceLanguageCacheFile:Ljava/lang/String;

.field private final mVoiceProcessInfos:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mVoiceUiCacheFile:Ljava/lang/String;

.field private mVoiceUiFeatureNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->sInstanceLock:[B

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceKeyWordInfos:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mLanguageList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    const-string v0, "RecogPattern"

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mRecogPatternPath:Ljava/lang/String;

    const-string v0, "UIPattern"

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mUIPatternPath:Ljava/lang/String;

    const-string v0, "PswdFile"

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPswdFilePath:Ljava/lang/String;

    const-string v0, "FeatureFile"

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mFeatureFilePath:Ljava/lang/String;

    const-string v0, "UBMFile"

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mUBMFilePath:Ljava/lang/String;

    const-string v0, "ModeFile"

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mModeFilePath:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->isCfgPrepared:Z

    const-string v0, "Voice_Ui"

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiCacheFile:Ljava/lang/String;

    const-string v0, "Voice_Language"

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceLanguageCacheFile:Ljava/lang/String;

    const-string v0, "CurLanguageIndex"

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurLanguageKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/mediatek/voicecommand/cfg/ConfigurationXML;

    invoke-direct {v0, p1}, Lcom/mediatek/voicecommand/cfg/ConfigurationXML;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mConfigurationXML:Lcom/mediatek/voicecommand/cfg/ConfigurationXML;

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mConfigurationXML:Lcom/mediatek/voicecommand/cfg/ConfigurationXML;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/voicecommand/cfg/ConfigurationXML;->readVoiceProcessInfoFromXml(Ljava/util/HashMap;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mConfigurationXML:Lcom/mediatek/voicecommand/cfg/ConfigurationXML;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/mediatek/voicecommand/cfg/ConfigurationXML;->readVoiceFilePathFromXml(Ljava/util/HashMap;)V

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mConfigurationXML:Lcom/mediatek/voicecommand/cfg/ConfigurationXML;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mLanguageList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/mediatek/voicecommand/cfg/ConfigurationXML;->readVoiceLanguangeFromXml(Ljava/util/ArrayList;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mRecogPatternPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->makeDirForPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mFeatureFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->makeDirForPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mUBMFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->makeDirForPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPswdFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->makeDirForPath(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->isCfgPrepared:Z

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->isCfgPrepared:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->checkVoiceCachePref()V

    iget v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mConfigurationXML:Lcom/mediatek/voicecommand/cfg/ConfigurationXML;

    iget-object v2, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceKeyWordInfos:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mLanguageList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;

    iget-object v0, v0, Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/mediatek/voicecommand/cfg/ConfigurationXML;->readKeyWordFromXml(Ljava/util/HashMap;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private checkVoiceCachePref()V
    .locals 11

    const/4 v10, 0x0

    iget-object v8, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiCacheFile:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    :try_start_0
    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v8, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iput-boolean v8, v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    iget-object v8, v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mRelationProcessName:Ljava/lang/String;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    iget-object v9, v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mRelationProcessName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-eqz v7, :cond_0

    iget-boolean v8, v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    iput-boolean v8, v7, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    goto :goto_1

    :catch_0
    move-exception v2

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v8, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceLanguageCacheFile:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    iget-object v8, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurLanguageKey:Ljava/lang/String;

    iget v9, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    invoke-interface {v6, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    const-string v8, "VCmdMgrService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "mCurrentLanguageIndex = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/mediatek/voicecommand/mgr/ConfigurationManager;
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->sCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    if-nez v0, :cond_1

    sget-object v1, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->sInstanceLock:[B

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->sCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-direct {v0, p0}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->sCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->sCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static makeDirForFile(Ljava/lang/String;)Z
    .locals 8
    .param p0    # Ljava/lang/String;

    const/4 v3, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x1fd

    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-static {v4, v5, v6, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x1b6

    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-static {v4, v5, v6, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static makeDirForPath(Ljava/lang/String;)Z
    .locals 7
    .param p0    # Ljava/lang/String;

    const/4 v2, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v2

    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1fd

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-static {v3, v4, v5, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public containOperationPermission(Ljava/lang/String;[I)Z
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [I

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    array-length v3, p2

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_1
    array-length v3, p2

    if-ge v0, v3, :cond_0

    iget-object v3, v1, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mPermissionIDList:Ljava/util/ArrayList;

    aget v4, p2, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getCurrentLanguage()I
    .locals 1

    iget v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    return v0
.end method

.method public getCurrentLanguageID()I
    .locals 2

    iget v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    if-gez v0, :cond_0

    iget v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mLanguageList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;

    iget v0, v0, Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;->mLanguageID:I

    goto :goto_0
.end method

.method public getFeatureEnable(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    iget-boolean v0, v2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    goto :goto_0
.end method

.method public getFeatureEnableArray()[I
    .locals 5

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v2, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    iget-boolean v3, v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getFeatureFilePath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mFeatureFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFeatureNameList()[Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    new-array v2, v0, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getKeyWord(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceKeyWordInfos:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceKeyWordInfos:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getKeyWordsPath()Ljava/lang/String;
    .locals 3

    iget v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    if-gez v0, :cond_0

    const-string v0, "VCmdMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mCurrentLanguageIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/system/etc/voicecommand/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mLanguageList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;

    iget-object v0, v0, Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLanguageList()[Ljava/lang/String;
    .locals 4

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mLanguageList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mLanguageList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;

    iget-object v3, v3, Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;->mName:Ljava/lang/String;

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getModelFile()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mModeFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPasswordFilePath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPswdFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getProcessID(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mID:I

    goto :goto_0
.end method

.method public getProcessInfo(Ljava/lang/String;)Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    return-object v0
.end method

.method public getProcessName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mProcessName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getUBMFilePath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mUBMFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getUiFeatureNumber()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getVoiceRecognitionPatternFilePath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mRecogPatternPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getVoiceUIPatternPath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mPaths:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mUIPatternPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hasOperationPermission(Ljava/lang/String;I)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mPermissionIDList:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public isAllOff()Z
    .locals 5

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    iget-boolean v1, v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    if-nez v1, :cond_0

    const/4 v3, 0x0

    :goto_1
    return v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public isAllowProcessRegister(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x3ed

    goto :goto_0
.end method

.method public isCfgPrepared()Z
    .locals 3

    const-string v0, "VCmdMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Voice Command Service CFG OK?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->isCfgPrepared:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->isCfgPrepared:Z

    return v0
.end method

.method public isProcessEnable(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-boolean v1, v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    goto :goto_0
.end method

.method public setCurrentLanguage(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    return-void
.end method

.method public updateCurLanguageToPref()V
    .locals 6

    iget-object v2, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mLanguageList:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceLanguageCacheFile:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurLanguageKey:Ljava/lang/String;

    iget v4, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceKeyWordInfos:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_1
    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceKeyWordInfos:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    iget v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    if-ltz v1, :cond_0

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mConfigurationXML:Lcom/mediatek/voicecommand/cfg/ConfigurationXML;

    iget-object v4, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceKeyWordInfos:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mLanguageList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mCurrentLanguageIndex:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;

    iget-object v1, v1, Lcom/mediatek/voicecommand/cfg/VoiceLanguageInfo;->mFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Lcom/mediatek/voicecommand/cfg/ConfigurationXML;->readKeyWordFromXml(Ljava/util/HashMap;Ljava/lang/String;)V

    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method public updateFeatureEnable(Ljava/lang/String;Z)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iput-boolean p2, v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    iget-object v1, v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mRelationProcessName:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mRelationProcessName:Ljava/lang/String;

    const-string v2, "android.mediatek.feature"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mRelationProcessName:Ljava/lang/String;

    invoke-virtual {p0, v1, p2}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->updateFeatureEnable(Ljava/lang/String;Z)Z

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public updateFeatureListEnable([Ljava/lang/String;[I)Ljava/lang/String;
    .locals 8
    .param p1    # [Ljava/lang/String;
    .param p2    # [I

    const/4 v7, 0x1

    if-eqz p1, :cond_0

    array-length v5, p1

    if-nez v5, :cond_2

    :cond_0
    const-string v0, "No process need to update"

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    if-eqz p2, :cond_3

    array-length v5, p2

    if-nez v5, :cond_4

    :cond_3
    const-string v0, "No enableValues"

    goto :goto_0

    :cond_4
    array-length v5, p1

    array-length v6, p2

    if-eq v5, v6, :cond_5

    const-string v0, "processList can\'t match enableValues"

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    array-length v3, p1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_9

    iget-object v5, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    aget-object v6, p1, v1

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-nez v2, :cond_7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p1, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_6
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_7
    aget v5, p2, v1

    if-ne v5, v7, :cond_8

    iput-boolean v7, v2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    :goto_3
    iget-object v5, v2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mRelationProcessName:Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    iget-object v6, v2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mRelationProcessName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-eqz v4, :cond_6

    iget-boolean v5, v2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    iput-boolean v5, v4, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    goto :goto_2

    :cond_8
    const/4 v5, 0x0

    iput-boolean v5, v2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    goto :goto_3

    :cond_9
    if-eqz v0, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Invalid Process Name"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public updateFeatureListEnableAll(Ljava/lang/Boolean;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/Boolean;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    if-nez v3, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Invalid Process Name"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public updateFeatureListEnableToPref()V
    .locals 7

    iget-object v4, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiCacheFile:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceUiFeatureNames:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "VCmdMgrService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateFeatureListEnableToPref featurename"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->mVoiceProcessInfos:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    iget-boolean v3, v3, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->isVoiceEnable:Z

    invoke-interface {v5, v0, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
