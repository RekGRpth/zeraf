.class public Lcom/mediatek/voicecommand/mgr/NativeDataManager;
.super Lcom/mediatek/voicecommand/mgr/VoiceDataManager;
.source "NativeDataManager.java"

# interfaces
.implements Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mHeadsetPlugReceiver:Landroid/content/BroadcastReceiver;

.field private mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

.field private mVoiceRecognize:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

.field private mVoiceServiceInternal:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

.field private mVoiceTraining:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

.field private mVoiceUI:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

.field private mjniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;


# direct methods
.method public constructor <init>(Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;)V
    .locals 5
    .param p1    # Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;

    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/mgr/VoiceDataManager;-><init>(Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;)V

    new-instance v1, Lcom/mediatek/voicecommand/mgr/NativeDataManager$1;

    invoke-direct {v1, p0}, Lcom/mediatek/voicecommand/mgr/NativeDataManager$1;-><init>(Lcom/mediatek/voicecommand/mgr/NativeDataManager;)V

    iput-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/voicecommand/mgr/NativeDataManager$2;

    invoke-direct {v1, p0}, Lcom/mediatek/voicecommand/mgr/NativeDataManager$2;-><init>(Lcom/mediatek/voicecommand/mgr/NativeDataManager;)V

    iput-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mHeadsetPlugReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;

    invoke-direct {v1, p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;-><init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;)V

    iput-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mjniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    new-instance v1, Lcom/mediatek/voicecommand/business/VoiceUI;

    iget-object v2, p1, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mConfigManager:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mjniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/mediatek/voicecommand/business/VoiceUI;-><init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;)V

    iput-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceUI:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    new-instance v1, Lcom/mediatek/voicecommand/business/VoiceTraining;

    iget-object v2, p1, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mConfigManager:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mjniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/mediatek/voicecommand/business/VoiceTraining;-><init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;)V

    iput-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceTraining:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    new-instance v1, Lcom/mediatek/voicecommand/business/VoiceRecognize;

    iget-object v2, p1, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mConfigManager:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mjniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/mediatek/voicecommand/business/VoiceRecognize;-><init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;)V

    iput-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceRecognize:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    new-instance v1, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;

    iget-object v2, p1, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mConfigManager:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    iget-object v3, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mjniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;-><init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;)V

    iput-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceServiceInternal:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mHeadsetPlugReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/voicecommand/mgr/NativeDataManager;)Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;
    .locals 1
    .param p0    # Lcom/mediatek/voicecommand/mgr/NativeDataManager;

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceUI:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/voicecommand/mgr/NativeDataManager;)Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;
    .locals 1
    .param p0    # Lcom/mediatek/voicecommand/mgr/NativeDataManager;

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceTraining:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/voicecommand/mgr/NativeDataManager;)Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;
    .locals 1
    .param p0    # Lcom/mediatek/voicecommand/mgr/NativeDataManager;

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceRecognize:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/voicecommand/mgr/NativeDataManager;)Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;
    .locals 1
    .param p0    # Lcom/mediatek/voicecommand/mgr/NativeDataManager;

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceServiceInternal:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    return-object v0
.end method


# virtual methods
.method public dispatchMessageDown(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 2
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    sparse-switch v1, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceUI:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    invoke-virtual {v1, p1}, Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;->handleSyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceTraining:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    invoke-virtual {v1, p1}, Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;->handleSyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceRecognize:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    invoke-virtual {v1, p1}, Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;->handleSyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceServiceInternal:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    invoke-virtual {v1, p1}, Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;->handleSyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x2710 -> :sswitch_3
    .end sparse-switch
.end method

.method public dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 3
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v1, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mService:Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;

    iget-object v1, v1, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mHeadsetPlugReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mVoiceServiceInternal:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    invoke-virtual {v1, p1}, Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;->handleSyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2710
        :pswitch_0
    .end packed-switch
.end method

.method public setDownDispatcher(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;)V
    .locals 0
    .param p1    # Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    return-void
.end method

.method public setUpDispatcher(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;)V
    .locals 0
    .param p1    # Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    iput-object p1, p0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    return-void
.end method
