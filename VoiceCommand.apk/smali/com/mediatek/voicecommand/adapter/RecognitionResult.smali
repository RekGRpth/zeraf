.class public Lcom/mediatek/voicecommand/adapter/RecognitionResult;
.super Ljava/lang/Object;
.source "RecognitionResult.java"


# static fields
.field public static final VOICE_UNLOCK_ACCESS_FAIL:I = 0x0

.field public static final VOICE_UNLOCK_ACCESS_SUCCESS:I = 0x1

.field public static final VOICE_UNLOCK_TOO_NOISY:I = 0x2

.field public static final VOID_UNLOCK_VOICE_LOW:I = 0x3


# instance fields
.field public msgid:I

.field public voicecmdid:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->voicecmdid:I

    iput p2, p0, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->msgid:I

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/voicecommand/adapter/RecognitionResult;)V
    .locals 1
    .param p1    # Lcom/mediatek/voicecommand/adapter/RecognitionResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p1, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->voicecmdid:I

    iput v0, p0, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->voicecmdid:I

    iget v0, p1, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->msgid:I

    iput v0, p0, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->msgid:I

    return-void
.end method


# virtual methods
.method public set(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->voicecmdid:I

    iput p2, p0, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->msgid:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecognitionResult("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->voicecmdid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->msgid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
