.class public Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;
.super Ljava/lang/Object;
.source "JNICommandAdapter.java"

# interfaces
.implements Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;
    }
.end annotation


# static fields
.field private static final MODE_VOICE_UNKNOW:I = -0x1

.field private static final NATIVE_MODE_VOICE_RECOGNITION:I = 0x4

.field private static final NATIVE_MODE_VOICE_TRAINING:I = 0x2

.field private static final NATIVE_MODE_VOICE_UI:I = 0x1

.field private static final NOTIFY_VOICE_ERROR:I = -0x1

.field private static final NOTIFY_VOICE_HEADSET_PLUG:I = 0x64

.field private static final NOTIFY_VOICE_RECOGNITION:I = 0x2

.field private static final NOTIFY_VOICE_TRAINING:I = 0x1

.field private static final NOTIFY_VOICE_TRAINING_EXIST_PSWD:I = 0x5

.field private static final NOTIFY_VOICE_TRAINING_FINISHED:I = 0x0

.field private static final NOTIFY_VOICE_TRAINING_OK_CONFIDENCE:I = 0x64

.field private static final NOTIFY_VOICE_TRAINING_TIMEOUT:I = 0x6

.field private static final NOTIFY_VOICE_UI:I = 0x0

.field private static final VOICE_HEADSET_MODE:I = 0x2

.field private static final VOICE_NORMAL_MODE:I = 0x1


# instance fields
.field private final curHandler:Landroid/os/Handler;

.field private mActiveProcessList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;",
            ">;"
        }
    .end annotation
.end field

.field private mCurMode:I

.field private mCurRecogProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

.field private mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

.field private mCurTrainingProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

.field private mFeatureFilePath:Ljava/lang/String;

.field private mFeatureFileStream:Ljava/io/FileOutputStream;

.field private mHeadsetMode:I

.field private mNativeContext:I

.field private mPasswordFilePath:Ljava/lang/String;

.field private mPasswordFileStream:Ljava/io/FileOutputStream;

.field private mPatternFilePath:Ljava/lang/String;

.field private mPatternFileStream:Ljava/io/FileOutputStream;

.field private mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

.field private mVoiceUiLanguageId:I

.field private mVoiceUiModelPath:Ljava/lang/String;

.field private mVoiceUiPatternPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v1, "voicerecognition_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->native_init()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;)V
    .locals 1
    .param p1    # Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mNativeContext:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mActiveProcessList:Ljava/util/ArrayList;

    new-instance v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$1;

    invoke-direct {v0, p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$1;-><init>(Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;)V

    iput-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->curHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mHeadsetMode:I

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->native_setup(Ljava/lang/Object;)V

    return-void
.end method

.method private final native _release()V
.end method

.method static synthetic access$000(Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;Landroid/os/Message;)V
    .locals 0
    .param p0    # Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;
    .param p1    # Landroid/os/Message;

    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->handleNotifyVoiceError(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;Landroid/os/Message;)V
    .locals 0
    .param p0    # Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;
    .param p1    # Landroid/os/Message;

    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->handleNotifyVoiceUI(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;Landroid/os/Message;)V
    .locals 0
    .param p0    # Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;
    .param p1    # Landroid/os/Message;

    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->handleNotifyVoiceTraining(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;Landroid/os/Message;)V
    .locals 0
    .param p0    # Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;
    .param p1    # Landroid/os/Message;

    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->handleNotifyVoiceRecognition(Landroid/os/Message;)V

    return-void
.end method

.method private addActiveProcess(Ljava/lang/String;I)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v3, v3, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget v3, v3, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mPid:I

    if-ne v3, p2, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mActiveProcessList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mActiveProcessList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v3, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mActiveProcessList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget v3, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mPid:I

    if-eq v3, p2, :cond_1

    const/4 v0, 0x0

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    invoke-direct {v0, p0, p1, p2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;-><init>(Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;Ljava/lang/String;I)V

    :cond_2
    iput-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mActiveProcessList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private deleteActiveProcess(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mActiveProcessList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_1

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mActiveProcessList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v3, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mPid:I

    if-ne v3, p2, :cond_0

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mActiveProcessList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private deleteCommandFiles(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [I

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    array-length v2, p4

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    :cond_0
    const-string v2, "VCmdMgrService"

    const-string v3, "deleteFile error "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x1

    aget v2, p4, v2

    if-ge v1, v2, :cond_1

    const/4 v2, 0x0

    aget v2, p4, v2

    shr-int/2addr v2, v1

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".dat"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteFile(Ljava/lang/String;)Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteFile(Ljava/lang/String;)Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteFile(Ljava/lang/String;)Z

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private deleteFile(Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_0
    :goto_0
    return v2

    :catch_0
    move-exception v0

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteFile error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private native getVoiceIntensity()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private handleNotifyVoiceError(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x5

    iget v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const-string v1, "VCmdMgrService"

    const-string v2, "handleNotifyVoiceError mCurMode =  MODE_VOICE_UNKNOW"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    invoke-direct {v0}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;-><init>()V

    iget v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    iput v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    iget v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    packed-switch v1, :pswitch_data_0

    :cond_1
    :goto_1
    const/16 v1, 0x3ee

    invoke-static {v1}, Lcom/mediatek/voicecommand/data/DataPackage;->packageErrorResult(I)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v1, v0}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v1, v1, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    iput-object v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    iput v3, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v1, v1, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget v2, v2, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mPid:I

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceUI(Ljava/lang/String;I)I

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTrainingProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTrainingProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v1, v1, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    iput-object v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    iput v3, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceTraining(Z)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurRecogProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurRecogProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v1, v1, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    iput-object v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    const/4 v1, 0x3

    iput v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    invoke-direct {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoicePWRecognition()I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleNotifyVoiceRecognition(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v4, 0x4

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurRecogProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v3, :cond_0

    new-instance v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    invoke-direct {v1}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;-><init>()V

    iput v4, v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    const/4 v3, 0x3

    iput v3, v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurRecogProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v3, v3, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    iput-object v3, v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    const/4 v3, 0x4

    :try_start_0
    invoke-direct {p0, v3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopCaptureVoice(I)V

    iget v3, p1, Landroid/os/Message;->arg1:I

    const/16 v4, 0x64

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    iget v4, p1, Landroid/os/Message;->arg1:I

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(III)Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v3, v1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    const/4 v3, -0x1

    iput v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    :cond_0
    return-void

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->startVoicePWRecognition()Lcom/mediatek/voicecommand/adapter/RecognitionResult;

    move-result-object v2

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleNotifyVoiceRecognition msgid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->msgid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " voicecmdid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->voicecmdid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x1

    iget v4, v2, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->msgid:I

    iget v5, v2, Lcom/mediatek/voicecommand/adapter/RecognitionResult;->voicecmdid:I

    invoke-static {v3, v4, v5}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(III)Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v3, 0x3ee

    invoke-static {v3}, Lcom/mediatek/voicecommand/data/DataPackage;->packageErrorResult(I)Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleNotifyVoiceRecognition Error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "exception = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleNotifyVoiceTraining(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/16 v4, 0x64

    const/4 v3, 0x5

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTrainingProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v2, :cond_1

    new-instance v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    invoke-direct {v1}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;-><init>()V

    const/4 v2, 0x3

    iput v2, v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    iput v3, v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    iget-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTrainingProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v2, v2, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    iput-object v2, v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    iget v2, p1, Landroid/os/Message;->arg1:I

    if-nez v2, :cond_2

    iput v4, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v6}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceTraining(Z)V

    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->startVoiceTraining()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-static {v6, v2, v3}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(III)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, v1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v2, v1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v5}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceTraining(Z)V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    const-string v2, "VCmdMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleNotifyVoiceTraining startVoiceTraining Error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eq v2, v3, :cond_3

    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v3, 0x6

    if-eq v2, v3, :cond_3

    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ne v2, v4, :cond_0

    :cond_3
    invoke-direct {p0, v5}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceTraining(Z)V

    goto :goto_0
.end method

.method private handleNotifyVoiceUI(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v3, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget v4, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mPid:I

    invoke-direct {p0, v3, v4}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteActiveProcess(Ljava/lang/String;I)V

    iput-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mVoiceUiModelPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mVoiceUiPatternPath:Ljava/lang/String;

    iget v5, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mVoiceUiLanguageId:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->startVoiceUI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v6, Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    invoke-direct {v6}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;-><init>()V

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v0, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    iput-object v0, v6, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, v6, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    const/4 v0, 0x5

    iput v0, v6, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    const/4 v0, 0x1

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v1, v2}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(IILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, v6, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v0, v6}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v0, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget v1, v1, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mPid:I

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceUI(Ljava/lang/String;I)I

    goto :goto_0
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchMethodException;
        }
    .end annotation
.end method

.method private final native native_setup(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "VCmdMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Message from native what="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " arg1="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " arg2="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->curHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->curHandler:Landroid/os/Handler;

    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->curHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private resetFdStream(Ljava/io/FileOutputStream;Ljava/lang/String;)Ljava/io/FileOutputStream;
    .locals 6
    .param p1    # Ljava/io/FileOutputStream;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopFdStream(Ljava/io/FileOutputStream;)V

    const/4 v1, 0x0

    invoke-static {p2}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->makeDirForFile(Ljava/lang/String;)Z

    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ResetFdStream Error path =  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private resetFdStream(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPasswordFileStream:Ljava/io/FileOutputStream;

    invoke-direct {p0, v0, p1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->resetFdStream(Ljava/io/FileOutputStream;Ljava/lang/String;)Ljava/io/FileOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPasswordFileStream:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPatternFileStream:Ljava/io/FileOutputStream;

    invoke-direct {p0, v0, p2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->resetFdStream(Ljava/io/FileOutputStream;Ljava/lang/String;)Ljava/io/FileOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPatternFileStream:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mFeatureFileStream:Ljava/io/FileOutputStream;

    invoke-direct {p0, v0, p3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->resetFdStream(Ljava/io/FileOutputStream;Ljava/lang/String;)Ljava/io/FileOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mFeatureFileStream:Ljava/io/FileOutputStream;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iput-object p1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPasswordFilePath:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPatternFilePath:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mFeatureFilePath:Ljava/lang/String;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private native setCommandId(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private final native setInputMode(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native setVoiceFeatureFile(Ljava/io/FileDescriptor;JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method private native setVoicePasswordFile(Ljava/io/FileDescriptor;JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method private native setVoicePatternFile(Ljava/io/FileDescriptor;JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method private native startCaptureVoice(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native startVoicePWRecognition()Lcom/mediatek/voicecommand/adapter/RecognitionResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native startVoiceTraining()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native stopCaptureVoice(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private stopFdStream(Ljava/io/FileOutputStream;)V
    .locals 1
    .param p1    # Ljava/io/FileOutputStream;

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 p1, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private stopFdStream(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPasswordFileStream:Ljava/io/FileOutputStream;

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopFdStream(Ljava/io/FileOutputStream;)V

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPatternFileStream:Ljava/io/FileOutputStream;

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopFdStream(Ljava/io/FileOutputStream;)V

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mFeatureFileStream:Ljava/io/FileOutputStream;

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopFdStream(Ljava/io/FileOutputStream;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPasswordFilePath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteFile(Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPatternFilePath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteFile(Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mFeatureFilePath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteFile(Ljava/lang/String;)Z

    :cond_0
    iput-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPasswordFilePath:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPatternFilePath:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mFeatureFilePath:Ljava/lang/String;

    return-void
.end method

.method private stopTopProcess()V
    .locals 2

    iget v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v0, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget v1, v1, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mPid:I

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceUI(Ljava/lang/String;I)I

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoicePWRecognition()I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceTraining(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private stopVoicePWRecognition()I
    .locals 6

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->isNativePrepared()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    move v2, v1

    :goto_1
    return v2

    :cond_0
    const/16 v1, 0x3ee

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    if-eq v3, v4, :cond_2

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stopVoicePWRecognition Error because the Native in other mode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x3ef

    :goto_2
    move v2, v1

    goto :goto_1

    :cond_2
    const/4 v3, 0x4

    :try_start_0
    invoke-direct {p0, v3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopCaptureVoice(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurRecogProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    const/4 v3, -0x1

    iput v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stopVoicePWRecognition Error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private stopVoiceTraining(Z)V
    .locals 6
    .param p1    # Z

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->isNativePrepared()Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-void

    :cond_0
    const/16 v0, 0x3ee

    goto :goto_0

    :cond_1
    const/4 v3, 0x2

    :try_start_0
    invoke-direct {p0, v3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopCaptureVoice(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    if-nez p1, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-direct {p0, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopFdStream(Z)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTrainingProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    const/4 v2, -0x1

    iput v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stopVoiceTraining Error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "exception = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public getNativeIntensity()I
    .locals 5

    const/4 v1, 0x0

    iget v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->getVoiceIntensity()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_1
    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v2, -0x1

    iput v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const-string v2, "VCmdMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVoiceTraingIntensity Error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isNativePrepared()Z
    .locals 1

    iget v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mNativeContext:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public release()V
    .locals 1

    iget v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->_release()V

    return-void

    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopCaptureVoice(I)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopCaptureVoice(I)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopCaptureVoice(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public resetVoiceTraining(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    const/4 v0, 0x0

    iget v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".dat"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteFile(Ljava/lang/String;)Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteFile(Ljava/lang/String;)Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteFile(Ljava/lang/String;)Z

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x3ef

    const-string v2, "VCmdMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resetVoiceTraining Error because the Native in other mode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public native setActiveAP(IZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setActiveLanguage(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setCurHeadsetMode(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x2

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    const-string v1, "VCmdMgrService"

    const-string v2, "handleHeadSetPlugEvent in"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mHeadsetMode:I

    :goto_0
    iget v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    :goto_1
    return-void

    :cond_0
    const-string v1, "VCmdMgrService"

    const-string v2, "handleHeadSetPlugEvent out"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mHeadsetMode:I

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iget v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    packed-switch v1, :pswitch_data_0

    :goto_2
    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->curHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :pswitch_0
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    goto :goto_2

    :pswitch_1
    iput v4, v0, Landroid/os/Message;->what:I

    goto :goto_2

    :pswitch_2
    iput v3, v0, Landroid/os/Message;->what:I

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public native setModeIFile(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setVoicePatternFile(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setVoicePatternPath(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setVoiceUBMFile(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public startVoicePWRecognition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    const/4 v6, -0x1

    const-string v3, "VCmdMgrService"

    const-string v4, "startVoicePWRecognition "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->isNativePrepared()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    move v2, v1

    :goto_1
    return v2

    :cond_0
    const/16 v1, 0x3ee

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    if-eq v3, v6, :cond_2

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startVoicePWRecognition: stop current mode ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopTopProcess()V

    :cond_2
    :try_start_0
    const-string v3, "VCmdMgrService"

    const-string v4, "startVoicePWRecognition  startCaptureVoice"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setVoicePatternFile(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setVoiceUBMFile(Ljava/lang/String;)V

    iget v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mHeadsetMode:I

    invoke-direct {p0, v3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setInputMode(I)V

    const/4 v3, 0x4

    invoke-direct {p0, v3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->startCaptureVoice(I)V

    const/4 v3, 0x4

    iput v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    new-instance v3, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    invoke-direct {v3, p0, p3, p4}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;-><init>(Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;Ljava/lang/String;I)V

    iput-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurRecogProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move v2, v1

    goto :goto_1

    :catch_0
    move-exception v0

    iput v6, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/16 v1, 0x3ee

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startVoicePWRecognition Error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public startVoiceTraining(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[ILjava/lang/String;I)I
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # [I
    .param p7    # Ljava/lang/String;
    .param p8    # I

    const-string v2, "VCmdMgrService"

    const-string v3, "startVoiceTraining "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->isNativePrepared()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v9, 0x0

    :goto_0
    if-eqz v9, :cond_1

    move v10, v9

    :goto_1
    return v10

    :cond_0
    const/16 v9, 0x3ee

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    const-string v2, "VCmdMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startVoiceTraining: stop current mode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopTopProcess()V

    :cond_2
    move-object/from16 v0, p6

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteCommandFiles(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".dat"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->resetFdStream(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPasswordFileStream:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setVoicePasswordFile(Ljava/io/FileDescriptor;JJ)V

    iget-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mPatternFileStream:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setVoicePatternFile(Ljava/io/FileDescriptor;JJ)V

    iget-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mFeatureFileStream:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setVoiceFeatureFile(Ljava/io/FileDescriptor;JJ)V

    invoke-virtual {p0, p2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setVoicePatternFile(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setVoiceUBMFile(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setCommandId(I)V

    iget v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mHeadsetMode:I

    invoke-direct {p0, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setInputMode(I)V

    const-string v2, "VCmdMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startVoiceTraining  pwdpath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " patternpath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " featurepath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " umbpath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " commandid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->startCaptureVoice(I)V

    const-string v2, "VCmdMgrService"

    const-string v3, "startVoiceTraining  success"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    new-instance v2, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    move-object/from16 v0, p7

    move/from16 v1, p8

    invoke-direct {v2, p0, v0, v1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;-><init>(Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;Ljava/lang/String;I)V

    iput-object v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTrainingProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    const/4 v2, 0x3

    iput v2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    :goto_3
    move v10, v9

    goto/16 :goto_1

    :catch_0
    move-exception v8

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopFdStream(Z)V

    const/16 v9, 0x3ee

    const-string v2, "VCmdMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startVoiceTraining Error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    const-string v2, "VCmdMgrService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startVoiceTraining Error because can\'t create the output stream "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v9, 0x3ee

    goto :goto_3
.end method

.method public startVoiceUI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I

    const/4 v1, 0x0

    const/4 v8, -0x1

    const/4 v7, 0x2

    const/4 v6, 0x1

    const-string v3, "VCmdMgrService"

    const-string v4, "startVoiceUI "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->isNativePrepared()Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    if-eqz v1, :cond_1

    move v2, v1

    :goto_1
    return v2

    :cond_0
    const/16 v1, 0x3ee

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    if-eq v3, v8, :cond_4

    iget v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    if-eq v3, v7, :cond_4

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startVoiceUI: stop current mode ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopTopProcess()V

    :cond_2
    :goto_2
    invoke-direct {p0, p3, p4}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->addActiveProcess(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    iput v7, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    :try_start_0
    iput-object p2, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mVoiceUiPatternPath:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mVoiceUiModelPath:Ljava/lang/String;

    iput p5, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mVoiceUiLanguageId:I

    iget v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mHeadsetMode:I

    invoke-direct {p0, v3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setInputMode(I)V

    invoke-virtual {p0, p1}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setModeIFile(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setVoicePatternPath(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {p0, p4, v3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setActiveAP(IZ)V

    add-int/lit8 v3, p5, -0x1

    shl-int v3, v6, v3

    and-int/lit16 v3, v3, 0xff

    invoke-virtual {p0, v3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setActiveLanguage(I)V

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startVoiceUI  modelpath="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " patternpath="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " languageid = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, p5, -0x1

    shl-int v5, v6, v5

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->startCaptureVoice(I)V

    const-string v3, "VCmdMgrService"

    const-string v4, "startVoiceUI  success"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    :goto_3
    move v2, v1

    goto/16 :goto_1

    :cond_4
    iget v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    if-ne v3, v7, :cond_2

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v3, :cond_2

    :try_start_1
    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget v3, v3, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mPid:I

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setActiveAP(IZ)V

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopCaptureVoice(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startVoiceUI Error when stop capture voice first"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :catch_1
    move-exception v0

    iput v8, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/16 v1, 0x3ee

    const-string v3, "VCmdMgrService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startVoiceUI Error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public stopCurMode(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceUI(Ljava/lang/String;I)I

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1, p2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoicePWRecognition(Ljava/lang/String;I)I

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1, p2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceTraining(Ljava/lang/String;I)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public stopVoicePWRecognition(Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurRecogProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurRecogProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v0, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurRecogProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget v0, v0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mPid:I

    if-eq v0, p2, :cond_1

    :cond_0
    const/16 v0, 0x3ed

    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoicePWRecognition()I

    move-result v0

    goto :goto_0
.end method

.method public stopVoiceTraining(Ljava/lang/String;I)I
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTrainingProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTrainingProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget-object v1, v1, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mProcessName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTrainingProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iget v1, v1, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;->mPid:I

    if-eq v1, p2, :cond_1

    :cond_0
    const/16 v0, 0x3ed

    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, v0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopVoiceTraining(Z)V

    goto :goto_0
.end method

.method public stopVoiceUI(Ljava/lang/String;I)I
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v1, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->isNativePrepared()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    if-eqz v1, :cond_1

    move v2, v1

    :goto_1
    return v2

    :cond_0
    const/16 v1, 0x3ee

    goto :goto_0

    :cond_1
    iget v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_2

    const-string v4, "VCmdMgrService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopVoiceUI Error because the Native in other mode "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x3ef

    :goto_2
    move v2, v1

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->deleteActiveProcess(Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mActiveProcessList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    iget-object v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mActiveProcessList:Ljava/util/ArrayList;

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    iput-object v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    goto :goto_2

    :cond_3
    iput v7, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurMode:I

    iput-object v6, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mCurTopActiveProcess:Lcom/mediatek/voicecommand/adapter/JNICommandAdapter$ActiveProcess;

    const/4 v4, 0x0

    :try_start_0
    iput-object v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mVoiceUiPatternPath:Ljava/lang/String;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mVoiceUiModelPath:Ljava/lang/String;

    const/4 v4, -0x1

    iput v4, p0, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->mVoiceUiLanguageId:I

    const/4 v4, 0x0

    invoke-virtual {p0, p2, v4}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->setActiveAP(IZ)V

    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/mediatek/voicecommand/adapter/JNICommandAdapter;->stopCaptureVoice(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v4, "VCmdMgrService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopVoiceUI Error "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
