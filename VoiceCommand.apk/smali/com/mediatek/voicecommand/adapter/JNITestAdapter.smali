.class public Lcom/mediatek/voicecommand/adapter/JNITestAdapter;
.super Ljava/lang/Object;
.source "JNITestAdapter.java"

# interfaces
.implements Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;


# instance fields
.field private final MODE_VOICE_UNKNOW:I

.field private final curHandler:Landroid/os/Handler;

.field private final mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

.field private mCurMode:I

.field private final mDelaytime:I

.field private mRecognitionCommandid:I

.field private final mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;


# direct methods
.method public constructor <init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;)V
    .locals 2
    .param p1    # Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;
    .param p2    # Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->MODE_VOICE_UNKNOW:I

    const/16 v0, 0x1388

    iput v0, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mDelaytime:I

    new-instance v0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter$1;

    invoke-direct {v0, p0}, Lcom/mediatek/voicecommand/adapter/JNITestAdapter$1;-><init>(Lcom/mediatek/voicecommand/adapter/JNITestAdapter;)V

    iput-object v0, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->curHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mRecognitionCommandid:I

    iput v1, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mCurMode:I

    iput-object p2, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    iput-object p1, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/voicecommand/adapter/JNITestAdapter;ILcom/mediatek/voicecommand/cfg/VoiceProcessInfo;)V
    .locals 0
    .param p0    # Lcom/mediatek/voicecommand/adapter/JNITestAdapter;
    .param p1    # I
    .param p2    # Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->sendVoiceTrainingCommand(ILcom/mediatek/voicecommand/cfg/VoiceProcessInfo;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/voicecommand/adapter/JNITestAdapter;ILcom/mediatek/voicecommand/cfg/VoiceProcessInfo;)V
    .locals 0
    .param p0    # Lcom/mediatek/voicecommand/adapter/JNITestAdapter;
    .param p1    # I
    .param p2    # Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->sendVoiceUICommand(ILcom/mediatek/voicecommand/cfg/VoiceProcessInfo;)V

    return-void
.end method

.method private sendVoiceTrainingCommand(ILcom/mediatek/voicecommand/cfg/VoiceProcessInfo;)V
    .locals 7
    .param p1    # I
    .param p2    # Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x1

    new-instance v2, Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    invoke-direct {v2}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;-><init>()V

    iget-object v3, p2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mProcessName:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    iput v5, v2, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    iput v6, v2, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    const/4 v0, 0x0

    if-ge p1, v6, :cond_0

    const/4 v0, 0x1

    add-int/lit8 p1, p1, 0x1

    :cond_0
    mul-int/lit8 v3, p1, 0x14

    invoke-static {v4, v0, v3}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(III)Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, v2, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v3, v2}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    if-ne v0, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->curHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v5, p1, v4, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->curHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1388

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    return-void
.end method

.method private sendVoiceUICommand(ILcom/mediatek/voicecommand/cfg/VoiceProcessInfo;)V
    .locals 6
    .param p1    # I
    .param p2    # Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    const/4 v5, 0x2

    iget-object v3, p2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mCommandIDList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt p1, v3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v3, p2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mCommandIDList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v2, Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    invoke-direct {v2}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;-><init>()V

    iget-object v3, p2, Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;->mProcessName:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    iput v5, v2, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    const/4 v3, 0x5

    iput v3, v2, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v3, v0, v4}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(IILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, v2, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v3, v2}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->curHandler:Landroid/os/Handler;

    add-int/lit8 p1, p1, 0x1

    const/4 v4, 0x0

    invoke-virtual {v3, v5, p1, v4, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->curHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1388

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method


# virtual methods
.method public getNativeIntensity()I
    .locals 3

    new-instance v0, Ljava/util/Random;

    const-wide/16 v1, 0x64

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    return v0
.end method

.method public isNativePrepared()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public release()V
    .locals 0

    return-void
.end method

.method public resetVoiceTraining(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    const/4 v0, 0x0

    return v0
.end method

.method public setCurHeadsetMode(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public startVoicePWRecognition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    const/4 v4, 0x0

    new-instance v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    invoke-direct {v0}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;-><init>()V

    iput-object p3, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    const/4 v1, 0x2

    iput v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    const/4 v1, 0x5

    iput v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    const/4 v1, 0x1

    iget v2, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mRecognitionCommandid:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mRecognitionCommandid:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(IILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget v1, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mRecognitionCommandid:I

    const/4 v2, 0x3

    if-le v1, v2, :cond_0

    iput v4, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mRecognitionCommandid:I

    :cond_0
    iget-object v1, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v1, v0}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    return v4
.end method

.method public startVoiceTraining(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[ILjava/lang/String;I)I
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # [I
    .param p7    # Ljava/lang/String;
    .param p8    # I

    const/4 v4, 0x3

    const/4 v2, 0x0

    iput v4, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mCurMode:I

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v3, p7}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getProcessInfo(Ljava/lang/String;)Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    move-result-object v1

    if-nez v1, :cond_0

    const/16 v2, 0x3ed

    :goto_0
    return v2

    :cond_0
    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->curHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4, v2, v2, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v3, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->curHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1388

    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public startVoiceUI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I

    const/4 v5, 0x2

    const/4 v3, 0x0

    iput v5, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mCurMode:I

    iget-object v4, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v4, p3}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getProcessInfo(Ljava/lang/String;)Lcom/mediatek/voicecommand/cfg/VoiceProcessInfo;

    move-result-object v2

    if-nez v2, :cond_0

    const/16 v3, 0x3ed

    :goto_0
    return v3

    :cond_0
    const/4 v0, 0x0

    iget-object v4, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->curHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5, v0, v3, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v4, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->curHandler:Landroid/os/Handler;

    const-wide/16 v5, 0x1388

    invoke-virtual {v4, v1, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public stopCurMode(Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    return-void
.end method

.method public stopVoicePWRecognition(Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mCurMode:I

    const/4 v0, 0x0

    return v0
.end method

.method public stopVoiceTraining(Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mCurMode:I

    const/4 v0, 0x0

    return v0
.end method

.method public stopVoiceUI(Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/voicecommand/adapter/JNITestAdapter;->mCurMode:I

    const/4 v0, 0x0

    return v0
.end method
