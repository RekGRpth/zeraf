.class public Lcom/mediatek/voicecommand/business/VoiceSetting;
.super Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;
.source "VoiceSetting.java"


# direct methods
.method public constructor <init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;)V
    .locals 0
    .param p1    # Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;
    .param p2    # Lcom/mediatek/voicecommand/mgr/ConfigurationManager;
    .param p3    # Landroid/os/Handler;

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;-><init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;)V

    return-void
.end method

.method private changeCurLanguage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 7
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget-object v4, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    if-nez v4, :cond_1

    const-string v4, "VCmdMgrService"

    const-string v5, "changeLanguage error: Invalid extra data is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x3f0

    :cond_0
    :goto_0
    if-nez v0, :cond_4

    invoke-static {}, Lcom/mediatek/voicecommand/data/DataPackage;->packageSuccessResult()Landroid/os/Bundle;

    move-result-object v4

    :goto_1
    iput-object v4, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v4, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v4, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    return v0

    :cond_1
    iget-object v4, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    const-string v5, "Send_Info"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v4, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v4}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getLanguageList()[Ljava/lang/String;

    move-result-object v4

    array-length v2, v4

    if-ltz v1, :cond_2

    if-lt v1, v2, :cond_3

    :cond_2
    const-string v4, "VCmdMgrService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "changeLanguage error: language index = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x3f0

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v4}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getCurrentLanguage()I

    move-result v4

    if-eq v4, v1, :cond_0

    iget-object v4, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v4, v1}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->setCurrentLanguage(I)V

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->copySelf(Z)Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/voicecommand/business/VoiceSetting;->sendMessageToHandler(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    goto :goto_0

    :cond_4
    invoke-static {v0}, Lcom/mediatek/voicecommand/data/DataPackage;->packageErrorResult(I)Landroid/os/Bundle;

    move-result-object v4

    goto :goto_1
.end method

.method private sendKeyWordsPath(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 4
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v2}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getKeyWordsPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(ILjava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v2, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    return v0
.end method

.method private sendLanguageList(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 5
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const-string v3, "VCmdMgrService"

    const-string v4, "sendLanguageList"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v3}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getLanguageList()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v3}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getCurrentLanguage()I

    move-result v0

    if-eqz v2, :cond_0

    array-length v3, v2

    if-nez v3, :cond_1

    :cond_0
    const/16 v1, 0x3ee

    const-string v3, "VCmdMgrService"

    const-string v4, "sendLanguageList error: No Language allowed to support voice command!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/mediatek/voicecommand/data/DataPackage;->packageErrorResult(I)Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    :goto_0
    iget-object v3, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v3, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    return v1

    :cond_1
    const/4 v3, 0x1

    invoke-static {v3, v2, v0}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(I[Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    goto :goto_0
.end method

.method private sendProcessList(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 7
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v5}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getUiFeatureNumber()I

    move-result v4

    if-lez v4, :cond_0

    iget-object v5, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v5}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getFeatureNameList()[Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v5}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getFeatureEnableArray()[I

    move-result-object v2

    const/4 v5, 0x1

    invoke-static {v5, v3, v2}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(I[Ljava/lang/String;[I)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    iput-object v0, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v5, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v5, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    return v1

    :cond_0
    const/16 v1, 0x3ee

    const-string v5, "VCmdMgrService"

    const-string v6, "sendProcessList error: No Porcess allowed to use voice command"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/mediatek/voicecommand/data/DataPackage;->packageErrorResult(I)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method private updateProcessList(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 9
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v6, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    if-nez v6, :cond_0

    const-string v6, "VCmdMgrService"

    const-string v7, "updateProcessList error: No Porcess allowed to use voice command"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v3, 0x3f0

    invoke-static {v3}, Lcom/mediatek/voicecommand/data/DataPackage;->packageErrorResult(I)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    iput-object v0, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v6, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v6, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    return v3

    :cond_0
    iget-object v6, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    const-string v7, "Send_Info"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    const-string v7, "Send_Info1"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    iget-object v6, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v6, v5, v1}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->updateFeatureListEnable([Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const/16 v3, 0x3f0

    const-string v6, "VCmdMgrService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateProcessList error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v3}, Lcom/mediatek/voicecommand/data/DataPackage;->packageErrorResult(I)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/mediatek/voicecommand/data/DataPackage;->packageSuccessResult()Landroid/os/Bundle;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->copySelf(Z)Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/mediatek/voicecommand/business/VoiceSetting;->sendMessageToHandler(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    goto :goto_0
.end method

.method private updateProcessListAll(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 7
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v5, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    if-nez v5, :cond_0

    const-string v5, "VCmdMgrService"

    const-string v6, "updateProcessListAll error: No Porcess allowed to use voice command"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v2, 0x3f0

    invoke-static {v2}, Lcom/mediatek/voicecommand/data/DataPackage;->packageErrorResult(I)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    iput-object v0, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v5, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v5, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    return v2

    :cond_0
    iget-object v5, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    const-string v6, "Send_Info"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v5, v3}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->updateFeatureListEnableAll(Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const/16 v2, 0x3f0

    invoke-static {v2}, Lcom/mediatek/voicecommand/data/DataPackage;->packageErrorResult(I)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/mediatek/voicecommand/data/DataPackage;->packageSuccessResult()Landroid/os/Bundle;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->copySelf(Z)Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/mediatek/voicecommand/business/VoiceSetting;->sendMessageToHandler(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    goto :goto_0
.end method


# virtual methods
.method public handleAsyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 2
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v1}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->updateFeatureListEnableToPref()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/voicecommand/business/VoiceSetting;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    invoke-virtual {v1}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->updateCurLanguageToPref()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleSyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 2
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    packed-switch v1, :pswitch_data_0

    const/16 v0, 0x3ef

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceSetting;->sendProcessList(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceSetting;->updateProcessList(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceSetting;->updateProcessListAll(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceSetting;->sendLanguageList(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceSetting;->changeCurLanguage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceSetting;->sendKeyWordsPath(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method
