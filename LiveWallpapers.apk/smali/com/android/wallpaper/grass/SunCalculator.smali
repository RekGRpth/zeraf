.class Lcom/android/wallpaper/grass/SunCalculator;
.super Ljava/lang/Object;
.source "SunCalculator.java"


# static fields
.field static final ZENITH_ASTRONOMICAL:D = 108.0

.field static final ZENITH_CIVIL:D = 96.0

.field static final ZENITH_NAUTICAL:D = 102.0

.field static final ZENITH_OFFICIAL:D = 90.8333


# instance fields
.field private mLocation:Landroid/location/Location;

.field private mTimeZone:Ljava/util/TimeZone;


# direct methods
.method constructor <init>(Landroid/location/Location;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/location/Location;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/wallpaper/grass/SunCalculator;->mLocation:Landroid/location/Location;

    invoke-static {p2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/grass/SunCalculator;->mTimeZone:Ljava/util/TimeZone;

    return-void
.end method

.method private adjustForDST(DLjava/util/Calendar;)D
    .locals 6
    .param p1    # D
    .param p3    # Ljava/util/Calendar;

    const-wide/high16 v4, 0x4038000000000000L

    move-wide v0, p1

    iget-object v2, p0, Lcom/android/wallpaper/grass/SunCalculator;->mTimeZone:Ljava/util/TimeZone;

    invoke-virtual {p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/high16 v2, 0x3ff0000000000000L

    add-double/2addr v0, v2

    :cond_0
    cmpl-double v2, v0, v4

    if-lez v2, :cond_1

    sub-double/2addr v0, v4

    :cond_1
    return-wide v0
.end method

.method private computeSolarEventTime(DLjava/util/Calendar;Z)D
    .locals 17
    .param p1    # D
    .param p3    # Ljava/util/Calendar;
    .param p4    # Z

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/wallpaper/grass/SunCalculator;->mTimeZone:Ljava/util/TimeZone;

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v15}, Lcom/android/wallpaper/grass/SunCalculator;->getLongitudeHour(Ljava/util/Calendar;Ljava/lang/Boolean;)D

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/android/wallpaper/grass/SunCalculator;->getMeanAnomaly(D)D

    move-result-wide v13

    invoke-static {v13, v14}, Lcom/android/wallpaper/grass/SunCalculator;->getSunTrueLongitude(D)D

    move-result-wide v3

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/android/wallpaper/grass/SunCalculator;->getCosineSunLocalHour(DD)D

    move-result-wide v9

    const-wide/high16 v15, -0x4010000000000000L

    cmpg-double v15, v9, v15

    if-ltz v15, :cond_0

    const-wide/high16 v15, 0x3ff0000000000000L

    cmpl-double v15, v9, v15

    if-lez v15, :cond_1

    :cond_0
    const-wide/16 v15, 0x0

    :goto_0
    return-wide v15

    :cond_1
    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    invoke-static {v9, v10, v15}, Lcom/android/wallpaper/grass/SunCalculator;->getSunLocalHour(DLjava/lang/Boolean;)D

    move-result-wide v7

    invoke-static/range {v3 .. v8}, Lcom/android/wallpaper/grass/SunCalculator;->getLocalMeanTime(DDD)D

    move-result-wide v11

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v11, v12, v1}, Lcom/android/wallpaper/grass/SunCalculator;->getLocalTime(DLjava/util/Calendar;)D

    move-result-wide v15

    goto :goto_0
.end method

.method private getBaseLongitudeHour()D
    .locals 4

    iget-object v0, p0, Lcom/android/wallpaper/grass/SunCalculator;->mLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    const-wide/high16 v2, 0x402e000000000000L

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private static getCosineOfSunDeclination(D)D
    .locals 4
    .param p0    # D

    invoke-static {p0, p1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    return-wide v2
.end method

.method private getCosineSunLocalHour(DD)D
    .locals 23
    .param p1    # D
    .param p3    # D

    invoke-static/range {p1 .. p2}, Lcom/android/wallpaper/grass/SunCalculator;->getSinOfSunDeclination(D)D

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Lcom/android/wallpaper/grass/SunCalculator;->getCosineOfSunDeclination(D)D

    move-result-wide v3

    invoke-static/range {p3 .. p4}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/wallpaper/grass/SunCalculator;->mLocation:Landroid/location/Location;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/location/Location;->getLatitude()D

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v11

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    move-result-wide v15

    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    mul-double v13, v17, v15

    sub-double v7, v5, v13

    mul-double v9, v3, v1

    div-double v21, v7, v9

    return-wide v21
.end method

.method private static getDayOfYear(Ljava/util/Calendar;)D
    .locals 2
    .param p0    # Ljava/util/Calendar;

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-double v0, v0

    return-wide v0
.end method

.method private static getLocalMeanTime(DDD)D
    .locals 10
    .param p0    # D
    .param p2    # D
    .param p4    # D

    const-wide/high16 v8, 0x4038000000000000L

    invoke-static {p0, p1}, Lcom/android/wallpaper/grass/SunCalculator;->getRightAscension(D)D

    move-result-wide v4

    const-wide v6, 0x3fb0d25edd052935L

    mul-double v0, p2, v6

    add-double v6, p4, v4

    sub-double v2, v6, v0

    const-wide v6, 0x401a7ced916872b0L

    sub-double/2addr v2, v6

    const-wide/16 v6, 0x0

    cmpg-double v6, v2, v6

    if-gez v6, :cond_1

    add-double/2addr v2, v8

    :cond_0
    :goto_0
    return-wide v2

    :cond_1
    cmpl-double v6, v2, v8

    if-lez v6, :cond_0

    sub-double/2addr v2, v8

    goto :goto_0
.end method

.method private getLocalTime(DLjava/util/Calendar;)D
    .locals 8
    .param p1    # D
    .param p3    # Ljava/util/Calendar;

    invoke-direct {p0}, Lcom/android/wallpaper/grass/SunCalculator;->getBaseLongitudeHour()D

    move-result-wide v6

    sub-double v4, p1, v6

    invoke-static {p3}, Lcom/android/wallpaper/grass/SunCalculator;->getUTCOffSet(Ljava/util/Calendar;)D

    move-result-wide v0

    add-double v2, v4, v0

    invoke-direct {p0, v2, v3, p3}, Lcom/android/wallpaper/grass/SunCalculator;->adjustForDST(DLjava/util/Calendar;)D

    move-result-wide v6

    return-wide v6
.end method

.method private getLongitudeHour(Ljava/util/Calendar;Ljava/lang/Boolean;)D
    .locals 9
    .param p1    # Ljava/util/Calendar;
    .param p2    # Ljava/lang/Boolean;

    const/16 v4, 0x12

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x6

    :cond_0
    int-to-double v5, v4

    invoke-direct {p0}, Lcom/android/wallpaper/grass/SunCalculator;->getBaseLongitudeHour()D

    move-result-wide v7

    sub-double v2, v5, v7

    const-wide/high16 v5, 0x4038000000000000L

    div-double v0, v2, v5

    invoke-static {p1}, Lcom/android/wallpaper/grass/SunCalculator;->getDayOfYear(Ljava/util/Calendar;)D

    move-result-wide v5

    add-double/2addr v5, v0

    return-wide v5
.end method

.method private static getMeanAnomaly(D)D
    .locals 4
    .param p0    # D

    const-wide v0, 0x3fef8a0902de00d2L

    mul-double/2addr v0, p0

    const-wide v2, 0x400a4fdf3b645a1dL

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method private static getRightAscension(D)D
    .locals 18
    .param p0    # D

    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->tan(D)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v14

    const-wide v16, 0x3fed5d4e8fb00bccL

    mul-double v2, v14, v16

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->atan(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v8

    const-wide/16 v14, 0x0

    cmpg-double v14, v8, v14

    if-gez v14, :cond_1

    const-wide v14, 0x4076800000000000L

    add-double/2addr v8, v14

    :cond_0
    :goto_0
    const-wide v6, 0x4056800000000000L

    div-double v14, p0, v6

    double-to-int v14, v14

    int-to-double v4, v14

    mul-double/2addr v4, v6

    div-double v14, v8, v6

    double-to-int v14, v14

    int-to-double v10, v14

    mul-double/2addr v10, v6

    sub-double v0, v4, v10

    add-double v14, v8, v0

    const-wide/high16 v16, 0x402e000000000000L

    div-double v14, v14, v16

    return-wide v14

    :cond_1
    const-wide v14, 0x4076800000000000L

    cmpl-double v14, v8, v14

    if-lez v14, :cond_0

    const-wide v14, 0x4076800000000000L

    sub-double/2addr v8, v14

    goto :goto_0
.end method

.method private static getSinOfSunDeclination(D)D
    .locals 4
    .param p0    # D

    invoke-static {p0, p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    const-wide v2, 0x3fd975e2046c764bL

    mul-double/2addr v2, v0

    return-wide v2
.end method

.method private static getSunLocalHour(DLjava/lang/Boolean;)D
    .locals 6
    .param p0    # D
    .param p2    # Ljava/lang/Boolean;

    invoke-static {p0, p1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    const-wide v4, 0x4076800000000000L

    sub-double v2, v4, v2

    :cond_0
    const-wide/high16 v4, 0x402e000000000000L

    div-double v4, v2, v4

    return-wide v4
.end method

.method private static getSunTrueLongitude(D)D
    .locals 16
    .param p0    # D

    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    const-wide/high16 v12, 0x4000000000000000L

    mul-double/2addr v12, v2

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    const-wide v12, 0x3ffea7ef9db22d0eL

    mul-double/2addr v12, v8

    add-double v0, p0, v12

    const-wide v12, 0x3f947ae147ae147bL

    mul-double/2addr v12, v6

    const-wide v14, 0x4071aa24dd2f1aa0L

    add-double v4, v12, v14

    add-double v10, v0, v4

    const-wide v12, 0x4076800000000000L

    cmpl-double v12, v10, v12

    if-lez v12, :cond_0

    const-wide v12, 0x4076800000000000L

    sub-double/2addr v10, v12

    :cond_0
    return-wide v10
.end method

.method private static getUTCOffSet(Ljava/util/Calendar;)D
    .locals 3
    .param p0    # Ljava/util/Calendar;

    const/16 v1, 0xf

    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const v1, 0x36ee80

    div-int v1, v0, v1

    int-to-double v1, v1

    return-wide v1
.end method

.method public static timeToDayFraction(D)F
    .locals 6
    .param p0    # D

    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    int-to-double v2, v0

    sub-double v2, p0, v2

    const-wide/high16 v4, 0x404e000000000000L

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    const/16 v2, 0x3c

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    mul-int/lit8 v2, v0, 0x3c

    add-int/2addr v2, v1

    int-to-float v2, v2

    const/high16 v3, 0x44b40000

    div-float/2addr v2, v3

    return v2
.end method

.method public static timeToHours(D)I
    .locals 6
    .param p0    # D

    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    int-to-double v2, v0

    sub-double v2, p0, v2

    const-wide/high16 v4, 0x404e000000000000L

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    const/16 v2, 0x3c

    if-ne v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public static timeToMinutes(D)I
    .locals 6
    .param p0    # D

    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    int-to-double v2, v0

    sub-double v2, p0, v2

    const-wide/high16 v4, 0x404e000000000000L

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    const/16 v2, 0x3c

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    return v1
.end method

.method public static timeToString(D)Ljava/lang/String;
    .locals 7
    .param p0    # D

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-int v1, v3

    int-to-double v3, v1

    sub-double v3, p0, v3

    const-wide/high16 v5, 0x404e000000000000L

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v2, v3

    const/16 v3, 0x3c

    if-ne v2, v3, :cond_0

    const/4 v2, 0x0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const/16 v4, 0x3a

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    const/16 v3, 0xa

    if-ge v2, v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method public computeSunriseTime(DLjava/util/Calendar;)D
    .locals 2
    .param p1    # D
    .param p3    # Ljava/util/Calendar;

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/wallpaper/grass/SunCalculator;->computeSolarEventTime(DLjava/util/Calendar;Z)D

    move-result-wide v0

    return-wide v0
.end method

.method public computeSunsetTime(DLjava/util/Calendar;)D
    .locals 2
    .param p1    # D
    .param p3    # Ljava/util/Calendar;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/wallpaper/grass/SunCalculator;->computeSolarEventTime(DLjava/util/Calendar;Z)D

    move-result-wide v0

    return-wide v0
.end method

.method public setLocation(Landroid/location/Location;)V
    .locals 0
    .param p1    # Landroid/location/Location;

    iput-object p1, p0, Lcom/android/wallpaper/grass/SunCalculator;->mLocation:Landroid/location/Location;

    return-void
.end method
