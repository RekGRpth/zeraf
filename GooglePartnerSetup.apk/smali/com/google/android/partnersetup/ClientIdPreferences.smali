.class public final Lcom/google/android/partnersetup/ClientIdPreferences;
.super Ljava/lang/Object;
.source "ClientIdPreferences.java"


# static fields
.field public static final GSERVICES_CLIENTID_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    sget-object v0, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    const-string v1, "client_id"

    const-string v2, "clientid_search"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    const-string v1, "search_client_id"

    const-string v2, "clientid_search"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    const-string v1, "chrome_client_id"

    const-string v2, "clientid_search"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    const-string v1, "maps_client_id"

    const-string v2, "clientid_maps"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    const-string v1, "youtube_client_id"

    const-string v2, "clientid_youtube"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    const-string v1, "market_client_id"

    const-string v2, "clientid_market"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    const-string v1, "voicesearch_client_id"

    const-string v2, "clientid_voicesearch"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    const-string v1, "shopper_client_id"

    const-string v2, "clientid_shopper"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    const-string v1, "wallet_client_id"

    const-string v2, "clientid_wallet"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
