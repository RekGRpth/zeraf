.class final Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/rpc/RpcResultHandler;


# instance fields
.field private callback:Ljava/lang/Object;

.field private methodId:I

.field final synthetic this$0:Landroid/support/place/music/AudioCalibrator;


# direct methods
.method public constructor <init>(Landroid/support/place/music/AudioCalibrator;ILjava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;->this$0:Landroid/support/place/music/AudioCalibrator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;->methodId:I

    iput-object p3, p0, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;->callback:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getCalibrationOutput([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/AudioCalibrator$OnGetCalibrationOutput;

    invoke-interface {v0, v1}, Landroid/support/place/music/AudioCalibrator$OnGetCalibrationOutput;->onGetCalibrationOutput(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onResult([B)V
    .locals 1

    iget v0, p0, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;->methodId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p1}, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;->stopCalibration([B)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1}, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;->getCalibrationOutput([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final stopCalibration([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/AudioCalibrator$OnStopCalibration;

    invoke-interface {v0, v1}, Landroid/support/place/music/AudioCalibrator$OnStopCalibration;->onStopCalibration(Z)V

    :cond_0
    return-void
.end method
