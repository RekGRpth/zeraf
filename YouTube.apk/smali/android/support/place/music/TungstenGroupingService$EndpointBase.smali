.class public abstract Landroid/support/place/music/TungstenGroupingService$EndpointBase;
.super Landroid/support/place/connector/Connector;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/place/connector/Connector;-><init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V

    return-void
.end method


# virtual methods
.method public abstract adjustGroupVolume(Ljava/lang/String;ILandroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract assignRxToGroup(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract createGroup(Ljava/lang/String;Ljava/util/List;Landroid/support/place/rpc/RpcContext;)Landroid/support/place/music/TgsGroup;
.end method

.method public abstract deleteGroup(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract getGroupState(Landroid/support/place/rpc/RpcContext;)Landroid/support/place/music/TgsState;
.end method

.method public abstract getRxVolumes(Landroid/support/place/rpc/RpcContext;)Ljava/util/List;
.end method

.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "versionCheck"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "clientMinApiVersion"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v2

    const-string v3, "clientMaxApiVersion"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->versionCheck(IILandroid/support/place/rpc/RpcContext;)Landroid/support/place/music/TgsVersionCheck;

    move-result-object v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    const-string v2, "getGroupState"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->getGroupState(Landroid/support/place/rpc/RpcContext;)Landroid/support/place/music/TgsState;

    move-result-object v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    goto :goto_0

    :cond_2
    const-string v2, "createGroup"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "groupId"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "rxIds"

    const-class v4, Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/support/place/rpc/RpcData;->getList(Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->createGroup(Ljava/lang/String;Ljava/util/List;Landroid/support/place/rpc/RpcContext;)Landroid/support/place/music/TgsGroup;

    move-result-object v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    goto :goto_0

    :cond_3
    const-string v2, "assignRxToGroup"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "groupId"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "rxId"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->assignRxToGroup(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_4
    const-string v2, "removeRxFromGroup"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "rxId"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->removeRxFromGroup(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_5
    const-string v2, "deleteGroup"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "groupId"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->deleteGroup(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_6
    const-string v2, "getRxVolumes"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->getRxVolumes(Landroid/support/place/rpc/RpcContext;)Ljava/util/List;

    move-result-object v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putFlattenableList(Ljava/lang/String;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_7
    const-string v2, "setRxVolumes"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "rxVolumes"

    sget-object v3, Landroid/support/place/music/TgsRxVolume;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v1, v2, v3}, Landroid/support/place/rpc/RpcData;->getFlattenableList(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->setRxVolumes(Ljava/util/List;Landroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_8
    const-string v2, "adjustGroupVolume"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "groupId"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "steps"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->adjustGroupVolume(Ljava/lang/String;ILandroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_9
    const-string v2, "setIsPlaying"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "groupId"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "isPlaying"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->setIsPlaying(Ljava/lang/String;ZLandroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_a
    const-string v2, "setGroupTransmitterConfig"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "groupId"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "unicastConfig"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->setGroupTransmitterConfig(Ljava/lang/String;Landroid/support/place/rpc/RpcData;Landroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_b
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/Connector;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v0

    goto/16 :goto_1
.end method

.method public pushOnStateChanged(Landroid/support/place/music/TgsState;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    const-string v1, "onStateChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public pushOnVolumeChanged(Ljava/util/List;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "volumes"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putFlattenableList(Ljava/lang/String;Ljava/util/List;)V

    const-string v1, "onVolumeChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/music/TungstenGroupingService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public abstract removeRxFromGroup(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract setGroupTransmitterConfig(Ljava/lang/String;Landroid/support/place/rpc/RpcData;Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract setIsPlaying(Ljava/lang/String;ZLandroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract setRxVolumes(Ljava/util/List;Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract versionCheck(IILandroid/support/place/rpc/RpcContext;)Landroid/support/place/music/TgsVersionCheck;
.end method
