.class public Landroid/support/place/music/TungstenReceiverService;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private _broker:Landroid/support/place/connector/Broker;

.field private _endpoint:Landroid/support/place/rpc/EndpointInfo;

.field private _presenter:Landroid/support/place/music/TungstenReceiverService$Presenter;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iput-object p2, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-void
.end method


# virtual methods
.method public adjustMasterVolume(ILandroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "steps"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "adjustMasterVolume"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-object v0
.end method

.method public getFixedVolumeLevel(Ljava/lang/String;Landroid/support/place/music/TungstenReceiverService$OnGetFixedVolumeLevel;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "output"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getFixedVolumeLevel"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/16 v5, 0x14

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getFixedVolumeOut(Ljava/lang/String;Landroid/support/place/music/TungstenReceiverService$OnGetFixedVolumeOut;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "output"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getFixedVolumeOut"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/16 v5, 0xb

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getMasterMute(Landroid/support/place/music/TungstenReceiverService$OnGetMasterMute;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getMasterMute"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/4 v5, 0x7

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getMasterVolume(Landroid/support/place/music/TungstenReceiverService$OnGetMasterVolume;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getMasterVolume"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/4 v5, 0x6

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getSyncDelay(Ljava/lang/String;Landroid/support/place/music/TungstenReceiverService$OnGetSyncDelay;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "output"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getSyncDelay"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/16 v5, 0xe

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public isOutputEnabled(Ljava/lang/String;Landroid/support/place/music/TungstenReceiverService$OnIsOutputEnabled;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "output"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "isOutputEnabled"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/16 v5, 0x10

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public reset(Landroid/support/place/music/TungstenReceiverService$OnReset;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "reset"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/4 v5, 0x1

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setEndpoint(Ljava/lang/String;ILandroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "address"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "port"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setEndpoint"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setFixedVolumeLevel(FLjava/lang/String;Landroid/support/place/music/TungstenReceiverService$OnSetFixedVolumeLevel;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "value"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    const-string v0, "output"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setFixedVolumeLevel"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/16 v5, 0x13

    invoke-direct {v4, p0, v5, p3}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setFixedVolumeOut(ZLjava/lang/String;Landroid/support/place/music/TungstenReceiverService$OnSetFixedVolumeOut;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "fixed_vout"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "output"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setFixedVolumeOut"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/16 v5, 0xa

    invoke-direct {v4, p0, v5, p3}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setMasterVolume(FZLandroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "volume"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    const-string v0, "mute"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setMasterVolume"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setOutputEnabled(ZLjava/lang/String;Landroid/support/place/music/TungstenReceiverService$OnSetOutputEnabled;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "enabled"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "output"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setOutputEnabled"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/16 v5, 0x11

    invoke-direct {v4, p0, v5, p3}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setSyncDelay(FLjava/lang/String;Landroid/support/place/music/TungstenReceiverService$OnSetSyncDelay;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "sync_delay"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    const-string v0, "output"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setSyncDelay"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/16 v5, 0xd

    invoke-direct {v4, p0, v5, p3}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public start(Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "start"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public startListening(Landroid/support/place/music/TungstenReceiverService$Listener;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/place/music/TungstenReceiverService;->stopListening()V

    new-instance v0, Landroid/support/place/music/TungstenReceiverService$Presenter;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    invoke-direct {v0, p0, v1, p1}, Landroid/support/place/music/TungstenReceiverService$Presenter;-><init>(Landroid/support/place/music/TungstenReceiverService;Landroid/support/place/connector/Broker;Landroid/support/place/music/TungstenReceiverService$Listener;)V

    iput-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_presenter:Landroid/support/place/music/TungstenReceiverService$Presenter;

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_presenter:Landroid/support/place/music/TungstenReceiverService$Presenter;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, v1}, Landroid/support/place/music/TungstenReceiverService$Presenter;->startListening(Landroid/support/place/rpc/EndpointInfo;)V

    return-void
.end method

.method public stop(Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "stop"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public stopListening()V
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_presenter:Landroid/support/place/music/TungstenReceiverService$Presenter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_presenter:Landroid/support/place/music/TungstenReceiverService$Presenter;

    invoke-virtual {v0}, Landroid/support/place/music/TungstenReceiverService$Presenter;->stopListening()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_presenter:Landroid/support/place/music/TungstenReceiverService$Presenter;

    :cond_0
    return-void
.end method

.method public versionCheck(IILandroid/support/place/music/TungstenReceiverService$OnVersionCheck;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "clientMinApiVersion"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "clientMaxApiVersion"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "versionCheck"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5, p3}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;-><init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method
