.class public Landroid/support/place/music/AudioCalibrator;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private _broker:Landroid/support/place/connector/Broker;

.field private _endpoint:Landroid/support/place/rpc/EndpointInfo;

.field private _presenter:Landroid/support/place/music/AudioCalibrator$Presenter;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/music/AudioCalibrator;->_broker:Landroid/support/place/connector/Broker;

    iput-object p2, p0, Landroid/support/place/music/AudioCalibrator;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-void
.end method


# virtual methods
.method public getCalibrationOutput(Landroid/support/place/music/AudioCalibrator$OnGetCalibrationOutput;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/AudioCalibrator;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getCalibrationOutput"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;

    const/4 v5, 0x2

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;-><init>(Landroid/support/place/music/AudioCalibrator;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-object v0
.end method

.method public setTickTrack(ZIZZJLandroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "enabled"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "period"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "enableLeft"

    invoke-virtual {v3, v0, p3}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "enableRight"

    invoke-virtual {v3, v0, p4}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "pulseWidth"

    invoke-virtual {v3, v0, p5, p6}, Landroid/support/place/rpc/RpcData;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/AudioCalibrator;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setTickTrack"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public startCalibration(Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "output"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/AudioCalibrator;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "startCalibration"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public startListening(Landroid/support/place/music/AudioCalibrator$Listener;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/place/music/AudioCalibrator;->stopListening()V

    new-instance v0, Landroid/support/place/music/AudioCalibrator$Presenter;

    iget-object v1, p0, Landroid/support/place/music/AudioCalibrator;->_broker:Landroid/support/place/connector/Broker;

    invoke-direct {v0, p0, v1, p1}, Landroid/support/place/music/AudioCalibrator$Presenter;-><init>(Landroid/support/place/music/AudioCalibrator;Landroid/support/place/connector/Broker;Landroid/support/place/music/AudioCalibrator$Listener;)V

    iput-object v0, p0, Landroid/support/place/music/AudioCalibrator;->_presenter:Landroid/support/place/music/AudioCalibrator$Presenter;

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator;->_presenter:Landroid/support/place/music/AudioCalibrator$Presenter;

    iget-object v1, p0, Landroid/support/place/music/AudioCalibrator;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, v1}, Landroid/support/place/music/AudioCalibrator$Presenter;->startListening(Landroid/support/place/rpc/EndpointInfo;)V

    return-void
.end method

.method public stopCalibration(FLandroid/support/place/music/AudioCalibrator$OnStopCalibration;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "compensationValue"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/music/AudioCalibrator;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "stopCalibration"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;

    const/4 v5, 0x1

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/music/AudioCalibrator$_ResultDispatcher;-><init>(Landroid/support/place/music/AudioCalibrator;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public stopListening()V
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator;->_presenter:Landroid/support/place/music/AudioCalibrator$Presenter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/AudioCalibrator;->_presenter:Landroid/support/place/music/AudioCalibrator$Presenter;

    invoke-virtual {v0}, Landroid/support/place/music/AudioCalibrator$Presenter;->stopListening()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/music/AudioCalibrator;->_presenter:Landroid/support/place/music/AudioCalibrator$Presenter;

    :cond_0
    return-void
.end method
