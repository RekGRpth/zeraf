.class public abstract Landroid/support/place/music/TungstenReceiverService$EndpointBase;
.super Landroid/support/place/connector/Connector;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/place/connector/Connector;-><init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V

    return-void
.end method


# virtual methods
.method public abstract adjustMasterVolume(ILandroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract getFixedVolumeLevel(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)F
.end method

.method public abstract getFixedVolumeOut(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract getMasterMute(Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract getMasterVolume(Landroid/support/place/rpc/RpcContext;)F
.end method

.method public abstract getSyncDelay(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)F
.end method

.method public abstract isOutputEnabled(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "versionCheck"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "clientMinApiVersion"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v2

    const-string v3, "clientMaxApiVersion"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->versionCheck(IILandroid/support/place/rpc/RpcContext;)Landroid/support/place/music/TgsVersionCheck;

    move-result-object v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    const-string v2, "reset"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->reset(Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const-string v2, "setEndpoint"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "address"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "port"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->setEndpoint(Ljava/lang/String;ILandroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto :goto_0

    :cond_3
    const-string v2, "start"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->start(Landroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto :goto_0

    :cond_4
    const-string v2, "stop"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->stop(Landroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto :goto_0

    :cond_5
    const-string v2, "setMasterVolume"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "volume"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v2

    const-string v3, "mute"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->setMasterVolume(FZLandroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto :goto_0

    :cond_6
    const-string v2, "getMasterVolume"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->getMasterVolume(Landroid/support/place/rpc/RpcContext;)F

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    goto :goto_0

    :cond_7
    const-string v2, "getMasterMute"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->getMasterMute(Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_8
    const-string v2, "adjustMasterVolume"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "steps"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->adjustMasterVolume(ILandroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_9
    const-string v2, "setFixedVolumeOut"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "fixed_vout"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "output"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->setFixedVolumeOut(ZLjava/lang/String;Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_a
    const-string v2, "getFixedVolumeOut"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "output"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->getFixedVolumeOut(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_b
    const-string v2, "setSyncDelay"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "sync_delay"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v2

    const-string v3, "output"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->setSyncDelay(FLjava/lang/String;Landroid/support/place/rpc/RpcContext;)F

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    goto/16 :goto_0

    :cond_c
    const-string v2, "getSyncDelay"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v2, "output"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->getSyncDelay(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)F

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    goto/16 :goto_0

    :cond_d
    const-string v2, "isOutputEnabled"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "output"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->isOutputEnabled(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_e
    const-string v2, "setOutputEnabled"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    const-string v2, "enabled"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "output"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->setOutputEnabled(ZLjava/lang/String;Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_f
    const-string v2, "setFixedVolumeLevel"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string v2, "value"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v2

    const-string v3, "output"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->setFixedVolumeLevel(FLjava/lang/String;Landroid/support/place/rpc/RpcContext;)F

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    goto/16 :goto_0

    :cond_10
    const-string v2, "getFixedVolumeLevel"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    const-string v2, "output"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->getFixedVolumeLevel(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)F

    move-result v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    goto/16 :goto_0

    :cond_11
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/Connector;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v0

    goto/16 :goto_1
.end method

.method public pushOnFixedVolumeLevelChanged(FLjava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "value"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    const-string v1, "output"

    invoke-virtual {v0, v1, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "onFixedVolumeLevelChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public pushOnFixedVolumeOutChanged(ZLjava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "fixed_vout"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "output"

    invoke-virtual {v0, v1, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "onFixedVolumeOutChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public pushOnMasterVolumeChanged(FZ)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "volume"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    const-string v1, "mute"

    invoke-virtual {v0, v1, p2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "onMasterVolumeChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public pushOnOutputEnabled(ZLjava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "enabled"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "output"

    invoke-virtual {v0, v1, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "onOutputEnabled"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public pushOnSyncDelayChanged(FLjava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "sync_delay"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    const-string v1, "output"

    invoke-virtual {v0, v1, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "onSyncDelayChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/music/TungstenReceiverService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public abstract reset(Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract setEndpoint(Ljava/lang/String;ILandroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract setFixedVolumeLevel(FLjava/lang/String;Landroid/support/place/rpc/RpcContext;)F
.end method

.method public abstract setFixedVolumeOut(ZLjava/lang/String;Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract setMasterVolume(FZLandroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract setOutputEnabled(ZLjava/lang/String;Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract setSyncDelay(FLjava/lang/String;Landroid/support/place/rpc/RpcContext;)F
.end method

.method public abstract start(Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract stop(Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract versionCheck(IILandroid/support/place/rpc/RpcContext;)Landroid/support/place/music/TgsVersionCheck;
.end method
