.class Landroid/support/place/music/TungstenReceiverService$Presenter;
.super Landroid/support/place/connector/EventListener;
.source "SourceFile"


# instance fields
.field private _listener:Landroid/support/place/music/TungstenReceiverService$Listener;

.field final synthetic this$0:Landroid/support/place/music/TungstenReceiverService;


# direct methods
.method public constructor <init>(Landroid/support/place/music/TungstenReceiverService;Landroid/support/place/connector/Broker;Landroid/support/place/music/TungstenReceiverService$Listener;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/music/TungstenReceiverService$Presenter;->this$0:Landroid/support/place/music/TungstenReceiverService;

    invoke-direct {p0, p2, p3}, Landroid/support/place/connector/EventListener;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/connector/EventListener$Listener;)V

    iput-object p3, p0, Landroid/support/place/music/TungstenReceiverService$Presenter;->_listener:Landroid/support/place/music/TungstenReceiverService$Listener;

    return-void
.end method


# virtual methods
.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "onMasterVolumeChanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "volume"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v1

    const-string v2, "mute"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iget-object v2, p0, Landroid/support/place/music/TungstenReceiverService$Presenter;->_listener:Landroid/support/place/music/TungstenReceiverService$Listener;

    invoke-virtual {v2, v1, v0, p3}, Landroid/support/place/music/TungstenReceiverService$Listener;->onMasterVolumeChanged(FZLandroid/support/place/rpc/RpcContext;)V

    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    const-string v1, "onFixedVolumeOutChanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "fixed_vout"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "output"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Landroid/support/place/music/TungstenReceiverService$Presenter;->_listener:Landroid/support/place/music/TungstenReceiverService$Listener;

    invoke-virtual {v2, v1, v0, p3}, Landroid/support/place/music/TungstenReceiverService$Listener;->onFixedVolumeOutChanged(ZLjava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_1
    const-string v1, "onSyncDelayChanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "sync_delay"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v1

    const-string v2, "output"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Landroid/support/place/music/TungstenReceiverService$Presenter;->_listener:Landroid/support/place/music/TungstenReceiverService$Listener;

    invoke-virtual {v2, v1, v0, p3}, Landroid/support/place/music/TungstenReceiverService$Listener;->onSyncDelayChanged(FLjava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_2
    const-string v1, "onOutputEnabled"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "enabled"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "output"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Landroid/support/place/music/TungstenReceiverService$Presenter;->_listener:Landroid/support/place/music/TungstenReceiverService$Listener;

    invoke-virtual {v2, v1, v0, p3}, Landroid/support/place/music/TungstenReceiverService$Listener;->onOutputEnabled(ZLjava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_3
    const-string v1, "onFixedVolumeLevelChanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "value"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v1

    const-string v2, "output"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Landroid/support/place/music/TungstenReceiverService$Presenter;->_listener:Landroid/support/place/music/TungstenReceiverService$Listener;

    invoke-virtual {v2, v1, v0, p3}, Landroid/support/place/music/TungstenReceiverService$Listener;->onFixedVolumeLevelChanged(FLjava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_4
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/EventListener;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v0

    goto :goto_1
.end method
