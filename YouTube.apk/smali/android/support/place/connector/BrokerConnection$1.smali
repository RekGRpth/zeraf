.class Landroid/support/place/connector/BrokerConnection$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic this$0:Landroid/support/place/connector/BrokerConnection;


# direct methods
.method constructor <init>(Landroid/support/place/connector/BrokerConnection;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    invoke-virtual {v0, p2}, Landroid/support/place/connector/BrokerConnection;->getBrokerService(Landroid/os/IBinder;)Landroid/support/place/connector/IBrokerService;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    new-instance v2, Landroid/support/place/connector/BrokerConnection$Callback;

    iget-object v3, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    invoke-direct {v2, v3}, Landroid/support/place/connector/BrokerConnection$Callback;-><init>(Landroid/support/place/connector/BrokerConnection;)V

    iput-object v2, v1, Landroid/support/place/connector/BrokerConnection;->mBrokerConnection:Landroid/support/place/connector/IBrokerConnection$Stub;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BrokerConnection(pid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " context="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    iget-object v2, v2, Landroid/support/place/connector/BrokerConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    iget-object v2, v2, Landroid/support/place/connector/BrokerConnection;->mBrokerConnection:Landroid/support/place/connector/IBrokerConnection$Stub;

    invoke-interface {v0, v2, v1}, Landroid/support/place/connector/IBrokerService;->registerCallback(Landroid/support/place/connector/IBrokerConnection;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    iput-object v0, v1, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection$1;->this$0:Landroid/support/place/connector/BrokerConnection;

    # invokes: Landroid/support/place/connector/BrokerConnection;->handleBrokerServiceDisconnected()V
    invoke-static {v0}, Landroid/support/place/connector/BrokerConnection;->access$100(Landroid/support/place/connector/BrokerConnection;)V

    return-void
.end method
