.class final Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/rpc/RpcResultHandler;


# instance fields
.field private callback:Ljava/lang/Object;

.field private methodId:I

.field final synthetic this$0:Landroid/support/place/connector/coordinator/Coordinator;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/coordinator/Coordinator;ILjava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->this$0:Landroid/support/place/connector/coordinator/Coordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->methodId:I

    iput-object p3, p0, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->callback:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getPlaceName([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/coordinator/Coordinator$OnGetPlaceName;

    invoke-interface {v0, v1}, Landroid/support/place/connector/coordinator/Coordinator$OnGetPlaceName;->onGetPlaceName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onResult([B)V
    .locals 1

    iget v0, p0, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->methodId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p1}, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->versionCheck([B)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1}, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->setPlaceName([B)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1}, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->getPlaceName([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setPlaceName([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/coordinator/Coordinator$OnSetPlaceName;

    invoke-interface {v0, v1}, Landroid/support/place/connector/coordinator/Coordinator$OnSetPlaceName;->onSetPlaceName(Z)V

    :cond_0
    return-void
.end method

.method public final versionCheck([B)V
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    sget-object v2, Landroid/support/place/connector/coordinator/VersionCheck;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/coordinator/VersionCheck;

    iget-object v1, p0, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/connector/coordinator/Coordinator$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v1, Landroid/support/place/connector/coordinator/Coordinator$OnVersionCheck;

    invoke-interface {v1, v0}, Landroid/support/place/connector/coordinator/Coordinator$OnVersionCheck;->onVersionCheck(Landroid/support/place/connector/coordinator/VersionCheck;)V

    :cond_0
    return-void
.end method
