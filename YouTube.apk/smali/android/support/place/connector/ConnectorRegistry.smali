.class public Landroid/support/place/connector/ConnectorRegistry;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String; = "aah.ConnectorRegistry"


# instance fields
.field mBroker:Landroid/support/place/connector/Broker;

.field private final mHandler:Landroid/os/Handler;

.field mListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

.field mRegistry:Landroid/support/place/connector/ConnectorRegistryRpc;

.field mRpcListener:Landroid/support/place/connector/ConnectorRegistryRpc$Listener;


# direct methods
.method constructor <init>(Landroid/support/place/connector/Broker;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mHandler:Landroid/os/Handler;

    new-instance v0, Landroid/support/place/connector/ConnectorRegistry$2;

    invoke-direct {v0, p0}, Landroid/support/place/connector/ConnectorRegistry$2;-><init>(Landroid/support/place/connector/ConnectorRegistry;)V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mRpcListener:Landroid/support/place/connector/ConnectorRegistryRpc$Listener;

    iput-object p1, p0, Landroid/support/place/connector/ConnectorRegistry;->mBroker:Landroid/support/place/connector/Broker;

    new-instance v0, Landroid/support/place/connector/ConnectorRegistryRpc;

    new-instance v1, Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "_registry"

    invoke-virtual {p1}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/place/connector/PlaceInfo;->getMaster()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/place/connector/PlaceInfo;->getMaster()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/place/rpc/EndpointInfo;->getPort()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Landroid/support/place/rpc/EndpointInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {v0, p1, v1}, Landroid/support/place/connector/ConnectorRegistryRpc;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mRegistry:Landroid/support/place/connector/ConnectorRegistryRpc;

    return-void
.end method

.method static synthetic access$000(Landroid/support/place/connector/ConnectorRegistry;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public listConnectors(Ljava/lang/String;Landroid/support/place/connector/ConnectorRegistry$OnListConnectors;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 2

    if-nez p1, :cond_0

    const-string p1, "---"

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mRegistry:Landroid/support/place/connector/ConnectorRegistryRpc;

    new-instance v1, Landroid/support/place/connector/ConnectorRegistry$1;

    invoke-direct {v1, p0, p2}, Landroid/support/place/connector/ConnectorRegistry$1;-><init>(Landroid/support/place/connector/ConnectorRegistry;Landroid/support/place/connector/ConnectorRegistry$OnListConnectors;)V

    invoke-virtual {v0, p1, v1, p3}, Landroid/support/place/connector/ConnectorRegistryRpc;->listConnectors(Ljava/lang/String;Landroid/support/place/connector/ConnectorRegistryRpc$OnListConnectors;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public startListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    if-ne v0, p1, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "ConnectorRegistry already has a listener"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    iput-object p1, p0, Landroid/support/place/connector/ConnectorRegistry;->mListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    iget-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mRegistry:Landroid/support/place/connector/ConnectorRegistryRpc;

    iget-object v1, p0, Landroid/support/place/connector/ConnectorRegistry;->mRpcListener:Landroid/support/place/connector/ConnectorRegistryRpc$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorRegistryRpc;->startListening(Landroid/support/place/connector/ConnectorRegistryRpc$Listener;)V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public stopListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mRegistry:Landroid/support/place/connector/ConnectorRegistryRpc;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorRegistryRpc;->stopListening()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method stopRegistryListener()V
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/ConnectorRegistry;->mRegistry:Landroid/support/place/connector/ConnectorRegistryRpc;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorRegistryRpc;->stopListening()V

    return-void
.end method
