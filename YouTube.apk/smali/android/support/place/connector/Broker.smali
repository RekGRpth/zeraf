.class public Landroid/support/place/connector/Broker;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final DEBUG:Z = true

.field public static final FLAG_RPC_ONEWAY:I = 0x1

.field public static final FLAG_RPC_SIGNED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "aah.Broker"


# instance fields
.field mBrokerConnector:Landroid/support/place/rpc/EndpointInfo;

.field mConnection:Landroid/support/place/connector/IBrokerConnection;

.field private mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

.field final mContainer:Landroid/support/place/connector/ConnectorContainer;

.field private mCoordinator:Landroid/support/place/connector/coordinator/Coordinator;

.field private final mHandler:Landroid/os/Handler;

.field mPlace:Landroid/support/place/connector/PlaceInfo;

.field private mSecurityService:Landroid/support/place/connector/security/SecurityService;

.field mService:Landroid/support/place/connector/IBrokerService;


# direct methods
.method constructor <init>(Landroid/support/place/connector/IBrokerConnection;Landroid/support/place/connector/ConnectorContainer;Landroid/support/place/connector/IBrokerService;Landroid/support/place/rpc/EndpointInfo;Landroid/support/place/rpc/EndpointInfo;Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    iput-object p2, p0, Landroid/support/place/connector/Broker;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    iput-object p3, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    iput-object p4, p0, Landroid/support/place/connector/Broker;->mBrokerConnector:Landroid/support/place/rpc/EndpointInfo;

    iput-object p6, p0, Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/support/place/connector/IBrokerService;Landroid/support/place/rpc/EndpointInfo;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/place/connector/Broker$FakeBrokerConnection;

    invoke-direct {v0, v1}, Landroid/support/place/connector/Broker$FakeBrokerConnection;-><init>(Landroid/support/place/connector/Broker$1;)V

    iput-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    iput-object v1, p0, Landroid/support/place/connector/Broker;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    iput-object p1, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    iput-object p2, p0, Landroid/support/place/connector/Broker;->mBrokerConnector:Landroid/support/place/rpc/EndpointInfo;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Landroid/support/place/connector/Broker;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public addPreferredPlace(Landroid/support/place/connector/PlaceInfo;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-interface {v0, p1}, Landroid/support/place/connector/IBrokerService;->addPreferredPlace(Landroid/support/place/connector/PlaceInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error adding preferred place: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public checkCallingPermission(Landroid/support/place/rpc/RpcContext;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v1, :cond_1

    const-string v1, "aah.Broker"

    const-string v2, "checkCallingPermission: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-virtual {p1}, Landroid/support/place/rpc/RpcContext;->getCertificate()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, p2, v3, p3}, Landroid/support/place/connector/IBrokerService;->hasPermission(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public enforceCallingPermission(Landroid/support/place/rpc/RpcContext;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0, p1, p2, p3}, Landroid/support/place/connector/Broker;->checkCallingPermission(Landroid/support/place/rpc/RpcContext;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incoming request not authorized for serviceType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and permission: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public enforceCallingPermission(Landroid/support/place/rpc/RpcContext;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    if-nez p3, :cond_1

    :cond_0
    return-void

    :cond_1
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    aget-object v2, p3, v0

    invoke-virtual {p0, p1, p2, v2}, Landroid/support/place/connector/Broker;->checkCallingPermission(Landroid/support/place/rpc/RpcContext;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incoming request not authorized for serviceType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and permissions: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCertificate()Ljava/security/cert/Certificate;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v1, :cond_1

    const-string v1, "aah.Broker"

    const-string v2, "getCertificate: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    iget-object v1, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-interface {v1}, Landroid/support/place/connector/IBrokerService;->getCertificate()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "X.509"

    invoke-static {v2}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v2

    invoke-static {v1}, Landroid/support/place/utils/Base64Utils;->decode(Ljava/lang/String;)[B

    move-result-object v1

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v2, v3}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "aah.Broker"

    const-string v2, "Error decoding certificate."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "aah.Broker"

    const-string v3, "Remote exception"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v2, "aah.Broker"

    const-string v3, "Something really wrong happened duringcertificate decoding.\n"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getConnectorRegistry()Landroid/support/place/connector/ConnectorRegistry;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    return-object v0
.end method

.method public getCoordinator()Landroid/support/place/connector/coordinator/Coordinator;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mCoordinator:Landroid/support/place/connector/coordinator/Coordinator;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getPlace()Landroid/support/place/connector/PlaceInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mPlace:Landroid/support/place/connector/PlaceInfo;

    return-object v0
.end method

.method public getPreferredPlaces()Ljava/util/List;
    .locals 3

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_0

    const-string v0, "aah.Broker"

    const-string v1, "getPreferredPlaces: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-interface {v0}, Landroid/support/place/connector/IBrokerService;->getPreferredPlaces()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "Error communicating with BrokerService"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getSecurityService()Landroid/support/place/connector/security/SecurityService;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mSecurityService:Landroid/support/place/connector/security/SecurityService;

    return-object v0
.end method

.method public getService()Landroid/support/place/connector/IBrokerService;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    return-object v0
.end method

.method public getUserData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/support/place/rpc/RpcData;
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/support/place/connector/IBrokerService;->getUserData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/support/place/rpc/RpcData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "Error communicating with BrokerService"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newConnectorInfo(Landroid/support/place/connector/Endpoint;)Landroid/support/place/connector/ConnectorInfo;
    .locals 3

    new-instance v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {p0}, Landroid/support/place/connector/Broker;->newEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/place/connector/ConnectorInfo;-><init>(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;)V

    return-object v0
.end method

.method public newEndpointInfo()Landroid/support/place/rpc/EndpointInfo;
    .locals 4

    new-instance v0, Landroid/support/place/rpc/EndpointInfo;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/connector/Broker;->mBrokerConnector:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v2}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Landroid/support/place/connector/Broker;->mBrokerConnector:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v3}, Landroid/support/place/rpc/EndpointInfo;->getPort()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Landroid/support/place/rpc/EndpointInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public registerConnector(Landroid/support/place/connector/Endpoint;Landroid/support/place/connector/ConnectorInfo;)V
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "registerConnector only works for Brokers created in Containers"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_1

    const-string v0, "aah.Broker"

    const-string v1, "registerConnector: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/support/place/connector/BrokerDeadException;

    const-string v1, "Broker Disconnected"

    invoke-direct {v0, v1}, Landroid/support/place/connector/BrokerDeadException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->onStart()V

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->getIEndpoint()Landroid/support/place/rpc/IEndpointStub;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/support/place/connector/IBrokerService;->registerConnector(Landroid/support/place/rpc/IEndpointStub;Landroid/support/place/connector/ConnectorInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/support/place/connector/BrokerDeadException;

    invoke-direct {v1, v0}, Landroid/support/place/connector/BrokerDeadException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public registerEndpoint(Landroid/support/place/connector/Endpoint;Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/rpc/EndpointInfo;
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_0

    const-string v0, "aah.Broker"

    const-string v1, "registerEndpoint: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/support/place/connector/BrokerDeadException;

    const-string v1, "Broker Disconnected"

    invoke-direct {v0, v1}, Landroid/support/place/connector/BrokerDeadException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->onStart()V

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->getIEndpoint()Landroid/support/place/rpc/IEndpointStub;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/support/place/connector/IBrokerService;->registerEndpoint(Landroid/support/place/rpc/IEndpointStub;Landroid/support/place/rpc/EndpointInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p2

    :catch_0
    move-exception v0

    new-instance v1, Landroid/support/place/connector/BrokerDeadException;

    invoke-direct {v1, v0}, Landroid/support/place/connector/BrokerDeadException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public removePreferredPlace(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_0

    const-string v0, "aah.Broker"

    const-string v1, "removePreferredPlace: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-interface {v0, p1}, Landroid/support/place/connector/IBrokerService;->removePreferredPlace(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error removing preferred place: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public saveUserData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V
    .locals 6

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Landroid/support/place/connector/IBrokerService;->saveUserData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "Error communicating with BrokerService"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;I)V

    return-void
.end method

.method public sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;I)V
    .locals 7

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_1

    const-string v0, "aah.Broker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendRpc "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/support/place/rpc/RpcError;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcError;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Landroid/support/place/rpc/RpcError;->status:I

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcError;->appendStackTrace(Ljava/lang/Throwable;)V

    invoke-interface {p5, v0}, Landroid/support/place/rpc/RpcErrorHandler;->onError(Landroid/support/place/rpc/RpcError;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v4, 0x0

    if-nez p4, :cond_2

    if-eqz p5, :cond_3

    :cond_2
    new-instance v4, Landroid/support/place/connector/Broker$1;

    invoke-direct {v4, p0, p4, p5}, Landroid/support/place/connector/Broker$1;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    :cond_3
    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    iget-object v5, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    move-object v1, p2

    move-object v2, p1

    move-object v3, p3

    move v6, p6

    invoke-interface/range {v0 .. v6}, Landroid/support/place/connector/IBrokerService;->sendRequest(Ljava/lang/String;Landroid/support/place/rpc/EndpointInfo;[BLandroid/support/place/rpc/IRpcCallback;Landroid/support/place/connector/IBrokerConnection;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    if-eqz p5, :cond_0

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mHandler:Landroid/os/Handler;

    new-instance v2, Landroid/support/place/connector/Broker$2;

    invoke-direct {v2, p0, p5, v0}, Landroid/support/place/connector/Broker$2;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/RpcErrorHandler;Ljava/lang/Exception;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setDefaultAccount(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-interface {v0, p1, p2}, Landroid/support/place/connector/IBrokerService;->setDefaultAccount(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "Error communicating with BrokerService"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method setPlace(Landroid/support/place/connector/PlaceInfo;)V
    .locals 5

    const/4 v1, 0x0

    if-nez p1, :cond_2

    iput-object v1, p0, Landroid/support/place/connector/Broker;->mPlace:Landroid/support/place/connector/PlaceInfo;

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorRegistry;->stopRegistryListener()V

    :cond_0
    iput-object v1, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iput-object v1, p0, Landroid/support/place/connector/Broker;->mSecurityService:Landroid/support/place/connector/security/SecurityService;

    iput-object v1, p0, Landroid/support/place/connector/Broker;->mCoordinator:Landroid/support/place/connector/coordinator/Coordinator;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mPlace:Landroid/support/place/connector/PlaceInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mPlace:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v0, p1}, Landroid/support/place/connector/PlaceInfo;->hasSameMaster(Landroid/support/place/connector/PlaceInfo;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    const/4 v0, 0x1

    :goto_1
    iput-object p1, p0, Landroid/support/place/connector/Broker;->mPlace:Landroid/support/place/connector/PlaceInfo;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->getMaster()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->getMaster()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/rpc/EndpointInfo;->getPort()I

    move-result v1

    iget-object v2, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    if-eqz v2, :cond_4

    iget-object v2, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    invoke-virtual {v2}, Landroid/support/place/connector/ConnectorRegistry;->stopRegistryListener()V

    :cond_4
    new-instance v2, Landroid/support/place/connector/ConnectorRegistry;

    invoke-direct {v2, p0}, Landroid/support/place/connector/ConnectorRegistry;-><init>(Landroid/support/place/connector/Broker;)V

    iput-object v2, p0, Landroid/support/place/connector/Broker;->mConnectorRegistry:Landroid/support/place/connector/ConnectorRegistry;

    new-instance v2, Landroid/support/place/connector/security/SecurityService;

    new-instance v3, Landroid/support/place/rpc/EndpointInfo;

    const-string v4, "_authService"

    invoke-direct {v3, v4, v0, v1}, Landroid/support/place/rpc/EndpointInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {v2, p0, v3}, Landroid/support/place/connector/security/SecurityService;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V

    iput-object v2, p0, Landroid/support/place/connector/Broker;->mSecurityService:Landroid/support/place/connector/security/SecurityService;

    new-instance v2, Landroid/support/place/connector/coordinator/Coordinator;

    new-instance v3, Landroid/support/place/rpc/EndpointInfo;

    const-string v4, "_coordinator"

    invoke-direct {v3, v4, v0, v1}, Landroid/support/place/rpc/EndpointInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {v2, p0, v3}, Landroid/support/place/connector/coordinator/Coordinator;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V

    iput-object v2, p0, Landroid/support/place/connector/Broker;->mCoordinator:Landroid/support/place/connector/coordinator/Coordinator;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setPlaceId(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_0

    const-string v0, "aah.Broker"

    const-string v1, "setPlaceId: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-interface {v0, p1}, Landroid/support/place/connector/IBrokerService;->joinPlace(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "Error communicating with BrokerService"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public startListeningForPlaces(Landroid/support/place/connector/Broker$PlaceListener;)Ljava/util/List;
    .locals 3

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_0

    const-string v0, "aah.Broker"

    const-string v1, "startListeningForPlaces: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    iget-object v2, p1, Landroid/support/place/connector/Broker$PlaceListener;->binder:Landroid/support/place/connector/IPlaceListener;

    invoke-interface {v0, v1, v2}, Landroid/support/place/connector/IBrokerService;->startListeningForPlaces(Landroid/support/place/connector/IBrokerConnection;Landroid/support/place/connector/IPlaceListener;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "Error communicating with BrokerService"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public stopListeningForPlaces(Landroid/support/place/connector/Broker$PlaceListener;)V
    .locals 3

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_0

    const-string v0, "aah.Broker"

    const-string v1, "stopListeningForPlaces: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    iget-object v2, p1, Landroid/support/place/connector/Broker$PlaceListener;->binder:Landroid/support/place/connector/IPlaceListener;

    invoke-interface {v0, v1, v2}, Landroid/support/place/connector/IBrokerService;->stopListeningForPlaces(Landroid/support/place/connector/IBrokerConnection;Landroid/support/place/connector/IPlaceListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "Error communicating with BrokerService"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public storeTrustedPeerCertficate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v1, :cond_0

    const-string v1, "aah.Broker"

    const-string v2, "storeTrustedPeerCertificate: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-interface {v1, p1, p2}, Landroid/support/place/connector/IBrokerService;->storeTrustedPeerCertificate(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "aah.Broker"

    const-string v3, "Cannot store the trusted peer certificate"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public unregisterConnector(Landroid/support/place/connector/Endpoint;)V
    .locals 3

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unregisterConnector only works for Brokers created in Containers"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_1

    const-string v0, "aah.Broker"

    const-string v1, "unregisterConnector: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->onStop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->getIEndpoint()Landroid/support/place/rpc/IEndpointStub;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/support/place/connector/IBrokerService;->unregisterConnector(Landroid/support/place/rpc/IEndpointStub;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "aah.Broker"

    const-string v1, "unregisterConnector: error communicating with BrokerService"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "aah.Broker"

    const-string v2, "Error invoking onStop (the object will be unregistered from the broker"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public unregisterEndpoint(Landroid/support/place/connector/Endpoint;)V
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    if-nez v0, :cond_0

    const-string v0, "aah.Broker"

    const-string v1, "unregisterEndpoint: Trying to use a broker after calling disconnect on the BrokerConnection that created it"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->onStop()V

    iget-object v0, p0, Landroid/support/place/connector/Broker;->mService:Landroid/support/place/connector/IBrokerService;

    invoke-virtual {p1}, Landroid/support/place/connector/Endpoint;->getIEndpoint()Landroid/support/place/rpc/IEndpointStub;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/support/place/connector/IBrokerService;->unregisterEndpoint(Landroid/support/place/rpc/IEndpointStub;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "aah.Broker"

    const-string v1, "unregisterEndpoint: error communicating with BrokerService"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
