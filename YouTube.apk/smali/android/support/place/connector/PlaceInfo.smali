.class public Landroid/support/place/connector/PlaceInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Landroid/support/place/rpc/Flattenable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final EXTRAS_FIELD_GUEST_MODE:Ljava/lang/String; = "guest_mode"

.field public static final EXTRAS_FIELD_MASTER_CERT:Ljava/lang/String; = "master_cert"

.field private static FIELD_EXTRAS:Ljava/lang/String;

.field private static FIELD_MASTER_SESSION_ID:Ljava/lang/String;

.field private static FIELD_PLACE_ID:Ljava/lang/String;

.field private static FIELD_PLACE_NAME:Ljava/lang/String;

.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;


# instance fields
.field private mMaster:Landroid/support/place/rpc/EndpointInfo;

.field private mMasterSessionId:Ljava/lang/String;

.field private mPlaceExtras:Landroid/support/place/rpc/RpcData;

.field private mPlaceId:Ljava/lang/String;

.field private mPlaceName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "placeId"

    sput-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_PLACE_ID:Ljava/lang/String;

    const-string v0, "placeName"

    sput-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_PLACE_NAME:Ljava/lang/String;

    const-string v0, "masterSessionId"

    sput-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_MASTER_SESSION_ID:Ljava/lang/String;

    const-string v0, "extras"

    sput-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_EXTRAS:Ljava/lang/String;

    new-instance v0, Landroid/support/place/connector/PlaceInfo$1;

    invoke-direct {v0}, Landroid/support/place/connector/PlaceInfo$1;-><init>()V

    sput-object v0, Landroid/support/place/connector/PlaceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Landroid/support/place/connector/PlaceInfo$2;

    invoke-direct {v0}, Landroid/support/place/connector/PlaceInfo$2;-><init>()V

    sput-object v0, Landroid/support/place/connector/PlaceInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    sget-object v0, Landroid/support/place/rpc/EndpointInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/rpc/EndpointInfo;

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    if-eqz v0, :cond_5

    sget-object v0, Landroid/support/place/rpc/RpcData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/rpc/RpcData;

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    :goto_5
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iput-object v3, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    iput-object v3, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    iput-object v3, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    goto :goto_5
.end method

.method private constructor <init>(Landroid/support/place/connector/PlaceInfo$Builder;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    # getter for: Landroid/support/place/connector/PlaceInfo$Builder;->placeId:Ljava/lang/String;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo$Builder;->access$000(Landroid/support/place/connector/PlaceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceId:Ljava/lang/String;

    # getter for: Landroid/support/place/connector/PlaceInfo$Builder;->placeName:Ljava/lang/String;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo$Builder;->access$100(Landroid/support/place/connector/PlaceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceName:Ljava/lang/String;

    # getter for: Landroid/support/place/connector/PlaceInfo$Builder;->master:Landroid/support/place/rpc/EndpointInfo;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo$Builder;->access$200(Landroid/support/place/connector/PlaceInfo$Builder;)Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    # getter for: Landroid/support/place/connector/PlaceInfo$Builder;->masterSessionId:Ljava/lang/String;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo$Builder;->access$300(Landroid/support/place/connector/PlaceInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    # getter for: Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo$Builder;->access$400(Landroid/support/place/connector/PlaceInfo$Builder;)Landroid/support/place/rpc/RpcData;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/place/connector/PlaceInfo$Builder;Landroid/support/place/connector/PlaceInfo$1;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/place/connector/PlaceInfo;-><init>(Landroid/support/place/connector/PlaceInfo$Builder;)V

    return-void
.end method

.method public constructor <init>(Landroid/support/place/rpc/RpcData;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_PLACE_ID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceId:Ljava/lang/String;

    sget-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_PLACE_NAME:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceName:Ljava/lang/String;

    const-string v0, "address"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/support/place/rpc/EndpointInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-interface {v0, p1}, Landroid/support/place/rpc/Flattenable$Creator;->createFromRpcData(Landroid/support/place/rpc/RpcData;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/rpc/EndpointInfo;

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    :cond_0
    sget-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_MASTER_SESSION_ID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_MASTER_SESSION_ID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    :cond_1
    sget-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_EXTRAS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_EXTRAS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    :cond_2
    return-void
.end method

.method static synthetic access$1000(Landroid/support/place/connector/PlaceInfo;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Landroid/support/place/connector/PlaceInfo;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Landroid/support/place/connector/PlaceInfo;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Landroid/support/place/connector/PlaceInfo;)Landroid/support/place/rpc/RpcData;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    return-object v0
.end method

.method static synthetic access$900(Landroid/support/place/connector/PlaceInfo;)Landroid/support/place/rpc/EndpointInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    return-object v0
.end method

.method private static safeEquals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getExtras()Landroid/support/place/rpc/RpcData;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    return-object v0
.end method

.method public getMaster()Landroid/support/place/rpc/EndpointInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    return-object v0
.end method

.method public getMasterCertificate()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    const-string v1, "master_cert"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    const-string v1, "master_cert"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMasterSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceId:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceName:Ljava/lang/String;

    return-object v0
.end method

.method public hasSameExtras(Landroid/support/place/connector/PlaceInfo;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/support/place/connector/PlaceInfo;->isGuestModeEnabled()Z

    move-result v0

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->isGuestModeEnabled()Z

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSameMaster(Landroid/support/place/connector/PlaceInfo;)Z
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    iget-object v1, p1, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    invoke-static {v0, v1}, Landroid/support/place/connector/PlaceInfo;->safeEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    iget-object v1, p1, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/support/place/connector/PlaceInfo;->safeEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGuestModeEnabled()Z
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    const-string v1, "guest_mode"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    const-string v1, "guest_mode"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PlaceInfo(mPlaceId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mPlaceName=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" master="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " masterSessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Extras:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, p1, p2}, Landroid/support/place/rpc/EndpointInfo;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_0
    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    if-eqz v0, :cond_2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v0, p1, p2}, Landroid/support/place/rpc/RpcData;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_2
    return-void

    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    sget-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_PLACE_ID:Ljava/lang/String;

    iget-object v1, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_PLACE_NAME:Ljava/lang/String;

    iget-object v1, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, p1}, Landroid/support/place/rpc/EndpointInfo;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_MASTER_SESSION_ID:Ljava/lang/String;

    iget-object v1, p0, Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    if-eqz v0, :cond_2

    sget-object v0, Landroid/support/place/connector/PlaceInfo;->FIELD_EXTRAS:Ljava/lang/String;

    iget-object v1, p0, Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    :cond_2
    return-void
.end method
