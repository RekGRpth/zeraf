.class final Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/rpc/RpcResultHandler;


# instance fields
.field private callback:Ljava/lang/Object;

.field private methodId:I

.field final synthetic this$0:Landroid/support/place/connector/security/SecurityService;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/security/SecurityService;ILjava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->this$0:Landroid/support/place/connector/security/SecurityService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->methodId:I

    iput-object p3, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final authenticate([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/security/SecurityService$OnAuthenticate;

    invoke-interface {v0, v1}, Landroid/support/place/connector/security/SecurityService$OnAuthenticate;->onAuthenticate(Z)V

    :cond_0
    return-void
.end method

.method public final banUser([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/security/SecurityService$OnBanUser;

    invoke-interface {v0, v1}, Landroid/support/place/connector/security/SecurityService$OnBanUser;->onBanUser(Z)V

    :cond_0
    return-void
.end method

.method public final configureAccountManager([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/security/SecurityService$OnConfigureAccountManager;

    invoke-interface {v0, v1}, Landroid/support/place/connector/security/SecurityService$OnConfigureAccountManager;->onConfigureAccountManager(Landroid/support/place/rpc/RpcData;)V

    :cond_0
    return-void
.end method

.method public final enableGuestMode([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/security/SecurityService$OnEnableGuestMode;

    invoke-interface {v0, v1}, Landroid/support/place/connector/security/SecurityService$OnEnableGuestMode;->onEnableGuestMode(Z)V

    :cond_0
    return-void
.end method

.method public final hasPermission([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/security/SecurityService$OnHasPermission;

    invoke-interface {v0, v1}, Landroid/support/place/connector/security/SecurityService$OnHasPermission;->onHasPermission(Z)V

    :cond_0
    return-void
.end method

.method public final isGuestModeEnabled([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/security/SecurityService$OnIsGuestModeEnabled;

    invoke-interface {v0, v1}, Landroid/support/place/connector/security/SecurityService$OnIsGuestModeEnabled;->onIsGuestModeEnabled(Z)V

    :cond_0
    return-void
.end method

.method public final listAccounts([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/security/SecurityService$OnListAccounts;

    invoke-interface {v0, v1}, Landroid/support/place/connector/security/SecurityService$OnListAccounts;->onListAccounts(Landroid/support/place/rpc/RpcData;)V

    :cond_0
    return-void
.end method

.method public final listUserAccounts([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/security/SecurityService$OnListUserAccounts;

    invoke-interface {v0, v1}, Landroid/support/place/connector/security/SecurityService$OnListUserAccounts;->onListUserAccounts(Landroid/support/place/rpc/RpcData;)V

    :cond_0
    return-void
.end method

.method public final onResult([B)V
    .locals 1

    iget v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->methodId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p1}, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->listAccounts([B)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1}, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->listUserAccounts([B)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1}, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->authenticate([B)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p1}, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->revokeAccount([B)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p1}, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->banUser([B)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, p1}, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->updateRoles([B)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, p1}, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->hasPermission([B)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p1}, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->configureAccountManager([B)V

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0, p1}, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->enableGuestMode([B)V

    goto :goto_0

    :pswitch_9
    invoke-virtual {p0, p1}, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->isGuestModeEnabled([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final revokeAccount([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/security/SecurityService$OnRevokeAccount;

    invoke-interface {v0, v1}, Landroid/support/place/connector/security/SecurityService$OnRevokeAccount;->onRevokeAccount(Z)V

    :cond_0
    return-void
.end method

.method public final updateRoles([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/security/SecurityService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/connector/security/SecurityService$OnUpdateRoles;

    invoke-interface {v0, v1}, Landroid/support/place/connector/security/SecurityService$OnUpdateRoles;->onUpdateRoles(Z)V

    :cond_0
    return-void
.end method
