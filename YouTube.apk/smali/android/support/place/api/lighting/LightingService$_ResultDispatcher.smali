.class final Landroid/support/place/api/lighting/LightingService$_ResultDispatcher;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/rpc/RpcResultHandler;


# instance fields
.field private callback:Ljava/lang/Object;

.field private methodId:I

.field final synthetic this$0:Landroid/support/place/api/lighting/LightingService;


# direct methods
.method public constructor <init>(Landroid/support/place/api/lighting/LightingService;ILjava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/api/lighting/LightingService$_ResultDispatcher;->this$0:Landroid/support/place/api/lighting/LightingService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Landroid/support/place/api/lighting/LightingService$_ResultDispatcher;->methodId:I

    iput-object p3, p0, Landroid/support/place/api/lighting/LightingService$_ResultDispatcher;->callback:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final listLights([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    iget-object v0, p0, Landroid/support/place/api/lighting/LightingService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/lighting/LightingService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/api/lighting/LightingService$OnListLights;

    invoke-interface {v0, v1}, Landroid/support/place/api/lighting/LightingService$OnListLights;->onListLights(Landroid/support/place/rpc/RpcData;)V

    :cond_0
    return-void
.end method

.method public final onResult([B)V
    .locals 1

    iget v0, p0, Landroid/support/place/api/lighting/LightingService$_ResultDispatcher;->methodId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p1}, Landroid/support/place/api/lighting/LightingService$_ResultDispatcher;->listLights([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
