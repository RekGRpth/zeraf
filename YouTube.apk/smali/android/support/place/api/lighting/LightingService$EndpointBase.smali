.class public abstract Landroid/support/place/api/lighting/LightingService$EndpointBase;
.super Landroid/support/place/connector/Connector;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/place/connector/Connector;-><init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V

    return-void
.end method


# virtual methods
.method public abstract listLights(Landroid/support/place/rpc/RpcContext;)Landroid/support/place/rpc/RpcData;
.end method

.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "listLights"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, p3}, Landroid/support/place/api/lighting/LightingService$EndpointBase;->listLights(Landroid/support/place/rpc/RpcContext;)Landroid/support/place/rpc/RpcData;

    move-result-object v2

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v1, v3, v2}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    const-string v2, "setLightLevel"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "id"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "level"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/api/lighting/LightingService$EndpointBase;->setLightLevel(Ljava/lang/String;ILandroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto :goto_0

    :cond_2
    const-string v2, "setLightName"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "id"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/api/lighting/LightingService$EndpointBase;->setLightName(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto :goto_0

    :cond_3
    const-string v2, "setLightLocation"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "id"

    invoke-virtual {v1, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "location"

    invoke-virtual {v1, v3}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1, p3}, Landroid/support/place/api/lighting/LightingService$EndpointBase;->setLightLocation(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    move-object v1, v0

    goto :goto_0

    :cond_4
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/Connector;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v0

    goto :goto_1
.end method

.method public pushOnLightsChanged(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "data"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    const-string v1, "onLightsChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/api/lighting/LightingService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public abstract setLightLevel(Ljava/lang/String;ILandroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract setLightLocation(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract setLightName(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V
.end method
