.class public Landroid/support/place/api/broker/BrokerManager;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/connector/BrokerConnection$Listener;


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "aah-BrokerManager"

.field private static sInstance:Landroid/support/place/api/broker/BrokerManager;

.field private static final sPackageChangeIntentFilter:Landroid/content/IntentFilter;


# instance fields
.field private mAvailablePlaces:Ljava/util/LinkedHashMap;

.field private mBeaconScanner:Landroid/support/place/beacon/BeaconScanner;

.field private mBeaconsCache:Ljava/util/List;

.field private mBroker:Landroid/support/place/connector/Broker;

.field private mConnectStateMachine:Landroid/support/place/api/broker/BrokerManager$ConnectStateMachine;

.field private mConnection:Landroid/support/place/connector/BrokerConnection;

.field private mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

.field private mConnectors:Ljava/util/LinkedHashMap;

.field private mContext:Landroid/content/Context;

.field private mFetchConnectors:Z

.field private mHandler:Landroid/os/Handler;

.field private mListeners:Ljava/util/LinkedList;

.field private mPackageChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mPlaceListener:Landroid/support/place/beacon/BeaconScanner$Listener;

.field private mRegistry:Landroid/support/place/connector/ConnectorRegistry;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Landroid/support/place/api/broker/BrokerManager;->sPackageChangeIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Landroid/support/place/api/broker/BrokerManager;->sPackageChangeIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Landroid/support/place/api/broker/BrokerManager;->sPackageChangeIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_RESTARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Landroid/support/place/api/broker/BrokerManager;->sPackageChangeIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mHandler:Landroid/os/Handler;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    new-instance v0, Landroid/support/place/api/broker/BrokerManager$1;

    invoke-direct {v0, p0}, Landroid/support/place/api/broker/BrokerManager$1;-><init>(Landroid/support/place/api/broker/BrokerManager;)V

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mPlaceListener:Landroid/support/place/beacon/BeaconScanner$Listener;

    new-instance v0, Landroid/support/place/api/broker/BrokerManager$2;

    invoke-direct {v0, p0}, Landroid/support/place/api/broker/BrokerManager$2;-><init>(Landroid/support/place/api/broker/BrokerManager;)V

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    new-instance v0, Landroid/support/place/api/broker/BrokerManager$5;

    invoke-direct {v0, p0}, Landroid/support/place/api/broker/BrokerManager$5;-><init>(Landroid/support/place/api/broker/BrokerManager;)V

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mPackageChangeReceiver:Landroid/content/BroadcastReceiver;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/support/place/connector/BrokerConnection;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/place/connector/BrokerConnection;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnection:Landroid/support/place/connector/BrokerConnection;

    new-instance v0, Landroid/support/place/beacon/BeaconScanner;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/place/api/broker/BrokerManager;->mPlaceListener:Landroid/support/place/beacon/BeaconScanner$Listener;

    invoke-direct {v0, v1, v2}, Landroid/support/place/beacon/BeaconScanner;-><init>(Landroid/content/Context;Landroid/support/place/beacon/BeaconScanner$Listener;)V

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBeaconScanner:Landroid/support/place/beacon/BeaconScanner;

    new-instance v0, Landroid/support/place/api/broker/BrokerManager$ConnectStateMachine;

    invoke-direct {v0, p0}, Landroid/support/place/api/broker/BrokerManager$ConnectStateMachine;-><init>(Landroid/support/place/api/broker/BrokerManager;)V

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectStateMachine:Landroid/support/place/api/broker/BrokerManager$ConnectStateMachine;

    return-void
.end method

.method static synthetic access$000(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$1200(Landroid/support/place/api/broker/BrokerManager;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/place/api/broker/BrokerManager;->disconnect()V

    return-void
.end method

.method static synthetic access$1300(Landroid/support/place/api/broker/BrokerManager;)Landroid/support/place/connector/BrokerConnection;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnection:Landroid/support/place/connector/BrokerConnection;

    return-object v0
.end method

.method static synthetic access$200(Landroid/support/place/api/broker/BrokerManager;)Landroid/support/place/connector/Broker;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    return-object v0
.end method

.method static synthetic access$300(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBeaconsCache:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Landroid/support/place/api/broker/BrokerManager;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Landroid/support/place/api/broker/BrokerManager;->mBeaconsCache:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$500(Landroid/support/place/api/broker/BrokerManager;)Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mPackageChangeReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$600()Landroid/content/IntentFilter;
    .locals 1

    sget-object v0, Landroid/support/place/api/broker/BrokerManager;->sPackageChangeIntentFilter:Landroid/content/IntentFilter;

    return-object v0
.end method

.method static synthetic access$700(Landroid/support/place/api/broker/BrokerManager;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Landroid/support/place/api/broker/BrokerManager;)Landroid/support/place/api/broker/BrokerManager$ConnectStateMachine;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectStateMachine:Landroid/support/place/api/broker/BrokerManager$ConnectStateMachine;

    return-object v0
.end method

.method private disconnect()V
    .locals 2

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorRegistry;->stopListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V

    :cond_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v0}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/place/api/broker/BrokerManager;->onPlaceDisconnected()V

    :cond_1
    invoke-virtual {p0}, Landroid/support/place/api/broker/BrokerManager;->onBrokerDisconnected()V

    :cond_2
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnection:Landroid/support/place/connector/BrokerConnection;

    invoke-virtual {v0, p0}, Landroid/support/place/connector/BrokerConnection;->disconnect(Landroid/support/place/connector/BrokerConnection$Listener;)V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/support/place/api/broker/BrokerManager;
    .locals 2

    const-class v1, Landroid/support/place/api/broker/BrokerManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Landroid/support/place/api/broker/BrokerManager;->sInstance:Landroid/support/place/api/broker/BrokerManager;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/place/api/broker/BrokerManager;

    invoke-direct {v0, p0}, Landroid/support/place/api/broker/BrokerManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Landroid/support/place/api/broker/BrokerManager;->sInstance:Landroid/support/place/api/broker/BrokerManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Landroid/support/place/api/broker/BrokerManager;->sInstance:Landroid/support/place/api/broker/BrokerManager;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private purgeConnectors()V
    .locals 3

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    iget-object v2, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    invoke-virtual {v2, v0}, Landroid/support/place/connector/ConnectorRegistry$Listener;->onConnectorRemoved(Landroid/support/place/connector/ConnectorInfo;)V

    goto :goto_1

    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method public connectToPlace(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/place/api/broker/BrokerManager;->isPlaceAvailable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v0, p1}, Landroid/support/place/connector/Broker;->setPlaceId(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public connectToPlaceAsGuestOrAuthorized(Landroid/support/place/connector/PlaceInfo;Ljava/lang/String;ZLandroid/support/place/rpc/RpcErrorHandler;)Z
    .locals 7

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/place/api/broker/BrokerManager;->isPlaceAvailable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    if-eqz v0, :cond_0

    new-instance v2, Landroid/support/place/api/broker/PlaceConnectHelper;

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    invoke-direct {v2, v0}, Landroid/support/place/api/broker/PlaceConnectHelper;-><init>(Landroid/support/place/connector/Broker;)V

    new-instance v0, Landroid/support/place/api/broker/BrokerManager$6;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Landroid/support/place/api/broker/BrokerManager$6;-><init>(Landroid/support/place/api/broker/BrokerManager;Landroid/support/place/api/broker/PlaceConnectHelper;Landroid/support/place/connector/PlaceInfo;Ljava/lang/String;ZLandroid/support/place/rpc/RpcErrorHandler;)V

    invoke-virtual {v2, p1, p2, p3, v0}, Landroid/support/place/api/broker/PlaceConnectHelper;->connect(Landroid/support/place/connector/PlaceInfo;Ljava/lang/String;ZLandroid/support/place/rpc/RpcErrorHandler;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBroker()Landroid/support/place/connector/Broker;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    return-object v0
.end method

.method public getConnectedPlace()Landroid/support/place/connector/PlaceInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v0}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getConnectors()Ljava/util/List;
    .locals 3

    iget-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorRegistry;->startListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V

    :cond_0
    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/LinkedList;

    iget-object v2, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getConnectorsForDevice(Ljava/lang/String;)Ljava/util/List;
    .locals 5

    iget-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorRegistry;->startListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V

    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iget-object v2, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public getConnectorsWithType(Ljava/lang/String;)Ljava/util/List;
    .locals 5

    iget-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorRegistry;->startListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V

    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iget-object v2, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public getPlaces()Ljava/util/List;
    .locals 3

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/LinkedList;

    iget-object v2, p0, Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public isPlaceAvailable(Ljava/lang/String;)Z
    .locals 2

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onBrokerConnected(Landroid/support/place/connector/Broker;)V
    .locals 8

    const/4 v1, 0x0

    iput-object p1, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBeaconScanner:Landroid/support/place/beacon/BeaconScanner;

    invoke-virtual {v0}, Landroid/support/place/beacon/BeaconScanner;->startListening()V

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBeaconScanner:Landroid/support/place/beacon/BeaconScanner;

    invoke-virtual {v0}, Landroid/support/place/beacon/BeaconScanner;->getBeacons()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;

    monitor-enter v5

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    :try_start_0
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/beacon/BeaconInfo;

    iget-object v0, v0, Landroid/support/place/beacon/BeaconInfo;->place:Landroid/support/place/connector/PlaceInfo;

    iget-object v6, p0, Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v1, p1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onBrokerConnected(Landroid/support/place/connector/Broker;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    return-void
.end method

.method public onBrokerDisconnected()V
    .locals 4

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mAvailablePlaces:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBeaconScanner:Landroid/support/place/beacon/BeaconScanner;

    invoke-virtual {v0}, Landroid/support/place/beacon/BeaconScanner;->stopListening()V

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onBrokerDisconnected()V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    return-void
.end method

.method public onMasterChanged(Landroid/support/place/rpc/EndpointInfo;)V
    .locals 2

    const-string v0, "aah-BrokerManager"

    const-string v1, "*****Master has changed! Stuff may go wonky.******"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorRegistry;->stopListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V

    invoke-direct {p0}, Landroid/support/place/api/broker/BrokerManager;->purgeConnectors()V

    :cond_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v0}, Landroid/support/place/connector/Broker;->getConnectorRegistry()Landroid/support/place/connector/ConnectorRegistry;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorRegistry;->startListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V

    :cond_1
    return-void
.end method

.method public onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V
    .locals 4

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v0}, Landroid/support/place/connector/Broker;->getConnectorRegistry()Landroid/support/place/connector/ConnectorRegistry;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorRegistry;->startListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V

    :cond_0
    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v1, p1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    return-void
.end method

.method public onPlaceDisconnected()V
    .locals 4

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onPlaceDisconnected()V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    return-void
.end method

.method public onPlaceNameChanged(Ljava/lang/String;)V
    .locals 4

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v1, p1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onPlaceNameChanged(Ljava/lang/String;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    return-void
.end method

.method public startListening(Landroid/support/place/api/broker/BrokerManager$ConnectionListener;)V
    .locals 3

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "aah-BrokerManager"

    const-string v2, "Attempted to add listener multiple times"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mHandler:Landroid/os/Handler;

    new-instance v2, Landroid/support/place/api/broker/BrokerManager$3;

    invoke-direct {v2, p0}, Landroid/support/place/api/broker/BrokerManager$3;-><init>(Landroid/support/place/api/broker/BrokerManager;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectStateMachine:Landroid/support/place/api/broker/BrokerManager$ConnectStateMachine;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/support/place/api/broker/BrokerManager$ConnectStateMachine;->sendMessage(I)V

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {p1, v0}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onBrokerConnected(Landroid/support/place/connector/Broker;)V

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v0}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V

    invoke-virtual {p0}, Landroid/support/place/api/broker/BrokerManager;->getConnectors()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onConnectedToRegistry(Ljava/util/List;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public startListeningForConnectors()V
    .locals 2

    iget-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/place/api/broker/BrokerManager;->mFetchConnectors:Z

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mRegistry:Landroid/support/place/connector/ConnectorRegistry;

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectorListener:Landroid/support/place/connector/ConnectorRegistry$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/ConnectorRegistry;->startListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V

    :cond_0
    return-void
.end method

.method public stopListening(Landroid/support/place/api/broker/BrokerManager$ConnectionListener;)V
    .locals 3

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mHandler:Landroid/os/Handler;

    new-instance v2, Landroid/support/place/api/broker/BrokerManager$4;

    invoke-direct {v2, p0}, Landroid/support/place/api/broker/BrokerManager$4;-><init>(Landroid/support/place/api/broker/BrokerManager;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager;->mConnectStateMachine:Landroid/support/place/api/broker/BrokerManager$ConnectStateMachine;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/support/place/api/broker/BrokerManager$ConnectStateMachine;->sendMessage(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
