.class Landroid/support/place/api/broker/BrokerManager$2;
.super Landroid/support/place/connector/ConnectorRegistry$Listener;
.source "SourceFile"


# instance fields
.field final synthetic this$0:Landroid/support/place/api/broker/BrokerManager;


# direct methods
.method constructor <init>(Landroid/support/place/api/broker/BrokerManager;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    invoke-direct {p0}, Landroid/support/place/connector/ConnectorRegistry$Listener;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectedToRegistry(Ljava/util/List;)V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$400(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$400(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;
    invoke-static {v6}, Landroid/support/place/api/broker/BrokerManager;->access$400(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v6

    invoke-virtual {v6, v5, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v2

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v1, p1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onConnectedToRegistry(Ljava/util/List;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    return-void
.end method

.method public onConnectorAdded(Landroid/support/place/connector/ConnectorInfo;)V
    .locals 4

    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;
    invoke-static {v1}, Landroid/support/place/api/broker/BrokerManager;->access$400(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;
    invoke-static {v2}, Landroid/support/place/api/broker/BrokerManager;->access$400(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v2

    invoke-virtual {v2, v0, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v1

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v1, p1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onConnectorAdded(Landroid/support/place/connector/ConnectorInfo;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    return-void
.end method

.method public onConnectorRemoved(Landroid/support/place/connector/ConnectorInfo;)V
    .locals 4

    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;
    invoke-static {v1}, Landroid/support/place/api/broker/BrokerManager;->access$400(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mConnectors:Ljava/util/LinkedHashMap;
    invoke-static {v2}, Landroid/support/place/api/broker/BrokerManager;->access$400(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedHashMap;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v1

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mListeners:Ljava/util/LinkedList;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$100(Landroid/support/place/api/broker/BrokerManager;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v1, p1}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onConnectorRemoved(Landroid/support/place/connector/ConnectorInfo;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    return-void
.end method

.method public onDisconnected()V
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$200(Landroid/support/place/api/broker/BrokerManager;)Landroid/support/place/connector/Broker;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$200(Landroid/support/place/api/broker/BrokerManager;)Landroid/support/place/connector/Broker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/connector/Broker;->getConnectorRegistry()Landroid/support/place/connector/ConnectorRegistry;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/broker/BrokerManager$2;->this$0:Landroid/support/place/api/broker/BrokerManager;

    # getter for: Landroid/support/place/api/broker/BrokerManager;->mBroker:Landroid/support/place/connector/Broker;
    invoke-static {v0}, Landroid/support/place/api/broker/BrokerManager;->access$200(Landroid/support/place/api/broker/BrokerManager;)Landroid/support/place/connector/Broker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/connector/Broker;->getConnectorRegistry()Landroid/support/place/connector/ConnectorRegistry;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/place/connector/ConnectorRegistry;->startListeningForConnectors(Landroid/support/place/connector/ConnectorRegistry$Listener;)V

    :cond_0
    return-void
.end method
