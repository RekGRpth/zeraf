.class public final Lcom/android/athome/picker/media/y;
.super Lcom/android/athome/picker/media/z;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/ArrayList;

.field final synthetic b:Lcom/android/athome/picker/media/v;

.field private o:Z


# direct methods
.method constructor <init>(Lcom/android/athome/picker/media/v;Lcom/android/athome/picker/media/x;)V
    .locals 1

    iput-object p1, p0, Lcom/android/athome/picker/media/y;->b:Lcom/android/athome/picker/media/v;

    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/media/z;-><init>(Lcom/android/athome/picker/media/v;Lcom/android/athome/picker/media/x;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    iput-object p0, p0, Lcom/android/athome/picker/media/y;->e:Lcom/android/athome/picker/media/y;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 6

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/athome/picker/media/y;->o:Z

    if-eqz v0, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/z;

    if-lez v1, :cond_0

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, v0, Lcom/android/athome/picker/media/z;->c:Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/media/y;->c:Ljava/lang/CharSequence;

    iput-boolean v2, p0, Lcom/android/athome/picker/media/y;->o:Z

    :cond_2
    invoke-super {p0}, Lcom/android/athome/picker/media/z;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/z;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/athome/picker/media/z;->e:Lcom/android/athome/picker/media/y;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/athome/picker/media/y;->o:Z

    iget-object v1, p0, Lcom/android/athome/picker/media/y;->b:Lcom/android/athome/picker/media/v;

    invoke-virtual {v1, v0, p0}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/z;Lcom/android/athome/picker/media/y;)V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/y;->d()V

    return-void
.end method

.method public final a(Lcom/android/athome/picker/media/z;)V
    .locals 3

    iget-object v0, p1, Lcom/android/athome/picker/media/z;->e:Lcom/android/athome/picker/media/y;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Route "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is already part of a group."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p1, Lcom/android/athome/picker/media/z;->f:Lcom/android/athome/picker/media/x;

    iget-object v1, p0, Lcom/android/athome/picker/media/y;->f:Lcom/android/athome/picker/media/x;

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Route cannot be added to a group with a different category. (Route category="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/android/athome/picker/media/z;->f:Lcom/android/athome/picker/media/x;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " group category="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/media/y;->f:Lcom/android/athome/picker/media/x;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p0, p1, Lcom/android/athome/picker/media/z;->e:Lcom/android/athome/picker/media/y;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/athome/picker/media/y;->o:Z

    invoke-virtual {p0}, Lcom/android/athome/picker/media/y;->c()V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/y;->d()V

    iget-object v1, p0, Lcom/android/athome/picker/media/y;->b:Lcom/android/athome/picker/media/v;

    invoke-virtual {v1, p1, p0, v0}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/z;Lcom/android/athome/picker/media/y;I)V

    return-void
.end method

.method public final b(I)Lcom/android/athome/picker/media/z;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/z;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/z;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/z;->b()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/android/athome/picker/media/z;->b()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/android/athome/picker/media/z;)V
    .locals 3

    iget-object v0, p1, Lcom/android/athome/picker/media/z;->e:Lcom/android/athome/picker/media/y;

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Route "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a member of this group."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/android/athome/picker/media/z;->e:Lcom/android/athome/picker/media/y;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/athome/picker/media/y;->o:Z

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->b:Lcom/android/athome/picker/media/v;

    invoke-virtual {v0, p1, p0}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/z;Lcom/android/athome/picker/media/y;)V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/y;->d()V

    return-void
.end method

.method final c()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/athome/picker/media/y;->b(I)Lcom/android/athome/picker/media/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/media/z;->f()I

    move-result v0

    if-le v0, v1, :cond_2

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/android/athome/picker/media/y;->j:I

    if-eq v1, v0, :cond_1

    iput v1, p0, Lcom/android/athome/picker/media/y;->j:I

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->b:Lcom/android/athome/picker/media/v;

    invoke-virtual {v0, p0}, Lcom/android/athome/picker/media/v;->c(Lcom/android/athome/picker/media/z;)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final c(I)V
    .locals 5

    invoke-virtual {p0}, Lcom/android/athome/picker/media/y;->g()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    int-to-float v1, p1

    int-to-float v0, v0

    div-float/2addr v1, v0

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/media/y;->b(I)Lcom/android/athome/picker/media/z;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/athome/picker/media/z;->g()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v1

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/android/athome/picker/media/z;->c(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/android/athome/picker/media/y;->j:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/android/athome/picker/media/y;->j:I

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->b:Lcom/android/athome/picker/media/v;

    invoke-virtual {v0, p0}, Lcom/android/athome/picker/media/v;->c(Lcom/android/athome/picker/media/z;)V

    goto :goto_0
.end method

.method final d()V
    .locals 10

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-nez v9, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->b:Lcom/android/athome/picker/media/v;

    invoke-virtual {v0, p0}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/z;)V

    :goto_0
    return-void

    :cond_0
    move v5, v4

    move v6, v3

    move v7, v3

    move v2, v4

    move v1, v4

    :goto_1
    if-ge v5, v9, :cond_3

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/z;

    iget v8, v0, Lcom/android/athome/picker/media/z;->d:I

    or-int/2addr v8, v1

    invoke-virtual {v0}, Lcom/android/athome/picker/media/z;->g()I

    move-result v1

    if-le v1, v2, :cond_7

    :goto_2
    iget v2, v0, Lcom/android/athome/picker/media/z;->h:I

    if-nez v2, :cond_1

    move v2, v3

    :goto_3
    and-int/2addr v7, v2

    iget v0, v0, Lcom/android/athome/picker/media/z;->l:I

    if-nez v0, :cond_2

    move v0, v3

    :goto_4
    and-int v2, v6, v0

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v6, v2

    move v2, v1

    move v1, v8

    goto :goto_1

    :cond_1
    move v2, v4

    goto :goto_3

    :cond_2
    move v0, v4

    goto :goto_4

    :cond_3
    if-eqz v7, :cond_4

    move v0, v4

    :goto_5
    iput v0, p0, Lcom/android/athome/picker/media/y;->h:I

    if-eqz v6, :cond_5

    move v0, v4

    :goto_6
    iput v0, p0, Lcom/android/athome/picker/media/y;->l:I

    iput v1, p0, Lcom/android/athome/picker/media/y;->d:I

    iput v2, p0, Lcom/android/athome/picker/media/y;->i:I

    if-ne v9, v3, :cond_6

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/z;

    iget-object v0, v0, Lcom/android/athome/picker/media/z;->g:Landroid/graphics/drawable/Drawable;

    :goto_7
    iput-object v0, p0, Lcom/android/athome/picker/media/y;->g:Landroid/graphics/drawable/Drawable;

    invoke-super {p0}, Lcom/android/athome/picker/media/z;->d()V

    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_5

    :cond_5
    move v0, v3

    goto :goto_6

    :cond_6
    const/4 v0, 0x0

    goto :goto_7

    :cond_7
    move v1, v2

    goto :goto_2
.end method

.method public final d(I)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/y;->g()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, Lcom/android/athome/picker/media/y;->b(I)Lcom/android/athome/picker/media/z;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/media/z;->d(I)V

    invoke-virtual {v0}, Lcom/android/athome/picker/media/z;->f()I

    move-result v0

    if-le v0, v1, :cond_3

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/android/athome/picker/media/y;->j:I

    if-eq v1, v0, :cond_0

    iput v1, p0, Lcom/android/athome/picker/media/y;->j:I

    iget-object v0, p0, Lcom/android/athome/picker/media/y;->b:Lcom/android/athome/picker/media/v;

    invoke-virtual {v0, p0}, Lcom/android/athome/picker/media/v;->c(Lcom/android/athome/picker/media/z;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method
