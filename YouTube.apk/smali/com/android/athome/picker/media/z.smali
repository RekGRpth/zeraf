.class public Lcom/android/athome/picker/media/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/CharSequence;

.field private b:Ljava/lang/Object;

.field c:Ljava/lang/CharSequence;

.field d:I

.field e:Lcom/android/athome/picker/media/y;

.field final f:Lcom/android/athome/picker/media/x;

.field g:Landroid/graphics/drawable/Drawable;

.field h:I

.field i:I

.field j:I

.field k:I

.field l:I

.field m:Lcom/android/athome/picker/media/ab;

.field final synthetic n:Lcom/android/athome/picker/media/v;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/media/v;Lcom/android/athome/picker/media/x;)V
    .locals 2

    const/16 v0, 0xf

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/android/athome/picker/media/z;->n:Lcom/android/athome/picker/media/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/android/athome/picker/media/z;->h:I

    iput v0, p0, Lcom/android/athome/picker/media/z;->i:I

    iput v0, p0, Lcom/android/athome/picker/media/z;->j:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/athome/picker/media/z;->k:I

    iput v1, p0, Lcom/android/athome/picker/media/z;->l:I

    iput-object p2, p0, Lcom/android/athome/picker/media/z;->f:Lcom/android/athome/picker/media/x;

    iget v0, p2, Lcom/android/athome/picker/media/x;->b:I

    iput v0, p0, Lcom/android/athome/picker/media/z;->d:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/z;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/z;->a:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/android/athome/picker/media/z;->a:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/athome/picker/media/z;->e:Lcom/android/athome/picker/media/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/z;->e:Lcom/android/athome/picker/media/y;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/media/y;->a(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/athome/picker/media/z;->d()V

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/media/z;->b:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/athome/picker/media/z;->d()V

    return-void
.end method

.method public b()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/z;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public c(I)V
    .locals 3

    iget v0, p0, Lcom/android/athome/picker/media/z;->h:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/z;->n:Lcom/android/athome/picker/media/v;

    invoke-static {v0}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/v;)Landroid/media/AudioManager;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/media/z;->k:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "MediaRouterFallback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".requestSetVolume(): Non-local volume playback on system route? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Could not request volume change."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method d()V
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/z;->n:Lcom/android/athome/picker/media/v;

    invoke-virtual {v0, p0}, Lcom/android/athome/picker/media/v;->b(Lcom/android/athome/picker/media/z;)V

    return-void
.end method

.method public d(I)V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/android/athome/picker/media/z;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/z;->f()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/android/athome/picker/media/z;->g()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/android/athome/picker/media/z;->n:Lcom/android/athome/picker/media/v;

    invoke-static {v1}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/v;)Landroid/media/AudioManager;

    move-result-object v1

    iget v2, p0, Lcom/android/athome/picker/media/z;->k:I

    invoke-virtual {v1, v2, v0, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "MediaRouterFallback"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".requestUpdateVolume(): Non-local volume playback on system route? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Could not request volume change."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final e()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/z;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final f()I
    .locals 2

    iget v0, p0, Lcom/android/athome/picker/media/z;->h:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/z;->n:Lcom/android/athome/picker/media/v;

    invoke-static {v0}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/v;)Landroid/media/AudioManager;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/media/z;->k:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/athome/picker/media/z;->j:I

    goto :goto_0
.end method

.method public final g()I
    .locals 2

    iget v0, p0, Lcom/android/athome/picker/media/z;->h:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/z;->n:Lcom/android/athome/picker/media/v;

    invoke-static {v0}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/v;)Landroid/media/AudioManager;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/media/z;->k:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/athome/picker/media/z;->i:I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    iget v0, p0, Lcom/android/athome/picker/media/z;->d:I

    invoke-static {v0}, Lcom/android/athome/picker/media/v;->a(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RouteInfo{ name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/athome/picker/media/z;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/media/z;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " category="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/media/z;->f:Lcom/android/athome/picker/media/x;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " supportedTypes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
