.class public final Lcom/android/athome/picker/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static G:Ljava/lang/Object;

.field private static H:Ljava/lang/Object;

.field static final a:Ljava/util/WeakHashMap;


# instance fields
.field private A:Ljava/lang/Object;

.field private B:Lcom/android/athome/picker/media/d;

.field private C:Lcom/android/athome/picker/media/c;

.field private final D:Ljava/lang/Object;

.field private E:Ljava/lang/String;

.field private F:Landroid/content/Context;

.field private I:Lcom/android/athome/picker/b/w;

.field private J:Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

.field private K:Ljava/lang/Runnable;

.field private L:Ljava/lang/Runnable;

.field private M:Landroid/support/place/music/TungstenGroupingService$Listener;

.field private b:Z

.field private c:Landroid/support/place/api/broker/BrokerManager;

.field private d:Landroid/support/place/music/TungstenGroupingService;

.field private e:Landroid/os/Handler;

.field private f:Landroid/support/place/connector/Broker;

.field private g:Ljava/lang/Object;

.field private h:Landroid/support/place/picker/MediaRouteProviderClient;

.field private i:Ljava/lang/String;

.field private j:Ljava/util/concurrent/atomic/AtomicInteger;

.field private k:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private l:Z

.field private m:Ljava/util/HashMap;

.field private n:Ljava/util/HashMap;

.field private o:Ljava/util/HashMap;

.field private p:Z

.field private q:Z

.field private r:Z

.field private final s:Ljava/util/ArrayList;

.field private t:Ljava/lang/Object;

.field private u:Z

.field private v:Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

.field private w:Landroid/support/place/connector/ConnectorInfo;

.field private x:Ljava/lang/Runnable;

.field private y:Ljava/util/Map;

.field private z:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/android/athome/picker/b/a;->a:Ljava/util/WeakHashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/Object;Landroid/support/place/api/broker/BrokerManager;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/android/athome/picker/b/a;->b:Z

    iput-object v1, p0, Lcom/android/athome/picker/b/a;->c:Landroid/support/place/api/broker/BrokerManager;

    iput-object v1, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->e:Landroid/os/Handler;

    iput-object v1, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    iput-object v1, p0, Lcom/android/athome/picker/b/a;->h:Landroid/support/place/picker/MediaRouteProviderClient;

    iput-object v1, p0, Lcom/android/athome/picker/b/a;->i:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->n:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->o:Ljava/util/HashMap;

    iput-boolean v2, p0, Lcom/android/athome/picker/b/a;->p:Z

    iput-boolean v2, p0, Lcom/android/athome/picker/b/a;->q:Z

    iput-boolean v2, p0, Lcom/android/athome/picker/b/a;->r:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->s:Ljava/util/ArrayList;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->t:Ljava/lang/Object;

    iput-boolean v2, p0, Lcom/android/athome/picker/b/a;->u:Z

    iput-object v1, p0, Lcom/android/athome/picker/b/a;->v:Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    new-instance v0, Lcom/android/athome/picker/b/b;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/b/b;-><init>(Lcom/android/athome/picker/b/a;)V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->x:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->z:Ljava/util/Map;

    iput-object v1, p0, Lcom/android/athome/picker/b/a;->A:Ljava/lang/Object;

    new-instance v0, Lcom/android/athome/picker/b/l;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/b/l;-><init>(Lcom/android/athome/picker/b/a;)V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->B:Lcom/android/athome/picker/media/d;

    new-instance v0, Lcom/android/athome/picker/b/m;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/b/m;-><init>(Lcom/android/athome/picker/b/a;)V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->C:Lcom/android/athome/picker/media/c;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->D:Ljava/lang/Object;

    iput-object v1, p0, Lcom/android/athome/picker/b/a;->E:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/athome/picker/b/a;->I:Lcom/android/athome/picker/b/w;

    new-instance v0, Lcom/android/athome/picker/b/n;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/b/n;-><init>(Lcom/android/athome/picker/b/a;)V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->J:Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    new-instance v0, Lcom/android/athome/picker/b/o;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/b/o;-><init>(Lcom/android/athome/picker/b/a;)V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->K:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/athome/picker/b/d;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/b/d;-><init>(Lcom/android/athome/picker/b/a;)V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->L:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/athome/picker/b/e;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/b/e;-><init>(Lcom/android/athome/picker/b/a;)V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->M:Landroid/support/place/music/TungstenGroupingService$Listener;

    iput-object p2, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    iput-object p3, p0, Lcom/android/athome/picker/b/a;->c:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->F:Landroid/content/Context;

    sget-object v0, Lcom/android/athome/picker/b/a;->G:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    const-string v1, "Nexus Q"

    invoke-static {v0, v1, v2}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Lcom/android/athome/picker/b/a;->G:Ljava/lang/Object;

    :cond_0
    sget-object v0, Lcom/android/athome/picker/b/a;->H:Ljava/lang/Object;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    const-string v1, "Nexus Q"

    invoke-static {v0, v1, v3}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Lcom/android/athome/picker/b/a;->H:Ljava/lang/Object;

    :cond_1
    new-instance v0, Landroid/support/place/picker/MediaRouteProviderClient;

    invoke-direct {v0, p1}, Landroid/support/place/picker/MediaRouteProviderClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->h:Landroid/support/place/picker/MediaRouteProviderClient;

    return-void
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Landroid/support/place/connector/Broker;)Landroid/support/place/connector/Broker;
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/b/a;->f:Landroid/support/place/connector/Broker;

    return-object p1
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;)Landroid/support/place/connector/ConnectorInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->w:Landroid/support/place/connector/ConnectorInfo;

    return-object v0
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Landroid/support/place/music/TungstenGroupingService;)Landroid/support/place/music/TungstenGroupingService;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    return-object v0
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;)Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->v:Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/android/athome/picker/b/a;
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v0, Lcom/android/athome/picker/b/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v1}, Lcom/android/athome/picker/media/k;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Media Router must be initialized before using AtHomeMediaRouter."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v1}, Landroid/support/place/api/broker/BrokerManager;->getInstance(Landroid/content/Context;)Landroid/support/place/api/broker/BrokerManager;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Broker Manager must be initialized before using AtHomeMediaRouter."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/android/athome/picker/b/a;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/athome/picker/b/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Landroid/support/place/api/broker/BrokerManager;)V

    sget-object v2, Lcom/android/athome/picker/b/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_2
    sget-object v0, Lcom/android/athome/picker/b/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/b/a;

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/b/a;->A:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;
    .locals 7

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;
    .locals 3

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    if-eqz p2, :cond_0

    sget-object v0, Lcom/android/athome/picker/b/a;->H:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v0}, Lcom/android/athome/picker/media/k;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/android/athome/picker/media/r;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    invoke-static {v0, p4}, Lcom/android/athome/picker/media/r;->b(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    const/16 v1, 0x64

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/r;->b(Ljava/lang/Object;I)V

    const/high16 v1, 0x42c80000

    mul-float/2addr v1, p5

    float-to-int v1, v1

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/r;->a(Ljava/lang/Object;I)V

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->B:Lcom/android/athome/picker/media/d;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/r;->a(Ljava/lang/Object;Lcom/android/athome/picker/media/d;)V

    invoke-static {v0, v2}, Lcom/android/athome/picker/media/r;->f(Ljava/lang/Object;I)V

    invoke-static {v0, v2}, Lcom/android/athome/picker/media/r;->e(Ljava/lang/Object;I)V

    invoke-static {v0, p1, p5, p6}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/Object;Ljava/lang/String;FZ)V

    return-object v0

    :cond_0
    sget-object v0, Lcom/android/athome/picker/b/a;->G:Ljava/lang/Object;

    goto :goto_0
.end method

.method private declared-synchronized a(I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/athome/picker/b/a;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->L:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    if-nez v0, :cond_1

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "TGS is not available yet. Not adjusting Rx volumes."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/android/athome/picker/b/a;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->L:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->A:Ljava/lang/Object;

    invoke-static {v1}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/athome/picker/b/r;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/b/r;-><init>(Lcom/android/athome/picker/b/a;)V

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/place/music/TungstenGroupingService;->adjustGroupVolume(Ljava/lang/String;ILandroid/support/place/rpc/RpcErrorHandler;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized a(Landroid/support/place/music/TgsGroup;)V
    .locals 15

    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_8

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v10}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/b/v;

    invoke-virtual {v1}, Lcom/android/athome/picker/b/v;->a()Ljava/lang/Object;

    move-result-object v12

    invoke-static {v12}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v2

    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-static {v12, v1}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v13, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    move v9, v1

    :goto_1
    if-ge v9, v10, :cond_5

    invoke-virtual/range {p1 .. p1}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/support/place/music/TgsService;

    move-object v8, v0

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v13, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v13, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/athome/picker/media/r;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/an;

    const/4 v3, 0x1

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    if-nez v1, :cond_3

    const/4 v6, 0x0

    :goto_3
    if-nez v1, :cond_4

    const/4 v7, 0x0

    :goto_4
    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v12, v1}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/android/athome/picker/b/a;->o:Ljava/util/HashMap;

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Added route: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to existing group: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_3
    :try_start_1
    invoke-virtual {v1}, Lcom/android/athome/picker/an;->b()F

    move-result v6

    goto :goto_3

    :cond_4
    invoke-virtual {v1}, Lcom/android/athome/picker/an;->c()Z

    move-result v7

    goto :goto_4

    :cond_5
    invoke-virtual {v13}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v14, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v13, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v12, v3}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/android/athome/picker/b/a;->o:Ljava/util/HashMap;

    invoke-virtual {v13, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_7
    iget-object v1, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/athome/picker/b/v;

    invoke-direct {v3, p0, v12, v11}, Lcom/android/athome/picker/b/v;-><init>(Lcom/android/athome/picker/b/a;Ljava/lang/Object;Ljava/util/List;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12}, Lcom/android/athome/picker/b/a;->f(Ljava/lang/Object;)F

    move-result v2

    invoke-static {v12}, Lcom/android/athome/picker/b/a;->e(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v12, v1, v2, v3}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/Object;Ljava/lang/String;FZ)V

    const/4 v1, 0x0

    invoke-static {v12, v1, v11}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/Object;ZLjava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_8
    monitor-exit p0

    return-void
.end method

.method private a(Landroid/support/place/music/TgsGroup;Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 3

    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3}, Lcom/android/athome/picker/b/a;->f(Ljava/lang/Object;)F

    move-result v1

    invoke-static {p3}, Lcom/android/athome/picker/b/a;->e(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {p3, v0, v1, v2}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/Object;Ljava/lang/String;FZ)V

    const/4 v0, 0x1

    invoke-static {p3, v0, p2}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/Object;ZLjava/util/List;)V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/athome/picker/b/v;

    invoke-direct {v2, p0, p3, p2}, Lcom/android/athome/picker/b/v;-><init>(Lcom/android/athome/picker/b/a;Ljava/lang/Object;Ljava/util/List;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->z:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getTx()Landroid/support/place/music/TgsTxService;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/place/music/TgsTxService;->getAppConnector()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(Landroid/support/place/music/TgsService;)V
    .locals 5

    invoke-virtual {p1}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->n:Ljava/util/HashMap;

    new-instance v2, Lcom/android/athome/picker/b/x;

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getPort()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/android/athome/picker/b/x;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(Landroid/support/place/rpc/EndpointInfo;)V
    .locals 7

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/an;

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "volume: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/athome/picker/an;->b()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mute: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/athome/picker/an;->c()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "volume"

    invoke-virtual {v0}, Lcom/android/athome/picker/an;->b()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/support/place/rpc/RpcData;->putFloat(Ljava/lang/String;F)V

    const-string v2, "mute"

    invoke-virtual {v0}, Lcom/android/athome/picker/an;->c()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->f:Landroid/support/place/connector/Broker;

    if-nez v0, :cond_0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Broker not ready."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/b/a;->f:Landroid/support/place/connector/Broker;

    const-string v2, "setMasterVolume"

    invoke-virtual {v1}, Landroid/support/place/rpc/RpcData;->ser()[B

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/android/athome/picker/b/i;

    invoke-direct {v5, p0}, Lcom/android/athome/picker/b/i;-><init>(Lcom/android/athome/picker/b/a;)V

    const/4 v6, 0x1

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;I)V
    .locals 5

    const/16 v0, 0xc8

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/athome/picker/b/a;->l:Z

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/athome/picker/b/a;->K:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/athome/picker/b/a;->K:Ljava/lang/Runnable;

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Landroid/support/place/connector/ConnectorInfo;)V
    .locals 4

    const/4 v3, 0x1

    iput-object p1, p0, Lcom/android/athome/picker/b/a;->w:Landroid/support/place/connector/ConnectorInfo;

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->f:Landroid/support/place/connector/Broker;

    if-nez v0, :cond_0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Broker not ready."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/support/place/music/TungstenGroupingService;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->f:Landroid/support/place/connector/Broker;

    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/place/music/TungstenGroupingService;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/athome/picker/b/a;->x:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    new-instance v1, Lcom/android/athome/picker/b/p;

    invoke-direct {v1, p0, v0}, Lcom/android/athome/picker/b/p;-><init>(Lcom/android/athome/picker/b/a;Landroid/support/place/music/TungstenGroupingService;)V

    new-instance v2, Lcom/android/athome/picker/b/q;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/b/q;-><init>(Lcom/android/athome/picker/b/a;)V

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/support/place/music/TungstenGroupingService;->versionCheck(IILandroid/support/place/music/TungstenGroupingService$OnVersionCheck;Landroid/support/place/rpc/RpcErrorHandler;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Landroid/support/place/music/TgsGroup;Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/athome/picker/b/a;->a(Landroid/support/place/music/TgsGroup;Ljava/util/ArrayList;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Landroid/support/place/music/TgsState;)V
    .locals 9

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processGroupAndReceiverList tgsState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/athome/picker/b/a;->p:Z

    if-eqz v0, :cond_1

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Creating group. Ignore TGS update."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/athome/picker/b/a;->q:Z

    if-eqz v0, :cond_2

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Assigning rx to group. Ignore TGS update."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/android/athome/picker/b/a;->r:Z

    if-eqz v0, :cond_3

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Removing rx from group. Ignore TGS update."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/athome/picker/b/a;->t:Ljava/lang/Object;

    monitor-enter v2

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/athome/picker/b/a;->u:Z

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Before updates."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, Landroid/support/place/music/TgsState;->getGroups()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsGroup;

    invoke-virtual {v0}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-virtual {v0}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0, v0}, Lcom/android/athome/picker/b/a;->a(Landroid/support/place/music/TgsGroup;)V

    :goto_2
    invoke-virtual {v0}, Landroid/support/place/music/TgsGroup;->getGroupId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_5
    :try_start_1
    invoke-direct {p0, v0}, Lcom/android/athome/picker/b/a;->b(Landroid/support/place/music/TgsGroup;)V

    goto :goto_2

    :cond_6
    iget-object v4, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/b/v;

    invoke-virtual {v1}, Lcom/android/athome/picker/b/v;->a()Ljava/lang/Object;

    move-result-object v6

    const-string v1, "AtHomeMediaRouter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "routeGroup to be removed: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v6}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_4
    if-ltz v1, :cond_8

    invoke-static {v6, v1}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;I)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_8
    const-string v1, "AtHomeMediaRouter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Remove stale group:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->z:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_9
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    sget-object v0, Lcom/android/athome/picker/b/a;->H:Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/o;->a(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    invoke-static {v3}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_5
    if-ltz v0, :cond_a

    invoke-static {v3, v0}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v0}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;I)V

    const-string v5, "AtHomeMediaRouter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Removed free pool rx: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    :cond_b
    invoke-virtual {p1}, Landroid/support/place/music/TgsState;->getAvailableRxs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsService;

    invoke-direct {p0, v0}, Lcom/android/athome/picker/b/a;->b(Landroid/support/place/music/TgsService;)V

    goto :goto_6

    :cond_c
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/athome/picker/b/a;->u:Z

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "After updates."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Ljava/lang/Object;I)V
    .locals 9

    const/high16 v8, 0x42c80000

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    invoke-static {p1}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/android/athome/picker/media/r;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "AtHomeMediaRouter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "RouteId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " GroupId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " volume: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    iget-object v4, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/b/v;

    invoke-virtual {v0}, Lcom/android/athome/picker/b/v;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    new-instance v6, Lcom/android/athome/picker/an;

    int-to-float v0, p2

    div-float v7, v0, v8

    if-nez p2, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {v6, v3, v7, v0}, Lcom/android/athome/picker/an;-><init>(Ljava/lang/String;FZ)V

    invoke-virtual {v4, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v5}, Lcom/android/athome/picker/b/a;->a(Landroid/support/place/rpc/EndpointInfo;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/athome/picker/b/a;->f()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/b/v;

    invoke-virtual {v0}, Lcom/android/athome/picker/b/v;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v1, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/athome/picker/b/a;->b(Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v4, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/android/athome/picker/an;

    int-to-float v7, p2

    div-float/2addr v7, v8

    if-nez p2, :cond_4

    :goto_2
    invoke-direct {v6, v3, v7, v1}, Lcom/android/athome/picker/an;-><init>(Ljava/lang/String;FZ)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/android/athome/picker/b/a;->a(Landroid/support/place/rpc/EndpointInfo;)V

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 5

    invoke-static {p1}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UpdateGroupMembership: routeId="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " groupId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    new-instance v2, Lcom/android/athome/picker/b/z;

    invoke-direct {v2, p0, v0, v1, p3}, Lcom/android/athome/picker/b/z;-><init>(Lcom/android/athome/picker/b/a;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Ljava/util/List;)V
    .locals 8

    const/4 v4, 0x0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "processRxVolumes:"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsRxVolume;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getRxId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v2, Lcom/android/athome/picker/an;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getRxId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/an;

    invoke-virtual {v1}, Lcom/android/athome/picker/an;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getVolume()F

    move-result v3

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getMute()Z

    move-result v6

    invoke-direct {v2, v1, v3, v6}, Lcom/android/athome/picker/an;-><init>(Ljava/lang/String;FZ)V

    :goto_1
    iget-object v1, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getRxId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/athome/picker/b/a;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getRxId()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v3, v4

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    :goto_3
    move-object v3, v0

    goto :goto_2

    :cond_1
    iget-object v1, p0, Lcom/android/athome/picker/b/a;->E:Ljava/lang/String;

    if-nez v1, :cond_2

    new-instance v1, Lcom/android/athome/picker/an;

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getRxId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getVolume()F

    move-result v3

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getMute()Z

    move-result v6

    invoke-direct {v1, v2, v3, v6}, Lcom/android/athome/picker/an;-><init>(Ljava/lang/String;FZ)V

    move-object v2, v1

    goto :goto_1

    :cond_2
    new-instance v1, Lcom/android/athome/picker/an;

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getVolume()F

    move-result v2

    invoke-virtual {v0}, Landroid/support/place/music/TgsRxVolume;->getMute()Z

    move-result v3

    invoke-direct {v1, v4, v2, v3}, Lcom/android/athome/picker/an;-><init>(Ljava/lang/String;FZ)V

    move-object v2, v1

    goto :goto_1

    :cond_3
    if-eqz v3, :cond_0

    invoke-static {v3, v2}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/Object;Lcom/android/athome/picker/an;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v2}, Lcom/android/athome/picker/an;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/b/v;

    invoke-virtual {v0}, Lcom/android/athome/picker/b/v;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/Object;Lcom/android/athome/picker/an;)V

    goto/16 :goto_0

    :cond_5
    return-void

    :cond_6
    move-object v0, v3

    goto :goto_3
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Ljava/util/List;Ljava/lang/Object;Z)V
    .locals 8

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    if-nez v0, :cond_0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "TGS is not available. Not creating group."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/android/athome/picker/b/a;->D:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-boolean v0, p0, Lcom/android/athome/picker/b/a;->p:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/athome/picker/b/a;->p:Z

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Creating group "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    new-instance v0, Lcom/android/athome/picker/b/g;

    move-object v1, p0

    move-object v3, p2

    move v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/athome/picker/b/g;-><init>(Lcom/android/athome/picker/b/a;Ljava/lang/String;Ljava/lang/Object;ZLjava/lang/Object;)V

    new-instance v1, Lcom/android/athome/picker/b/h;

    invoke-direct {v1, p0}, Lcom/android/athome/picker/b/h;-><init>(Lcom/android/athome/picker/b/a;)V

    invoke-virtual {v7, v2, p1, v0, v1}, Landroid/support/place/music/TungstenGroupingService;->createGroup(Ljava/lang/String;Ljava/util/List;Landroid/support/place/music/TungstenGroupingService$OnCreateGroup;Landroid/support/place/rpc/RpcErrorHandler;)V

    :cond_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method private static a(Ljava/lang/Object;Lcom/android/athome/picker/an;)V
    .locals 3

    if-eqz p0, :cond_0

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setVolumeState: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/athome/picker/an;->b()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Lcom/android/athome/picker/media/r;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/android/athome/picker/an;->b()F

    move-result v0

    const/high16 v1, 0x42c80000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-static {p0, v0}, Lcom/android/athome/picker/media/r;->a(Ljava/lang/Object;I)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;FZ)V
    .locals 1

    new-instance v0, Lcom/android/athome/picker/an;

    invoke-direct {v0, p1, p2, p3}, Lcom/android/athome/picker/an;-><init>(Ljava/lang/String;FZ)V

    invoke-static {p0, v0}, Lcom/android/athome/picker/media/q;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;ZLjava/util/List;)V
    .locals 5

    invoke-static {p0}, Lcom/android/athome/picker/media/q;->b(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-eqz p1, :cond_0

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "add route group to cache: Name= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " Id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "connector size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "update route group in cache: Name= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " Id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "connector size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/athome/picker/b/a;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/athome/picker/b/a;->l:Z

    return v0
.end method

.method private b(Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/rpc/EndpointInfo;
    .locals 3

    new-instance v0, Lcom/android/athome/picker/b/x;

    invoke-virtual {p1}, Landroid/support/place/rpc/EndpointInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/place/rpc/EndpointInfo;->getPort()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/android/athome/picker/b/x;-><init>(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->n:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->n:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/rpc/EndpointInfo;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/android/athome/picker/media/q;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/athome/picker/an;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/athome/picker/an;

    invoke-virtual {v0}, Lcom/android/athome/picker/an;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/athome/picker/b/a;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    return-object v0
.end method

.method private declared-synchronized b(Landroid/support/place/music/TgsGroup;)V
    .locals 14

    const/4 v9, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_4

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13, v12}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    move v10, v9

    move-object v11, v1

    :goto_0
    if-ge v10, v12, :cond_3

    invoke-virtual {p1}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/support/place/music/TgsService;

    move-object v8, v0

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/athome/picker/an;

    const/4 v3, 0x1

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    if-nez v1, :cond_0

    const/4 v6, 0x0

    :goto_1
    if-nez v1, :cond_1

    move v7, v9

    :goto_2
    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;

    move-result-object v1

    if-nez v10, :cond_2

    iget-object v2, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v1}, Lcom/android/athome/picker/media/q;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    :goto_3
    iget-object v3, p0, Lcom/android/athome/picker/b/a;->o:Ljava/util/HashMap;

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v8}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v1

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v10, 0x1

    move v10, v1

    move-object v11, v2

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/android/athome/picker/an;->b()F

    move-result v6

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/android/athome/picker/an;->c()Z

    move-result v7

    goto :goto_2

    :cond_2
    invoke-static {v11, v1}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v2, v11

    goto :goto_3

    :cond_3
    if-eqz v11, :cond_4

    invoke-direct {p0, p1, v13, v11}, Lcom/android/athome/picker/b/a;->a(Landroid/support/place/music/TgsGroup;Ljava/util/ArrayList;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized b(Landroid/support/place/music/TgsService;)V
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/an;

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/support/place/music/TgsService;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    if-nez v0, :cond_0

    const/4 v5, 0x0

    :goto_0
    if-nez v0, :cond_1

    const/4 v6, 0x0

    :goto_1
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;FZ)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    invoke-static {v2, v0}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v2, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Added user route:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->o:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/support/place/music/TgsService;->getConnectorInfo()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lcom/android/athome/picker/an;->b()F

    move-result v5

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/android/athome/picker/an;->c()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/android/athome/picker/b/a;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/athome/picker/b/a;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/android/athome/picker/b/a;Landroid/support/place/connector/ConnectorInfo;)V
    .locals 3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/android/athome/picker/b/t;

    invoke-direct {v2, p0, v0, p1}, Lcom/android/athome/picker/b/t;-><init>(Lcom/android/athome/picker/b/a;Ljava/lang/String;Landroid/support/place/connector/ConnectorInfo;)V

    invoke-virtual {v2}, Lcom/android/athome/picker/b/t;->a()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/android/athome/picker/b/a;Landroid/support/place/music/TgsState;)V
    .locals 3

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getRxServiceConnector tgsState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    invoke-virtual {p1}, Landroid/support/place/music/TgsState;->getGroups()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsGroup;

    invoke-virtual {v0}, Landroid/support/place/music/TgsGroup;->getRxs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsService;

    invoke-direct {p0, v0}, Lcom/android/athome/picker/b/a;->a(Landroid/support/place/music/TgsService;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/support/place/music/TgsState;->getAvailableRxs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsService;

    invoke-direct {p0, v0}, Lcom/android/athome/picker/b/a;->a(Landroid/support/place/music/TgsService;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/android/athome/picker/b/a;Landroid/support/place/music/TungstenGroupingService;)V
    .locals 3

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    invoke-virtual {v0}, Landroid/support/place/music/TungstenGroupingService;->stopListening()V

    :cond_0
    iput-object p1, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->M:Landroid/support/place/music/TungstenGroupingService$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/music/TungstenGroupingService;->startListening(Landroid/support/place/music/TungstenGroupingService$Listener;)V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/athome/picker/b/a;->a(I)V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    new-instance v1, Lcom/android/athome/picker/b/s;

    invoke-direct {v1, p0}, Lcom/android/athome/picker/b/s;-><init>(Lcom/android/athome/picker/b/a;)V

    new-instance v2, Lcom/android/athome/picker/b/c;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/b/c;-><init>(Lcom/android/athome/picker/b/a;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/place/music/TungstenGroupingService;->getRxVolumes(Landroid/support/place/music/TungstenGroupingService$OnGetRxVolumes;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method static synthetic b(Lcom/android/athome/picker/b/a;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/athome/picker/b/a;->p:Z

    return v0
.end method

.method private static c(Ljava/lang/Object;)F
    .locals 2

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/android/athome/picker/media/q;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/athome/picker/an;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/athome/picker/an;

    invoke-virtual {v0}, Lcom/android/athome/picker/an;->b()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/athome/picker/b/a;Landroid/support/place/connector/ConnectorInfo;)Lcom/android/athome/picker/an;
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/athome/picker/b/a;->b(Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/an;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/athome/picker/b/a;->G:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/android/athome/picker/b/a;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->z:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lcom/android/athome/picker/b/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/athome/picker/b/a;->r:Z

    return p1
.end method

.method static synthetic d(Lcom/android/athome/picker/b/a;Landroid/support/place/connector/ConnectorInfo;)Landroid/support/place/connector/ConnectorInfo;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->w:Landroid/support/place/connector/ConnectorInfo;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/athome/picker/b/a;->H:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Lcom/android/athome/picker/b/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/b/a;->u:Z

    return v0
.end method

.method static synthetic d(Lcom/android/athome/picker/b/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/athome/picker/b/a;->q:Z

    return p1
.end method

.method private static d(Ljava/lang/Object;)Z
    .locals 2

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/android/athome/picker/media/q;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/athome/picker/an;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/athome/picker/an;

    invoke-virtual {v0}, Lcom/android/athome/picker/an;->c()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/athome/picker/b/a;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->o:Ljava/util/HashMap;

    return-object v0
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "OnTgsLost:"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    invoke-virtual {v0}, Landroid/support/place/music/TungstenGroupingService;->stopListening()V

    iput-object v2, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    :cond_0
    invoke-direct {p0}, Lcom/android/athome/picker/b/a;->g()V

    iput-object v2, p0, Lcom/android/athome/picker/b/a;->w:Landroid/support/place/connector/ConnectorInfo;

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->x:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->c:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {v0}, Landroid/support/place/api/broker/BrokerManager;->getBroker()Landroid/support/place/connector/Broker;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->c:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {v0}, Landroid/support/place/api/broker/BrokerManager;->getConnectedPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->c:Landroid/support/place/api/broker/BrokerManager;

    const-class v1, Landroid/support/place/music/TungstenGroupingService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/place/api/broker/BrokerManager;->getConnectorsWithType(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->J:Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v1, v0}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;->onConnectorAdded(Landroid/support/place/connector/ConnectorInfo;)V

    :cond_1
    return-void
.end method

.method private static e(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x1

    move v0, v1

    :goto_0
    invoke-static {p0}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-static {p0, v0}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Lcom/android/athome/picker/b/a;->d(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private static f(Ljava/lang/Object;)F
    .locals 4

    const/high16 v1, 0x7fc00000

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-static {p0, v0}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/android/athome/picker/b/a;->d(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2}, Lcom/android/athome/picker/b/a;->c(Ljava/lang/Object;)F

    move-result v1

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/android/athome/picker/b/a;->c(Ljava/lang/Object;)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto :goto_1

    :cond_2
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    :cond_3
    return v1
.end method

.method static synthetic f(Lcom/android/athome/picker/b/a;)Lcom/android/athome/picker/b/w;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->I:Lcom/android/athome/picker/b/w;

    return-object v0
.end method

.method private f()Z
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->E:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 4

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Clear all routes."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->t:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/athome/picker/b/a;->u:Z

    sget-object v0, Lcom/android/athome/picker/b/a;->H:Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/athome/picker/media/o;->a(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-static {v3, v0}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/athome/picker/b/a;->G:Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/athome/picker/media/o;->a(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    invoke-static {v3, v2}, Lcom/android/athome/picker/media/k;->b(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->z:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/athome/picker/b/a;->u:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic g(Lcom/android/athome/picker/b/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/b/a;->b:Z

    return v0
.end method

.method static synthetic h(Lcom/android/athome/picker/b/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/android/athome/picker/b/a;)Landroid/support/place/picker/MediaRouteProviderClient;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->h:Landroid/support/place/picker/MediaRouteProviderClient;

    return-object v0
.end method

.method static synthetic j(Lcom/android/athome/picker/b/a;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->A:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic k(Lcom/android/athome/picker/b/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/athome/picker/b/a;->e()V

    return-void
.end method

.method static synthetic l(Lcom/android/athome/picker/b/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/android/athome/picker/b/a;)Landroid/support/place/music/TungstenGroupingService;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    return-object v0
.end method

.method static synthetic n(Lcom/android/athome/picker/b/a;)Landroid/support/place/api/broker/BrokerManager;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->c:Landroid/support/place/api/broker/BrokerManager;

    return-object v0
.end method

.method static synthetic o(Lcom/android/athome/picker/b/a;)Landroid/support/place/connector/Broker;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->f:Landroid/support/place/connector/Broker;

    return-object v0
.end method

.method static synthetic p(Lcom/android/athome/picker/b/a;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic q(Lcom/android/athome/picker/b/a;)Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->v:Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    return-object v0
.end method

.method static synthetic r(Lcom/android/athome/picker/b/a;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->x:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic s(Lcom/android/athome/picker/b/a;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic t(Lcom/android/athome/picker/b/a;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic u(Lcom/android/athome/picker/b/a;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic v(Lcom/android/athome/picker/b/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/b/a;->l:Z

    return v0
.end method

.method static synthetic w(Lcom/android/athome/picker/b/a;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/athome/picker/b/a;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic x(Lcom/android/athome/picker/b/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/b/a;->p:Z

    return v0
.end method

.method static synthetic y(Lcom/android/athome/picker/b/a;)V
    .locals 3

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    if-nez v0, :cond_0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "TGS not ready."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/b/a;->d:Landroid/support/place/music/TungstenGroupingService;

    new-instance v1, Lcom/android/athome/picker/b/j;

    invoke-direct {v1, p0}, Lcom/android/athome/picker/b/j;-><init>(Lcom/android/athome/picker/b/a;)V

    new-instance v2, Lcom/android/athome/picker/b/k;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/b/k;-><init>(Lcom/android/athome/picker/b/a;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/place/music/TungstenGroupingService;->getGroupState(Landroid/support/place/music/TungstenGroupingService$OnGetGroupState;Landroid/support/place/rpc/RpcErrorHandler;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/android/athome/picker/b/v;
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/android/athome/picker/media/q;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/athome/picker/an;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    check-cast v0, Lcom/android/athome/picker/an;

    invoke-virtual {v0}, Lcom/android/athome/picker/an;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/b/v;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/athome/picker/b/a;->b:Z

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->c:Landroid/support/place/api/broker/BrokerManager;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->J:Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v0, v1}, Landroid/support/place/api/broker/BrokerManager;->startListening(Landroid/support/place/api/broker/BrokerManager$ConnectionListener;)V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    const/high16 v1, 0x800000

    iget-object v2, p0, Lcom/android/athome/picker/b/a;->C:Lcom/android/athome/picker/media/c;

    invoke-static {v0, v1, v2}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;ILcom/android/athome/picker/media/b;)V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->h:Landroid/support/place/picker/MediaRouteProviderClient;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/place/picker/MediaRouteProviderClient;->getRouteIdForApplication(Ljava/lang/String;)Landroid/support/place/picker/MediaRouteProviderClient$RouteId;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->v:Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    :cond_0
    return-void
.end method

.method protected final a(Landroid/support/place/connector/ConnectorInfo;)V
    .locals 5

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AtHomeMediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "removeConnector: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/b/v;

    invoke-virtual {v0}, Lcom/android/athome/picker/b/v;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/android/athome/picker/media/k;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->y:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/athome/picker/b/a;->b(Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/b/a;->E:Ljava/lang/String;

    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/athome/picker/b/a;->b:Z

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->c:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {v0}, Landroid/support/place/api/broker/BrokerManager;->getBroker()Landroid/support/place/connector/Broker;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/athome/picker/b/a;->e()V

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/b/a;->c:Landroid/support/place/api/broker/BrokerManager;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->J:Landroid/support/place/api/broker/BrokerManager$ConnectionListener;

    invoke-virtual {v0, v1}, Landroid/support/place/api/broker/BrokerManager;->stopListening(Landroid/support/place/api/broker/BrokerManager$ConnectionListener;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/athome/picker/b/a;->f:Landroid/support/place/connector/Broker;

    invoke-direct {p0}, Lcom/android/athome/picker/b/a;->g()V

    iget-object v0, p0, Lcom/android/athome/picker/b/a;->g:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/athome/picker/b/a;->C:Lcom/android/athome/picker/media/c;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Lcom/android/athome/picker/media/b;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/b/a;->i:Ljava/lang/String;

    return-void
.end method
