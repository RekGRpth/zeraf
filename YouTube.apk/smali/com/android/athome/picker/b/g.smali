.class final Lcom/android/athome/picker/b/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/music/TungstenGroupingService$OnCreateGroup;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Z

.field final synthetic d:Ljava/lang/Object;

.field final synthetic e:Lcom/android/athome/picker/b/a;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/b/a;Ljava/lang/String;Ljava/lang/Object;ZLjava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/b/g;->e:Lcom/android/athome/picker/b/a;

    iput-object p2, p0, Lcom/android/athome/picker/b/g;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/athome/picker/b/g;->b:Ljava/lang/Object;

    iput-boolean p4, p0, Lcom/android/athome/picker/b/g;->c:Z

    iput-object p5, p0, Lcom/android/athome/picker/b/g;->d:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateGroup(Landroid/support/place/music/TgsGroup;)V
    .locals 5

    const/4 v0, 0x0

    const-string v1, "AtHomeMediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Created group "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/athome/picker/b/g;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/athome/picker/b/g;->e:Lcom/android/athome/picker/b/a;

    invoke-static {v1}, Lcom/android/athome/picker/b/a;->x(Lcom/android/athome/picker/b/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/b/g;->e:Lcom/android/athome/picker/b/a;

    invoke-static {v1, v0}, Lcom/android/athome/picker/b/a;->b(Lcom/android/athome/picker/b/a;Z)Z

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/android/athome/picker/b/g;->b:Ljava/lang/Object;

    invoke-static {v2}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lcom/android/athome/picker/b/g;->b:Ljava/lang/Object;

    invoke-static {v3, v0}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lcom/android/athome/picker/b/g;->e:Lcom/android/athome/picker/b/a;

    invoke-static {v4}, Lcom/android/athome/picker/b/a;->e(Lcom/android/athome/picker/b/a;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "AtHomeMediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "connector size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/b/g;->e:Lcom/android/athome/picker/b/a;

    iget-object v2, p0, Lcom/android/athome/picker/b/g;->b:Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, Lcom/android/athome/picker/b/a;->a(Lcom/android/athome/picker/b/a;Landroid/support/place/music/TgsGroup;Ljava/util/ArrayList;Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/android/athome/picker/b/g;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/b/g;->e:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->f(Lcom/android/athome/picker/b/a;)Lcom/android/athome/picker/b/w;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/b/g;->e:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->f(Lcom/android/athome/picker/b/a;)Lcom/android/athome/picker/b/w;

    iget-object v0, p0, Lcom/android/athome/picker/b/g;->d:Ljava/lang/Object;

    :cond_2
    return-void
.end method
