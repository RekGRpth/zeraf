.class final Lcom/android/athome/picker/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lcom/android/athome/picker/q;

.field private b:Ljava/util/HashMap;

.field private c:F


# direct methods
.method private constructor <init>(Lcom/android/athome/picker/q;)V
    .locals 1

    iput-object p1, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/ad;->b:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/athome/picker/q;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/athome/picker/ad;-><init>(Lcom/android/athome/picker/q;)V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 7

    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->h(Lcom/android/athome/picker/q;)Landroid/graphics/drawable/LevelListDrawable;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->i(Lcom/android/athome/picker/q;)Lcom/android/athome/picker/MediaOutput;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->f(Lcom/android/athome/picker/q;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    int-to-float v0, p2

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v1}, Lcom/android/athome/picker/q;->i(Lcom/android/athome/picker/q;)Lcom/android/athome/picker/MediaOutput;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/athome/picker/MediaOutput;->setVolume(F)V

    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->j(Lcom/android/athome/picker/q;)Lcom/android/athome/picker/ac;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v1}, Lcom/android/athome/picker/q;->i(Lcom/android/athome/picker/q;)Lcom/android/athome/picker/MediaOutput;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/athome/picker/ac;->a(Lcom/android/athome/picker/MediaOutput;)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->k(Lcom/android/athome/picker/q;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->f(Lcom/android/athome/picker/q;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->g(Lcom/android/athome/picker/q;)Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v2

    iget-object v0, p0, Lcom/android/athome/picker/ad;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget v1, p0, Lcom/android/athome/picker/ad;->c:F

    const/4 v4, 0x0

    cmpl-float v1, v1, v4

    if-eqz v1, :cond_5

    int-to-float v1, p2

    iget v4, p0, Lcom/android/athome/picker/ad;->c:F

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x3f800000

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sub-float v1, v5, v1

    int-to-float v5, p2

    iget v6, p0, Lcom/android/athome/picker/ad;->c:F

    sub-float/2addr v5, v6

    mul-float/2addr v1, v5

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/android/athome/picker/ad;->c:F

    sub-float/2addr v5, v6

    div-float/2addr v1, v5

    add-float/2addr v1, v4

    :goto_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/MediaOutput;->setVolume(F)V

    goto :goto_2

    :cond_4
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    int-to-float v4, p2

    mul-float/2addr v1, v4

    iget v4, p0, Lcom/android/athome/picker/ad;->c:F

    div-float/2addr v1, v4

    goto :goto_3

    :cond_5
    int-to-float v1, p2

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v1, v4

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0, v2}, Lcom/android/athome/picker/q;->a(Lcom/android/athome/picker/q;Lcom/android/athome/picker/MediaOutputGroup;)V

    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->j(Lcom/android/athome/picker/q;)Lcom/android/athome/picker/ac;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/android/athome/picker/ac;->a(Lcom/android/athome/picker/MediaOutput;)V

    goto/16 :goto_1
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4

    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->f(Lcom/android/athome/picker/q;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->f(Lcom/android/athome/picker/q;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/athome/picker/ad;->c:F

    iget-object v0, p0, Lcom/android/athome/picker/ad;->a:Lcom/android/athome/picker/q;

    invoke-static {v0}, Lcom/android/athome/picker/q;->g(Lcom/android/athome/picker/q;)Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "selected group is null in group volume mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, p0, Lcom/android/athome/picker/ad;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/athome/picker/ad;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutput;->getVolume()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method
