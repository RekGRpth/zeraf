.class final Lcom/android/athome/picker/a/d;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/athome/picker/a/a;

.field private b:I

.field private final c:Ljava/util/ArrayList;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private final f:Ljava/util/ArrayList;

.field private final g:Ljava/util/ArrayList;

.field private h:Z


# direct methods
.method constructor <init>(Lcom/android/athome/picker/a/a;)V
    .locals 1

    iput-object p1, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/athome/picker/a/d;->b:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/a/d;->f:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/a/d;->g:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/athome/picker/a/d;->a()V

    return-void
.end method

.method static synthetic a(Lcom/android/athome/picker/a/d;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/android/athome/picker/a/d;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/a/d;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/android/athome/picker/a/d;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/a/d;->d:Ljava/lang/Object;

    return-object p1
.end method


# virtual methods
.method final a()V
    .locals 12

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/athome/picker/a/d;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    iget-object v0, v0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v2}, Lcom/android/athome/picker/a/a;->a(Lcom/android/athome/picker/a/a;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/android/athome/picker/media/k;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v4

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/athome/picker/a/d;->b:I

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    iget-object v0, v0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/athome/picker/media/k;->b(Ljava/lang/Object;)I

    move-result v5

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_8

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    iget-object v0, v0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/android/athome/picker/media/k;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/android/athome/picker/a/d;->f:Ljava/util/ArrayList;

    invoke-static {v0, v2}, Lcom/android/athome/picker/media/o;->a(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iget-object v2, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/athome/picker/a/d;->d:Ljava/lang/Object;

    if-ne v0, v2, :cond_6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v2, v1

    :goto_2
    if-ge v2, v7, :cond_4

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/android/athome/picker/media/q;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-ne v8, v0, :cond_2

    invoke-static {v8}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v9

    move v0, v1

    :goto_3
    if-ge v0, v9, :cond_3

    invoke-static {v8, v0}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v10

    iget-object v11, p0, Lcom/android/athome/picker/a/d;->g:Ljava/util/ArrayList;

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    iget-object v8, p0, Lcom/android/athome/picker/a/d;->g:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/android/athome/picker/a/d;->g:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    iget-object v2, v2, Lcom/android/athome/picker/a/a;->Z:Lcom/android/athome/picker/a/h;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/athome/picker/a/d;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-interface {v6}, Ljava/util/List;->clear()V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_6
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    move v0, v1

    :goto_4
    if-ge v0, v2, :cond_5

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    if-ne v7, v4, :cond_7

    iget-object v8, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    iput v8, p0, Lcom/android/athome/picker/a/d;->b:I

    :cond_7
    iget-object v8, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    invoke-virtual {p0}, Lcom/android/athome/picker/a/d;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v0}, Lcom/android/athome/picker/a/a;->b(Lcom/android/athome/picker/a/a;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/athome/picker/a/d;->b:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v0}, Lcom/android/athome/picker/a/a;->b(Lcom/android/athome/picker/a/a;)Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/a/d;->b:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto/16 :goto_0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method final b()V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v0}, Lcom/android/athome/picker/a/a;->b(Lcom/android/athome/picker/a/a;)Landroid/widget/ListView;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    move v0, v2

    :goto_1
    if-ge v1, v3, :cond_4

    iget-object v4, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v5, p0, Lcom/android/athome/picker/a/d;->d:Ljava/lang/Object;

    if-ne v4, v5, :cond_2

    move v0, v1

    :cond_2
    if-nez v4, :cond_3

    :goto_2
    iget-object v2, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v2}, Lcom/android/athome/picker/a/a;->b(Lcom/android/athome/picker/a/a;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/widget/ListView;->smoothScrollToPosition(II)V

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method final c()V
    .locals 2

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v0}, Lcom/android/athome/picker/a/a;->b(Lcom/android/athome/picker/a/a;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/athome/picker/a/d;->b:I

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v0}, Lcom/android/athome/picker/a/a;->b(Lcom/android/athome/picker/a/a;)Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/a/d;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    goto :goto_0
.end method

.method final d()Z
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/athome/picker/a/d;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/athome/picker/a/d;->e:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/athome/picker/a/d;->a()V

    invoke-virtual {p0}, Lcom/android/athome/picker/a/d;->c()V

    return-void
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/a/d;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/android/athome/picker/media/k;->d(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/android/athome/picker/media/q;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/a/d;->d:Ljava/lang/Object;

    if-ne v0, v1, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    const/4 v9, 0x2

    const/16 v3, 0x8

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/a/d;->getItemViewType(I)I

    move-result v6

    if-nez p2, :cond_3

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v0}, Lcom/android/athome/picker/a/a;->c(Lcom/android/athome/picker/a/a;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {}, Lcom/android/athome/picker/a/a;->E()[I

    move-result-object v1

    aget v1, v1, v6

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v1, Lcom/android/athome/picker/a/i;

    invoke-direct {v1, v2}, Lcom/android/athome/picker/a/i;-><init>(B)V

    iput p1, v1, Lcom/android/athome/picker/a/i;->f:I

    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/athome/picker/a/i;->a:Landroid/widget/TextView;

    const v0, 0x1020015

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/athome/picker/a/i;->b:Landroid/widget/TextView;

    sget v0, Lcom/android/athome/picker/ai;->h:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/android/athome/picker/a/i;->c:Landroid/widget/ImageView;

    sget v0, Lcom/android/athome/picker/ai;->e:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/android/athome/picker/a/i;->g:Landroid/widget/CheckBox;

    sget v0, Lcom/android/athome/picker/ai;->f:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, v1, Lcom/android/athome/picker/a/i;->d:Landroid/widget/ImageButton;

    iget-object v0, v1, Lcom/android/athome/picker/a/i;->d:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/athome/picker/a/f;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/a/f;-><init>(Lcom/android/athome/picker/a/d;)V

    iput-object v0, v1, Lcom/android/athome/picker/a/i;->e:Lcom/android/athome/picker/a/f;

    iget-object v0, v1, Lcom/android/athome/picker/a/i;->d:Landroid/widget/ImageButton;

    iget-object v5, v1, Lcom/android/athome/picker/a/i;->e:Lcom/android/athome/picker/a/f;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    check-cast p3, Landroid/widget/ListView;

    new-instance v0, Lcom/android/athome/picker/a/e;

    invoke-direct {v0, p0, p3, p2, v1}, Lcom/android/athome/picker/a/e;-><init>(Lcom/android/athome/picker/a/d;Landroid/widget/ListView;Landroid/view/View;Lcom/android/athome/picker/a/i;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v5, v1

    move-object v1, p2

    :goto_0
    packed-switch v6, :pswitch_data_0

    :cond_1
    :goto_1
    instance-of v0, v1, Landroid/widget/Checkable;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Landroid/widget/Checkable;

    const/4 v3, 0x3

    if-ne v6, v3, :cond_e

    iget-object v3, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/athome/picker/media/q;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, Lcom/android/athome/picker/a/d;->e:Ljava/lang/Object;

    if-ne v3, v5, :cond_d

    :goto_2
    invoke-interface {v0, v4}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_2
    :goto_3
    return-object v1

    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/a/i;

    iput p1, v0, Lcom/android/athome/picker/a/i;->f:I

    move-object v5, v0

    move-object v1, p2

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    iget-object v0, v5, Lcom/android/athome/picker/a/i;->a:Landroid/widget/TextView;

    invoke-static {v7}, Lcom/android/athome/picker/media/q;->b(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v7}, Lcom/android/athome/picker/media/q;->c(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v0, v5, Lcom/android/athome/picker/a/i;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_4
    invoke-static {v7}, Lcom/android/athome/picker/media/q;->g(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    iget-object v8, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-virtual {v8}, Lcom/android/athome/picker/a/a;->k()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_4
    iget-object v8, v5, Lcom/android/athome/picker/a/i;->c:Landroid/widget/ImageView;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v8, v5, Lcom/android/athome/picker/a/i;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    move v0, v2

    :goto_5
    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-static {v7}, Lcom/android/athome/picker/media/q;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v8, p0, Lcom/android/athome/picker/a/d;->d:Ljava/lang/Object;

    if-ne v0, v8, :cond_a

    invoke-static {v7}, Lcom/android/athome/picker/media/q;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    iget-object v8, v5, Lcom/android/athome/picker/a/i;->g:Landroid/widget/CheckBox;

    invoke-static {v7}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v0

    if-le v0, v4, :cond_8

    move v0, v4

    :goto_6
    invoke-virtual {v8, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v8, v5, Lcom/android/athome/picker/a/i;->g:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->e:Ljava/lang/Object;

    if-ne v7, v0, :cond_9

    move v0, v4

    :goto_7
    invoke-virtual {v8, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    move v0, v2

    :goto_8
    iget-object v7, v5, Lcom/android/athome/picker/a/i;->d:Landroid/widget/ImageButton;

    if-eqz v7, :cond_1

    iget-object v7, v5, Lcom/android/athome/picker/a/i;->d:Landroid/widget/ImageButton;

    if-eqz v0, :cond_5

    move v3, v2

    :cond_5
    invoke-virtual {v7, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, v5, Lcom/android/athome/picker/a/i;->e:Lcom/android/athome/picker/a/f;

    iput p1, v0, Lcom/android/athome/picker/a/f;->a:I

    goto/16 :goto_1

    :cond_6
    iget-object v8, v5, Lcom/android/athome/picker/a/i;->b:Landroid/widget/TextView;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v8, v5, Lcom/android/athome/picker/a/i;->b:Landroid/widget/TextView;

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_7
    move v0, v3

    goto :goto_5

    :cond_8
    move v0, v2

    goto :goto_6

    :cond_9
    move v0, v2

    goto :goto_7

    :cond_a
    invoke-static {v0}, Lcom/android/athome/picker/media/o;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-static {v7}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v0

    if-gt v0, v4, :cond_b

    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/a/d;->getItemViewType(I)I

    move-result v0

    if-eq v0, v9, :cond_b

    invoke-virtual {p0}, Lcom/android/athome/picker/a/d;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_c

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/a/d;->getItemViewType(I)I

    move-result v0

    if-ne v0, v9, :cond_c

    :cond_b
    move v0, v4

    goto :goto_8

    :cond_c
    move v0, v2

    goto :goto_8

    :pswitch_1
    iget-object v0, p0, Lcom/android/athome/picker/a/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v3, v5, Lcom/android/athome/picker/a/i;->a:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/athome/picker/media/o;->a(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_d
    move v4, v2

    goto/16 :goto_2

    :cond_e
    iget v3, p0, Lcom/android/athome/picker/a/d;->b:I

    if-ne p1, v3, :cond_f

    :goto_9
    invoke-interface {v0, v4}, Landroid/widget/Checkable;->setChecked(Z)V

    goto/16 :goto_3

    :cond_f
    move v4, v2

    goto :goto_9

    :cond_10
    move v0, v2

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/a/d;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p0, p3}, Lcom/android/athome/picker/a/d;->getItemViewType(I)I

    move-result v0

    if-eq v0, v5, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/android/athome/picker/a/d;->e()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p3}, Lcom/android/athome/picker/a/d;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/android/athome/picker/media/k;->c(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    iget-object v0, v0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v2}, Lcom/android/athome/picker/a/a;->a(Lcom/android/athome/picker/a/a;)I

    move-result v2

    invoke-static {v0, v2, v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-virtual {v0}, Lcom/android/athome/picker/a/a;->a()V

    goto :goto_0

    :cond_3
    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    check-cast p2, Landroid/widget/Checkable;

    invoke-interface {p2}, Landroid/widget/Checkable;->isChecked()Z

    move-result v0

    iput-boolean v5, p0, Lcom/android/athome/picker/a/d;->h:Z

    invoke-static {v1}, Lcom/android/athome/picker/media/q;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v0, :cond_6

    iget-object v3, p0, Lcom/android/athome/picker/a/d;->e:Ljava/lang/Object;

    if-eq v2, v3, :cond_6

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    iget-object v0, v0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v3}, Lcom/android/athome/picker/a/a;->a(Lcom/android/athome/picker/a/a;)I

    move-result v3

    invoke-static {v0, v3}, Lcom/android/athome/picker/media/k;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    iget-object v0, v0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    invoke-static {v3}, Lcom/android/athome/picker/a/a;->a(Lcom/android/athome/picker/a/a;)I

    move-result v3

    iget-object v4, p0, Lcom/android/athome/picker/a/d;->e:Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    :cond_4
    invoke-static {v2, v1}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->e:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p2, v5}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_5
    :goto_1
    iput-boolean v6, p0, Lcom/android/athome/picker/a/d;->h:Z

    invoke-virtual {p0}, Lcom/android/athome/picker/a/d;->a()V

    goto :goto_0

    :cond_6
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->e:Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v0

    if-le v0, v5, :cond_5

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->e:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/athome/picker/a/d;->a:Lcom/android/athome/picker/a/a;

    iget-object v0, v0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p2, v6}, Landroid/widget/Checkable;->setChecked(Z)V

    goto :goto_1
.end method
