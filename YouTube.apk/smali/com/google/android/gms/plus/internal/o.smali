.class final Lcom/google/android/gms/plus/internal/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/c;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/o;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/a;Lcom/google/android/gms/plus/data/a/a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/o;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-boolean v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/o;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->a:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/o;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->e:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->refreshDrawableState()V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/o;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iput-object p2, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/o;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->i()V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/o;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->d()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/o;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->f()V

    goto :goto_0
.end method
