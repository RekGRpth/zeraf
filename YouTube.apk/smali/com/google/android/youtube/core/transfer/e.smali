.class public final Lcom/google/android/youtube/core/transfer/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/LinkedList;

.field private final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final c:Ljava/lang/Object;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/transfer/e;-><init>(I)V

    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/e;->a:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/e;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/e;->c:Ljava/lang/Object;

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/android/youtube/core/transfer/e;->d:I

    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/transfer/e;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/transfer/e;Ljava/io/InputStream;)I
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/e;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/transfer/e;->b(I)Lcom/google/android/youtube/core/transfer/g;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/transfer/g;->a(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/transfer/e;Ljava/io/InputStream;[BII)I
    .locals 2

    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/e;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    invoke-virtual {p1, p2, p3, p4}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p4}, Lcom/google/android/youtube/core/transfer/e;->b(I)Lcom/google/android/youtube/core/transfer/g;

    move-result-object v1

    iget v0, v1, Lcom/google/android/youtube/core/transfer/g;->b:I

    invoke-virtual {p1, p2, p3, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/transfer/g;->a(I)V

    goto :goto_0
.end method

.method private declared-synchronized b(I)Lcom/google/android/youtube/core/transfer/g;
    .locals 17

    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/core/transfer/e;->c:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/core/transfer/e;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v9

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/transfer/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v2, v5

    move v7, v1

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/transfer/g;

    iget-wide v11, v1, Lcom/google/android/youtube/core/transfer/g;->a:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/youtube/core/transfer/e;->d:I

    int-to-long v13, v4

    sub-long v13, v5, v13

    cmp-long v4, v11, v13

    if-gtz v4, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_0
    :try_start_3
    invoke-virtual {v1}, Lcom/google/android/youtube/core/transfer/g;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/google/android/youtube/core/transfer/g;->a()I

    move-result v4

    :goto_2
    add-int/2addr v4, v7

    iget-wide v11, v1, Lcom/google/android/youtube/core/transfer/g;->a:J

    cmp-long v7, v11, v2

    if-gez v7, :cond_4

    iget-wide v1, v1, Lcom/google/android/youtube/core/transfer/g;->a:J

    :goto_3
    move v7, v4

    move-wide v15, v1

    move-wide v2, v15

    goto :goto_1

    :cond_1
    iget v4, v1, Lcom/google/android/youtube/core/transfer/g;->b:I

    goto :goto_2

    :cond_2
    sub-int v1, v9, v7

    move/from16 v0, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-lez v1, :cond_3

    new-instance v2, Lcom/google/android/youtube/core/transfer/g;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/core/transfer/e;->c:Ljava/lang/Object;

    invoke-direct {v2, v3, v1, v5, v6}, Lcom/google/android/youtube/core/transfer/g;-><init>(Ljava/lang/Object;IJ)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/core/transfer/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v2

    :cond_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/core/transfer/e;->c:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/youtube/core/transfer/e;->d:I

    int-to-long v9, v4

    sub-long v2, v5, v2

    sub-long v2, v9, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    :try_start_5
    new-instance v2, Ljava/io/IOException;

    const-string v3, "interrupted"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_4
    move-wide v1, v2

    goto :goto_3
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "bytesPerSecond cannot be negative"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/e;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
