.class public final Lcom/google/android/youtube/core/transfer/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/transfer/i;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/google/android/youtube/core/client/bc;

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/Map;

.field private final f:Ljava/util/Map;

.field private final g:Ljava/util/Map;

.field private final h:Ljava/util/Map;

.field private i:Lcom/google/android/youtube/core/transfer/aa;

.field private j:Lcom/google/android/youtube/core/utils/aa;

.field private k:Z

.field private final l:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Ljava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->d:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->e:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->f:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->g:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->h:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/youtube/core/transfer/x;->l:Ljava/util/concurrent/Executor;

    iput-object p1, p0, Lcom/google/android/youtube/core/transfer/x;->b:Landroid/app/Activity;

    const-string v0, "gdataClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->c:Lcom/google/android/youtube/core/client/bc;

    return-void
.end method

.method private a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v2, v4

    const-string v1, "mime_type"

    aput-object v1, v2, v5

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v3

    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/File;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const-string v4, "file"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "not a file uri ["

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    :try_start_2
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "video/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "invalid file type ["

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    :try_start_3
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/transfer/x;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->h:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/transfer/x;Lcom/google/android/youtube/core/transfer/y;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/transfer/x;->b(Lcom/google/android/youtube/core/transfer/y;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/transfer/y;)Z
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->f:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->b(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/l;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->b(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/Transfer;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/Transfer;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v2, "metadata_updated"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->c(Lcom/google/android/youtube/core/transfer/y;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->i:Lcom/google/android/youtube/core/transfer/aa;

    invoke-interface {v0}, Lcom/google/android/youtube/core/transfer/aa;->c()V

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/l;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->b(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->e:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->b(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/l;

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->b(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/transfer/l;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/transfer/x;->b(Lcom/google/android/youtube/core/transfer/y;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/transfer/x;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->f:Ljava/util/Map;

    return-object v0
.end method

.method private b(Lcom/google/android/youtube/core/transfer/y;)V
    .locals 11

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/x;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->f:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->b(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->d(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/l;

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->b(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->d(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->a(Lcom/google/android/youtube/core/transfer/y;)Lcom/google/android/youtube/core/transfer/d;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/core/transfer/l;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/transfer/d;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->c:Lcom/google/android/youtube/core/client/bc;

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->e(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->f(Lcom/google/android/youtube/core/transfer/y;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->g(Lcom/google/android/youtube/core/transfer/y;)Lcom/google/android/youtube/core/model/Video$Privacy;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->h(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->i(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->j(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->k(Lcom/google/android/youtube/core/transfer/y;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/y;->l(Lcom/google/android/youtube/core/transfer/y;)Landroid/util/Pair;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/youtube/core/transfer/x;->b:Landroid/app/Activity;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/google/android/youtube/core/transfer/x;->b:Landroid/app/Activity;

    new-instance v10, Lcom/google/android/youtube/core/transfer/z;

    invoke-direct {v10, p0, p1}, Lcom/google/android/youtube/core/transfer/z;-><init>(Lcom/google/android/youtube/core/transfer/x;Lcom/google/android/youtube/core/transfer/y;)V

    invoke-static {v9, v10}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v9

    :goto_1
    invoke-interface/range {v0 .. v9}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :cond_2
    new-instance v9, Lcom/google/android/youtube/core/transfer/z;

    invoke-direct {v9, p0, p1}, Lcom/google/android/youtube/core/transfer/z;-><init>(Lcom/google/android/youtube/core/transfer/x;Lcom/google/android/youtube/core/transfer/y;)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/youtube/core/transfer/x;)Lcom/google/android/youtube/core/transfer/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->i:Lcom/google/android/youtube/core/transfer/aa;

    return-object v0
.end method

.method private c()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/y;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/transfer/x;->a(Lcom/google/android/youtube/core/transfer/y;)Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/youtube/core/transfer/d;ZZ[Ljava/lang/String;)V
    .locals 4

    const-string v1, "URI of the file being uploaded was not provided."

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "filename of the video being uploaded was not provided."

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "userAuth cannot be null."

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "privacy cannot be null."

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "title cannot be null."

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Video metadata was not provided."

    invoke-static {p10, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/transfer/x;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fileUri must resolve to a valid video file"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/youtube/core/transfer/y;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/google/android/youtube/core/transfer/y;-><init>(B)V

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/transfer/y;->a(Lcom/google/android/youtube/core/transfer/y;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v2, p2}, Lcom/google/android/youtube/core/transfer/y;->b(Lcom/google/android/youtube/core/transfer/y;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v2, p3}, Lcom/google/android/youtube/core/transfer/y;->a(Lcom/google/android/youtube/core/transfer/y;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz p11, :cond_0

    if-eqz p12, :cond_1

    :cond_0
    sget-object p4, Lcom/google/android/youtube/core/model/Video$Privacy;->PRIVATE:Lcom/google/android/youtube/core/model/Video$Privacy;

    :cond_1
    invoke-static {v2, p4}, Lcom/google/android/youtube/core/transfer/y;->a(Lcom/google/android/youtube/core/transfer/y;Lcom/google/android/youtube/core/model/Video$Privacy;)Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-static {v2, p5}, Lcom/google/android/youtube/core/transfer/y;->c(Lcom/google/android/youtube/core/transfer/y;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v2, p6}, Lcom/google/android/youtube/core/transfer/y;->d(Lcom/google/android/youtube/core/transfer/y;Ljava/lang/String;)Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/transfer/y;->e(Lcom/google/android/youtube/core/transfer/y;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v2, p8}, Lcom/google/android/youtube/core/transfer/y;->f(Lcom/google/android/youtube/core/transfer/y;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v2, p9}, Lcom/google/android/youtube/core/transfer/y;->a(Lcom/google/android/youtube/core/transfer/y;Landroid/util/Pair;)Landroid/util/Pair;

    iget-object v3, p0, Lcom/google/android/youtube/core/transfer/x;->h:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/transfer/y;->g(Lcom/google/android/youtube/core/transfer/y;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/youtube/core/transfer/y;->a(Lcom/google/android/youtube/core/transfer/y;Landroid/net/Uri;)Landroid/net/Uri;

    move/from16 v0, p12

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/transfer/y;->a(Lcom/google/android/youtube/core/transfer/y;Z)Z

    invoke-static {v2, p10}, Lcom/google/android/youtube/core/transfer/y;->a(Lcom/google/android/youtube/core/transfer/y;Lcom/google/android/youtube/core/transfer/d;)Lcom/google/android/youtube/core/transfer/d;

    if-nez p12, :cond_2

    invoke-static {v2}, Lcom/google/android/youtube/core/transfer/y;->a(Lcom/google/android/youtube/core/transfer/y;)Lcom/google/android/youtube/core/transfer/d;

    move-result-object v1

    const-string v3, "metadata_updated"

    invoke-virtual {v1, v3, p11}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Z)V

    :cond_2
    move-object/from16 v0, p13

    array-length v1, v0

    if-lez v1, :cond_3

    invoke-static {v2}, Lcom/google/android/youtube/core/transfer/y;->a(Lcom/google/android/youtube/core/transfer/y;)Lcom/google/android/youtube/core/transfer/d;

    move-result-object v1

    const-string v3, "upload_social_post_networks"

    move-object/from16 v0, p13

    invoke-virtual {v1, v3, v0}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/x;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/x;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/transfer/x;->a(Lcom/google/android/youtube/core/transfer/y;)Z

    :cond_4
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->g:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->g:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/transfer/aa;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/youtube/core/transfer/x;->i:Lcom/google/android/youtube/core/transfer/aa;

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/x;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/x;->t_()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->a:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/youtube/core/transfer/UploadService;->a(Landroid/content/Context;Lcom/google/android/youtube/core/transfer/i;)Lcom/google/android/youtube/core/utils/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/x;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/l;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v1, v0, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v3, "metadata_updated"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/transfer/l;

    iget-object v0, v0, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/transfer/l;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/x;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/aa;->b(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->j:Lcom/google/android/youtube/core/utils/aa;

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :goto_1
    return-void

    :cond_2
    iput-boolean v4, p0, Lcom/google/android/youtube/core/transfer/x;->k:Z

    goto :goto_1
.end method

.method public final b(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final c(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 4

    iget-wide v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->e:J

    iget-wide v2, p1, Lcom/google/android/youtube/core/transfer/Transfer;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->i:Lcom/google/android/youtube/core/transfer/aa;

    invoke-interface {v0}, Lcom/google/android/youtube/core/transfer/aa;->d()V

    :cond_0
    return-void
.end method

.method public final d(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/y;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/transfer/x;->b(Lcom/google/android/youtube/core/transfer/y;)V

    :cond_0
    return-void
.end method

.method public final e(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->f:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->f:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/y;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/x;->g:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/x;->i:Lcom/google/android/youtube/core/transfer/aa;

    iget-object v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v3, "metadata_updated"

    invoke-virtual {v0, v3, v1}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/transfer/aa;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/x;->c()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t_()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/transfer/x;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/transfer/x;->k:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/x;->b()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/x;->c()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/x;->i:Lcom/google/android/youtube/core/transfer/aa;

    invoke-interface {v0}, Lcom/google/android/youtube/core/transfer/aa;->b()V

    goto :goto_0
.end method
