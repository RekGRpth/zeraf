.class public interface abstract Lcom/google/android/youtube/core/ui/PagedView;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract b()I
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()Lcom/google/android/youtube/core/ui/PagedView$State;
.end method

.method public abstract h()Lcom/google/android/youtube/core/ui/i;
.end method

.method public abstract j()V
.end method

.method public abstract setAdapter(Landroid/widget/ListAdapter;)V
.end method

.method public abstract setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
.end method

.method public abstract setOnPagedViewStateChangeListener(Lcom/google/android/youtube/core/ui/g;)V
.end method

.method public abstract setOnRetryClickListener(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract setOnScrollListener(Lcom/google/android/youtube/core/ui/h;)V
.end method
