.class public Lcom/google/android/youtube/core/ui/TabRow;
.super Landroid/widget/HorizontalScrollView;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/widget/LinearLayout;

.field private final b:Z

.field private final c:Landroid/graphics/drawable/Drawable;

.field private d:Lcom/google/android/youtube/core/ui/s;

.field private e:Lcom/google/android/youtube/core/ui/t;

.field private f:I

.field private g:Z

.field private final h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v5, -0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v3, p0, Lcom/google/android/youtube/core/ui/TabRow;->f:I

    iput-boolean v4, p0, Lcom/google/android/youtube/core/ui/TabRow;->g:Z

    sget-object v0, Lcom/google/android/youtube/c;->k:[I

    const v1, 0x7f0d0005

    invoke-virtual {p1, p2, v0, v3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/ui/TabRow;->h:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/ui/TabRow;->b:Z

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/ui/TabRow;->c:Landroid/graphics/drawable/Drawable;

    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/core/ui/TabRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/core/ui/TabRow;->setHorizontalScrollBarEnabled(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/core/ui/TabRow;->setFillViewport(Z)V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/ui/TabRow;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/core/ui/TabRow;->setFillViewport(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getPaddingLeft()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/ui/TabRow;->i:I

    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public final a(IZ)V
    .locals 5

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->g:Z

    if-eqz v0, :cond_1

    iput p1, p0, Lcom/google/android/youtube/core/ui/TabRow;->f:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->c:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    :goto_1
    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-ne v2, p1, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v4, v0}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    mul-int/lit8 p1, p1, 0x2

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    if-eqz p2, :cond_6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/ui/TabRow;->smoothScrollTo(II)V

    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->e:Lcom/google/android/youtube/core/ui/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->e:Lcom/google/android/youtube/core/ui/t;

    goto :goto_0

    :cond_6
    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/ui/TabRow;->scrollTo(II)V

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    const v0, 0x7f040095

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->h:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->h:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    invoke-static {v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/TabRow;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    const/4 v3, -0x2

    const/4 v4, -0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->g:Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->d:Lcom/google/android/youtube/core/ui/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/TabRow;->d:Lcom/google/android/youtube/core/ui/s;

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/TabRow;->c:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/ui/s;->a(I)V

    :cond_0
    return-void

    :cond_1
    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    invoke-super/range {p0 .. p5}, Landroid/widget/HorizontalScrollView;->onLayout(ZIIII)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->g:Z

    iget v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->f:I

    if-ltz v0, :cond_0

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/youtube/core/ui/TabRow;->f:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/ui/TabRow;->a(IZ)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getMeasuredWidth()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/ui/TabRow;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    if-ge v1, v0, :cond_0

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getPaddingBottom()I

    move-result v3

    invoke-super {p0, v0, v1, v2, v3}, Landroid/widget/HorizontalScrollView;->setPadding(IIII)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/core/ui/TabRow;->i:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/TabRow;->getPaddingBottom()I

    move-result v3

    invoke-super {p0, v0, v1, v2, v3}, Landroid/widget/HorizontalScrollView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public setOnTabClickListener(Lcom/google/android/youtube/core/ui/s;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/TabRow;->d:Lcom/google/android/youtube/core/ui/s;

    return-void
.end method

.method public setOnTabFocusListener(Lcom/google/android/youtube/core/ui/t;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/TabRow;->e:Lcom/google/android/youtube/core/ui/t;

    return-void
.end method

.method public setPadding(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->setPadding(IIII)V

    iput p1, p0, Lcom/google/android/youtube/core/ui/TabRow;->i:I

    return-void
.end method
