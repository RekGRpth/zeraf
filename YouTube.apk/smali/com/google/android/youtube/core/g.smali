.class public Lcom/google/android/youtube/core/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/c;


# instance fields
.field protected final c:Landroid/content/ContentResolver;

.field protected final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/g;->c:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/google/android/youtube/core/g;->d:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/g;->c:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/core/g;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 2

    const-string v0, "device_supports_3d_playback"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final B()Z
    .locals 2

    const-string v0, "is_low_end_mobile_network"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final C()Z
    .locals 2

    const-string v0, "device_supports_720p_playback"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final D()Lcom/google/android/youtube/core/async/GDataRequest$Version;
    .locals 2

    const-string v0, "gdata_version"

    const-string v1, "2.1"

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/async/GDataRequest$Version;->parse(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest$Version;

    move-result-object v0

    return-object v0
.end method

.method public final E()Ljava/lang/String;
    .locals 2

    const-string v0, "adsense_query_domain"

    const-string v1, "googleads.g.doubleclick.net"

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final F()Ljava/lang/String;
    .locals 2

    const-string v0, "adsense_query_domain"

    const-string v1, "/pagead/ads"

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/g;->c:Landroid/content/ContentResolver;

    const-string v1, "device_country"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final H()I
    .locals 2

    const-string v0, "awful_player_buffer_low_millis"

    const/16 v1, 0x1388

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final I()I
    .locals 2

    const-string v0, "awful_player_buffer_full_millis"

    const/16 v1, 0x2710

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final J()Z
    .locals 2

    const-string v0, "vmap_ad_request_enabled"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected final a(Ljava/lang/String;I)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/g;->c:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/core/g;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method protected final a(Ljava/lang/String;J)J
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/g;->c:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/core/g;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/32 v2, 0x240c8400

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected final a(Ljava/lang/String;Z)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/g;->c:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/core/g;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    const-string v0, "analytics_property_id"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected i()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final u()Z
    .locals 2

    const-string v0, "analytics_enabled"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final v()I
    .locals 2

    const-string v0, "analytics_update_secs"

    const/16 v1, 0x1e

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final w()I
    .locals 2

    const-string v0, "analytics_sample_ratio"

    invoke-virtual {p0}, Lcom/google/android/youtube/core/g;->i()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2

    const-string v0, "analytics_category_suffix"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final y()Z
    .locals 2

    const-string v0, "enable_device_retention"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2

    const-string v0, "experiment_id"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
