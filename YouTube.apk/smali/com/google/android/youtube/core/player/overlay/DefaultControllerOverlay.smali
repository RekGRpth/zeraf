.class public Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;
.implements Lcom/google/android/youtube/core/player/overlay/ac;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

.field private H:Z

.field private I:Z

.field protected final a:Landroid/view/animation/Animation;

.field private b:Lcom/google/android/youtube/core/player/overlay/e;

.field private final c:F

.field private final d:Landroid/view/View;

.field private final e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

.field private final f:Landroid/widget/ImageButton;

.field private final g:Landroid/widget/ImageButton;

.field private final h:Landroid/widget/ImageButton;

.field private final i:Landroid/widget/ImageView;

.field private final j:Landroid/widget/ImageView;

.field private final k:Lcom/google/android/youtube/core/player/overlay/x;

.field private final l:Landroid/widget/LinearLayout;

.field private final m:Landroid/widget/TextView;

.field private final n:Landroid/widget/ImageView;

.field private final o:Lcom/google/android/youtube/core/player/overlay/p;

.field private final p:Landroid/os/Handler;

.field private final q:I

.field private final r:I

.field private s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

.field private t:I

.field private u:I

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    const/4 v2, -0x2

    const v8, 0x7f040065

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c:F

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->d:Landroid/view/View;

    const v4, 0x7f020090

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->d:Landroid/view/View;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-direct {v0, p1, p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/overlay/ac;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v8, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    const v4, 0x7f0201d1

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    const v4, 0x7f0b0090

    invoke-virtual {p1, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v8, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    const v4, 0x7f0201cc

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    const v4, 0x7f0b008c

    invoke-virtual {p1, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v8, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    const v3, 0x7f0201cf

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    const v3, 0x7f0b008a

    invoke-virtual {p1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-boolean v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->A:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    const v3, 0x7f0201a5

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    const v3, 0x7f0b008d

    invoke-virtual {p1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    const/high16 v3, -0x34000000

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    const v2, 0x7f020063

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    const v2, 0x7f0b0092

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c:F

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(IF)I

    move-result v2

    const/16 v3, 0xc

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c:F

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(IF)I

    move-result v3

    invoke-virtual {v0, v6, v2, v3, v6}, Landroid/widget/ImageView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    const v2, 0x7f02005f

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    const v2, 0x7f0b0093

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    const/16 v2, 0xc

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c:F

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(IF)I

    move-result v2

    const/4 v3, 0x3

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c:F

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(IF)I

    move-result v3

    invoke-virtual {v0, v2, v3, v6, v6}, Landroid/widget/ImageView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/p;

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/g;

    invoke-direct {v1, p0, v6}, Lcom/google/android/youtube/core/player/overlay/g;-><init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;B)V

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/overlay/p;-><init>(Lcom/google/android/youtube/core/player/overlay/q;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->o:Lcom/google/android/youtube/core/player/overlay/p;

    const v0, 0x7f050002

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->q:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->r:I

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->YOUTUBE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iput-boolean v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->E:Z

    iput-boolean v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->F:Z

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/x;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/player/overlay/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->k:Lcom/google/android/youtube/core/player/overlay/x;

    invoke-virtual {p0, v6}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setClipToPadding(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h()V

    return-void
.end method

.method private static a(IF)I
    .locals 2

    int-to-float v0, p0

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private static a(Landroid/view/View;II)I
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, p2, v1

    add-int v2, p1, v0

    invoke-virtual {p0, p1, v1, v2, p2}, Landroid/view/View;->layout(IIII)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a:Landroid/view/animation/Animation;

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->q:I

    int-to-long v0, v0

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->d:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;)V

    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->r:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/TimeBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    return-object v0
.end method

.method private static b(Landroid/view/View;II)V
    .locals 4

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v2, v0, 0x2

    sub-int v2, p1, v2

    div-int/lit8 v3, v1, 0x2

    sub-int v3, p2, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p2

    invoke-virtual {p0, v2, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->z:Z

    return v0
.end method

.method private k()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->x:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->minimalWhenNotFullscreen:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, v2, v2, v2, v2}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowTimes(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowScrubber(Z)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->c()I

    move-result v0

    const/16 v3, 0x18

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c:F

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(IF)I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v3, v0, v2, v0, v2}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowTimes(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsScrubber:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowScrubber(Z)V

    goto :goto_1
.end method

.method private l()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->w:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->E:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p:Landroid/os/Handler;

    const-wide/16 v1, 0x9c4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    return-void
.end method

.method private m()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    return-void
.end method

.method private n()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->B:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsNextPrevious:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->d()V

    :cond_0
    return-void
.end method

.method private o()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsNextPrevious:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->e()V

    :cond_0
    return-void
.end method

.method private p()V
    .locals 7

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    const v1, 0x7f0201a5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0b008d

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->w:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v1

    if-eqz v1, :cond_e

    :cond_0
    move v1, v2

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_8

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-ne v5, v0, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v6, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-eq v3, v6, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    if-ne v5, v3, :cond_7

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-eqz v3, :cond_7

    :cond_2
    move v3, v4

    :goto_3
    invoke-static {v5, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    const v1, 0x7f0201a3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0b008e

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    const v1, 0x7f0201ad

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0b008f

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    goto :goto_1

    :cond_7
    move v3, v2

    goto :goto_3

    :cond_8
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v3, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-eq v1, v3, :cond_9

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_9
    move v1, v4

    :goto_4
    invoke-static {p0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    :goto_5
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->w:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_a
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsNextPrevious:Z

    if-eqz v1, :cond_18

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->B:Z

    if-nez v1, :cond_b

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->C:Z

    if-eqz v1, :cond_18

    :cond_b
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-nez v1, :cond_18

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    if-ne v0, v1, :cond_18

    :goto_6
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    if-eqz v4, :cond_19

    const v0, 0x7f020061

    :goto_7
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    if-eqz v4, :cond_c

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->B:Z

    if-eqz v0, :cond_1a

    const v0, 0x7f02019f

    :goto_8
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->C:Z

    if-eqz v0, :cond_1b

    const v0, 0x7f0201a9

    :goto_9
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_c
    return-void

    :cond_d
    move v1, v2

    goto :goto_4

    :cond_e
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->d:Landroid/view/View;

    invoke-static {v1, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsTimeBar:Z

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->x:Z

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->showButtonsWhenNotFullscreen:Z

    if-nez v1, :cond_f

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->I:Z

    if-eqz v1, :cond_11

    :cond_f
    move v1, v4

    :goto_a
    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-nez v3, :cond_12

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v3, :cond_12

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->D:Z

    if-eqz v3, :cond_12

    if-eqz v1, :cond_12

    move v3, v4

    :goto_b
    invoke-static {v5, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-nez v3, :cond_13

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v3, :cond_13

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->z:Z

    if-eqz v3, :cond_13

    if-eqz v1, :cond_13

    move v3, v4

    :goto_c
    invoke-static {v5, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    iget-boolean v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-nez v5, :cond_14

    iget-boolean v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->A:Z

    if-eqz v5, :cond_14

    if-nez v1, :cond_10

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->H:Z

    if-eqz v1, :cond_14

    :cond_10
    move v1, v4

    :goto_d
    invoke-static {v3, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    if-ne v0, v1, :cond_15

    move v1, v4

    :goto_e
    invoke-static {v3, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    if-ne v0, v1, :cond_16

    move v1, v4

    :goto_f
    invoke-static {v3, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    if-ne v0, v1, :cond_17

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v1, :cond_17

    move v1, v4

    :goto_10
    invoke-static {v3, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setVisibility(I)V

    goto/16 :goto_5

    :cond_11
    move v1, v2

    goto :goto_a

    :cond_12
    move v3, v2

    goto :goto_b

    :cond_13
    move v3, v2

    goto :goto_c

    :cond_14
    move v1, v2

    goto :goto_d

    :cond_15
    move v1, v2

    goto :goto_e

    :cond_16
    move v1, v2

    goto :goto_f

    :cond_17
    move v1, v2

    goto :goto_10

    :cond_18
    move v4, v2

    goto/16 :goto_6

    :cond_19
    const v0, 0x7f0201cb

    goto/16 :goto_7

    :cond_1a
    const v0, 0x7f0201a0

    goto/16 :goto_8

    :cond_1b
    const v0, 0x7f0201aa

    goto/16 :goto_9
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->c()V

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/e;->a(I)V

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 5

    const/4 v1, 0x1

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "context cannot be null"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->touchscreen:I

    if-eq v0, v1, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b001a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n\n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g()V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->UNRECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b001b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    const-string v0, ""

    goto :goto_3
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->k:Lcom/google/android/youtube/core/player/overlay/x;

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/f;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/overlay/f;-><init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/player/overlay/x;->a(Ljava/util/List;Lcom/google/android/youtube/core/player/overlay/z;)V

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g()V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setBufferedPercent(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g()V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->a()V

    return-void
.end method

.method public final g()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->w:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->w:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->h()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l()V

    return-void
.end method

.method public final h()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->w:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->w:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->i()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    :cond_1
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Z)V

    :goto_0
    return v0

    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->k:Lcom/google/android/youtube/core/player/overlay/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/x;->a()V

    return-void
.end method

.method public final j()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->b()I

    move-result v0

    return v0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h()V

    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->g()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->x:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->x:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/e;->b(Z)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->o()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->k()V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->b()V

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->a()V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m()V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/google/android/youtube/core/player/overlay/p;->b(I)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g()V

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v4, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v3, v4, :cond_6

    if-eqz v2, :cond_6

    const/16 v2, 0x14

    if-eq p1, v2, :cond_2

    const/16 v2, 0x15

    if-eq p1, v2, :cond_2

    const/16 v2, 0x16

    if-eq p1, v2, :cond_2

    const/16 v2, 0x13

    if-ne p1, v2, :cond_5

    :cond_2
    move v2, v0

    :goto_1
    if-nez v2, :cond_6

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/overlay/e;->j()V

    :cond_3
    :goto_2
    return v0

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->o:Lcom/google/android/youtube/core/player/overlay/p;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/youtube/core/player/overlay/p;->a(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_2
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->o:Lcom/google/android/youtube/core/player/overlay/p;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/p;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    sub-int v0, p5, p3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    sub-int v1, p4, p2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->d:Landroid/view/View;

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsTimeBar:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v2

    const/4 v3, 0x0

    sub-int v4, p5, p3

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/widget/TextView;->layout(IIII)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v0

    div-int/lit8 v2, v1, 0x2

    add-int/2addr v0, v2

    sub-int v2, p5, p3

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-static {v3, v0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b(Landroid/view/View;II)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    invoke-static {v3, v0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->isError()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-static {v0, v3, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v3

    add-int/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b(Landroid/view/View;II)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingRight()I

    move-result v2

    sub-int v2, p4, v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 9

    const/4 v1, 0x0

    const/16 v8, 0x8

    const/high16 v7, 0x40000000

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->i:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    invoke-static {v1, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getDefaultSize(II)I

    move-result v3

    invoke-static {v1, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getDefaultSize(II)I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v0

    sub-int v0, v3, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingTop()I

    move-result v0

    sub-int v0, v4, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getPaddingBottom()I

    move-result v2

    sub-int v2, v0, v2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->measure(II)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->c()I

    move-result v0

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5, v5}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v1, v0

    :goto_0
    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v6

    if-eq v6, v8, :cond_0

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v6, v5, v5}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v6

    sub-int/2addr v0, v6

    :cond_0
    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v6

    if-eq v6, v8, :cond_1

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v6, v5, v5}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v0, v5

    :cond_1
    iget-boolean v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-eqz v5, :cond_2

    div-int/lit8 v2, v2, 0x30

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    :goto_1
    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v5, v0, v2}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->measure(II)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getMeasuredHeight()I

    move-result v0

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_2
    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->d:Landroid/view/View;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    invoke-virtual {p0, v3, v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setMeasuredDimension(II)V

    return-void

    :cond_2
    const/high16 v5, -0x80000000

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->b()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    const/16 v2, 0xa

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    int-to-float v0, p1

    const v1, 0x3e2aaaab

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v2, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->t:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_2
    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->u:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->t:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->u:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    if-eqz v2, :cond_6

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4b

    if-le v2, v1, :cond_5

    if-lez v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->n()V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->t:I

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->u:I

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->o()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->j()V

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->w:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g()V

    goto :goto_0

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->F:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Z)V

    goto :goto_0
.end method

.method public setAutoHide(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->E:Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->m()V

    goto :goto_0
.end method

.method public setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    return-void
.end method

.method public setFullscreen(Z)V
    .locals 3

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->y:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->x:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->x:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_2

    const v0, 0x7f0b008b

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->k()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const v0, 0x7f0b008a

    goto :goto_1
.end method

.method public setHQ(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    if-eqz p1, :cond_0

    const v0, 0x7f0b0091

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const v0, 0x7f0b0090

    goto :goto_0
.end method

.method public setHQisHD(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->f:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const v0, 0x7f0201d0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    return-void

    :cond_0
    const v0, 0x7f0201d1

    goto :goto_0
.end method

.method public setHasCc(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->z:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    return-void
.end method

.method public setHasNext(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->B:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    return-void
.end method

.method public setHasPrevious(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->C:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    return-void
.end method

.method public setHideOnTap(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->F:Z

    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/overlay/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/overlay/e;

    return-void
.end method

.method public setLoading()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l()V

    goto :goto_0
.end method

.method public setLoadingView(Landroid/view/View;Landroid/widget/LinearLayout$LayoutParams;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setLockedInFullscreen(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->y:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->x:Z

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setFullscreen(Z)V

    return-void
.end method

.method public setPlaying()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->s:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->l()V

    goto :goto_0
.end method

.method public setScrubbingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setEnabled(Z)V

    return-void
.end method

.method public setShowButtonsWhenNotFullscreen(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->I:Z

    return-void
.end method

.method public setShowFullscreen(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->A:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    return-void
.end method

.method public setShowFullscreenInPortrait(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->H:Z

    return-void
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->G:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->k()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget v1, p1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->progressColor:I

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setProgressColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-boolean v1, p1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsBuffered:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowBuffered(Z)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->v:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-boolean v1, p1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsScrubber:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowScrubber(Z)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    return-void
.end method

.method public setSupportsQualityToggle(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->D:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->p()V

    return-void
.end method

.method public setTimes(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->e:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setTime(III)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->o:Lcom/google/android/youtube/core/player/overlay/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/p;->a(II)V

    return-void
.end method
