.class public final Lcom/google/android/youtube/core/player/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/utils/e;

.field private final b:Ljava/util/LinkedList;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/utils/e;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "clock can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/c;->a:Lcom/google/android/youtube/core/utils/e;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/c;->c:Z

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/c;->b:Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/c;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    return-void
.end method

.method private a()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/core/player/c;->a:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v0

    const-wide/32 v2, 0x36ee80

    sub-long v1, v0, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/c;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/c;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/c;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 3

    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Video;->moderatedAutoplay:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/c;->a:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/c;->a()V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/c;->b:Ljava/util/LinkedList;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/model/Video;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/c;->a()V

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/c;->c:Z

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->owner:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->e(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    iget-boolean v2, p1, Lcom/google/android/youtube/core/model/Video;->moderatedAutoplay:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/c;->b:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
