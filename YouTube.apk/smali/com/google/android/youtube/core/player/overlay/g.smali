.class final Lcom/google/android/youtube/core/player/overlay/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/q;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/g;-><init>(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setScrubberTime(I)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->a()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->k()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->b(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(I)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->k()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->b()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->d(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/g;->a:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->g()V

    :cond_0
    return-void
.end method
