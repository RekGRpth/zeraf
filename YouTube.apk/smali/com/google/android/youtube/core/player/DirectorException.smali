.class public final Lcom/google/android/youtube/core/player/DirectorException;
.super Ljava/lang/Exception;
.source "SourceFile"


# instance fields
.field public final isRetriable:Z

.field public final reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iput-boolean p2, p0, Lcom/google/android/youtube/core/player/DirectorException;->isRetriable:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p3, p4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object p1, p0, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iput-boolean p2, p0, Lcom/google/android/youtube/core/player/DirectorException;->isRetriable:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;ZLjava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    iput-object p1, p0, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iput-boolean p2, p0, Lcom/google/android/youtube/core/player/DirectorException;->isRetriable:Z

    return-void
.end method
