.class public Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ai;


# instance fields
.field private final a:Ljava/util/List;

.field private b:Lcom/google/android/youtube/core/player/aj;

.field private c:Lcom/google/android/youtube/core/player/overlay/t;

.field private d:Landroid/view/View;

.field private e:Z

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->a:Ljava/util/List;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setFocusable(Z)V

    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setDescendantFocusability(I)V

    return-void
.end method

.method private a(Landroid/util/AttributeSet;)Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private a()V
    .locals 4

    const v2, 0x3d99999a

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    :goto_0
    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->f:I

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->e:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    :goto_1
    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->g:I

    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/r;

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->f:I

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->g:I

    invoke-interface {v0, v2, v3}, Lcom/google/android/youtube/core/player/overlay/r;->a(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public final varargs a([Lcom/google/android/youtube/core/player/overlay/s;)V
    .locals 5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_2

    aget-object v0, p1, v1

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/s;->b()Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Overlay "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " does not provide a View and LayoutParams"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    instance-of v3, v0, Lcom/google/android/youtube/core/player/overlay/r;

    if-eqz v3, :cond_1

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/r;

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->f:I

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->g:I

    invoke-interface {v0, v3, v4}, Lcom/google/android/youtube/core/player/overlay/r;->a(II)V

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    aget-object v0, p1, v1

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/s;->c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    return v0
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->c:Lcom/google/android/youtube/core/player/overlay/t;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->c:Lcom/google/android/youtube/core/player/overlay/t;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/overlay/t;->a(Landroid/graphics/Rect;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    const/4 v2, -0x2

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->a(Landroid/util/AttributeSet;)Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->a(Landroid/util/AttributeSet;)Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 8

    const/4 v1, 0x0

    const/high16 v7, -0x80000000

    const v6, 0x3fe374bc

    const/high16 v5, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    if-ne v3, v5, :cond_0

    if-ne v4, v5, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->resolveSize(II)I

    move-result v1

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->resolveSize(II)I

    move-result v0

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    return-void

    :cond_0
    if-eq v3, v5, :cond_1

    if-ne v3, v7, :cond_2

    if-nez v4, :cond_2

    :cond_1
    int-to-float v0, v2

    div-float/2addr v0, v6

    float-to-int v0, v0

    move v1, v2

    goto :goto_0

    :cond_2
    if-eq v4, v5, :cond_3

    if-ne v4, v7, :cond_4

    if-nez v3, :cond_4

    :cond_3
    int-to-float v1, v0

    mul-float/2addr v1, v6

    float-to-int v1, v1

    goto :goto_0

    :cond_4
    if-ne v3, v7, :cond_6

    if-ne v4, v7, :cond_6

    int-to-float v1, v0

    int-to-float v3, v2

    div-float/2addr v3, v6

    cmpg-float v1, v1, v3

    if-gez v1, :cond_5

    int-to-float v1, v0

    mul-float/2addr v1, v6

    float-to-int v1, v1

    goto :goto_0

    :cond_5
    int-to-float v0, v2

    div-float/2addr v0, v6

    float-to-int v0, v0

    move v1, v2

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->e:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->a()V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->b:Lcom/google/android/youtube/core/player/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->b:Lcom/google/android/youtube/core/player/aj;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aj;->b()V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setListener(Lcom/google/android/youtube/core/player/aj;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->b:Lcom/google/android/youtube/core/player/aj;

    return-void
.end method

.method public setMakeSafeForOverscan(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->e:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->e:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->a()V

    :cond_0
    return-void
.end method

.method public setSystemWindowInsetsListener(Lcom/google/android/youtube/core/player/overlay/t;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->c:Lcom/google/android/youtube/core/player/overlay/t;

    return-void
.end method

.method public setVideoView(Landroid/view/View;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, -0x2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->d:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "videoView has already been set"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->d:Landroid/view/View;

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;->addRule(I)V

    invoke-virtual {p0, p1, v1, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method
