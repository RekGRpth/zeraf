.class public final Lcom/google/android/youtube/core/player/Director;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/b;
.implements Lcom/google/android/youtube/core/player/overlay/b;
.implements Lcom/google/android/youtube/core/player/overlay/j;


# instance fields
.field private A:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field private B:Lcom/google/android/youtube/core/client/AdStatsClient;

.field private C:Lcom/google/android/youtube/core/client/QoeStatsClient;

.field private D:Z

.field private E:I

.field private F:I

.field private G:Ljava/util/Map;

.field private H:Lcom/google/android/youtube/core/model/Video;

.field private I:Lcom/google/android/youtube/core/model/ak;

.field private J:Lcom/google/android/youtube/core/model/ak;

.field private K:Lcom/google/android/youtube/core/model/VastAd;

.field private L:Z

.field private M:Z

.field private final N:Z

.field private O:Lcom/google/android/youtube/core/async/a/a;

.field private P:Lcom/google/android/youtube/core/async/p;

.field private Q:Lcom/google/android/youtube/core/async/p;

.field private R:Z

.field private S:Z

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Lcom/google/android/youtube/core/player/DirectorException;

.field private final Y:Lcom/google/android/youtube/core/player/c;

.field private Z:Z

.field private final a:Lcom/google/android/youtube/core/player/ai;

.field private final aa:Lcom/google/android/youtube/core/player/v;

.field private final ab:Ljava/lang/String;

.field private ac:Lcom/google/android/youtube/core/player/Director$PlayerState;

.field private ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

.field private ae:Lcom/google/android/youtube/core/async/n;

.field private af:Lcom/google/android/youtube/core/async/n;

.field private ag:Lcom/google/android/youtube/core/async/n;

.field private ah:Lcom/google/android/youtube/core/async/n;

.field private ai:Lcom/google/android/youtube/core/player/al;

.field private aj:Lcom/google/android/youtube/core/player/al;

.field private final b:Landroid/os/Handler;

.field private final c:Lcom/google/android/youtube/core/player/as;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/youtube/core/Analytics;

.field private final f:Landroid/content/SharedPreferences;

.field private final g:Lcom/google/android/youtube/core/utils/e;

.field private final h:Lcom/google/android/youtube/core/client/bc;

.field private final i:Lcom/google/android/youtube/core/client/be;

.field private final j:Lcom/google/android/youtube/core/utils/p;

.field private final k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

.field private final l:Lcom/google/android/youtube/core/player/overlay/a;

.field private final m:Lcom/google/android/youtube/core/player/overlay/i;

.field private final n:Lcom/google/android/youtube/core/player/overlay/ab;

.field private final o:Lcom/google/android/youtube/core/player/ap;

.field private final p:Lcom/google/android/youtube/core/player/an;

.field private final q:Lcom/google/android/youtube/core/player/d;

.field private final r:Lcom/google/android/youtube/core/player/a;

.field private final s:Lcom/google/android/youtube/core/client/d;

.field private final t:Lcom/google/android/youtube/core/client/bo;

.field private final u:Lcom/google/android/youtube/core/client/b;

.field private final v:Lcom/google/android/youtube/core/client/bj;

.field private final w:Lcom/google/android/youtube/core/player/ak;

.field private final x:Lcom/google/android/youtube/core/e;

.field private final y:Z

.field private z:Lcom/google/android/youtube/core/client/VideoStats2Client;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/as;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/d;Lcom/google/android/youtube/core/client/bo;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/v;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/ab;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/core/player/ak;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/client/b;Lcom/google/android/youtube/core/client/bj;Z)V
    .locals 11

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "playerUi cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/ai;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->a:Lcom/google/android/youtube/core/player/ai;

    const-string v1, "player cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/as;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    const-string v1, "context cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->d:Landroid/content/Context;

    const-string v1, "preferences cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->f:Landroid/content/SharedPreferences;

    const-string v1, "preferences cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "gdataClient cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/bc;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/client/bc;

    const-string v1, "imageClient cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/be;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/client/be;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->s:Lcom/google/android/youtube/core/client/d;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Lcom/google/android/youtube/core/client/b;

    const-string v1, "qoeStatsClientFactory cannot be null"

    move-object/from16 v0, p25

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/bj;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->v:Lcom/google/android/youtube/core/client/bj;

    const-string v1, "statsClientFactory cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/bo;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->t:Lcom/google/android/youtube/core/client/bo;

    const-string v1, "subtitlesClient cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "adultContentHelper cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/a;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->r:Lcom/google/android/youtube/core/player/a;

    const-string v1, "analytics cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/Analytics;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ab:Ljava/lang/String;

    const-string v1, "listener cannot be null"

    move-object/from16 v0, p13

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/v;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    const-string v1, "controllerOverlay cannot be null"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    const-string v1, "brandingOverlay cannot be null"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/overlay/ab;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    const-string v1, "liveOverlay cannot be null"

    move-object/from16 v0, p18

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/overlay/i;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->m:Lcom/google/android/youtube/core/player/overlay/i;

    const-string v1, "subtitlesOverlay cannot be null"

    move-object/from16 v0, p19

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "errorHelper cannot be null"

    move-object/from16 v0, p20

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/e;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->x:Lcom/google/android/youtube/core/e;

    const-string v1, "networkStatus cannot be null"

    move-object/from16 v0, p21

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/utils/p;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->j:Lcom/google/android/youtube/core/utils/p;

    const-string v1, "streamSelector cannot be null"

    move-object/from16 v0, p22

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/ak;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->w:Lcom/google/android/youtube/core/player/ak;

    const-string v1, "autoplayHelper cannot be null"

    move-object/from16 v0, p23

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/c;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->Y:Lcom/google/android/youtube/core/player/c;

    move/from16 v0, p26

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->y:Z

    sget-object v1, Lcom/google/android/youtube/core/player/Director$PlayerState;->NOT_PREPARED:Lcom/google/android/youtube/core/player/Director$PlayerState;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->ac:Lcom/google/android/youtube/core/player/Director$PlayerState;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->NEW:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    sget-object v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->NO_FEATURE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/core/player/w;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/core/player/w;-><init>(Lcom/google/android/youtube/core/player/Director;B)V

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v1}, Lcom/google/android/youtube/core/utils/ag;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->g:Lcom/google/android/youtube/core/utils/e;

    new-instance v1, Lcom/google/android/youtube/core/player/x;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/core/player/x;-><init>(Lcom/google/android/youtube/core/player/Director;B)V

    invoke-interface {p1, v1}, Lcom/google/android/youtube/core/player/ai;->setListener(Lcom/google/android/youtube/core/player/aj;)V

    const/4 v1, 0x0

    move-object/from16 v0, p14

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setScrubbingEnabled(Z)V

    new-instance v1, Lcom/google/android/youtube/core/player/t;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/t;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    move-object/from16 v0, p14

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setListener(Lcom/google/android/youtube/core/player/overlay/e;)V

    if-eqz p17, :cond_0

    move-object/from16 v0, p17

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/overlay/a;->setListener(Lcom/google/android/youtube/core/player/overlay/b;)V

    :cond_0
    move-object/from16 v0, p18

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/overlay/i;->setListener(Lcom/google/android/youtube/core/player/overlay/j;)V

    const-string v1, "default_hq"

    const/4 v2, 0x0

    invoke-interface {p4, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface/range {p22 .. p22}, Lcom/google/android/youtube/core/player/ak;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    move-object/from16 v0, p14

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHQ(Z)V

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->N:Z

    new-instance v4, Lcom/google/android/youtube/core/player/y;

    const/4 v1, 0x0

    invoke-direct {v4, p0, v1}, Lcom/google/android/youtube/core/player/y;-><init>(Lcom/google/android/youtube/core/player/Director;B)V

    new-instance v1, Lcom/google/android/youtube/core/player/an;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->b:Landroid/os/Handler;

    const v3, 0x7f0b0030

    invoke-virtual {p3, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v3, p4

    move-object/from16 v5, p9

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/core/player/an;-><init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/player/ao;Lcom/google/android/youtube/core/client/bl;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/player/an;

    new-instance v5, Lcom/google/android/youtube/core/player/ap;

    iget-object v6, p0, Lcom/google/android/youtube/core/player/Director;->b:Landroid/os/Handler;

    move-object v7, p4

    move-object/from16 v8, p19

    move-object v9, v4

    move-object/from16 v10, p9

    invoke-direct/range {v5 .. v10}, Lcom/google/android/youtube/core/player/ap;-><init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/player/aq;Lcom/google/android/youtube/core/client/bl;)V

    iput-object v5, p0, Lcom/google/android/youtube/core/player/Director;->o:Lcom/google/android/youtube/core/player/ap;

    new-instance v1, Lcom/google/android/youtube/core/player/d;

    new-instance v5, Lcom/google/android/youtube/core/player/s;

    const/4 v2, 0x0

    invoke-direct {v5, p0, v2}, Lcom/google/android/youtube/core/player/s;-><init>(Lcom/google/android/youtube/core/player/Director;B)V

    iget-object v6, p0, Lcom/google/android/youtube/core/player/Director;->b:Landroid/os/Handler;

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    move-object/from16 v4, p15

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/core/player/d;-><init>(Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/f;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/player/d;

    new-instance v1, Lcom/google/android/youtube/core/player/m;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/m;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->ae:Lcom/google/android/youtube/core/async/n;

    new-instance v1, Lcom/google/android/youtube/core/player/n;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/n;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->af:Lcom/google/android/youtube/core/async/n;

    new-instance v1, Lcom/google/android/youtube/core/player/o;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/o;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->aj:Lcom/google/android/youtube/core/player/al;

    new-instance v1, Lcom/google/android/youtube/core/player/p;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/p;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->ag:Lcom/google/android/youtube/core/async/n;

    new-instance v1, Lcom/google/android/youtube/core/player/q;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/q;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->ah:Lcom/google/android/youtube/core/async/n;

    new-instance v1, Lcom/google/android/youtube/core/player/r;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/r;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->ai:Lcom/google/android/youtube/core/player/al;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->b:Landroid/os/Handler;

    invoke-virtual {p2, v1}, Lcom/google/android/youtube/core/player/as;->a(Landroid/os/Handler;)V

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic A(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/an;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/player/an;

    return-object v0
.end method

.method private A()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->y:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private B()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->isDummy()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->g:Lcom/google/android/youtube/core/utils/e;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/VastAd;->hasExpired(Lcom/google/android/youtube/core/utils/e;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->A()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private C()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->AD:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->LIVE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isMovie()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->MOVIE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->y:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->EMBEDDED:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->N:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->YOUTUBE_TABLET:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->YOUTUBE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    goto :goto_0
.end method

.method private D()V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget v1, p0, Lcom/google/android/youtube/core/player/Director;->F:I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget v2, v2, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    mul-int/lit16 v2, v2, 0x3e8

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setTimes(III)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget v1, p0, Lcom/google/android/youtube/core/player/Director;->E:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->s()I

    move-result v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setTimes(III)V

    goto :goto_0
.end method

.method private E()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->X:Lcom/google/android/youtube/core/player/DirectorException;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->X:Lcom/google/android/youtube/core/player/DirectorException;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/DirectorException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->X:Lcom/google/android/youtube/core/player/DirectorException;

    iget-boolean v2, v2, Lcom/google/android/youtube/core/player/DirectorException;->isRetriable:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->a(Ljava/lang/String;Z)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->NEW:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADING:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ac:Lcom/google/android/youtube/core/player/Director$PlayerState;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$PlayerState;->PREPARING:Lcom/google/android/youtube/core/player/Director$PlayerState;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setLoading()V

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ENDED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->e()V

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_AD:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_VIDEO:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setPlaying()V

    goto :goto_1

    :cond_6
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ENDED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-nez v0, :cond_9

    :cond_7
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_AD:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_VIDEO:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->b()Z

    move-result v0

    if-nez v0, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->d()V

    goto/16 :goto_1

    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setLoading()V

    goto/16 :goto_1
.end method

.method private F()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/VideoStats2Client;->f()V

    :cond_0
    return-void
.end method

.method private G()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->f()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/async/p;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->Q:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->Q:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->Q:Lcom/google/android/youtube/core/async/p;

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/player/an;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/an;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->o:Lcom/google/android/youtube/core/player/ap;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ap;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/player/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/d;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->r:Lcom/google/android/youtube/core/player/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/a;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->i()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/overlay/ab;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/overlay/ab;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ab;->f()V

    :cond_3
    return-void
.end method

.method private H()I
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_AD:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/player/Director;->F:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/player/Director;->E:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method private a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/n;
    .locals 2

    invoke-static {p1}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/async/p;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->b:Landroid/os/Handler;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;
    .locals 4

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v0, "dnc"

    const-string v1, "1"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ab:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "androidcid"

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->ab:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->G:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->G:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/player/Director$PlayerState;)Lcom/google/android/youtube/core/player/Director$PlayerState;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/Director;->ac:Lcom/google/android/youtube/core/player/Director$PlayerState;

    return-object p1
.end method

.method public static a(Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/as;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/d;Lcom/google/android/youtube/core/client/bo;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/v;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/ab;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/core/player/ak;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/client/b;Lcom/google/android/youtube/core/client/bj;)Lcom/google/android/youtube/core/player/Director;
    .locals 28

    const-string v1, "adsClient cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "thumbnailOverlay cannot be null"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "adOverlay cannot be null"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "adStatsClientFactory cannot be null"

    move-object/from16 v0, p22

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "qoeStatsClientFactory cannot be null"

    move-object/from16 v0, p23

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/youtube/core/player/Director;

    const/4 v13, 0x0

    const/16 v27, 0x1

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    move-object/from16 v16, p13

    move-object/from16 v17, p14

    move-object/from16 v18, p15

    move-object/from16 v19, p16

    move-object/from16 v20, p17

    move-object/from16 v21, p18

    move-object/from16 v22, p19

    move-object/from16 v23, p20

    move-object/from16 v24, p21

    move-object/from16 v25, p22

    move-object/from16 v26, p23

    invoke-direct/range {v1 .. v27}, Lcom/google/android/youtube/core/player/Director;-><init>(Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/as;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/d;Lcom/google/android/youtube/core/client/bo;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/v;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/ab;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/core/player/ak;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/client/b;Lcom/google/android/youtube/core/client/bj;Z)V

    return-object v1
.end method

.method public static a(Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/as;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/d;Lcom/google/android/youtube/core/client/bo;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/v;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/core/player/ak;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/client/b;Lcom/google/android/youtube/core/client/bj;)Lcom/google/android/youtube/core/player/Director;
    .locals 28

    const-string v1, "adsClient cannot be null: use createAdFreeDirector"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "revShareClientId cannot be empty: use createAdFreeDirector"

    move-object/from16 v0, p11

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v1, "adOverlay cannot be empty: use createAdFreeDirector"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "adStatsClientFactory cannot be null"

    move-object/from16 v0, p22

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "qoeStatsClientFactory cannot be null"

    move-object/from16 v0, p23

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/youtube/core/player/Director;

    const/16 v17, 0x0

    const/16 v27, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move-object/from16 v18, p15

    move-object/from16 v19, p16

    move-object/from16 v20, p17

    move-object/from16 v21, p18

    move-object/from16 v22, p19

    move-object/from16 v23, p20

    move-object/from16 v24, p21

    move-object/from16 v25, p22

    move-object/from16 v26, p23

    invoke-direct/range {v1 .. v27}, Lcom/google/android/youtube/core/player/Director;-><init>(Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/as;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/d;Lcom/google/android/youtube/core/client/bo;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/v;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/ab;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/core/player/ak;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/client/b;Lcom/google/android/youtube/core/client/bj;Z)V

    return-object v1
.end method

.method private a(Lcom/google/android/youtube/core/async/a/a;ZI)V
    .locals 9

    const/4 v5, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v6, v5

    move-object v7, v4

    move-object v8, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;ZILcom/google/android/youtube/core/model/VastAd;IZLcom/google/android/youtube/core/client/VideoStats2Client;Lcom/google/android/youtube/core/client/QoeStatsClient;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/async/a/a;ZILcom/google/android/youtube/core/model/VastAd;IZLcom/google/android/youtube/core/client/VideoStats2Client;Lcom/google/android/youtube/core/client/QoeStatsClient;)V
    .locals 1

    iput-boolean p2, p0, Lcom/google/android/youtube/core/player/Director;->S:Z

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/a/a;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    iput p3, p0, Lcom/google/android/youtube/core/player/Director;->E:I

    iput-object p4, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iput p5, p0, Lcom/google/android/youtube/core/player/Director;->F:I

    iput-boolean p6, p0, Lcom/google/android/youtube/core/player/Director;->W:Z

    iput-object p7, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    iput-object p8, p0, Lcom/google/android/youtube/core/player/Director;->C:Lcom/google/android/youtube/core/client/QoeStatsClient;

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->w:Lcom/google/android/youtube/core/player/ak;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/as;->a()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->ai:Lcom/google/android/youtube/core/player/al;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/youtube/core/player/ak;->a(Lcom/google/android/youtube/core/model/Video;Ljava/util/Set;Lcom/google/android/youtube/core/player/al;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;II)V
    .locals 3

    const/16 v1, 0x195

    const/16 v0, 0x191

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->j:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v2}, Lcom/google/android/youtube/core/utils/p;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    if-ne p1, v2, :cond_3

    const/16 v2, -0x3ea

    if-ne p2, v2, :cond_1

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/client/AdStatsClient;->a(I)V

    return-void

    :cond_1
    const/16 v2, -0x3eb

    if-eq p2, v2, :cond_0

    const/16 v0, -0x3ed

    if-ne p2, v0, :cond_2

    const/16 v0, 0x192

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/model/LiveEvent;)V
    .locals 6

    iget-object v0, p1, Lcom/google/android/youtube/core/model/LiveEvent;->video:Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/LiveEvent;->isUpcoming()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->h()V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->m:Lcom/google/android/youtube/core/player/overlay/i;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/LiveEvent;->start:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/LiveEvent;->isPlayable()Z

    move-result v4

    iget-object v0, p1, Lcom/google/android/youtube/core/model/LiveEvent;->status:Lcom/google/android/youtube/core/model/LiveEvent$Status;

    sget-object v5, Lcom/google/android/youtube/core/model/LiveEvent$Status;->COMPLETED:Lcom/google/android/youtube/core/model/LiveEvent$Status;

    if-ne v0, v5, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/youtube/core/player/overlay/i;->a(JZZ)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/model/Video;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/model/VastAd;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Lcom/google/android/youtube/core/client/b;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/client/b;->a(Lcom/google/android/youtube/core/model/VastAd;)Lcom/google/android/youtube/core/client/AdStatsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/AdStatsClient;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->w:Lcom/google/android/youtube/core/player/ak;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/as;->a()Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->aj:Lcom/google/android/youtube/core/player/al;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/player/ak;->a(Ljava/util/Collection;Ljava/util/Set;Lcom/google/android/youtube/core/player/al;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->y()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/model/Video;)V
    .locals 8

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->EMPTY_PLAYLIST:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/v;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->S:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->Z:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->Y:Lcom/google/android/youtube/core/player/c;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/c;->b(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->AUTOPLAY_DENIED:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/v;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->D()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->C()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setScrubbingEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v3}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v3

    invoke-interface {v0, v3}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHasNext(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v3}, Lcom/google/android/youtube/core/async/a/a;->e()Z

    move-result v3

    invoke-interface {v0, v3}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHasPrevious(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/v;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/player/d;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/d;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-eq v0, v3, :cond_2

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$State;->PROCESSING:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v3, :cond_5

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    move v0, v2

    :goto_1
    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/Director;->y:Z

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/Director;->y:Z

    if-eqz v3, :cond_6

    iget-boolean v3, p1, Lcom/google/android/youtube/core/model/Video;->embedAllowed:Z

    if-eqz v3, :cond_6

    :cond_3
    move v3, v2

    :goto_2
    if-eqz v0, :cond_4

    if-nez v3, :cond_a

    :cond_4
    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v4, "PlayCannotProceeed"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v2, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->EMBEDDING_DISABLED:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->d:Landroid/content/Context;

    const v4, 0x7f0b0051

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;ZLjava/lang/String;)V

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/DirectorException;)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v3, v1

    goto :goto_2

    :cond_7
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$State;->PROCESSING:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v3, :cond_8

    new-instance v0, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v3, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->BAD_STATE:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    const-string v4, "%s\n%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/youtube/core/player/Director;->d:Landroid/content/Context;

    iget-object v7, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    iget v7, v7, Lcom/google/android/youtube/core/model/Video$State;->explanationId:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    iget-object v6, p0, Lcom/google/android/youtube/core/player/Director;->d:Landroid/content/Context;

    const v7, 0x7f0b0049

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;ZLjava/lang/String;)V

    goto :goto_3

    :cond_8
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->y:Z

    if-eqz v0, :cond_9

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v2, Lcom/google/android/youtube/core/model/Video$State;->BLOCKED_FOR_CLIENT_APP:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v2, :cond_9

    new-instance v0, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v2, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->BLOCKED_FOR_CLIENT_APP:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->d:Landroid/content/Context;

    iget-object v4, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    iget v4, v4, Lcom/google/android/youtube/core/model/Video$State;->explanationId:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;ZLjava/lang/String;)V

    goto :goto_3

    :cond_9
    new-instance v0, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v2, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->BAD_STATE:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->d:Landroid/content/Context;

    iget-object v4, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    iget v4, v4, Lcom/google/android/youtube/core/model/Video$State;->explanationId:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;ZLjava/lang/String;)V

    goto :goto_3

    :cond_a
    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Video;->adultContent:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->r:Lcom/google/android/youtube/core/player/a;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/a;->a(Lcom/google/android/youtube/core/player/b;)V

    goto/16 :goto_0

    :cond_b
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->x()V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/model/ak;)V
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/model/ak;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/ak;->b:Lcom/google/android/youtube/core/model/Stream;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/ak;->c:Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/model/ak;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/core/model/Stream;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->J:Lcom/google/android/youtube/core/model/ak;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->y()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/player/DirectorException;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/DirectorException;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Ljava/lang/Exception;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->j:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v2, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->x:Lcom/google/android/youtube/core/e;

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3, p1}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/DirectorException;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v1}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHasNext(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v1}, Lcom/google/android/youtube/core/async/a/a;->e()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHasPrevious(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Ljava/lang/Object;II)V
    .locals 4

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_AD:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "AdPlayError"

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->H()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "PlayErrorException"

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->r()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    if-eqz p3, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PlayErrorMediaUnknown"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->r()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PlayError"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->r()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/player/DirectorException;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/youtube/core/player/Director;->X:Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PlayCannotProceeed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->E()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/v;->a(Lcom/google/android/youtube/core/player/DirectorException;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->NEW:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/model/ak;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    move-object v2, v0

    :goto_0
    if-nez v2, :cond_2

    const-string v0, "?"

    move-object v1, v0

    :goto_1
    if-eqz v2, :cond_3

    iget-boolean v0, v2, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    const/4 v2, -0x1

    if-eq p2, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    invoke-interface {v2, p1, v1, v0, p2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;ZI)V

    :goto_3
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/model/ak;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ak;->b:Lcom/google/android/youtube/core/model/Stream;

    move-object v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/model/ak;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ak;->c:Lcom/google/android/youtube/core/model/Stream;

    move-object v2, v0

    goto :goto_0

    :cond_2
    iget-object v0, v2, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream$Quality;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    invoke-interface {v2, p1, v1, v0}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_3
.end method

.method private a(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->D:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/player/DirectorException;)Lcom/google/android/youtube/core/player/DirectorException;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->X:Lcom/google/android/youtube/core/player/DirectorException;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/Director;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->y()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/model/ak;)V
    .locals 4

    new-instance v0, Lcom/google/android/youtube/core/model/ak;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/ak;->b:Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/ak;->c:Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/model/ak;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/core/model/Stream;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/model/ak;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/ak;->b:Lcom/google/android/youtube/core/model/Stream;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Stream;->isHD()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHQisHD(Z)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->s:Lcom/google/android/youtube/core/client/d;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->A()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Video;->isMonetized(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->s:Lcom/google/android/youtube/core/client/d;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/client/d;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->s:Lcom/google/android/youtube/core/client/d;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->af:Lcom/google/android/youtube/core/async/n;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/n;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->w:Lcom/google/android/youtube/core/player/ak;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/as;->a()Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->aj:Lcom/google/android/youtube/core/player/al;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/player/ak;->a(Ljava/util/Collection;Ljava/util/Set;Lcom/google/android/youtube/core/player/al;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->y()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/Director;Ljava/lang/Exception;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->j:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v2, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->x:Lcom/google/android/youtube/core/e;

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3, p1}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/DirectorException;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/Director;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->Z:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/Director;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    const/16 v1, 0x193

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/client/AdStatsClient;->a(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->y()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/Director;Ljava/lang/Exception;)V
    .locals 4

    new-instance v0, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v1, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->NO_STREAMS:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->x:Lcom/google/android/youtube/core/e;

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/DirectorException;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/Director;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/Director;->i(Z)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/overlay/ab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/overlay/ab;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/Director$PlayerState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ac:Lcom/google/android/youtube/core/player/Director$PlayerState;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/client/VideoStats2Client;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/client/AdStatsClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/client/QoeStatsClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Lcom/google/android/youtube/core/client/QoeStatsClient;

    return-object v0
.end method

.method private i(Z)V
    .locals 2

    const/4 v1, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->s:Lcom/google/android/youtube/core/client/d;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/d;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/VideoStats2Client;->f()V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->C:Lcom/google/android/youtube/core/client/QoeStatsClient;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->z()V

    return-void
.end method

.method static synthetic j(Lcom/google/android/youtube/core/player/Director;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->E()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/youtube/core/player/Director;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->D:Z

    return v0
.end method

.method static synthetic l(Lcom/google/android/youtube/core/player/Director;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->Z:Z

    return v0
.end method

.method static synthetic m(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/model/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->Y:Lcom/google/android/youtube/core/player/c;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->o:Lcom/google/android/youtube/core/player/ap;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->a:Lcom/google/android/youtube/core/player/ai;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/overlay/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/youtube/core/player/Director;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    return v0
.end method

.method static synthetic t(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/model/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/model/ak;

    return-object v0
.end method

.method static synthetic u(Lcom/google/android/youtube/core/player/Director;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    return v0
.end method

.method static synthetic v(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/v;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    return-object v0
.end method

.method private v()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->G()V

    sget-object v2, Lcom/google/android/youtube/core/player/Director$PlayerState;->NOT_PREPARED:Lcom/google/android/youtube/core/player/Director$PlayerState;

    iput-object v2, p0, Lcom/google/android/youtube/core/player/Director;->ac:Lcom/google/android/youtube/core/player/Director$PlayerState;

    sget-object v2, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADING:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iput-object v2, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/model/ak;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->J:Lcom/google/android/youtube/core/model/ak;

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->D:Z

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->X:Lcom/google/android/youtube/core/player/DirectorException;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    invoke-interface {v2}, Lcom/google/android/youtube/core/player/v;->a()V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/as;->i()V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/as;->g()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->C()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->D()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->E()V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setScrubbingEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->B()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-boolean v2, v2, Lcom/google/android/youtube/core/model/VastAd;->isPublicVideo:Z

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/model/VastAd;->isSkippable()Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    :cond_1
    iget-object v4, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    invoke-interface {v2, v0, v3, v1, v4}, Lcom/google/android/youtube/core/player/overlay/a;->a(Ljava/lang/String;ZZLjava/lang/String;)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->m:Lcom/google/android/youtube/core/player/overlay/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/i;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/overlay/ab;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/overlay/ab;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ab;->e()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/v;->h_()V

    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/a;->d()V

    goto :goto_0
.end method

.method private w()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->v()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->a()V

    return-void
.end method

.method static synthetic w(Lcom/google/android/youtube/core/player/Director;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/v;->j_()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->Z:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/Director;->c(Z)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ENDED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->E()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->ITERATOR_FINISHED:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/v;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    goto :goto_0
.end method

.method static synthetic x(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/utils/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->j:Lcom/google/android/youtube/core/utils/p;

    return-object v0
.end method

.method private x()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->liveEventUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->ag:Lcom/google/android/youtube/core/async/n;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/n;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/model/Video;)V

    goto :goto_0
.end method

.method static synthetic y(Lcom/google/android/youtube/core/player/Director;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->d:Landroid/content/Context;

    return-object v0
.end method

.method private y()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->W:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ENDED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/v;->b()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->S:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->D()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->E()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/overlay/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ah:Lcom/google/android/youtube/core/async/n;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->Q:Lcom/google/android/youtube/core/async/p;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/client/be;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->b:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->Q:Lcom/google/android/youtube/core/async/p;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/overlay/ab;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ab;->d()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    goto :goto_1
.end method

.method private z()V
    .locals 8

    const/4 v6, 0x0

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ENDED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v6, p0, Lcom/google/android/youtube/core/player/Director;->E:I

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_VIDEO:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/a;->d()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->D()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->C()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/player/an;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    const-string v1, "video cannot be null"

    invoke-static {v5, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v5, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, v5, Lcom/google/android/youtube/core/model/Video;->captionTracksUri:Landroid/net/Uri;

    iget-boolean v3, v5, Lcom/google/android/youtube/core/model/Video;->showSubtitlesByDefault:Z

    iget-boolean v4, v5, Lcom/google/android/youtube/core/model/Video;->showSubtitlesAlways:Z

    invoke-virtual {v5}, Lcom/google/android/youtube/core/model/Video;->getDefaultSubtitleLanguageCode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/core/player/an;->a(Ljava/lang/String;Landroid/net/Uri;ZZLjava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/player/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/d;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/model/ak;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/model/ak;->a:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setSupportsQualityToggle(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHQ(Z)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/v;->b(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->a:Lcom/google/android/youtube/core/player/ai;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ai;->setKeepScreenOn(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, v6}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setScrubbingEnabled(Z)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/model/ak;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ak;->b:Lcom/google/android/youtube/core/model/Stream;

    move-object v7, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->t:Lcom/google/android/youtube/core/client/bo;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget v2, v2, Lcom/google/android/youtube/core/model/Video;->duration:I

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/Director;->Z:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->A()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget-boolean v5, v5, Lcom/google/android/youtube/core/model/Video;->claimed:Z

    iget-object v6, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/youtube/core/client/bo;->a(Ljava/lang/String;IZZZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/VideoStats2Client;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, v7, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "cpn"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v7}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->v:Lcom/google/android/youtube/core/client/bj;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v3

    invoke-virtual {v7}, Lcom/google/android/youtube/core/model/Stream;->getItag()I

    move-result v4

    invoke-interface {v1, v0, v2, v3, v4}, Lcom/google/android/youtube/core/client/bj;->a(Ljava/lang/String;Ljava/lang/String;ZI)Lcom/google/android/youtube/core/client/QoeStatsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Lcom/google/android/youtube/core/client/QoeStatsClient;

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0, v7}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/model/Stream;)V

    :goto_1
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/model/ak;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ak;->c:Lcom/google/android/youtube/core/model/Stream;

    move-object v7, v0

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    iget v1, p0, Lcom/google/android/youtube/core/player/Director;->E:I

    invoke-virtual {v0, v7, v1}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/model/Stream;I)V

    goto :goto_1
.end method

.method static synthetic z(Lcom/google/android/youtube/core/player/Director;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->S:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->x()V

    return-void
.end method

.method public final a(I)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ENDED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_VIDEO:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_VIDEO:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ac:Lcom/google/android/youtube/core/player/Director$PlayerState;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$PlayerState;->NOT_PREPARED:Lcom/google/android/youtube/core/player/Director$PlayerState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/as;->a(I)V

    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/android/youtube/core/player/Director;->E:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->D()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZI)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZILjava/util/Map;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZILjava/util/Map;)V
    .locals 7

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZILjava/util/Map;Z)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZILjava/util/Map;Z)V
    .locals 3

    const/4 v1, 0x0

    if-ltz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "startPosition has to be >= 0"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->h()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->F()V

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    iput-object p5, p0, Lcom/google/android/youtube/core/player/Director;->G:Ljava/util/Map;

    iput-boolean p6, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    iput-object p2, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;ZI)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ae:Lcom/google/android/youtube/core/async/n;

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/async/a/a;->a(Lcom/google/android/youtube/core/async/n;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->w()V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/player/Director$DirectorState;)V
    .locals 10

    const/4 v2, 0x1

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->h()V

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    iget-object v0, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->streamParams:Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->G:Ljava/util/Map;

    iget-boolean v0, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->userInitiatedPlayback:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    iget-object v0, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iget-object v0, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->g:Lcom/google/android/youtube/core/utils/e;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/VastAd;->hasExpired(Lcom/google/android/youtube/core/utils/e;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v4, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    :goto_0
    iget-object v0, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->statsClientState:Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    if-nez v0, :cond_1

    move-object v7, v9

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->v:Lcom/google/android/youtube/core/client/bj;

    iget-object v1, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->qoeStatsClientState:Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/client/bj;->a(Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;)Lcom/google/android/youtube/core/client/QoeStatsClient;

    move-result-object v8

    iget-boolean v0, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->isPlaying:Z

    if-nez v0, :cond_2

    :goto_2
    iget v3, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->videoStartPositionMillis:I

    iget v5, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStartPositionMillis:I

    iget-boolean v6, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->wasEnded:Z

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;ZILcom/google/android/youtube/core/model/VastAd;IZLcom/google/android/youtube/core/client/VideoStats2Client;Lcom/google/android/youtube/core/client/QoeStatsClient;)V

    iget-boolean v0, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->hq:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    if-eqz v4, :cond_3

    iget-object v0, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStatsClientState:Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Lcom/google/android/youtube/core/client/b;

    iget-object v1, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStatsClientState:Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

    invoke-interface {v0, v4, v1}, Lcom/google/android/youtube/core/client/b;->a(Lcom/google/android/youtube/core/model/VastAd;Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;)Lcom/google/android/youtube/core/client/AdStatsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    :goto_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ae:Lcom/google/android/youtube/core/async/n;

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/async/a/a;->a(Lcom/google/android/youtube/core/async/n;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->w()V

    return-void

    :cond_0
    move-object v4, v9

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->t:Lcom/google/android/youtube/core/client/bo;

    iget-object v1, p2, Lcom/google/android/youtube/core/player/Director$DirectorState;->statsClientState:Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/client/bo;->a(Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v7

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    if-eqz v4, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Lcom/google/android/youtube/core/client/b;

    invoke-interface {v0, v4}, Lcom/google/android/youtube/core/client/b;->a(Lcom/google/android/youtube/core/model/VastAd;)Lcom/google/android/youtube/core/client/AdStatsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    goto :goto_3

    :cond_4
    iput-object v9, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    goto :goto_3
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->x:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->ADULT_CONTENT_ERROR:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/v;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->g()Z

    move-result v0

    const-string v1, "call init() first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->h()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->v()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->c()V

    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/AdStatsClient;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/content/Context;Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 8

    const/4 v6, 0x0

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "play() called when the player wasn\'t loaded."

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iput-boolean v6, p0, Lcom/google/android/youtube/core/player/Director;->S:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    and-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ac:Lcom/google/android/youtube/core/player/Director$PlayerState;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$PlayerState;->PREPARED:Lcom/google/android/youtube/core/player/Director$PlayerState;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ENDED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_VIDEO:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->e()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->B()Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_AD:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/v;->a(Lcom/google/android/youtube/core/model/VastAd;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->D()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->C()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/VastAd;->isPublicVideo:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/VastAd;->isSkippable()Z

    move-result v3

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_2
    iget-object v4, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    invoke-interface {v2, v0, v3, v1, v4}, Lcom/google/android/youtube/core/player/overlay/a;->a(Ljava/lang/String;ZZLjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->J:Lcom/google/android/youtube/core/model/ak;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ak;->b:Lcom/google/android/youtube/core/model/Stream;

    move-object v7, v0

    :goto_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    if-nez v0, :cond_4

    const/16 v0, 0x1e

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget v1, v1, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    if-eqz v0, :cond_8

    :goto_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->t:Lcom/google/android/youtube/core/client/bo;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bo;->a()Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v0

    :goto_5
    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/VideoStats2Client;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->v:Lcom/google/android/youtube/core/client/bj;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/google/android/youtube/core/model/Stream;->getItag()I

    move-result v3

    invoke-interface {v1, v0, v2, v6, v3}, Lcom/google/android/youtube/core/client/bj;->a(Ljava/lang/String;Ljava/lang/String;ZI)Lcom/google/android/youtube/core/client/QoeStatsClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->C:Lcom/google/android/youtube/core/client/QoeStatsClient;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v7, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "cpn"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v7}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v7

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, v6}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setScrubbingEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    iget v1, p0, Lcom/google/android/youtube/core/player/Director;->F:I

    invoke-virtual {v0, v7, v1}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/model/Stream;I)V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    move v1, v6

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->J:Lcom/google/android/youtube/core/model/ak;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ak;->c:Lcom/google/android/youtube/core/model/Stream;

    move-object v7, v0

    goto :goto_3

    :cond_8
    move v3, v6

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->t:Lcom/google/android/youtube/core/client/bo;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget v2, v2, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    iget-object v4, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/VastAd;->adFormat:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/youtube/core/client/bo;->a(Ljava/lang/String;IILjava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v0

    goto :goto_5

    :cond_a
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->z()V

    goto/16 :goto_0
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/AdStatsClient;->b()V

    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Next"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/v;->g()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->F()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-direct {p0, v0, v2, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;ZI)V

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->w()V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/AdStatsClient;->c()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->i(Z)V

    :cond_0
    return-void
.end method

.method public final d(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Previous"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/v;->h()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->F()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-direct {p0, v0, v2, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;ZI)V

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->v()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->b()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/VastAd;->isPublicVideo:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/AdStatsClient;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->AD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    :cond_0
    return-void
.end method

.method public final e(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    and-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHQ(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "HQ"

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    if-eqz v0, :cond_2

    const-string v0, "On"

    :goto_1
    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/model/ak;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_AD:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v0, "Off"

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/Director;->E:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->z()V

    goto :goto_2
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->ADULT_CONTENT_DECLINED:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/v;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    return-void
.end method

.method public final f(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->j()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->G()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/VideoStats2Client;->f()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/as;->b(Landroid/os/Handler;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/as;->b(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->o:Lcom/google/android/youtube/core/player/ap;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ap;->b()V

    return-void
.end method

.method public final g(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->M:Z

    if-eq p1, v0, :cond_1

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/Director;->M:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/overlay/a;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/a;->setFullscreen(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->aa:Lcom/google/android/youtube/core/player/v;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/v;->a_(Z)V

    :cond_1
    return-void
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setShowFullscreen(Z)V

    return-void
.end method

.method public final h()Z
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_AD:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->f()V

    :cond_0
    return-void
.end method

.method public final j()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->H()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/Director;->F:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->r()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/Director;->E:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->S:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->h()V

    return-void
.end method

.method public final k()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->e()Z

    move-result v0

    return v0
.end method

.method public final m()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "CC"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/player/an;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/an;->a()V

    return-void
.end method

.method public final n()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->U:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Z)V

    :cond_0
    :goto_0
    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/Director;->U:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/Director;->V:Z

    return-void

    :cond_1
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/player/d;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/d;->a(Lcom/google/android/youtube/core/model/Video;)V

    goto :goto_0
.end method

.method public final o()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADING:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->U:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->t()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->V:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/VideoStats2Client;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Lcom/google/android/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Lcom/google/android/youtube/core/client/QoeStatsClient;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/QoeStatsClient;->e()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->G()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->j()V

    return-void
.end method

.method public final p()Lcom/google/android/youtube/core/model/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_VIDEO:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ENDED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->s()I

    move-result v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/player/Director;->E:I

    goto :goto_0
.end method

.method public final s()I
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_VIDEO:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->ac:Lcom/google/android/youtube/core/player/Director$PlayerState;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$PlayerState;->PREPARED:Lcom/google/android/youtube/core/player/Director$PlayerState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->d()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Lcom/google/android/youtube/core/model/Video;

    iget v0, v0, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v0, v0, 0x3e8

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_VIDEO:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->PLAYING_AD:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->c:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/as;->b()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->ac:Lcom/google/android/youtube/core/player/Director$PlayerState;

    sget-object v3, Lcom/google/android/youtube/core/player/Director$PlayerState;->PREPARING:Lcom/google/android/youtube/core/player/Director$PlayerState;

    if-ne v2, v3, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/Director;->S:Z

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ENDED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final u()Lcom/google/android/youtube/core/player/Director$DirectorState;
    .locals 13

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->t()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/Director;->V:Z

    if-eqz v2, :cond_1

    :cond_0
    move v3, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->ad:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    sget-object v4, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->ENDED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    if-ne v2, v4, :cond_2

    move v4, v1

    :goto_1
    new-instance v0, Lcom/google/android/youtube/core/player/Director$DirectorState;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->K:Lcom/google/android/youtube/core/model/VastAd;

    iget-boolean v5, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->H()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->r()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    if-nez v8, :cond_3

    move-object v8, v10

    :goto_2
    iget-object v9, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    if-nez v9, :cond_4

    move-object v9, v10

    :goto_3
    iget-object v11, p0, Lcom/google/android/youtube/core/player/Director;->C:Lcom/google/android/youtube/core/client/QoeStatsClient;

    if-nez v11, :cond_5

    :goto_4
    iget-object v11, p0, Lcom/google/android/youtube/core/player/Director;->G:Ljava/util/Map;

    iget-boolean v12, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    invoke-direct/range {v0 .. v12}, Lcom/google/android/youtube/core/player/Director$DirectorState;-><init>(Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/model/VastAd;ZZZIILcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;Ljava/util/Map;Z)V

    return-object v0

    :cond_1
    move v3, v0

    goto :goto_0

    :cond_2
    move v4, v0

    goto :goto_1

    :cond_3
    iget-object v8, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/client/VideoStats2Client;

    invoke-interface {v8}, Lcom/google/android/youtube/core/client/VideoStats2Client;->g()Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    move-result-object v8

    goto :goto_2

    :cond_4
    iget-object v9, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/client/AdStatsClient;

    invoke-interface {v9}, Lcom/google/android/youtube/core/client/AdStatsClient;->j()Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

    move-result-object v9

    goto :goto_3

    :cond_5
    iget-object v10, p0, Lcom/google/android/youtube/core/player/Director;->C:Lcom/google/android/youtube/core/client/QoeStatsClient;

    invoke-interface {v10}, Lcom/google/android/youtube/core/client/QoeStatsClient;->i()Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    move-result-object v10

    goto :goto_4
.end method

.method public final v_()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Z

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Z)V

    return-void
.end method
