.class final Lcom/google/android/youtube/core/player/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ao;
.implements Lcom/google/android/youtube/core/player/aq;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/Director;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/Director;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/Director;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/y;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->f(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHasCc(Z)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->o(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ap;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/ap;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->f(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->a(Ljava/util/List;)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->f(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHasCc(Z)V

    return-void
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->y(Lcom/google/android/youtube/core/player/Director;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0031

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->f(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setCcEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->r(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->o(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/Director;->r(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/as;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/as;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/ap;->b(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->v(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/v;

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->f(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setCcEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/y;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->v(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/v;

    return-void
.end method
