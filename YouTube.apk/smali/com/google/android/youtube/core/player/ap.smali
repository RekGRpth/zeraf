.class public final Lcom/google/android/youtube/core/player/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/youtube/core/async/n;
.implements Lcom/google/android/youtube/core/utils/an;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/youtube/core/player/overlay/aa;

.field private final c:Lcom/google/android/youtube/core/player/aq;

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Lcom/google/android/youtube/core/client/bl;

.field private final f:Lcom/google/android/youtube/core/utils/al;

.field private g:Lcom/google/android/youtube/core/model/Subtitles;

.field private h:Ljava/util/List;

.field private i:I

.field private j:Lcom/google/android/youtube/core/async/p;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/player/aq;Lcom/google/android/youtube/core/client/bl;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ap;->a:Landroid/os/Handler;

    const-string v0, "subtitlesOverlay cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/aa;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ap;->b:Lcom/google/android/youtube/core/player/overlay/aa;

    const-string v0, "subtitlesClient cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bl;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ap;->e:Lcom/google/android/youtube/core/client/bl;

    const-string v0, "listener cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/aq;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ap;->c:Lcom/google/android/youtube/core/player/aq;

    new-instance v0, Lcom/google/android/youtube/core/utils/al;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/al;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ap;->f:Lcom/google/android/youtube/core/utils/al;

    const-string v0, "preferences cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ap;->d:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/ap;->e()V

    return-void
.end method

.method private e()V
    .locals 5

    const/4 v1, 0x4

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ap;->d:Landroid/content/SharedPreferences;

    const-string v3, "subtitles_size"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-le v2, v1, :cond_0

    sparse-switch v2, :sswitch_data_0

    :goto_0
    :sswitch_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/ap;->d:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "subtitles_size"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/core/player/ap;->b:Lcom/google/android/youtube/core/player/overlay/aa;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/overlay/aa;->setFontSizeLevel(I)V

    return-void

    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :sswitch_3
    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_1
        0x12 -> :sswitch_0
        0x19 -> :sswitch_2
        0x23 -> :sswitch_3
    .end sparse-switch
.end method

.method private f()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/ap;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->g:Lcom/google/android/youtube/core/model/Subtitles;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->c:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->e()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ap;->g:Lcom/google/android/youtube/core/model/Subtitles;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/ap;->g:Lcom/google/android/youtube/core/model/Subtitles;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/ap;->b:Lcom/google/android/youtube/core/player/overlay/aa;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ap;->g:Lcom/google/android/youtube/core/model/Subtitles;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/model/Subtitles;->getSubtitleWindowSnapshotsAt(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/overlay/aa;->a(Ljava/util/List;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/ap;->g:Lcom/google/android/youtube/core/model/Subtitles;

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget v1, p0, Lcom/google/android/youtube/core/player/ap;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/youtube/core/player/ap;->i:I

    iget v1, p0, Lcom/google/android/youtube/core/player/ap;->i:I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ap;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/android/youtube/core/player/ap;->i:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/ap;->f()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->j:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->j:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ap;->j:Lcom/google/android/youtube/core/async/p;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->j:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->j:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/SubtitleTrack;->isDisableOption()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/ap;->f()V

    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ap;->j:Lcom/google/android/youtube/core/async/p;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->e:Lcom/google/android/youtube/core/client/bl;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/ap;->a:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ap;->j:Lcom/google/android/youtube/core/async/p;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/client/bl;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "error retrieving subtitle"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/ap;->f()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    check-cast p2, Lcom/google/android/youtube/core/model/Subtitles;

    iput-object p2, p0, Lcom/google/android/youtube/core/player/ap;->g:Lcom/google/android/youtube/core/model/Subtitles;

    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/Subtitles;->getEventTimes()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ap;->h:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->c:Lcom/google/android/youtube/core/player/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->d()V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public final b(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->g:Lcom/google/android/youtube/core/model/Subtitles;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->b:Lcom/google/android/youtube/core/player/overlay/aa;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/ap;->g:Lcom/google/android/youtube/core/model/Subtitles;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/model/Subtitles;->getSubtitleWindowSnapshotsAt(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/aa;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->h:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    add-int/lit8 v0, v0, 0x1

    :goto_0
    iput v0, p0, Lcom/google/android/youtube/core/player/ap;->i:I

    iget v0, p0, Lcom/google/android/youtube/core/player/ap;->i:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/ap;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/ap;->f:Lcom/google/android/youtube/core/utils/al;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->h:Ljava/util/List;

    iget v2, p0, Lcom/google/android/youtube/core/player/ap;->i:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, p0, p1, v0}, Lcom/google/android/youtube/core/utils/al;->a(Lcom/google/android/youtube/core/utils/an;II)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    xor-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/ap;->d()V

    goto :goto_1
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->f:Lcom/google/android/youtube/core/utils/al;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/utils/al;->a(Lcom/google/android/youtube/core/utils/an;)V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->f:Lcom/google/android/youtube/core/utils/al;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/utils/al;->a(Lcom/google/android/youtube/core/utils/an;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->b:Lcom/google/android/youtube/core/player/overlay/aa;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/aa;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ap;->b:Lcom/google/android/youtube/core/player/overlay/aa;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/aa;->e()V

    return-void
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    const-string v0, "subtitles_size"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/ap;->e()V

    :cond_0
    return-void
.end method
