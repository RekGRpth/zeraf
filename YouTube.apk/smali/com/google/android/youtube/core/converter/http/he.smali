.class final Lcom/google/android/youtube/core/converter/http/he;
.super Lcom/google/android/youtube/core/converter/n;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/converter/http/hc;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/converter/http/hc;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/converter/http/he;->a:Lcom/google/android/youtube/core/converter/http/hc;

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/n;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/utils/ae;Lorg/xml/sax/Attributes;)V
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/model/am;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/am;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/he;->a:Lcom/google/android/youtube/core/converter/http/hc;

    const-string v2, "timeOffset"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/youtube/core/converter/http/hc;->a(Lcom/google/android/youtube/core/converter/http/hc;Ljava/lang/String;Lcom/google/android/youtube/core/model/am;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/he;->a:Lcom/google/android/youtube/core/converter/http/hc;

    const-string v2, "breakType"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/youtube/core/converter/http/hc;->b(Lcom/google/android/youtube/core/converter/http/hc;Ljava/lang/String;Lcom/google/android/youtube/core/model/am;)V

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/utils/ae;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 2

    const-class v0, Lcom/google/android/youtube/core/model/am;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/am;

    const-class v1, Lcom/google/android/youtube/core/model/ap;

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/core/utils/ae;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/ap;

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/am;->b()Lcom/google/android/youtube/core/model/VmapAdBreak;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/model/ap;->a(Lcom/google/android/youtube/core/model/VmapAdBreak;)Lcom/google/android/youtube/core/model/ap;

    goto :goto_0
.end method
