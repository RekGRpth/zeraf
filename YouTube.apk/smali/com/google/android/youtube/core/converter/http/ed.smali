.class public final Lcom/google/android/youtube/core/converter/http/ed;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/youtube/core/converter/e;)V
    .locals 2

    const-string v0, ""

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/converter/http/ed;->b(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V

    const-string v0, "/entry"

    new-instance v1, Lcom/google/android/youtube/core/converter/http/ef;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/ef;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    return-void
.end method

.method public static a(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V
    .locals 2

    invoke-static {p0, p1}, Lcom/google/android/youtube/core/converter/http/ed;->b(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/converter/http/ee;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/ee;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    return-void
.end method

.method private static b(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/entry/summary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/converter/http/ej;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/ej;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/link"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ei;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/ei;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/author/name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/eh;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/eh;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/author/uri"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/eg;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/eg;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    return-void
.end method
