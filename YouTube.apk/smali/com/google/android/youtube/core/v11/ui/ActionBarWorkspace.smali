.class public Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;
.super Lcom/google/android/youtube/core/ui/AbstractWorkspace;
.source "SourceFile"


# instance fields
.field private final b:Landroid/app/ActionBar$TabListener;

.field private c:Landroid/app/ActionBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/youtube/core/v11/ui/a;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/v11/ui/a;-><init>(Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->b:Landroid/app/ActionBar$TabListener;

    return-void
.end method

.method public static a(Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;Landroid/app/ActionBar;I)Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;
    .locals 6

    const/4 v2, 0x0

    const-string v0, "workspace may not be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "actionBar may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->c:Landroid/app/ActionBar;

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->c:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->removeAllTabs()V

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->c:Landroid/app/ActionBar;

    iget-object v4, p0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->c:Landroid/app/ActionBar;

    invoke-virtual {v4}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v4

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->b:Landroid/app/ActionBar$TabListener;

    invoke-virtual {v4, v5}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->c:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getTabCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    :goto_2
    iget-object v1, p0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->c:Landroid/app/ActionBar;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->requestLayout()V

    return-object p0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->c(I)V

    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->c:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->c:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_0
    return-void
.end method
