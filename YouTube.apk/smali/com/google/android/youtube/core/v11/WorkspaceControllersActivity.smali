.class public abstract Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;
.super Lcom/google/android/youtube/core/v11/ControllerActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/ui/b;


# instance fields
.field private a:Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;

.field private b:Landroid/app/ActionBar;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/ControllerActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/v11/ControllerActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->a:Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->a:Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->a:Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->setOnTabSelectedListener(Lcom/google/android/youtube/core/ui/b;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->b:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->b:Landroid/app/ActionBar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->b:Landroid/app/ActionBar;

    const/16 v1, 0x10

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    return-void
.end method

.method protected onStart()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/youtube/core/v11/ControllerActivity;->onStart()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->c:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->a:Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;

    iget-object v1, p0, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->b:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->a()Lcom/google/android/youtube/core/v11/Controller;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/v11/WorkspaceControllersActivity;->a(Lcom/google/android/youtube/core/v11/Controller;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;->a(Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;Landroid/app/ActionBar;I)Lcom/google/android/youtube/core/v11/ui/ActionBarWorkspace;

    :cond_0
    return-void
.end method
