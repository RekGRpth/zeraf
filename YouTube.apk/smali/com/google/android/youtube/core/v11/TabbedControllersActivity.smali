.class public abstract Lcom/google/android/youtube/core/v11/TabbedControllersActivity;
.super Lcom/google/android/youtube/core/v11/ControllerActivity;
.source "SourceFile"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# instance fields
.field private a:Landroid/app/ActionBar;

.field private b:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/ControllerActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/v11/ControllerActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/TabbedControllersActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/TabbedControllersActivity;->a:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/TabbedControllersActivity;->a:Landroid/app/ActionBar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/TabbedControllersActivity;->a:Landroid/app/ActionBar;

    const/16 v1, 0x10

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/TabbedControllersActivity;->b:Ljava/util/Map;

    return-void
.end method
